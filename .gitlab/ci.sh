#!/usr/bin/env bash

#Adapted from the GHC .gitlab/ci.sh file

TOP="$CI_PROJECT_DIR"
if [ ! -d "$TOP/.gitlab" ]; then
	echo "This script expects to be run from the root of a checkout"
fi

export CABAL_CACHE="$TOP/cabal-cache"
mkdir -p "$CABAL_CACHE"
export CABAL_DIR="$TOP/cabal"
mkdir -p "$CABAL_DIR"
function setup() {
	if [ -d "$CABAL_CACHE" ]; then
#		info "Extracting cabal cache from $CABAL_CACHE to $CABAL_DIR..."
		mkdir -p "$CABAL_DIR"
		cp -Rf "$CABAL_CACHE"/* "$CABAL_DIR"
	fi
}

function save_cache () {
	#	info "Storing cabal cache from $CABAL_DIR to $CABAL_CACHE..."
	#	rm -Rf "$CABAL_CACHE"
	#	cp -Rf "$CABAL_DIR" "$CABAL_CACHE"
	return
}
