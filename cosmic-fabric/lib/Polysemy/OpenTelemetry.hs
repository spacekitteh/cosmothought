{-# OPTIONS_GHC -Wwarn=orphans #-}
module Polysemy.OpenTelemetry where
import Control.Monad
import Control.Lens (view, (<&>))
import Control.Exception (Exception)
import Polysemy
import GHC.Exts (IsList(..))    
import OpenTelemetry.Attributes hiding (addAttributes)
import qualified OpenTelemetry.Trace.Core as OT
--import Polysemy.Error
--import Polysemy.Conc.Effect.Scoped
import Polysemy.Resource
import Polysemy.Input
import GHC.Stack
import Prelude
import Data.Text as T
import qualified OpenTelemetry.Context as OTC
import qualified OpenTelemetry.Context.ThreadLocal as OTCT
import OpenTelemetry.Common
import Control.Monad.IO.Class
import OpenTelemetry.Context as OT
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HMS
    
instance OT.HasTracer OT.Tracer where
    {-# INLINE tracerL #-}
    tracerL = id
    
data OpenTelemetryContext m a where
  GetContext :: OpenTelemetryContext m OT.Context
  AdjustContext :: (OT.Context->OT.Context) -> OpenTelemetryContext m ()
  AttachContext :: OT.Context -> OpenTelemetryContext m (Maybe OT.Context)
  ForceFlushGlobalTracerProvider :: Maybe Int -> OpenTelemetryContext m ()
makeSem ''OpenTelemetryContext
{-# INLINE runOpenTelemetryContext #-}
runOpenTelemetryContext :: Members '[Embed IO] r => InterpreterFor OpenTelemetryContext r
runOpenTelemetryContext = interpret \case
  GetContext -> embed OTCT.getContext
  AdjustContext f -> embed $ OTCT.adjustContext f
  AttachContext ctx -> embed $ OTCT.attachContext ctx
  ForceFlushGlobalTracerProvider timeout -> do
                                              provider <- OT.getGlobalTracerProvider
                                              void $! OT.forceFlushTracerProvider provider timeout



-- Operations on a Span. Intended to be used with a scoped spanargs?
data OpenTelemetrySpan m a where
  CreateSpanWithoutCallStack :: OT.Tracer -> OT.Context -> Text -> OT.SpanArguments -> OpenTelemetrySpan m OT.Span
  CreateSpan ::  OT.Tracer -> OT.Context -> Text -> OT.SpanArguments -> OpenTelemetrySpan m OT.Span
  EndSpan :: OT.Span -> Maybe Timestamp -> OpenTelemetrySpan m ()
  IsSpanRecording :: OT.Span -> OpenTelemetrySpan m Bool
  AddAttributes' :: OT.Span -> HashMap Text Attribute -> OpenTelemetrySpan m ()
  AddEvent' :: OT.Span -> OT.NewEvent -> OpenTelemetrySpan m ()
  RecordException' :: Exception e => OT.Span -> HashMap Text Attribute -> Maybe Timestamp -> e -> OpenTelemetrySpan m ()
  SetSpanStatus' :: OT.Span -> OT.SpanStatus -> OpenTelemetrySpan m ()
{-# INLINE addEvent' #-}
addEvent' :: Members '[OpenTelemetrySpan] r => OT.Span -> OT.NewEvent -> Sem r ()
addEvent' span event = send $ AddEvent' span event
                       
{-# INLINE addAttributes' #-}
{-# SPECIALIZE INLINE addAttributes' :: (Members '[OpenTelemetrySpan] r) => OT.Span -> HashMap Text Attribute -> Sem r () #-}
addAttributes' :: (Members '[OpenTelemetrySpan] r, IsList attrs, Item (attrs) ~ (Text,Attribute)) => OT.Span -> attrs -> Sem r ()
addAttributes' span attrs = send $ AddAttributes' span (fromList . toList $ attrs)
{-# INLINE createSpanWithoutCallStack #-}                     
createSpanWithoutCallStack :: (Members '[OpenTelemetrySpan] r) => OT.Tracer -> OT.Context -> Text -> OT.SpanArguments -> Sem r OT.Span
createSpanWithoutCallStack tracer context name args = send $ CreateSpanWithoutCallStack tracer context name args

{-# INLINE recordException' #-}
recordException' :: (Exception e, Members '[OpenTelemetrySpan] r, IsList attrs, Item (attrs) ~ (Text,Attribute)) =>  OT.Span -> attrs -> Maybe Timestamp -> e -> Sem r ()
recordException' span attrs ts e = send $ RecordException' span (fromList . toList $  attrs) ts e
{-# INLINE setSpanStatus' #-}
setSpanStatus' :: Members '[OpenTelemetrySpan] r => OT.Span -> OT.SpanStatus -> Sem r ()
setSpanStatus' span status = send $ SetSpanStatus' span status                                                      
{-# INLINE endSpan #-}
endSpan :: Members '[OpenTelemetrySpan] r => OT.Span -> Maybe Timestamp -> Sem r ()
endSpan span ts = send $ EndSpan span ts
{-# INLINE createSpan #-}
createSpan :: (HasCallStack, Members '[OpenTelemetrySpan] r) => OT.Tracer -> OT.Context -> Text -> OT.SpanArguments -> Sem r OT.Span
createSpan tracer context text args = withFrozenCallStack (send $ CreateSpan  tracer context text args)
{-# INLINE isSpanRecording #-}
isSpanRecording :: Members '[OpenTelemetrySpan] r => OT.Span -> Sem r Bool
isSpanRecording span = send $ IsSpanRecording span
{-# INLINE whenSpanIsRecording #-}
whenSpanIsRecording :: (Monoid a, Members '[OpenTelemetrySpan] r) => OT.Span -> Sem r a -> Sem r a
whenSpanIsRecording span a = do
  wellIsIt <- isSpanRecording span
  if wellIsIt then a else pure mempty


{-# INLINE inSpan #-}
inSpan :: (OT.HasTracer agent, HasCallStack, Members '[Input agent, Resource, OpenTelemetryContext, OpenTelemetrySpan] r) => Text -> OT.SpanArguments -> Sem r a-> Sem r a
inSpan n args m = withFrozenCallStack do
  inSpan''  callStack n args (const m)
                                      
{-# INLINE inSpan' #-}
inSpan' :: (OT.HasTracer agent, HasCallStack, Members '[Input agent, Resource, OpenTelemetryContext, OpenTelemetrySpan] r) => Text -> OT.SpanArguments -> (OT.Span -> Sem r a) -> Sem r a
inSpan' = withFrozenCallStack $ inSpan''  callStack

{-# INLINEABLE inSpan'' #-}            
inSpan'' :: (OT.HasTracer agent, Members '[Input agent, Resource, OpenTelemetryContext, OpenTelemetrySpan] r) => CallStack -> Text -> OT.SpanArguments -> (OT.Span -> Sem r a) -> Sem r a
inSpan'' cs n args f = withFrozenCallStack $  do
  tracer <- input <&> view OT.tracerL
  inSpan''_ tracer cs n args f

     
{-# INLINE inSpan_ #-}
inSpan_ :: (HasCallStack, Members '[Resource, OpenTelemetryContext, OpenTelemetrySpan] r) => OT.Tracer -> Text -> OT.SpanArguments -> Sem r a-> Sem r a
inSpan_ t n args m =  withFrozenCallStack $ inSpan''_ t ( callStack) n args (const m)
                                      
{-# INLINE inSpan'_ #-}
inSpan'_ :: (HasCallStack, Members '[Resource, OpenTelemetryContext, OpenTelemetrySpan] r) => OT.Tracer -> Text -> OT.SpanArguments -> (OT.Span -> Sem r a) -> Sem r a
inSpan'_ t = withFrozenCallStack $ inSpan''_ t (callStack)

{-# INLINEABLE inSpan''_ #-}            
inSpan''_ :: ( Members '[Resource, OpenTelemetryContext, OpenTelemetrySpan] r) => OT.Tracer -> CallStack -> Text -> OT.SpanArguments -> (OT.Span -> Sem r a) -> Sem r a
inSpan''_ t cs n args f =  do
  bracket
    (do
      ctx <- getContext
      s <- createSpanWithoutCallStack t ctx n args
      adjustContext (OT.insertSpan s)
     -- whenSpanIsRecording s $
      do
        case getCallStack (popCallStack cs) of
          [] -> pure ()
          (fn, loc):_ -> do
            addAttributes' s
              [ ("code.function", toAttribute $ T.pack fn)
              , ("code.namespace", toAttribute $ T.pack $ srcLocModule loc)
              , ("code.filepath", toAttribute $ T.pack $ srcLocFile loc)
              , ("code.lineno", toAttribute $ srcLocStartLine loc)
              , ("code.package", toAttribute $ T.pack $ srcLocPackage loc)
              ]
      pure (lookupSpan ctx, s)
     )
     (\(parent,s) -> do -- TODO figure out how to grab the exception raised.
       endSpan s Nothing
       adjustContext $ \ctx ->
           maybe (OT.removeSpan ctx) (`OT.insertSpan` ctx) parent
     )
     (\(_,s) -> f s)

{-# INLINEABLE withContext' #-}     
withContext' :: MonadIO m => Context -> m a -> m a
withContext' ctx m = do
  priorCtx <- OTCT.attachContext ctx
  res <- m
  case priorCtx of
    Nothing -> pure ()
    Just prior -> do
        void $  OTCT.attachContext prior
  pure res
     
{-# INLINEABLE withContext #-}     
withContext ::  Members '[OpenTelemetryContext] r => Context -> Sem r a -> Sem r a
withContext ctx m = do
  priorCtx <- attachContext ctx
  res <- m
  case priorCtx of
    Nothing -> pure ()
    Just prior -> do
        void $ attachContext prior
             
  pure res

{-# INLINEABLE withFreshContext' #-}       
withFreshContext' :: MonadIO m  => m a -> m a
withFreshContext' action = withContext' OTC.empty action
       
{-# INLINEABLE withFreshContext #-}       
withFreshContext :: Members '[OpenTelemetryContext] r => Sem r a -> Sem r a
withFreshContext action = withContext OTC.empty action

                          
     
{-# INLINE runOpenTelemetrySpan #-}
runOpenTelemetrySpan :: ( Members '[Embed IO] r) => InterpreterFor OpenTelemetrySpan r
runOpenTelemetrySpan  = interpret \case
  CreateSpanWithoutCallStack tracer context n args -> embed $ OT.createSpanWithoutCallStack tracer context n args
  CreateSpan tracer context n args -> embed $ OT.createSpan tracer context n args
  IsSpanRecording span -> embed $  OT.isRecording span
  EndSpan span time -> embed $ OT.endSpan span time
  AddAttributes' span attrs -> embed $  OT.addAttributes span attrs
  AddEvent' span event -> embed $ OT.addEvent span event
  RecordException' span attrs ts e -> embed $  OT.recordException span attrs ts e
  SetSpanStatus' span status -> embed $ OT.setStatus span status

{-# INLINE addAttributes #-}
addAttributes :: (IsList attrs, Item (attrs) ~ (Text,Attribute), Members '[OpenTelemetryContext, OpenTelemetrySpan] r) => attrs -> Sem r ()
addAttributes ~attrs = withCurrentSpan (\span -> addAttributes' span attrs)
{-# INLINE withCurrentSpan #-}
withCurrentSpan :: Members '[OpenTelemetryContext, OpenTelemetrySpan] r => (OT.Span -> Sem r ()) -> Sem r ()
withCurrentSpan f = do
  ctx <- getContext
  case OT.lookupSpan ctx of
    Nothing -> pure ()
    Just span -> f span
{-# INLINE addEvent #-}
addEvent :: (IsList attrs, Item (attrs) ~ (Text,Attribute), Members '[OpenTelemetryContext, OpenTelemetrySpan] r) => Text -> attrs -> Sem r ()
addEvent ~name ~attrs = withCurrentSpan (\span -> addEvent' span (OT.NewEvent name (fromList . toList $  attrs) Nothing))

recordException :: (Exception e, Members '[OpenTelemetryContext, OpenTelemetrySpan] r) => e ->  [(Text, Attribute)]  -> Sem r ()
recordException exc attrs = withCurrentSpan (\span -> recordException' span attrs Nothing exc)
{-# INLINE setSpanStatus #-}
setSpanStatus :: Members '[OpenTelemetryContext, OpenTelemetrySpan] r => OT.SpanStatus -> Sem r ()
setSpanStatus status = withCurrentSpan (\span -> setSpanStatus' span status)

class HasWellKnownAttributes a where
    toAttributes :: a -> HashMap Text Attribute

instance {-# OVERLAPPABLE #-} HasWellKnownAttributes a where
    toAttributes _ = HMS.empty

                     
addWellKnownAttributes :: (HasWellKnownAttributes a, Members '[OpenTelemetryContext, OpenTelemetrySpan] r) => a -> Sem r ()
addWellKnownAttributes a = withCurrentSpan (\span -> addAttributes' span (toAttributes a))
