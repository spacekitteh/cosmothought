module Logic.Symbolic.SAT.First where
import           Algebra.Heyting.Free.Expr
import           Polysemy
import           Polysemy.State
import           Control.Effects.Commentary
import           Control.Effects.Progress
import           Control.Effects.WorkingMemory
import           Control.Effects.Configuration
import           Control.Effects.Log
import           Control.Effects.Substitution
import qualified Data.HashMap.Strict           as HM
import qualified Data.HashSet                  as HS
import           Polysemy.Fresh
import           Polysemy.Error
import           Data.Unique
import           Data.Data
import           GHC.Generics
import           Control.Lens
import           Data.Hashable
import           Data.Traversable
import           Polysemy.NonDet
import           Control.Lens.Plated
import           Polysemy.Async
import           Data.Monoid
import           GHC.Types                      ( SPEC(..) )

type Atom = String-- Unique
type Prop = Expr Atom
type Valuation = Substitution Atom Prop
instance Plated Prop
data UnboundVariable = UnboundVariable Atom deriving (Eq, Show)

pattern Not p = p :=>: Bottom

{-# INLINE lookupVar #-}
lookupVar :: Members '[Valuation, Error UnboundVariable] r => Atom -> Sem r Prop
lookupVar a = do
    result <- lookupVariable a
    case result of
        Just x  -> return x
        Nothing -> throw (UnboundVariable a)
{-# INLINE applySubstitution #-}
applySubstitution :: Members '[Valuation] r => Prop -> Sem r Prop
applySubstitution t = flip rewriteM t $ \case
    Var x -> lookupVariable x
    _     -> return Nothing

type StandardEffects r
    = Members '[Commentary, Async,{-Logger String, Progress,-}
                                   Valuation] r

data SATResult = Satisfiable (HM.HashMap Atom Prop) | Unsatisfiable | GaveUp
    deriving (Eq,Data, Generic, Show)

type SATEffects r = (StandardEffects r, Member SAT r)
data SAT m a where
    AddTerms ::Traversable t => t Prop -> SAT m ()
    Solve ::SAT m SATResult

makeSem ''SAT

{-# INLINE atoms #-}
atoms :: (Eq a, Hashable a) => Fold (Expr a) a
atoms = folding (foldr HS.insert HS.empty)

{-# INLINE trivialSimplifications #-}
trivialSimplifications :: StandardEffects r => Prop -> Sem r Prop
trivialSimplifications = rewriteM trivialSimplificationRules
{-# INLINABLE trivialSimplificationRules #-}
trivialSimplificationRules :: StandardEffects r => Prop -> Sem r (Maybe Prop)
trivialSimplificationRules prop = do
    explanation "This function applies trivial simplifications to the input."
    case prop of
        Bottom :/\: _      -> return (Just Bottom)
        Top    :/\: p      -> return (Just p)
        _      :/\: Bottom -> return (Just Bottom)
        p      :/\: Top    -> return (Just p)
        Top    :\/: _      -> return (Just Top)
        Bottom :\/: p      -> return (Just p)
        p      :\/: Bottom -> return (Just p)
        _      :\/: Top    -> return (Just Top)
        Var x -> lookupVariable x
        Not (Not p)        -> do
            explanation
                "Because we are not yet directly encoding negation in our term structure, it is encoded as 'p => False'. We therefore remove double negations."
            assumption "Classical logic"
            return (Just p)
        _      :=>: Top -> pure (Just Top)
        Top    :=>: p   -> return (Just p)
        Bottom :=>: _   -> do
            assumption "Classical logic"
            return (Just Top)
        _ -> return Nothing

{-# INLINE evaluate #-}
evaluate
    :: (StandardEffects r, Member (Error UnboundVariable) r)
    => Prop
    -> Sem r Bool
evaluate prop = do
    comment "We proceed by simple induction."
    case prop of
        Top    -> pure True
        Bottom -> pure False
        Var v  -> do
            term <- lookupVar v
            evaluate term
        a :/\: b -> do
            a' <- evaluate a
            b' <- evaluate b
            return (a' && b')
        a :\/: b -> do
            a' <- evaluate a
            b' <- evaluate b
            return (a' || b')
        a :=>: b -> do
            assumption "Classical logic"
            a' <- evaluate a
            b' <- evaluate b
            return ((not a') || b')

{-# INLINE trivial #-}
trivial :: StandardEffects r => Prop -> Sem r (Maybe Bool)
trivial prop = do
    p <- trivialSimplifications prop
    case p of
        Top    -> return (Just True)
        Bottom -> return (Just False)
        _      -> return Nothing

{-# INLINE generateAssignments #-}
generateAssignments :: [Atom] -> [[(Atom, Prop)]]
generateAssignments [] = []
generateAssignments l  = go SPEC [[]] l  where
    go _     acc []       = acc
    go !sPEC acc (a : as) = do
        value <- [Top, Bottom]
        map ((a, value) :) (go sPEC acc as)

{-# INLINABLE trivialSAT #-}
trivialSAT :: (StandardEffects r, Members '[Async] r) => Prop -> Sem r SATResult
trivialSAT prop = do
    p <- trivialSimplifications prop
    case p of
        Top -> do
            subs <- substitutions
            return (Satisfiable (HM.fromList subs))
        Bottom -> return Unsatisfiable
        _      -> do
            let vars        = p ^.. atoms
                assignments = generateAssignments vars
            spawn <- for assignments $ \assignment -> async $ do
                addSubstitutions assignment
                p' <- applySubstitution p
                trivialSAT p'
            results <- traverse await spawn
            let possibleResult =
                    getFirst $ foldMap (First . satToMaybe) results
            case possibleResult of
                Just result -> return (Satisfiable result)
                _ | all (\r -> r == (Just Unsatisfiable)) results ->
                    return Unsatisfiable
                _ -> return GaveUp
{-# INLINABLE satToMaybe #-}
satToMaybe :: Maybe SATResult -> Maybe (HM.HashMap Atom Prop)
satToMaybe (Just (Satisfiable p)) = Just p
satToMaybe _                      = Nothing
z = Var "c" :/\: (Var "a" :=>: Bottom)
{-# INLINE interpretSAT #-}
interpretSAT
    :: (Members '[State Prop] r, StandardEffects r)
    => Sem (SAT : r) a
    -> Sem r a
interpretSAT = interpret $ \case
    AddTerms newTerms -> do
        simplified <- mapM trivialSimplifications newTerms
        let conj = foldr (:/\:) Top simplified
        existing <- get
        newTerm  <- trivialSimplifications (existing :/\: conj)
        put newTerm
    Solve -> do
        terms <- get
        comment
            "For our first attempt, we will just see if it is naively satisfiable."
        trivialSAT terms


{-# INLINE runSAT #-}
runSAT :: StandardEffects r => Sem (SAT : State Prop : r) a -> Sem r a
runSAT s = (evalState Top) $ interpretSAT s

test :: Prop -> IO (Either UnboundVariable SATResult)
test a =
    runFinal
        $ embedToFinal
        $ errorToIOFinal
        $ ignoreCommentary
        $ asyncToIOFinal
        $ evalState HM.empty
        $ runSubstitutionsAsHashMapState
        $ trivialSAT a
