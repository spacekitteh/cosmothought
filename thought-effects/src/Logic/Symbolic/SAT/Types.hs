module Logic.Symbolic.SAT.Types where
import Polysemy
import Data.Data
import GHC.Generics
import Control.Effects.Substitution
import Control.Effects.Commentary
import Polysemy.Async
import qualified Data.HashMap.Strict as HM
import qualified Data.HashSet as HS
import Logic.Symbolic.SAT.Representations.Types
import Logic.Symbolic.SAT.Representations.Expression
import Polysemy.Fresh
import Control.Effects.HypothesisTesting
type StandardEffects r
    = Members '[Commentary, Async, Fresh Variable,
                                    HypothesisTesting (HS.HashSet Variable)
                                    {-Logger String, Progress,-}
                                   , Valuation] r

newtype UnsatCore = UnsatCore (HS.HashSet Variable) deriving (Eq, Data, Generic, Show)
data SATResult = Satisfiable (HM.HashMap Variable Prop)
    | Unsatisfiable UnsatCore
    | GaveUp
    deriving (Eq,Data, Generic, Show)

{-#INLINE getUnsatCore#-}
getUnsatCore :: SATResult -> Maybe (HS.HashSet Variable)
getUnsatCore (Unsatisfiable (UnsatCore c)) = Just c
getUnsatCore _ = Nothing



data TooComplicatedForTautology = TooComplicatedForTautology

