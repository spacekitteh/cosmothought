module Logic.Symbolic.SAT.Simplification where
import           Logic.Symbolic.SAT.Types
import           Logic.Symbolic.SAT.Representations.Types
import           Logic.Symbolic.SAT.Representations.Expression
import           Logic.Symbolic.SAT.Representations.CNF
import qualified Data.HashSet                  as HS
import           Data.Hashable
import Logic.Symbolic.SAT.Representations.Clause
import           Data.Either
import           Control.Effects.Commentary
import           Control.Effects.Substitution
import           Control.Lens
import           Polysemy
import Debug.Trace
import           Data.List                      ( partition )
import           Data.Data
import Data.Maybe (isJust)
import           Debug.Trace
import           Logic.Symbolic.SAT.Representations.ConversionRules
import           Data.Foldable                  ( traverse_ )

{-# INLINABLE oneLiteralRule #-}
oneLiteralRule
    :: (Eq v, Hashable v, StandardEffects r, v ~ Variable)
    => CNF v
    -> Sem r (Maybe (CNF v))
oneLiteralRule (CNF cnf) = if HS.null targets || HS.null singulars
    then return Nothing
    else do
        traverse     addSubstitutions definitions -- FIXME need to ensure not already defined
        return (Just result)
  where

    withoutPositives =
        setLiteralsTrue (p' `HS.difference` both) (CNF singulars)
    result =
        setLiteralsFalse (n' `HS.difference` both) withoutPositives
    singles = HS.filter (\c -> 1 == clauseSize c) cnf
    (posis', negis') = partition ((== Top) . snd) (concat lits)
    posis            = fmap (fst) posis'
    negis            = fmap (fst) negis'
    p' = HS.fromList posis
    n' = HS.fromList negis
    both = HS.intersection p' n'
    targets = HS.filter (\clause -> not (clauseContains both clause)) singles
    singulars        = cnf `HS.difference` targets
    singlesList :: [Clause Variable]
    singlesList = HS.toList singles

    lits        = fmap (mapClause literalAtom) singlesList
    definitions = fmap (mapClause assignLiteralVar) singlesList
    literalAtom :: forall m a . Hashable a =>  Literal m a -> (Atom a, Expr a)
    literalAtom (PositiveLiteral a) = (a, Top)
    literalAtom (NegativeLiteral a) = (a, Bottom)
    assignLiteralVar :: forall m a .  Hashable a => Literal m a -> (a, Expr a)
    assignLiteralVar (PositiveLiteral (Variable a)) = (a, Top)
    assignLiteralVar (NegativeLiteral (Variable a)) = (a, Bottom)

{-# INLINABLE positiveNegativeRule #-}
positiveNegativeRule
    :: (Eq v, Hashable v, StandardEffects r, v ~ Variable)
    => CNF v
    -> Sem r (Maybe (CNF v))
positiveNegativeRule cnf = do
    let posis     = positiveLiterals cnf
        negis     = negativeLiterals cnf
        onlyPosis = posis `HS.difference` negis
        onlyNegis = negis `HS.difference` posis
    if (HS.null onlyPosis && HS.null onlyNegis)
        then return Nothing
        else do
            addSubstitutions
                (  (fmap (\(Variable v) -> (v, Top)) $ HS.toList onlyPosis)
                ++ ( fmap (\(Variable v) -> (v, Bottom))
                   $ HS.toList onlyNegis
                   )
                )
            return $ Just
                (removeClausesWith (onlyPosis `HS.union` onlyNegis) cnf)


{-#INLINE removeTautologies#-}
removeTautologies :: (Eq v, Hashable v) => CNF v -> Maybe (CNF v)
removeTautologies (CNF f) =
    let tautologies = HS.filter (isJust . isTautology) f
    in
        if HS.null tautologies
            then Nothing
            else Just . CNF $ HS.difference f tautologies

{-# INLINE trivialCNFSimplifications #-}
trivialCNFSimplifications
    :: (Eq v, Hashable v, StandardEffects r, v ~ Variable)
    => CNF v
    -> Sem r (Maybe (CNF v))
trivialCNFSimplifications =
    combineRewrites [oneLiteralRule, positiveNegativeRule, pure . removeTautologies]


