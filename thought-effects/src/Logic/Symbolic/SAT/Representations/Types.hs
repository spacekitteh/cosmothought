{-@LIQUID "--no-totality" @-}
module Logic.Symbolic.SAT.Representations.Types where
import Data.Kind
import           Data.Unique
import           Polysemy
import           Polysemy.Fresh
import qualified Data.HashMap.Strict           as HM
import           Data.Hashable
import           Data.Data
import           GHC.Generics
import           Data.Bits                      ( xor )
import           Control.Lens.Plated hiding ((...))
import           GDP as GDP
import Data.Discrimination.Grouping
import Data.Functor.Contravariant.Divisible

---------------------------------------------
--- Variables

data Variable = AVariable (Maybe String) Unique
    deriving (Ord, Data, Typeable, Generic)
instance Grouping Unique where
    grouping = hashing
instance Eq Variable where
    {-# INLINE (==) #-}
    (AVariable _ a) == (AVariable _ b) = a == b
instance Grouping Variable where
    grouping = divide (\(AVariable _ u) -> ((), u)) conquer grouping
instance Hashable Variable where
    {-# INLINE hash #-}
    hash (AVariable _ a) = hashUnique a
    {-# INLINE hashWithSalt #-}
    hashWithSalt i (AVariable _ a) = i `xor` (hashUnique a)

instance Show Variable where
    show (AVariable (Just s) _) = s
    show _                      = "An anonymous coward"
{-# INLINE stringVarsToVariables #-}
stringVarsToVariables
    :: (Member (Fresh Variable) r)
    => [String]
    -> Sem r (HM.HashMap String Variable)
stringVarsToVariables strings = do
    assignments <- traverse stringToFresh strings
    return (HM.fromList assignments)
  where
    stringToFresh
        :: (Member (Fresh Variable) r) => String -> Sem r (String, Variable)
    stringToFresh str = do
        (AVariable _ var) <- fresh
        return (str, AVariable (Just str) var)
{-# INLINE runFreshVariable #-}
runFreshVariable :: Sem ((Fresh Variable) : r) a -> Sem ((Fresh Unique) : r) a
runFreshVariable = reinterpret $ \case
    Fresh -> do
        v <- fresh @Unique
        return (AVariable Nothing v)
newtype UnboundVariable = UnboundVariable Variable deriving (Eq, Show)

-------------------------------------------------
--- Atoms
{-@autosize Atom@-}
data Atom v = Variable v
            | Term [Atom v] -- todo sort this out. it will contain multiple v's, but they can't be split up
    deriving stock (Eq, Read, Data, Typeable, Generic, Generic1, Functor, Foldable, Traversable)
instance Grouping v => Grouping (Atom v)
instance Show v => Show (Atom v) where
    show (Variable v) = show v
instance Hashable v => Hashable (Atom v)
instance Data v => Plated (Atom v)




-----------------------------------------------
--- Literals

data Sense = Positive | Negative deriving (Eq, Generic, Data, Typeable)

data Literal (m :: Sense) v where
    PositiveLiteral ::Atom v -> Literal Positive v
    NegativeLiteral ::Atom v -> Literal Negative v

{-@
measure isPositiveLiteral' :: Literal Positive v -> Bool
isPositiveLiteral' (PositiveLiteral a) = True
@-}
{-@
measure isNegativeLiteral' :: Literal Negative v -> Bool
isNegativeLiteral' (NegativeLiteral a) = True
@-}
deriving stock instance (Eq (Atom v)) => Eq (Literal m v)
instance Show (Atom v) => Show (Literal m v) where
    show (PositiveLiteral a) = show a
    show (NegativeLiteral a) = "Not " ++ (show a)--'¬' : show a
deriving stock instance Functor (Literal m)
deriving stock instance Foldable (Literal m)
deriving stock instance Traversable (Literal m)
deriving stock instance Typeable (Literal m v)
{-# INLINE rawLiterals #-}
rawLiterals :: Literal m v -> Atom v
rawLiterals (PositiveLiteral v) = v
rawLiterals (NegativeLiteral v) = v


instance Hashable (Atom v) => Hashable (Literal Positive v) where
    {- instance Hashable (Atom v) => Hashable (Literal Positive v) where
        hashWithSalt :: Int -> a:{x:(Literal Positive v) | isPositiveLiteral' x}
    -> Int @-}
    {-@ignore hashWithSalt @-}
    {-# INLINE hashWithSalt #-}
    hashWithSalt :: Int -> Literal Positive v -> Int
    hashWithSalt i (PositiveLiteral a) = hashWithSalt @(Atom v) i a


instance Hashable v => Hashable (Literal Negative v) where
    {- instance Hashable (Atom v) => Hashable (Literal Negative v) where
            hashWithSalt :: Int ->  a:{x:(Literal Negative v) |
            isNegativeLiteral' x} -> Int @-}
    {-# INLINE hashWithSalt #-}
{-@lazy hashWithSalt @-}
    hashWithSalt i (NegativeLiteral a) = (hashWithSalt @(Atom v)) i a


-------------------------------------------------------------
--- GDP logic

--type Fact p = Given (Proof p)
--using    :: Fact (p n) => (Proof (p n) -> Proof q) -> (a ~~ n) -> (Fact q => t) -> t
--using impl x = give (impl Data.Reflection.given)


newtype Equisatisfiable prop = MkEquisatisfiable prop
type role Equisatisfiable nominal



type Lambda p x = ForAll x p
newtype CAnd a b x = MkCAnd Defn
type role CAnd nominal nominal nominal

andToCAnd :: Proof (And (a x) (b x)) -> Proof (CAnd a b x)
andToCAnd _ = axiom

cAndToAnd :: Proof (CAnd a b x) -> Proof (And (a x) (b x))
cAndToAnd _ = axiom

cAndToAnd' :: forall {k} (a:: k-> Type) (b::k -> Type) (x::k). CAnd a b x -> Proof (And (a x) (b x))
cAndToAnd' _ = axiom

cAndEquiv :: Proof (CAnd a b x == And (a x) (b x))
cAndEquiv = axiom

{-# INLINE fmapP #-}
fmapP :: Functor f
          => (forall name. (a ~~ name ::: p name) -> b)
          -> f (a ?p)
          -> f (b)
fmapP f = fmap (\x -> rename x f)

newtype MemberOf parent child = MkMemberOf Defn
type role MemberOf nominal nominal
{-#INLINE outToIn#-}
outToIn ::  forall p q outer a f. Functor f =>
        (((f a) ~~ outer ::: p outer) -> (a ? MemberOf outer) -> a ? q outer)
            --((f a) ~~ outer ::: p outer -> a ->  a ~~ inner ::: q outer inner)--Proof (Exists inner (q outer inner)))
        ->  (f a) ~~ outer ::: p outer
        ->  f (a ? q outer)
outToIn p f@(The functor) = fmap ((p f) . assert) functor {-fmap (\a -> ((name a $ \a -> unname $ a ... theProof))) (the . exorcise $ f) where
    theProof = p f-}

--foo :: (Fact (p n) => a ~~ n ::: p n -> t) -> a ? p -> t
--foo f p' = rename p' $ \z -> f (exorcise z)

-------------------------------------------------------------
--- Orphan instances
instance Data Unique where
    toConstr = toConstr
    dataTypeOf _ = mkNoRepType "Data.Unique.Unique"
    gunfold = gunfold
