module Logic.Symbolic.SAT.Representations.CNF where
import           Logic.Symbolic.SAT.Representations.Expression
import           Logic.Symbolic.SAT.Representations.Clause
import           Data.Hashable
import qualified Data.HashSet                  as HS
import           Control.Lens
import           Data.Data
import           Data.Foldable                 as DF
import           GHC.Generics
import           Logic.Symbolic.SAT.Representations.Types
import           Polysemy
import           Control.Effects.Commentary
import           Logic.Symbolic.SAT.Representations.ConversionRules

import           GHC.Stack
import           GDP

import Data.Discrimination.Grouping
-----------------------------------------------------------------------------
--- CNF
-----------------------------------------------------------------------------

-- | A representation of an expression in clausal normal form.
newtype CNF v = CNF (HS.HashSet (Clause v)) deriving (Eq, Show, Typeable, Generic)

instance Data v => Data (CNF v)
instance Data v => Plated (CNF v)

instance (Eq v, Hashable v) => Semigroup (CNF v) where
    {-# INLINE (<>) #-}
    (CNF a) <> (CNF b) = CNF (a <> b)

instance (Eq v, Hashable v) => Monoid (CNF v) where
    mempty = emptyCNF

-- TODO: Partial order based on subsumption: For every clause in subsumee, is there a clause in subsumer that subsumes it?

emptyCNF :: CNF v
emptyCNF = CNF (HS.empty)

data CNFExprCase v prop where
    ACNFdLiteral :: (Fact (IsLiteral prop), Fact (IsClause prop), Fact (IsCNF prop)) => CNFExprCase v prop
    ACNFdTop :: (Fact (IsTop prop), Fact (IsTruthValue prop), Fact (IsClause prop), Fact (IsCNF prop)) => CNFExprCase v prop
    ACNFdBottom :: (Fact (IsBottom prop), Fact (IsTruthValue prop), Fact (IsClause prop), Fact (IsCNF prop)) => CNFExprCase v prop
    ACNFdConjunct :: (Fact (IsAnd prop), Fact (IsCNF prop)) =>  [Expr v ? IsClause] -> CNFExprCase v prop
    AnEmptyCNFdClause :: (Fact (IsOr (prop)), Fact (IsDegenerateTruthValue (prop)), Fact (IsClause prop), Fact (IsCNF prop)) => CNFExprCase v prop
    ACNFdClause :: (Fact (IsOr prop), Fact (IsClause prop), Fact (IsCNF prop)) => CNFExprCase v prop

{-# INLINE classifyCNFdExpression #-}
classifyCNFdExpression :: forall v (prop :: *) . ((Expr v) ~~ prop ::: IsCNF prop) -> CNFExprCase v prop
classifyCNFdExpression prop = note (conjure prop) $ case classifyExpression' (exorcise prop) of
    AnAnd -> ACNFdConjunct (fmapP (\p -> unname $ p ...> cnfClauses) (conjuncts (exorcise prop)))
    AnOr | DF.null (disjuncts (exorcise prop)) -> note (axiom :: Proof (IsDegenerateTruthValue prop)) $ note (sorry :: Proof (IsClause prop)) $ note (clauseIsCNF `on` (exorcise prop)) AnEmptyCNFdClause
    AnOr -> note (cnfdOrIsClause `on` (exorcise prop)) $ note (clauseIsCNF `on` (exorcise prop)) ACNFdClause
    ATop -> note (truthValueIsClause `on` (exorcise prop)) $ note (truthValueIsCNF `on` (exorcise prop)) $ ACNFdTop
    ABottom -> note (truthValueIsClause `on` (exorcise prop)) $ note (truthValueIsCNF `on` (exorcise prop)) $ ACNFdBottom
    _ | isLiteral (the prop) -> note (sorry :: Proof (IsLiteral prop)) $ note (literalIsClause `on` (exorcise prop)) $ note (clauseIsCNF `on` (exorcise prop)) ACNFdLiteral

    _ -> error "Exhaustiveness checking"


{-# INLINE exprToCNF #-}
exprToCNF :: (HasCallStack, Eq v, Hashable v, Grouping v, Data v, Show v) => Expr v -> CNF v
exprToCNF prop = run $ ignoreCommentary $ do
    cnfd' <- toCNF prop
    rename cnfd' $ \cnfd ->
        case classifyCNFdExpression cnfd of
            ACNFdConjunct clauses -> return . CNF . HS.fromList $ fmap makeClause' clauses
            ACNFdClause -> return . CNF . HS.singleton . makeClause . exorcise $ cnfd
            AnEmptyCNFdClause -> return . CNF . HS.singleton $ emptyClause
            ACNFdLiteral -> return . CNF . HS.singleton . makeClause . exorcise $ cnfd
            ACNFdTop -> return . CNF $ HS.empty
            ACNFdBottom -> return . CNF . HS.singleton $ emptyClause

{-# INLINE cnfToExpr #-}
cnfToExpr :: (Eq v, Hashable v, Data v) => CNF v -> (Expr v) ? IsCNF
cnfToExpr (CNF c) | c == HS.empty        = assert Top
cnfToExpr (CNF c) | emptyClause `elem` c = assert Bottom
cnfToExpr (CNF cnf) =
    assert
        -- $ run
        -- $ ignoreCommentary
        -- $ rewriteM trivialSimplificationRules
        $ And
        $ fmap (the . fromClause) (HS.toList cnf)

{-# INLINE cnf #-}
cnf :: (Eq v, Hashable v, Data v, Show v, Grouping v) => Iso (Expr v) (Expr v ? IsCNF) (CNF v) (CNF v)
cnf = iso exprToCNF cnfToExpr

{-# INLINE cnf' #-}
cnf' :: (Eq v, Hashable v, Data v, Show v, Grouping v) => Iso' (Expr v) (CNF v)
cnf' = iso exprToCNF (the . cnfToExpr)

{-# INLINABLE positiveLiterals #-}
positiveLiterals :: (Eq v, Hashable v) => CNF v -> HS.HashSet (Atom v)
positiveLiterals (CNF clauses) = foldr HS.union HS.empty
    $ fmap positiveLiteralsFromClause (clauses ^.. folded)

{-# INLINABLE negativeLiterals #-}
negativeLiterals :: (Eq v, Hashable v) => CNF v -> HS.HashSet (Atom v)
negativeLiterals (CNF clauses) = foldr HS.union HS.empty
    $ fmap negativeLiteralsFromClause (clauses ^.. folded)

{-# INLINABLE removeClausesWith #-}
removeClausesWith :: (Eq v, Hashable v) => HS.HashSet (Atom v) -> CNF v -> CNF v
removeClausesWith h (CNF a) = CNF (HS.filter (not . (clauseContains h)) a)

{-# INLINABLE removeClausesWithPositive #-}
removeClausesWithPositive :: (Eq v, Hashable v) => HS.HashSet (Atom v) -> CNF v -> CNF v
removeClausesWithPositive h (CNF a) =
    CNF (HS.filter (not . (clauseContainsPositive h)) a)

{-# INLINABLE removeClausesWithNegative #-}
removeClausesWithNegative :: (Eq v, Hashable v) => HS.HashSet (Atom v) -> CNF v -> CNF v
removeClausesWithNegative h (CNF a) =
    CNF (HS.filter (not . (clauseContainsNegative h)) a)

{-# INLINE setLiteralsTrue #-}
setLiteralsTrue :: (Eq v, Hashable v) => HS.HashSet (Atom v) -> CNF v -> CNF v
setLiteralsTrue h c = CNF result  where
    (CNF withoutPositives) = removeClausesWithPositive h c
    result                 = HS.map (removeFromNegative h) withoutPositives

{-# INLINE setLiteralsFalse #-}
setLiteralsFalse :: (Eq v, Hashable v) => HS.HashSet (Atom v) -> CNF v -> CNF v
setLiteralsFalse h c = CNF result  where
    (CNF withoutNegatives) = removeClausesWithNegative h c
    result                 = HS.map (removeFromPositive h) withoutNegatives


------------------------------------------------------------------
--- CNF proofs
------------------------------------------------------------------

cnfDefinition ::
    Proof ((IsCNF prop)
        == IsClause prop
        ∨ (IsAnd prop ∧ ForAll child (IsConjunctOf prop child ∧ IsClause child)))
cnfDefinition = axiom

cnfDefinition' :: Proof (IsCNF prop) -> Proof (IsClause prop
        ∨ (IsAnd prop ∧ ForAll child (IsConjunctOf prop child ∧ IsClause child)))
cnfDefinition' _ = sorry -- TODO: Figure out Theory.Equality machinery

{-#INLINE truthValueIsCNF#-}
truthValueIsCNF :: Proof (IsTruthValue prop) -> Proof (IsCNF prop)
truthValueIsCNF = clauseIsCNF . truthValueIsClause

{-#INLINE literalIsCNF#-}
literalIsCNF :: Proof (IsLiteral prop) -> Proof (IsCNF prop)
literalIsCNF = clauseIsCNF . literalIsClause

{-#INLINE clauseIsCNF#-}
clauseIsCNF :: Proof (IsClause prop) -> Proof (IsCNF prop)
clauseIsCNF _ = axiom

{-# INLINE cnfdOrIsClause #-}
cnfdOrIsClause :: forall prop. Fact (IsOr prop) => Proof (IsCNF prop) -> Proof (IsClause prop)
cnfdOrIsClause c = elimOrL id (cnfDefinition' c) (\ isand -> absurd . contradicts (known @(IsOr prop)) $ contrapositive $ \p -> distinctOrAnd p ( elimAndL isand) ) -- TODO: Create injectivity axioms

{-#INLINE conjunctOfClausesIsCNF#-}
conjunctOfClausesIsCNF :: Proof (IsAnd prop)
    -> Proof (ForAll child (IsClause child ∧ IsConjunctOf prop child))
    -> Proof (IsCNF prop)
conjunctOfClausesIsCNF _ _ = sorry -- TODO: use cnfDefinition

{-#INLINE cnfClauses#-}
cnfClauses :: Fact (IsCNF prop) => IsConjunctOf prop child -> Proof (IsClause child)
cnfClauses _ = sorry