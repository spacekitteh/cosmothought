module Logic.Symbolic.SAT.Representations.Clause
  ( IsClause,
    Clause,
    clauseDefn,
    removeFromPositive,
    removeFromNegative,
    clauseContains,
    clauseContainsPositive,
    literalIsClause,
    clauseContainsNegative,
    emptyClause,
    clauseSize,
    clauseAtoms,
    mapClause,
    makeClause,
    fromClause,
    positiveLiteralsFromClause,
    negativeLiteralsFromClause,
    truthValueIsClause,
    makeClause',
    _AClause',
    isTautology,
  )
where

import Algebra.PartialOrd
import Control.Effects.Commentary
import Control.Lens hiding ((...))
import Data.Data
import Data.Foldable as DF
import qualified Data.HashSet as HS
import Data.HashSet.Lens
import Data.Hashable
import Data.List
  ( intercalate,
    partition,
  )
import Data.Maybe
import Data.Monoid
import Debug.Trace
import GDP
import GHC.Generics
import GHC.Stack
import Logic.Symbolic.SAT.Representations.ConversionRules
import Logic.Symbolic.SAT.Representations.Expression
import Logic.Symbolic.SAT.Representations.Types
import Numeric.Natural
import Polysemy

-------------------------------------------------------------
--- ClauseSize
-------------------------------------------------------------

-- | A datatype for keeping track of the size of a clause.
data ClauseSize = MkClauseSize {_totalSize :: !Natural, _positives :: !Natural, _negatives :: !Natural} deriving (Eq, Generic, Show, Ord, Typeable)

{-# INLINE makeClauseSize #-}
makeClauseSize :: Natural -> Natural -> ClauseSize
makeClauseSize p n = MkClauseSize (p + n) p n

zeroClauseSize :: ClauseSize
zeroClauseSize = makeClauseSize 0 0

instance Semigroup ClauseSize where
  {-# INLINE (<>) #-}
  (MkClauseSize t1 p1 n1) <> (MkClauseSize t2 p2 n2) =
    MkClauseSize (t1 + t2) (p1 + p2) (n1 + n2)

instance Monoid ClauseSize where
  mempty = zeroClauseSize

instance Hashable ClauseSize

instance PartialOrd ClauseSize where
  {-# INLINE leq #-}
  (MkClauseSize t1 p1 n1) `leq` (MkClauseSize t2 p2 n2) = t1 <= t2 && p1 <= p2 && n1 <= n2

-------------------------------------------------------------
--- Clauses
-------------------------------------------------------------

-- | A representation of an expression in clausal form.
data Clause v
  = -- UnitClause v Sense
    MkClause ClauseSize (HS.HashSet (Literal Positive v)) (HS.HashSet (Literal Negative v))
  deriving stock
    ( Eq,
      Typeable,
      Generic
    )

instance (Data v) => Data (Clause v)

instance (Data v) => Plated (Clause v)

instance (Show v) => Show (Clause v) where
  show (MkClause _ posis negis) = intercalate " ∨ " combined
    where
      combined =
        (fmap show $ HS.toList posis) ++ (fmap show $ HS.toList negis)

instance (Eq v, Hashable v) => PartialOrd (Clause v) where
  {-# INLINE leq #-}
  a `leq` b = (name2 a b $ \a' b' -> isJust $ subsumes a' b')

instance (Hashable v) => Hashable (Clause v)

instance (Eq v, Hashable v) => Semigroup (Clause v) where
  {-# INLINE (<>) #-}
  (MkClause s1 p1 n1) <> (MkClause s2 p2 n2) = MkClause (s1 <> s2) (p1 <> p2) (n1 <> n2)

instance (Eq v, Hashable v) => Monoid (Clause v) where
  mempty = emptyClause

---------------------
--- Clause operations

{-# INLINE newClause #-}
newClause :: HS.HashSet (Literal Positive v) -> HS.HashSet (Literal Negative v) -> Clause v
newClause p n = newClauseSized (fromIntegral $ HS.size p) (fromIntegral $ HS.size n) p n

{-# INLINE newClauseSized #-}
newClauseSized :: Natural -> Natural -> HS.HashSet (Literal Positive v) -> HS.HashSet (Literal Negative v) -> Clause v
newClauseSized ps ns p n = MkClause (makeClauseSize ps ns) p n

{-# INLINE clauseSize #-}
clauseSize :: Clause v -> Natural
clauseSize (MkClause (MkClauseSize s _ _) _ _) = s

{-# INLINE removeFromPositive #-}
removeFromPositive :: (Eq v, Hashable v) => HS.HashSet (Atom v) -> Clause v -> Clause v
removeFromPositive h (MkClause _ p n) =
  newClause (HS.difference p (HS.map PositiveLiteral h)) n

{-# INLINE removeFromNegative #-}
removeFromNegative :: (Eq v, Hashable v) => HS.HashSet (Atom v) -> Clause v -> Clause v
removeFromNegative h (MkClause _ p n) =
  newClause p (HS.difference n (HS.map NegativeLiteral h))

{-# INLINEABLE clauseContains #-}
clauseContains :: (Eq v, Hashable v) => HS.HashSet (Atom v) -> Clause v -> Bool
clauseContains vars (MkClause _ posis negis) =
  not $
    HS.null $
      (HS.union (HS.map rawLiterals posis) (HS.map rawLiterals negis))
        `HS.intersection` vars

{-# INLINEABLE clauseContainsPositive #-}
clauseContainsPositive :: (Eq v, Hashable v) => HS.HashSet (Atom v) -> Clause v -> Bool
clauseContainsPositive vars (MkClause _ posis _) =
  not $ HS.null $ (HS.map rawLiterals posis) `HS.intersection` vars

{-# INLINEABLE clauseContainsNegative #-}
clauseContainsNegative :: (Eq v, Hashable v) => HS.HashSet (Atom v) -> Clause v -> Bool
clauseContainsNegative vars (MkClause _ _ negis) =
  not $ HS.null $ (HS.map rawLiterals negis) `HS.intersection` vars

emptyClause :: Clause v
emptyClause = MkClause zeroClauseSize HS.empty HS.empty

{-# INLINE isTautology #-}
isTautology :: (Eq v, Hashable v) => Clause v -> Maybe (HS.HashSet (Atom v))
isTautology (MkClause _ p n) =
  let overlapping = HS.intersection (HS.map rawLiterals p) (HS.map rawLiterals n)
   in if HS.null overlapping
        then Nothing
        else Just overlapping

{-# INLINEABLE clauseAtoms #-}
clauseAtoms :: (Eq v, Hashable v) => Fold (Clause v) v
clauseAtoms = (Control.Lens.to (the . fromClause)) . variables

{-# INLINEABLE makeClause #-}

-- | Construct a 'Clause' from a given 'Expr' when we know that the 'Expr' is indeed a clause.
makeClause :: forall v (prop :: *). (Eq v, Hashable v, Fact (IsClause prop)) => (Expr v) ~~ prop -> Clause v
makeClause prop = case classifyExpression' prop of
  AnOr -> newClause pos neg
    where
      (p, n) = partitionLiterals . clauseLiterals $ prop
      plist =
        fmap
          ( \p' -> rename p' $ \z ->
              note (conjure z) $ positiveLiteral (exorcise z)
          )
          p
      nlist =
        fmap
          ( \n' -> rename n' $ \z ->
              note (conjure z) $ negativeLiteral (exorcise z)
          )
          n
      pos = HS.fromList plist
      neg = HS.fromList nlist
  AnAtom -> newClauseSized 1 0 (HS.singleton (positiveLiteral prop)) HS.empty
  ANot | isNegativeLiteral (the prop) -> newClauseSized 0 1 HS.empty (HS.singleton (note (isClauseAndIsNotImpliesNegativeLiteral `on` prop) $ (negativeLiteral prop)))
  --    AnImplication ->
  a -> error (show a)

{-# INLINE makeClause' #-}
makeClause' :: (Eq v, Hashable v) => Expr v ? IsClause -> Clause v
makeClause' prop = rename prop $ \p' -> note (conjure p') (makeClause (exorcise p'))

{-# INLINE clauseDisjunctLiterals #-}
clauseDisjunctLiterals :: (Fact (IsClause prop), Fact (IsOr prop)) => (Expr v) ~~ prop -> [Expr v ? (CAnd IsLiteral (IsDisjunctOf prop))]
clauseDisjunctLiterals prop@(The (Or os)) = fmapP (\x -> unname (x ...> (\p -> sorry))) (disjuncts prop)

{-# INLINE clauseLiterals #-}
clauseLiterals :: forall prop child v. (Fact (IsClause prop), Fact (IsOr prop)) => (Expr v) ~~ prop -> [Expr v ? IsLiteral]
clauseLiterals prop = fmap conv (clauseDisjunctLiterals prop)
  where
    conv :: Expr v ? (CAnd IsLiteral (IsDisjunctOf prop)) -> Expr v ? IsLiteral
    conv o' = rename o' $ \o ->
      unname $ o ...> (elimAndL . cAndToAnd' @IsLiteral @(IsDisjunctOf prop))

{-# INLINE literalsToExpr #-}
literalsToExpr :: [(Expr v) ? IsLiteral] -> (Expr v) ? IsClause
literalsToExpr = assert . Or . fmap the

{-# INLINEABLE fromClause #-}
fromClause :: Clause v -> (Expr v) ? IsClause
fromClause = literalsToExpr . (mapClause toAtomic)

{-# INLINEABLE _AClause' #-}
_AClause' :: (Eq v, Hashable v) => Iso' ((Expr v) ? IsClause) (Clause v)
_AClause' = iso makeClause' fromClause

-- TODO: Create this prism
-- {- INLINABLE _AClause -}
-- _AClause :: (Eq v, Hashable v) => Prism (Expr v) ((Expr v) ? IsClause) (Clause v) (Clause v)
-- _AClause = prism makeClause fromClause

{-# INLINE subsumes #-}
subsumes ::
  (Eq v, Hashable v) =>
  (Clause v) ~~ subsumer ->
  (Clause v) ~~ subsumee ->
  Maybe ((Clause v) ~~ subsumer ::: Subsumes subsumee subsumer)
subsumes (The (MkClause subsumer _ _)) (The (MkClause subsumee _ _))
  | not (subsumer `leq` subsumee) = Nothing
subsumes subsumer@(The (MkClause _ pos neg)) (The (MkClause _ pos' neg')) =
  if HS.null (HS.difference pos pos') && HS.null (HS.difference neg neg')
    then Just (subsumer ... axiom)
    else Nothing

{-# INLINEABLE mapClause #-}
mapClause :: (forall m. Literal m v -> a) -> Clause v -> [a]
mapClause f (MkClause _ p n) = (fmap f (HS.toList p)) ++ (fmap f (HS.toList n))

{-# INLINE positiveLiteralsFromClause #-}
positiveLiteralsFromClause :: (Eq v, Hashable v) => Clause v -> HS.HashSet (Atom v)
positiveLiteralsFromClause (MkClause _ p _) = HS.map rawLiterals p

{-# INLINE negativeLiteralsFromClause #-}
negativeLiteralsFromClause :: (Eq v, Hashable v) => Clause v -> HS.HashSet (Atom v)
negativeLiteralsFromClause (MkClause _ _ n) = HS.map rawLiterals n

{-# INLINE clauseSemantics #-}
clauseSemantics :: (Members [Valuation, Commentary] r) => Clause Variable -> Sem r (Prop ? IsClause)
clauseSemantics a | a == emptyClause = return (assert Bottom)
clauseSemantics a = do
  let converted = fromClause a
  simplified <- trivialSimplifications (the converted)
  return (assert simplified)

---------------------------------------------------------
--- Clause proofs
---------------------------------------------------------
newtype IsClause prop = MkIsClause Defn

type role IsClause nominal

newtype Subsumes subsumee subsumer = MkSubsumes Defn

newtype PositiveLiteralCount num clause = MkIsPositiveLiteralCount Defn

newtype NegativeLiteralCount num clause = MkIsNegativeLiteralCount Defn

newtype IsHornClause clause = MkIsHornClause Defn

newtype IsDefiniteHornClause clause = MkIsDefiniteHornClause Defn

clauseDefn ::
  Proof
    ( (IsClause prop)
        == (IsTruthValue prop)
        ∨ (IsLiteral prop)
        ∨ ( IsOr prop ∧ (ForAll child (IsLiteral child ∧ (IsDisjunctOf prop child)))
          )
    )
clauseDefn = axiom

{-# INLINE clauseDisjunctsAreLiteralsLemma #-}
clauseDisjunctsAreLiteralsLemma :: Proof (IsClause prop ∧ IsDisjunctOf prop child) -> Proof (IsLiteral child)
clauseDisjunctsAreLiteralsLemma p = sorry

{-# INLINE clauseDisjunctsAreLiteralsLemma' #-}
clauseDisjunctsAreLiteralsLemma' ::
  Proof (IsClause prop) ->
  Proof (IsDisjunctOf prop child) ->
  Proof (IsLiteral child)
clauseDisjunctsAreLiteralsLemma' p _ = sorry

{-# INLINE literalIsClause #-}
literalIsClause :: Proof (IsLiteral prop) -> Proof (IsClause prop)
literalIsClause _ = sorry -- TODO: Use clauseDefn

{-# INLINE isClauseAndIsNotImpliesNegativeLiteral #-}
isClauseAndIsNotImpliesNegativeLiteral :: Fact (IsClause prop) => Proof (IsNot prop) -> Proof (IsNegativeLiteral prop)
isClauseAndIsNotImpliesNegativeLiteral _ = sorry

{-# INLINE posLitIsClause #-}
posLitIsClause :: Proof (IsPositiveLiteral prop) -> Proof (IsClause prop)
posLitIsClause = literalIsClause . positiveLitIsLit

{-# INLINE negLitIsClause #-}
negLitIsClause :: Proof (IsNegativeLiteral prop) -> Proof (IsClause prop)
negLitIsClause = literalIsClause . negativeLitIsLit

{-# INLINE orClause #-}
orClause ::
  Proof (IsOr prop) ->
  Proof (ForAll child (IsLiteral child ∧ (IsDisjunctOf prop child))) ->
  Proof (IsClause prop)
orClause _ _ = sorry

{-# INLINE truthValueIsClause #-}
truthValueIsClause :: Proof (IsTruthValue prop) -> Proof (IsClause prop)
truthValueIsClause _ = sorry

-- hornClauseDefinition :: Proof (
--    IsHornClause clause === (Exists num (num <= 1))
-- )

{-# INLINE hornClauseDefinition #-}
hornClauseDefinition ::
  Fact (IsHornClause clause) =>
  (Clause v) ~~ clause ->
  Either
    (Proof (PositiveLiteralCount 0 clause))
    (Proof (PositiveLiteralCount 1 clause))
hornClauseDefinition clause =
  if (clauseSize (the clause) == 0) then Left (sorry) else Right (sorry)

{-# INLINE definiteHornClauseDefinition #-}
definiteHornClauseDefinition ::
  Fact (IsDefiniteHornClause clause) =>
  (Clause v) ~~ clause ->
  Proof (PositiveLiteralCount 1 clause)
definiteHornClauseDefinition _ = sorry

{-# INLINE defHornIsHorn #-}
defHornIsHorn :: Proof (IsDefiniteHornClause clause) -> Proof (IsHornClause clause)
defHornIsHorn _ = sorry
