module Logic.Symbolic.Symbol where

import           Data.Unique
import qualified Data.Text                     as T


data SymbolType = Variable | Identifier | AnInt | ADouble | Textual

data SymbolData (ty :: SymbolType) where
    VariableData :: T.Text -> SymbolData Variable
    IdentifierData :: T.Text -> SymbolData Identifier
    IntData :: Int -> SymbolData AnInt
    DoubleData :: Double -> SymbolData ADouble
    TextData :: T.Text -> SymbolData Textual


data Symbol (ty :: SymbolType) = Foo Unique (SymbolData ty)

