module Ontology.Atom where

import qualified Data.Text                     as T

class Atom a where
    name :: a -> T.Text

data AnAtom where
    AnAtom ::Atom a => a -> AnAtom
class Atom a => Node a where

class Atom a => Link a where
    outgoing :: a -> [AnAtom]
