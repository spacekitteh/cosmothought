module Language.SMTLib2.Syntax.Theory where

import Data.Data
import Data.Hashable
import Data.List.NonEmpty (NonEmpty)
import Data.Text (Text)
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Attribute
import Language.SMTLib2.Syntax.Constants
import Language.SMTLib2.Syntax.Identifier
import Language.SMTLib2.Syntax.Keyword
import Language.SMTLib2.Syntax.Sort
import Language.SMTLib2.Syntax.Symbol
import Prettyprinter
  ( Pretty,
    pretty,
    (<+>),
  )
import qualified Prettyprinter as PP
import Text.Megaparsec

data SortSymbolDec
  = SortSymbolDec Identifier Numeral [Attribute]
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty SortSymbolDec where
  pretty (SortSymbolDec ident n attrs) =
    prettyPrintSList
      [pretty ident, pretty n, PP.fillSep . (fmap pretty) $ attrs]

instance Show SortSymbolDec where
  showsPrec _ = showsPrecDefault

parseSortSymbolDec :: Parser SortSymbolDec
parseSortSymbolDec =
  parens
    ( ( SortSymbolDec <$> parseIdentifier <*> parseNumeral <*> many parseAttribute
      )
        <?> "sort symbol declaration"
    )

data FunSymbolDec
  = TheoryConst SpecConstant Sort [Attribute]
  | MetaSpecTheoryConst MetaSpecConst Sort [Attribute]
  | TheoryIdentifier Identifier (NonEmpty Sort) [Attribute]
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty FunSymbolDec where
  pretty (TheoryConst spec sort attrs) =
    prettyPrintSList $ ((pretty spec) : (pretty sort) : (fmap pretty attrs))
  pretty (MetaSpecTheoryConst spec sort attrs) =
    prettyPrintSList $ ((pretty spec) : (pretty sort) : (fmap pretty attrs))
  pretty (TheoryIdentifier ident sorts attrs) =
    prettyPrintSList $
      (pretty ident)
        : ((prettyPrintNonEmpty sorts) <> (fmap pretty attrs))

instance Show FunSymbolDec where
  showsPrec _ = showsPrecDefault

parseFunSymbolDec :: Parser FunSymbolDec
parseFunSymbolDec =
  parens $
    TheoryConst
      <$> parseSpecConstant
      <*> parseSort
      <*> many parseAttribute
      <|> MetaSpecTheoryConst
        <$> parseMetaSpecConst
        <*> parseSort
        <*> many parseAttribute
      <|> TheoryIdentifier
        <$> parseIdentifier
        <*> parseNonEmpty parseSort
        <*> many parseAttribute

data ParFunSymbolDec
  = MonomorphicFunDec FunSymbolDec
  | ParFunDec (NonEmpty Symbol) Identifier (NonEmpty Sort) [Attribute]
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty ParFunSymbolDec where
  pretty (MonomorphicFunDec fun) = pretty fun
  pretty (ParFunDec syms ident sorts attrs) =
    prettyPrintSList'
      "par"
      [ prettyPrintSList (prettyPrintNonEmpty syms),
        prettyPrintSList
          ( (pretty ident)
              : ((prettyPrintNonEmpty sorts) <> (fmap pretty attrs))
          )
      ]

instance Show ParFunSymbolDec where
  showsPrec _ = showsPrecDefault

parseParFunSymbolDec :: Parser ParFunSymbolDec
parseParFunSymbolDec =
  ( parens $ do
      _ <- verbatim "par"
      syms <- parseNonEmpty parseSymbol
      (ident, sorts, attrs) <-
        parens $
          (,,)
            <$> parseIdentifier
            <*> parseNonEmpty parseSort
            <*> many parseAttribute
      pure (ParFunDec syms ident sorts attrs)
  )
    <|> MonomorphicFunDec
      <$> parseFunSymbolDec

data TheoryAttribute
  = TheorySorts (NonEmpty SortSymbolDec)
  | TheoryFuns (NonEmpty ParFunSymbolDec)
  | SortsDescription Text
  | FunsDescription Text
  | TheoryDefinition Text
  | TheoryValues Text
  | TheoryNotes Text
  | CustomTheoryAttribute Attribute
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty TheoryAttribute where
  pretty (TheorySorts sorts) =
    pp ":sorts" <+> prettyPrintSList (prettyPrintNonEmpty sorts)
  pretty (TheoryFuns funs) =
    pp ":funs" <+> prettyPrintSList (prettyPrintNonEmpty funs)
  pretty (SortsDescription t) =
    pp ":sorts-description" <+> PP.dquotes (pretty t)
  pretty (FunsDescription t) =
    pp ":funs-description" <+> PP.dquotes (pretty t)
  pretty (TheoryDefinition t) = pp ":definition" <+> PP.dquotes (pretty t)
  pretty (TheoryValues t) = pp ":values" <+> PP.dquotes (pretty t)
  pretty (TheoryNotes t) = pp ":notes" <+> PP.dquotes (pretty t)
  pretty (CustomTheoryAttribute a) = pretty a

instance Show TheoryAttribute where
  showsPrec _ = showsPrecDefault

parseTheoryAttribute :: Parser TheoryAttribute
parseTheoryAttribute =
  ( do
      kw <- parseKeyword
      case kw of
        "sorts-description" -> SortsDescription <$> parseString
        "sorts" ->
          TheorySorts <$> (parens $ parseNonEmpty parseSortSymbolDec)
        "funs-description" -> FunsDescription <$> parseString
        "funs" ->
          TheoryFuns <$> (parens $ parseNonEmpty parseParFunSymbolDec)
        "definition" -> TheoryDefinition <$> parseString
        "values" -> TheoryValues <$> parseString
        "notes" -> TheoryNotes <$> parseString
        _ -> empty
  )
    <|> CustomTheoryAttribute
      <$> parseAttribute

data TheoryDec
  = TheoryDec Symbol (NonEmpty TheoryAttribute)
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty TheoryDec where
  pretty (TheoryDec sym attrs) =
    prettyPrintSList' "theory" ((pretty sym) : prettyPrintNonEmpty attrs)

instance Show TheoryDec where
  showsPrec _ = showsPrecDefault

parseTheoryDec :: Parser TheoryDec
parseTheoryDec =
  parens $
    TheoryDec
      <$ verbatim "theory"
      <*> parseSymbol
      <*> parseNonEmpty parseTheoryAttribute
