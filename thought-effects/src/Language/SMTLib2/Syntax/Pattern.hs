module Language.SMTLib2.Syntax.Pattern where

import Data.Data
import Data.Hashable
import Data.List.NonEmpty (NonEmpty (..))
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Symbol
import Prettyprinter
  ( Pretty,
    pretty,
  )
import Text.Megaparsec

newtype Pattern = Pattern (NonEmpty Symbol)
  deriving newtype (Eq, Ord, Generic, Hashable, Typeable)
  deriving (Data)

instance Pretty Pattern where
  pretty (Pattern syms) = prettyPrintSList . prettyPrintNonEmpty $ syms

instance Show Pattern where
  showsPrec _ = showsPrecDefault

parsePattern :: Parser Pattern
parsePattern =
  Pattern
    <$> ( (parens $ parseNonEmpty parseSymbol) <|> do
            sym <- parseSymbol
            pure (sym :| [])
        )
