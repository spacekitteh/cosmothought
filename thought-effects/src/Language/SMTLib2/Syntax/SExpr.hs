module Language.SMTLib2.Syntax.SExpr where

import Data.Data
import Data.Hashable
import Data.List.NonEmpty as NE
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Constants
import Language.SMTLib2.Syntax.Keyword
import Language.SMTLib2.Syntax.Symbol
import Prettyprinter
  ( Pretty,
    pretty,
  )
import Text.Megaparsec

data SExpr
  = SymbolExpr Symbol
  | ConstantExpr SpecConstant
  | SubExpressions (NonEmpty SExpr)
  | KeywordExpr Keyword
  | EmptyExpr
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty SExpr where
  pretty (SymbolExpr s) = pretty s
  pretty (ConstantExpr c) = pretty c
  pretty (KeywordExpr kw) = pretty kw
  pretty EmptyExpr = pp "()"
  pretty (SubExpressions exprs) =
    prettyPrintSList . prettyPrintNonEmpty $ exprs

instance Show SExpr where
  showsPrec _ = showsPrecDefault

parseSExprApplication :: Parser (NonEmpty SExpr)
parseSExprApplication = lexeme $ parseNonEmpty (parens parseSExpr)

parseSExpr :: Parser SExpr
parseSExpr =
  lexeme $
    SubExpressions
      <$> try parseSExprApplication
      <|> KeywordExpr
        <$> try parseKeyword
      <|> SymbolExpr
        <$> try parseSymbol
      <|> ConstantExpr
        <$> try parseSpecConstant
      <|> EmptyExpr
        <$ parseEmptyParens

class IsSExpr s where
  toSExpr :: s -> SExpr

instance IsSExpr SExpr where
  toSExpr = id

instance IsSExpr Symbol where
  toSExpr = SymbolExpr

instance IsSExpr Keyword where
  toSExpr = KeywordExpr

instance IsSExpr SpecConstant where
  toSExpr = ConstantExpr
