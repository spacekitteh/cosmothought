module Language.SMTLib2.Syntax.AST where

import Data.Data
import Data.Hashable
import Data.List.NonEmpty as NE
  ( NonEmpty,
    unzip,
  )
import qualified Data.SemVer as SemVer
import Data.Text (Text)
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Attribute
import Language.SMTLib2.Syntax.Command
import Language.SMTLib2.Syntax.Constants
import Language.SMTLib2.Syntax.Declaration
import Language.SMTLib2.Syntax.Definition
import Language.SMTLib2.Syntax.SExpr
import Language.SMTLib2.Syntax.Symbol
import Language.SMTLib2.Syntax.Term
import Prettyprinter
  ( Pretty,
    pretty,
    (<+>),
  )
import qualified Prettyprinter as PP
import Text.Megaparsec

type SMTLib2Script = [SMTCommand]

instance {-# OVERLAPS #-} Pretty SMTLib2Script where
  pretty s = PP.vsep (fmap pretty s)

instance {-# OVERLAPS #-} Show SMTLib2Script where
  showsPrec _ = showsPrecDefault

parseSMTLib2Script :: Parser SMTLib2Script
parseSMTLib2Script =
  -- FIXME: Fails to parse last line of input if it ends with a line comment
  (some (skipManyTill sc parseSMTCommand) <?> "script") <* eof

data ErrorBehaviour
  = ImmediateExit
  | ContinuedExecution
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable, Enum)

instance Pretty ErrorBehaviour where
  pretty ImmediateExit = pp "immediate-exit"
  pretty ContinuedExecution = pp "continued-execution"

instance Show ErrorBehaviour where
  showsPrec _ = showsPrecDefault

data ReasonUnknown
  = OutOfMemory
  | Incomplete
  | OtherReason SExpr
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty ReasonUnknown where
  pretty OutOfMemory = pp "memout"
  pretty Incomplete = pp "incomplete"
  pretty (OtherReason r) = pretty r

instance Show ReasonUnknown where
  showsPrec _ = showsPrecDefault

data ModelResponse
  = DefineFunResponse FunctionDef
  | DefineFunRecResponse FunctionDef
  | DefineFunsRecResponse (NonEmpty (FunctionDec, Term))
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty ModelResponse where
  pretty (DefineFunResponse fun) =
    prettyPrintSList' "define-fun" [pretty fun]
  pretty (DefineFunRecResponse fun) =
    prettyPrintSList' "define-fun-rec" [pretty fun]
  pretty (DefineFunsRecResponse funs) =
    let (decs, terms) = NE.unzip funs
     in prettyPrintSList'
          "define-funs-rec"
          [ prettyPrintSList (prettyPrintNonEmpty decs),
            prettyPrintSList (prettyPrintNonEmpty terms)
          ]

instance Show ModelResponse where
  showsPrec _ = showsPrecDefault

data InfoResponse
  = AssertionStackLevelsResponse Numeral
  | AuthorsResponse Text
  | ErrorBehaviourResponse ErrorBehaviour
  | NameResponse Text
  | ReasonUnknownResponse ReasonUnknown
  | VersionResponse SemVer.Version
  | AttributeResponse Attribute
  deriving (Eq, Ord, Generic, Hashable, Typeable)

instance Pretty InfoResponse where
  pretty (AssertionStackLevelsResponse n) =
    pp ":assertion-stack-levels" <+> pretty n
  pretty (AuthorsResponse t) = pp ":authors" <+> PP.dquotes (pretty t)
  pretty (ErrorBehaviourResponse r) = pp ":error-behavior" <+> pretty r -- yankee go home
  pretty (NameResponse t) = pp ":name" <+> PP.dquotes (pretty t)
  pretty (ReasonUnknownResponse r) = pp ":reason-unknown" <+> pretty r
  pretty (VersionResponse v) =
    pp ":version" <+> PP.dquotes (pretty (SemVer.toText v))
  pretty (AttributeResponse r) = pretty r

instance Show InfoResponse where
  showsPrec _ = showsPrecDefault

data ValuationPair
  = Valuation Term Term
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty ValuationPair where
  pretty (Valuation a b) = PP.parens $ pretty a <+> pretty b

instance Show ValuationPair where
  showsPrec _ = showsPrecDefault

data TValuationPair
  = TValuation Symbol Bool
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty TValuationPair where
  pretty (TValuation sym b) = PP.parens $ pretty sym <+> prettyBool b

instance Show TValuationPair where
  showsPrec _ = showsPrecDefault

data CheckSatResponse
  = SAT
  | UNSAT
  | Unknown
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty CheckSatResponse where
  pretty SAT = pp "sat"
  pretty UNSAT = pp "unsat"
  pretty Unknown = pp "unknown"

instance Show CheckSatResponse where
  showsPrec _ = showsPrecDefault

type EchoResponse = Text

prettyPrintEchoResponse :: EchoResponse -> PP.Doc ann
prettyPrintEchoResponse = PP.dquotes . pretty

type GetAssertionsResponse = [Term]

type GetAssignmentResponse = NonEmpty TValuationPair

type GetInfoResponse = NonEmpty InfoResponse

type GetModelResponse = [ModelResponse]

type GetOptionResponse = AttributeValue

type GetProofResponse = SExpr

type GetUnsatAssumptionsResponse = [Symbol]

type GetUnsatCoreResponse = [Symbol]

type GetValueResponse = NonEmpty ValuationPair

data SpecificSuccessResponse
  = CheckSatResponse CheckSatResponse
  | EchoResponse EchoResponse
  | GetAssertionsResponse GetAssertionsResponse
  | GetAssignmentResponse GetAssignmentResponse
  | GetInfoResponse GetInfoResponse
  | GetModelResponse GetModelResponse
  | GetOptionResponse GetOptionResponse
  | GetProofResponse GetProofResponse
  | GetUnsatAssumptionsResponse GetUnsatAssumptionsResponse
  | GetUnsatCoreResponse GetUnsatCoreResponse
  | GetValueResponse GetValueResponse
  deriving (Eq, Ord, Generic, Hashable, Typeable)

instance Pretty SpecificSuccessResponse where
  pretty (CheckSatResponse r) = pretty r
  pretty (EchoResponse r) = prettyPrintEchoResponse r
  pretty (GetAssertionsResponse r) = prettyPrintSList (fmap pretty r)
  pretty (GetAssignmentResponse r) =
    prettyPrintSList . prettyPrintNonEmpty $ r
  pretty (GetInfoResponse r) = prettyPrintSList . prettyPrintNonEmpty $ r
  pretty (GetModelResponse r) = prettyPrintSList (fmap pretty r)
  pretty (GetOptionResponse r) = pretty r
  pretty (GetProofResponse r) = pretty r
  pretty (GetUnsatAssumptionsResponse r) = prettyPrintSList (fmap pretty r)
  pretty (GetUnsatCoreResponse r) = prettyPrintSList (fmap pretty r)
  pretty (GetValueResponse r) = prettyPrintSList . prettyPrintNonEmpty $ r

instance Show SpecificSuccessResponse where
  showsPrec _ = showsPrecDefault

data GeneralResponse
  = Success
  | SpecificResponse SpecificSuccessResponse
  | Unsupported
  | Error Text
  deriving (Eq, Ord, Generic, Hashable, Typeable)

instance Pretty GeneralResponse where
  pretty Success = pp "success"
  pretty (SpecificResponse r) = pretty r
  pretty Unsupported = pp "unsupported"
  pretty (Error t) = prettyPrintSList' "error" [pretty t]

instance Show GeneralResponse where
  showsPrec _ = showsPrecDefault
