module Language.SMTLib2.Syntax.ASTUtils where

import Data.Char
import Data.Foldable
import Data.List.NonEmpty as NE
  ( NonEmpty (..),
    length,
    zip,
  )
import Data.Text (Text)
import Data.Void
import Prettyprinter
  ( Pretty,
    pretty,
  )
import qualified Prettyprinter as PP
import qualified Prettyprinter.Render.String as PP
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

type Parser = Parsec Void Text

{-# INLINE pp #-}
pp :: Text -> PP.Doc ann
pp = pretty

{-# INLINE showsPrecDefault #-}
showsPrecDefault :: Pretty a => a -> ShowS
showsPrecDefault =
  PP.renderShowS . PP.layoutSmart PP.defaultLayoutOptions . pretty

{-# INLINE prettyPrintNonEmpty #-}
prettyPrintNonEmpty :: Pretty a => NonEmpty a -> [PP.Doc ann]
prettyPrintNonEmpty a = fmap pretty (toList a)

{-# INLINE prettyPrintSList #-}
prettyPrintSList :: [PP.Doc ann] -> PP.Doc ann
prettyPrintSList [p] = p
prettyPrintSList ps = PP.parens . PP.fillSep $ ps

{-# INLINE prettyPrintSList' #-}
prettyPrintSList' :: Text -> [PP.Doc ann] -> PP.Doc ann
prettyPrintSList' name list = prettyPrintSList $ (pretty name) : list

{-# INLINE parseLineComment #-}
parseLineComment :: Parser ()
parseLineComment = L.skipLineComment ";" <* eol

{-# INLINE sc #-}
sc :: Parser ()
sc = L.space space1 parseLineComment empty

{-# INLINE lexeme #-}
lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

{-# INLINE verbatim #-}
verbatim :: Text -> Parser Text
verbatim = L.symbol sc

{-# INLINE parens #-}
parens :: Parser a -> Parser a
parens = between (verbatim "(") (verbatim ")")

{-# INLINE parseEmptyParens #-}
parseEmptyParens :: Parser ()
parseEmptyParens = parens sc

{-# INLINE parseNonEmpty #-}
parseNonEmpty :: Parser a -> Parser (NonEmpty a)
parseNonEmpty p = (:|) <$> p <*> many p

{-# INLINE parseNonEmptyPairList #-}
parseNonEmptyPairList :: Parser a -> Parser b -> Parser (NonEmpty (a, b))
parseNonEmptyPairList pa pb = do
  as <- parens $ parseNonEmpty pa
  (b : bs) <- parens $ count (NE.length as) pb
  pure (NE.zip as (b :| bs))

parseBool :: Parser Bool
parseBool = True <$ verbatim "true" <|> False <$ verbatim "false"

prettyBool :: Bool -> PP.Doc ann
prettyBool True = pp "true"
prettyBool False = pp "false"

type OutputChannel = Text

prettyOutputChannel :: OutputChannel -> PP.Doc ann
prettyOutputChannel = PP.dquotes . pretty

parseOutputChannel :: Parser OutputChannel
parseOutputChannel = parseString

parseString :: Parser Text
parseString = takeWhileP (Just "string token") isPrint
