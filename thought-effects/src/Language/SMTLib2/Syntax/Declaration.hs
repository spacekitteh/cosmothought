module Language.SMTLib2.Syntax.Declaration where

import Data.Data
import Data.Hashable
import Data.List.NonEmpty (NonEmpty)
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Constants
import Language.SMTLib2.Syntax.Sort
import Language.SMTLib2.Syntax.Symbol
import Prettyprinter
  ( Pretty,
    pretty,
    (<+>),
  )
import qualified Prettyprinter as PP
import Text.Megaparsec

data SortDec = SortDec Symbol Numeral
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty SortDec where
  pretty (SortDec s n) = PP.parens (pretty s <+> pretty n)

instance Show SortDec where
  showsPrec _ = showsPrecDefault

parseSortDec :: Parser SortDec
parseSortDec = parens $ SortDec <$> parseSymbol <*> parseNumeral

data SelectorDec = SelectorDec Symbol Sort
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty SelectorDec where
  pretty (SelectorDec sym sort) = PP.parens (pretty sym <+> pretty sort)

instance Show SelectorDec where
  showsPrec _ = showsPrecDefault

parseSelectorDec :: Parser SelectorDec
parseSelectorDec = parens $ SelectorDec <$> parseSymbol <*> parseSort

data ConstructorDec = ConstructorDec Symbol [SelectorDec]
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty ConstructorDec where
  pretty (ConstructorDec sym selectors) =
    PP.parens (pretty sym <+> PP.fillSep (fmap pretty selectors))

instance Show ConstructorDec where
  showsPrec _ = showsPrecDefault

parseConstructorDec :: Parser ConstructorDec
parseConstructorDec =
  parens $ ConstructorDec <$> parseSymbol <*> many parseSelectorDec

data DatatypeDec
  = MonomorphicDatatype (NonEmpty ConstructorDec)
  | PolymorphicDatatype (NonEmpty Symbol) (NonEmpty ConstructorDec)
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty DatatypeDec where
  pretty (MonomorphicDatatype cons) =
    prettyPrintSList . prettyPrintNonEmpty $ cons
  pretty (PolymorphicDatatype syms cons) =
    prettyPrintSList
      [ pp "par",
        prettyPrintSList . prettyPrintNonEmpty $ syms,
        prettyPrintSList . prettyPrintNonEmpty $ cons
      ]

instance Show DatatypeDec where
  showsPrec _ = showsPrecDefault

parseDatatypeDec :: Parser DatatypeDec
parseDatatypeDec =
  parens
    ( ( PolymorphicDatatype
          <$ verbatim "par"
          <*> parens (parseNonEmpty parseSymbol)
          <*> parens (parseNonEmpty parseConstructorDec)
      )
        <|> (MonomorphicDatatype <$> parseNonEmpty parseConstructorDec)
    )

data FunctionDec
  = FunctionDec Symbol [SortedVar] Sort
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty FunctionDec where
  pretty (FunctionDec sym vars sort) =
    prettyPrintSList
      [pretty sym, prettyPrintSList (fmap pretty vars), pretty sort]

instance Show FunctionDec where
  showsPrec _ = showsPrecDefault

parseFunctionDec :: Parser FunctionDec
parseFunctionDec =
  parens $
    FunctionDec
      <$> parseSymbol
      <*> parens (many parseSortedVar)
      <*> parseSort
