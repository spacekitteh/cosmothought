module Language.SMTLib2.Syntax.Attribute where

import Data.Data
import Data.Hashable
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Constants
import Language.SMTLib2.Syntax.Keyword
import Language.SMTLib2.Syntax.SExpr
import Language.SMTLib2.Syntax.Symbol
import Prettyprinter
  ( Pretty,
    pretty,
    (<+>),
  )
import Text.Megaparsec

data AttributeValue
  = ConstantAttr SpecConstant
  | SymbolAttr Symbol
  | SExprsAttr [SExpr]
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty AttributeValue where
  pretty (ConstantAttr c) = pretty c
  pretty (SymbolAttr s) = pretty s
  pretty (SExprsAttr exprs) = prettyPrintSList (fmap pretty exprs)

instance Show AttributeValue where
  showsPrec _ = showsPrecDefault

parseAttributeValue :: Parser AttributeValue
parseAttributeValue =
  ConstantAttr
    <$> parseSpecConstant
    <|> SymbolAttr
      <$> parseSymbol
    <|> SExprsAttr
      <$> many parseSExpr

data Attribute
  = BareKeyword Keyword
  | KeywordValue Keyword AttributeValue
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty Attribute where
  pretty (BareKeyword kw) = pretty kw
  pretty (KeywordValue kw av) = pretty kw <+> pretty av

instance Show Attribute where
  showsPrec _ = showsPrecDefault

parseAttribute :: Parser Attribute
parseAttribute =
  ( do
      keyword <- parseKeyword
      KeywordValue keyword <$> parseAttributeValue
        <|> pure
          (BareKeyword keyword)
  )
    <?> "attribute"
