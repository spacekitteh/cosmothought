module Language.SMTLib2.Syntax.FunctionName where

import Data.Data
import Data.Hashable
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Identifier
import Language.SMTLib2.Syntax.SExpr
import Language.SMTLib2.Syntax.Sort
import Prettyprinter
  ( Pretty,
    pretty,
  )

data FunctionName
  = Not
  | Implies
  | And
  | Or
  | Xor
  | Equals
  | Distinct
  | IfThenElse
  | Select
  | Store
  | ArbitraryFunction QualifiedIdentifier -- TODO arity, sorts, etc here
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance IsSExpr FunctionName where
  toSExpr = error "unimplemented"

instance Pretty FunctionName where
  pretty Not = pp "not"
  pretty Implies = pp "=>"
  pretty And = pp "and"
  pretty Or = pp "or"
  pretty Xor = pp "xor"
  pretty Equals = pp "="
  pretty Distinct = pp "distinct"
  pretty IfThenElse = pp "ite"
  pretty Select = pp "select"
  pretty Store = pp "store"
  pretty (ArbitraryFunction q) = pretty q

instance Show FunctionName where
  showsPrec _ = showsPrecDefault

-- TODO: Reject ill-sorted qualified identifiers
parseFunctionName :: Parser FunctionName
parseFunctionName = do
  qi <- parseQualifiedIdentifier
  let (i, ms) = qualIdToMaybeTyped qi
      symbolHead = identifierHead i
  pure $ case symbolHead of
    "not" -> Not
    "and" -> And
    "or" -> Or
    "xor" -> Xor
    "select" -> Select
    "store" -> Store
    "distinct" -> Distinct
    "=>" -> Implies
    "=" -> Equals
    "ite" -> IfThenElse
    _ -> ArbitraryFunction qi
