module Language.SMTLib2.Syntax.Symbol where

import Data.Char
import Data.Data
import Data.Hashable
import Data.Text (Text)
import qualified Data.Text as T
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Prettyprinter
  ( Pretty,
    pretty,
  )
import Text.Megaparsec
import Text.Megaparsec.Char

type SimpleSymbol = Text

symbol :: Parser Char
symbol = satisfy (\c -> elem c ("!$%&|*+-/:<=>?@^_~#" :: String))

parseSimpleSymbol :: Parser SimpleSymbol
parseSimpleSymbol =
  lexeme
    ( ( do
          first <- letterChar <|> symbol
          rest <- many (alphaNumChar <|> symbol)
          return (T.pack (first : rest))
      )
        <?> "simple symbol"
    )

parseQuotedSymbol :: Parser Text
parseQuotedSymbol =
  lexeme
    ( between
        (char '|')
        (char '|')
        ( takeWhile1P
            (Just "quoted symbol char")
            (\c -> isPrint c && c /= '|' && c /= '\\')
        )
        <?> "quoted symbol"
    )

data Symbol
  = SimpleSymbol SimpleSymbol
  | QuotedSymbol Text
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

symbolText :: Symbol -> Text
symbolText (SimpleSymbol s) = s
symbolText (QuotedSymbol s) = "|" <> s <> "|"

instance Pretty Symbol where
  pretty = pretty . symbolText

instance Show Symbol where
  showsPrec _ = showsPrecDefault

parseSymbol :: Parser Symbol
parseSymbol =
  (QuotedSymbol <$> parseQuotedSymbol)
    <|> (SimpleSymbol <$> parseSimpleSymbol)
