module Language.SMTLib2.Syntax.Sort where

import Data.Data
import Data.Hashable
import Data.List.NonEmpty (NonEmpty)
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Identifier
import Language.SMTLib2.Syntax.SExpr
import Language.SMTLib2.Syntax.Symbol
import Prettyprinter
  ( Pretty,
    pretty,
  )
import Text.Megaparsec

data Sort
  = BoolSort
  | ArraySort Sort Sort
  | PlainSort Identifier
  | IndexedSort Identifier (NonEmpty Sort)
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty Sort where
  pretty BoolSort = pp "Bool"
  pretty (ArraySort s1 s2) = prettyPrintSList' "Array" [pretty s1, pretty s2]
  pretty (PlainSort i) = pretty i
  pretty (IndexedSort ident sorts) =
    prettyPrintSList $ (pretty ident) : prettyPrintNonEmpty sorts

instance Show Sort where
  showsPrec _ = showsPrecDefault

parseSort :: Parser Sort
parseSort =
  (BoolSort <$ verbatim "Bool")
    <|> (ArraySort <$ verbatim "Array" <*> parseSort <*> parseSort)
    <|> (PlainSort <$> parseIdentifier <?> "plain sort")
    <|> ( IndexedSort
            <$> parseIdentifier
            <*> parseNonEmpty parseSort
            <?> "indexed sort"
        )

data SortedVar = SortedVar Symbol Sort
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty SortedVar where
  pretty (SortedVar sym sort) = prettyPrintSList [pretty sym, pretty sort]

instance Show SortedVar where
  showsPrec _ = showsPrecDefault

parseSortedVar :: Parser SortedVar
parseSortedVar =
  parens $ (SortedVar <$> parseSymbol <*> parseSort <?> "sorted variable")

data QualifiedIdentifier
  = PlainIdentifier Identifier
  | TypedIdentifier Identifier Sort
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance IsSExpr QualifiedIdentifier where
  toSExpr = error "unimplemented"

{-# INLINE qualIdToMaybeTyped #-}
qualIdToMaybeTyped :: QualifiedIdentifier -> (Identifier, Maybe Sort)
qualIdToMaybeTyped (PlainIdentifier i) = (i, Nothing)
qualIdToMaybeTyped (TypedIdentifier i s) = (i, Just s)

instance Pretty QualifiedIdentifier where
  pretty (PlainIdentifier ident) = pretty ident
  pretty (TypedIdentifier ident sort) =
    prettyPrintSList [pp "as", pretty ident, pretty sort]

instance Show QualifiedIdentifier where
  showsPrec _ = showsPrecDefault

parseQualifiedIdentifier :: Parser QualifiedIdentifier
parseQualifiedIdentifier =
  (PlainIdentifier <$> parseIdentifier <?> "plain identifier")
    <|> ( parens
            ( verbatim "as"
                *> (TypedIdentifier <$> parseIdentifier <*> parseSort)
                <?> "typed identifier"
            )
        )
