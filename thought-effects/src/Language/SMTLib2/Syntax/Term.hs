module Language.SMTLib2.Syntax.Term where

import Data.Data
import Data.Foldable
import Data.Hashable
import Data.List.NonEmpty (NonEmpty (..))
import Data.Text (Text)
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Attribute
import Language.SMTLib2.Syntax.Constants
import Language.SMTLib2.Syntax.FunctionName
import Language.SMTLib2.Syntax.Pattern
import Language.SMTLib2.Syntax.SExpr
import Language.SMTLib2.Syntax.Sort
import Language.SMTLib2.Syntax.Symbol
import Prettyprinter
  ( Pretty,
    pretty,
  )
import qualified Prettyprinter as PP
import Text.Megaparsec

prettyPrintBinding :: Pretty a => Text -> NonEmpty a -> Term -> PP.Doc ann
prettyPrintBinding quantifier bindings term =
  prettyPrintSList
    [ pretty quantifier,
      prettyPrintSList . prettyPrintNonEmpty $ bindings,
      pretty term
    ]

data VarBinding = VarBinding Symbol Term
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance IsSExpr VarBinding where
  toSExpr (VarBinding s t) = SubExpressions (toSExpr s :| [toSExpr t])

instance Pretty VarBinding where
  pretty (VarBinding s t) = prettyPrintSList [pretty s, pretty t]

instance Show VarBinding where
  showsPrec _ = showsPrecDefault

parseVarBinding :: Parser VarBinding
parseVarBinding =
  parens $ (VarBinding <$> parseSymbol <*> parseTerm <?> "variable binding")

data MatchCase = MatchCase Pattern Term
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty MatchCase where
  pretty (MatchCase p t) = prettyPrintSList [pretty p, pretty t]

instance Show MatchCase where
  showsPrec _ = showsPrecDefault

parseMatchCase :: Parser MatchCase
parseMatchCase = parens $ MatchCase <$> parsePattern <*> parseTerm

data Quantifier
  = Forall (NonEmpty SortedVar) Term
  | Exists (NonEmpty SortedVar) Term
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty Quantifier where
  pretty (Forall bindings term) = prettyPrintBinding "forall" bindings term
  pretty (Exists bindings term) = prettyPrintBinding "exists" bindings term

instance Show Quantifier where
  showsPrec _ = showsPrecDefault

parseQuantifier :: Parser Quantifier
parseQuantifier =
  ( Forall
      <$> (verbatim "forall" *> parseNonEmpty parseSortedVar)
      <*> parseTerm
      <?> "forall binding"
  )
    <|> ( Exists
            <$> (verbatim "exists" *> parseNonEmpty parseSortedVar)
            <*> parseTerm
            <?> "exists binding"
        )

data Term
  = Constant SpecConstant
  | QualifiedIdentifier QualifiedIdentifier
  | Application FunctionName (NonEmpty Term)
  | Quantifier Quantifier
  | LetBinding (NonEmpty VarBinding) Term
  | Match Term (NonEmpty MatchCase)
  | Annotated Term (NonEmpty Attribute)
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance IsSExpr Term where
  toSExpr (Constant c) = ConstantExpr c
  toSExpr (QualifiedIdentifier i) = toSExpr i
  toSExpr (Application func terms) =
    SubExpressions (toSExpr func :| (toList . fmap toSExpr $ terms))

instance Pretty Term where
  pretty (Constant c) = pretty c
  pretty (QualifiedIdentifier ident) = pretty ident
  pretty (Application ident (h :| t)) =
    prettyPrintSList [pretty ident, pretty h, PP.fillSep (fmap pretty t)]
  pretty (Quantifier q) = pretty q
  pretty (LetBinding bindings term) = prettyPrintBinding "let" bindings term
  pretty (Match term cases) =
    prettyPrintSList'
      "match"
      [pretty term, prettyPrintSList . prettyPrintNonEmpty $ cases]
  pretty (Annotated term attrs) =
    prettyPrintSList'
      "!"
      [pretty term, PP.fillSep . prettyPrintNonEmpty $ attrs]

instance Show Term where
  showsPrec _ = showsPrecDefault

parseConstant :: Parser Term
parseConstant = Constant <$> parseSpecConstant <?> "constant term"

parseQualifiedIdentifierTerm :: Parser Term
parseQualifiedIdentifierTerm =
  QualifiedIdentifier
    <$> parseQualifiedIdentifier
    <?> "qualified identifier term"

parseTerm :: Parser Term
parseTerm =
  ( try $
      parens
        ( ( LetBinding
              <$> (verbatim "let" *> parseNonEmpty parseVarBinding)
              <*> parseTerm
              <?> "let binding"
          )
            <|> (Quantifier <$> parseQuantifier)
            <|> ( Match
                    <$> (verbatim "match" *> parseTerm)
                    <*> parseNonEmpty parseMatchCase
                    <?> "case splitting binding"
                )
            <|> ( Annotated
                    <$> (verbatim "!" *> parseTerm)
                    <*> parseNonEmpty parseAttribute
                    <?> "annotation binding"
                )
            <|> ( Application
                    <$> parseFunctionName
                    <*> parseNonEmpty parseTerm
                    <?> "application term"
                )
        )
  )
    <|> parseConstant
    <|> parseQualifiedIdentifierTerm
