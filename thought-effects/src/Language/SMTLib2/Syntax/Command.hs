module Language.SMTLib2.Syntax.Command where

import Data.Data
import Data.Hashable
import Data.List.NonEmpty as NE
  ( NonEmpty,
    unzip,
  )
import Data.Text (Text)
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Attribute
import Language.SMTLib2.Syntax.Constants
import Language.SMTLib2.Syntax.Declaration
import Language.SMTLib2.Syntax.Definition
import Language.SMTLib2.Syntax.InfoFlag
import Language.SMTLib2.Syntax.Keyword
import Language.SMTLib2.Syntax.PropLiteral
import Language.SMTLib2.Syntax.Sort
import Language.SMTLib2.Syntax.Symbol
import Language.SMTLib2.Syntax.Term
import Prettyprinter
  ( Pretty,
    pretty,
    (<+>),
  )
import qualified Prettyprinter as PP
import Text.Megaparsec

data CommandOption
  = DiagnosticOutputChannel OutputChannel
  | GlobalDeclarations Bool
  | InteractiveMode Bool
  | PrintSuccess Bool
  | ProduceAssertions Bool
  | ProduceAssignments Bool
  | ProduceModels Bool
  | ProduceProofs Bool
  | ProduceUnsatAssumptions Bool
  | ProduceUnsatCores Bool
  | RandomSeed Numeral
  | RegularOutputChannel OutputChannel
  | ReproducibleResourceLimit Numeral
  | Verbosity Numeral
  | CustomOption Attribute
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty CommandOption where
  pretty (DiagnosticOutputChannel channel) =
    pp ":diagnostic-output-channel" <+> prettyOutputChannel channel
  pretty (GlobalDeclarations b) = pp ":global-declarations" <+> prettyBool b
  pretty (InteractiveMode b) = pp ":interactive-mode" <+> prettyBool b
  pretty (PrintSuccess b) = pp ":print-success" <+> prettyBool b
  pretty (ProduceAssertions b) = pp ":produce-assertions" <+> prettyBool b
  pretty (ProduceAssignments b) = pp ":produce-assignments" <+> prettyBool b
  pretty (ProduceModels b) = pp ":produce-models" <+> prettyBool b
  pretty (ProduceProofs b) = pp ":produce-proofs" <+> prettyBool b
  pretty (ProduceUnsatAssumptions b) =
    pp ":produce-unsat-assumptions" <+> prettyBool b
  pretty (ProduceUnsatCores b) = pp ":produce-unsat-cores" <+> prettyBool b
  pretty (RandomSeed n) = pp ":random-seed" <+> pretty n
  pretty (RegularOutputChannel channel) =
    pp ":regular-output-channel" <+> prettyOutputChannel channel
  pretty (ReproducibleResourceLimit n) =
    pp ":reproducible-resource-limit" <+> pretty n
  pretty (Verbosity n) = pp "verbosity" <+> pretty n
  pretty (CustomOption attr) = pretty attr

instance Show CommandOption where
  showsPrec _ = showsPrecDefault

parseCommandOption :: Parser CommandOption
parseCommandOption =
  ( do
      kw <- parseKeyword
      case kw of
        "diagnostic-output-channel" ->
          DiagnosticOutputChannel <$> parseOutputChannel
        "global-declarations" -> GlobalDeclarations <$> parseBool
        "interactive-mode" -> InteractiveMode <$> parseBool
        "print-success" -> PrintSuccess <$> parseBool
        "produce-assertions" -> ProduceAssertions <$> parseBool
        "produce-assignments" -> ProduceAssignments <$> parseBool
        "produce-models" -> ProduceModels <$> parseBool
        "produce-proofs" -> ProduceProofs <$> parseBool
        "produce-unsat-assumptions" ->
          ProduceUnsatAssumptions <$> parseBool
        "produce-unsat-cores" -> ProduceUnsatCores <$> parseBool
        "random-seed" -> RandomSeed <$> parseNumeral
        "regular-output-channel" ->
          RegularOutputChannel <$> parseOutputChannel
        "reproducible-resource-limit" ->
          ReproducibleResourceLimit <$> parseNumeral
        "verbosity" -> Verbosity <$> parseNumeral
        _ -> empty
  )
    <|> CustomOption
      <$> parseAttribute

data DeclareCommand
  = DeclareConst Symbol Sort
  | DeclareDatatype Symbol DatatypeDec
  | DeclareDatatypes (NonEmpty (SortDec, DatatypeDec))
  | DeclareFun Symbol [Sort] Sort
  | DeclareSort Symbol Numeral
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty DeclareCommand where
  pretty (DeclareConst sym sort) =
    PP.fillSep [pp "declare-const", pretty sym, pretty sort]
  pretty (DeclareDatatype sym dec) =
    PP.fillSep [pp "declare-datatype", pretty sym, pretty dec]
  pretty (DeclareDatatypes decls) =
    let (sorts, tys) = NE.unzip decls
     in PP.fillSep
          [ pp "declare-datatypes",
            prettyPrintSList . prettyPrintNonEmpty $ sorts,
            prettyPrintSList . prettyPrintNonEmpty $ tys
          ]
  pretty (DeclareFun sym sorts sort) =
    PP.fillSep
      [ pp "declare-fun",
        pretty sym,
        prettyPrintSList $ fmap pretty sorts,
        pretty sort
      ]
  pretty (DeclareSort sort n) =
    PP.fillSep [pp "declare-sort", pretty sort, pretty n]

instance Show DeclareCommand where
  showsPrec _ = showsPrecDefault

parseDeclareCommand :: Parser DeclareCommand
parseDeclareCommand =
  DeclareConst
    <$ verbatim "declare-const"
    <*> parseSymbol
    <*> parseSort
    <|> DeclareDatatypes
      <$ verbatim "declare-datatypes"
      <*> parseNonEmptyPairList parseSortDec parseDatatypeDec
    <|> DeclareDatatype
      <$ verbatim "declare-datatype"
      <*> parseSymbol
      <*> parseDatatypeDec
    <|> DeclareFun
      <$ verbatim "declare-fun"
      <*> parseSymbol
      <*> parens (many parseSort)
      <*> parseSort
    <|> DeclareSort
      <$ verbatim "declare-sort"
      <*> parseSymbol
      <*> parseNumeral

data DefineCommand
  = DefineFun FunctionDef
  | DefineFunRec FunctionDef
  | DefineFunsRec (NonEmpty (FunctionDec, Term))
  | DefineSort Symbol [Symbol] Sort
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty DefineCommand where
  pretty (DefineFun def) = PP.fillSep [pp "define-fun", pretty def]
  pretty (DefineFunRec def) = PP.fillSep [pp "define-fun-rec", pretty def]
  pretty (DefineFunsRec defns) =
    let (decs, terms) = NE.unzip defns
     in PP.fillSep
          [ pp "define-funs-rec",
            prettyPrintSList . prettyPrintNonEmpty $ decs,
            prettyPrintSList . prettyPrintNonEmpty $ terms
          ]
  pretty (DefineSort sym syms sort) =
    PP.fillSep
      [ pp "define-sort",
        pretty sym,
        prettyPrintSList (fmap pretty syms),
        pretty sort
      ]

instance Show DefineCommand where
  showsPrec _ = showsPrecDefault

parseDefineCommand :: Parser DefineCommand
parseDefineCommand =
  DefineFunsRec
    <$ verbatim "define-funs-rec"
    <*> parseNonEmptyPairList parseFunctionDec parseTerm
    <|> DefineFunRec
      <$ verbatim "define-fun-rec"
      <*> parseFunctionDef
    <|> DefineFun
      <$ verbatim "define-fun"
      <*> parseFunctionDef
    <|> DefineSort
      <$ "define-sort"
      <*> parseSymbol
      <*> parens (many parseSymbol)
      <*> parseSort

data GetCommand
  = GetAssertions
  | GetAssignment
  | GetInfo InfoFlag
  | GetModel
  | GetOption Keyword
  | GetProof
  | GetUnsatAssumptions
  | GetUnsatCore
  | GetValue (NonEmpty Term)
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty GetCommand where
  pretty GetAssertions = pp "get-assertions"
  pretty GetAssignment = pp "get-assignment"
  pretty (GetInfo flag) = pp "get-info" <+> pretty flag
  pretty GetModel = pp "get-model"
  pretty (GetOption kw) = pp "get-option" <+> pretty kw
  pretty GetProof = pp "get-proof"
  pretty GetUnsatAssumptions = pp "get-unsat-assumptions"
  pretty GetUnsatCore = pp "get-unsat-core"
  pretty (GetValue terms) =
    pp "get-value" <+> prettyPrintSList (prettyPrintNonEmpty terms)

instance Show GetCommand where
  showsPrec _ = showsPrecDefault

parseGetCommand :: Parser GetCommand
parseGetCommand =
  GetAssertions
    <$ verbatim "get-assertions"
    <|> GetAssignment
      <$ verbatim "get-assignment"
    <|> GetInfo
      <$ verbatim "get-info"
      <*> parseInfoFlag
    <|> GetModel
      <$ verbatim "get-model"
    <|> GetOption
      <$ verbatim "get-option"
      <*> parseKeyword
    <|> GetProof
      <$ verbatim "get-proof"
    <|> GetUnsatAssumptions
      <$ verbatim "get-unsat-assumptions"
    <|> GetUnsatCore
      <$ verbatim "get-unsat-core"
    <|> GetValue
      <$ verbatim "get-value"
      <*> parseNonEmpty parseTerm

data SetCommand
  = SetInfo Attribute
  | SetLogic Symbol
  | SetOption CommandOption
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty SetCommand where
  pretty (SetInfo attr) = pp "set-info" <+> pretty attr
  pretty (SetLogic s) = pp "set-logic" <+> pretty s
  pretty (SetOption o) = pp "set-option" <+> pretty o

instance Show SetCommand where
  showsPrec _ = showsPrecDefault

parseSetCommand :: Parser SetCommand
parseSetCommand =
  SetInfo
    <$ verbatim "set-info"
    <*> parseAttribute
    <|> SetLogic
      <$ verbatim "set-logic"
      <*> parseSymbol
    <|> SetOption
      <$ verbatim "set-option"
      <*> parseCommandOption

data SMTCommand
  = Assert Term
  | CheckSat
  | CheckSatAssuming [PropLiteral]
  | Declare DeclareCommand
  | Define DefineCommand
  | Echo Text
  | Exit
  | Get GetCommand
  | Pop Numeral
  | Push Numeral
  | Reset
  | ResetAssertions
  | Set SetCommand
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty SMTCommand where
  pretty = PP.parens . go
    where
      go (Assert t) = pp "assert" <+> pretty t
      go CheckSat = pp "check-sat"
      go (CheckSatAssuming s) =
        pp "check-sat-assuming" <+> prettyPrintSList (fmap pretty s)
      go (Declare d) = pretty d
      go (Define d) = pretty d
      go (Echo t) = pp "echo" <+> PP.dquotes (pretty t)
      go Exit = pp "exit"
      go (Get c) = pretty c
      go (Pop n) = pp "pop" <+> pretty n
      go (Push n) = pp "push" <+> pretty n
      go Reset = pp "reset"
      go ResetAssertions = pp "reset-assertions"
      go (Set c) = pretty c

instance Show SMTCommand where
  showsPrec _ = showsPrecDefault

parseSMTCommand :: Parser SMTCommand
parseSMTCommand =
  ( parens $
      Assert
        <$ verbatim "assert"
        <*> parseTerm
        <|> CheckSatAssuming
          <$ verbatim "check-sat-assuming"
          <*> many parsePropLiteral
        <|> CheckSat
          <$ verbatim "check-sat"
        <|> Declare
          <$> parseDeclareCommand
        <|> Define
          <$> parseDefineCommand
        <|> Echo
          <$ verbatim "echo"
          <*> parseString
        <|> Exit
          <$ verbatim "exit"
        <|> Get
          <$> parseGetCommand
        <|> Pop
          <$ verbatim "pop"
          <*> parseNumeral
        <|> Push
          <$ verbatim "push"
          <*> parseNumeral
        <|> ResetAssertions
          <$ verbatim "reset-assertions"
        <|> Reset
          <$ verbatim "reset"
        <|> Set
          <$> parseSetCommand
  )
    <?> "parenthesised sexpr"
