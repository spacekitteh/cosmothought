module Language.SMTLib2.Syntax.Constants where

import Data.Char
import Data.Data
import Data.Hashable
import Data.Scientific
import Data.Text (Text)
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Numeric
import Numeric.Natural
import Prettyprinter
  ( Pretty,
    pretty,
  )
import qualified Prettyprinter as PP
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

type Numeral = Natural

stringLiteral :: Parser Text
stringLiteral = lexeme $ between (char '"') (char '"') parseString

parseNumeral :: Parser Numeral
parseNumeral = lexeme L.decimal

type Decimal = Scientific

parseDecimal :: Parser Decimal
parseDecimal = lexeme L.scientific

type Hexadecimal = Natural

parseHexadecimal :: Parser Hexadecimal
parseHexadecimal =
  lexeme
    ( char '#'
        >> char' 'x'
        >> L.hexadecimal
        <?> "hexadecimal number prefixed with \"#x\""
    )

type Binary = Natural

parseBinary :: Parser Binary
parseBinary =
  lexeme
    (char '#' >> char' 'b' >> L.binary <?> "binary number prefixed with \"#b\"")

data SpecConstant
  = NumeralConst Numeral
  | DecimalConst Decimal
  | HexadecimalConst Hexadecimal
  | BinaryConst Binary
  | StringConst Text
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty SpecConstant where
  pretty (NumeralConst n) = pretty n
  pretty (DecimalConst n) = pretty (show n)
  pretty (HexadecimalConst n) = pretty (showHex n "#x")
  pretty (BinaryConst n) = pretty (showIntAtBase 2 intToDigit n "#b")
  pretty (StringConst t) = PP.dquotes (pretty t)

instance Show SpecConstant where
  showsPrec _ = showsPrecDefault

parseSpecConstant :: Parser SpecConstant
parseSpecConstant =
  lexeme
    ( ( NumeralConst
          <$> parseNumeral
          <|> DecimalConst
            <$> parseDecimal
          <|> HexadecimalConst
            <$> parseHexadecimal
          <|> BinaryConst
            <$> parseBinary
          <|> StringConst
            <$> stringLiteral
      )
        <?> "spec constant"
    )

data MetaSpecConst
  = NUMERAL
  | DECIMAL
  | STRING
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable, Enum)

instance Pretty MetaSpecConst where
  pretty NUMERAL = pp "NUMERAL"
  pretty DECIMAL = pp "DECIMAL"
  pretty STRING = pp "STRING"

instance Show MetaSpecConst where
  showsPrec _ = showsPrecDefault

parseMetaSpecConst :: Parser MetaSpecConst
parseMetaSpecConst =
  NUMERAL
    <$ verbatim "NUMERAL"
    <|> DECIMAL
      <$ verbatim "DECIMAL"
    <|> STRING
      <$ verbatim "STRING"
