module Language.SMTLib2.Syntax.Keyword where

import Data.Data
import Data.Hashable
import Data.String (IsString)
import Data.Text (Text)
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Symbol
import Prettyprinter
  ( Pretty,
    pretty,
  )
import Text.Megaparsec
import Text.Megaparsec.Char

newtype Keyword = Keyword {unKeyword :: Text}
  deriving newtype (Eq, Ord, Hashable, Typeable, IsString)
  deriving (Data, Generic)

instance Pretty Keyword where
  pretty (Keyword kw) = pp ":" <> pretty kw

instance Show Keyword where
  showsPrec _ = showsPrecDefault

parseKeyword :: Parser Keyword
parseKeyword = Keyword <$ char ':' <*> parseSimpleSymbol <?> "keyword"
