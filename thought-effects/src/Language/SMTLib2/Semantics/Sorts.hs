module Language.SMTLib2.Semantics.Sorts where

import Polysemy
import GDP
import Numeric.Natural
data KnownSort where
    BoolSort :: KnownSort
    ArraySort :: Natural -> Natural -> KnownSort
    BitVectorSort :: Natural -> KnownSort
    IntSort :: KnownSort
    RealSort :: KnownSort
    FloatingPointSort :: Natural -> Natural -> KnownSort
    ArrowSort :: Sort -> Sort -> KnownSort

data Sort where
    Known :: KnownSort -> Sort
    UserSort :: {- name, arity, etc -> -} Sort

data SortCardinality where
    Exactly :: Natural -> SortCardinality
    VeryBigButFinite :: SortCardinality
    Infinite :: Natural -> SortCardinality