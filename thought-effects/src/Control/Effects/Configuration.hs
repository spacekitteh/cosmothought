module Control.Effects.Configuration where
import           Polysemy
import           Control.Lens

data ConfigurationOwner = Sandbox | User | Machine
    deriving (Eq, Ord, Show)
data Configuration c m a where
    DefaultConfig ::Configuration c m c
    SaveConfig ::ConfigurationOwner -> c -> Configuration c m ()
    LoadConfig ::ConfigurationOwner -> Configuration c m c


makeSem ''Configuration