module Control.Effects.RootCauseAnalysis where
import Polysemy

data RootCauseAnalysis problem cause m a where
    TraceCause :: problem -> cause -> RootCauseAnalysis problem cause m ()
    CurrentSolution :: RootCauseAnalysis problem cause m (problem,cause)

makeSem ''RootCauseAnalysis