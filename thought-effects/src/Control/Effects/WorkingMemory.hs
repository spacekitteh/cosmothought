{-#LANGUAGE FunctionalDependencies#-}
module Control.Effects.WorkingMemory where

import           Polysemy
import           GHC.Generics
import           Data.Data
import           Data.Hashable
import qualified Data.HashSet                  as HS
import           Control.Lens
import           Data.Function
data PortDirection = Undirected | Input | Output deriving (Eq, Ord, Data, Generic, Read, Show)
instance Hashable PortDirection

data Port n l = Port {_portDirection :: PortDirection, _germ :: (Maybe n), _connection :: l} deriving (Eq, Ord, Data, Generic, Read, Show)
instance (Hashable n, Hashable l) => Hashable (Port n l)

class Linkage n l | l -> n where
    ports :: Traversal' l (Port n l)
    nodes :: Traversal' l n

data GT = NodeK | LinkK

data Foo ( ty:: GT) node link r where
    BV ::Foo ty node link r
    M ::([r] -> (Foo ty node link r)) -> Foo ty node link r
    E ::Foo ty node link r
    N ::node -> [(PortDirection, Foo LinkK node link r)] -> Foo NodeK node link r
    L ::link -> [(PortDirection, Foo NodeK node link r)] ->[(PortDirection, Foo LinkK node link r)] -> Foo LinkK node link r


data LGSheaf label r = Empty'
              | Node' label
              -- | Merge two graphs without adding new edges
              | Overlay' r r
              -- | Link label, undirected ports, input ports, output ports
              | Connect' label r r r
              deriving (Eq, Ord, Functor, Foldable, Traversable, Data, Generic)
data LG label =
    Empty
    | Node label
    | Overlay (LG label) (LG label)
    | Connect label (LG label) (LG label) (LG label)
    deriving (Eq, Ord, Functor, Foldable, Traversable, Data, Generic)
data Rec f a =
      BoundVar a
    | Mu ([a] -> [f (Rec f a)]) -- TODO redo this with nominal terms
    | In (f (Rec f a))
    deriving ( Generic)
newtype Graph f = Down { up :: forall a. Rec f a}

type LogicGraphRec label = Graph (LGSheaf label)

{-# INLINABLE gfold #-}
gfold
    :: Functor f
    => (t -> c)
    -> (([t] -> [c]) -> c)
    -> (f c -> c)
    -> Graph f
    -> c
gfold v l f = trans . up  where
    trans (BoundVar x ) = v x
    trans (Mu       g ) = l (fmap (f . fmap trans) . g)
    trans (In       fa) = f (fmap trans fa)

fold :: Functor f => (f c -> c) -> c -> Graph f -> c
fold alg k = gfold id (\g -> head (g (repeat k))) alg

cfold :: Functor f => (f t -> t) -> Graph f -> t
cfold = gfold id (head . fix)

sfold :: (Eq t, Functor f) => (f t -> t) -> t -> Graph f -> t
sfold alg k = gfold id (head . fixVal (repeat k)) alg

fixVal :: Eq a => a -> (a -> a) -> a
fixVal v f = if v == v' then v else fixVal v' f where v' = f v

{-class Section g n l | g -> n l, l -> n where
    glue ::
class LinkageStructure g n l  | g -> n l, l -> n where
-}



data GraphConstruction identifier node edge m a where
    AddNode ::node -> GraphConstruction identifier node edge m identifier
    AddEdge ::edge -> GraphConstruction identifier node edge m identifier
    AddNodes ::Traversable t => t node -> GraphConstruction identifier node edge m (t identifier)
    AddEdges ::Traversable t => t edge -> GraphConstruction identifier node edge m (t identifier)
    AddElements ::Traversable t => t node -> t edge -> GraphConstruction identifier node edge m (t identifier,t identifier)
    AddCompleteSubgraph ::Traversable t => t node -> GraphConstruction identifier node edge m (t identifier)

data GraphDeconstruction identifier node edge m a where
    RemoveNode ::identifier -> GraphDeconstruction identifier node edge m ()
    RemoveEdge ::identifier -> GraphDeconstruction identifier node edge m ()
    RemoveNodes ::Traversable t => t identifier -> GraphDeconstruction identifier node edge m ()
    RemoveEdges ::Traversable t => t identifier -> GraphDeconstruction identifier node edge m ()
    RemoveElements ::Traversable t => t identifier -> t identifier -> GraphDeconstruction identifier node edge m ()

data GraphNodeQuery identifier node m a where
    LookupNode ::identifier -> GraphNodeQuery identifier node m node
