module Control.Effects.Console where

import           Polysemy
import           Data.Text
import           Data.Text.Prettyprint.Doc      ( Pretty
                                                , Doc
                                                )

-- TODO: Add panes, etc. Use an actual terminal lib.
data Console m a where
    GetLine ::Console m Text
    PutLine ::Text -> Console m ()
    DisplayDoc ::Pretty a => a -> Console m ()

makeSem ''Console
