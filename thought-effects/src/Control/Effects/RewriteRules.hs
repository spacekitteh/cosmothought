module Control.Effects.RewriteRules where
import           Polysemy
import           Data.Data
import           GHC.Generics

infixr 8 :-->:
data RewriteRule m a where
    (:-->:) ::a -> Maybe a -> RewriteRule m a
    (:==>:) ::Monad m => a -> m (Maybe a) -> RewriteRule m a


data Rewriting term m a where
    AddRules ::Traversable t => t (RewriteRule m term) -> Rewriting term m ()
