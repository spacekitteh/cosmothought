module Control.Effects.Progress where

import           Data.String
import           Polysemy

data Progress m a where
    UpdateProgress ::Int -> Progress m ()
    UpdateProgressAndInform ::IsString s => s -> Int -> Progress m ()

makeSem ''Progress
