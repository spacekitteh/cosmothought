module Control.Effects.CaseSplitting where
import Polysemy

data CaseSplitting var term m a where
    ChooseCases ::term -> CaseSplitting var term m [var]


makeSem ''CaseSplitting