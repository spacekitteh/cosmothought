module Control.Effects.SMTSolver where
import           Language.SMTLib2.Syntax.AST
import           Polysemy
import           Language.SMTLib2.Syntax.Command


data SMTSolver m a where
    Initialise ::SMTSolver m ()
    Command ::SMTCommand -> SMTSolver m GeneralResponse

makeSem ''SMTSolver
