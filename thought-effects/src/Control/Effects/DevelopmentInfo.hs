module Control.Effects.DevelopmentInfo where
import Polysemy
import Data.Text
import Data.SemVer

data DevelopmentInfo m a where
    ArtifactName :: DevelopmentInfo m Text
    ArtifactAuthors :: DevelopmentInfo m [Text]
    ArtifactVersion :: DevelopmentInfo m Version

makeSem ''DevelopmentInfo