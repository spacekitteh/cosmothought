module Control.Effects.Cache
    ( Cache(..)
    , storeInCache
    , storeListInCache
    , updateCacheEntry
    , isCached
    , cacheContents
    , foldRCacheWithKey
    , removeFromCache
    , flushCache
    , runCacheWithHashMapAsState
    , runCacheWithHashMapAsAtomicState
    , runCacheWithHashMap
    , best
    )
where

import           Polysemy
import           Debug.Trace
import           Polysemy.State
import           Polysemy.AtomicState
import           StmContainers.Map
import qualified Data.HashMap.Lazy             as HML
import qualified Data.HashMap.Strict           as HM
import           Data.Hashable                  ( Hashable )
import           Data.List                      ( head )
import           Data.Tuple                     ( swap )
data Cache k v m a where
    StoreInCache ::k -> v -> Cache k v m ()
    StoreListInCache ::[(k,v)] -> Cache k v m ()
    UpdateCacheEntry ::k -> (v -> Maybe v)-> Cache k v m ()
    IsCached ::k -> Cache k v m (Maybe v)
    CacheContents ::Cache k v m [(k, v)]
    FoldRCacheWithKey ::(k -> v -> a -> a) -> a -> Cache k v m a
    RemoveFromCache ::k -> Cache k v m ()
    FlushCache ::Cache k v m ()

makeSem ''Cache

{-# INLINE runCacheWithHashMapAsState #-}
runCacheWithHashMapAsState
    :: (Eq k, Hashable k)
    => Sem ((Cache k v) ': r) a
    -> Sem (State (HM.HashMap k v) ': r) a
runCacheWithHashMapAsState = reinterpret $ \case
    StoreInCache !k !v      -> modify $ HM.insert k v
    StoreListInCache !l     -> modify $ HM.union (HM.fromList l)
    UpdateCacheEntry !k !f  -> modify $ HM.update f k
    IsCached !k             -> gets $ HM.lookup k
    CacheContents           -> gets $ HM.toList
    FoldRCacheWithKey !f !a -> gets $ HM.foldrWithKey f a
    RemoveFromCache !k      -> modify $ HM.delete k
    FlushCache              -> put HM.empty


{-# INLINE runCacheWithHashMapAsAtomicState #-}
runCacheWithHashMapAsAtomicState
    :: (Eq k, Hashable k)
    => Sem ((Cache k v) ': r) a
    -> Sem (AtomicState (HM.HashMap k v) ': r) a
runCacheWithHashMapAsAtomicState = reinterpret $ \case
    StoreInCache !k !v      -> atomicModify $ HM.insert k v
    StoreListInCache !l     -> atomicModify $ HM.union (HM.fromList l)
    UpdateCacheEntry !k !f  -> atomicModify $ HM.update f k
    IsCached !k             -> fmap (HM.lookup k) (atomicGet)
    CacheContents           -> fmap HM.toList (atomicGet)
    FoldRCacheWithKey !f !a -> fmap (HM.foldrWithKey f a) atomicGet
    RemoveFromCache !k      -> atomicModify $ HM.delete k
    FlushCache              -> atomicPut HM.empty
{-# INLINE runCacheWithHashMap #-}
runCacheWithHashMap
    :: (Eq k, Hashable k)
    => HM.HashMap k v
    -> Sem ((Cache k v) ': r) a
    -> Sem r (HM.HashMap k v, a)
runCacheWithHashMap m = runState m . runCacheWithHashMapAsState

-- 1. KVStore reinterpreter
-- 2. STM-HAMT
-- 3. Redis

{-# INLINABLE best #-}
best :: (Ord v, Member (Cache k v) r) => Sem r (v, k)
best = do
    theFirst <- swap . head <$> cacheContents
    foldRCacheWithKey
        (\critter score a@(bestScore, _) ->
            if bestScore >= score then a else (score, critter)
        )
        theFirst
