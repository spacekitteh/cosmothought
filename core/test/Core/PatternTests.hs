{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}

module Core.PatternTests (patternMatchingTests) where
import Core.Utils.Prettyprinting
--import Algebra.Lattice
import Control.Lens -- (folded, has, (&), (.~), (^.), (^..), (^?!))

-- import Polysemy.Test

import Control.Monad
import Core.CoreTypes as CT

import Core.Patterns
import Core.Patterns.PatternSyntax


import Core.Variable.Substitution



import Data.List (nub)
import Data.Maybe (isJust, isNothing)

import Data.String (fromString)

import qualified Gen
import qualified Gen.Patterns as Gen
import Hedgehog


import Test.Tasty
import Test.Tasty.Hedgehog


wmesCanBeUnified :: Property
wmesCanBeUnified = {-withConfidence 100000000000000 $ -} withTests 15000 $ property $ do
  (wme, sub', pat) <- forAll (Gen.mkWMEAndCompatiblePattern Gen.metadataGen)
  let sub = sub'
  cover 20 "wme has symbolic attribute" $ has (attribute . _Symbol) wme
  cover 20 "wme has identifier attribute" $ has (attribute . _Identifier) wme
  cover 15 "wme has symbol value" $ has (CT.value . _Symbol) wme
  cover 15 "wme has identifier value" $ has (CT.value . _Identifier) wme
  cover 15 "wme has constant value" $ has (CT.value . _ConstantValue) wme
  cover 15 "wme has textual value" $ has (CT.value . _Textual) wme
  cover 15 "wme is state" $ wme ^. isState
  cover 40 "wme is not state" $ not (wme ^. isState)
  annotateShow wme
  annotateShow sub
  annotateShow pat
  let (unifiedObject :: SubstitutionLattice Value) = unifyObject sub (pat ^. patternObject) wme
  annotate ("Unified object: " ++ show unifiedObject)
  let (unifiedAttr :: SubstitutionLattice Value) = unifyAttribute sub (pat ^. patternAttribute) wme
  annotate ("Unified attribute: " ++ show  unifiedAttr)
  let (unifiedVal :: SubstitutionLattice Value) = unifyValue sub (pat ^. patternValue) wme
  annotate ("Unified value: " ++ show unifiedVal)
  let (unifiedPref :: SubstitutionLattice Value) = unifyPreference sub (pat^.patternPreference) wme
  annotate ("Unified preference: " ++ show unifiedPref)
             
  let sub' = patternMatchesWME sub pat wme

  patternMatchIsValid wme pat sub'

                      
patternMatchIsValid :: (MonadTest m, WMEIsState a, Renderable a, Show a) => WME' a -> WMEPattern -> Maybe (WMEPatternMatch' a) -> m ()
patternMatchIsValid wme pat sub' = do
  cover 10 "pattern matches" ( isJust sub' )
  cover 20 "pattern doesn't match" (isNothing sub')
  cover 4 "pattern matches against state" (has (patternObject . patVariable . _StatePatternVariable) pat)
  cover 1 "pattern matches against state and wme is state" (wme ^. isState && has (patternObject . patVariable . _StatePatternVariable) pat)
  cover 10 "pattern contains variable for attribute" (isJust $ pat ^? patternAttribute . patVariable)
  cover 5 "pattern contains equality test for value" (isJust $ pat ^? patternValue . _AConstantEqualityTest)
  let patternVariableCount = lengthOf (each . _APatternVariable) pat
  cover 10 "pattern contains multiple variables" $ patternVariableCount >= 2
  label $ fromString $ "number of pattern variables: " ++ show patternVariableCount
  let uniquePatternVariables = nub $ pat ^.. (each . _APatternVariable)
      uniquePatternVariableCount = length uniquePatternVariables
  label $ fromString $ "number of unique pattern variables: " ++ show uniquePatternVariableCount
  cover 7 "pattern contains multiple unique variables" $ uniquePatternVariableCount >= 2
  cover 3 "pattern contains multiple copies of the same variable" $ uniquePatternVariableCount < patternVariableCount
  cover 1 "pattern requires an operator preference to exist" $ has (patternPreference . _MustHaveAPreference) pat
  cover 1 "pattern requires an operator preference to not exist" $ has (patternPreference . _MustNotHavePreference) pat
  cover 1 "pattern doesn't care about preferences" $ has (patternPreference . _Don'tCareAboutPreference) pat
  cover 1 "pattern requires an operator preference to exist and must match" $ has (patternPreference . _MustHaveAPreference) pat && isJust sub'
  cover 1 "pattern requires an operator preference to exist and must not match" $ has (patternPreference . _MustHaveAPreference) pat && isNothing sub'        
  cover 1 "pattern requires an operator preference to not exist and must match" $ has (patternPreference . _MustNotHavePreference) pat && isJust sub'
  cover 1 "pattern requires an operator preference to not exist and must not match" $ has (patternPreference . _MustNotHavePreference) pat && isNothing sub'        
        
  when (isJust sub') do
    let Just sub = sub'
    annotate ("Substitution dict: " ++ show (render sub))
    annotateShow sub
    

    sub ^. matchedWME === wme
    sub ^. patternMatched === pat
    annotate "Number of substitution vars is greater than or equal to number of unique vars"
    annotate ("Unique variables: " ++ show uniquePatternVariables)
    annotate ("Substitution variables: " ++ show (sub^.. wmeSubstitution . folded))
    assert(lengthOf (wmeSubstitution . folded) sub >= uniquePatternVariableCount)
    
    when (isJust $ pat ^? patternObject . patVariable) $ do
      annotate "Pattern has object variable"                               
      (sub ^. wmeSubstitution . at (pat ^?! patternObject . patVariable . patternVariableVariable)) === Just (wme ^. object . re _ObjectIdentity)
    when (isJust $ pat ^? patternAttribute . patVariable) $ do
      annotate "Pattern has attribute variable"                               
      (sub ^. wmeSubstitution . at (pat ^?! patternAttribute . patVariable . patternVariableVariable)) === Just (wme ^. attribute . re _SymbolicValue)
    when (isJust $ pat ^? patternValue . patVariable) $ do
      annotate "Pattern has value variable"                               
      (sub ^. wmeSubstitution . at (pat ^?! patternValue . patVariable . patternVariableVariable)) === Just (wme ^. CT.value)

patternMatchingTests :: TestTree
patternMatchingTests =
  testGroup
    "Pattern matching"
    [ testProperty "Pattern matching on WMEs" wmesCanBeUnified
    ]
