{-# OPTIONS_GHC -Wwarn=orphans #-}
module Core.Variables where
import Core.Patterns.PatternSyntax (WMEPattern)
import qualified Test.QuickCheck.Hedgehog as H2QC
import Algebra.Lattice.Ordered
import qualified Hedgehog.Gen.QuickCheck as QC2H    
import Agent.Run.Initialisation

import Agent.Run.LowLevelInfrastructure
--import Algebra.PartialOrd
import Algebra.Heyting
import Data.Int
import qualified Hedgehog.Gen as Gen
import Algebra.Heyting.Free
import Algebra.Lattice

import Control.Lens
--import Core.CoreTypes
import Core.Identifiers.IdentifierGenerator
import Core.Utils.Rendering.CoreDoc
import Core.Variable
import Core.Variable.AlphaEquivalence
import Core.Variable.Substitution
import Core.WME
import Data.List
import Gen
import Gen.Patterns as Gen
import Hedgehog
import Hedgehog.Classes
import Prettyprinter
import Test.Tasty
import Test.Tasty.Hedgehog
import Util
import QuickSpec
import QuickSpec.Internal    
import qualified Test.QuickCheck as QC    
valueEqualityRespectsEqualityModuloAlpha :: (Show term, Eq term, HasVariables term, MonadTest m) => term -> m ()
valueEqualityRespectsEqualityModuloAlpha term = do
  term === term
  Hedgehog.diff (EqualityModuloAlpha term) (=@=) (EqualityModuloAlpha term)

testEqualityModuloAlpha :: (Show term, Eq term, HasVariables term, Monad m) => Gen term -> PropertyT m ()
testEqualityModuloAlpha gen = do
  term <- forAll gen
  valueEqualityRespectsEqualityModuloAlpha term

variableTests :: TestTree
variableTests = testGroup "Variables" [subsumptionTests]

subsumptionTests :: TestTree
subsumptionTests = testGroup "Subsumption" [aVariableSubsumesAVariable, equalityModuloRespectsRepeatedVariables, generaliseVariablesCreatesSubsumer]
                   
aVariableSubsumesAVariable :: TestTree
aVariableSubsumesAVariable = testProperty "A variable will subsume another variable" $ withTests 20 $ property $ do
  a <- forAll Gen.variable
  b <- forAll $ (Gen.filter (/= a)) Gen.variable
  (renameA, renameB) <- evalMaybe (subsumesVariables a b)
  renameA === ((a |=> b $ emptySubstitution) ^. re __Substitution)
  renameB === ((b |=> a $ emptySubstitution) ^. re __Substitution)

equalityModuloRespectsRepeatedVariables :: TestTree
equalityModuloRespectsRepeatedVariables = testProperty "EqualityModuloAlpha respects repeated variables" $ property $ do
  a <- forAll Gen.variable
  b <- forAll $ Gen.filter (/= a) Gen.variable
  c <- forAll $ Gen.filter (\c' -> c' /= a && c' /= b) Gen.variable
  let w1 = [] :: [Int]
      w2 = [123]
      w3 = [123,456]
      w4 = [123,456,123]
      x1 = [a,b]
      x2 = [b,c]
      x3 = [c,b]
      y1 = [a,a]
      z1 = [a, a, b]
      z2 = [a,b,c]
      cyclePairs x y = Hedgehog.diff (EqualityModuloAlpha x) (=@=) (EqualityModuloAlpha y)
      cycleNonModuloAlphaPairs x y = Hedgehog.diff (EqualityModuloAlpha x) (\a b -> not (a =@= b)) (EqualityModuloAlpha y)

  Hedgehog.annotate "[] =@= []"
  cyclePairs w1 w1
  Hedgehog.annotate "[a,b] =@= [b,c]"
  cyclePairs x1 x2
  Hedgehog.annotate "[b,c] =@= [c,b]"
  cyclePairs x2 x3
  Hedgehog.annotate "[c,b] =@= [a,b]"
  cyclePairs x3 x1
  
  Hedgehog.annotate "[a,b]=/@= [a,a]"
  cycleNonModuloAlphaPairs x1 y1
  Hedgehog.annotate "[b,c]=/@= [a,a]"
  cycleNonModuloAlphaPairs x2 y1
  Hedgehog.annotate "[a,a,b]=/@= [a,b,c]"
  cycleNonModuloAlphaPairs z1 z2
  Hedgehog.annotate "[a,b,c]=/@= [a,a,b]"
  cycleNonModuloAlphaPairs z2 z1

                         
  Hedgehog.annotate "[]=/@= [123]"
  cycleNonModuloAlphaPairs w1 w2


                             
          
      
          
generaliseVariablesCreatesSubsumer :: TestTree
generaliseVariablesCreatesSubsumer = testProperty "generaliseVariables creates subsumer" $ withTests 1000 $ property $ do
  (_, _, pat') <- forAll (Gen.mkWMEAndCompatiblePattern Gen.metadataGen)
  let pat = makeVariablesTermLocal pat'
  Right agent <- evalIO (getNewAgentData @WMEMetadata Nothing)
  generalised <- evalIO $ runLowLevelEffects Nothing agent . runIDGen $ generaliseVariables pat
  Hedgehog.annotate . show $ "Original pattern variables: " <> show (pat ^.. variables)
  Hedgehog.annotate . show $ "Original pattern:" <+> render pat
  Hedgehog.annotate . show $ "Generalised pattern variables: " <> show (generalised ^.. variables)
  Hedgehog.annotate . show $ "Generalised pattern:" <+> render generalised
  Hedgehog.annotate "Generalised equals original only when unique variable count is equal to the variable count"
  cover 35 "Equal to original" ((length $ pat ^.. variables) == (length $ nub (pat ^.. variables)))
  cover 15 "Not equal to original" ((length $ pat ^.. variables) /= (length $ nub (pat ^.. variables)))
  if ((length $ pat ^.. variables) == (length $ nub (pat ^.. variables)))
    then do
      Hedgehog.annotate "...which is the case"
      Hedgehog.diff (EqualityModuloAlpha generalised) (=@=) (EqualityModuloAlpha pat)
    else do
      Hedgehog.annotate "...which isn't the case"
      Hedgehog.diff (EqualityModuloAlpha generalised) (\a b -> not (a =@= b)) (EqualityModuloAlpha pat)

  (subTo, _subFrom) <- subsumesVariables generalised pat
  substituted <- renameVariables generalised subTo
  Hedgehog.annotate "Generalised with substitution equals original"
  Hedgehog.diff (EqualityModuloAlpha substituted) (=@=) (EqualityModuloAlpha pat)

substitutionTests :: TestTree
substitutionTests = testGroup "Substitution" [substitutionLatticeLaws]



                    

substitutionLatticeLaws :: TestTree
substitutionLatticeLaws =
  testGroup
    "Substitution mathematical properties"
    [ lawsAsTestTree (eqLaws subGen),
      lawsAsTestTree (ordLaws subGen),
      lawsAsTestTree (semigroupLaws subGen),
      lawsAsTestTree (idempotentSemigroupLaws subGen),
      lawsAsTestTree (commutativeSemigroupLaws subGen),
      lawsAsTestTree (monoidLaws subGen),
      lawsAsTestTree (commutativeMonoidLaws subGen),
      testGroup "Join is an idempotent commutative semigroup" [
                     lawsAsTestTree (semigroupLaws joinSubGen),
                     lawsAsTestTree (idempotentSemigroupLaws joinSubGen),
                     lawsAsTestTree (commutativeSemigroupLaws joinSubGen)
--                     lawsAsTestTree (monoidLaws joinSubGen),
--                     lawsAsTestTree (commutativeMonoidLaws joinSubGen)
                    ],
      testGroup "Meet is an idempotent commutative monoid" [
                     lawsAsTestTree (semigroupLaws meetSubGen),
                     lawsAsTestTree (idempotentSemigroupLaws meetSubGen),
                     lawsAsTestTree (commutativeSemigroupLaws meetSubGen),
                     lawsAsTestTree (monoidLaws meetSubGen),
                     lawsAsTestTree (commutativeMonoidLaws meetSubGen)
                    ]
--      property
    ]

-- partialOrdLaws :: (Eq a, PartialOrd a) => Gen a -> Laws
-- partialOrdLaws gen = Laws "PartialOrd"
--                      [
--                       ("
--                      ]

-- eqTransitive :: forall a. (Eq a, Show a) => Gen a -> Property
-- eqTransitive gen = property $ do
--   a <- forAll gen
--   b <- forAll gen
--   c <- forAll gen
--   let lhs = a == b && b == c; rhs = a == c
--   let ctx =
--         contextualise $
--           LawContext
--             { lawContextLawName = "Transitivity",
--               lawContextLawBody = "a == b ∧ b == c" `congruency` "a == c",
--               lawContextTcName = "Eq",
--               lawContextTcProp =
--                 let showA = show a; showB = show b; showC = show c
--                  in lawWhere
--                       [ "a == b ∧ b == c" `congruency` "a == c, where",
--                         "a = " ++ showA,
--                         "b = " ++ showB,
--                         "c = " ++ showC
--                       ],
--               lawContextReduced = reduced lhs rhs
--             }
--   case a == b of
--     True -> case b == c of True -> heqCtx a c ctx; False -> hneqCtx a c ctx
--     False -> case b == c of True -> hneqCtx a c ctx; False -> success

-- latticeLaws :: (Eq a, PartialOrd a, Show a) => Gen a -> Laws
-- latticeLaws gen =
--   Laws
--     "Lattice"
--     [ ("Absorption - Meet", absorption gen Join Meet),
--       ("Absorption - Join", absorption gen Meet Join)                                                     
--     ]

-- -- substitutionIs


subMemberGen :: (Eq a, Arbitrary a) => QC.Gen (Variable, SubstitutionLattice a)
subMemberGen = H2QC.hedgehog (subWithMember QC2H.arbitrary)

basicSubstitutionSignature :: Sig
basicSubstitutionSignature = signature [
 withMaxTestSize 500, withInferInstanceTypes,  withMaxTests 100000, withMaxTermSize 10, withConsistencyCheck, 
 withMaxCommutativeSize 7, 
 con "singleton" (Core.Variable.Substitution.singleton :: Variable -> A -> SubstitutionLattice A),
     
 predicateGen "member"  ( ((\key sub-> has ((at key) . _Just) sub) :: Variable -> SubstitutionLattice Int8 -> Bool))  ( subMemberGen),
 con "|=>" ((|=>) :: Variable -> A -> SubstitutionLattice A -> SubstitutionLattice A),
 defaultTo (Proxy :: Proxy Int8),
-- defaultTo (Proxy :: Proxy Value),
           {-mono @Value,-} mono @Variable,
                mono @Int8,
          
 vars ["v"] (Proxy :: Proxy Value),
 vars ["k"] (Proxy :: Proxy Variable),
 vars ["v"] (Proxy :: Proxy Bool),
 vars ["v"] (Proxy :: Proxy Int8),
 inst (Sub Dict :: (Eq A) :- Eq (SubstitutionLattice A)),
 inst (Sub Dict :: () :- Eq (Value)),
 inst (Sub Dict :: () :- Eq (Variable)),
 inst (Sub Dict :: () :- Eq (Int8)),                   
 inst (Sub Dict :: (Eq A, Arbitrary A) :- Arbitrary (SubstitutionLattice A)),
 inst (Sub Dict :: (() :- Arbitrary Value)),
 inst (Sub Dict :: (() :- Arbitrary Int8)),      
 inst (Sub Dict :: (() :- Arbitrary Variable)),
 inst (Sub Dict :: (() :- Ord Value)),
 inst (Sub Dict :: (() :- Ord Int8)),      
 inst (Sub Dict :: (() :- Ord Variable)),
 inst (Sub Dict :: (Ord A :- Ord (SubstitutionLattice A)))]

substitutionLatticeSigs :: Sig
substitutionLatticeSigs = signature [
 withMaxTestSize 500, withInferInstanceTypes,  withMaxTests 100000, withMaxTermSize 10, withConsistencyCheck,                            withMaxCommutativeSize 7, 
 con "/\\" $ liftC @(Eq A) ((/\) :: ( SubstitutionLattice A -> SubstitutionLattice A -> SubstitutionLattice A)),
 con "\\/" $ liftC @(Eq A) ((\/) :: ( SubstitutionLattice A -> SubstitutionLattice A -> SubstitutionLattice A)),
 con "merge" $ liftC @(Eq A) $ (\(a ::(SubstitutionLattice A)) (b :: (SubstitutionLattice A)) -> (mergeSubstitutions [a,b]) :: SubstitutionLattice A)
                          ]
boundedSubstitutionLatticeSigs :: Sig
boundedSubstitutionLatticeSigs = signature [
 withMaxTestSize 500, withInferInstanceTypes,  withMaxTests 100000, withMaxTermSize 10, withConsistencyCheck,                            withMaxCommutativeSize 7,                  con "trivialSubstitution" $ liftC @(Eq A) ( ( emptySubstitution) :: SubstitutionLattice A),
 con "incompatibleSubstitution" $  ( incompatibleSubstitution :: SubstitutionLattice A)
                  ]
subSig :: Sig
subSig = signature [


                     background [--bools,  --prelude,
                                                   con "False" False,
                                                   con "True" True,
                                                   predicateGen "/="  ( ((/=) :: Variable -> Variable -> Bool)) (H2QC.hedgehog (disjointVariables))],
                                                  substitutionLatticeSigs, basicSubstitutionSignature,  boundedSubstitutionLatticeSigs]

-- con "mergeSubstitutions" $ liftC @(Eq A) (mergeSubstitutions :: [SubstitutionLattice A] -> SubstitutionLattice A),

--instance Ord a => Observe () (Expr a) (Expr a)
instance (Ord a, Bounded a) =>  Observe () (Ordered a ) (Free a) where
    observe _ a =  lowerFree Ordered a  --toExpr a --(\x -> Debug.Trace.trace ("output: " ++ show x ) x) $ toExpr $ Debug.Trace.trace ("input: " ++ show a) a


latticeSig :: Sig
latticeSig = --signature [
  --           series [
     signature [funs, bools,             monoObserve @(Free Int),
                             instanceOf @(Lattice (Free A)), instanceOf @(Heyting (Free A)), instanceOf @(Eq Int),
                             inst (Sub Dict :: ((Ord A) :- Eq (Free A))),                                        
                             con "Var"  (Algebra.Heyting.Free.Var :: A -> Free A),
              predicate "/=" $ liftC @(Eq A) $  ((/=) :: A -> A -> Bool),
              con  "/\\" $ liftC @(Lattice A) $    ((/\) :: (  A ->   A ->  A)),
              con  "\\/" $ liftC @(Lattice A) $  ((\/) :: ( A ->   A -> A)),
                                 
--               ],
--     signature [
--              con "lfp" $ liftC @(Eq A, Heyting A) $ (lfp :: (A -> A) -> A),
--              con "gfp" $ liftC @(Eq A, Heyting A) $ (gfp :: (A -> A) -> A),
              con "fromBool" $ liftC @(Heyting A) $  (fromBool :: Bool -> A),
              con "Bottom" $ liftC @(Heyting A) $ (bottom :: A),
              con "Top" $ liftC @(Heyting A) $ (top :: A),

--              inst (Sub Dict :: ((Ord A, Bounded A) :- Observe () (Ordered A) (Free A))),
--              inst (Sub Dict :: ((Eq A) :- Eq (Ordered A))),
--              inst (Sub Dict :: ((Arbitrary A ) :- Arbitrary (Free A))),
--              inst (Sub Dict :: ((Ord A) :- Ord (Ordered A))),

--     signature [

              con "==>" $ liftC @(Heyting A) $ ((==>) :: A -> A -> A),
              con "neg" $ liftC @(Heyting A) $ (neg ::  A -> A),
              con "<=>" $ liftC @(Heyting A) $ ((<=>) :: A -> A -> A)
  --       ]


--              mono @Int8, monoObserve @(Free Int8)





--              inst (Sub Dict :: ((Ord A) :- Observe () (Expr A) (Expr A))),                   

              -- 
              -- inst (Sub Dict :: ((Eq A) :- Eq (Expr A))),
              -- inst (Sub Dict :: ((Ord A) :- Ord (Expr A))),
              -- mono @Int, monoObserve @(Free Int), 
--              withInferInstanceTypes


--               instanceOf @(Eq Int8),
--               inst (Sub Dict :: (() :- (Lattice (Free Int8)))),
--               instanceOf @(Arbitrary (Free Int8)),
--               instanceOf @(Ord Int8)

--               instanceOf @(Eq Int8), instanceOf @(Eq (Free Int8)), instanceOf @(Observe () Int8 Int8),
-- --              inst (Sub Dict :: ((

--               instanceOf @(Lattice (Free Int8)),                   
--               
--               monoObserve @(Free Int8),
--               instanceOf @(Arbitrary Int8), instanceOf @(Ord Int8), instanceOf @(Observe () (Expr Int8) (Free Int8)),
--               defaultTo (Proxy :: Proxy ( Int8))
              ]


alphaEquivalenceSignature :: Sig
alphaEquivalenceSignature = signature [
                             instanceOf @(Eq WMEPattern), instanceOf @(HasVariables WMEPattern), instanceOf @(AlphaEquivalence (EqualityModuloAlpha WMEPattern)),
                             inst (Sub Dict :: ((Eq A, HasVariables A) :- (AlphaEquivalence (EqualityModuloAlpha A)))),
                             
                             mono @WMEPattern,
                             con "makeVariablesTermLocal" $ liftC @(HasVariables A) $ makeVariablesTermLocal @A,
                             predicate "=@=" $ liftC @(AlphaEquivalence A) $ ((=@=) :: A -> A -> Bool)]
