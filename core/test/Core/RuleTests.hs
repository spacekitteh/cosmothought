module Core.RuleTests where
import Algebra.Lattice
import Control.Applicative
import Control.Lens

import Control.Lens.Action
import Control.Monad
import Control.Monad.IO.Class
import Agent
import Core.Identifiers
import qualified PSCM.ProductionSystem as PSCM
import Rete
import Rete.ReteEffect
import Rete.ReteGraphChecks
import Rete.ReteTypes
import Rete.Types.Tokens
import Core.Rule
import Core.Rule.Match
import Core.Rule.Parser
import Core.Rule.Parts
import Core.Utils.Rendering.CoreDoc
import Core.WME
import qualified Data.HashSet as HS
import Data.List.NonEmpty
import Data.Maybe
import Data.Sequence
import Data.Set as Set
import Data.The
import Data.Typeable
import GHC.Exts (IsList (..))
import GHC.Generics (Generic, M1 (M1))
import qualified Gen hiding (metadataGen)
import qualified Gen.Rules as Gen
import Hedgehog
import Hedgehog.Gen as Gen
import Hedgehog.Range as Range
import Meta.Model
import Numeric.Natural
import OpenTelemetry.Trace.Core hiding (inSpan)
import qualified OpenTelemetry.Trace.Core as OT
import qualified OpenTelemetry.Context as OTC    
import Polysemy.OpenTelemetry
import Prettyprinter
import System.IO.Unsafe
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Sugar
import Util
import Prelude hiding (Ordering (..))

-- todo: add rule subsumption test?

allRuleCommands ::
  forall a gen m state.
  ( HasSpanContext (state Symbolic),
    a ~ WMEMetadata,
    HasMaybeWMPreference a ObjectIdentifier,
    WMEInAlphaNodes (WMEToken' a) a (WME' a),
    HasMemoryBlock (WMEToken' a) (WMEToken' a) a,
    WMEIsState a,
    Show a,
    Show (state Concrete),
    Typeable a,
    MonadIO m,
    MonadTest m,
    HasReteGraph a state,
    MonadGen gen,
    MonadIO gen,
    HasWMETokens a a
  ) =>
  [ Callback
      (AddWMEs a)
      ( HS.HashSet (RuleActivation),
        HS.HashSet (RuleRetraction)
      )
      state
  ] ->
  [ Callback
      (AddRules a)
      ( ( HS.HashSet (ReteNode a),
          HS.HashSet (RuleActivation),
          HS.HashSet (RuleRetraction)
        )
      )
      state
  ] ->
  Gens state gen a ->
  Agent ->
  [Command gen m state]
allRuleCommands verifyWMEActions verifyRuleActions gens agent = (($ agent) <$> [addWMEsCommand verifyWMEActions gens, addRulesCommand @a verifyRuleActions gens])

ruleTests :: IO ([CUBE], TestTree)
ruleTests = do
  (lhsSugar, lhsTests) <- ruleLHSParsingTests
{-  (rhsSugar, rhsTests) <- ruleRHSParsingTests-}
  (ruleSugar, fullRuleTests) <- ruleParsingTests
  pure (lhsSugar <> {-rhsSugar <>-} ruleSugar, testGroup "Rules" [lhsTests, {-rhsTests,-} fullRuleTests])

ruleLHSParsingTests :: IO ([CUBE], TestTree)
ruleLHSParsingTests =
  makeParsingTests
    "*.rulelhs"
    "LHS parsing"
    (objectRespectsAlphaEquivalence)
    ( do
        (info, lhs) <- parseRuleLHSWithPlaceholders (verbatim "-->")
        pure (ruleLHSInfoToPlainLHSInfo info, lhs)
    )

--ruleRHSParsingTests :: IO ([CUBE], TestTree)
--ruleRHSParsingTests = makeParsingTests "*.rulerhs" "RHS parsing" (objectRespectsAlphaEquivalence) (parseRuleRHSWithPlaceholders)

addRuleToPSCM :: (RuleInfoPlain, Rule) -> IO [TestTree]
addRuleToPSCM (_, rule) = do
  pure
    [ testCase "Add parsed rule via PSCM" $ withFreshAgentIO Nothing \agent -> do
        OT.inSpan' (agent ^. tracerL) "Test:AddRuleToPSCM" OT.defaultSpanArguments $ \span ->  runPSCM' (Just $ OTC.insertSpan span mempty)  False agent (PSCM.addRules (rule :| []))
    ]

ruleExtraParseTests :: (RuleInfoPlain, Rule) -> IO [TestTree]
ruleExtraParseTests args = do
  alpha <- objectRespectsAlphaEquivalence args
  pscm <- addRuleToPSCM args
  pure (alpha <> pscm)

ruleParsingTests :: IO ([CUBE], TestTree)
ruleParsingTests = makeParsingTests "*.rule" "Complete rule parsing" (ruleExtraParseTests) (parseRule >>= \rule -> pure (ruleInfoToPlainInfo (rule ^. ruleInfo), rule))

data RuleInfoPlain = RuleInfo RuleLHSInfoPlain RuleRHSInfoPlain Bool deriving (Eq, Show, Read)

data RuleLHSInfoPlain = RuleLHSInfo
  { numberOfPatterns :: Natural,
    numberOfUniqueSymbols :: Natural,
    numberOfSymbols :: Natural,
    -- | Excludes variables appearing in conditions.
    numberOfUniqueBindingVariables :: Natural,
    numberOfBindingVariables :: Natural,
    testsOperator :: Bool
  }
  deriving (Eq, Show, Read)
data RuleRHSInfoPlain = RuleRHSInfo
  { numberOfActions :: Natural,
    numberOfCreatedWMEs :: Natural,
    numberOfRemovedWMEs :: Natural,
    numberOfUniqueActionSymbols :: Natural,
    numberOfActionSymbols :: Natural,
    numberOfUniqueActionBindingVariables :: Natural,
    numberOfActionBindingVariables :: Natural,
    numberOfOperatorPreferencesSet :: Natural
  }
  deriving (Eq, Generic, Show, Read)

ruleInfoToPlainInfo :: RuleInfo -> RuleInfoPlain
ruleInfoToPlainInfo (Core.Rule.Parts.RuleInfo lhs rhs _ruleProvenance testsOp) = Core.RuleTests.RuleInfo (ruleLHSInfoToPlainLHSInfo lhs) (ruleRHSInfoToPlainRHSInfo rhs) (has _Just testsOp)

ruleLHSInfoToPlainLHSInfo :: RuleLHSInfo -> RuleLHSInfoPlain
ruleLHSInfoToPlainLHSInfo Core.Rule.Parts.RuleLHSInfo {..} = Core.RuleTests.RuleLHSInfo {numberOfPatterns = numberOfPatterns, numberOfUniqueSymbols = numberOfUniqueSymbols, numberOfSymbols = numberOfSymbols, numberOfUniqueBindingVariables = numberOfUniqueBindingVariables, numberOfBindingVariables = numberOfBindingVariables, testsOperator = isJust testsOperator}
ruleRHSInfoToPlainRHSInfo :: RuleRHSInfo -> RuleRHSInfoPlain
ruleRHSInfoToPlainRHSInfo Core.Rule.Parts.RuleRHSInfo {..} = Core.RuleTests.RuleRHSInfo {..} 
data AddRules a v = AddRules SpanContext (NonEmpty Rule) (Seq (WME' a)) deriving (Show, Generic, FunctorB, TraversableB)

generateWMEsAndRules :: (MonadIO m, HasMaybeWMPreference a ObjectIdentifier, WMEIsState a,  MonadGen m) => m a -> m ([WME' a], NonEmpty Rule)
generateWMEsAndRules gen = do
  datii <- Gen.nonEmpty (Range.linear 1 10) $ (Gen.makeRuleAndNearbyWMEs gen)
  let (!wmes', _, !rules) = sequence (datii & traversed %~ \(a,b,c) -> (a, Meet b, c))
  when (nub rules /= rules) do
                         Gen.discard
  let wmes = wmes'^..folded.filtered (hasn't isState)
  pure (wmes, rules)
                  
addRulesCommand ::
  forall a gen m state.
  ( HasCallStack,
    a ~ WMEMetadata,
    HasSpanContext (state Symbolic),
    HasMaybeWMPreference a ObjectIdentifier,
    WMEInAlphaNodes (WMEToken' a) a (WME' a),
    HasMemoryBlock (WMEToken' a) (WMEToken' a) a,
    HasWMETokens a a,
    Pretty a,
    HasReteGraph a state,
    Show (TokenAddition a {-Hashable (WMEToken'  a),-}),
    Show a,
    WMEIsState a,
    Show (state Concrete),
    Typeable a,
    MonadIO m,
    MonadTest m,
    HasReteGraph a state,
    MonadGen gen, MonadIO gen
  ) =>
  [Callback (AddRules a) ((HS.HashSet (ReteNode a), HS.HashSet (RuleActivation), HS.HashSet (RuleRetraction))) state] ->
  Gens state gen a ->
  Agent ->
  Command gen m state
addRulesCommand verifyActions gens agent =
  let genInput :: state (Hedgehog.Symbolic) -> Maybe (gen (AddRules a Hedgehog.Symbolic))
      genInput state =
        Just $ do
          (wmes :: [WME' a],rules :: NonEmpty Rule) <- generateWMEsAndRules ((gens ^. metadataGen) state)
          pure (AddRules (state ^. telemetrySpanContext) rules (Data.Sequence.fromList wmes))
      execute ::
        (AddRules a Hedgehog.Concrete) -> m ((HS.HashSet (ReteNode a), HS.HashSet (RuleActivation), HS.HashSet (RuleRetraction)))
      execute (AddRules spanContext (rules :: NonEmpty Rule)  wmes) = do
        label "Adding rules"
        Util.annotate $ show $ "Adding rules:" <+> render rules
        Util.annotate $ "Adding rules: " ++ show rules

        -- (logs, output)
        output <- evalIO @m $ execute'
        -- annotateShow logs
        pure output
        where
          -- execute' :: forall r. (Members '[IDGen, Log, RuleDatabase, Embed IO, Final IO] r) => Sem r ( (HS.HashSet (ReteNode  a), HS.HashSet (RuleActivation), HS.HashSet (RuleRetraction )))
          execute' =
            do
              -- TODO: What I want here is to pass *context*, not the *span context*. Create a context per test.

              runPSCM' Nothing False agent $ inSpan "addRulesCommand" (defaultSpanArguments) do
                addRules rules
      addWMEsAndNodesToState = Update $ \s (AddRules _ _ wmes) _ -> s & wmesYetToAdd @a %~ union (Set.fromList . GHC.Exts.toList $ wmes)
   in Command genInput execute (addWMEsAndNodesToState : modifiesReteGraphCallbacks True ++ verifyActions) -- ([clearsWMEs, wmesNowInNodes])

data AddWMEs a v = AddWMEs (Seq (WME' a)) deriving (Show, Generic, FunctorB, TraversableB)

addWMEsCommand ::
  forall a gen m state.
  ( HasCallStack,
    a ~ WMEMetadata,
    HasMaybeWMPreference a ObjectIdentifier,
    WMEInAlphaNodes (WMEToken' a) a (WME' a),
    HasMemoryBlock (WMEToken' a) (WMEToken' a) a,
    HasWMETokens a a,
    Pretty a,
    HasReteGraph a state,
    Show (TokenAddition a {-Hashable (WMEToken'  a),-}),
    Show a,
    WMEIsState a,
    Show (state Concrete),
    Typeable a,
    MonadIO m,
    MonadTest m,
    HasReteGraph a state,
    MonadGen gen
  ) =>
  [Callback (AddWMEs a) (HS.HashSet (RuleActivation), HS.HashSet (RuleRetraction)) state] ->
  Gens state gen a ->
  Agent ->
  Command gen m state
addWMEsCommand verifyActions gens agent =
  let genInput :: state (Hedgehog.Symbolic) -> Maybe (gen (AddWMEs a Hedgehog.Symbolic))
      genInput state =
        if (state ^. modelProductionNodes @a == Set.empty || state ^. modelJoinNodes @a == Set.empty)
          then Nothing
          else
            if state ^. wmesYetToAdd @a == Set.empty
              then Just $ do
                let rng = Range.exponential 1 5 -- 20
                wmes <- Gen.seq rng (Gen.randomWME ((gens ^. metadataGen) state))
                pure (AddWMEs wmes)
              else Just $ do
                wmes <- fmap Data.Sequence.fromList . Gen.subsequence . Set.toAscList $ (state ^. wmesYetToAdd @a)
                pure (AddWMEs wmes)
      -- execute :: (AddWMEs  a Hedgehog.Concrete) -> m (HS.HashSet (RuleActivation), HS.HashSet (RuleRetraction))
      execute (AddWMEs wmes) = do
        label "Adding WMEs"
        output <- evalIO @m . runMinimalParser' Nothing False agent $ runReteEffects $ addWMEs wmes
        --        annotateShow logs
        pure output
        where

      clearsWMEs = Update $ \s (AddWMEs added) _ -> s & wmesYetToAdd @a %~ \old -> Set.difference old (Set.fromList (added ^.. folded))
      goesThroughVarietyOfImmediateParents = Ensure $ \_ _ _ (activations, retractions) -> do
        when (HS.size (activations) > 0) $ do
          forM_ (activations ^.. folded) \(RuleActivation match) -> do
            let joinParents = unsafePerformIO . doTransaction $ (match ^!! matchingToken . to the . containingReteNode . parentNodes . folded . ignoringLevel . _JoinNode)
            cover 2 "Production token was formed from a join node" (Prelude.length joinParents > 0)
            let alphaParents = unsafePerformIO . doTransaction $ (match ^!! matchingToken . to the . containingReteNode . parentNodes . folded . ignoringLevel . _AlphaNode)
            cover 2 "Production token was formed directly from an alpha node" (Prelude.length alphaParents > 0)
            let memoryParents = unsafePerformIO . doTransaction $ (match ^!! matchingToken . to the . containingReteNode . parentNodes . folded . ignoringLevel . _MemoryNode)
            cover 2 "Production token was formed directly from a memory node" (Prelude.length memoryParents > 0)
   in Command genInput execute (goesThroughVarietyOfImmediateParents : clearsWMEs : modifiesReteGraphCallbacks True ++ verifyActions) -- ([clearsWMEs, wmesNowInNodes])
