{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE RankNTypes #-}

module Core.Identifiers.IdentifierGeneratorTests where

-- (Concrete, FunctorB, Property, Symbolic, TraversableB, Var, forAll, property, test)


import Control.Concurrent.STM
import Control.Lens
import Control.Monad
import Control.Monad.IO.Class


import Core.Identifiers.IdentifierGenerator
import Data.HashSet as HS

import Data.Set as S
import GHC.Generics
import GHC.Stack
import Gen
import Hedgehog
import Hedgehog.Classes
import Hedgehog.Gen as Gen
import Hedgehog.Range as Range
import Meta.Model
import Polysemy
import Polysemy.Embed (runEmbedded)
import Polysemy.Test hiding ((===))
import Test.Tasty
import Test.Tasty.Hedgehog
import Util

idGenCommands :: (MonadIO m, MonadGen gen, MonadTest m, HasKnownIdentifiers state) => [Command gen m state]
idGenCommands = [genNewGID, genNewCID]

data GenerateNewGlobalID v = GenerateNewGlobalID deriving (Eq, Ord, Show, Generic, Generic1, FunctorB, TraversableB)

genNewGID :: forall gen m state. (MonadIO m, MonadTest m, HasKnownIdentifiers state, MonadGen gen) => Command gen m state
genNewGID =
  let genInput :: state (Hedgehog.Symbolic) -> Maybe (gen (GenerateNewGlobalID Hedgehog.Symbolic))
      genInput state =
        Just $ pure GenerateNewGlobalID
      execute :: (GenerateNewGlobalID Hedgehog.Concrete) -> m Identifier
      execute _ = do
        label "Generate new unique ID"
        footnote "Generating UID"
        i <- evalIO @m . atomically . runIDGenSTMGlobal $ newGlobalIdentifier
        footnote ("Made UID: " ++ show i)
        pure i
      cbs =
        [ Update $ \s _ out -> s & knownIdentifiers %~ S.insert out & identifiersCreated +~ 1,
          Ensure $ \_ new _ _ -> S.size (new ^. knownIdentifiers) === (new ^. identifiersCreated)
        ]
   in Command genInput execute cbs

data GenerateNewContextualID v = GenerateNewContextualID (Var Identifier v) deriving (Eq, Ord, Show, Generic, Generic1, FunctorB, TraversableB)

genNewCID :: forall gen m state. (MonadIO m, MonadTest m, HasKnownIdentifiers state, MonadGen gen) => Command gen m state
genNewCID =
  let genInput :: state (Hedgehog.Symbolic) -> Maybe (gen (GenerateNewContextualID Hedgehog.Symbolic))
      genInput state =
        if state ^. identifiersCreated < 1
          then Nothing
          else Just $ do
            cxt <- Gen.element (state ^.. knownIdentifiers . taking 5 folded) -- Five is good enough honestly.
            pure $ GenerateNewContextualID cxt
      execute :: (GenerateNewContextualID Hedgehog.Concrete) -> m Identifier
      execute (GenerateNewContextualID cxt) = do
        label "Generate new contextual  ID"
        footnote "Generating contextual ID"
        i <- evalIO @m . atomically . runIDGenSTMGlobal $ newLocalIdentifierInContext (concrete cxt)
        footnote ("Made contextual ID: " ++ show i)
        pure i
      cbs =
        [ Require $ \s (GenerateNewContextualID v) -> S.member v (s ^. knownIdentifiers),
          Update $ \s _ out -> s & knownIdentifiers %~ S.insert out & identifiersCreated +~ 1,
          Ensure $ \_ new _ _ -> do
            annotateShow (new ^. knownIdentifiers)
            S.size (new ^. knownIdentifiers) === (new ^. identifiersCreated)
        ]
   in Command genInput execute cbs

noDuplicateGlobalIDsAreGenerated :: Property
noDuplicateGlobalIDsAreGenerated = property do
  n <- forAll $ Gen.int (Range.exponentialFrom 2 1000 10000)
  test $ runAutoTest globalAgent $ noDuplicateIDsAreGenerated' @IO n
  where
    noDuplicateIDsAreGenerated' :: forall m r. (Monad m, HasCallStack, Members '[Transactional, Hedgehog m, Embed IO] r) => Int -> Sem r ()
    noDuplicateIDsAreGenerated' n = runEmbedded atomically . runIDGen $ do
      idSet <- HS.fromList <$> replicateM n newGlobalIdentifier
      assertEq @Int @m n (HS.size idSet)

noDuplicateContextualIDsAreGenerated :: Property
noDuplicateContextualIDsAreGenerated = property do
  n <- forAll $ Gen.int (Range.exponentialFrom 2 1000 10000)
  test $ runAutoTest globalAgent $ noDuplicateIDsAreGenerated' @IO n
  where
    noDuplicateIDsAreGenerated' :: forall m r. (Monad m, HasCallStack, Members '[Transactional, Hedgehog m, Embed IO] r) => Int -> Sem r ()
    noDuplicateIDsAreGenerated' n = runEmbedded atomically . runIDGen $ do
      context <- newGlobalIdentifier
      idSet <- HS.fromList <$> replicateM n (newLocalIdentifierInContext context)
      assertEq @Int @m n (HS.size idSet)

idGeneratorTests :: TestTree
idGeneratorTests =
  testGroup
    "ID generation"
    [ testProperty "No duplicate global IDs are generated" noDuplicateGlobalIDsAreGenerated,
      testProperty "No duplicate contextual IDs are generated" noDuplicateContextualIDsAreGenerated,
      testLaws (eqLaws Gen.identifier)
    ]
