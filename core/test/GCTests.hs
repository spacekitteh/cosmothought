{-# OPTIONS_GHC -Wwarn=x-partial #-}
module GCTests where
import GC.Model
import Agent    
import qualified OpenTelemetry.Trace.Core as OT    
import Util
import qualified Data.HashSet as HS    
import Data.Graph.Inductive.Query.DFS
import Data.Graph.Inductive.PatriciaTree as PT
import Data.Graph.Inductive.Graph as Graph hiding ((&))
import Data.Graph.Inductive.Arbitrary ()
import Hedgehog.Gen.QuickCheck as Gen
import Test.QuickCheck.Arbitrary    
import Hedgehog
import Polysemy.Tagged
import Polysemy.Methodology
import Test.Tasty
import Test.Tasty.Hedgehog
import PSCM.ProductionSystem (removeGarbageSimple)
import qualified Hedgehog.Gen as Gen
import Prelude
import qualified OpenTelemetry.Context as OTC
gcTests :: TestTree
gcTests = testGroup "Garbage collection" [gcModelTests]    
gcModelTests :: TestTree
gcModelTests = testGroup "Model" [gcLeavesConnectedComponent]

rootedGraph :: (Arbitrary (gr nodeLabel edgeLabel), Graph.Graph gr, MonadGen m) => m (gr nodeLabel edgeLabel, LNode nodeLabel)
rootedGraph = do
  graph <- Gen.filterT (not . isEmpty) Gen.arbitrary
  let root = head . labNodes $ graph
  pure (graph, root)
  
               
gcLeavesConnectedComponent :: TestTree
gcLeavesConnectedComponent =
    testProperty "GC preserves rooted connected components" $ withTests 4000 $ property $ do
      (graph :: Gr () Int,root)  <- forAll rootedGraph
      let nodesToConsider = HS.fromList $ nodes graph
          roots = HS.singleton (fst root)
      garbage <- evalIO $ withFreshAgentIO Nothing
                 \agent -> OT.inSpan' (agent^.OT.tracerL) "Test:GC:Model:leavesConnectedComponent" OT.defaultSpanArguments \span -> 
                    do
                      runPSCM' (Just $ OTC.insertSpan span mempty) False agent do
                        removeGarbageSimple nodesToConsider roots
                          & untag @GetLinks & runMethodologyPure (\(node) -> suc graph node)
      let gcedGraph = delNodes garbage graph
          remainingNodes = HS.fromList $ nodes gcedGraph
          reachableFromRootOriginal = HS.fromList (reachable (fst root) graph)
          reachableFromRootGCed = HS.fromList $ reachable (fst root) gcedGraph
      reachableFromRootGCed === reachableFromRootOriginal
      remainingNodes === reachableFromRootGCed
      subgraph (reachableFromRootOriginal^..folded) graph === subgraph (reachableFromRootGCed^..folded) gcedGraph
      cover 25 "Single connected component" (noComponents graph == 1)
      cover 60 "Multiple connected components" (noComponents graph > 1)
      1 === noComponents gcedGraph
      Hedgehog.assert (not (HS.member (fst root) (HS.fromList garbage)))
