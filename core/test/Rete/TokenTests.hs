module Rete.TokenTests where


import Agent
import Control.Applicative
import Control.Concurrent.STM (atomically)
import Control.Lens
import Core.Identifiers


import Control.Monad
import Control.Monad.IO.Class

import Core.CoreTypes
import Rete.Types.Tokens
import Core.Patterns.PatternSyntax
import Rete.Dynamics.TokenFlow



import Rete.ReteTypes


import Core.Identifiers.IdentifierGenerator
import Core.WME


import qualified Data.HashSet as HS







import Data.Set as Set 

import Data.Typeable
import Debug.Trace

import GHC.Generics (Generic, M1 (M1))

import Hedgehog
import Hedgehog.Gen as Gen

import Meta.Model




import Prettyprinter
import qualified StmContainers.Set as STMS
import System.IO.Unsafe
import Util
import Prelude hiding (Ordering (..))

allReteTokenCommands ::
  forall  a gen m state.
  ( 
    a ~ WMEMetadata ,
    HasMaybeWMPreference a ObjectIdentifier,
    WMEInAlphaNodes  (WMEToken'  a) a (WME' a),
    HasMemoryBlock (WMEToken'  a)  (WMEToken'  a) a,
    WMEIsState a,
    Show a,
    Show (state Concrete),
    Typeable a,
    MonadIO m,
    MonadTest m,
    HasReteGraph  a state,
    MonadGen gen,
    HasWMETokens a  a
  ) =>
  Gens state gen  a ->
  Agent ->
  [Command gen m state]
allReteTokenCommands gens env = (($ env) <$> [addWMEsToAlphaMemsCommand  @a])

data AddWMEsToAlphaMems  a v = AddWMEsToAlphaMems (Set (WME' a)) deriving (Show, Generic, FunctorB, TraversableB)

addWMEsToAlphaMemsCommand ::
  forall  a gen m state.
  ( a ~ WMEMetadata, HasMaybeWMPreference a ObjectIdentifier,
    WMEInAlphaNodes  (WMEToken'  a) a (WME' a),
    HasMemoryBlock (WMEToken'  a)  (WMEToken'  a) a,
    HasWMETokens a  a,
    Pretty a,
    HasReteGraph  a state,
    Show (TokenAddition  a {-Hashable (WMEToken'  a),-}),
    Show a,
    WMEIsState a,

    Show (state Concrete),
    Typeable a,
    MonadIO m,
    MonadTest m,
    HasReteGraph  a state,
    MonadGen gen
  ) =>
  Agent ->
  Command gen m state
addWMEsToAlphaMemsCommand agent =
  let genInput :: state (Hedgehog.Symbolic) -> Maybe (gen (AddWMEsToAlphaMems  a Hedgehog.Symbolic))
      genInput state =
        if state ^. wmesYetToAdd  @a == Set.empty
          then Nothing
          else Just do
            set <- fmap Set.fromDistinctAscList . Gen.subsequence . Set.toAscList $ (state ^. wmesYetToAdd  @a)
            pure (AddWMEsToAlphaMems set)

      execute :: (AddWMEsToAlphaMems  a Hedgehog.Concrete) -> m (HS.HashSet (TokenAddition  a))
      execute (AddWMEsToAlphaMems wmes) = do
        evalIO @m $ traceMarkerIO "executing addWMEsToAlphaMemsCommand"
        label "Flushing cached WMEs into the graph"
        (logs, output) <- evalIO @m . runMinimalParser' Nothing False agent $ execute' --runFinal . embedToFinal @(IO) $ runEmbedded @(STM) @IO atomically $ execute'
        annotateShow logs
        pure output
        where
--          execute' :: forall r. (Members '[Embed STM, Embed IO, Final IO] r) => Sem r ([LogMessage], HS.HashSet (TokenAddition  a))
          execute' =
            do
              runIDGen $ logToList . runReteTokenManipulation  $ runReteLookup (agent^.reteEnvironment) $ do
                toks <- forM (wmes ^.. folded) \wme -> do
                  nodes <- findAlphaMemoriesByWME wme
                  addWMEsToAlphaMemory [wme] nodes
                pure (HS.unions toks)

      clearsWMEs = Update $ \s (AddWMEsToAlphaMems added) _ -> s & wmesYetToAdd  @a %~ \old -> Set.difference old added
      wmesNowInNodes :: Callback (AddWMEsToAlphaMems  a) (HS.HashSet (TokenAddition  a)) state
      wmesNowInNodes = Ensure $ \_old state (AddWMEsToAlphaMems wmes) additions -> do
        forM_ (additions) \addition ->
          -- verify tokens are alpha memory tokens
          case addition of
            FreshlyAdded tok@(MemToken {}) -> do
              footnote "Checking that tokens are alpha mem tokens"
              annotateShow tok
              footnote "Checking that the memblock's node matches the token's node"
              tok ^?! memBlock . inReteNode === tok ^. containingReteNode
              footnote "Checking that the memblock's node is in the models' known rete nodes"
              Hedgehog.assert (Set.member (Var (Concrete (tok ^?! memBlock . inReteNode))) (state ^. reteNodes  @a @state))
              footnote "Checking that the WME has the node in it's referenced in its alpha node set"
              Hedgehog.assert (unsafePerformIO . atomically $ STMS.lookup (tok ^?! containingReteNode . _AlphaNode ) (tok ^?! (failing  match (tailMatches.folded.match)) . matchedWME .  inhabitedAlphaNodes))
            FreshlyAdded tok -> do
              footnote "Not a mem token!"
              annotateShow tok
              failure
            AlreadyPresent -> do
              footnote "Already present"
              pure ()
   in Command genInput execute ([clearsWMEs, wmesNowInNodes])
