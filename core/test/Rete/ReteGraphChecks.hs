{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ImpredicativeTypes #-}
module Rete.ReteGraphChecks where
import Core.Utils.Prettyprinting

import Control.Concurrent.STM (readTVarIO)
import Core.Utils.MetaTypes


import Data.The

import Control.Applicative

import Control.Lens

import Control.Lens.Action
import Control.Monad

import Control.Parallel.Strategies





import Rete.ReteTypes

import Core.Utils.STM

import Data.Foldable


import qualified Data.HashSet as HS


import Data.Heap (Entry (Entry), payload)



import Data.Maybe (fromJust)
import Data.Set as Set (insert)
import qualified Data.Set.Lens as DSL

import Debug.Trace

import Hedgehog


import Meta.Model




import System.IO.Unsafe
import Util
import Prelude hiding (Ordering (..))

commonNodeParentChildLevelsValid :: MonadTest m => CommonNodeData  a -> CommonNodeData  a -> m ()
commonNodeParentChildLevelsValid parent child = do
  annotateShow parent
  annotateShow child
  Hedgehog.assert $ child ^. level > parent ^. level

commonNodeDataIsValid :: (Renderable a, MonadTest m) => CommonNodeData  a -> m ()
commonNodeDataIsValid cnd = do
  footnote "Checking common node data is valid"
  annotateShow cnd

  footnote "Checking parents have valid node levels"
  let parents = unsafePerformIO . doTransaction $ cnd ^! parentNodes

  forM_ parents (\(Entry parLvl par) -> (par ^. level) === parLvl)
  forM_ parents (\(Entry parLvl par) -> liftTest $ commonNodeParentChildLevelsValid (par ^. commonNodeData) cnd)

  footnote "Checking children have valid node levels"
  let children = unsafePerformIO . doTransaction $ cnd ^! childNodes
  forM_ children (\(Entry childLvl child) -> (child ^. level) === childLvl)
  forM_ children (\(Entry childLvl child) -> liftTest $ commonNodeParentChildLevelsValid cnd (child ^. commonNodeData))

reteNodeIsValid :: (Renderable a, MonadTest m) => Bool -> Bool -> ReteNode  a -> m ()
reteNodeIsValid fromHighLevel checkAncestors node = do
  annotateShow node
  commonNodeDataIsValid (node ^. commonNodeData)
  cover 10 "Alpha nodes" $ has _AlphaNode node
  when (not fromHighLevel) (do
    cover 10 "Memory nodes" $ has _MemoryNode node
    cover 10 "Join nodes" $ has _JoinNode node
    cover 10 "Negative nodes" $ has _NegativeNode node)
  cover 10 "Production nodes" $ has _ProductionNode node

  when checkAncestors do
    -- TODO: Compute ancestors once per graph.
    Hedgehog.annotate "Is a node one of its own ancestors?"
    let ancestors = unsafePerformIO . doTransaction $ withStrategy rpar (allAncestors node)
    Hedgehog.assert $ not $ HS.member node ancestors

  let parents = unsafePerformIO . doTransaction $ node ^!! parentNodes . folded
  if canHaveParentNodes node
    then do
      length parents /== 0
    else do
      let empties = unsafePerformIO . doTransaction $ node ^!! emptyParentNodes . folded
      length empties === 0
      length parents === 0

  when (not . canHaveChildNodes $ node) do
    let children = unsafePerformIO . doTransaction $ node ^!! childNodes . folded
    length children === 0

  {-  let nodeMemoryIsEmpty = unsafePerformIO . atomically $ hasEmptyMemory node
    when (nodeMemoryIsEmpty && canHaveChildNodes node) do
      footnote "The node is empty, so ensure it has a single parent"

      length  === 0
  -}

  case node of
    (maybeCorrectSubType' -> Just node')   -> do -- Memory
      let linkedNodes = unsafePerformIO . doTransaction $ node ^!! childNodes . folded . to payload
      annotateShow linkedNodes

      let (allNodes, !toks) = unsafePerformIO . doTransaction $ do
            ns <- node' ^!! allChildNodes
            toks <- node' ^!! memory . tokens . elems
            pure (ns, toks)
      annotateShow allNodes
      when fromHighLevel do
              length allNodes /== 0                   
      Hedgehog.assert $ (HS.fromList linkedNodes) `HS.isSubsetOf` (HS.fromList allNodes)
    (maybeCorrectSubType' -> Just node') -> do -- Alpha
      let children = unsafePerformIO . doTransaction $ node' ^!! childNodes . folded . to payload
          refs = fromJust $ unsafePerformIO . doTransaction $ node' ^!? getReferenceCount
      annotateShow children
      when fromHighLevel do
              length children /== 0
      annotateShow refs
      Hedgehog.assert $ fromIntegral (length children) <= refs
    (maybeCorrectSubType' -> Just node')  -> do -- Join
      let memNode = unsafePerformIO $ readTVarIO (node' ^?! memoryNode)
      footnote "Checking join node's memory node"
      reteNodeIsValid fromHighLevel checkAncestors (the memNode)
    -- ProductionNode {} -> do 
    --   pure ()
    otherwise -> pure ()

reteGraphIsConsistent :: (Renderable a, MonadTest m, Traversable f) => Bool -> f (ReteNode  a) -> m ()
reteGraphIsConsistent fromHighLevel graph = traceMarker "Checking rete graph consistency" do
  mapM_ (reteNodeIsValid fromHighLevel True) graph

  -- Does the number of unique node identifiers equal the number of nodes?
  HS.size (HS.fromList $ graph ^.. folded . identifier) === lengthOf folded graph

modifiesReteGraphCallbacks :: forall input output  a state. (Renderable a, Show (state Concrete), HasReteGraph  a state) => Bool -> [Callback (input  a) output state]
modifiesReteGraphCallbacks fromHighLevel = [consistentReteGraph fromHighLevel ]

addsNodeCallbacks :: forall input  a state. (Renderable a, Show (state Concrete), HasReteGraph  a state) => Bool -> [Callback (input  a) ((ReteNode  a)) state]
addsNodeCallbacks fromHighLevel =
  [ updateAddsNode  @a @input @state, -- & reteNodes %~ (Set.insert (node ^. memoryNode)),
    reteEnvIsntBlank ,
    addedGivesSubset 
  ]
    ++ modifiesReteGraphCallbacks fromHighLevel

consistentReteGraph :: forall  a input output state. (Renderable a, HasReteGraph  a state) => Bool -> Callback (input a) (output) state
consistentReteGraph fromHighLevel = Ensure $ \_ state _ newNode -> do
  let nodes = unsafePerformIO . doTransaction $ state ^!! agent . reteEnvironment  @_ @a . allNodes . elems
  reteGraphIsConsistent fromHighLevel nodes --  $ state ^.. reteNodes  @a . folded . to concrete

addedGivesSubset :: forall  a input output state. (Renderable a, HasReteGraph  a state) => Callback (input a) (output) state
addedGivesSubset = Ensure $ \oldState newState _ _ -> addedGivesSubset'  @a oldState newState
  where
    addedGivesSubset' :: forall  a state. (Renderable a, HasReteGraph  a state) => state Concrete -> state Concrete -> Test ()
    addedGivesSubset' oldState newState = do
      let oldNodes = over DSL.setmapped concrete (oldState ^. reteNodes  @a)
          newNodes = over DSL.setmapped concrete (newState ^. reteNodes)
      annotateShow oldNodes
      annotateShow newNodes
      Hedgehog.assert $ (HS.fromList (oldNodes ^.. folded)) `HS.isSubsetOf` (HS.fromList (newNodes ^.. folded))

reteEnvIsntBlank :: forall  a input output state. (Show (state Concrete), HasReteGraph  a state) => Callback (input  a) (output) state
reteEnvIsntBlank = Ensure $ \_ state _ _ -> do
  annotateShow state
  Hedgehog.assert $ not (unsafePerformIO $ doTransaction $ reteEnvironmentIsEmpty (state ^. agent . reteEnvironment  @_ @a))

updateAddsNode :: forall  a input state. (HasReteGraph  a state) => Callback (input  a) (ReteNode  a) state
updateAddsNode = Update $ \state _ node ->
  state & reteNodes  @a %~ (Set.insert (node))
