
module PSCMTests where
import PSCM
import PSCM.Errors    
import qualified Command
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Trans.Control
import Agent    
import Commands.Results
--import Control.Exception.Lifted
--import Control.Concurrent
--import Polysemy    
import Cosmolude hiding (bracket)
import Control.Exception
import Meta.Model (testTracer)
--import Control.Concurrent    
import PSCM.ProductionSystemTests
import PSCM.OperatorTests    
import OpenTelemetry.Trace.Core as OT-- (getGlobalTracerProvider, forceFlushTracerProvider, createSpan, endSpan, )

import qualified OpenTelemetry.Context.ThreadLocal as OTCTL
import qualified OpenTelemetry.Context as OTC
--import Core.Configuration
import Data.HashSet.Lens
import ForTesting
import Polysemy.Reader    
import Util
import Test.Tasty.Hedgehog
import Test.Tasty
import Model.PSCM.BasicTypes
--import Core.Utils.Logging    (push)
--import PSCM
import Polysemy.State
import Polysemy.Tagged
import Model.PSCM
import Data.Text
import Data.Int (Int64)    
import Numeric.Natural
import Control.DeepSeq
import Gen
import qualified Hedgehog.Range as Range    
import qualified Hedgehog.Gen as Gen    
import PSCM.Operators.OperatorProposals
import PSCM.Effects.WorkingMemory

pscmTests :: TestTree
pscmTests = testGroup "PSCM"
                    [
                     pscmLoopTests,
                     operatorTests,
                     operatorProposalAndRetrievalTests,
                     productionSystemTests,
                     pscmSubstateTests
                     
  
                    ]    

operatorProposalAndRetrievalTests :: TestTree
operatorProposalAndRetrievalTests = testGroup "Operator manipulation in PSCM" [
                                     operatorProposalsAren'tGC'd,
                                     proposeOperatorThenGetIt
                                     ]

operatorProposalsAren'tGC'd :: TestTree
operatorProposalsAren'tGC'd = testProperty "Operator proposals aren't GC'd" $ property do
     pref <- forAll $ Gen.wmPreference @ObjectIdentifier []
     let wme1 = "(<s> ^operator <hello> " ++ show (render pref) ++ ")"
         wme2 = "(<hello> ^name hiya)"
     [ordered1, ordered2] <- forAll $ Gen.shuffle [wme1, wme2]
     let addedWMEs = ordered1 ++ " " ++ ordered2
         command =  fromString $ "add rules {(state <s> type state) (<s> ^name top)  --> " ++ addedWMEs ++ "}"
     (errs, allwmes1, allwmes2) <-  evalIO $ fst <$>  runPSCMTest Nothing "Test:PSCM:OperatorProposal:OperatorProposalsAren'tGC'd"  \agent -> do
       errs    <- runReader agent $ parseAndExecute "" command
       runGC
       allwmes1 <- (WMEPattern DontCareCondition DontCareCondition DontCareCondition  Don'tCareAboutPreference)^!!wmesMatchingPattern
       allwmes2 <- (fmap snd) <$> getAllObjectsAsKVs 
       pure (errs, allwmes1, allwmes2)

                 
     case asTestResult errs of
                            Nothing -> pure ()
                            Just TestSuccessful -> pure ()
                            Just t@(TestErrors _ _) -> do
                                     footnoteShow t
                                     Util.failure
                                         
     Util.footnote (show allwmes1)
     Util.footnote (show allwmes2)
     Cosmolude.length allwmes2 === 4
     Cosmolude.length allwmes1 === 4
     setOf folded allwmes1 === setOf folded allwmes2


substateChainWithDepth :: MonadGen m => Range Int -> String -> String -> String -> m (String, Int)
substateChainWithDepth range starting ending topname =  do
    depth <- Gen.integral range
    pure (Cosmolude.concat $ theChain' (depth), depth)
    
      where
        theChain' n | n <= 0 = ["(<" ++ starting ++ "> ^name " ++ topname ++ ")"]
                    | n == 1 = ["(<" ++ starting ++ "> ^superstate <" ++ ending ++ ">)"]
                    | otherwise = ("(<" ++ starting ++ "> ^superstate <s" ++ show (n-1) ++ ">) ") : chain' (n-1)
        chain' n | n <= 1 = [" (<s" ++ show n ++ "> ^superstate <" ++ ending ++ ">)"]
                 | otherwise = ("(<s" ++ show n ++ "> ^superstate <s" ++ show (n-1) ++ ">)" ) : chain' (n-1)


wmesAfterItersForDepth :: Integral a => (a -> a -> a -> a) -> a -> a -> a
wmesAfterItersForDepth topStateExtrasOnceReachedDepth iters desiredDepth =
     let wmesPerSubstate = 6
         initialWMEsAtTopState = 2

         phasesPerIter = 3
         curDepth = iters `div` phasesPerIter

     in
         initialWMEsAtTopState + (curDepth * wmesPerSubstate) + (topStateExtrasOnceReachedDepth iters curDepth  desiredDepth) --
operatorSubstateProposalChainTest :: (MonadTest m, MonadIO m) => Span -> Bool -> WMPreference ObjectIdentifier ->  Int -> Int -> Text -> m ()
operatorSubstateProposalChainTest s actuallyProposeOperator pref depth iters' command = do
     
     let extraWMEsFromOperatorProposals iters curDepth desiredDepth = (let  depthReached = curDepth == desiredDepth
                                                                            wmesAtTopStateForOperatorProposal = 2
                                                                        in if  depthReached && ((actuallyProposeOperator && iters >= 3 * desiredDepth && iters > 0) || ((not actuallyProposeOperator) && iters >= 3 * desiredDepth  ))then wmesAtTopStateForOperatorProposal else 0)
     
     Util.footnote $ "depth: " ++ show depth
     
     let iters = fromIntegral iters'
     let wmeCount =  if actuallyProposeOperator  && depth > 0  && iters' >= 3 * depth then 4 else wmesAfterItersForDepth extraWMEsFromOperatorProposals iters' depth
     Util.footnote $ "extraWMEs: " ++ show (extraWMEsFromOperatorProposals iters' (iters' `div` 3) depth)
     Util.footnote $ "wmeCount: " ++ show wmeCount
     Util.footnote $ "iters: " ++ show iters


     Util.footnote (show command)
     (stats, errs, (propList, props, operatorwmes, allwmes1, allwmes2, operatorwmes2)) <- doCommandThenNPSCMPhasesThen (Just s) "Test:PSCM:OperatorProposal:ProposeOperatorThenRetrieveIt" command iters  \_ agent -> do
       
       (propList, props) <- gatherOperatorProposals topLevelStateIdentifier
       operatorwmes <- (operatorPreferenceTestPattern (topLevelStateIdentifier^.stateObject)) ^!! wmesMatchingPattern
       allwmes1 <- (WMEPattern DontCareCondition DontCareCondition DontCareCondition  Don'tCareAboutPreference)^!!wmesMatchingPattern
       allwmes2 <- (fmap snd) <$> getAllObjectsAsKVs
       operatorwmes2 <- (WMEPattern DontCareCondition DontCareCondition DontCareCondition  MustHaveAPreference)^!!wmesMatchingPattern                                      
       pure (propList, props, operatorwmes, allwmes1, allwmes2, operatorwmes2)

                 
     case asTestResult errs of
                            Nothing -> pure ()
                            Just TestSuccessful -> pure ()
                            Just t@(TestErrors _ _) -> do
                                     footnoteShow t
                                     Util.failure
     Util.footnote $ "Next phase: " ++ show (stats ^. currentPhase )
     Util.footnote (show $ render (operatorPreferenceTestPattern (topLevelStateIdentifier^.stateObject)) )
     Util.footnote (show $ render operatorwmes2)
     Util.footnote "------------------------"         
     Util.footnote (show allwmes1)
     Util.footnote "------------------------"
     Util.footnote "wmes2"
     Util.footnote (show allwmes2)
     Util.footnote (show $ render allwmes2)         
     Util.footnote "------------------------"
     Util.footnote (show operatorwmes)
     Util.footnote (show props)

     Cosmolude.length allwmes2 === wmeCount
     Cosmolude.length allwmes1 === wmeCount

     setOf folded operatorwmes === setOf folded operatorwmes2
     setOf folded allwmes1 === setOf folded allwmes2         
     Cosmolude.length operatorwmes === (if actuallyProposeOperator  && iters' >= depth * 3 then 1 else 0)


     Cosmolude.length propList === (if actuallyProposeOperator && iters' >= depth * 3 then 1 else 0)
pscmSubstateTests :: TestTree
pscmSubstateTests = testGroup "PSCM subgoaling" [canMatchWithSuperstates]


canMatchWithSuperstates :: TestTree
canMatchWithSuperstates = testProperty "Can match with superstates" $ property do
    (chain, depth) <- forAll $ substateChainWithDepth {-(Range.linear 0 4)-} (Range.singleton 1) "s" "stop" "top"
    let command = fromString $ "add rules {(state <s> type state) " ++ chain ++ "(<stop> ^name top) --> success on activation }"
    let iters = fromIntegral $ depth*3
    Util.annotate $ "Command: " ++ show command
    (_stats,errs, testResult)  <- doCommandThenNPSCMPhasesThen Nothing
                                  "Test:PSCM:Subgoaling:CanMatchWithSuperstates"
                                  command
                                  iters
                                  (\_ agent -> do
                                     runUntilQuiescence                                         
                                     result <- readT (agent^.testOutcome)
                                     pure result)
--    evalIO $ threadDelay 1000000
    case asTestResult errs of
                            Nothing -> case testResult of
                                         TestWasSuccessful -> success
                                         TestFailed msg -> do
                                                         Util.annotate (show msg)
                                                         Util.failure
                                         NoTestCompleted -> do
                                                         Util.annotate "No test completed!"
                                                         Util.failure
                            Just TestSuccessful -> success
                            Just t@(TestErrors _ _) -> do
                                     footnoteShow t
                                     Util.failure
liftedBracket :: MonadBaseControl IO m => m a -> (a -> m b) -> (a -> m c) -> m c
liftedBracket acquire release action = control $ \runInBase ->
    bracket (runInBase acquire)
            (\saved -> runInBase (restoreM saved >>= release))
            (\saved -> runInBase (restoreM saved >>= action))                                                   



-- the problem is that it assumes only a single token from each rete node can participate in a join. wait, does it? do i not create additional nodes afterwards?
-- gotta try adding the rule and then showing rete node diagram
-- ❯ cabal run cosmothought-core-oneoff -- "add rules {(state <s> type state) (<s> name <x>) (<s> name <y>) -->  (s names <x>) (s names2 <y>)} {(state <s> type state) (<s> names <x>) (<s> names2 <y>) --> success on activation }"      
-- either iterate over the tokens, or do a special case for repeated join nodes
            
proposeOperatorThenGetIt :: TestTree
proposeOperatorThenGetIt = testProperty "Add an operator proposal and retrieve it" $ withTests 800 $ property do
  liftedBracket (do                         
       ctx <- evalIO OTCTL.getContext
       OT.createSpan testTracer ctx "Test:AddAnOperatorProposalAndRetrieveIt" defaultSpanArguments
       )
    (\s -> OT.endSpan s Nothing)
    (\s -> do
     actuallyProposeOperator <- -- pure False --
                                forAll Gen.bool
     pref <- forAll $ Gen.wmPreference @ObjectIdentifier []
     let maxDepth = 3
     (substateChain, depth) <- forAll $ substateChainWithDepth (Range.linear 0 maxDepth) -- (Range.singleton 1)
                               "s" "stop" "top"
     iters' <- -- pure 3 --
               forAll $ Gen.integral (Range.exponentialFrom 0 1 (( depth * 3) + 2))  -- iters to run to get to the relevant operator decision phase
                                   
     let operatorProposalWME = if actuallyProposeOperator then "(<stop> ^operator <hello> " ++ show (render pref) ++ ")" else "(<stop> ^notoperator <hello>)"
     let command =  fromString $ "add rules {(state <s> type state) " ++  substateChain   ++     " (<stop> ^name top) --> " ++ operatorProposalWME ++ " (<hello> ^name hiya)}"                               
     operatorSubstateProposalChainTest s actuallyProposeOperator pref depth iters' command)
     
     
            
    
pscmLoopTests :: TestTree
pscmLoopTests = testGroup "PSCM loop" [emptyLoopTest, first10PhasesAfterValidOperatorProposal, first10PhasesAfterValidOperatorProposalInSubstate] --fivePSCMPhasesTest]


first10PhasesAfterValidOperatorProposalInSubstate :: TestTree
first10PhasesAfterValidOperatorProposalInSubstate = testGroup "First 10 phases after valid operator proposal in a substate" [validOperatorProposal 0 0 InputElaboration, -- Don't run anything. So, 10+1 I guess?
                                                                                         validOperatorProposal 1 1 OperatorProposalAndEvaluation,
                                                                                         validOperatorProposal 2 1 OperatorDecision,
                                                                                         validOperatorProposal 3 2 InputElaboration,
                                                                                         validOperatorProposal 4 2 OperatorProposalAndEvaluation,
                                                                                         validOperatorProposal 5 2 OperatorDecision,
                                                                                         validOperatorProposal 6 2 OperatorElaboration, -- Should have decided on an operator in the superstate
                                                                                         validOperatorProposal 7 1 OperatorApplication, -- Pop to superstate
                                                                                         validOperatorProposal 8 1 OutputToEnvironment,
                                                                                         validOperatorProposal 9 1 InputElaboration,
                                                                                         validOperatorProposal 10 1 OperatorProposalAndEvaluation]
       where
         validOperatorProposal = nPSCMPhasesTest "SubstateProposal" "add rules {(state <s1> type state) (<s2> type state) (<s1> superstate <s2>) (<s2> ^name top) --> (<s2> ^operator <hello> +) (<hello> name thingy)}"
                                                                                                       

first10PhasesAfterValidOperatorProposal :: TestTree
first10PhasesAfterValidOperatorProposal = testGroup "First 10 phases after valid operator proposal in a state" [validOperatorProposal 0 0 InputElaboration, -- Don't run anything. So, 10+1 I guess?
                                                                                         validOperatorProposal 1 1 OperatorProposalAndEvaluation,
                                                                                         validOperatorProposal 2 1 OperatorDecision,
                                                                                         validOperatorProposal 3 1 OperatorElaboration,
                                                                                         validOperatorProposal 4 1 OperatorApplication,
                                                                                         validOperatorProposal 5 1 OutputToEnvironment,
                                                                                         validOperatorProposal 6 1 InputElaboration,
                                                                                         validOperatorProposal 7 1 OperatorProposalAndEvaluation, 
                                                                                         validOperatorProposal 8 1 OperatorDecision, 
                                                                                         validOperatorProposal 9 2 InputElaboration, -- Should have an operator no-change impasse
                                                                                         validOperatorProposal 10 2 OperatorProposalAndEvaluation]--,
--                                                                                         validOperatorProposal 10 2 OperatorDecision]
       where
         validOperatorProposal = nPSCMPhasesTest "TopStateProposal"  "add rules {(state <s1> type state) (<s1> ^name top) --> (<s1> ^operator <hello> +) (<hello> ^name hiya)}"

doCommandThenNPSCMPhasesThen :: forall m r x. ( (State PSCMStatistics : PSCMEffectStack Agent) ~ r, MonadTest m, MonadIO m) => Maybe Span -> Text -> Text -> PSCMCycleCount -> ( PSCMCycleCount -> Agent -> Sem r x) -> m (PSCMStatistics, Command.CommandResults,x)
doCommandThenNPSCMPhasesThen span testName command n action = do
  
  ((stats, (commandResults, actionResults )), tracer') <- evalIO $  runPSCMTest (fmap (\s -> OTC.insertSpan s mempty) span) testName \agent -> do
      let stats = newPSCMStatistics
          props = mempty
      runState stats ( do
                         res <- push "doCommandThenNPSCMPhasesThen-worker" $ do
                          
                           Cosmolude.addAttributes [("test.pscm.iterations", toAttribute @Int64 $ fromIntegral n),
                                          ("test.pscm.command", toAttribute command)
                                         ]
                           cres    <- runReader agent $ parseAndExecute "" command

                           --      Util.annotate ("Initialisation results: " ++ show initialisationResults)                        
                           runNTimes n agent                  & untag @AllOperatorProposals & evalState props
                           ares <- action n agent
                           pure (cres, ares)
                         forceFlushGlobalTracerProvider Nothing
                         pure res
                        
                      )
  case asTestResult commandResults of
                            Nothing -> pure ()
                            Just TestSuccessful -> pure ()
                            Just t@(TestErrors _ _) -> do
                                     footnoteShow t
                                     Util.failure
                                         
  pure (stats, commandResults,  actionResults)
 where
--  performN :: Monad m => PSCMCycleCount -> m t -> m [t]
  performN _ 0 _ = return []
  performN i n m =  do
       
       !x  <- push (fromString $ "iteration " ++ show i) m
       prov <- getGlobalTracerProvider
       _ <- forceFlushTracerProvider prov Nothing
       !xs <- performN (i + 1) (n-1) m
       _ <- forceFlushTracerProvider prov Nothing              
       return $!  (x:xs)
  runNTimes n agent = do
    !_res <- fmap force (performN 0 n do
                           enterPSCMPhase
                        )
    pure ()                       
                                 
nPSCMPhasesTest :: String -> Text -> PSCMCycleCount -> Natural -> PSCMLoopPhase ->  TestTree
nPSCMPhasesTest testName command n depth nextPhase  = localOption (mkTimeout 50000000) $ testProperty (fromString $ show n ++ " PSCM Phases with an operator proposal") $ withTests 1 $ property $  do
  (stats, errors, ()) <- doCommandThenNPSCMPhasesThen Nothing (fromString $ "Test:PSCM:PSCMLoop:nPSCMPhases, " ++ testName ++ ", n=" ++ show n) command n (\ _ _ -> pure ())
  -- (stats, errors) <- evalIO $  runPSCMTest (fromString $ "Test:PSCM:PSCMLoop:nPSCMPhases, " ++ testName ++ ", n=" ++ show n)  \agent -> do
  --     let stats = newPSCMStatistics
  --         props = mempty
  --     runState stats ( do
  --                         -- initialisationResults
  --                         errs    <- runReader agent $ parseAndExecute "" command

  --                         --      Util.annotate ("Initialisation results: " ++ show initialisationResults)                        
  --                         runNTimes n agent                  & untag @AllOperatorProposals & evalState props
  --                         pure errs
  --                     )

                   
  Util.annotate $ "Rule count: " <> (show $ stats^.totalRules)

  Util.annotate "PSCM cycle count:"                      
  stats^.pscmCycleCount === n
  Util.annotate "Current state depth:"
  stats ^. currentStateDepth === depth
  Util.annotate "Next phase:"
  stats ^. currentPhase === nextPhase




                
fivePSCMPhasesTest :: TestTree
fivePSCMPhasesTest = localOption (mkTimeout 50000000) $ testProperty "Five PSCM Phases with an operator proposal" $ withTests 1 $ property $  do
  (stats, tracer) <- evalIO $  runPSCMTest Nothing "Test:PSCM:PSCMLoop:fivePSCMPhasesTest" \agent ->  do

      let stats = newPSCMStatistics
          props = mempty
      execState stats (
                       runFiveTimes agent
                       & untag @AllOperatorProposals & evalState props
                      )
                             
                      
  stats^.pscmCycleCount === 5
  stats ^. currentPhase === OperatorElaboration
 where
   runFiveTimes agent = do
      !_ <- runReader agent $ parseAndExecute ""{-processCommand' agent-} "add rules {(state <s1> type state) (<s2> type state) (<s1> superstate <s2>) --> (<s2> ^operator <hello> +)}" --"!add rules {(state <s1> type state) (<s1> been hit) --> log at level error on activation \"hello\"} {(state <s1> type state) (<s2> type state) (<s2> superstate <s2>) --> (<s2> ^operator <hello> +)} {(state <s1> type state) (<s1> operator <hello>) --> (<s1> been hit)}"
     
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase

              
emptyLoopTest :: TestTree
emptyLoopTest = localOption (mkTimeout 50000000) $ testProperty "Empty loop" $ withTests 1 $ property $  do
  (stats, tracer) <- evalIO $ runPSCMTest Nothing "Test:PSCM:PSCMLoop:emptyLoopTest" \agent -> do

      let stats = newPSCMStatistics
          props = mempty
      execState stats (
                       runThirteenTimes
                       & untag @AllOperatorProposals & evalState props
                      )
                             
                      
  stats^.pscmCycleCount === 13
  stats ^. currentPhase === OperatorProposalAndEvaluation
 where
   runThirteenTimes = do
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
