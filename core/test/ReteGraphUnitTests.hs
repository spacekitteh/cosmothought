module ReteGraphUnitTests where

import Util

-- |
--  Grandmother rule:
--  (alice ^mother-of beth)
--  (beth ^mother-of claire)
--  =====>
--  (alice ^grandmother-of claire)
grandmotherRuleFires :: Property
grandmotherRuleFires = undefined
