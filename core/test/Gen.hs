{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE TupleSections #-}
{-# OPTIONS_GHC -foptimal-applicative-do -Wwarn=unused-matches -Wwarn=unused-local-binds -Wwarn=type-defaults  -Wwarn=orphans #-}

module Gen where

import Control.Lens hiding (Identity (..))

import Control.Monad.STM
import Core.Variable


import Core.CoreTypes as CT
import Core.Identifiers

import Core.Identifiers.IdentifierType



import qualified Hedgehog.Gen.QuickCheck as QC2H
import Hedgehog.Range as Range
import Test.QuickCheck (Arbitrary (..))
import qualified Test.QuickCheck.Hedgehog as H2QC
import Core.Variable.Substitution
import Algebra.Lattice    


import Core.WME
import Data.Interned
import Data.Interned.Text

import Data.Ratio




import Data.UUID (fromWords64)
import Data.Word

import Hedgehog
import Hedgehog.Corpus
import Hedgehog.Gen as Gen


import System.IO.Unsafe (unsafePerformIO)

addExtra :: (MonadGen m, Eq a) => [a] -> m a -> m a
addExtra extras = Gen.shrink (\current -> if not $ elem current extras then extras else [])

globalIdentifier' :: MonadGen m => m Identifier
globalIdentifier' = addExtra [arbitraryTestIdentifier] $ do
  a <- Gen.prune (Gen.integral Range.constantBounded) -- We don't need to be testing UUIDs.
  b <- Gen.enum 0 (fromIntegral (20 :: Int))
  pure (UniqueIdentifier (fromWords64 a b))

globalIdentifier :: MonadGen m => m Identifier
globalIdentifier = do
  a <- Gen.prune $ Gen.word64 Range.constantBounded
  b <- Gen.prune $ Gen.word64 Range.constantBounded
  pure (UniqueIdentifier (fromWords64 a b))
       
globalIdentifierGiven :: MonadGen m => Word64 -> Word64 -> m Identifier
globalIdentifierGiven a b = addExtra [arbitraryTestIdentifier] $ pure (UniqueIdentifier (fromWords64 a b))

identifierForPatternVariables :: MonadGen m => m Identifier
identifierForPatternVariables = addExtra [arbitraryTestIdentifier] $ do
  a <- Gen.prune $ Gen.word64 Range.constantBounded                                  
  b <- Gen.enum 0 (fromIntegral (100 :: Int))
  pure (UniqueIdentifier (fromWords64 a b))

identifier :: MonadGen m => m Identifier
identifier = Gen.recursive Gen.choice [globalIdentifier] [Gen.subtermM Gen.identifier (\con -> ContextualIdentifier <$> ((Gen.prune (Gen.enum 1 10))) <*> pure con)] 

objectIdentifierNamed :: MonadGen m => SymbolName -> m ObjectIdentifier
objectIdentifierNamed n = do
  i <- Gen.identifier
  pure (ObjectIdentifier n i)

internedText :: MonadGen m => m InternedText
internedText = Gen.prune $ Gen.element (intern <$> animals) -- text (Range.constant 1 4) alpha

uriIdentifier :: MonadGen m => m (SymbolName, Identifier)
uriIdentifier = do
  name <- Gen.internedText
  i <- justT (pure (symbolNameToURIIdentifier name))
  pure $! (name, i)

objectIdentifier :: MonadGen m => m ObjectIdentifier
objectIdentifier = do
  (name, i) <- Gen.prune uriIdentifier
  pure $! ObjectIdentifier name i -- <$> (Gen.internedText) <*> Gen.identifier

symbolicValue :: MonadGen m => m SymbolicValue
symbolicValue = addExtra ["some-arbitrary-symbol"] $ choice [Symbol <$> Gen.internedText, SymbolicIdentity <$> Gen.identifier]

constantValue :: MonadGen m => m ConstantValue
constantValue =
  choice
    [ fmap IntValue (integral (Range.constantFrom 0 (-10 ^ 2) (10 ^ 2))),
      fmap RationalValue $ do
        num <- integral (Range.constantFrom 0 (-10 ^ 2) (10 ^ 2))
        denom <- Gen.prune <$> Gen.filterT (/= 0) $ Gen.integral (Range.exponentialFrom 1 (-10) (10))
        pure (num % denom),
      fmap FloatValue (double (exponentialFloatFrom 0 (-10 ^ 2) (10 ^ 2)))
    ]

value :: MonadGen m => m Value
value = choice [fmap Symbolic Gen.symbolicValue, fmap Constant Gen.constantValue, fmap Textual (Gen.text (Range.linearFrom 0 1 30) Gen.unicode)]
instance Arbitrary Value where
  arbitrary = H2QC.hedgehog Gen.value

randomWME :: forall m a. (MonadGen m, HasMaybeWMPreference a ObjectIdentifier, WMEIsState a) => m a -> m (WME' a)
randomWME genMetadata = do
  -- WME identifier
  !wi <- WMEIdentifier <$> Gen.globalIdentifier
  -- object
  !i <- Gen.objectIdentifier
  -- attribute
  !a <- Gen.symbolicValue
  -- value
  !v <- Gen.value
  meta' <- genMetadata
  let meta = if meta' ^. isState then meta' & preference @a @ObjectIdentifier .~ Nothing else meta'
  pure $! (WME i a v wi meta)


wmPreference :: forall v m. (Eq v, MonadGen m) => [v] -> m (WMPreference v)
wmPreference othersToMaybePrefer = do
  indiff <- Indifferent <$> Gen.maybe (Gen.double (Range.exponentialFloatFrom 0 (-10) 10))
  let baseList = [Acceptable, Reject, Prohibit, Required, indiff,  Best, Worst] 
  if othersToMaybePrefer == []
    then do
      Gen.element (baseList)
    else do
      chosen <- Gen.element othersToMaybePrefer
      Gen.element (baseList ++ [PreferableOverOther chosen, OtherIsPreferable chosen, IndifferentTo chosen])

metadataGen :: (MonadGen gen, HasMaybeWMPreference WMEMetadata ObjectIdentifier) => gen WMEMetadata
metadataGen = do
  wmeIsState <- Gen.frequency [(80, pure False), (20, pure True)]
  pref <- Gen.maybe (Gen.wmPreference @ObjectIdentifier []) -- TODO include existing identifiers
  let !blank = unsafePerformIO . atomically $ blankWMEMetadata
  pure $! (blank & isState .~ wmeIsState & preference .~ pref)



subGen' :: Eq a => Gen a -> Gen (SubstitutionLattice a)
subGen' gen = addExtra [incompatibleSubstitution, trivialSubstitution] $ do
  vals <- Gen.list (Range.exponentialFrom 0 10 100) gen
  theMap <-
    mapM
      ( \a -> do
          var <- Gen.variable
          pure (var, a)
      )
      vals
  pure $ constructSubstitution theMap

instance {-# OVERLAPPABLE #-} (Arbitrary a, Eq a) => Arbitrary (SubstitutionLattice a) where
  arbitrary = H2QC.hedgehog (subGen' (QC2H.arbitrary))
  shrink a = constructSubstitution  <$> (Test.QuickCheck.shrink (a^@..ifoldedSubstitution))

subGen :: Gen (SubstitutionLattice Value)
subGen = subGen' Gen.value

joinSubGen :: Gen (Join (SubstitutionLattice Value))
joinSubGen = Join <$> subGen

meetSubGen :: Gen (Meet (SubstitutionLattice Value))
meetSubGen = Meet <$> subGen       
variable :: (MonadGen m) => m Variable
variable = do
  name <- Gen.prune Gen.internedText
  Gen.variableNamed name

instance Arbitrary Variable where
  arbitrary = H2QC.hedgehog Gen.variable

variableNamed :: (MonadGen m) => InternedText -> m Variable
variableNamed name = do
  i <- Gen.prune Gen.identifierForPatternVariables
  pure $! (AVariable i name)

disjointVariables :: Gen (Variable,Variable)
disjointVariables = do
  x <- Gen.variable
  y <- Gen.filter (/= x) Gen.variable
  pure (x,y)
       
subWithMember :: (Eq a) => Gen a ->   Gen (Variable, SubstitutionLattice a)
subWithMember valgen  = do
  existing <- subGen' valgen
  newVar <- Gen.variable
  newVal <- valgen
  pure $ (newVar, (newVar |=> newVal) existing)
