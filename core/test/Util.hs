{-# LANGUAGE TupleSections #-}
module Util
  ( module Util,
    module Core.WME,
    module Rete.Environment,
    module Polysemy.Transaction,
    module Prettyprinter,
    module Data.Kind,
    module Core.CoreTypes,
    module Rete.ReteTypes,
    module Hedgehog,
    module Control.Lens,
    module Control.Lens.Action,

    module Range,
    module PP,
    module Core.Utils.Parsing,
    module Core.Rule.Database,
    module Core.Identifiers.IdentifierType
  )
where
import Hedgehog.Classes
import Core.Error  
import Paths_cosmothought_core
import qualified Polysemy.Test as PT
import Polysemy.Fail
import Hedgehog.Internal.Property (Failure)

import Polysemy.Transaction
import qualified Di.Df1 as Dif1

import Polysemy.Error

import Polysemy.Internal
import Core.Identifiers.IdentifierType
import System.IO (readFile')

import Core.Rule.Database

import Core.Utils.Logging

import Control.Concurrent.STM
import Control.Lens hiding (Identity)
import Control.Lens.Action
import Core.CoreTypes

import Core.Variable
import Core.Variable.AlphaEquivalence
import Rete.Environment
import Rete.ReteTypes
import Core.Identifiers.IdentifierGenerator
import Core.Utils.Parsing hiding (empty, failure, label)
import Core.WME 

import qualified Data.HashMap.Strict as HMS
import Data.Kind

import Meta.Model    

import Data.String (fromString)
import Data.Text (Text)

import Error.Diagnose

import GHC.Stack
import Hedgehog hiding (Action)


import qualified Hedgehog.Range as Range
import Numeric.Natural
import OpenTelemetry.Trace as OT
import OpenTelemetry.Trace.Core    
import qualified  OpenTelemetry.Context as OTC
import Polysemy.Output

import Prettyprinter hiding (annotate)
import qualified Prettyprinter as PP

import System.FilePath.GlobPattern (GlobPattern)
import System.IO.Unsafe (unsafePerformIO)
import Test.Tasty

import Test.Tasty.Hedgehog
import Test.Tasty.Sugar


import Agent


-- withFreshAgent ::  Bool -> (Agent -> Sem (PSCMEffectStack Agent) x) -> IO x
-- parseMinimal' :: HasCallStack => AgentData -> Parser (MinimalParserEffects AgentData) a -> String -> Text -> IO (Either (ParseErrorBundle DiagnosticMessageType ParsingError) a)

parseMinimal :: HasCallStack => Parser (MinimalParserEffects Agent) a -> String -> Text -> Either (ParseErrorBundle DiagnosticMessageType ParsingError) a
parseMinimal p !s !t =
  unsafePerformIO (withFreshAgentIO Nothing (\agent -> do
                                       tp <- getGlobalTracerProvider
                                       result <- inSpan' testTracer "test.parsing.file" defaultSpanArguments (\span -> do
                                         addAttribute span "test.parsing.file.name" (toAttribute $ fromString @Text s)
                                         addAttribute span "test.parsing.input" t
                                         parseMinimal' agent p s t)
                                       !_ <-  forceFlushTracerProvider tp Nothing -- (Just 100000) -- 100 millisecond timeout
                                       pure result
                                                                                                   ))

------------------------------------------------------
--- From `hspec-megaparsec`
------------------------------------------------------

----------------------------------------------------------------------------
-- Basic expectations

-- | Create an expectation by saying what the result should be.
--
-- > parse letterChar "" "x" `shouldParse` 'x'
shouldParseTo' ::
  ( HasCallStack,
    Show a,
    Eq a,
    MonadTest m
  ) =>
  FilePath ->
  -- | The contents being parsed
  String ->
  -- | Result of parsing as returned by function like 'parse'
  Either (ParseErrorBundle DiagnosticMessageType ParsingError) a ->
  -- | Desired result
  a ->
  m ()
shouldParseTo' file contents r v = case r of
  Left e -> do
    annotate
      ( "expected: "
          ++ show v
          ++ "\nbut parsing failed with error:\n"
          ++ (diagnosticToPrettyString . (\d -> addFile d file contents) . diagnose @WhenParsing @DiagnosticMessageType $ e)
      )
    Hedgehog.failure
  Right x -> do
    annotateShow x
    annotateShow v
    x === v
    success

-- | Create an expectation by saying what the result should be.
--
-- > parse letterChar "" "x" `shouldParse` 'x'
shouldParse ::
  ( HasCallStack,
    Show a,
    Eq a,
    MonadTest m
  ) =>
  -- | Result of parsing as returned by function like 'parse'
  Either (ParseErrorBundle DiagnosticMessageType ParsingError) a ->
  -- | Desired result
  () ->
  m ()
r `shouldParse` () = case r of
  Left e -> do
    annotate
      ( "Parsing failed with error:\n"
          ++ (diagnosticToPrettyString . diagnose @WhenParsing @DiagnosticMessageType $ e)
      )
    Hedgehog.failure
  Right x -> do
    annotateShow x
    success

-- | Create an expectation by saying what the result should be.
--
-- > parse letterChar "" "x" `shouldParse` 'x'
shouldParseToM ::
  ( HasCallStack,
    Show a,
    Eq a,
    MonadTest m
  ) =>
  -- | Result of parsing as returned by function like 'parse'
  m (Either (ParseErrorBundle DiagnosticMessageType ParsingError) a) ->
  -- | Desired result
  a ->
  m ()
r' `shouldParseToM` v = do
  r <- r'
  case r of
    Left e -> do
      annotate
        ( "expected: "
            ++ show v
            ++ "\nbut parsing failed with error:\n"
            ++ (diagnosticToPrettyString . diagnose @WhenParsing @DiagnosticMessageType $ e)
        )
      Hedgehog.failure
    Right x -> do
      annotateShow x
      annotateShow v
      x === v
      success

-- | Create an expectation by saying that the parse should fail.
--
-- > parse letterChar "" "x" `shouldParse` 'x'
shouldFailToParseWithM ::
  ( HasCallStack,
    Show a,
    Eq a,
    MonadTest m
  ) =>
  -- | Result of parsing as returned by function like 'parse'
  m (Either (ParseErrorBundle DiagnosticMessageType ParsingError) a) ->
  -- | Desired result
  (ParseErrorBundle DiagnosticMessageType ParsingError) ->
  m ()
r' `shouldFailToParseWithM` v = do
  r <- r'
  case r of
    Left e -> do
      e === v
    Right x -> do
      annotate $ "Expected a parse to fail, but instead it returned " ++ show x
      annotate $ "Expected parse error: " ++ (showBundle v)
      annotate $ "Prettified expected error: " ++ (diagnosticToPrettyString . diagnose @WhenParsing @DiagnosticMessageType $ v)
      Hedgehog.failure

-- | Create an expectation by saying that the parse should fail.
--
-- > parse letterChar "" "x" `shouldParse` 'x'
shouldFailToParseWith ::
  ( HasCallStack,
    Show a,
    Eq a
  ) =>
  -- | Result of parsing as returned by function like 'parse'
  Either (ParseErrorBundle DiagnosticMessageType ParsingError) a ->
  -- | Desired result
  (FilePath, String, String) ->
  Test ()
r `shouldFailToParseWith` (path, source, v) = case r of
  Left e -> do
    (diagnosticToPrettyString . (\d -> addFile d path source) . diagnose @WhenParsing @DiagnosticMessageType $ e) === v
  Right x -> do
    annotate $ "Expected a parse to fail, but instead it returned " ++ show x
    annotate $ "Expected parse error: " ++ (show v)
    --    annotate $ "Prettified expected error: " ++ (concatMap (diagnosticToPrettyString . diagnose @DiagnosticMessageType) v)
    Hedgehog.failure

-- | Create an expectation by saying that the parse should fail.
--
-- > parse letterChar "" "x" `shouldParse` 'x'
shouldn'tParse ::
  ( HasCallStack,
    Show a,
    Eq a
  ) =>
  -- | Result of parsing as returned by function like 'parse'
  Either (ParseErrorBundle DiagnosticMessageType ParsingError) a ->
  -- | Desired result
  () ->
  Test ()
r `shouldn'tParse` () = case r of
  Left e -> do
    success
  Right x -> do
    annotate $ "Expected a parse to fail, but instead it returned " ++ show x
    Hedgehog.failure

-- | Render a parse error bundle in a way that is suitable for inserting it                                                 │    │
-- in a test suite report.
-- From `hspec-megaparseec`
showBundle ::
  ( ShowErrorComponent e,
    Stream s,
    VisualStream s,
    TraversableStream s
  ) =>
  ParseErrorBundle s e ->
  String
showBundle = unlines . fmap indent . lines . errorBundlePretty
  where
    indent x =
      if Prelude.null x
        then x
        else "  " ++ x

objectRespectsAlphaEquivalence :: (Eq object, Show object, HasVariables object, AlphaEquivalence object) => (info, object) -> IO [TestTree]
objectRespectsAlphaEquivalence (_, object) = do
  newObject <- atomically $ runIDGenSTMGlobal (freshenVariableIDs object)

  pure
    [ testGroup
        "Respects α-equivalence"
        [ testProperty "With freshly generated IDs" (withTests 1 $ property $ Hedgehog.diff object (=@=) newObject),
          testProperty "With term-local IDs" (withTests 1 $ property $ Hedgehog.diff object (=@=) (makeVariablesTermLocal object))
        ]
    ]

makeParsingTests :: (HasCallStack, Eq info, Show info, Read info) => GlobPattern -> TestName -> ((info, object) -> IO [TestTree]) -> Parser (MinimalParserEffects Agent) (info, object) -> IO ([CUBE], TestTree)
makeParsingTests rn groupName extraTests parser = do
  succeedDir <- getDataFileName "test/tests/golden/succeed"
  failDir <- getDataFileName "test/tests/golden/fail"
  let succeedCube = succeedSugarCube [succeedDir]
      failCube = failSugarCube [failDir]
  succeedSweets <- findSugar succeedCube
  failSweets <- findSugar failCube
  succeedTests <- withSugarGroups succeedSweets testGroup (mkTest True)
  failTests <- withSugarGroups failSweets testGroup (mkTest False)
  pure $ ([succeedCube, failCube], testGroup groupName [testGroup "Golden tests" [testGroup "Succeed tests" succeedTests, testGroup "Fail tests" failTests]])
  where
    mkTest :: Bool -> Sweets -> Natural -> Expectation -> IO [TestTree]
    mkTest itShouldParse s n e = do
      -- let Just filename = lookup "inputs" $ associated e
      input <- readFile' $ rootFile s

      let params = HMS.fromList (expParamsMatch e)
          -- itShouldParse = True -- (traceShowId $ params ^. at "parses") == (Just NotSpecified)
          whenParsing = theParsing (rootMatchName s) (fromString input)
      if itShouldParse
        then do
          let shouldParseTo = shouldParseTo' (rootFile s) input
          exp <- fmap read $ readFile' $ expectedFile e
          extras <- case actualParse (rootMatchName s) (fromString input) of
            Right a -> extraTests a
            Left _ -> pure []
          pure
            ([testProperty "Should parse" (withTests 1 $ property $ 
                                             liftTest (whenParsing `shouldParseTo` exp)
                                             
                                          )] ++ extras)
        else do
          exp <- readFile' $ expectedFile e
          pure [testProperty "Should fail to parse" (withTests 1 $ property $ liftTest (whenParsing `shouldFailToParseWith` (rootMatchName s, input, exp)))]

    actualParse name input =  do
        Util.parseMinimal
          parser
          name
          input
    theParsing name input =
      fmap fst (actualParse name input)
    succeedSugarCube :: [FilePath] -> CUBE
    succeedSugarCube dataDirs=
      mkCUBE
        { inputDirs = dataDirs,
          rootName = rn,
          associatedNames = [("inputs", "")],
          expectedSuffix = "expected"
        }
    failSugarCube :: [FilePath] -> CUBE
    failSugarCube dataDirs=
      mkCUBE
        { inputDirs = dataDirs,
          rootName = rn,
          associatedNames = [("inputs", "")],
          expectedSuffix = "expected"
        }

testLaws :: Laws -> TestTree
testLaws (Laws className classLaws) = fromGroup (Group (fromString className) (fmap (\(axiomName, axiomProp) -> (fromString axiomName, axiomProp)) classLaws))

testManyLaws :: TestName -> Gen a -> [Gen a -> Laws] -> [Laws] -> TestTree
testManyLaws tn gen laws laws' = testGroup tn ((pure testLaws <*> (laws <*> (pure gen))) ++ (fmap testLaws laws'))

logToList :: forall r a. Members '[Transactional] r => Sem (Log ': r) a -> Sem r ([(Dif1.Level, Dif1.Message)], a)
logToList = runOutputList . runDiOutput . raiseUnder -- runOutputList . interpretLogOutput . raiseUnder -- fmap ([], ) .  


type MinimalTestEffects = PrePSCMEffects Agent --Append '[Fixpoint, RuleDatabase, Error RuleNotInRuleDatabase] (MinimalParserEffects)





-- runMin ::  Agent  -> Sem (PrePSCMEffects Agent) x -> IO ( [(Dif1.Level, Dif1.Message)],x)
-- runMin agent s =  do

  
  
--   (msgs, res') <- postLogInterpreter . logToList . preLogInterpreter env $ s--runIDGen . handleOIDAndWM .   errorToIOFinal . runRuleDatabase ruledb . fixpointToFinal $ s
--   let res = case res' of
--               Left x -> Prelude.error $ show x
--               Right y -> y
--   pure (msgs,res)


runAutoTest :: HasCallStack => Agent -> Sem (PT.Test ': (Fail ': (Error PT.TestError ': (PT.Hedgehog IO ': (Error Failure ':  (PostLogInfrastructureEffectsIO)))))) a -> TestT IO a 
runAutoTest agent = PT.runTestAutoWith (runPostLogInfrastructureIO mempty agent)


lawsAsTestTree ::
    Laws
  -> TestTree
lawsAsTestTree Laws{..} = testGroup lawsTypeClass (fmap  (uncurry testProperty) lawsProperties)                    


runPSCMTest :: Maybe OTC.Context -> Text -> (Agent -> Sem (PSCMEffectStack Agent)  a) -> IO (a, Tracer)
runPSCMTest ctx testName theTest = withFreshAgentIO ctx \agent -> OT.inSpan'  (agent^.tracerL) testName OT.defaultSpanArguments $ \span -> do
    (,) <$> (runPSCM' (Just $ OTC.insertSpan span mempty) False agent (theTest agent)) <*> (pure $ agent ^. tracerL)
