{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE TupleSections #-}
{-# OPTIONS_GHC -foptimal-applicative-do -Wwarn=unused-matches -Wwarn=unused-local-binds -Wwarn=type-defaults -Wwarn=orphans #-}

module Gen.Patterns where

import Algebra.Lattice
import Control.Lens hiding (Identity (..))
import Control.Monad
import Core.CoreTypes as CT
import Core.Identifiers
import Core.Identifiers.IdentifierType
import Core.Patterns.PatternSyntax
import Core.Variable
import Core.Variable.Substitution
import Data.Bifunctor
import Data.Interned
import Data.Sequence as Seq hiding ((:<))
import Data.String (IsString (..))
import Data.Word
import Gen
import Hedgehog
import Hedgehog.Gen as Gen
import Hedgehog.Range as Range
import QuickSpec
import Test.QuickCheck (Arbitrary (..))
import qualified Test.QuickCheck.Hedgehog as H2QC

patternVariable :: (MonadGen m) => m PatternVariable
patternVariable = do
  v <- Gen.variable
  Gen.Patterns.patternVariableWith v

patternVariableWith :: (MonadGen m) => Variable -> m PatternVariable
patternVariableWith v = Gen.frequency [(20, pure (PlainPatternVariable v)), (9, pure (StatePatternVariable v))]

constantPatternValue :: (MonadGen m) => m ConstantPatternValue
constantPatternValue = choice [fmap PatternSymbol Gen.internedText, fmap PatternConstant Gen.constantValue]

comparisonTest :: (MonadGen m) => m v -> m (Substitution v, ComparisonTest)
comparisonTest gv = do
  (!sub, !val) <-
    Gen.choice
      [ ( do
            val' <- Constant <$> Gen.constantValue
            pure (emptySubstitution, CompareAgainstValue val')
        ),
        ( do
            val1 <- gv
            var1 <- Gen.variable
            val2 <- gv
            var2 <- Gen.variable
            let sub = constructSubstitution [(var1, val1), (var2, val2)]
            firstOrSecond <- Gen.prune Gen.bool
            pure (sub, CompareAgainstVariable (if firstOrSecond then var1 else var2)) -- It really doesn't matter whether var1 or var2, but just in case there's something wrong with substitution lookups...
        )
      ]
  !cons <- Gen.element [EqualTo, LessThan, LessThanOrEqualTo, GreaterThan, GreaterThanOrEqualTo, NotEqualTo]
  pure $! (sub, cons val)

comparisonTests :: (BoundedMeetSemiLattice (SubstitutionLattice v), MonadGen m) => m v -> m (SubstitutionLattice v, Seq ComparisonTest)
comparisonTests gv = do
  tests <- Gen.seq (Range.linear 1 5) (Gen.Patterns.comparisonTest gv)
  let (condensed', seqs) = tests ^. folded . to (\(a, b) -> (Meet (a ^. re __Substitution), Seq.singleton b))
  pure $! (getMeet $! condensed', seqs)

comparisonPredicate :: (BoundedMeetSemiLattice (SubstitutionLattice v), MonadGen m) => m v -> m (SubstitutionLattice v, PatternPredicate)
comparisonPredicate gv = do
  (!sub, !res) <- Gen.Patterns.comparisonTests gv
  pure $! (sub, AComparisonTest res)

patternCondition :: forall m v. (BoundedMeetSemiLattice (SubstitutionLattice v), MonadGen m) => m v -> m (SubstitutionLattice v, PatternCondition)
patternCondition gv = choice [dc, pv, c]
  where
    dc = Gen.constant (emptySubstitution, DontCareCondition)
    c = do
      v <- Gen.Patterns.constantPatternValue
      pure (emptySubstitution, AConstantEqualityTest v)
    pv = do
      includePredicate <- Gen.bool
      (!sub, !pred) <-
        if includePredicate
          then do
            (sub, pred) <- Gen.Patterns.comparisonPredicate gv
            pure (sub, Just pred)
          else pure (emptySubstitution, Nothing)
      pv' <- PlainPatternVariable <$> Gen.variable -- patternVariable
      pure $! (sub, APatternVariable pv' pred)

wmPrefPattern :: forall m. (MonadGen m) => [Variable] -> m WMPreferencePattern
wmPrefPattern vars = Gen.choice [pure Don'tCareAboutPreference, pure MustHaveAPreference,  pure MustNotHavePreference, WithAPreference <$> (Gen.wmPreference vars)]
  
wmePattern :: forall m. (MonadGen m) => m Variable -> [Variable] -> m WMEPattern
wmePattern gv vars = addExtra [WMEPattern DontCareCondition DontCareCondition DontCareCondition Don'tCareAboutPreference] (WMEPattern <$!> (snd <$!> patternCondition') <*> (snd <$!> patternCondition') <*> (snd <$!> patternCondition') <*> (wmPrefPattern vars))
  where
    patternCondition' = fmap (first latticeToVariableRenaming) (Gen.Patterns.patternCondition gv) :: m (VariableRenaming, PatternCondition)

instance Arbitrary WMEPattern where
  arbitrary = H2QC.hedgehog $ do
    vars <- Gen.list (Range.exponentialFrom 0 1 10) Gen.variable
    wmePattern Gen.variable vars

generateAttributePattern :: forall m. (MonadGen m) => Identifier -> Variable -> SymbolicValue -> m (Variable, SubstitutionLattice Value, PatternCondition)
generateAttributePattern idVariableID objRawVar a = do
  (attrConstantPredicate :: (SubstitutionLattice Value, PatternPredicate)) <- Gen.Patterns.comparisonPredicate (fmap (Symbolic) Gen.symbolicValue)
  (attrPred) <- Gen.frequency [(3, pure Nothing), (1, pure . Just $ attrConstantPredicate)]
  --  attrPred :: m (Maybe PatternPredicate)
  let !(attrSymTest :: [(SubstitutionLattice Value, PatternCondition)]) = fmap ((trivialSubstitution,) . AConstantEqualityTest . PatternSymbol) $ a ^.. _Symbol
  attrVariable <-
    Gen.filterT (\attrV -> attrV ^. variableID /= idVariableID && attrV /= objRawVar) Gen.variable
  --       [ (3, Gen.filterT (\attrV -> idVariableID /= attrV ^. variableID) Gen.variable), -- Don't generate operators which are defined in terms of themselves?
  --         (2, Gen.variable)
  -- --        (1, pure (objectVariable ^. patternVariableVariable)) -- TODO Re-enable me when intra-pattern inconsistency is dealt with
  --       ]
--  (attrConstantValPattern :: (SubstitutionLattice Value, PatternCondition)) <- Gen.filterT (\(_, c) -> hasn't (constantPattern . _PatternConstant) c) $ Gen.Patterns.patternCondition (fmap Symbolic Gen.genSymbolicValue)
  ((attrSub :: SubstitutionLattice Value), (attrCondition :: PatternCondition)) <-
    Gen.element
      ( attrSymTest
          ++ [ ( let (!sub, !pred) = case attrPred of
                       Nothing -> (trivialSubstitution, Nothing)
                       Just (s, p) -> (s, Just p)
                  in (sub, APatternVariable (PlainPatternVariable attrVariable) pred)
               )--,
  --             attrConstantValPattern
             ]
      )
  pure (attrVariable, attrSub, attrCondition)


       
{-# INLINEABLE mkWMEAndCompatiblePattern #-}
mkWMEAndCompatiblePattern :: forall m a. (HasMaybeWMPreference a ObjectIdentifier, WMEIsState a, MonadGen m) => m a -> m (WME' a, SubstitutionLattice Value, WMEPattern)
mkWMEAndCompatiblePattern genMetadata = do
  w@(WME !i !(a) !v !_ !_) <- Gen.randomWME genMetadata
  idPred <- frequency [(5, pure Nothing)] ---, (1, Gen.maybe mkComparisonPredicate)]
  --  idPred :: m (Maybe PatternPredicate)
  idVariableID <- Gen.identifierForPatternVariables
  objRawVar <- Gen.variable
  objectVariable <- Gen.Patterns.patternVariableWith objRawVar
  idCondition <- Gen.frequency [(1, pure DontCareCondition), (4, pure (APatternVariable objectVariable idPred)) ]

  -- attribute pattern
  (attrVariable, attrSub :: SubstitutionLattice Value, attrCondition :: PatternCondition) <- generateAttributePattern idVariableID objRawVar a

  let valConditionEqualityConstructor =
        if has _ConstantValue v
          then Just $ PatternConstant (v ^?! _ConstantValue)
          else
            if has _Symbol v
              then Just $ PatternSymbol (v ^?! _Symbol)
              else Nothing
      valConstantEqualityTest = fmap (pure . (trivialSubstitution,) . AConstantEqualityTest) $ valConditionEqualityConstructor ^.. folded
  valVar <- Gen.filterT (\valV -> valV ^. variableID /= idVariableID && valV /= objRawVar && valV /= attrVariable) Gen.variable
  (valSub :: SubstitutionLattice Value, valCondition) <-
    choice
      ( valConstantEqualityTest
          ++ [ pure (emptySubstitution, DontCareCondition),
               patternCondition (fmap Constant Gen.constantValue),
               pure $ (Core.Variable.Substitution.singleton valVar v, APatternVariable (PlainPatternVariable valVar) Nothing)
             ]
      )
  !dm1 <- newDoesntMatch 1
  !dm2 <- newDoesntMatch 2
  !dm3 <- newDoesntMatch 3
  let !sub = mergeSubstitutions [attrSub, valSub]
  !wmPref <- if (has _StatePatternVariable objectVariable) then pure MustNotHavePreference else wmPrefPattern [] -- TODO: Include known identifiers
  !simple <- simpleCombo
  res@(!x, !y, !z) <-
    addExtra [simple] $
      Gen.frequency
        [ (4, pure $! (w, sub, WMEPattern idCondition attrCondition valCondition wmPref)),
          (1, pure $! (w & CT.value .~ Symbolic (w ^. attribute), sub, WMEPattern dm1 dm2 dm2 wmPref)),
          (1, pure $! (w & CT.value .~ Symbolic (w ^. attribute), sub, WMEPattern dm1 dm2 dm3 wmPref))
        ]
  pure $! res
  where
    newDoesntMatch :: Word64 -> m PatternCondition
    newDoesntMatch n = do
      doesntMatchId <- addExtra [arbitraryTestIdentifier] $ Gen.prune $ Gen.globalIdentifierGiven (123 * n) (456 + n)
      let name = intern $ fromString $ "nope!" ++ (show n)
      pure $! (APatternVariable (PlainPatternVariable (AVariable doesntMatchId name)) Nothing)
    simpleCombo = do
      metadata <- genMetadata
      o <- (Gen.objectIdentifier)
      let !wme = WME o "b" "c" (WMEIdentifier arbitraryTestIdentifier) metadata
          !sub = trivialSubstitution :: SubstitutionLattice Value
          !pat = WMEPattern DontCareCondition DontCareCondition DontCareCondition Don'tCareAboutPreference
      pure $! (wme, sub, pat)

subGenFromPattern :: Gen (SubstitutionLattice Value)
subGenFromPattern = do
  (_, sub, _) <- (mkWMEAndCompatiblePattern Gen.metadataGen)
  pure (sub)
