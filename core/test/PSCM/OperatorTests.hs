{-# LANGUAGE QuasiQuotes #-}

module PSCM.OperatorTests where

import Math.ParetoFront    
import Agent
import Agent.Run.LowLevelInfrastructure
import Control.Monad.IO.Class
import Core.Identifiers
import Core.Identifiers.KnownIdentifiers (topLevelStateIdentifier)
import Core.Utils.Rendering.CoreDoc
import Data.Foldable
import DiPolysemy
import Gen
import Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Model.PSCM.Impasses    
import PSCM.Operators
import PSCM.States (Impasse)
import Polysemy.Input
import Polysemy.Reader    
import Test.Tasty
import Test.Tasty.Hedgehog
import Util
import PSCM.Operators.OperatorProposals
operatorTests :: TestTree
operatorTests =
  testGroup
    "Operators"
    [ testGroup
        "Evaluation"
        [simpleOperatorEvaluationTests]
    ]


  
simpleOperatorEvaluationTests :: TestTree
simpleOperatorEvaluationTests = testGroup "Simple cases" [singleRequired, multipleRequired, singleRequiredButProhibited, multipleRequiredButProhibited, twoAcceptableOnePreference, twoAcceptableOneBestOrWorst]

required', prohibited', acceptable', reject', best', worst' :: ObjectIdentifier -> OperatorProposal metadata
required' o = OperatorProposal o Required Nothing
prohibited' o = OperatorProposal o Prohibit Nothing
acceptable' o = OperatorProposal o Acceptable Nothing
reject' o = OperatorProposal o Reject Nothing
best' o = OperatorProposal o Best Nothing
worst' o = OperatorProposal o Worst Nothing
indifferent' :: Maybe Double -> ObjectIdentifier -> OperatorProposal metadata           
indifferent' numeric o = OperatorProposal o (Indifferent numeric) Nothing

required, prohibited, acceptable, reject, best, worst, indifferent :: (MonadGen m) => m (OperatorProposals metadata)
required = proposeOperator <$> (required' <$> Gen.objectIdentifier)
prohibited = proposeOperator <$> (prohibited' <$> Gen.objectIdentifier)
acceptable = proposeOperator <$> (acceptable' <$> Gen.objectIdentifier)
reject = proposeOperator <$> (reject' <$> Gen.objectIdentifier)
best = proposeOperator <$> (best' <$> Gen.objectIdentifier)
worst = proposeOperator <$> (worst' <$> Gen.objectIdentifier)
indifferent = do
  indiff <- Gen.maybe (Gen.double (Range.exponentialFloatFrom 0 (-10) 10))
  proposeOperator <$> (indifferent' indiff <$> Gen.objectIdentifier)

acceptables, requireds, prohibiteds, rejects, bests, worsts, indifferents :: (MonadGen m) => m (OperatorProposals metadata)
acceptables = fold <$> Gen.list (Range.linearFrom 0 1 15) acceptable
requireds = fold <$> Gen.list (Range.linearFrom 0 1 15) required
prohibiteds = fold <$> Gen.list (Range.linearFrom 0 1 15) prohibited
rejects = fold <$> Gen.list (Range.linearFrom 0 1 15) reject
bests = fold <$> Gen.list (Range.linearFrom 0 1 15) best
worsts = fold <$> Gen.list (Range.linearFrom 0 1 15) worst
indifferents = fold <$> Gen.list (Range.linearFrom 0 1 15) indifferent

acceptablesAndRejects :: (MonadGen m) => m (OperatorProposals metadata)
acceptablesAndRejects = do
  accs <- acceptables
  rejs <- rejects
  pure (accs <> rejs)

acceptablesRejectsIndifferents :: (MonadGen m) => m (OperatorProposals metadata)
acceptablesRejectsIndifferents = do
  accs <- acceptablesAndRejects
  indiffs <- indifferents
  pure (accs <> indiffs)

-- bettersAndWorses :: MonadGen m => m OperatorProposals
-- bettersAndWorses = do
--   bs <- betters
--   ws <- worses

acceptableAndLowers :: (MonadGen m) => m (OperatorProposals metadata)
acceptableAndLowers = do
  accs <- acceptablesRejectsIndifferents
  bs <- bests
  ws <- worsts
  pure (accs <> bs <> ws)

guaranteedConstraintFailure :: (MonadGen m) => m (OperatorProposals metadata)
guaranteedConstraintFailure = do
  oid <- Gen.objectIdentifier
  pure ((proposeOperator . required' $ oid) <> (proposeOperator . prohibited' $ oid))

runEvalOperators :: (MonadIO t, MonadTest t) => OperatorProposals WMEMetadata -> t (Either (Impasse WMEMetadata) (ChosenOperator WMEMetadata))
runEvalOperators proposals = evalIO $ withFreshAgentIO Nothing \agent -> do
  runLowLevelEffects Nothing agent $ runDiNoop $ runInputConst topLevelStateIdentifier $ runReader agent $ evaluateOperators proposals

singleRequired :: TestTree
singleRequired = testProperty "Single required operator" $ property do
  oid <- forAll Gen.objectIdentifier
  others <- forAll acceptableAndLowers
  let proposals = proposeOperator (required' oid) <> others
  annotateShow proposals
  results <- runEvalOperators proposals
  removeMetadata ((Right (ChosenOperator oid proposals topLevelStateIdentifier))) === removeMetadata results

singleRequiredButProhibited :: TestTree
singleRequiredButProhibited = testProperty "Single required-but-prohibited operator" $ property do
  baddies <- forAll guaranteedConstraintFailure
  others' <- forAll acceptableAndLowers
  others'' <- forAll prohibiteds
  let proposals = others' <> others'' <> baddies
  results <- runEvalOperators proposals
  removeMetadata (Left (ConstraintFailure (ConstraintFailureImpasse baddies))) === removeMetadata results

multipleRequired :: TestTree
multipleRequired = testProperty "Multiple required operators" $ property do
  oid1 <- forAll Gen.objectIdentifier
  oid2 <- forAll $ Gen.filterT (/= oid1) Gen.objectIdentifier
  others <- forAll acceptableAndLowers
  let conflicts = proposeOperator (required' oid1) <> proposeOperator (required' oid2)
  let proposals = others <> conflicts
  results <- runEvalOperators proposals
  removeMetadata (Left (ConstraintFailure (ConstraintFailureImpasse (conflicts)))) === removeMetadata results

-- testProposals :: MonadTest m => OperatorProposals WMEMetadata -> OperatorProposals WMEMetadata -> m ()
-- testProposals (OperatorProposals a) (OperatorProposals b) = do
--   (nestedFold ((fmap (const ())) . HS.singleton) HS.singleton a) === (nestedFold ((fmap (const ())) . HS.singleton) HS.singleton b)

removeMetadata' :: OperatorProposals WMEMetadata -> OperatorProposals ()
removeMetadata' a = a & allProposals %~ (nestedFold (stratum . (fmap (const ())) ) id )

removeMetadata :: Either (Impasse WMEMetadata) (ChosenOperator WMEMetadata) -> Either (Impasse ()) (ChosenOperator ())
removeMetadata (Left a) = Left (fmap removeMetadata' a)
removeMetadata (Right (ChosenOperator winner props tlsi)) = Right (ChosenOperator winner (removeMetadata' props) tlsi)
                   
multipleRequiredButProhibited :: TestTree
multipleRequiredButProhibited = testProperty "Multiple required-but-prohibited operators" $ property do
  baddies <- forAll (fold <$> (Gen.list (Range.linearFrom 2 10 20) guaranteedConstraintFailure))
  others <- forAll acceptableAndLowers
  let proposals = others <> baddies
  results <- runEvalOperators proposals
  (removeMetadata (Left (ConstraintFailure (ConstraintFailureImpasse baddies)))) === (removeMetadata results)

twoAcceptableOnePreference :: TestTree
twoAcceptableOnePreference = testProperty "Two acceptable candidates with one better/worse preference" $ property do
  first <- forAll $ acceptable' <$> Gen.objectIdentifier
  second <- forAll $ Gen.filterT (\a -> a ^. proposedOperator /= first ^. proposedOperator) $ acceptable' <$> Gen.objectIdentifier
  let betterPreference1 = OperatorProposal (first ^. proposedOperator) (PreferableOverOther (second ^. proposedOperator)) Nothing
      betterPreference2 = OperatorProposal (second ^. proposedOperator) (PreferableOverOther (first ^. proposedOperator)) Nothing
      worsePreference1 = OperatorProposal (first ^. proposedOperator) (OtherIsPreferable (second ^. proposedOperator)) Nothing
      worsePreference2 = OperatorProposal (second ^. proposedOperator) (OtherIsPreferable (first ^. proposedOperator)) Nothing
  (winner, pref) <- forAll $ Gen.element [(first ^. proposedOperator, betterPreference1), (second ^. proposedOperator, betterPreference2), (second ^. proposedOperator, worsePreference1), (first ^. proposedOperator, worsePreference2)] -- in case of ordering issues
  indiffs <- forAll indifferents
  bs <- forAll bests
  ws <- forAll worsts
  let proposals = ((proposeOperator first) <> (proposeOperator second) <> (proposeOperator pref)) <> indiffs <> bs <> ws
  annotateShow (render proposals)
  results <- runEvalOperators proposals
  removeMetadata (Right (ChosenOperator winner proposals topLevelStateIdentifier)) === removeMetadata results--(results & _Right . fromProposals %~ removeMetadata) --Right (ChosenOperator w' (removeMetadata p') tlsi)

twoAcceptableOneBestOrWorst :: TestTree
twoAcceptableOneBestOrWorst = testProperty "Two acceptable candidates with one best/worst preference" $ property do
  first <- forAll $ acceptable' <$> Gen.objectIdentifier
  second <- forAll $ Gen.filterT (\a -> a ^. proposedOperator /= first ^. proposedOperator) $ acceptable' <$> Gen.objectIdentifier
  let bestPreference1 = OperatorProposal (first ^. proposedOperator) Best Nothing
      bestPreference2 = OperatorProposal (second ^. proposedOperator) Best Nothing
      worstPreference1 = OperatorProposal (first ^. proposedOperator) Worst Nothing
      worstPreference2 = OperatorProposal (second ^. proposedOperator) Worst Nothing
  (winner, pref) <- forAll $ Gen.element [(first ^. proposedOperator, bestPreference1), (second ^. proposedOperator, bestPreference2), (second ^. proposedOperator, worstPreference1), (first ^. proposedOperator, worstPreference2)] -- in case of ordering issues
  indiffs <- forAll indifferents
  let proposals = ((proposeOperator first) <> (proposeOperator second) <> (proposeOperator pref)) <> indiffs
  annotateShow (render proposals)
  results <- runEvalOperators proposals
  removeMetadata (Right (ChosenOperator winner proposals topLevelStateIdentifier)) === removeMetadata results

-- testStrata :: Strata OperatorProposal
-- testStrata = stratum (OperatorProposal topLevelStateObjectID Required) <> stratum (OperatorProposal topLevelStateObjectID Prohibit) <> stratum (OperatorProposal noOpOperatorIdentifier Acceptable) <> stratum (OperatorProposal noOpOperatorIdentifier Reject) <> stratum (OperatorProposal noOpOperatorIdentifier (PreferableOverOther topLevelStateObjectID)) <> stratum (OperatorProposal topLevelStateObjectID Acceptable) <> stratum (OperatorProposal topLevelStateObjectID (PreferableOverOther noOpOperatorIdentifier)) <> stratum (OperatorProposal noOpOperatorIdentifier Required) <> stratum (OperatorProposal noOpOperatorIdentifier Prohibit) <> stratum (OperatorProposal topLevelStateObjectID Reject) <> stratum (OperatorProposal (ObjectIdentifier "foo" arbitraryTestIdentifier) Acceptable) <> stratum (OperatorProposal (ObjectIdentifier "lonesome" [ident|lolol|]) Best) <> stratum (OperatorProposal noOpOperatorIdentifier Best) <> stratum (OperatorProposal topLevelStateObjectID Best) <> stratum (OperatorProposal topLevelStateObjectID Worst) <> stratum (OperatorProposal (ObjectIdentifier "lonesome" [ident|lolol|]) Acceptable)
