{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wwarn=missing-signatures -Wwarn=unused-foralls -Wwarn=unused-imports#-}

module Main where
import QuickSpec
import Agent
import Agent.Run.Initialisation
--import Test.Tasty.CoverageReporter
import Agent.Run.LowLevelInfrastructure
-- import OpenTelemetry.Trace.Core (createSpan, endSpan, defaultSpanArguments, getSpanContext, kind, SpanKind(..))

-- import Polysemy.Test hiding ((===))

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Resource
import Core.CoreTypes
import Core.Identifiers
import Core.Identifiers.IdentifierGeneratorTests
import Core.PatternTests
import Core.RuleTests
import Core.Variables
import Core.WME
import Core.WMETests
import qualified Data.HashSet as HS
--import Data.Typeable
import GCTests
import Gen
import qualified Hedgehog
import Hedgehog.Gen as Gen
import Hedgehog.Internal.Property (forAllT)
import Hedgehog.Range as Range
import Meta.Model
import qualified OpenTelemetry.Context.ThreadLocal as OTCT
import OpenTelemetry.Trace
import OpenTelemetry.Trace.Core (getSpanContext, forceFlushTracerProvider)
import PSCMTests
import Prettyprinter
import Rete.ReteNodeTests
import Rete.ReteTypes
import Rete.TokenTests
import Rete.Types.Tokens
import System.Directory
import Test.Tasty
import Test.Tasty.Hedgehog
import Test.Tasty.Ingredients
import Test.Tasty.Runners.AntXML
import Test.Tasty.Runners.Html
import Test.Tasty.Stats
import Test.Tasty.Sugar
import Util

manualManipulationStateMachineCommands ::
  forall a gen m.
  ( a ~ WMEMetadata,
    HasMaybeWMPreference a ObjectIdentifier,
    WMEInAlphaNodes (WMEToken' a) a (WME' a),
    HasMemoryBlock (WMEToken' a) (WMEToken' a) a,
    HasWMETokens a a,
    Show (TokenAddition a),
    Show a,
    WMEIsState a,
    Pretty a,
    MonadTest m,
    MonadIO m,
    Typeable a,
    MonadGen gen
  ) =>
  [Callback (AddWMEs a) (HS.HashSet (RuleActivation), HS.HashSet (RuleRetraction)) (ModelState a)] ->
  [Callback (AddRules a) (HS.HashSet (ReteNode a), HS.HashSet (RuleActivation), HS.HashSet (RuleRetraction)) (ModelState a)] ->
  Gens (ModelState a) gen a ->
  Agent ->
  [Command gen m (ModelState a)]
manualManipulationStateMachineCommands verifyWMEActions verifyRuleActions gens agent' = {-idGenCommands ++-} {-(reteNodeCommands gens agent') ++-} (allReteTokenCommands gens agent')

myGens :: (HasReteGraph MyMetadata state, MonadGen gen, WMEIsState MyMetadata) => Gens state gen MyMetadata
myGens = Gens (const myMetadataGen)

verifyWMEActions = []

verifyRuleActions = []

outputsFirings :: Callback (AddWMEs WMEMetadata) (HS.HashSet (RuleActivation), HS.HashSet (RuleRetraction)) state
outputsFirings = Ensure $ \_ _ _ (out, _) -> do
  when (HS.size (out) > 0) $ do
    annotateShow out
    error (show out)
    failure

{-# INLINE myMetadataGen #-}
myMetadataGen :: (MonadGen gen, WMEIsState MyMetadata, HasMaybeWMPreference (MyMetadata) ObjectIdentifier) => gen MyMetadata
myMetadataGen = Gen.metadataGen

--  let toks = unsafePerformIO STMS.newIO
--      alphas = unsafePerformIO STMS.newIO

allManualManipulationStateMachineCommands ::
  forall a gen m state.
  ( a ~ WMEMetadata,
    WMEInAlphaNodes (WMEToken' a) a (WME' a),
    HasMaybeWMPreference a ObjectIdentifier,
    HasMemoryBlock (WMEToken' a) (WMEToken' a) a,
    HasWMETokens a a,
    Show a,
    WMEIsState a,
    Pretty a,
    MonadTest m,
    MonadIO m,
    Typeable a,
    MonadGen gen
  ) =>
  [Callback (AddWMEs a) (HS.HashSet (RuleActivation), HS.HashSet (RuleRetraction)) (ModelState a)] ->
  [Callback (AddRules a) (HS.HashSet (ReteNode a), HS.HashSet (RuleActivation), HS.HashSet (RuleRetraction)) (ModelState a)] ->
  Gens (ModelState a) gen a ->
  Agent ->
  [Command gen m (ModelState a)]
allManualManipulationStateMachineCommands verifyWMEActions verifyRuleActions gens agent' = idGenCommands ++ {-(allReteNodeCommands @a gens agent') ++-} (allReteTokenCommands @a gens agent')

manualManipulationSequentialStateMachineTest :: (HasMemoryBlock' (WMEToken' MyMetadata) MyMetadata) => Property
manualManipulationSequentialStateMachineTest = withShrinks 10000 . withTests 500 $ property $ do
  Right agent' <- evalIO $ getNewAgentData Nothing
  telemContext <- OTCT.getContext
  --  tracer <- evalIO cosmoThoughtCoreTracer
  span <- evalIO $ runResourceT $ createTestSpan agent' telemContext "manualManipulationSequentialStateMachineTest" defaultSpanArguments
  spanCtx <- getSpanContext span

  sequentialActions <-
    forAll $
      Gen.sequential
        (Range.exponentialFrom 1 1 50)
        (initialState spanCtx agent')
        (allManualManipulationStateMachineCommands @MyMetadata verifyWMEActions verifyRuleActions myGens agent')
  Hedgehog.annotateShow sequentialActions
  -- Right agent' <- evalIO getNewAgentData
  -- evalIO $ resetAgentData agent'
  executeSequential (initialState spanCtx agent') sequentialActions

manualManipulationParallelStateMachineTest :: (HasMemoryBlock' (WMEToken' MyMetadata) MyMetadata) => Property
manualManipulationParallelStateMachineTest = withShrinks 10000 . withRetries 4 . withTests 200 . property $ -- withFreshContext'
  do
    Right agent' <- evalIO $ getNewAgentData Nothing
    telemContext <- OTCT.getContext
    span <- evalIO $ runResourceT $ createTestSpan agent' telemContext "manualManipulationParallelStateMachineTest" defaultSpanArguments
    spanCtx <- getSpanContext span

    parallelActions <-
      forAll $
        Gen.parallel
          (Range.exponentialFrom 1 1 10)
          (Range.exponentialFrom 1 1 15)
          (initialState spanCtx agent')
          (manualManipulationStateMachineCommands @MyMetadata verifyWMEActions verifyRuleActions myGens agent')

    Hedgehog.annotateShow parallelActions

    -- evalIO $ resetAgentData agent'
    --  Right agent' <- evalIO getNewAgentData
    executeParallel (initialState spanCtx agent') parallelActions

reteEffectStateMachineCommands ::
  forall a gen m.
  ( a ~ WMEMetadata,
    HasMaybeWMPreference a ObjectIdentifier,
    WMEInAlphaNodes (WMEToken' a) a (WME' a),
    HasMemoryBlock (WMEToken' a) (WMEToken' a) a,
    HasWMETokens a a,
    Show (TokenAddition a),
    Show a,
    MonadIO gen,
    WMEIsState a,
    Pretty a,
    MonadTest m,
    MonadIO m,
    Typeable a,
    MonadGen gen
  ) =>
  [Callback (AddWMEs a) (HS.HashSet (RuleActivation), HS.HashSet (RuleRetraction)) (ModelState a)] ->
  [Callback (AddRules a) (HS.HashSet (ReteNode a), HS.HashSet (RuleActivation), HS.HashSet (RuleRetraction)) (ModelState a)] ->
  Gens (ModelState a) gen a ->
  Agent ->
  [Command gen m (ModelState a)]
reteEffectStateMachineCommands verifyWMEActions verifyRuleActions gens agent' = {-idGenCommands ++-} (allRuleCommands @a verifyWMEActions verifyRuleActions gens agent')

reteEffectSequentialStateMachineTest :: (HasMemoryBlock' (WMEToken' MyMetadata) MyMetadata) => Property
reteEffectSequentialStateMachineTest = withShrinks 10000 . withTests 200 $ withDiscards 250 $
  property $ -- withFreshContext'
    do
      Right agent' <- evalIO $ getNewAgentData Nothing
      telemContext <- OTCT.getContext
      tracer <- evalIO cosmoThoughtCoreTracer
      span <- evalIO $ runResourceT $ createTestSpan agent' telemContext "reteEffectSequentialMachineTest" defaultSpanArguments
      spanCtx <- getSpanContext span

      sequentialActions <-
        forAllT $
          Gen.sequential
            (Range.exponentialFrom 1 1 50)
            (initialState spanCtx agent')
            (reteEffectStateMachineCommands @MyMetadata verifyWMEActions verifyRuleActions myGens agent')

      Hedgehog.annotateShow sequentialActions

      -- evalIO $ resetAgentData agent'
      Right agent' <- evalIO $ getNewAgentData Nothing
      executeSequential (initialState spanCtx agent') sequentialActions

reteEffectParallelStateMachineTest :: (HasMemoryBlock' (WMEToken' MyMetadata) MyMetadata) => Property
reteEffectParallelStateMachineTest =
  -- verifiedTermination . withConfidence (10 ^ 9) .
  withShrinks 10000 . withRetries 10 . property $ -- withFreshContext'
    do
      Right agent' <- evalIO $ getNewAgentData Nothing
      telemContext <- OTCT.getContext
      tracer <- evalIO cosmoThoughtCoreTracer
      span <- evalIO $ runResourceT $ createTestSpan agent' telemContext "reteEffectParallelMachineTest" defaultSpanArguments
      spanCtx <- getSpanContext span

      parallelActions <-
        forAllT $
          Gen.parallel
            (Range.exponentialFrom 1 1 10)
            (Range.exponentialFrom 1 1 6)
            (initialState spanCtx agent')
            (reteEffectStateMachineCommands @MyMetadata verifyWMEActions verifyRuleActions myGens agent')

      Hedgehog.annotateShow parallelActions

      -- evalIO $ resetAgentData agent'
      Right agent' <- evalIO $ getNewAgentData Nothing
      executeParallel (initialState spanCtx agent') parallelActions

main :: (HasMemoryBlock' (WMEToken' MyMetadata) MyMetadata) => IO ()
main = do
  initializeGlobalTracerProvider
--  quickSpec latticeSig
--  quickSpec subSig
  cacheDir <- getXdgDirectory XdgCache "cosmothought"
  putStrLn $ "Cache dir:  " ++ cacheDir
  (wmeSugarCubes, wmeTests') <- wmeTests Gen.metadataGen
  (ruleSugarCubes, ruleTests') <- ruleTests
  let ingredients =
        htmlRunner
          `composeReporters` antXMLRunner
          `composeReporters` consoleStatsReporter
  defaultMainWithIngredients
    (  includingOptions sugarOptions
        : {-[coverageReporter] <>-} sugarIngredients (wmeSugarCubes <> ruleSugarCubes) <> [ingredients] <> defaultIngredients
    )
    ( testGroup
        "Cosmothought core"
        [ testGroup
            "Core"
            [ idGeneratorTests,
              wmeTests',
              variableTests,
                           substitutionTests,
              ruleTests',
              patternMatchingTests
            ],
          after AllFinish "Core" gcTests,
          after AllFinish "Core" $
            ( testGroup
                "Rete network"
                [ -- testGroup
                  -- "Manual rete graph manipulation"
                  -- [ testProperty "Sequential state machine" manualManipulationSequentialStateMachineTest, --
                  --  testProperty "Parallel state machine" manualManipulationParallelStateMachineTest
                  -- ],
                  testGroup
                    "Directly using ReteEffect"
                    [ testProperty "Sequentially" reteEffectSequentialStateMachineTest,
                      testProperty "In parallel" reteEffectParallelStateMachineTest
                    ]
                ]
            ),
          --                     after AllSucceed "Core"
          pscmTests
        ]
    )
  prov <- getGlobalTracerProvider
  void $! forceFlushTracerProvider prov Nothing
  shutdownTracerProvider prov
