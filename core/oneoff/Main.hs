module Main where

--import Core.Configuration
--import Core.Utils.Rendering.CoreDoc
import Data.Foldable
import Data.String
import ForTesting
import System.Environment
import OpenTelemetry.Trace
import Control.Exception (bracket)
import Control.Monad.Trans.Resource
import qualified OpenTelemetry.Context.ThreadLocal as OTCT
main :: IO ()
main = do
--  initializeGlobalTracerProvider
  telemContext <- OTCT.getContext
  _span <-  runResourceT $ createTestSpan globalAgent telemContext "oneoff" defaultSpanArguments
  args <- getArgs
  bracket (pure ()) (\() -> do
      prov <- getGlobalTracerProvider
      shutdownTracerProvider prov)
                  (\() -> do
         _ <- testCommand "!set log level warning"
         forM_ args \command -> do
            results <- testCommand . fromString $ command
            putStrLn (show results)) 
