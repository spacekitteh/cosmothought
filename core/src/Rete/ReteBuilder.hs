{-# LANGUAGE GADTs #-}
{-# LANGUAGE LinearTypes #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Rete.ReteBuilder
  ( ReteBuilder,
    newEmptyReteNetwork,
    createOrShareAlphaNodes,
    createOrShareMemoryNodes,
    createOrShareNegativeNode,
    runReteBuilder,
    createOrShareJoinNode,
    createProductionNode,
    createOrShareNegatedConjugationNode,
  )
where
--import Data.Tuple (swap)
import Control.Lens.Extras (is)
import Polysemy.Embed

import Rete.Dynamics.Linking
import Rete.Dynamics.TokenFlow
import Rete.Environment
import Rete.ReteTypes
import Rete.Types.Tokens
import Core.Identifiers.IdentifierGenerator
-- import Data.Refined

-- import Numeric.Natural

import PSCM.Effects.WorkingMemory
import Cosmolude
import qualified Data.HashMap.Strict as HMS
import Data.HashSet as HS
import Data.Heap as Heap
import Data.Sequence.Lens
import Polysemy.Fixpoint
import Polysemy.Reader
import Prelude (error, fst)

-- | Builds rete networks. It only creates them;  it doesn't initialise them with WMETokens.
data ReteBuilder metadata m a where
  NewEmptyReteNetwork :: ReteBuilder metadata m ()
  CreateOrShareAlphaNodes :: Traversable t => t WMEPattern -> ReteBuilder metadata m (t ((ReteNode metadata) ? IsAlphaNode metadata, VariableRenaming))
  CreateOrShareMemoryNodes :: Traversable t => {_parents :: t (t (ReteNode metadata))} -> ReteBuilder metadata m (t ((ReteNode metadata) ? IsMemoryNode metadata))
  CreateOrShareJoinNode :: Traversable t => {_joinParents :: t (ReteNode metadata), _rightAlpha :: (ReteNode metadata) ? IsAlphaNode metadata, _joinConditions :: t (JoinCondition)} -> ReteBuilder metadata m ((ReteNode metadata) ? IsJoinNode metadata)
  CreateOrShareNegativeNode :: Traversable t => {_negParents :: t (ReteNode metadata), _rightAlphaForNeg :: (ReteNode metadata) ? IsAlphaNode metadata, _negatedJoinConditions :: t (JoinCondition)} -> ReteBuilder metadata m ((ReteNode metadata) ? IsNegativeNode metadata)
  CreateProductionNode :: Traversable t => {_prodParents :: t (ReteNode metadata), _ruleID :: RuleIdentifier, _accumulatedSubstitution :: Substitution Variable} -> ReteBuilder metadata m ((ReteNode metadata) ? IsProductionNode metadata)
  CreateOrShareNegatedConjugationNode :: Traversable t => {_nccLeftParents :: t (ReteNode metadata), _negatedJoinParents :: t (ReteNode metadata)} -> ReteBuilder metadata m ((ReteNode metadata) ? IsNCCNode metadata)

makeSem ''ReteBuilder

{-# INLINEABLE runReteBuilder #-}
runReteBuilder ::
  forall agent metadata r.
  (HasTracer agent, HasCallStack, HasReteEnvironment agent metadata, Renderable metadata, Typeable metadata, Members '[Assert, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, WorkingMemory' metadata, ReteTokenManipulation metadata, Log, Transactional, Fixpoint, IDGen, Embed IO] r) =>
  InterpreterFor (ReteBuilder metadata) r
runReteBuilder = interpret $ \i -> do
  env <- ask <&> view reteEnvironment
  case i of
    NewEmptyReteNetwork -> push "NewEmptyReteNetwork" do
      embedSTM $ resetReteEnvironment env
    CreateOrShareAlphaNodes t ->

      push "CreateOrShareAlphaNodes" do
        forM @_ @(Sem r) @WMEPattern -- @((ReteNode metadata) ? IsAlphaNode metadata)
          t
          $ \pat -> push "CreateOrShareAlphaNode" $ attr "rete.builder.pattern" (fromString . show . render $ pat) do
            let hashKey = (wmePatternToConstantTestHashKey pat) ^??! _Just -- TODO FIXME when intra-pattern coherency is sorted out
                constantTestNodes = env ^. constantTestAlphaNodes
            addAttributes [("rete.builder.constant_test_hash_key", fromString . show . render $ hashKey)]
            -- TODO check non-constant test nodes and constraints on variables
            existing <- lookupMap hashKey constantTestNodes
            case existing of
              Just existingNode -> push "UseExistingAlphaNode" do
                debug' $ "Found existing node" <+> render existingNode
                addAttributes ((makeAttributesWithRaw "rete.builder.existing_node.match_pattern" (existingNode^.matchPattern)))
                let strippedCurrentPattern = pat & patternObject. predicate .~ Nothing & patternAttribute.predicate .~ Nothing & patternValue . predicate .~ Nothing
                case (existingNode ^. matchPattern) `subsumesVariables` strippedCurrentPattern of
                  Nothing -> assert False "Could not construct variable mapping" (existingNode, pat) ((existingNode, incompatibleSubstitution))
                  Just (varMappingTo, varMappingFrom) ->  do
                                       addAttributeWithRaw "rete.builder.existing_node.variable_mapping.to" varMappingTo
                                       addAttributeWithRaw "rete.builder.existing_node.variable_mapping.from" varMappingFrom
                                       pure (existingNode, varMappingFrom)
              Nothing -> push "CreateNewAlphaNode" do
                ident <- newNodeIdentifier
                (sub', freshenedPat) <- extractSubstitutionFromVariableMappingBy pure (sanitisePatternWithNewVariables pat)
                let sub = sub'^. re __Substitution
                addAttributeWithRaw "rete.builder.new_node.variable_mapping" sub
                let substitutedPat = freshenedPat & patternObject. predicate .~ Nothing & patternAttribute.predicate .~ Nothing & patternValue . predicate .~ Nothing
                node <- do
                  newNode <- makeNewRawAlphaNode ident substitutedPat hashKey
                  addNodeToReteEnvironment (the newNode) env
                  pure newNode
                -- Populate with matches
                wmes' <- allWMEsByHash hashKey
                wmes <- wmes' ^!! _Just . elems
                nodeset <- do
                  s <- newSet
                  s ^! inserting node
                  pure s
                addWMEsToAlphaMemory wmes nodeset
                gcIfDebug
                pure (node, sub)
    CreateOrShareMemoryNodes t ->

      push "CreateOrShareMemoryNodes" do
        forM t $ \parents' -> do
          let parents = relativesCollection parents'
          -- look for existing nodes in parents
          existing <- checkForExistingNode (has (knownCorrectSubType @(ReteNode metadata) @_ @(IsMemoryNode metadata))) parents
          case (existing >>= maybeCorrectSubType' @_) of
            Just child -> pure child
            Nothing -> do
              ident <- newNodeIdentifier
              newNode <- spawnMemoryNode ident parents
              addNodeToReteEnvironment (the newNode) env
              gcIfDebug
              pure newNode
    -- CreateOrShareNegatedConjugationNode parents' partner -> do

    CreateOrShareJoinNode parents' rightAlpha conditions ->

      push "CreateOrShareJoinNode" do
        let parents = relativesCollection parents'
            plainConds = HS.fromList (conditions ^.. folded)

        existing <- checkForExistingNode (\candidate -> has (knownCorrectSubType . (joinConditions @(WMEToken' metadata) @metadata @(IsJoinNode metadata)) . filtered (== plainConds)) candidate) parents -- TODO FIXME is this correct, w.r.t. rightAmem?
        case (existing >>= maybeCorrectSubType') of
          Just child -> push "ShareJoinNode" do
            info' $ "Identical join node already exists:" <+> render (child ^. nodeIdentifier)
            pure child
          Nothing -> push "CreateNewJoinNode" do
            ident <- newNodeIdentifier
            memNodeIdent <- newNodeIdentifier
            let indexedCondsRHS = foldlOf (folded . to \cond -> HMS.singleton (cond ^. currentTokenNode) (HS.singleton cond)) (HMS.unionWith HS.union) HMS.empty conditions
                indexedCondsLHS = foldlOf (folded . to \cond -> case cond^.priorArgument of
                                                               NodeArgument (JoinNodeArgument _ priorTokenNode) -> HMS.singleton  priorTokenNode (HS.singleton cond)
                                                               ValueArgument _ -> HMS.singleton ident (HS.singleton cond)
                                          ) (HMS.unionWith HS.union) HMS.empty conditions
                indexedConds = HMS.unionWith HS.union indexedCondsLHS indexedCondsRHS
            joinNode <- unsafeMakeNewJoinNode ident indexedConds rightAlpha parents
            memNode <- spawnMemoryNode memNodeIdent (relativesCollection [the joinNode])
            -- Set memory node, avoiding fixpoint
            joinNode ^! memoryNode . act (writeT memNode)
            actOnRelatives parents \(Entry _ parent) -> addMetaDataToParentNode parent (the joinNode)
            addNodeToReteEnvironment (the joinNode) env
            gcIfDebug                                     
            pure joinNode
    CreateOrShareNegatedConjugationNode leftParents' partnerParents' ->
      push "CreateOrShareNCCNode" do
        let leftParents = relativesCollection leftParents'
            partnerParents = relativesCollection partnerParents'

        existing' <-
          checkForExistingNode
            ( \candidate ->
                is _NegatedConjugationNode candidate
            )
            leftParents
        let existingNCC = existing' >>= maybeCorrectSubType'
        existingNCCPartnerParents <- existingNCC ^!? _Just . nccPartnerNode . parentNodes
        case existingNCC of
          Just child | existingNCCPartnerParents == Just partnerParents -> push "ShareNCCNode" do
            info' $ "Identical NCC node already exists:" <+> render (child ^. nodeIdentifier)
            pure child
          _ -> push "CreateNewNCCNode" do
            nccIdent <- newNodeIdentifier
            partnerIdent <- newNodeIdentifier
            let branchPoint = leftParents ^??! folded . ignoringLevel . nodeIdentifier
            partnerNode <- _UNSAFEmakeNewNCCPartnerNode partnerIdent branchPoint partnerParents
            nccNode <- _UNSAFEmakeNewNCCNode nccIdent partnerNode leftParents
            partnerNode ^! actualNCCNode . act (writeT nccNode)
            actOnRelatives leftParents \(Entry _ parent) -> addMetaDataToParentNode parent (the nccNode)
            actOnRelatives partnerParents \(Entry _ parent) -> addMetaDataToParentNode parent (the partnerNode)
            addNodeToReteEnvironment (the partnerNode) env
            addNodeToReteEnvironment (the nccNode) env
            gcIfDebug
            pure nccNode
    CreateOrShareNegativeNode parents' rightAlpha conditions ->

      push "CreateOrShareNegativeNode" do
        let parents = relativesCollection parents'
            plainConds = HS.fromList (conditions ^.. folded)
        -- look for existing nodes in parents
        existing <- checkForExistingNode (\candidate -> has (knownCorrectSubType . (joinConditions @(WMEToken' metadata) @metadata @(IsNegativeNode metadata)) . filtered (== plainConds)) candidate) parents
        case (existing >>= maybeCorrectSubType') of
          Just child -> pure child
          Nothing -> do
            ident <- newNodeIdentifier
            let indexedCondsRHS = foldlOf (folded . to \cond -> HMS.singleton (cond ^. currentTokenNode) (HS.singleton cond)) (HMS.unionWith HS.union) HMS.empty conditions
                indexedCondsLHS = foldlOf (folded . to \cond -> case cond^.priorArgument of
                                                               NodeArgument (JoinNodeArgument _ priorTokenNode) -> HMS.singleton  priorTokenNode (HS.singleton cond)
                                                               ValueArgument _ -> HMS.singleton ident (HS.singleton cond)
                                          ) (HMS.unionWith HS.union) HMS.empty conditions                                  

                indexedConds = HMS.unionWith HS.union indexedCondsLHS indexedCondsRHS

            newNode <- makeNewNegativeNode ident indexedConds rightAlpha parents
            addNodeToReteEnvironment (the newNode) env
            gcIfDebug
            pure newNode
    CreateProductionNode parents' ruleID' varMap ->

      push "CreateProductionNode" do
        addAttributes [("rule.id", toAttribute ruleID')]
        let parents = relativesCollection parents'
        ident <- newNodeIdentifier
        node <- makeNewProductionNode ident parents ruleID' varMap
        addNodeToReteEnvironment (the node) env
        gcIfDebug
        pure node
  where

    spawnMemoryNode :: NodeIdentifier -> RelativesCollection metadata -> Sem r ((ReteNode metadata) ? IsMemoryNode metadata)
    spawnMemoryNode ident parents = push "spawnMemorynode" do
      env <- ask <&> view reteEnvironment
      newNode <- makeNewMemoryNode ident Heap.empty parents
      addNodeToReteEnvironment (the newNode) env
      actOnRelatives parents \(Entry _ parent) -> addMetaDataToParentNode parent (the newNode)
      gcIfDebug
      pure newNode

--

{-# INLINEABLE checkForExistingNode #-}
checkForExistingNode :: Member Transactional r => (ReteNode metadata -> Bool) -> RelativesCollection metadata -> Sem r (Maybe (ReteNode metadata))
checkForExistingNode childPredicate parents =
  parents
    ^!? folded -- foreach parent
      . to payload
      . childNodes
      . folded -- foreach child
      . to payload
      . filtered childPredicate
      . act -- extract out the parent nodes
        ( \child ->
            do
              p <- child ^! parentNodes
              pure (child, p)
        )
      . filtered -- child.parents == parents?
        ( \(_, p) -> p == parents
        )
      . _1

{-# INLINEABLE newCommonNodeData #-}
newCommonNodeData ::
  (Members '[Transactional, Embed IO] r, HasCallStack, Typeable metadata, Renderable metadata) =>
  NodeIdentifier ->
  RelativesCollection metadata ->
  RelativesCollection metadata ->
  ParentArity ->
  Sem r (CommonNodeData metadata)
newCommonNodeData i children parents arity = do
  let oldestParentEntry = getLast $ foldMap (Last . Just) parents
      youngestChildEntry = if Heap.null children then Nothing else Just (Heap.minimum children)
      nodeLevel' = compatibleLevel youngestChildEntry oldestParentEntry

  when (isNothing nodeLevel') do
    throwTransaction (ReteNodeOrderingInvariantException i children parents)

  emptyParents' <-
    Heap.mapM
      ( \e -> do
          hasEmpty <- hasEmptyMemory (payload e)
          pure (hasEmpty, e)
      )
      parents

  let emptyParents = seqOf (folded . filtered fst . _2 . to payload) emptyParents'

  c <- newT children
  p <- newT parents
  e <- newT emptyParents
  let nodeLevel = fromJust nodeLevel'
  gcIfDebug
  pure (CommonNodeData i nodeLevel c e p arity)

{-# INLINEABLE makeNewMemoryBlockForNode #-}
makeNewMemoryBlockForNode :: Member Transactional r => ReteNode metadata -> Sem r (MemoryBlock metadata)
makeNewMemoryBlockForNode node = MemoryBlock <$> pure node <*> newSet

{-# INLINEABLE makeNewMemoryNode #-}
makeNewMemoryNode :: (Renderable metadata, Typeable metadata, Members '[Transactional, Fixpoint, Embed IO] r) => NodeIdentifier -> RelativesCollection metadata -> RelativesCollection metadata -> Sem r ((ReteNode metadata) ? IsMemoryNode metadata)
makeNewMemoryNode i children parents = do
  rec cnd <- newCommonNodeData i children parents Unary
      mem <- makeNewMemoryBlockForNode (the node)
      allChildren <- newT children
      let node = MemoryNode mem (coerce allChildren) cnd
  linkNodeToRelatives (the node)
  pure node

{-# INLINEABLE makeNewProductionNode #-}
makeNewProductionNode :: (Renderable metadata, Typeable metadata, Members '[Transactional, Fixpoint, Embed IO] r) => NodeIdentifier -> RelativesCollection metadata -> RuleIdentifier -> Substitution Variable -> Sem r ((ReteNode metadata) ? IsProductionNode metadata)
makeNewProductionNode i parents ruleID varMap = do
  rec cnd <- newCommonNodeData i Heap.empty parents Unary
      mem <- makeNewMemoryBlockForNode (the node)
      let node = ProductionNode ruleID mem varMap cnd
  linkNodeToParents (the node)
  pure node

{-# INLINEABLE makeNewRawAlphaNode #-}
makeNewRawAlphaNode :: (Renderable metadata, Typeable metadata, Members '[Transactional, Fixpoint, Embed IO] r) => NodeIdentifier -> WMEPattern -> MemoryConstantTestHashKey -> Sem r ((ReteNode metadata) ? IsAlphaNode metadata)
makeNewRawAlphaNode i pat hashKey = do
  rec cnd <- newCommonNodeData i Heap.empty Heap.empty Nullary
      mem <- makeNewMemoryBlockForNode (the node)
      refcount <- newT 0
      let node = AlphaNode pat mem refcount hashKey cnd
  pure node

{-# INLINEABLE makeNewNegativeNode #-}
makeNewNegativeNode :: (Renderable metadata, Typeable metadata, Members '[Transactional, Fixpoint, Embed IO] r) => NodeIdentifier -> HashMap NodeIdentifier (HashSet JoinCondition) -> (ReteNode metadata) ? IsAlphaNode metadata -> RelativesCollection metadata -> Sem r ((ReteNode metadata) ? IsNegativeNode metadata)
makeNewNegativeNode i indexedConds rightAmem parents = do
  rec cnd <- newCommonNodeData i Heap.empty parents KAry
      mem <- makeNewMemoryBlockForNode (the node)
      rightAmemVar <- newT rightAmem
      let node = NegativeNode indexedConds rightAmemVar mem cnd
  linkNodeToParents (the node)
  pure node
{-# INLINEABLE _UNSAFEmakeNewNCCNode #-}
_UNSAFEmakeNewNCCNode :: (Renderable metadata, Typeable metadata, Members '[Transactional, Fixpoint, Embed IO] r) => NodeIdentifier -> (ReteNode metadata) ? IsNCCPartnerNode metadata -> RelativesCollection metadata -> Sem r ((ReteNode metadata) ? IsNCCNode metadata)
_UNSAFEmakeNewNCCNode ident partner parents = do
  rec cnd' <- newCommonNodeData ident Heap.empty parents KAry
      mem <- makeNewMemoryBlockForNode (the node)
      let node = NegatedConjugationNode mem partner cnd
          cnd = cnd' & level .~ (partner ^. level + 1)
  linkNodeToParents (the node)
  pure node
{-# INLINEABLE _UNSAFEmakeNewNCCPartnerNode #-}
_UNSAFEmakeNewNCCPartnerNode :: (Renderable metadata, Typeable metadata, Members '[Transactional, Fixpoint, Embed IO] r, HasCallStack) => NodeIdentifier -> NodeIdentifier -> RelativesCollection metadata -> Sem r ((ReteNode metadata) ? IsNCCPartnerNode metadata)
_UNSAFEmakeNewNCCPartnerNode i branchPoint parents = do
  rec cnd <- newCommonNodeData i Heap.empty parents Unary
      actual <- newT (Prelude.error "NCC Partner node without NCC node")
      mem <- makeNewMemoryBlockForNode (the node)
      let node = NegatedConjugationPartnerNode branchPoint mem actual cnd
  linkNodeToParents (the node)
  pure node

{-# INLINEABLE unsafeMakeNewJoinNode #-}
unsafeMakeNewJoinNode :: (HasCallStack, Renderable metadata, Typeable metadata, Members '[Embed IO, Transactional] r) => NodeIdentifier -> HashMap NodeIdentifier (HashSet JoinCondition) -> (ReteNode metadata) ? IsAlphaNode metadata -> RelativesCollection metadata -> Sem r ((ReteNode metadata) ? IsJoinNode metadata)
unsafeMakeNewJoinNode i indexedConds rightAmem parents = do
  cnd <- newCommonNodeData i Heap.empty parents KAry
  memNode <- newT (Prelude.error "Join node without memory node")
  rightAmemVar <- newT rightAmem
  let res = JoinNode indexedConds rightAmemVar memNode cnd

  linkNodeToParents (the res)
  pure res
