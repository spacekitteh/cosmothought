{-# LANGUAGE MonadComprehensions #-}

module Rete.Dynamics.CollectionNodeActivation (activateNode, addNewToken, newCommonTokenData) where

import Core.CoreTypes
import Core.Patterns.PatternSyntax
import Rete.Environment
import Rete.Joins
import Rete.ReteTypes
import Core.Identifiers.IdentifierGenerator
import Cosmolude
import qualified Data.HashMap.Strict as HM
import qualified Data.HashSet as HS
import Data.HashSet.Lens
import qualified Data.Sequence as Seq
import Data.Sequence.Lens
import Data.String

import Polysemy.State
import StmContainers.Map as STMM
import StmContainers.Set as STMS
import Prelude (error, undefined)


{-# INLINEABLE activateNode #-}
{-# SCC activateNode #-}
activateNode ::  (Pretty metadata, HasMemoryBlock' (WMEToken' action metadata) action metadata, HasWMETokens (WME' metadata) action metadata, Members '[State (ProductionRuleActivity action metadata), Embed STM, Log, IDGen] r) =>
  -- | Originating node
  ReteNode action metadata ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken action metadata) -> -------- TODO MAYBE change tokens to have only alpha tokens contain WMEPatternMatch'es.

  -- | The activated node
  ReteNode action metadata ->
  Sem r ()
activateNode origin toks newNode = -- attr_ "Node" (fromString . show .  pretty $ newNode^.identifier) $
  activateNode' origin toks newNode

{-# INLINEABLE activateNode' #-}
activateNode' ::
  (Pretty metadata, HasMemoryBlock' (WMEToken' action metadata) action metadata, HasWMETokens (WME' metadata) action metadata, Members '[State (ProductionRuleActivity action metadata), Embed STM, Log, IDGen] r) =>
  -- | Originating node
  ReteNode action metadata ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken action metadata) -> -------- TODO MAYBE change tokens to have only alpha tokens contain WMEPatternMatch'es.

  -- | The activated node
  ReteNode action metadata ->
  Sem r ()
activateNode' origin toks newNode
     | JoinNode {..} <- newNode = -- attr_ "NodeType" "Join" 
       do
        debug_ . fromString $ "Activating Join node " ++ show newNode ++ " with tokens " ++ show toks
        activateJoinNode origin toks newNode
     | MemoryNode {..} <- newNode = -- attr_ "NodeType" "Memory" 
       do
         debug_ "Activating memory node"
         activateMemoryNode origin toks newNode
     | ProductionNode {..} <- newNode = -- attr_ "NodeType" "Production"
       do
        debug_ "Activating production node"
        activateProductionNode origin toks newNode
     | otherwise = Prelude.error "what activate node" -- pure ()

activateProductionNode ::
  (Pretty metadata, HasMemoryBlock' (WMEToken' action metadata) action metadata, HasWMETokens (WME' metadata) action metadata, Members '[State (ProductionRuleActivity action metadata), Embed STM, Log, IDGen] r) =>
  -- | Originating node
  ReteNode action metadata ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken action metadata) ->
  -- | The activated memory node
  ReteNode action metadata ->
  Sem r ()
activateProductionNode origin toks prodNode = push "activateProductionNode" do
  rfs <- gets (view ruleFirings)
  newTokens' <- (flip HM.traverseWithKey) toks \patMatch tok -> do
    debug_ . fromString . show $ "Creating new production token from" <+> pretty tok
    -- only a single token from each node can be considered as a parent
    let parentTokens = (tok ^. tailMatches) `HM.union` (HM.singleton (origin ^. nodeIdentifier) (tok ^. rawToken))
        immediateParents = Seq.singleton (tok ^. rawToken)
    newToken <- addNewToken prodNode patMatch parentTokens immediateParents
    debug_ . fromString . show $ "New production token:" <+> pretty newToken
    pure newToken
  embed $ forMOf_ (folded . _FreshlyAdded) newTokens' \tok -> do
    rfs ^! inserting tok

{-# INLINEABLE activateMemoryNode #-}
{-# SCC activateMemoryNode #-}
activateMemoryNode ::
  (Pretty metadata, HasMemoryBlock' (WMEToken' action metadata) action metadata, HasWMETokens (WME' metadata) action metadata, Members '[State (ProductionRuleActivity action metadata), Embed STM, Log, IDGen] r) =>
  -- | Originating node
  ReteNode action metadata ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken action metadata) ->
  -- | The activated memory node
  ReteNode action metadata ->
  Sem r ()
activateMemoryNode origin toks node = do
  newTokens' <- (flip HM.traverseWithKey) toks \patMatch tok -> do
    -- only a single token from each node can be considered as a parent
    let parentTokens = (tok ^. tailMatches) `HM.union` (HM.singleton (origin ^. nodeIdentifier) (tok ^. rawToken))
        immediateParents = Seq.singleton (tok ^. rawToken)
    addNewToken node patMatch parentTokens immediateParents

  let newTokens = HM.fromList $ newTokens' ^@.. itraversed . _FreshlyAdded . from rawToken
  children <- embed $ node ^! childNodes
  debug_ . fromString $ show (lengthOf folded newTokens) ++" new tokens in memory node: " ++ show newTokens
  forMOf_ (folded . ignoringLevel) children \child -> do
    activateNode node newTokens child

-- TODO FIXME TODO FIXME refactor this into a general k-ary node activation procedure

{-# INLINEABLE activateJoinNode #-}
{-# SCC activateJoinNode #-}
activateJoinNode ::
  (Pretty metadata, HasMemoryBlock' (WMEToken' action metadata) action metadata, HasWMETokens (WME' metadata) action metadata, HasCallStack, Members '[State (ProductionRuleActivity action metadata), Log, Embed STM, IDGen] r) =>
  -- | Originating node
  ReteNode action metadata ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken action metadata) ->
  -- | The join node
  ReteNode action metadata ->
  Sem r ()
activateJoinNode originatingNode toks currentNode@(JoinNode {..}) = do
  children <- embed $ currentNode ^! childNodes
  -- vvv HashSet is a monoid. Will give HS.empty if we don't have conditions.
  let conditions = currentNode ^. joinConditions -- ByInitiatingNode . at (originatingNode ^. nodeIdentifier) . _Just
  debug_ . fromString $ "Conditions to test: " ++ show conditions
  debug_ . fromString $ "Toks in join: " ++ show toks
  resultsToPropagate <- allSats conditions currentNode originatingNode toks
  debug_ . fromString $ (show $ lengthOf folded resultsToPropagate) ++ " results to propagate from join: " ++ show resultsToPropagate
  children <- embed $ currentNode ^! childNodes
  forMOf_ (folded . ignoringLevel) children \child -> do
    forM_ resultsToPropagate \tokens -> do
      TODO THIS IS WRONG. TOKENS IS A SINGLE JOIN. DONT FOLD OVER IT.
      let tokenMap = HM.fromList $ tokens ^.. folded . to (\tok -> (tok ^. match, PropagatingToken tok))
      activateNode currentNode tokenMap child

{-# INLINEABLE allSats #-}
allSats ::
  ( Pretty metadata, Members '[Log, State (ProductionRuleActivity action metadata), IDGen, Embed STM] r,
    HasMemoryBlock' (WMEToken' action metadata) action metadata,
    HasCallStack
  ) =>
  HashSet JoinCondition ->
  ReteNode action metadata ->
  ReteNode action metadata ->
  HashMap (WMEPatternMatch' metadata) (PropagatingToken action metadata) ->
  Sem r (HashMap (WMEPatternMatch' metadata) (HashSet (WMEToken' action metadata))) -- JoinSatisfaction action metadata))
allSats conditions currentNode nodeTokensCameFrom propagatingTokens = do
  sets <- mapM (allTokenSets currentNode nodeTokensCameFrom) propagatingTokens
  debug_ . fromString $ "Sets of tokens: " ++ show sets
  let testedJoins = foldMap (\pctc -> satisfactions pctc conditions propagatingTokens) (fold sets)
  debug_ . fromString $ "Tested join conditions: " ++ show testedJoins
  pure
    ( HM.mapMaybe
        ( ( \sat -> case sat of
              SomeConditionFailed -> Nothing
              JoinSucceeded successfulTokenCombo -> Just (successfulTokenCombo)
          )
        )
        testedJoins
    )

{-# INLINEABLE satisfactions #-}
satisfactions :: Functor f => PreviouslyConsideredTokenCombo action metadata -> HashSet JoinCondition -> f (PropagatingToken action metadata) -> f (JoinSatisfaction action metadata)
satisfactions previouslyConsideredTokens conditions propagatingTokens =
  propagatingTokens
    & mapped
      %~ \propagatingToken ->
        ( evaluateJoin
            conditions
            previouslyConsideredTokens
            propagatingToken
        )

{-# INLINEABLE allTokenSets #-}
allTokenSets :: (Pretty metadata,
                 HasMemoryBlock' (WMEToken' action metadata) action metadata, HasCallStack, Members '[Log, IDGen, Embed STM] r) => ReteNode action metadata -> ReteNode action metadata -> PropagatingToken action metadata -> Sem r (Seq (PreviouslyConsideredTokenCombo action metadata))
allTokenSets currentNode nodeTokenCameFrom propagatedToken = do
  tokenSequences <- embed $ do
    tokenSets <- fmap Seq.fromList $ currentNode ^!! parentNodes . folded . ignoringLevel . filtered (\n -> (n ^. nodeIdentifier) /= (nodeTokenCameFrom ^. nodeIdentifier)) . memory . tokens
    traverse (perform toSeq) tokenSets
  tokCartesianProduct currentNode propagatedToken (tokenSequences)

{-# INLINEABLE tokCartesianProduct #-}
tokCartesianProduct ::
  (Pretty metadata, Members '[Log, IDGen, Embed STM] r) =>
  ReteNode action metadata ->
  PropagatingToken action metadata -> -- The propagating token. In binary left activations, this is the beta token.
  Seq (Seq (WMEToken' action metadata)) ->
  Sem r (Seq (PreviouslyConsideredTokenCombo action metadata))
tokCartesianProduct node propagatingToken toksInEachParent = do
  debug_ . fromString $ "Cartesian product containing: " ++ show propagatingToken
  debug_ . fromString $ "Cartesian product with: " ++ show toksInEachParent
  forM cartesianProduct \combo -> do
    ident <- TokenIdentifier <$> newGlobalIdentifier
    let parentMatches =
          ( HM.fromList $
              ( combo
                  ^.. folded
                    . to (\tok -> (tok ^. containingReteNode . nodeIdentifier, tok))
              )
          )
            <> (combo ^. folded . tailMatches)
    ctd <- embed $ newCommonTokenData ident node (propagatingToken ^. match) parentMatches combo
    pure (PreviouslyConsideredTokenCombo ctd)
  where
    cartesianProduct = sequence toksInEachParent

{-# INLINEABLE removeTokenFromMemoryBlock #-}
removeTokenFromMemoryBlock ::
  ( Hashable (WMEToken' action metadata),
    HasMemoryBlock' (WMEToken' action metadata) action metadata
  ) =>
  WMEToken' action metadata ->
  STM ()
removeTokenFromMemoryBlock tok = STMS.delete tok (tok ^. memoryBlock . tokens)

{-# INLINEABLE newCommonTokenData #-}
newCommonTokenData :: TokenIdentifier -> ReteNode action metadata -> WMEPatternMatch' metadata -> HashMap NodeIdentifier (WMEToken' action metadata) -> Seq (WMEToken' action metadata) -> STM (CommonTokenData action metadata)
newCommonTokenData i node newMatch parentMatches immediateParents =
  CommonTokenData i node newMatch parentMatches immediateParents <$> STMS.new

-- | Create a new token
{-# INLINEABLE newToken #-}
newToken :: (HasCallStack, Members [Embed STM, IDGen] r) => ReteNode action metadata -> WMEPatternMatch' metadata -> HashMap NodeIdentifier (WMEToken' action metadata) -> Seq (WMEToken' action metadata) -> Sem r (WMEToken' action metadata)
newToken n patMatch parentToks immediateParents = do
  newIdentifier <- TokenIdentifier <$> newLocalIdentifierInContext (n ^. identifier)
  commonData' <- embed $ newCommonTokenData newIdentifier n patMatch parentToks immediateParents
  case n of
    AlphaNode {..} -> pure $ MemToken (n ^?! memory) commonData'
    MemoryNode {..} -> pure $ MemToken (n ^?! memory) commonData'
    ProductionNode {..} -> pure $ ProductionToken (n ^?! memory) commonData'

-- | Create a new token and add it to the memory block
{-# INLINEABLE addNewToken #-}
addNewToken :: forall action metadata r. (HasMemoryBlock' (WMEToken' action metadata) action metadata, HasWMETokens (WME' metadata) action metadata, Hashable (WMEToken' action metadata), HasCallStack, Members [Embed STM, IDGen] r) => ReteNode action metadata -> WMEPatternMatch' metadata -> HashMap NodeIdentifier (WMEToken' action metadata) -> Seq (WMEToken' action metadata) -> Sem r (TokenAddition action metadata)
addNewToken n patMatch parentToks immediateParents = do
  token <- newToken n patMatch parentToks immediateParents
  case token of
    FreshlyAdded tok -> addNewGivenToken n tok
    AlreadyPresent -> pure AlreadyPresent

-- | Create a new token and add it to the memory block
{-# INLINEABLE addNewGivenToken #-}
addNewGivenToken :: forall action metadata r. (HasMemoryBlock' (WMEToken' action metadata) action metadata, HasWMETokens (WME' metadata) action metadata, Hashable (WMEToken' action metadata), HasCallStack, Members [Embed STM, IDGen] r) => ReteNode action metadata -> WMEToken' action metadata -> Sem r (TokenAddition action metadata)
addNewGivenToken n token = do
  token <- newToken n patMatch parentToks immediateParents
  changed <- embed @STM $ do
    patMatch ^! matchedWME . ownedTokens . inserting token
    n ^! memory . tokens . insertingTested token . to Any
  pure $ if getAny changed then FreshlyAdded token else AlreadyPresent
       
