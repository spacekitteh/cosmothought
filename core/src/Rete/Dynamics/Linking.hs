{-# LANGUAGE LinearTypes #-}
{-# LANGUAGE RecursiveDo #-}

module Rete.Dynamics.Linking ( actOnRelatives, linkNodeToParents,unlinkChildren, relativesCollection, validNodeLevel, removeEmptyParentFromChild, relinkChildren, computeParentsToRelink, makeOffspring, addMetaDataToParentNode, linkNodeToRelatives, unlinkNodes, compatibleLevel, unlinkNodeForDeletion) where

--import Rete.ReteBuilder
import Rete.ReteTypes
import Cosmolude
--import StmContainers.Set as STMS
--import StmContainers.Map as STMM
--import StmContainers.Set as STMS
import qualified Unsafe.Linear as Unsafe
--import Core.WorkingMemory
import Data.Heap as Heap
import qualified Data.Sequence as Seq
--import Control.Monad.ST (RealWorld)

{-# INLINE nodeTypeIncreasesAlphaReferenceCount #-}
nodeTypeIncreasesAlphaReferenceCount :: ReteNode  metadata -> Bool
nodeTypeIncreasesAlphaReferenceCount _ = True -- ?
--nodeTypeIncreasesAlphaReferenceCount JoinNode {} = True
--nodeTypeIncreasesAlphaReferenceCount NegativeNode {} = True
--nodeTypeIncreasesAlphaReferenceCount _ = False

{-# INLINEABLE addMetaDataToParentNode #-}
{-# SCC addMetaDataToParentNode #-}
addMetaDataToParentNode ::  Members '[Transactional] r =>
  ReteNode  metadata ->
  ReteNode  metadata ->
  Sem r ()
addMetaDataToParentNode parent' n = case parent' of
  (maybeCorrectSubType' -> Just parent) -> do    
    parent ^! allChildNodesCollection.act  (modifyT (insertNode n))
  (maybeCorrectSubType' -> Just parent)  | nodeTypeIncreasesAlphaReferenceCount n -> do
    parent ^! referenceCount . act (modifyT (+ 1))
  _ -> pure ()
{-# INLINEABLE removeMetaDataFromParentNode #-}
{-# SCC removeMetaDataFromParentNode #-}
removeMetaDataFromParentNode :: Members '[Transactional] r =>
  ReteNode  metadata ->
  ReteNode  metadata ->
  Sem r ()
removeMetaDataFromParentNode parent' n = case parent' of
  (maybeCorrectSubType' -> Just parent) -> do
    parent ^! allChildNodesCollection. act (modifyT (removeNode n))
  (maybeCorrectSubType' -> Just parent)  | nodeTypeIncreasesAlphaReferenceCount n -> do
    parent ^! referenceCount. act (modifyT (+ 1))
  _ -> pure ()    

-- TODO IMPLEMENT
{-# INLINEABLE unlinkNodes #-}
unlinkNodes :: (HasCallStack,  Members '[Transactional] r  ) => ReteNode metadata -> Sem r ()
-- unlinkNodes n@MemoryNode {..} = do
--   isEmpty <- (STMS.null (n ^?! memory . tokens))
--   when isEmpty do
--      unlinkChildren n
unlinkNodes _ = pure ()

-- unlinkNodes n@NegativeNode
{-# INLINEABLE unlinkNodeForDeletion #-}
unlinkNodeForDeletion ::  Members '[Transactional] r =>ReteNode metadata -> Sem r ()
unlinkNodeForDeletion node = do
  children <- node^!childNodes' . act readT
  actOnRelatives children \(Entry _ child) -> do
    removeMetaDataFromParentNode node child
    unlinkChildFromParent node child
    
  

{-# INLINE insertNode #-}
{-# SCC insertNode #-}
insertNode :: ReteNode  metadata -> RelativesCollection  metadata %m -> RelativesCollection  metadata
insertNode node heap = (Unsafe.toLinear2 Heap.insert) (Entry (node ^. level) node) heap

{-# INLINE removeNode #-}
{-# SCC removeNode #-}
removeNode :: ReteNode  metadata -> RelativesCollection  metadata %m -> RelativesCollection  metadata
removeNode node heap = (Unsafe.toLinear2 Heap.filter) (\(Entry _ n) -> n ^. identifier /= node ^. identifier) heap

{-# INLINEABLE relativesCollection #-}
{-# SCC relativesCollection #-}
relativesCollection :: Foldable f => f (ReteNode  metadata) -> RelativesCollection  metadata
relativesCollection = Cosmolude.foldr insertNode Heap.empty

{-# INLINEABLE compatibleLevel #-}
compatibleLevel :: Maybe (Entry NodeLevel a) -> Maybe (Entry NodeLevel a) -> Maybe NodeLevel
compatibleLevel Nothing (Just (Entry parent _)) = Just (parent + 1)
compatibleLevel (Just (Entry child _)) Nothing = Just (child - 1)
-- Children must have a higher level than their parent, and must have space for the intermediary.
compatibleLevel (Just (Entry child _)) (Just (Entry parent _))
  | child > (parent + 1) = Just (parent + 1)
  | otherwise = Nothing
compatibleLevel Nothing Nothing = Just 0

{-# INLINE validNodeLevel #-}
validNodeLevel :: Entry NodeLevel a -> Entry NodeLevel a -> Bool
validNodeLevel (Entry parent _) (Entry child _) = child > parent



{-# INLINEABLE linkNodeToChildren #-}
{-# SCC linkNodeToChildren #-}
linkNodeToChildren :: (Members '[Transactional] r) => ReteNode  metadata -> Sem r ()
linkNodeToChildren n = do
  children <- n ^! childNodes -- runSLens @OpSTM @RealWorld (asLens (n ^. commonNodeData . childNodes)) stateRead
  actOnRelatives children \(Entry _ child) -> do
    addParentLinkToChild n child

{-# INLINEABLE linkNodeToParents #-}
{-# SCC linkNodeToParents #-}
linkNodeToParents :: Members '[Transactional] r => ReteNode  metadata -> Sem r ()
linkNodeToParents n = do
  parents <- n ^! parentNodes -- runSLens @OpSTM @RealWorld (asLens (n ^. commonNodeData . parentNodes')) stateRead
  actOnRelatives parents \(Entry _ parent) -> do
    addChildLinkToParent parent n
    addMetaDataToParentNode parent n

{-# INLINE actOnRelatives #-}
{-# SCC actOnRelatives #-}
actOnRelatives :: Monad m => Heap a %1 -> (a -> m b) -> m ()
actOnRelatives h action = (Unsafe.toLinear2 mapM_) action ((Unsafe.toLinear toUnsortedList) h)

{-# INLINEABLE linkNodeToRelatives #-}
linkNodeToRelatives :: Members '[Transactional] r => ReteNode  metadata -> Sem r ()
linkNodeToRelatives n = do
  linkNodeToChildren n
  linkNodeToParents n


{-# INLINEABLE addParentLinkToChild #-}
{-# SCC addParentLinkToChild #-}
addParentLinkToChild :: Members '[Transactional] r => ReteNode  metadata -> ReteNode  metadata -> Sem r ()
addParentLinkToChild parent child =
  when (canHaveParentNodes child) do
    isEmptyParent <- hasEmptyMemory parent
    when isEmptyParent $ addEmptyParentToChild parent child
    child^!commonNodeData.parentNodes' . act (modifyT (insertNode parent))
    --runSLens @OpSTM @RealWorld (asLens (child ^. commonNodeData . parentNodes')) (stateModify (insertNode parent))

{-# INLINEABLE addChildLinkToParent #-}
{-# SCC addChildLinkToParent #-}
-- TODO FIXME: Ensure invariants on child/parent collections
addChildLinkToParent :: Members '[Transactional] r => ReteNode  metadata -> ReteNode  metadata -> Sem r ()
addChildLinkToParent parent child =
  when (canHaveChildNodes parent) do
    --    isEmptyParent <- hasEmptyMemory parent
    --    let getsLinked = (not isEmptyParent) || getsLinkedWhenEmpty parent
    --    when getsLinked do
    parent^!commonNodeData.childNodes' . act (modifyT (insertNode child))
    --runSLens @OpSTM @RealWorld (asLens (parent ^. commonNodeData . childNodes')) (stateModify (insertNode child))

{-# INLINEABLE unlinkChildFromParent #-}
{-# SCC unlinkChildFromParent #-}
unlinkChildFromParent :: Members '[Transactional] r => ReteNode  metadata -> ReteNode  metadata -> Sem r ()
unlinkChildFromParent parent child = parent^!commonNodeData.childNodes'.act (modifyT (removeNode child)) --runSLens @OpSTM @RealWorld (asLens (parent ^. commonNodeData . childNodes')) (stateModify (removeNode child))

{-# INLINEABLE addEmptyParentToChild #-}
{-# SCC addEmptyParentToChild #-}
addEmptyParentToChild :: Members '[Transactional] r =>  ReteNode  metadata -> ReteNode  metadata -> Sem r ()
addEmptyParentToChild parent child = child ^! commonNodeData . emptyParentNodes'. act (modifyT (\others -> others Seq.|> parent))

{-# INLINEABLE removeEmptyParentFromChild #-}
{-# SCC removeEmptyParentFromChild #-}
removeEmptyParentFromChild ::  Members '[Transactional] r => ReteNode  metadata -> ReteNode  metadata -> Sem r ()
removeEmptyParentFromChild parent child = child ^! commonNodeData . emptyParentNodes'. act (modifyT (Seq.filter (parent /=)))

-- | Unlink when empty
{-# INLINEABLE unlinkChildren #-}
{-# SCC unlinkChildren #-}
unlinkChildren :: Members '[Transactional] r => ReteNode  metadata -> Sem r ()
unlinkChildren parent = do
  -- foreach child
  children <- parent ^! childNodes
  actOnRelatives children \(Entry _ child) -> do
    addEmptyParentToChild parent child
    -- foreach parent of the child that isn't the initiating parent
    parents <- child ^! parentNodes
    actOnRelatives parents \(Entry _ otherParent) -> do
      unlinkChildFromParent otherParent child

-- | Relink children's parents' nodes after the given node becomes non-empty
{-# INLINEABLE relinkChildren #-}
{-# SCC relinkChildren #-}
relinkChildren ::  Members '[Transactional] r =>ReteNode  metadata -> Sem r ()
relinkChildren parent = do
  children <- parent ^! childNodes
  -- foreach child
  actOnRelatives children \(Entry _ child) -> do
    removeEmptyParentFromChild parent child
    -- foreach parent of the child which we will relink
    parents <- computeParentsToRelink child
    forM_ parents \(Entry _ otherParent) -> do
      when (parent /= child) do
        addChildLinkToParent otherParent child

{-# INLINEABLE computeParentsToRelink #-}
computeParentsToRelink ::  Members '[Transactional] r => ReteNode  metadata -> Sem r (RelativesCollection  metadata)
computeParentsToRelink child = do
  parents <- child ^! parentNodes
  emptyParents <- child ^! emptyParentNodes
  -- If every parent is full, return them all
  if (Seq.null emptyParents)
    then pure parents
    else do
      -- Otherwise, we need to select a node which is empty
      -- As a heuristic, we'll chose the one which became empty first
      pure (relativesCollection (emptyParents ^.. _head))

{-# INLINEABLE makeOffspring #-}
makeOffspring ::  Members '[Transactional] r => ReteNode  metadata -> ReteNode  metadata -> Sem r ()
makeOffspring parent child = do
  addChildLinkToParent parent child
  addParentLinkToChild parent child
