{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE UndecidableInstances #-}

module Rete.Dynamics.TokenFlow
  ( ReteTokenManipulation,
    addWMEsToAlphaMemory,
    addTokens,
    removeTokens,
    runReteTokenManipulation,
    updateNewNodeWithMatchesFromAbove,
    OriginIsNewlyNonEmpty (OriginIsNewlyEmpty),
    activateNode,
  )
where

-- import Rete.Environment
import Control.Exception (Exception)  
import Polysemy.Reader
import Control.Parallel.Strategies
import Core.CoreTypes
import Core.Patterns
import OpenTelemetry.Trace.Core
import Rete.Dynamics.Linking
import Rete.Dynamics.TokenCreation
import Rete.Joins
import Rete.ReteTypes
import Rete.Types.Tokens
import Core.Identifiers.IdentifierGenerator
import Cosmolude hiding ((...))
import qualified Data.HashMap.Strict as HM
import qualified Data.HashSet as HS
import Data.HashSet.Lens
import qualified Data.Sequence as Seq
import Data.Sequence.Lens
import GHC.Exts (SPEC (..))

import StmContainers.Set as STMS

import qualified Prelude (error)

type TokenFlowConstraints agent metadata r =
  ( Typeable metadata,
    HasCallStack,
    Renderable metadata,
    HasTracer agent,
    HasMemoryBlock' (WMEToken' metadata) metadata,
    HasWMETokens (WME' metadata) metadata,
    HasProductionRuleActivity agent metadata,
    Members '[Reader agent, OpenTelemetrySpan, OpenTelemetryContext , Resource, Reader agent , Transactional, Log, IDGen, Assert] r
  )

data ReteTokenManipulation metadata m a where
  -- | Start process of propagating WMEs through the network
  AddWMEsToAlphaMemory :: Traversable t => t (WME' metadata) -> STMS.Set ((ReteNode metadata) ? IsAlphaNode metadata) -> ReteTokenManipulation metadata m (HS.HashSet (TokenAddition metadata))
  AddTokens ::
    Traversable t =>
    -- | Node we're adding tokens to
    ReteNode metadata ->
    -- | Did origin just become nonempty?
    OriginIsNewlyNonEmpty metadata ->
    t (WMEToken' metadata) ->
    ReteTokenManipulation metadata m (t (TokenAddition metadata))
  RemoveTokens ::
    Traversable t =>
    t (WMEToken' metadata) ->
    ReteTokenManipulation metadata m ()

makeSem ''ReteTokenManipulation

{-# INLINE runReteTokenManipulation #-}
runReteTokenManipulation ::
  forall agent metadata r.
  ( Show metadata,
         (HasMaybeWMPreference metadata ObjectIdentifier),
    Renderable metadata,
    WMEInAlphaNodes
      ( WMEToken' metadata
      )
      metadata
      (WME' metadata),
    HasMemoryBlock' (WMEToken' metadata) metadata,
    HasWMETokens (WME' metadata) metadata,
    Hashable (WMEToken' metadata),
    Hashable (TokenAddition metadata),
    WMEIsState metadata,
    TokenFlowConstraints agent metadata r
  ) =>
  InterpreterFor (ReteTokenManipulation metadata) r
runReteTokenManipulation = interpret $ \i -> 
  case i of

    AddTokens destination newlyNonEmpty toks ->     {-#SCC addTokens #-} push "AddTokens" do
      forM toks \tok -> addNewGivenToken destination newlyNonEmpty tok

    RemoveTokens toks ->     {-# SCC removeTokens #-}push "RemoveTokens" do
      debug' $ "Removing tokens:" <+> render (toks ^.. folded)
      forM_ toks \tok -> removeToken tok

    AddWMEsToAlphaMemory wmes nodes' ->
          {-# SCC addWmesToAlphaMemory #-}push "AddWMEsToAlphaMemory" do
        nodes <- nodes' ^!! elems
        debug' $ "Adding wmes to these nodes: " <+> render nodes
        additions <- forM nodes \node -> do
          -- add matching wmes to node
          tokens <-
            wmes
              ^!! traversed
                . act
                  ( \wme -> do
                      debug' $ "Trying wme" <+> render wme
                      pure wme
                  )
                . to (patternMatchesWME trivialSubstitution (node ^. matchPattern)) -- TODO FIXME: Pretty sure this should be correct for variables in alpha mem patterns?
                . act
                  ( \res -> do
                      debug' $ "result: " <+> viaShow res
                      pure res
                  )
                . _Just
                . act
                  ( \match -> do
                      alreadyPresent <- lookupSet node (match ^. matchedWME . inhabitedAlphaNodes)
                      if alreadyPresent
                        then pure Nothing
                        else do
                          match ^! matchedWME . inhabitedAlphaNodes . inserting node

                          tok <- addNewAlphaToken node OriginIsAlphaNode match HM.empty Seq.empty
                          debug' $ "Adding new token" <+> render tok
                          pure (Just tok)
                  )
                . _Just
          let attemptedAdditions = HS.fromList tokens
              additions = HM.fromList $ attemptedAdditions ^.. folded . _FreshlyAdded . to (\tok -> (tok ^??! match, PropagatingToken tok))
          when (not (HM.null additions)) do
            children <- node ^!! childNodes . folded . ignoringLevel
            forM_ children \child -> do
              activateNode (the node) OriginIsAlphaNode additions child
          pure attemptedAdditions
        pure (HS.unions additions)

-- AddTokens origin justBecameNonEmpty toks node -> do

{-# INLINE removeChildTokens #-}
{-# SCC removeChildTokens #-}
removeChildTokens ::
  TokenFlowConstraints agent metadata r =>
  WMEToken' metadata ->
  Sem r ()
removeChildTokens tok = removeChildTokens' SPEC tok

{-# INLINEABLE removeChildTokens' #-}
{-# SCC removeChildTokens' #-}
removeChildTokens' ::
  TokenFlowConstraints agent metadata r =>
  SPEC ->
  WMEToken' metadata ->
  Sem r ()
removeChildTokens' !sPEC tok = push "RemoveChildTokens" $ do
  toRemove <- tok ^!! childTokens . elems
  debug' $ "Number of child tokens:" <+> render (lengthOf folded toRemove)
  tok ^! childTokens . actAndDelete (act (removeToken' sPEC))

{-# INLINE removeToken #-}

-- | Recursively delete a token and its descendants.
removeToken :: (TokenFlowConstraints agent metadata r) => WMEToken' metadata -> Sem r ()
removeToken tok = removeToken' SPEC tok

{-# INLINEABLE removeTokenFromMemoryBlock #-}
removeTokenFromMemoryBlock ::
  ( Member Transactional r,
    Hashable (WMEToken' metadata),
    HasMemoryBlock' (WMEToken' metadata) metadata
  ) =>
  WMEToken' metadata ->
  Sem r ()
removeTokenFromMemoryBlock tok = deleteFromSet tok (tok ^. memoryBlock . tokens)

{-# INLINEABLE removeToken' #-}
{-# SCC removeToken' #-}
removeToken' :: (HasCallStack, TokenFlowConstraints agent metadata r) => SPEC -> WMEToken' metadata -> Sem r ()
removeToken' !sPEC tok = push "RemoveToken" $ attr "ID" (fromString . show . render $ tok ^. identifier) do
  removeChildTokens' sPEC tok
  ~rrs <- asks (view ruleRetractions)
  debug' $ "Removing token" <+> render tok
  ( name tok \token -> case maybeCorrectSubType token of
      Just t ->
        push "RemoveProductionToken" do
          debug' $ "Adding token to retraction set:" <+> render t
          rrs ^! inserting t
      Nothing -> pure ()
    )

  ( name tok \token -> case maybeCorrectSubType token of
      Just (t :: (WMEToken' metadata) ? IsNCCToken' metadata) ->
        push "RemoveNCCToken" do
          t
            ^! nccResults
            . actAndDelete
              ( act \resultTok -> do
                  resultTok ^! match . matchedWME . ownedTokens . deleting tok
                  resultTok ^! immediateParents . folded . childTokens . deleting tok
                  removeToken' sPEC (the resultTok)
              )
      Nothing -> pure ()
    )

  ( name tok \token' -> case maybeCorrectSubType token' of
      Just (token :: (WMEToken' metadata) ? IsNCCPartnerToken' metadata) ->
        push "RemoveNCCPartnerToken" do
          owner'' <- readT (token ^??! nccTokenOwner)
          case owner'' of
            Nothing -> do
              debug' $ "Token has no owner"
            Just owner' -> do
              owner' ^! nccResults . deleting token
              ownerResultsEmpty <- isNullSet (owner' ^. nccResults)
              --              ownerResults <- owner'^!!nccResults.elems
              debug' $ "Owner results empty?" <+> render ownerResultsEmpty
              when ownerResultsEmpty do
                let ncc = owner' ^. containingReteNode
                children <- ncc ^! childNodes
                debug' $ "NCC children:" <+> render (children ^.. folded . ignoringLevel)
                forMOf_ (folded . ignoringLevel) children \child -> do
                  activateNode ncc (Prelude.error "fix me") (HM.singleton (owner' ^??! immediateParents . folded . match) (PropagatingToken (the owner'))) child
      Nothing -> do
        -- Don't remove from memory when token is an NCC Partner token
        removeTokenFromMemoryBlock tok
        -- node ^! memory . tokens . deleting tok
    )

  -- (name tok \token -> case maybeCorrectSubType token of
  --   Just (t :: (WMEToken' metadata) ? IsNegationToken' metadata) ->
  --     do
  --         jrs <- tok^!!negativeJoinResults' . elems
  --         forM_ jrs \jr -> do

  --         wmes <- tok^!!negativeJoinWMEs . elems
  --         forM_ wmes \wme -> do
  --           wme^!negativeJoinResults . deleting tok

  --   Nothing -> pure ())

  tok ^! match . matchedWME . ownedTokens . deleting tok

  tok ^! match . matchedWME . ownedTokens . deleting tok
  tok ^!! immediateParents . folded . childTokens . deleting tok

  unlinkNodes (tok ^. containingReteNode)

-- where
--   node = tok ^. containingReteNode

{-
  DESIGN
  =======================================
  For now, I will only consider "token-at-a-time" activations; that is, not worry about "collection rete".
  Thus, each token is propagated individually.

-}
{-# INLINEABLE updateNewNodeWithMatchesFromAbove #-}
updateNewNodeWithMatchesFromAbove ::
  forall agent metadata r.
  TokenFlowConstraints agent metadata r =>
  ReteNode metadata ->
  Sem r ()
updateNewNodeWithMatchesFromAbove newNode | has _JoinNode newNode = push "UpdateNewJoinNodeWithMatchesFromAbove" $ do
  -- TODO IMPROVE: This only applies when all the parents of a join node are alpha nodes; figure out how to
  -- test for that case and only then should we match on this branch.

  parents <- newNode ^!! parentNodes . folded . ignoringLevel
  tempChildList <- newT (relativesCollection [newNode])
  --  forM_ parents \parent -> do
  updateNewNodeWithMatchesFromAbove ((parents ^??! folded) & _UNSAFEchildNodes .~ tempChildList)
updateNewNodeWithMatchesFromAbove newNode | has _AlphaNode newNode = push "UpdateNewAlphaNode" $ do
  toks <- HM.fromList <$> newNode ^!! _AlphaNode . memoryBlock . tokens . elems . to \tok -> (tok ^??! match, PropagatingToken tok)
  children <- newNode ^!! childNodes . folded . ignoringLevel
  forM_ children \child -> do
    activateNode newNode OriginIsAlphaNode toks child
updateNewNodeWithMatchesFromAbove newNode = push "UpdateNewNodeWithMatchesFromAbove" $ do
  parents <- newNode ^!! parentNodes . folded . ignoringLevel

  forM_ parents \(parent' :: ReteNode metadata) -> do
   tempChildList <- newT (relativesCollection [newNode])
   let parent = (parent' & _UNSAFEchildNodes .~ tempChildList)
   case parent of
    _ | has _AlphaNode parent -> activatePositiveNode parent
    _ | has _MemoryNode parent -> activatePositiveNode parent
    _ | has _JoinNode parent -> activatePositiveNode parent
    _ | has _NegativeNode parent -> activateNegativeNode negativeJoinResults' parent
    _ | has _NegatedConjugationNode parent -> activateNegativeNode nccResults' parent
    _ -> pure ()
  where
    activateNegativeNode :: Fold (WMEToken' metadata) (STMS.Set s) -> ReteNode metadata -> Sem r ()
    activateNegativeNode s parent = push "activateParentNegativeNode" do                                  
      toks <-
        fmap HM.fromList $
          parent
            ^!! memory
              . tokens
              . elems
              . act
                ( \tok -> do
                    empty <- isNullSet (tok ^??! s)
                    pure (empty, tok)
                )
              . (filtered (view _1))
              . _2
              . to (\tok -> (tok ^??! failing match (immediateParents . folded . match), PropagatingToken tok))
--      _ <- Prelude.error $ show toks
      activateNode parent  ActivatedNodeIsBeingConstructed toks newNode
    activatePositiveNode :: ReteNode metadata -> Sem r ()
    activatePositiveNode parent = do
      toks <- fmap HM.fromList $ parent ^!! memory . tokens . elems . to (\tok -> (tok ^??! failing match (immediateParents . folded . match), PropagatingToken tok))
      activateNode parent ActivatedNodeIsBeingConstructed toks newNode
    
{-# INLINEABLE activateNode #-}
{-# SCC activateNode #-}
activateNode ::
  TokenFlowConstraints agent metadata r =>
  -- | Originating node
  ReteNode metadata ->
  -- | Did origin just become nonempty?
  OriginIsNewlyNonEmpty metadata ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) -> -------- TODO MAYBE change tokens to have only alpha tokens contain WMEPatternMatch'es.

  -- | The activated node
  ReteNode metadata ->
  Sem r ()
activateNode origin newlyNonEmpty toks newNode = push "activateNode" do
  activateNode' origin newlyNonEmpty toks newNode

{-# INLINE activateNode' #-}
activateNode' ::
  TokenFlowConstraints agent metadata r =>
  -- | Originating node
  ReteNode metadata ->
  -- | Did origin just become nonempty?
  OriginIsNewlyNonEmpty metadata ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) -> -------- TODO MAYBE change tokens to have only alpha tokens contain WMEPatternMatch'es.

  -- | The activated node
  ReteNode metadata ->
  Sem r ()
activateNode' origin _originIsNewlyNonEmpty toks newNode
  | Just (newNode') <- maybeCorrectSubType' newNode -- JOIN
    =
      do
        debug' $ "Activating Join node " <+> viaShow newNode <+> " with tokens " <+> viaShow toks
        activateJoinNode origin toks newNode'
  | Just (newNode') <- maybeCorrectSubType' newNode =
      do
        debug' "Activating memory node"
        activateMemoryNode origin toks newNode'
  | Just (newNode') <- maybeCorrectSubType' newNode =
      do
        debug' "Activating production node"
        activateProductionNode origin toks newNode'
  | Just (newNode') <- maybeCorrectSubType' newNode =
      do
        debug' "Activating negative node"
        activateNegativeNode origin toks newNode'
  | Just (newNode') <- maybeCorrectSubType' newNode =
      do
        debug' "Activating NCC partner node"
        activateNCCPartnerNode origin toks newNode'
  | Just (newNode') <- maybeCorrectSubType' newNode =
      do
        debug' "Activating NCC node"
        activateNCCNode origin toks newNode'
  | otherwise = Prelude.error "what activate node" -- pure ()

{-# INLINEABLE createParentTokens #-}
createParentTokens :: HashSet (WMEToken' metadata) -> HashMap NodeIdentifier (WMEToken' metadata)
createParentTokens toks = HM.union immediates tails
  where
    immediates = HM.fromList . fmap (\tok -> (tok ^. containingReteNode . nodeIdentifier, tok)) . HS.toList $ toks
    tails = HM.unions $ immediates ^.. folded . tailMatches
{-# SCC activateNCCNode #-}
activateNCCNode ::
  TokenFlowConstraints agent metadata r => -- (HasCallStack, Pretty metadata, HasMemoryBlock' (WMEToken' metadata) metadata, HasWMETokens (WME' metadata) metadata, Members '[State (ProductionRuleActivity metadata), Transactional, Log, IDGen] r) =>

  -- | Originating node
  ReteNode metadata ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) ->
  -- | The activated memory node
  (ReteNode metadata) ? IsNCCNode metadata ->
  Sem r ()
activateNCCNode _origin toks nccNode = push "activateNCCNode" do
  currentTokenStatus <- do
    isEmpty <- isNullSet (nccNode ^. memoryBlock . tokens)
    if isEmpty
      then pure OriginIsNewlyNonEmpty
      else pure OriginAlreadyHadTokens
  let partner = (nccNode ^. nccPartnerNode)
  newTokens' <- (flip HM.traverseWithKey) toks \_patMatch tok -> do
    debug' $ "Creating new NCC token from" <+> render tok
    -- only a single token from each node can be considered as a parent
    let parentTokens = createParentTokens (HS.singleton (tok ^. rawToken))
        immediateParents = Seq.singleton (tok ^. rawToken)

    newToken' <- addNewNCCToken nccNode currentTokenStatus parentTokens immediateParents
    (activate, newToken'') <- case newToken' of
      AlreadyPresent -> do
        pure (False, newToken')
      FreshlyAdded newToken -> do
        resetSet (newToken ^??! nccResults')
        partner
          ^! pendingResultsForNCCNode
          . tokens
          . actAndDelete
            ( act
                ( \result' -> do
                    name result' \result'' -> case maybeCorrectSubType result'' of
                      Nothing -> pure ()
                      Just result -> do
                        newToken ^! nccResults' . inserting result
                        writeT (newToken ^? knownCorrectSubType) (result ^??! nccTokenOwner)
                )
            )

        assertM (isNullSet (partner ^. pendingResultsForNCCNode . tokens)) "Pending results haven't been cleared from NCC partner node" partner ()
        activate <- isNullSet (newToken ^??! nccResults')

        pure (activate, newToken')

    pure (activate, newToken'')

  newTokens <-
    HM.fromList
      <$> newTokens'
      ^@!! itraversed
      <. (filtered (view _1))
      . _2
      . _FreshlyAdded
      . from rawToken

  when (has folded newTokens) do
    children <- nccNode ^! childNodes
    assertM (isNullSet (partner ^. pendingResultsForNCCNode . tokens)) "NCC tokens trying to propagate while NCC partner node has pending results!" newTokens ()
    forM_ newTokens \token -> do
      assert (has (rawToken . _NCCToken) token) "trying to propagate a non-NCC token in an NCC node!" token () 
      assertM (isNullSet (token ^??! rawToken . nccResults')) "NCC token about to activate while it has NCC partner results!" token ()

    forMOf_ (folded . ignoringLevel) children \child -> do
      activateNode (the nccNode) currentTokenStatus newTokens child
{-# SCC activateNCCPartnerNode #-}
activateNCCPartnerNode ::
  TokenFlowConstraints agent metadata r =>
  -- | Originating node
  ReteNode metadata ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) ->
  -- | The activated memory node
  (ReteNode metadata) ? IsNCCPartnerNode metadata ->
  Sem r ()
activateNCCPartnerNode _origin toks partnerNode = push "activateNCCPartnerNode" do
  nccNode <- readT (partnerNode ^. actualNCCNode)
  nccToks <- nccNode ^!! memoryBlock . tokens . elems
  emptyStatus <- do
    isEmpty <- isNullSet (partnerNode ^. memoryBlock . tokens)
    if isEmpty
      then pure OriginIsNewlyNonEmpty
      else pure OriginAlreadyHadTokens
  newTokens' <- (flip HM.traverseWithKey) toks \_patMatch tok -> do
    debug'  $ "Creating new NCC Partner token from" <+> render tok
    -- only a single token from each node can be considered as a parent
    let parentTokens = createParentTokens (HS.singleton (tok ^. rawToken))
        immediateParents = Seq.singleton (tok ^. rawToken)
    newToken <- addNewNCCPartnerToken partnerNode emptyStatus parentTokens immediateParents
    debug' $ "New  NCC Partner token:" <+> render newToken

    pure newToken

  forMOf_ (folded . _FreshlyAdded) newTokens' \newResult -> do
    -- Get the token in the NCC node which unifies with the tokens in the NCC network
    let branchToken = newResult ^??! tailMatches . ix ((the partnerNode) ^??! nccBranchPoint)
        ownerToken = firstOf (folded . (filtered (\o -> has (immediateParents . folded . filtered (== branchToken)) o))) nccToks
    debug' $ "Branch token:" <+> render branchToken
    debug' $ "Owner token:" <+> render ownerToken
    case ownerToken of
      Just owningToken -> do
        owningToken ^! knownCorrectSubType . nccResults . inserting (newResult ^??! knownCorrectSubType)
        writeT (owningToken ^? knownCorrectSubType) (newResult ^??! nccTokenOwner)
        removeChildTokens owningToken
      Nothing -> do
        partnerNode ^! pendingResultsForNCCNode . tokens . inserting newResult


{-# SCC activateProductionNode #-}
activateProductionNode ::
  TokenFlowConstraints agent metadata r =>
  -- | Originating node
  ReteNode metadata ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) ->
  -- | The activated memory node
  (ReteNode metadata) ? IsProductionNode metadata ->
  Sem r ()
activateProductionNode _origin toks prodNode = push "activateProductionNode" do
  rfs <- asks (view ruleFirings)
  newTokens' <- (flip HM.traverseWithKey) toks \_patMatch tok -> do
    debug' $ "Creating new production token from" <+> render tok
    -- only a single token from each node can be considered as a parent
    let parentTokens = createParentTokens (HS.singleton (tok ^. rawToken)) 
        immediateParents = Seq.singleton (tok ^. rawToken)

    newToken <- addNewProductionToken prodNode ReachedProductionNode parentTokens immediateParents
    debug' $ "New production token:" <+> render newToken
    pure newToken

  forMOf_ (folded . _FreshlyAdded . knownCorrectSubType) newTokens' \tok -> do
    rfs ^! inserting tok

  debug' $ "New tokens in production node:" <+> render (newTokens' ^.. folded)
  contents <- prodNode ^!! memoryBlock . tokens . elems
  debug' $ "Production node total contents:" <+> viaShow contents

{-# INLINEABLE activateMemoryNode #-}
{-# SCC activateMemoryNode #-}
activateMemoryNode ::
  TokenFlowConstraints agent metadata r =>
  -- | Originating node
  ReteNode metadata ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) ->
  -- | The activated memory node
  (ReteNode metadata) ? IsMemoryNode metadata ->
  Sem r ()
activateMemoryNode _origin toks node = push "activateMemoryNode" do
  currentTokenStatus <- do
    isEmpty <- isNullSet (node ^. memoryBlock . tokens)
    if isEmpty
      then pure OriginIsNewlyNonEmpty
      else pure OriginAlreadyHadTokens
  newTokens' <- (flip HM.traverseWithKey) toks \patMatch tok -> do
    -- only a single token from each node can be considered as a parent
    let parentTokens = createParentTokens (HS.singleton (tok ^. rawToken))
        immediateParents = Seq.singleton (tok ^. rawToken)
    addNewMemToken node currentTokenStatus patMatch parentTokens immediateParents

  let newTokens = HM.fromList $ newTokens' ^@.. itraversed . _FreshlyAdded . from rawToken
  children <- (the node) ^! childNodes
  debug' $ viaShow (lengthOf folded newTokens) <+> " new tokens in memory node: " <+> viaShow newTokens
  when (has folded newTokens) do
    forMOf_ (folded . ignoringLevel) children \child -> fmap (withStrategy rpar) do
      activateNode (the node) currentTokenStatus newTokens child

{-# INLINEABLE activateMemoryNodeWithJoinPseudoTokens #-}
{-# SCC activateMemoryNodeWithJoinPseudoTokens #-}
activateMemoryNodeWithJoinPseudoTokens ::
  TokenFlowConstraints agent metadata r =>
  -- | Originating node
  (ReteNode metadata) ? IsJoinNode metadata ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (JoinPseudoToken metadata) ->
  -- | The activated memory node
  ReteNode metadata ? IsMemoryNode metadata ->
  Sem r ()
activateMemoryNodeWithJoinPseudoTokens _origin toks node = push "activateMemoryNodeWithJoinPseudoTokens" $ do
  currentTokenStatus <- do
    isEmpty <- isNullSet (node ^. memoryBlock . tokens)
    if isEmpty
      then pure OriginIsNewlyNonEmpty
      else pure OriginAlreadyHadTokens
  newTokens' <- (flip HM.traverseWithKey) toks \patMatch (JoinPseudoToken tokens) -> do
    -- only a single token from each node can be considered as a parent
    let parentTokens = createParentTokens tokens
        immediateParents = seqOf folded tokens
    newToken <- addNewMemToken node currentTokenStatus patMatch parentTokens immediateParents
    case newToken of
      AlreadyPresent -> pure ()
      FreshlyAdded tok -> do
        let (AccumulatedTokenMatches _ accumulated) = tokenMatchData tok
        assert (accumulated /= incompatibleSubstitution) "Join node produced incompatible substitution" (JoinNodeProducedIncompatibleSubstitution tok) ()
    pure newToken
  let newTokens = HM.fromList $ newTokens' ^@.. itraversed . _FreshlyAdded . from rawToken                  
  debug' $ viaShow (lengthOf folded newTokens) <+> " new tokens in memory node: " <+> viaShow newTokens
  children <- (the node) ^! childNodes
  debug' $ viaShow (lengthOf folded children) <+> " children in memory node: " <+> viaShow children
  when (has folded newTokens) do
    forMOf_ (folded . ignoringLevel) children \child ->
      -- fmap (withStrategy rpar)
      do
        activateNode (the node) currentTokenStatus newTokens child
data JoinNodeProducedIncompatibleSubstitution metadata = JoinNodeProducedIncompatibleSubstitution (WMEToken' metadata) deriving (Eq, Show, Exception)
-- TODO FIXME TODO FIXME refactor this into a general k-ary node activation procedure

{-# INLINEABLE activateJoinNode #-}
{-# SCC activateJoinNode #-}
activateJoinNode ::
  TokenFlowConstraints agent metadata r =>
  -- | Originating node
  (ReteNode metadata) ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) ->
  -- | The join node
  (ReteNode metadata) ? IsJoinNode metadata ->
  Sem r ()
activateJoinNode originatingNode toks currentNode = push "activateJoinNode" do
  child <- readT (currentNode ^. memoryNode)
  -- vvv HashSet is a monoid. Will give HS.empty if we don't have conditions.
  let conditions = setOf (joinConditions . folded) currentNode
  debug' $ "Conditions to test: " <+> viaShow conditions
  debug' $ "Toks in join: " <+> viaShow toks

  resultsToPropagate <- allSats conditions currentNode originatingNode toks
  debug' $ viaShow (lengthOf folded resultsToPropagate) <+> " results to propagate from join: " <+> viaShow resultsToPropagate
  when (has folded resultsToPropagate) $ do
    activateMemoryNodeWithJoinPseudoTokens currentNode resultsToPropagate child

{-# INLINEABLE activateNegativeNode #-}
{-# SCC activateNegativeNode #-}
activateNegativeNode ::
  TokenFlowConstraints agent metadata r =>
  -- | Originating node
  (ReteNode metadata) ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) ->
  -- | The join node
  (ReteNode metadata) ? IsNegativeNode metadata ->
  Sem r ()
activateNegativeNode originatingNode toks currentNode = push "activateNegativeNode" do
  amem <- currentNode ^! rightAmem
  if the amem == originatingNode
    then attr "rete.node.activation.chirality" (fromString "Right") $ rightActivateNegativeNode originatingNode toks currentNode
    else attr "rete.node.activation.chirality" (fromString "Left") $ leftActivateNegativeNode originatingNode toks currentNode

{-# INLINEABLE leftActivateNegativeNode #-}
{-# SCC leftActivateNegativeNode #-}
leftActivateNegativeNode ::
  TokenFlowConstraints agent metadata r =>
  -- | Originating node
  (ReteNode metadata) ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) ->
  -- | The join node
  (ReteNode metadata) ? IsNegativeNode metadata ->
  Sem r ()
leftActivateNegativeNode originatingNode toks currentNode = do
  -- vvv HashSet is a monoid. Will give HS.empty if we don't have conditions.

  currentTokenStatus <- do
    isEmpty <- isNullSet (currentNode ^. memoryBlock . tokens)
    if isEmpty
      then pure OriginIsNewlyNonEmpty
      else pure OriginAlreadyHadTokens
  -- currentTokens <- embed $ currentNode^!memoryBlock.tokens.extractSet

  let conditions = setOf (joinConditions . folded) currentNode
  debug' $ "Conditions to test: " <+> viaShow conditions
  debug' $ "Toks in join: " <+> viaShow toks
  amem <- currentNode ^! rightAmem
  amemTokens <- amem ^! memoryBlock . tokens . extractSet
  numberOfNonRightParents <- fmap (\l -> Cosmolude.length l - 1) $ currentNode ^!! parentNodes . folded
  resultsToPropagate <- allNegativeSats conditions currentNode originatingNode toks amemTokens LeftActivation numberOfNonRightParents
  debug' $ viaShow (lengthOf folded resultsToPropagate) <+> " results to propagate from negation: " <+> viaShow resultsToPropagate
  (flip HM.traverseWithKey) resultsToPropagate \patMatch t -> case t of
    NegationJoinSucceeded tokens -> push "NegationJoinSucceeded" do
      debug' $ "Originating node:" <+> render originatingNode
      debug' $ "Right amem:" <+> render amem

      let parentTokens = createParentTokens tokens
          immediateParents = seqOf folded tokens
      newToken <- addNewNegationToken currentNode currentTokenStatus parentTokens immediateParents
      case newToken of
        AlreadyPresent -> pure ()
        FreshlyAdded tok -> addNegationTokenResults tok
      let newTokens' = HM.singleton patMatch newToken
          newTokens = HM.fromList $ newTokens' ^@.. itraversed . _FreshlyAdded . from rawToken
      children <- (the currentNode) ^! childNodes
      debug' $ viaShow (lengthOf folded children) <+> " children in negation node: " <+> viaShow children
      forMOf_ (folded . ignoringLevel) children \child ->
        -- fmap (withStrategy rpar)
        do
          activateNode (the currentNode) currentTokenStatus newTokens child
    NegationMustRetract tokens -> push "NegationMustRetract" do
      debug' $ "Tokens to retract:" <+> render (tokens ^.. folded . filtered (has negativeJoinResults'))
      forMOf_ (folded . filtered (has negativeJoinResults')) tokens \(tok) -> do
        removeChildTokens tok
        let wme = patMatch ^. matchedWME
            jr = NegativeJoinResult tok wme
        wme ^! negativeJoinResults . inserting jr
        tok ^! negativeJoinResults' . inserting jr
    SomeNegationConditionsFailed -> push "SomeNegationConditionsFailed" do
      -- Prelude.error "lol"
      debug' $ "Some negation conditions failed for" <+> render patMatch
  pure ()

--   else do
--     toDelete <- embed $ currentNode^!!memoryBlock.tokens.elems
--     forM_ toDelete  removeToken
{-# INLINEABLE rightActivateNegativeNode #-}
{-# SCC rightActivateNegativeNode #-}
rightActivateNegativeNode ::
  TokenFlowConstraints agent metadata r =>
  -- | Originating node
  (ReteNode metadata) ->
  -- | New tokens
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) ->
  -- | The join node
  (ReteNode metadata) ? IsNegativeNode metadata ->
  Sem r ()
rightActivateNegativeNode originatingNode toks currentNode = do
  -- currentTokenStatus <-  do
  --   isEmpty <- isNullSet (currentNode^.memoryBlock.tokens)
  --   if isEmpty then
  --     pure OriginIsNewlyNonEmpty
  --   else
  --     pure OriginAlreadyHadTokens
  -- currentTokens <- embed $ currentNode^!memoryBlock.tokens.extractSet

  let conditions = setOf (joinConditions . folded) currentNode
  debug' $ "Conditions to test: " <+>viaShow conditions
  debug' $ "Toks in join: " <+> viaShow toks
  amem <- currentNode ^! rightAmem
  amemTokens <- amem ^! memoryBlock . tokens . extractSet
  numberOfNonRightParents <- fmap (\l -> Cosmolude.length l - 1) $ currentNode ^!! parentNodes . folded
  resultsToPropagate <- allNegativeSats conditions currentNode originatingNode toks amemTokens RightActivation numberOfNonRightParents
  debug' $ viaShow (lengthOf folded resultsToPropagate) <+> " results to propagate from negation: " <+> viaShow resultsToPropagate
  (flip HM.traverseWithKey) resultsToPropagate \patMatch t -> case t of
    NegationMustRetract tokens -> push "NegationMustRetract" do
      debug' $ "Tokens to retract:" <+> render (tokens ^.. folded . filtered (has negativeJoinResults'))
      forMOf_ (folded . filtered (has negativeJoinResults')) tokens \(tok) -> do
        removeChildTokens tok
        let wme = patMatch ^. matchedWME
            jr = NegativeJoinResult tok wme
        wme ^! negativeJoinResults . inserting jr
        tok ^! negativeJoinResults' . inserting jr
    NegationJoinSucceeded _toksToRetract -> push "NegationJoinSucceeded" do
      Prelude.error "lol?"
      pure ()
    SomeNegationConditionsFailed -> push "SomeNegationConditionsFailed" do
        pure ()
  pure ()
