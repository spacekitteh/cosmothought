module Rete.Dynamics.Joins.NegativeJoins where

import qualified Data.HashMap.Strict as HM
import qualified Data.Sequence as Seq
import Prelude (Int)
--import Polysemy.State
import Core.Patterns.PatternSyntax
import Data.Sequence.Lens
import Rete.Dynamics.TokenCreation
import Core.CoreTypes

import Rete.Dynamics.Joins.Common
import Rete.ReteTypes
import Rete.Types.Tokens
import Cosmolude
import qualified Data.HashSet as HS
import Data.HashSet.Lens
import Data.Semigroup


data NegationJoinConditionSatisfaction metadata
  = NegationJoinConditionFails
  | NegationJoinConditionMet (WMEToken' metadata) JoinCondition (PropagatingToken metadata)
  | NegationJoinConditionMustRetract (PreviouslyConsideredTokenCombo metadata)
  deriving (Show, Generic)

deriving instance (Hashable (WMEToken' metadata)) => Hashable (NegationJoinConditionSatisfaction metadata)

deriving instance (Eq (WMEToken' metadata)) => Eq (NegationJoinConditionSatisfaction metadata)

{-# INLINEABLE performNegationJoinConditionTest #-}
{-# SCC performNegationJoinConditionTest #-}
performNegationJoinConditionTest ::
  forall metadata.
  Renderable metadata =>
  -- | Existing, prior tokens, that have already been searched from prior rete network propagations. For a binary left activation, this will be the amem token.
  PreviouslyConsideredTokenCombo metadata ->
  JoinCondition ->
  -- | The new token to test the rest of the tokens against
  PropagatingToken metadata ->
  HashSet (WMEToken' metadata) ->
  NegationJoinConditionSatisfaction metadata
performNegationJoinConditionTest existingTokens joinCond propagatingToken rightTokens
  | allMatches <- (propagatingToken ^. tailMatches) <> existingTokens^.tailMatches, Just currentVal <-( allMatches ^? at (joinCond^.currentArgument . tokenNode) . _Just . match . {-propagatingMatch .-} matchedWME . field (joinCond ^. currentTokenField)) <|> propagatingToken ^? propagatingMatch . matchedWME . field (joinCond ^. currentTokenField),-- Just currentVal <- propagatingToken ^? propagatingMatch . matchedWME . field (joinCond ^. currentTokenField),
      (Just priorValue, priorToken) <-
          case joinCond ^. priorArgument of
            NodeArgument priorArg
                | Just priorTokenToGetFieldFrom <- (if (priorArg^.tokenNode) == (propagatingToken^.containingReteNode.nodeIdentifier) then Just (propagatingToken ^. rawToken) else (allMatches ^.   at (priorArg^.tokenNode)))  -> --Just priorTokenToGetFieldFrom <- allMatches ^. at (priorArg^.tokenNode) ->
                            (priorTokenToGetFieldFrom ^? match . matchedWME . field (priorArg ^. tokenField), priorTokenToGetFieldFrom)
            ValueArgument v -> (Just v, propagatingToken^.rawToken)
            _ -> (Nothing, propagatingToken^.rawToken),
                                               
      JoinPredicateMet _ _ _ <- performJoinPredicate currentVal (joinCond ^. joinTest) priorValue = 
        if (not (priorToken `HS.member` rightTokens) && not ((propagatingToken ^. rawToken) `HS.member` rightTokens))
          then NegationJoinConditionMet priorToken joinCond propagatingToken
          else NegationJoinConditionMustRetract existingTokens

performNegationJoinConditionTest _ _ _ _ = NegationJoinConditionFails

--------------------------------------------------------------------------------
-- Partial join matches
--------------------------------------------------------------------------------

-- data PartialJoinResult  metadata = PartialJoinResult {_conditionsUnmet :: Multimap NodeIdentifier JoinCondition, _conditionsMet :: Multimap NodeIdentifier (JoinConditionSatisfaction  metadata)

data NegationJoinSatisfaction metadata
  = SomeNegationConditionsFailed -- (WMEToken'  metadata) JoinCondition (WMEToken'  metadata)
  | NegationMustRetract (HashSet (WMEToken' metadata))
  | NegationJoinSucceeded (HashSet (WMEToken' metadata))
  deriving (Show, Generic)

instance Renderable metadata => Renderable (NegationJoinSatisfaction metadata) where
  render SomeNegationConditionsFailed = "Some negation conditions failed"
  render (NegationMustRetract toks) = "Negation must retract due to" <+> render (toks ^.. folded)
  render (NegationJoinSucceeded toks) = "Negation join succeeded with" <+> render (toks ^.. folded)

deriving instance (Eq (WMEToken' metadata)) => Eq (NegationJoinSatisfaction metadata)

deriving instance (Hashable (WMEToken' metadata)) => Hashable (NegationJoinSatisfaction metadata)

instance (Hashable (WMEToken' metadata)) => Semigroup (NegationJoinSatisfaction metadata) where
  {-# INLINE (<>) #-}
  (NegationMustRetract a) <> _ = NegationMustRetract a
  _ <> (NegationMustRetract b) = NegationMustRetract b
  SomeNegationConditionsFailed <> _ = SomeNegationConditionsFailed
  _ <> SomeNegationConditionsFailed = SomeNegationConditionsFailed
  (NegationJoinSucceeded lhs) <> (NegationJoinSucceeded rhs) = NegationJoinSucceeded (lhs <> rhs)

instance (Hashable (WMEToken' metadata)) => Monoid (NegationJoinSatisfaction metadata) where
  {-# INLINE mempty #-}
  mempty = NegationJoinSucceeded (HS.empty)

{-# INLINE negationJoinConditionSatisfactionToJoinSatisfaction #-}
negationJoinConditionSatisfactionToJoinSatisfaction :: (Hashable (WMEToken' metadata)) => NegationJoinConditionSatisfaction metadata -> NegationJoinSatisfaction metadata
negationJoinConditionSatisfactionToJoinSatisfaction (NegationJoinConditionMustRetract t) = NegationMustRetract (setOf (immediateParents . folded) t)
negationJoinConditionSatisfactionToJoinSatisfaction NegationJoinConditionFails = SomeNegationConditionsFailed
negationJoinConditionSatisfactionToJoinSatisfaction (NegationJoinConditionMet lhs _ rhs) = NegationJoinSucceeded (HS.singleton lhs <> HS.singleton (rhs ^. rawToken))

{-# INLINE evaluateNegationJoinConditions #-}
evaluateNegationJoinConditions :: Hashable (WMEToken' metadata) => HashSet (NegationJoinConditionSatisfaction metadata) -> NegationJoinSatisfaction metadata
evaluateNegationJoinConditions conds = (conds & setmapped %~ negationJoinConditionSatisfactionToJoinSatisfaction) ^. folded

{-# INLINE evaluateNegationJoin #-}
evaluateNegationJoin ::
  (Renderable metadata, Hashable (WMEToken' metadata)) =>
  HashSet JoinCondition ->
  -- | Existing, prior tokens. This is the token that ISN'T currently being propagated into the join node. For binary left activations, this will be the amem.
  PreviouslyConsideredTokenCombo metadata ->
  -- | The new, initiating token. For binary left activations, this will be the propagating token.
  PropagatingToken metadata ->
  HashSet (WMEToken' metadata) ->
  NegationJoinSatisfaction metadata
evaluateNegationJoin conditions joinTok activatingTok rightTokens = successfulJoinSatisfaction
  where
    evaluated = evaluateNegationJoinConditions satisfactions
    satisfactions =
      conditions
        & setmapped
          %~ \condition ->
            performNegationJoinConditionTest joinTok condition activatingTok rightTokens
    successfulJoinSatisfaction = case evaluated of
      NegationJoinSucceeded _ -> NegationJoinSucceeded (HS.fromList (activatingTok ^. rawToken : (joinTok ^.. immediateParents . folded)))
      s -> s


{-# INLINEABLE allNegativeSats #-}
allNegativeSats ::
  ( HasReteNode ((ReteNode metadata) ?  pred) (WMEToken' metadata) metadata, ProveCorrectSubType (ReteNode metadata) pred subtype, Renderable metadata,
    Members '[Log, OpenTelemetryContext, OpenTelemetrySpan,{-State (ProductionRuleActivity metadata),-} IDGen, Transactional] r,
    HasMemoryBlock' (WMEToken' metadata) metadata,
    HasCallStack,
    HasJoinConditions (WMEToken' metadata) metadata pred,
    HasMemoryBlock (ReteNode metadata ? pred) (WMEToken' metadata) metadata
  ) =>
  HashSet JoinCondition ->
  (ReteNode metadata) ? pred  ->
  ReteNode metadata ->
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) ->
  HashSet (WMEToken' metadata) ->
  LeftOrRightActivation -> 
  Int -> 
  Sem r (HashMap (WMEPatternMatch' metadata) (NegationJoinSatisfaction metadata)) -- JoinSatisfaction  metadata))
allNegativeSats conditions currentNode nodeTokensCameFrom propagatingTokens rightTokens leftOrRight numberOfNonRightParents = do
  sets <- mapM (allNegationTokenSets  currentNode nodeTokensCameFrom -- (seqOf folded rightTokens)
                numberOfNonRightParents leftOrRight) propagatingTokens
  debug' $ "Sets of tokens: " <+> viaShow sets
  let testedJoins = foldMap (\pctc -> negationSatisfactions pctc conditions propagatingTokens  rightTokens numberOfNonRightParents ) (fold sets)
  debug' $ "Tested join conditions: " <+> viaShow testedJoins
  amem <-  currentNode ^!  rightAmem
  if (leftOrRight == LeftActivation && numberOfNonRightParents == 1 && nodeTokensCameFrom /= the amem ) then do --TODO TODO TODO get rid of the null test. but then weird stuff happens x.x need to generate the join sets properly
      pure testedJoins 
    else
      if (leftOrRight == RightActivation && numberOfNonRightParents == 1 && nodeTokensCameFrom == the amem) then do
        pure $ fmap ( \case
                        NegationJoinSucceeded toks -> NegationMustRetract toks
                        x -> x )  testedJoins
       else do
        pure HM.empty
 

{-# INLINEABLE allNegationTokenSets #-}
allNegationTokenSets ::
  ( Renderable metadata,
    HasMemoryBlock' (WMEToken' metadata) metadata,
    HasCallStack,
    Members '[Log, OpenTelemetryContext, OpenTelemetrySpan,IDGen, Transactional] r,
        HasJoinConditions (WMEToken' metadata) metadata pred,
        HasMemoryBlock (ReteNode metadata ? pred) (WMEToken' metadata) metadata,
        ProveCorrectSubType (ReteNode metadata) pred subtype,
        HasReteNode ((ReteNode metadata) ?  pred) (WMEToken' metadata) metadata
  ) =>
  (ReteNode metadata) ? pred ->
  ReteNode metadata ->
  Int ->
  LeftOrRightActivation -> 
  PropagatingToken metadata ->
  Sem r (Seq (PreviouslyConsideredTokenCombo metadata))
allNegationTokenSets currentNode nodeTokenCameFrom numberOfNonRightParents leftOrRight propagatedToken   = do

  case leftOrRight of
        LeftActivation -> do
          tokenSequences <-  do
              rightAmemIdent <- currentNode^!rightAmem.nodeIdentifier
              tokenSets <- fmap Seq.fromList $ (the currentNode) ^!! parentNodes . folded . ignoringLevel . filtered (\n -> (n ^. nodeIdentifier) /= (nodeTokenCameFrom ^. nodeIdentifier) && n^.nodeIdentifier /= rightAmemIdent) . memory . tokens
              traverse (perform toSeq) tokenSets
          tokCartesianProductForNegationOnLeft currentNode propagatedToken tokenSequences numberOfNonRightParents
        RightActivation -> do
          tokenSequences <- do
            tokenSets <- fmap Seq.fromList $ currentNode^!!memoryBlock.tokens
            traverse (perform toSeq) tokenSets
          tokCartesianProduct currentNode propagatedToken tokenSequences  --undefined

{-# INLINEABLE negationSatisfactions #-}
negationSatisfactions ::(Renderable metadata,  Functor f) => PreviouslyConsideredTokenCombo metadata -> HashSet JoinCondition -> f (PropagatingToken metadata) -> HashSet (WMEToken' metadata) ->  Int -> f (NegationJoinSatisfaction metadata)
negationSatisfactions previouslyConsideredTokens conditions propagatingTokens  rightTokens _numberOfNonRightParents=
  propagatingTokens
    & mapped
      %~ \propagatingToken ->
        ( evaluateNegationJoin
            conditions
            previouslyConsideredTokens
            propagatingToken
            rightTokens
        )

{-# INLINEABLE tokCartesianProductForNegationOnLeft #-}
tokCartesianProductForNegationOnLeft ::
  (Renderable metadata, Members '[Log, OpenTelemetryContext, OpenTelemetrySpan,IDGen, Transactional] r,     HasJoinConditions (WMEToken' metadata) metadata pred) =>
  (ReteNode metadata) ? pred ->
  PropagatingToken metadata -> -- The propagating token. In binary left activations, this is the beta token.
  Seq (Seq (WMEToken' metadata)) ->
  Int -> 
  Sem r (Seq (PreviouslyConsideredTokenCombo metadata))
tokCartesianProductForNegationOnLeft node propagatingToken toksInEachParent numberOfNonRightParents = do
  debug' $ "Cartesian product propagatingToken: " <+> viaShow propagatingToken
  debug' $ "Cartesian product toksInEachParent: " <+> viaShow toksInEachParent
  debug' $ "Cartesian product: " <+> viaShow cartesianProduct
  debug' $ "Non-right parents: " <+> viaShow numberOfNonRightParents
  debug' $ "Non-empty parents' tokens: " <+> viaShow (toksInEachParent^.. filtered (has folded) )
  forM cartesianProduct \combo -> do
    ident <- TokenIdentifier <$> newGlobalIdentifier
    let immediateParentMatches =           ( HM.fromList $
              ( combo
                  ^.. folded
                    . to (\tok -> (tok ^. containingReteNode . nodeIdentifier, tok))
              )
          )
        parentMatches = immediateParentMatches  <> (combo ^. folded . tailMatches)
        truth = (foldByOf (folded.accumulatedTruth) (/\) top combo)
    ctd <-  newCommonTokenData ident (the node) -- (propagatingToken ^. match)
                       parentMatches combo truth -- TODO FIXME negate the right token truth
    pure (PreviouslyConsideredTokenCombo ctd)
  where
    cartesianProduct = if ( lengthOf (folded . filtered (has folded)) toksInEachParent == (numberOfNonRightParents - 1)) then (seqOf ( folded  . filtered (has folded)) toksInEachParent ) <> (Seq.singleton (Seq.singleton (propagatingToken^.rawToken)))
                     else sequence (toksInEachParent)
        
    
