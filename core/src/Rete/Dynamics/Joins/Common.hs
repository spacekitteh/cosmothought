module Rete.Dynamics.Joins.Common where

import Core.CoreTypes

import qualified Data.HashMap.Strict as HM
import Rete.Dynamics.TokenCreation
import Rete.ReteTypes

import Rete.Types.Tokens
import Cosmolude






--------------------------------------------------------------------------------
-- Join predicate testing
--------------------------------------------------------------------------------
{-# INLINE performJoinPredicate #-}
performJoinPredicate :: Value -> JoinPredicate -> Value -> JoinPredicateSatisfaction
performJoinPredicate a EQ b | a == b = JoinPredicateMet a EQ b
performJoinPredicate a'@(Constant a) LT b'@(Constant b) | asRational a < asRational b = JoinPredicateMet a' LT b'
performJoinPredicate a'@(Constant a) LTE b'@(Constant b) | asRational a <= asRational b = JoinPredicateMet a' LTE b'
performJoinPredicate a'@(Constant a) GT b'@(Constant b) | asRational a > asRational b = JoinPredicateMet a' GT b'
performJoinPredicate a'@(Constant a) GTE b'@(Constant b) | asRational a >= asRational b = JoinPredicateMet a' GTE b'
performJoinPredicate _ _ _ = JoinPredicateFails

--------------------------------------------------------------------------------
-- Join condition testing
--------------------------------------------------------------------------------
data JoinConditionSatisfaction metadata
  = JoinConditionFails
  | JoinConditionMet (WMEToken' metadata) JoinCondition (PropagatingToken metadata)
  deriving (Show, Generic)

deriving instance (Hashable (WMEToken' metadata)) => Hashable (JoinConditionSatisfaction metadata)

deriving instance (Eq (WMEToken' metadata)) => Eq (JoinConditionSatisfaction metadata)

{-# INLINEABLE tokCartesianProduct #-}
tokCartesianProduct ::
  (Renderable metadata, Members '[OpenTelemetryContext, OpenTelemetrySpan,Log, IDGen, Transactional] r) =>
  (ReteNode metadata) ? pred ->
  PropagatingToken metadata -> -- The propagating token. In binary left activations, this is the beta token.
  Seq (Seq (WMEToken' metadata)) ->
  Sem r (Seq (PreviouslyConsideredTokenCombo metadata))
tokCartesianProduct node propagatingToken toksInEachParent = do
  debug' $ "Cartesian product containing: " <+> viaShow propagatingToken
  debug' $ "Cartesian product with: " <+> viaShow toksInEachParent
  debug' $ "Cartesian product: " <+> viaShow cartesianProduct
  forM cartesianProduct \combo -> do
    ident <- TokenIdentifier <$> newGlobalIdentifier
    let parentMatches =
          ( HM.fromList $
              ( combo
                  ^.. folded
                    . to (\tok -> (tok ^. containingReteNode . nodeIdentifier, tok))
              )
          )
            <> (combo ^. folded . tailMatches)
        truth = (foldByOf (folded.accumulatedTruth) (/\) top combo)
    ctd <-  newCommonTokenData ident (the node) -- (propagatingToken ^. match)
                       parentMatches combo truth
    pure (PreviouslyConsideredTokenCombo ctd)
  where
    cartesianProduct = sequence toksInEachParent

data JoinPseudoToken metadata = JoinPseudoToken (HashSet (WMEToken' metadata)) deriving (Show)
