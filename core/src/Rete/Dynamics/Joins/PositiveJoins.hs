module Rete.Dynamics.Joins.PositiveJoins where
import Cosmolude
--import Polysemy.Reader
--import OpenTelemetry.Trace.Core
import Polysemy.Reader    
import qualified Data.Sequence as Seq
import qualified Data.HashMap.Strict as HM
import Rete.Dynamics.Joins.Common

import Rete.ReteTypes
import Core.CoreTypes
import Rete.Types.Tokens

import qualified Data.HashSet as HS
import Data.HashSet.Lens
import Data.Semigroup
--import qualified Prelude

{-# INLINEABLE performJoinConditionTest #-}
{-# SCC performJoinConditionTest #-}
performJoinConditionTest ::
  (HasTracer agent, Renderable metadata, HasCallStack, Members '[OpenTelemetryContext, OpenTelemetrySpan,Log,IDGen,  Transactional, Reader agent, Resource] r,
  Hashable (WMEToken' metadata)) => 
  -- | Existing, prior tokens, that have already been searched from prior rete network propagations. For a binary left activation, this will be the amem token.
  PreviouslyConsideredTokenCombo metadata ->
  JoinCondition ->
  -- | The new token to test the rest of the tokens against
  PropagatingToken metadata ->
  Sem r (JoinConditionSatisfaction metadata)
performJoinConditionTest existingTokens joinCond propagatingToken -- = push "performJoinConditionTest" do
--   debug' $ "Existing tokens:" <+> render existingTokens
--   debug' $ "Propagating token:" <+> render propagatingToken
--   debug' $ "Current argument (propagating):" <+> render (joinCond ^.currentArgument)
--   debug' $ "Prior argument (existing):" <+> render (joinCond ^. priorArgument)
--   let allMatches = (propagatingToken ^. tailMatches) <> existingTokens^.tailMatches
--   let curVal =  ( allMatches ^? at (joinCond^.currentArgument . tokenNode) . _Just . match . {-propagatingMatch .-} matchedWME . field (joinCond ^. currentTokenField)) <|> propagatingToken ^? propagatingMatch . matchedWME . field (joinCond ^. currentTokenField)
--   case curVal of
--     Nothing -> pure JoinConditionFails
--     Just currentVal -> do
--       debug' $ "Current value:" <+> render currentVal
--       case joinCond^.priorArgument of
--         ValueArgument _v -> Prelude.undefined
--         NodeArgument priorArg -> do
--           debug' $ "Prior argument:" <+> render priorArg
--           case (if (priorArg^.tokenNode) == (propagatingToken^.containingReteNode.nodeIdentifier) then Just (propagatingToken ^. rawToken) else (allMatches ^.   at (priorArg^.tokenNode)))  of
--             Nothing -> do debug' "No prior token"
-- --                          debug' $ "Is there a match in propagating?" <+> render 
--                           debug' $ "Existing tail matches:" <+> render (existingTokens^.tailMatches)
--                           pure JoinConditionFails
--             Just priorTokenToGetFieldFrom -> do
--               debug' $ "Prior token to get field from:" <+> render priorTokenToGetFieldFrom
--               let priorValue' = priorTokenToGetFieldFrom ^? match . matchedWME . field (priorArg ^. tokenField)
--                   priorToken = priorTokenToGetFieldFrom
--               debug' $ "Prior value:" <+> render priorValue'
--               case priorValue' of
--                 Nothing -> do
--                          debug' $ "No prior value?"
--                          pure JoinConditionFails
--                 Just priorValue -> do
--                   case performJoinPredicate currentVal (joinCond^.joinTest) priorValue of
--                     JoinPredicateMet a b c -> do
--                       debug' $ "a:" <+> render a
--                       debug' $ "b:" <+> render b
--                       debug' $ "c:" <+> render c
--                       pure $ JoinConditionMet priorToken joinCond propagatingToken
--                     _ -> do
--                         debug' "Join condition failed"
--                         pure JoinConditionFails





  | allMatches <- (propagatingToken ^. tailMatches) <> existingTokens^.tailMatches, Just currentVal <-( allMatches ^? at (joinCond^.currentArgument . tokenNode) . _Just . match . {-propagatingMatch .-} matchedWME . field (joinCond ^. currentTokenField)) <|> propagatingToken ^? propagatingMatch . matchedWME . field (joinCond ^. currentTokenField) -- propagatingToken ^? propagatingMatch . matchedWME . field (joinCond ^. currentTokenField)
                  ,
      (Just priorValue, priorToken) <-
          case joinCond ^. priorArgument of
            NodeArgument priorArg
                | Just priorTokenToGetFieldFrom <- (if (priorArg^.tokenNode) == (propagatingToken^.containingReteNode.nodeIdentifier) then Just (propagatingToken ^. rawToken) else (allMatches ^.   at (priorArg^.tokenNode))) {-allmatches ^. at (priorArg^.tokenNode)-} ->
                            (priorTokenToGetFieldFrom ^? match . matchedWME . field (priorArg ^. tokenField), priorTokenToGetFieldFrom)
            ValueArgument v -> (Just v, propagatingToken^.rawToken)
            _ -> (Nothing, propagatingToken^.rawToken),
                                               
      JoinPredicateMet _ _ _ <- performJoinPredicate currentVal (joinCond ^. joinTest) priorValue = 
        pure $ JoinConditionMet priorToken joinCond propagatingToken

performJoinConditionTest _ _ _ = pure $ JoinConditionFails


--                                 the memeory node isnt being activated??//


--------------------------------------------------------------------------------
-- Join testing
--------------------------------------------------------------------------------

data JoinSatisfaction metadata
  = SomeConditionsFailed -- (WMEToken'  metadata) JoinCondition (WMEToken'  metadata)
  | JoinSucceeded (HashSet (WMEToken' metadata))
  deriving (Show, Generic)

deriving instance (Eq (WMEToken' metadata)) => Eq (JoinSatisfaction metadata)

deriving instance (Hashable (WMEToken' metadata)) => Hashable (JoinSatisfaction metadata)

instance (Hashable (WMEToken' metadata)) => Semigroup (JoinSatisfaction metadata) where
  {-# INLINE (<>) #-}
  SomeConditionsFailed <> _ = SomeConditionsFailed
  _ <> SomeConditionsFailed = SomeConditionsFailed
  (JoinSucceeded lhs) <> (JoinSucceeded rhs) = JoinSucceeded (lhs <> rhs)

instance (Hashable (WMEToken' metadata)) => Monoid (JoinSatisfaction metadata) where
  {-# INLINE mempty #-}
  mempty = JoinSucceeded (HS.empty)

{-# INLINE joinConditionSatisfactionToJoinSatisfaction #-}
joinConditionSatisfactionToJoinSatisfaction :: (Hashable (WMEToken' metadata)) => JoinConditionSatisfaction metadata -> JoinSatisfaction metadata
joinConditionSatisfactionToJoinSatisfaction JoinConditionFails = SomeConditionsFailed
joinConditionSatisfactionToJoinSatisfaction (JoinConditionMet lhs _ rhs) = JoinSucceeded (HS.singleton lhs <> HS.singleton (rhs ^. rawToken))

{-# INLINE evaluateJoinConditions #-}
evaluateJoinConditions :: Hashable (WMEToken' metadata) => HashSet (JoinConditionSatisfaction metadata) -> JoinSatisfaction metadata
evaluateJoinConditions conds = (conds & setmapped %~ joinConditionSatisfactionToJoinSatisfaction) ^. folded

{-# INLINE evaluateJoin #-}
evaluateJoin ::
  (HasTracer agent, Renderable metadata, HasCallStack, Members '[OpenTelemetryContext, OpenTelemetrySpan,Log,IDGen,  Transactional, Reader agent, Resource] r,
  Hashable (WMEToken' metadata)) =>
  HashSet JoinCondition ->
  -- | Existing, prior tokens. This is the token that ISN'T currently being propagated into the join node. For binary left activations, this will be the amem.
  PreviouslyConsideredTokenCombo metadata ->
  -- | The new, initiating token. For binary left activations, this will be the propagating token.
  PropagatingToken metadata ->
  Sem r (JoinSatisfaction metadata)
evaluateJoin conditions joinTok activatingTok = push "evaluateJoin" do
    debug' $ "conditions:" <+> render conditions
    debug' $ "joinTok:" <+> render joinTok
    debug' $ "activatingTok:" <+> render activatingTok
    sats <- HS.fromList <$> conditions^!!folded .  act  (\condition ->   performJoinConditionTest joinTok condition activatingTok)
    debug' $ "satisfactions:" <+> viaShow sats
    let evaluated = evaluateJoinConditions sats
    debug' $ "evaluateD:" <+> viaShow evaluated
    let successfulJoinSatisfaction = case evaluated of
          JoinSucceeded _ -> JoinSucceeded (HS.fromList (activatingTok ^. rawToken : (joinTok ^.. immediateParents . folded)))
          _ -> SomeConditionsFailed
    debug' $ "success?" <+> viaShow successfulJoinSatisfaction
    pure successfulJoinSatisfaction
  where
    
    

{-# INLINEABLE satisfactions #-}
satisfactions :: (HasCallStack, HasTracer agent, Renderable metadata,
    Members '[OpenTelemetryContext, OpenTelemetrySpan,Log, IDGen, Transactional, Reader agent, Resource] r, Traversable f, Renderable metadata) => PreviouslyConsideredTokenCombo metadata -> HashSet JoinCondition -> f (PropagatingToken metadata) -> Sem r (f (JoinSatisfaction metadata))
satisfactions previouslyConsideredTokens conditions propagatingTokens = push "satisfactions" do
  results <- forM propagatingTokens
                 \propagatingToken -> do
                          
                          evaluated <-  evaluateJoin
                            conditions
                            previouslyConsideredTokens
                            propagatingToken
                          pure evaluated
  pure results
{-# INLINEABLE allTokenSets #-}
allTokenSets ::
  ( Renderable metadata,
    HasMemoryBlock' (WMEToken' metadata) metadata,
    HasCallStack,
                HasTracer agent,
    Members '[OpenTelemetryContext, OpenTelemetrySpan,Log,IDGen,  Transactional, Reader agent, Resource] r,
        HasJoinConditions (WMEToken' metadata) metadata pred
  ) =>
  (ReteNode metadata) ? pred ->
  ReteNode metadata ->
  PropagatingToken metadata ->
  Sem r (Seq (PreviouslyConsideredTokenCombo metadata))
allTokenSets currentNode nodeTokenCameFrom propagatedToken = push "allTokenSets" $ do
  tokenSequences <-  do
    tokenSets <- fmap Seq.fromList $ (the currentNode) ^!! parentNodes . folded . ignoringLevel . filtered (\n -> (n ^. nodeIdentifier) /= (nodeTokenCameFrom ^. nodeIdentifier)) . memory . tokens
    traverse (perform toSeq) tokenSets
  debug' $ "tokenSequences: " <+> viaShow tokenSequences
  afterCartesian <- tokCartesianProduct currentNode propagatedToken (tokenSequences)
  debug' $ "afterCartesian:" <+> viaShow afterCartesian
  pure afterCartesian


{-# INLINEABLE allSats #-}
allSats ::
  ( Renderable metadata,
--    HasProductionRuleActivity agent metadata,
    HasTracer agent,
    Members '[OpenTelemetryContext, OpenTelemetrySpan,Log, IDGen, Transactional, Reader agent, Resource] r,
    HasMemoryBlock' (WMEToken' metadata) metadata,
    HasCallStack,
    HasJoinConditions (WMEToken' metadata) metadata pred
  ) =>
  HashSet JoinCondition ->
  (ReteNode metadata) ? pred  ->
  ReteNode metadata ->
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) ->
  Sem r (HashMap (WMEPatternMatch' metadata) (JoinPseudoToken metadata)) -- JoinSatisfaction  metadata))
allSats conditions currentNode nodeTokensCameFrom propagatingTokens = do
  sets <- mapM (allTokenSets currentNode nodeTokensCameFrom) propagatingTokens
  debug' $ "Sets of tokens: " <+> viaShow sets
  testedJoins <- mapM (\pctc -> satisfactions pctc conditions propagatingTokens) (fold sets)
  debug' $ "Tested join conditions: " <+> viaShow testedJoins
  let res' = fmap (\joined ->  HM.mapMaybe
              ( ( \sat -> case sat of
                            JoinSucceeded successfulTokenCombo | (AccumulatedTokenMatches _ sub) <- combineTokenMatches successfulTokenCombo, sub /= incompatibleSubstitution -> Just (JoinPseudoToken successfulTokenCombo)
                            _ -> Nothing
                )
              )
              joined
            ) testedJoins
      res = HM.unions (res'^..folded)
  debug' $ "results:" <+> viaShow res
  pure res

