module Rete.Dynamics.TokenCreation where

import Rete.Types.Tokens
import Data.Refined
import Core.CoreTypes
import Polysemy.Reader (Reader)


import Rete.ReteTypes
import Core.Identifiers.IdentifierGenerator
import Cosmolude hiding ((...))

import StmContainers.Set as STMS







{-# INLINEABLE newCommonTokenData #-}
newCommonTokenData :: Member Transactional r => TokenIdentifier -> ReteNode metadata ->  HashMap NodeIdentifier (WMEToken' metadata) -> Seq (WMEToken' metadata) -> WMETruth -> Sem r (CommonTokenData metadata)
newCommonTokenData i node  parentMatches immediateParents truth =
  CommonTokenData i node parentMatches immediateParents <$> newSet <*> pure truth



-- | Create a new token and add it to a memory node
{-# INLINEABLE addNewMemToken #-}
addNewMemToken :: forall agent metadata r. (HasTracer agent, Renderable metadata, HasMemoryBlock' (WMEToken' metadata) metadata, HasWMETokens (WME' metadata) metadata, Hashable (WMEToken' metadata), HasCallStack, Members [Reader agent, OpenTelemetrySpan, OpenTelemetryContext , Resource, Log, Transactional, IDGen] r) => (ReteNode metadata) ? IsMemoryNode metadata ->  OriginIsNewlyNonEmpty metadata -> WMEPatternMatch' metadata -> HashMap NodeIdentifier (WMEToken' metadata) -> Seq (WMEToken' metadata) -> Sem r (TokenAddition metadata)
addNewMemToken n newlyNonEmpty patMatch parentToks immediateParents = do
  newIdentifier <- TokenIdentifier <$> newLocalIdentifierInContext ((the n) ^. identifier)
  commonData' <- newCommonTokenData newIdentifier (the n) parentToks immediateParents (foldByOf (folded.accumulatedTruth) (/\) top immediateParents)
  let token = MemToken patMatch (n^.memoryBlock) commonData'
  debug' $ "Trying to add" <+> viaShow token
  addNewGivenToken (the n) newlyNonEmpty token


-- | Create a new token and add it to an alpha node
{-# INLINEABLE addNewAlphaToken #-}
addNewAlphaToken :: forall agent metadata r. (HasTracer agent, Renderable metadata, HasMemoryBlock' (WMEToken' metadata) metadata, HasWMETokens (WME' metadata) metadata, Hashable (WMEToken' metadata), HasCallStack, Members [ Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource ,Log, Transactional, IDGen] r) => (ReteNode metadata) ? IsAlphaNode metadata ->  OriginIsNewlyNonEmpty metadata ->WMEPatternMatch' metadata -> HashMap NodeIdentifier (WMEToken' metadata) -> Seq (WMEToken' metadata) -> Sem r (TokenAddition metadata)
addNewAlphaToken n newlyNonEmpty patMatch parentToks immediateParents = do
  newIdentifier <- TokenIdentifier <$> newLocalIdentifierInContext ((the n) ^. identifier)
  commonData' <- newCommonTokenData newIdentifier (the n) parentToks immediateParents (foldByOf (folded.accumulatedTruth) (/\) top immediateParents)
  let token = MemToken patMatch (n^.memoryBlock) commonData'
  debug' $ "Trying to add" <+> viaShow token
  addNewGivenToken (the n) newlyNonEmpty token

{-# INLINEABLE addNegationTokenResults #-}
addNegationTokenResults :: forall agent metadata r. (HasTracer agent, Renderable metadata, HasMemoryBlock' (WMEToken' metadata) metadata, HasWMETokens (WME' metadata) metadata, Hashable (WMEToken' metadata), HasCallStack, Members [Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource , Log, Transactional, IDGen] r) =>  WMEToken' metadata -> Sem r ()
addNegationTokenResults token = 
  forMOf_ (immediateParents.folded.match.matchedWME) token \wme -> do
      let jr = NegativeJoinResult token wme
      wme^!negativeJoinResults . inserting  jr
      token^!negativeJoinResults' . inserting jr


{-# INLINEABLE addNewNCCToken #-}
addNewNCCToken :: forall agent metadata r. (HasTracer agent, Renderable metadata, HasMemoryBlock' (WMEToken' metadata) metadata, HasWMETokens (WME' metadata) metadata, Hashable (WMEToken' metadata), HasCallStack, Members [ Reader agent, OpenTelemetrySpan, OpenTelemetryContext , Resource,Log, Transactional,  IDGen] r) => (ReteNode metadata) ? IsNCCNode metadata ->  OriginIsNewlyNonEmpty metadata ->  HashMap NodeIdentifier (WMEToken' metadata) -> Seq (WMEToken' metadata) -> Sem r (TokenAddition metadata)
addNewNCCToken n newlyNonEmpty parentToks immediateParents = do
  newIdentifier <- TokenIdentifier <$> newLocalIdentifierInContext ((the n) ^. identifier)
  commonData' <-  newCommonTokenData newIdentifier (the n) parentToks immediateParents (foldByOf (folded.accumulatedTruth) (/\) top immediateParents)
  results <- embedSTM STMS.new
  let token = NCCToken  results (n^.memoryBlock) commonData'
  addNewGivenToken (the n) newlyNonEmpty token


{-# INLINEABLE addNewNCCPartnerToken #-}
addNewNCCPartnerToken :: forall agent metadata r. (HasTracer agent, Renderable metadata, HasMemoryBlock' (WMEToken' metadata) metadata, HasWMETokens (WME' metadata) metadata, Hashable (WMEToken' metadata), HasCallStack, Members [Reader agent, OpenTelemetrySpan, OpenTelemetryContext , Resource, Log, Transactional,  IDGen] r) => (ReteNode metadata) ? IsNCCPartnerNode metadata ->  OriginIsNewlyNonEmpty metadata ->  HashMap NodeIdentifier (WMEToken' metadata) -> Seq (WMEToken' metadata) -> Sem r (TokenAddition metadata)
addNewNCCPartnerToken n newlyNonEmpty parentToks immediateParents = do
  newIdentifier <- TokenIdentifier <$> newLocalIdentifierInContext ((the n) ^. identifier)
  commonData' <-  newCommonTokenData newIdentifier (the n) parentToks immediateParents (foldByOf (folded.accumulatedTruth) (/\) top immediateParents)
  owner <- newT Nothing
  let token = NegatedConjunctionPartnerToken owner (n^.memoryBlock) commonData'
  addNewGivenToken (the n) newlyNonEmpty token

-- | Create a new token and add it to a negative node
{-# INLINEABLE addNewNegationToken #-}
addNewNegationToken :: forall agent metadata r. (HasTracer agent, Renderable metadata, HasMemoryBlock' (WMEToken' metadata) metadata, HasWMETokens (WME' metadata) metadata, Hashable (WMEToken' metadata), HasCallStack, Members [Reader agent, OpenTelemetrySpan, OpenTelemetryContext ,Resource,  Log, Transactional, IDGen] r) => (ReteNode metadata) ? IsNegativeNode metadata -> 
  OriginIsNewlyNonEmpty metadata -> HashMap NodeIdentifier (WMEToken' metadata) -> Seq (WMEToken' metadata) -> Sem r (TokenAddition metadata)
addNewNegationToken n newlyNonEmpty parentToks immediateParents = do
  newIdentifier <- TokenIdentifier <$> newLocalIdentifierInContext ((the n) ^. identifier)
  commonData' <-  newCommonTokenData newIdentifier (the n) parentToks immediateParents (neg $ foldByOf (folded.accumulatedTruth) (/\) top immediateParents)
  wmes <- newSet
  let token = NegatedToken wmes  (n^.memoryBlock) commonData'
  addNewGivenToken (the n) newlyNonEmpty token

-- | Create a new token and add it to a production node
{-# INLINEABLE addNewProductionToken #-}
addNewProductionToken :: forall agent metadata r. (HasTracer agent, Renderable metadata, HasMemoryBlock' (WMEToken' metadata) metadata, HasWMETokens (WME' metadata) metadata, Hashable (WMEToken' metadata), HasCallStack, Members [Reader agent, OpenTelemetrySpan, OpenTelemetryContext , Resource, Log, Transactional,  IDGen] r) => (ReteNode metadata) ? IsProductionNode metadata ->  OriginIsNewlyNonEmpty metadata ->  HashMap NodeIdentifier (WMEToken' metadata) -> Seq (WMEToken' metadata) -> Sem r (TokenAddition metadata)
addNewProductionToken n newlyNonEmpty parentToks immediateParents = do
  newIdentifier <- TokenIdentifier <$> newLocalIdentifierInContext ((the n) ^. identifier)
  commonData' <-  newCommonTokenData newIdentifier (the n) parentToks immediateParents (foldByOf (folded.accumulatedTruth) (/\) top immediateParents)
  let token = ProductionToken (n^.memoryBlock) commonData'
  addNewGivenToken (the n) newlyNonEmpty token



  

-- | Create a new token and add it to the memory block
{-# INLINEABLE addNewGivenToken #-}
addNewGivenToken :: forall agent metadata r. (HasTracer agent, Renderable metadata, HasMemoryBlock' (WMEToken' metadata) metadata, HasWMETokens (WME' metadata) metadata, Hashable (WMEToken' metadata), HasCallStack, Members [Reader agent, OpenTelemetrySpan, OpenTelemetryContext , Resource,Log, Transactional, IDGen] r) => ReteNode metadata ->  OriginIsNewlyNonEmpty metadata -> WMEToken' metadata -> Sem r (TokenAddition metadata)
addNewGivenToken n _newlyNonEmpty token = push "addNewGivenToken" do
  existingToks <- n^!!memory.tokens.elems
  debug' $ "Existing tokens:" <+> viaShow existingToks
  tokenAlreadyPresent <-  n^!memory.tokens . act (lookupSet token) . to Any
  debug' $ "Already present?" <+> viaShow tokenAlreadyPresent
  inserted <- do
    tokenAlreadyPresent <-  n^!memory.tokens . act (lookupSet token) . to Any
    if getAny tokenAlreadyPresent then
      pure False
      else do
        n ^! memory . tokens . inserting token
        pure True
  if inserted then do
    debug' $ "Inserting token" <+> viaShow token
    let wmes = ((token^..match.matchedWME)) <>  token^..tailMatches.folded.match.matchedWME
    push "AllMatchedWMEsForToken" $ do
      forM_ wmes \wme -> do
         wme^!ownedTokens.inserting token
    forMOf_ (immediateParents.folded) token \parent -> do
      parent^!childTokens.inserting token
    pure $ FreshlyAdded token
  else 
     pure AlreadyPresent
