module Rete.Dynamics.Activation where

import Cosmolude
import Polysemy

data ReteNodeActivation metadata m a where
  ActivateNode ::   -- | Originating node
    ReteNode metadata ->
  -- | Did origin just become nonempty?
    OriginIsNewlyNonEmpty metadata ->
  -- | New tokens
    HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) -> -------- TODO MAYBE change tokens to have only alpha tokens contain WMEPatternMatch'es.

  -- | The activated node
    ReteNode metadata ->
    ReteNodeActivation metadata m ()


makeSem ''ReteNodeActivation

