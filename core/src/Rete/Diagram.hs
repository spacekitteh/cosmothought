{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC -Wwarn=x-partial #-}
module Rete.Diagram (renderAllReteNodes, renderReteNetworkGraphForRule, ReteNodeGraphNodeData(..), ReteGraph, RuleNodesNotInReteEnvironment(..)) where
-- import Diagrams.Backend.PGF
-- import           Data.GraphViz.Commands
import Control.Exception

--import Data.Text.Lens (unpacked)
import Rete.Environment
import Rete.ReteTypes
import Core.Rule
import Core.Rule.Database
import Cosmolude hiding (note, vsep, (#))
import Data.Graph.Inductive.PatriciaTree
import Data.GraphViz
import Data.GraphViz.Attributes.Complete 
import qualified Data.Text as T
--import Diagrams.Backend.SVG
import Diagrams.Prelude hiding (render)
import Diagrams.TwoD.GraphViz
import Diagrams.TwoD.Text
import GHC.Stack
import Polysemy.Embed
import Polysemy.Error
import Polysemy.Reader hiding (local)
import Prettyprinter.Render.Text
import Prelude (Double, head, fromIntegral, (/))


    
data ReteNodeGraphNodeData metadata = Alpha ((ReteNode metadata) ? IsAlphaNode metadata) WMEPattern MemoryConstantTestHashKey
                                    | Prod ((ReteNode metadata) ? IsProductionNode metadata) Rule (Substitution Variable)
                                    | Join ((ReteNode metadata) ? IsJoinNode metadata) (HashSet JoinCondition)
                                    | NCCP ((ReteNode metadata) ? IsNCCPartnerNode metadata) ((ReteNode metadata) ? IsNCCNode metadata) 
                                    | Other (ReteNode metadata)
                                      deriving (Eq, Ord, Show)
{-# INLINE underlyingNode #-}                                      
underlyingNode :: Getter (ReteNodeGraphNodeData metadata) (ReteNode metadata)
underlyingNode = to go where
    go (Alpha  a _ _) = the a
    go (Prod p _ _) = the p
    go (NCCP nccp _) = the nccp
    go (Join j _) = the j
    go (Other o) = o
type ReteGraph metadata = Gr (ReteNodeGraphNodeData metadata) ()

data RuleNodesNotInReteEnvironment = RuleNodesNotInReteEnvironment CallStack Rule deriving (Show, Generic, Exception)

nodeToNodeData :: forall  metadata r.
                                     (Members '[Transactional, RuleDatabase, Error RuleNotInRuleDatabase] r) =>
                                    ReteNode metadata -> Sem r (ReteNodeGraphNodeData metadata)
nodeToNodeData (maybeCorrectSubType' -> Just (a@(AlphaNode pat _ _ key _)) ) = pure $ Alpha a pat key
nodeToNodeData (maybeCorrectSubType' -> Just (a@(JoinNode tests _ _ _)) ) = pure $ Join a (tests^.folded)
nodeToNodeData (maybeCorrectSubType' -> Just (p@(ProductionNode ruleIdent _ mapping _))) = do
                                                           rule <- lookUpRule ruleIdent
                                                           pure (Prod p rule mapping)
nodeToNodeData (maybeCorrectSubType' -> Just (nccp@(NegatedConjugationPartnerNode _ _ _ _))) = do
                                                           ncc <- readT (nccp ^. actualNCCNode)
                                                           pure $ NCCP nccp ncc
                                                           
nodeToNodeData node =  pure (Other node)

generateReteNetworkGraphFromNodes :: forall  metadata r.
                                     (Members '[RuleDatabase, Transactional, Error RuleNotInRuleDatabase] r) =>
                                    [ReteNode metadata] -> Sem r (ReteGraph metadata)
generateReteNetworkGraphFromNodes nodes' = do
  nodesWithData <- forM nodes' nodeToNodeData
  edges <- forM nodesWithData \node -> do
    nodeChildren <- node ^!! underlyingNode . childNodes . folded . ignoringLevel . act nodeToNodeData
    nccNode <- case node of
      NCCP _ ncc -> do
        ncc' <- nodeToNodeData (the ncc)
        pure [ncc']
      _ -> pure []
    let thingsToGiveEdgesTo = nodeChildren ++ nccNode
        edges = fmap (\n -> (node, n, ())) thingsToGiveEdgesTo
    pure edges
  pure $ mkGraph nodesWithData (concat edges)
  
                                   
generateReteNetworkGraphForRule :: forall agent metadata r. (HasTracer agent, HasReteEnvironment agent metadata, Members '[RuleDatabase, Error RuleNotInRuleDatabase, Log,  OpenTelemetrySpan, OpenTelemetryContext, Resource, Transactional, Error RuleNodesNotInReteEnvironment, Reader agent] r) => Rule -> Sem r (ReteGraph metadata)
generateReteNetworkGraphForRule rule = do
  agent <- ask
  nodes' <- runReteLookup (agent ^. reteEnvironment) $ getNodesForRule @metadata rule
  nodes'' <- note (RuleNodesNotInReteEnvironment callStack rule) nodes'
  nodes <- nodes'' ^!! elems
  generateReteNetworkGraphFromNodes nodes

strictCD :: CoreDoc -> [String]
strictCD = (fmap T.unpack) . T.lines . renderStrict . layoutPretty defaultLayoutOptions

diagramFromNodeText :: (Diagrams.Prelude.Renderable (Diagrams.Prelude.Path V2 Double) b, Diagrams.Prelude.Renderable (Text Double) b) => Colour Double -> [String] -> P2 Double -> QDiagram b V2 Double Any
diagramFromNodeText colour nodeText =
    let renderedNodeText = ( fontSizeO 12 $
              vsep
                4.5
                (fmap text nodeText)
                
          )
        w = (maybe 40.0 fromIntegral (maximumOf (folded.to length) nodeText)) * 1.3 + 18 --3    --Diagrams.Prelude.width nodeText
        h =  (fromIntegral $ lengthOf (folded) nodeText) * 4.5 + 6 --Diagrams.Prelude.height nodeText
        
    in   place $
        ( 
            renderedNodeText <> (roundedRect w h 2 & translateY (-(h - 11)/2) & lw veryThin) -- & translateX (w/2) )--40 15 1
            # fc colour
        )
renderReteNode :: (Diagrams.Prelude.Renderable (Diagrams.Prelude.Path V2 Double) b, Diagrams.Prelude.Renderable (Text Double) b) => ReteNodeGraphNodeData metadata -> P2 Double -> QDiagram b V2 Double Any
renderReteNode (Alpha (The node) pat key) =
    let nodeText = concatMap
                    strictCD
                    [ nodeSymbol node <+> render pat,
                      render key,
                      render (node ^. identifier)
                    ]
    in diagramFromNodeText  cornflowerblue nodeText                   

renderReteNode (Join (The node) tests) =
    let nodeText = concatMap
                    strictCD
                    [
                     nodeSymbol node,
                     render tests,
                     render (node ^. identifier)
                    ]
    in diagramFromNodeText seagreen nodeText       
renderReteNode (Prod (The node) rule mapping) =
    let nodeText = concatMap
                    strictCD
                    [
                     nodeSymbol node <+> render rule,
                     render mapping,
                     render (node ^. identifier)
                    ]
    in diagramFromNodeText  pink nodeText
            
            
renderReteNode other | node <- other^.underlyingNode =
    let nodeText =  concatMap
                strictCD
                [ nodeSymbol node,
                  render (node ^. identifier)
                ]            
    in diagramFromNodeText  gold nodeText
       
renderReteNodeEdge :: (Diagrams.Prelude.Renderable (Diagrams.Prelude.Path V2 Double) b, Diagrams.Prelude.Renderable (Text Double) b) => ReteNodeGraphNodeData metadata -> P2 Double -> ReteNodeGraphNodeData metadata -> P2 Double -> e -> Diagrams.Prelude.Path V2 Double -> QDiagram b V2 Double Any
renderReteNodeEdge _ p1 _ p2 _ p = lineWidth thin $  arrowBetween' (opts) p1 p2
  where
    opts = with & gaps .~ tiny & headLength .~ (local 8 `atLeast` output 15)  & arrowShaft .~ ((unLoc . head $ pathTrails p) ) -- & _lineWidth.~  thin)

drawReteGraph :: (Diagrams.Prelude.Renderable (Diagrams.Prelude.Path V2 Double) b, Diagrams.Prelude.Renderable (Text Double) b, Members '[Embed IO] r) => ReteGraph metadata -> Sem r (QDiagram b V2 Double Any)
drawReteGraph g = do
  laidOut <- embed $ layoutGraph' @() (defaultDiaParams {globalAttributes = [NodeAttrs [shape BoxShape], GraphAttrs [Splines SplineEdges, NodeSep  0.75]]} ) Dot g 
  let drawing = drawGraph' VerticesOnTop renderReteNode renderReteNodeEdge laidOut
  pure drawing
           
renderReteNetworkGraphForRule :: forall agent metadata r b. (HasTracer agent, Diagrams.Prelude.Renderable (Diagrams.Prelude.Path V2 Double) b, Diagrams.Prelude.Renderable (Text Double) b, HasReteEnvironment agent metadata, Members '[Log, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, Embed IO, Transactional, Error RuleNodesNotInReteEnvironment, Error RuleNotInRuleDatabase, RuleDatabase] r) => Rule -> Sem r (QDiagram b V2 Double Any, ReteGraph metadata)
renderReteNetworkGraphForRule rule = do
  g <- generateReteNetworkGraphForRule @agent @metadata rule
  dia <- drawReteGraph g
  pure (dia,g)

renderAllReteNodes :: forall agent metadata r b. (Diagrams.Prelude.Renderable (Diagrams.Prelude.Path V2 Double) b, Diagrams.Prelude.Renderable (Text Double) b, HasReteEnvironment agent metadata, Members '[Error RuleNotInRuleDatabase, RuleDatabase, Embed IO, Transactional, Reader agent] r) => Sem r (QDiagram b V2 Double Any, ReteGraph metadata)
renderAllReteNodes = do
  agent <- ask
  nodes <- agent^!!reteEnvironment.allNodes.elems
  g <- generateReteNetworkGraphFromNodes nodes
  dia <- drawReteGraph g
  pure (dia,g)
  
  
