module Rete.Environment(ReteEnvironment, newReteEnvironment, resetReteEnvironment, HasReteEnvironment(..), ReteAssociationToken, associateAMemsWithRule, associateNodeWithRule, addNodeToReteEnvironment, unAssociateNodeWithRule, reteEnvironmentIsEmpty, findAlphaMemoriesByWME, ReteLookup,isNodeAssociatedWithARule, getNodesForRule, findAlphaMemoriesByPattern, runReteLookup, removeNodeFromReteEnvironment )  where

import Core.CoreTypes
import Rete.ReteTypes
import Core.Identifiers.IdentifierGenerator
import Cosmolude
import Polysemy.Reader (Reader)    
import StmContainers.Map as STMM
import StmContainers.Set as STMS
import StmContainers.Multimap as STMMM
data ReteEnvironment  a = ReteEnvironment
  { _constantTestAlphaNodes :: STMM.Map MemoryConstantTestHashKey ((Rete.ReteTypes.ReteNode  a) ? IsAlphaNode a),
    _nonConstantTestAlphaNodes :: STMM.Map PatternAffectsFields ((Rete.ReteTypes.ReteNode  a) ? IsAlphaNode a),
    _allAlphaNodes :: STMS.Set ((Rete.ReteTypes.ReteNode  a) ? IsAlphaNode a),
    _allNodes :: STMS.Set (Rete.ReteTypes.ReteNode  a),
    _topNodes :: STMS.Set (Rete.ReteTypes.ReteNode  a),
    _productionNodes :: STMS.Set ((Rete.ReteTypes.ReteNode  a) ? IsProductionNode a),
    _rulesForNode :: STMMM.Multimap (ReteNode a) Rule,
    _nodesForRule :: STMMM.Multimap Rule (ReteNode a)
  }

{-# INLINE newReteEnvironment #-}
newReteEnvironment :: STM (ReteEnvironment  a)
newReteEnvironment = ReteEnvironment <$> STMM.new <*> STMM.new <*> STMS.new <*> STMS.new <*> STMS.new <*> STMS.new <*> STMMM.new <*> STMMM.new



{-# INLINE resetReteEnvironment #-}
resetReteEnvironment :: ReteEnvironment  a -> STM ()
resetReteEnvironment (ReteEnvironment a b c d e f g h) = do
  STMM.reset a
  STMM.reset b
  STMS.reset c
  STMS.reset d
  STMS.reset e
  STMS.reset f
  STMMM.reset g
  STMMM.reset h

makeClassy ''ReteEnvironment

data ReteAssociationToken name = ReteAssociationToken 
type role ReteAssociationToken nominal
associateAMemsWithRule :: (HasTracer agent, Renderable metadata, Typeable metadata, Members '[Log, Resource, Assert, Transactional, Reader agent, OpenTelemetryContext, OpenTelemetrySpan] r) => ReteEnvironment metadata -> HashMap WMEPattern ((ReteNode metadata) ? IsAlphaNode metadata) -> Rule ~~ name -> Sem r (ReteAssociationToken name)
associateAMemsWithRule env amems rule' = do
  forMOf_ (conditions.conditionPatterns) rule \pat -> push "AssociatingAmem" $ do
            addAttributeWithRaw "rete.rule_addition.pattern" pat 
            case amems ^? ix pat of
              Nothing ->
                assert False "WME pattern with no corresponding alpha node during construction" (pat,amems) ()
              Just amem'  -> let amem = the amem' in  attr "rete.rule_addition.amem" (fromString . show $ render amem) do
                debug' $ "Associating" <+> render amem <+> "with rule"
                associateNodeWithRule amem rule env
  pure ReteAssociationToken
       where rule = the rule'

    
{-# INLINE associateNodeWithRule #-}
{-# SCC associateNodeWithRule #-}
associateNodeWithRule :: Member Transactional r => ReteNode metadata -> Rule -> ReteEnvironment metadata -> Sem r ()
associateNodeWithRule node rule env = do
  env^!rulesForNode.insertingKVMM node rule
  env^!nodesForRule.insertingKVMM rule node

{-# INLINE unAssociateNodeWithRule #-}
{-# SCC unAssociateNodeWithRule #-}
unAssociateNodeWithRule :: Member Transactional r => ReteNode metadata -> Rule -> ReteEnvironment metadata -> Sem r ()
unAssociateNodeWithRule node rule env = do
  env^!rulesForNode.deletingKVMM node rule
  env^!nodesForRule.deletingKVMM rule node


{-# INLINE addNodeToReteEnvironment #-}
{-# SCC addNodeToReteEnvironment #-}
addNodeToReteEnvironment :: Member Transactional r => ReteNode  metadata -> ReteEnvironment  metadata -> Sem r ()
addNodeToReteEnvironment newNode' env = do
  env ^! allNodes . inserting newNode'

  case newNode' of
    (maybeCorrectSubType' -> Just newNode) -> do
      -- TODO FIXME add to non-constant-test alpha test nodes
      env ^! constantTestAlphaNodes . insertingKVM (newNode^.constantTestHashKey) newNode
      env ^! allAlphaNodes . inserting newNode
      env ^! topNodes . inserting (the newNode)
    (maybeCorrectSubType' -> Just newNode) -> do
      env ^! productionNodes . inserting newNode
    _ -> pure ()

{-# INLINE removeNodeFromReteEnvironment #-}
removeNodeFromReteEnvironment :: Member Transactional r => ReteNode metadata -> ReteEnvironment metadata -> Sem r ()
removeNodeFromReteEnvironment node' env = do
  env^!allNodes . deleting node'
  case node' of
    (maybeCorrectSubType' -> Just node) -> do -- Alphas
      env^!constantTestAlphaNodes . deletingKVM (node^.constantTestHashKey) --node
      env^!allAlphaNodes . deleting node
      env^!topNodes . deleting (the node)
    (maybeCorrectSubType' -> Just node) -> do
      env^! productionNodes . deleting node
    _ -> pure ()

{-# INLINEABLE reteEnvironmentIsEmpty #-}
reteEnvironmentIsEmpty :: Member Transactional r => ReteEnvironment  a -> Sem r Bool
reteEnvironmentIsEmpty (ReteEnvironment a b c d e f g h) = do
  an <- isNullMap a
  bn <- isNullMap b
  cn <- isNullSet c
  dn <- isNullSet d
  en <- isNullSet e
  fn <- isNullSet f
  gn <- isNullMultimap g
  hn <- isNullMultimap h
  pure (an && bn && cn && dn && en && fn && gn && hn)

instance Show (ReteEnvironment  a) where
  show _ = "Rete environment: TODO SHOW"

data ReteLookup  metadata m a where
  -- | Given a `WMEPattern`, find all alpha nodes which can be impacted.
  FindAlphaMemoriesByPattern :: WMEPattern -> ReteLookup  metadata m (STMS.Set ((ReteNode  metadata) ? IsAlphaNode metadata))
  GetNodesForRule :: Rule -> ReteLookup metadata m (Maybe (STMS.Set (ReteNode metadata)))
  IsNodeAssociatedWithARule :: ReteNode metadata -> ReteLookup metadata m Bool

makeSem ''ReteLookup

{-# INLINE findAlphaMemoriesByWME #-}


-- | Given a WME, find all of the matching alpha nodes it belongs in.
findAlphaMemoriesByWME :: (HasMaybeWMPreference metadata ObjectIdentifier, WMEIsState metadata, Members '[IDGen, ReteLookup  metadata] r) => WME' metadata -> Sem r (STMS.Set ((ReteNode  metadata) ? IsAlphaNode metadata))
findAlphaMemoriesByWME wme = do
  pat <- wmeAsConstantPattern wme
  findAlphaMemoriesByPattern pat

{-# INLINE runReteLookup #-}
runReteLookup :: forall agent  metadata r. (HasTracer agent, HasCallStack, Members '[Reader agent, OpenTelemetrySpan, OpenTelemetryContext , Resource, Transactional, Log] r) => ReteEnvironment  metadata -> InterpreterFor (ReteLookup  metadata) r
runReteLookup env =   interpret $ \i -> (push "ReteLookup") 
  case i of
    IsNodeAssociatedWithARule node ->  do
      rules <- lookupMultimapByKey node (env^.rulesForNode)
      pure (isJust rules)
    GetNodesForRule rule -> do
       lookupMultimapByKey rule (env^.nodesForRule)
    FindAlphaMemoriesByPattern pat ->
      {-# SCC findAlphaMemoriesByPattern #-}
      push "FindAlphaMemoriesByPattern" do
        results <- newSet
        let masterPattern' = wmePatternToConstantTestHashKey pat
        case masterPattern' of
          Nothing -> do
            error "Invalid pattern!" (makeAttributesWithRaw "rete.alpha_memory.pattern" pat)
          Just masterPattern -> do
            debug' $ "Master pattern: " <+> viaShow masterPattern                               
            mapM_ (addPatternMem results) (generaliseMemoryConstantTestHashKey masterPattern)
        pure results                
  where
    addPatternMem results p =
      {-# SCC addPatternMem #-}
      do
        val <- lookupMap p (env ^. constantTestAlphaNodes)
        case val of
          Nothing -> pure @(Sem r) ()
          Just amem -> insertInSet  amem results
