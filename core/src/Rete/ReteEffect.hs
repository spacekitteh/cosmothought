{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# OPTIONS_GHC -Wwarn=incomplete-patterns #-}

module Rete.ReteEffect (Rete, runRete, addWMEs, removeWMEs, addRules, removeRules) where

import Control.Exception (Exception)
-- import Control.Parallel.Strategies
-- import qualified System.IO.Unsafe as UNSAFE (unsafePerformIO)

import Control.Monad
import Control.Monad.Zip
-- import qualified Prelude (error)

import Core.Identifiers
-- import Prelude (error)

import Core.Identifiers.IdentifierGenerator
import Core.Rule.Database
import Core.Rule.Match
import Core.Utils.STM
import Core.WME
import Cosmolude
import Data.Bitraversable
import Data.Either (Either (..))
import qualified Data.HashMap.Strict as HM
import qualified Data.HashSet as HS
import Data.HashSet.Lens
import Data.List.NonEmpty as DLNE
import qualified Data.Sequence as Seq
import Data.Sequence.Lens
import Data.Text (Text)
import Data.Tuple (fst)
import qualified OpenTelemetry.Trace.Core as OT
import PSCM.Effects.RuleMatcher
import Polysemy
import Polysemy.Reader
import Polysemy.State
import Rete.Dynamics.Linking
import Rete.Dynamics.TokenFlow
import Rete.Environment
import Rete.ReteBuilder
import Rete.ReteTypes
import Rete.Types.Tokens
import StmContainers.Set as STMS (Set)

import Theory.Equality
import Prelude (Integral(toInteger))
type Rete = RuleMatcher WMEMetadata (ReteNode WMEMetadata) (WMEToken ? IsProductionToken)

{-# INLINEABLE runRete #-}
runRete ::
  ( -- HasMemoryBlock' ((ReteNode WMEMetadata) ? pred)  WMEMetadata, -- ProveCorrectSubType (ReteNode WMEMetadata) pred subtype,
    HasCallStack,
    HasTracer agent,
    HasProductionRuleActivity agent WMEMetadata,
    HasReteEnvironment agent WMEMetadata,
    Members '[OpenTelemetrySpan, OpenTelemetryContext, Resource, RuleDatabase, ReteBuilder (WMEMetadata), Transactional, IDGen, Log, Reader agent, Assert, Embed IO] r
  ) =>
  InterpreterFor Rete r
runRete = interpret $ \i -> push "ReteEffect" do
  env <- ask <&> view reteEnvironment
  case i of
    --    RemoveWMEs wmes -> push "RemoveWMEs" do
    --      activity <- embed $ ProductionRuleActivity <$> STMS.new <*> STMS.new
    GetAllRules -> do
      numberOfRules <- (fromInteger . toInteger ) <$> env^!nodesForRule . multimapAsMap . mapSize
      rules <- env^!!nodesForRule . multimapKeys
      pure (numberOfRules, rules)
      
           
    AddWMEs wmes -> push "AddWMEs" $ do
      (activations, retractions, _) <- capturingActivityOnWMEs env wmes (addWMEsToAlphaMemory)
      pure (activations, retractions)
    RemoveWMEs wmes -> push "RemoveWMEs" $ do
      (activations, retractions, _) <-
        capturingActivityOnFoldable env wmes
          $ ( \(wme :: WME) -> do
                debug' $ "WME:" <+> viaShow wme
                toks <- wme ^!! ownedTokens . elems
                removeTokens toks
                jrs <- wme ^!! negativeJoinResults . elems
                debug' $ "Negative join results" <+> fillSep (fmap render jrs)
                forM_ jrs \jr -> do
                  let (negativeToken :: WMEToken' WMEMetadata) = jr ^. owner
                  debug' $ "Owner:" <+> render negativeToken
                  njrs <- negativeToken ^!! negativeJoinResults' . elems
                  debug' $ "Negative join results of owner prior" <+> vsep (fmap render njrs)
                  negativeToken ^! negativeJoinResults' . deleting jr
                  noMore <- isNullSet (negativeToken ^??! negativeJoinResults')
                  njrs <- negativeToken ^!! negativeJoinResults' . elems
                  debug' $ "Negative join results of owner posterior" <+> vsep (fmap render njrs)
                  when noMore $ do
                    children <- negativeToken ^!! containingReteNode . childNodes . folded . ignoringLevel
                    forM_ children \child -> do
                      activateNode (jr ^. owner . containingReteNode) OriginIsNewlyEmpty (HM.singleton (negativeToken ^??! immediateParents . folded . match) (PropagatingToken $ jr ^. owner)) child
            )
      pure (activations, retractions)
    RemoveRules rules -> push "RemoveRules" $ do
      (activations, retractions, _) <- capturingActivityOnFoldable env rules \rule -> do
        -- foreach rule
        -- Get nodes for rule
        nodeSet <- getNodesForRule rule
        case nodeSet of
          Nothing -> pure ()
          Just nodes' -> do
            nodes <- nodes' ^!! elems

            -- foreach node
            forM_ nodes \node -> do
              -- remove association
              unAssociateNodeWithRule node rule env

              nodeIsGarbage <- not <$> isNodeAssociatedWithARule node

              -- if no rules associated to node:
              when nodeIsGarbage do
                deleteNode env node
      pure (activations, retractions)
    AddRules rules -> push "AddRules" $ do
      debug "Adding rules" [("rete.rule_addition.raw_rules", (toAttribute (rules ^.. folded . to show . to (fromString @Text)))), ("rete.rule_addition.rules", toAttribute ((rules ^.. folded . to ByRenderable)))]
      (activations, retractions, allNodes) <- captureActivity env do
        (normalisedRules, amems') <- producePatternMap rules

        --        debug' $ "Alpha memories for rules:" <+> (viaShow . vsep . fmap (\(k, v) -> render k <+> "⥽" <+> render v <+> hardline) . HM.toList $ amems)
        nodes <- forMOf traversed normalisedRules (nodesForNormalisedRule amems')
        let amems = fmap (view _1) amems'
        let allNodes = setOf (traversed . traversed) nodes <> setOf (traversed . to the) amems
        debug' $ "Nodes created:" <+> render (allNodes ^.. folded)

        pure allNodes
      pure (allNodes, activations, retractions)

getStateTestingPatternAMem :: (HasTracer agent, Members '[Log, Reader agent, Resource, Assert, Transactional, OpenTelemetryContext, OpenTelemetrySpan] r) => HashMap WMEPattern ((ReteNode WMEMetadata) ? IsAlphaNode WMEMetadata, VariableRenaming) -> Rule ~~ name -> ReteAssociationToken name -> Sem r ((ReteNode WMEMetadata) ? IsAlphaNode WMEMetadata, VariableRenaming)
getStateTestingPatternAMem amems rule !_tok = push "getStateTestingPatternAMem" $ do
  addAttributeWithRaw "rete.rule_addition.state_pattern" ((the rule) ^. stateTestingPattern)
  case (amems ^? ix ((the rule) ^. stateTestingPattern)) of
    Just amem -> pure amem
    Nothing -> do
      assert False "State testing pattern alpha memory not in amems during rete net construction" ((the rule) ^. stateTestingPattern, amems) (amems ^??! folded)

nodesForNormalisedRule :: (HasTracer agent, HasProductionRuleActivity agent WMEMetadata, HasReteEnvironment agent WMEMetadata, Members '[RuleDatabase, IDGen, ReteBuilder WMEMetadata, Reader agent, Embed IO, Log, Resource, Assert, Transactional, OpenTelemetryContext, OpenTelemetrySpan] r) => HashMap WMEPattern ((ReteNode WMEMetadata) ? IsAlphaNode WMEMetadata, VariableRenaming) -> Rule -> Sem r (NonEmpty (ReteNode WMEMetadata))
nodesForNormalisedRule amems'' rule' = push "nodesForNormalisedRule" $ do
  addAttributeWithRaw "rete.rule_addition.original_rule" rule'
  let rule = updateStateTestPattern rule'
  addAttributeWithRaw "rule.id" (rule ^. identifier)
  addAttributeWithRaw "rete.rule_addition.fixed_rule" rule
  env <- ask <&> view reteEnvironment
  let pats = rule ^.. conditions . conditionPatterns
      amems' = HM.filterWithKey (\p _ -> elemOf (conditions . conditionPatterns) p rule) amems''
      vars = pats ^.. folded . patternBindingVariables
  (substitution' :: VariableRenaming) <- push "mergeRuleSubstitutions" $ do
    addAttributeWithRaw "rete.rule_addition.alpha_nodes.substitutions" (amems' ^.. folded . _2)
    addAttributeWithRaw "rete.rule_addition.alpha_nodes.substitutions.alternate" (amems' ^@.. folded . _2 . ifoldedSubstitution)
    pure (constructSubstitution $ amems' ^@.. folded . _2 . ifoldedSubstitution) -- at this stage we don't care which exact amem+field gets chosen as they will be the same anyway.
    -- case mergeSubstitutions ((amems'^..folded. _2)) of
    --   Nothing -> assert False "Could not merge substitutions" (show . render $ amems') bottom
    --   Just sub -> pure sub

  -- Add rule to rule database
  addRule rule
  tracerProvider <- OT.getGlobalTracerProvider

  let amems = amems' & traversed %~ fst

  debug' $ "LHS WME patterns for rule:" <+> render pats
  debug' $ "Raw state test pattern:" <+> viaShow (rule ^. stateTestingPattern)
  debug' $ "Raw patterns:" <+> viaShow pats

  -- <ugh> TODO HACKHACK FIXME
  (stateTestingPatternAMem, stateTestingSub) <- name rule \rule' -> do
    assocTok <- associateAMemsWithRule env amems rule'
    -- skip first condition as it is just the state test alpha node.
    getStateTestingPatternAMem amems' rule' assocTok

  let substitution = substitution' ^??! __Substitution
      unmappedVars :: Substitution Variable = constructSubstitution $ mzip vars vars
      stateVar = rule ^??! stateTestingPattern . patternObject . patVariable . patternVariableVariable
      ruleSub = stateVar |=> (stateTestingSub ^??! at stateVar . _Just) $ substitution
  --              ruleSub = (composeSubstitution substitution unmappedVars   ) ------------- swap?
  -- </ugh>
  -------------- TODO FIXME Sometimes the state testing pattern isn't included in the substitution? see test-a0b98060-029a-d41d-5475-85ba47830858

  let initialState = (Seq.singleton (PositiveCondition (rule ^. stateTestingPattern)), (the stateTestingPatternAMem) :| [])
      initialState :: (Seq RuleCondition, NonEmpty (ReteNode WMEMetadata))
  debug' $ "Initial state:" <+> render initialState
  -- For each rule condition, create its corresponding subnetwork, and grab the last node created
  debug
    "Rule conditions"
    [ ("actual_rule_conditions", toAttribute $ rule ^.. conditions . to ByRenderable),
      ("passed_rule_conditions", fromString . show . render $ (seqOf (dropping 1 conditions) rule))
    ]

  (_, createdNodes@(lastNode :| _)) <- execState initialState $ interpretTransaction . transaction . raise $ createNetworkForConditions amems (seqOf (dropping 1 conditions) rule) -- skip first condition as it is just the state test alpha node.
  OT.forceFlushTracerProvider tracerProvider Nothing
  addAttributeWithRaw "rete.rule_addition.original_unmapped_variables" unmappedVars
  addAttributeWithRaw "rete.rule_addition.node_substitutions" substitution'
  addAttributeWithRaw "rete.rule_addition.final_rule_substitution" ruleSub
  -- Create the production rule attached to the last node
  prod <- createProductionNode [lastNode] (rule ^. ruleIdentifier) ruleSub
  updateNewNodeWithMatchesFromAbove (the prod)
  let allNodes = (the prod) DLNE.<| createdNodes
  forM_ allNodes \node -> associateNodeWithRule node rule env

  pure (allNodes)

producePatternMap :: (HasTracer agent, Members '[ReteBuilder WMEMetadata, Reader agent, Log, Resource, Assert, Transactional, OpenTelemetryContext, OpenTelemetrySpan] r, Each1 (t Rule) (t Rule) Rule Rule, Traversable t) => t Rule -> Sem r (t Rule, HashMap WMEPattern ((ReteNode WMEMetadata) ? IsAlphaNode WMEMetadata, VariableRenaming))
producePatternMap rules = push "producePatternMap" do
  let (normalisedRules, patternMap) = productionConditionMap rules
  debug
    "Pattern condition map"
    [ ("rete_rule_addition.normalised_pattern_condition_map", fromString @Text . show . render $ patternMap),
      ("rete_rule_addition.raw_normalised_pattern_condition_map", fromString . show $ patternMap)
    ]

  -- Create a mapping from raw WMEPatterns to alpha nodes
  amems' :: (HashMap WMEPattern ((ReteNode WMEMetadata) ? IsAlphaNode WMEMetadata, VariableRenaming)) <- do
    let ks = HM.keys (patternMap ^. rulesForCondition)
    debug' $ "Keys:" <+> viaShow ks
    (l, r) <- bitraverse pure createOrShareAlphaNodes (ks, ks)

    pure $ HM.fromList (mzip l r)
  addAttributeWithRaw "rete_rule_addition.alpha_memory_map" amems'
  pure (normalisedRules, amems')

{-# INLINEABLE createNetworkForConditions #-}
createNetworkForConditions ::
  ( HasCallStack,
    HasProductionRuleActivity agent WMEMetadata,
    OT.HasTracer agent,
    Members
      '[ OpenTelemetrySpan,
         OpenTelemetryContext,
         Resource,
         Reader agent,
         State (Seq RuleCondition, NonEmpty (ReteNode WMEMetadata)),
         RuleDatabase,
         ReteBuilder (WMEMetadata),
         Transactional,
         IDGen,
         Log,
         Assert, Embed IO
       ]
      r
  ) =>
  HashMap WMEPattern ((ReteNode WMEMetadata) ? IsAlphaNode WMEMetadata) ->
  Seq RuleCondition ->
  Sem r (ReteNode WMEMetadata)
createNetworkForConditions amems conds = push "createNetworkForConditions" . attr "rete.rule_addition.conditions" (fromString . show $ render conds) $ do
  nodes <- forM conds \cond -> do
    results <- createNetworkForCondition amems cond
    gcIfDebug
    pure results
  pure (nodes ^??! _last)

-- | Given a 'RuleCondition', create or share the appropriate rete subgraph and return the exit node.
{-# INLINEABLE createNetworkForCondition #-}
createNetworkForCondition ::
  ( HasCallStack,
    HasProductionRuleActivity agent WMEMetadata,
    HasTracer agent,
    Members
      '[ OpenTelemetrySpan,
         OpenTelemetryContext,
         Resource,
         Reader agent,
         State (Seq RuleCondition, NonEmpty (ReteNode WMEMetadata)),
         RuleDatabase,
         ReteBuilder (WMEMetadata),
         Transactional,
         IDGen,
         Log,
         Assert, Embed IO
       ]
      r
  ) =>
  HashMap WMEPattern ((ReteNode WMEMetadata) ? IsAlphaNode WMEMetadata) ->
  RuleCondition ->
  Sem r (ReteNode WMEMetadata)
-- TODO FIXME Check for negative conditions here
createNetworkForCondition amems cond = push "NodeForCondition" . attr "Condition" (fromString . show $ render cond) $ name cond \condition -> case proveIsOfSense condition of
  Right proof -> note proof $ (fmap the $ createNetworkForPositiveCondition amems condition)
  Left proof | lengthOf plate cond == 1 -> note proof $ (the <$> createNegativeNode amems condition)
  Left proof -> note proof $ push "NCCSubnetwork" do
    let subConditions = seqOf plate cond
    (currentCondsHandled, (leftParent :| _)) <- get
    debug'' $ "NCC node parent" <+> viaShow leftParent
    rightParent <- createNetworkForConditions amems subConditions

    debug'' $ "rightparent: " <+> viaShow rightParent

    (ncc :: (ReteNode WMEMetadata) ? IsNCCNode WMEMetadata) <- createOrShareNegatedConjugationNode [leftParent] [rightParent]
    (_, createdNodes) <- get
    debug'' $ "New NCC node: " <+> viaShow ncc
    memNode :| _ <- createOrShareMemoryNodes (DLNE.singleton (DLNE.singleton (the ncc)))
    put (cond :< currentCondsHandled, (the memNode) DLNE.<| (the ncc) DLNE.<| (the (ncc ^. nccPartnerNode)) DLNE.<| createdNodes)
    updateNewNodeWithMatchesFromAbove (the (ncc ^. nccPartnerNode))
    updateNewNodeWithMatchesFromAbove (the ncc)
    updateNewNodeWithMatchesFromAbove (the memNode)
    let nccParents = relativesCollection [leftParent]
        partnerParents = relativesCollection [rightParent]
    assertM
      ( do
          parents <- ncc ^! parentNodes
          pure (parents == nccParents)
      )
      "NCC node parents are not what they ought to be"
      ncc
      ()
    assertM
      ( do
          parents <- ncc ^! nccPartnerNode . parentNodes
          pure (parents == partnerParents)
      )
      "NCC partner node parents are not what they ought to be"
      (ncc ^. nccPartnerNode)
      ()
    gcIfDebug
    pure (the memNode)

-- | Create or share a single a negative node for a given, known-negative condition
{-# INLINEABLE createNegativeNode #-}
createNegativeNode ::
  ( Fact ((AppearanceSense cond) == NegativeAppearance),
    HasCallStack,
    HasProductionRuleActivity agent WMEMetadata,
    HasTracer agent,
    Members
      '[ OpenTelemetrySpan,
         OpenTelemetryContext,
         Resource,
         Reader agent,
         State (Seq RuleCondition, NonEmpty (ReteNode WMEMetadata)),
         RuleDatabase,
         ReteBuilder (WMEMetadata),
         Transactional,
         IDGen,
         Log,
         Assert
       ]
      r
  ) =>
  HashMap WMEPattern ((ReteNode WMEMetadata) ? IsAlphaNode WMEMetadata) ->
  RuleCondition ~~ cond ->
  Sem r ((ReteNode WMEMetadata) ? IsNegativeNode WMEMetadata)
createNegativeNode amems (The cond) = do
  let pat = first1Of conditionPatterns cond
  (earlierConds, earlierNodes@(priorNode :| _)) <- get
  let tests = computeJoinTestsForPattern amems pat earlierConds
  debug' $ "Join tests for condition:" <+> render (tests ^.. folded)
  -- Simple binary joins: The new condition maps to the new alpha node. The parents are just the new
  -- alpha node and the head of earlierNodes.
  let newAmem = amems ^??! ix pat
      joinParents = (Seq.fromList [the newAmem, priorNode])
  debug' $ "New negative node parents: " <+> viaShow joinParents
  newNode <- createOrShareNegativeNode joinParents newAmem tests
  debug' $ "New negative node: " <+> viaShow newNode
  put (cond :< earlierConds, (the newNode) DLNE.<| earlierNodes)

  updateNewNodeWithMatchesFromAbove (the newNode)

  pure newNode

-- | Create or share a network for a single positive condition.
{-# INLINEABLE createNetworkForPositiveCondition #-}
createNetworkForPositiveCondition ::
  ( Fact ((AppearanceSense cond) == PositiveAppearance),
    HasCallStack,
    HasProductionRuleActivity agent WMEMetadata,
    HasTracer agent,
    Members
      '[ OpenTelemetrySpan,
         OpenTelemetryContext,
         Resource,
         Reader agent,
         State (Seq RuleCondition, NonEmpty (ReteNode WMEMetadata)),
         RuleDatabase,
         ReteBuilder (WMEMetadata),
         Transactional,
         IDGen,
         Log,
         Assert, Embed IO
       ]
      r
  ) =>
  HashMap WMEPattern ((ReteNode WMEMetadata) ? IsAlphaNode WMEMetadata) ->
  RuleCondition ~~ cond ->
  Sem r ((ReteNode WMEMetadata) ? IsMemoryNode WMEMetadata)
createNetworkForPositiveCondition amems (The cond@(PositiveCondition pat)) = do
  (earlierConds, earlierNodes@(priorNode :| _)) <- get
  let tests = computeJoinTestsForPattern amems pat earlierConds
  debug' $ "Join tests for condition:" <+> render (tests ^.. folded)
  -- Simple binary joins: The new condition maps to the new alpha node. The parents are just the new
  -- alpha node and the head of earlierNodes.
  let newAmem = amems ^??! ix pat
      joinParents = (Seq.fromList [the newAmem, priorNode])
  debug' $ "New join node parents: " <+> viaShow joinParents
  newNode <- createOrShareJoinNode joinParents newAmem tests
  debug' $ "New join node: " <+> viaShow newNode
  memNode <- newNode ^! betaMemoryNode
  debug' $ "New memory node: " <+> viaShow memNode
  put (cond :< earlierConds, (the memNode) DLNE.<| (the newNode) DLNE.<| earlierNodes)

  -- TODO: Test if newly created or not.
  updateNewNodeWithMatchesFromAbove (the newNode)
  gcIfDebug
  pure memNode

{-# INLINEABLE computeJoinTestsForPattern #-}
computeJoinTestsForPattern ::
  (IxValue amems ~ ((ReteNode metadata) ? IsAlphaNode metadata), Ixed amems, Index amems ~ WMEPattern, Each container container RuleCondition RuleCondition) =>
  amems ->
  WMEPattern ->
  container ->
  Seq JoinCondition
computeJoinTestsForPattern amems cond earlierConds = ifoldMapOf (patternConditions . _APatternVariable) tf cond
  where
    tf field (v, Nothing) | var <- v ^. variable = case (earlierConds ^? varsInPositiveCondition amems var) of
      Nothing -> Seq.empty
      Just ((priorNode, priorField), _) -> Seq.singleton $ JoinCondition (JoinNodeArgument field (amems ^??! ix cond . nodeIdentifier)) (NodeArgument (JoinNodeArgument priorField (priorNode ^. nodeIdentifier))) EQ
    tf field (_, Just (AComparisonTest preds)) =
      -- \| var <- v^. variable
      do
        (pred :: ComparisonTest) <- preds

        priorArg <- case pred ^. comparingTo of
          CompareAgainstVariable otherVar -> do
            ((priorNode, priorField), _) <- seqOf (varsInPositiveCondition amems otherVar) earlierConds
            pure (NodeArgument (JoinNodeArgument priorField (priorNode ^. nodeIdentifier)))
          CompareAgainstValue val -> do
            pure (ValueArgument val)
        pure $ JoinCondition (JoinNodeArgument field (amems ^??! ix cond . nodeIdentifier)) priorArg (patternComparisonTestToJoinPredicate pred)
    tf _ (_, Just _) = Seq.empty

-- TODO FIXME: Support regex here

{-# INLINEABLE matchFromToken #-}
matchFromToken ::
  forall agent r.
  (HasTracer agent, HasCallStack, Members '[Log, Resource, Assert, Transactional, Reader agent, OpenTelemetryContext, OpenTelemetrySpan, IDGen, RuleDatabase, Embed IO] r) =>
  WMEToken ? IsProductionToken ->
  Sem r RuleMatch
matchFromToken tok@(The token) = push "matchFromToken" do
  gcIfDebug                                   
  --  let provenToken = tok Data.Refined.... known
  let varMapping = token ^??! containingReteNode . to maybeCorrectSubType' . _Just . matchVariableMapping . __Substitution
  addAttributeWithRaw "rule.match.rete_mapping" varMapping
  rule <- lookUpRule (token ^??! containingReteNode . associatedRule)
  newObjects' <-
    mapM
      ( \a -> do
          oid <- newObjectIdentifierBasedOnVariable a
          pure (a, oid)
      )
      (rule ^. objectsToCreate)
  let accumulated = tokenMatchData token
      newObjects = constructSubstitution newObjects'
      wmes' = accumulated ^. accumulatedWMEs
      newObjectsAsValues = fmap (view (re _ObjectIdentifier)) newObjects
      accumulated' = accumulated ^. accumulatedSubstitution
  assert (accumulated' /= incompatibleSubstitution) "Accumulated substitution is invalid!" AccumulatedSubstitutionFromReteTokenIsInvalid ()
  let --accumulated' = accumulated'' ^??! __Substitutio
      mergedNewAndAccumulated' = ((mergeSubstitutions [accumulated', newObjectsAsValues]))
  assert (mergedNewAndAccumulated' /= incompatibleSubstitution) "Merged substitution is invalid!" MergedSubstitutionFromReteTokenIsInvalid ()
  let substitution' = (mergedNewAndAccumulated'^??! __Substitution) `composeSubstitution` varMapping
  addAttributeWithRaw "rule.match.new_objects" newObjects
  addAttributeWithRaw "rule.match.new_objects2" ((newObjects^??! __Substitution)  `composeSubstitution` varMapping)
  addAttributeWithRaw "rule.match.token" token
  addAttributeWithRaw "rule.match.token_match_data" accumulated
  addAttributeWithRaw "rule.match.wmes" wmes'
  addAttributeWithRaw "rule.match.rete_token_variable_mapping" (accumulated')
  addAttributeWithRaw "rule.match.resultant_mapping" substitution'
  ident <- InstantiationIdentifier <$> newGlobalIdentifier
  addAttributeWithRaw "rule.id" (rule ^. identifier)
--  assert (substitution' /= incompatibleSubstitution) "Substitution from production token is invalid!" ProductionTokenSubstitutionIsInvalid ()
  assert (newObjects /= incompatibleSubstitution) "Substitution for new objects from production token is invalid!" NewObjectSubstitutionsFromReteTokenIsInvalid ()
  pure (RuleMatch rule tok wmes' substitution' (newObjects^??! __Substitution) ident)

data NewObjectSubstitutionsFromReteTokenIsInvalid = NewObjectSubstitutionsFromReteTokenIsInvalid deriving (Eq, Show, Exception)
data AccumulatedSubstitutionFromReteTokenIsInvalid = AccumulatedSubstitutionFromReteTokenIsInvalid deriving (Eq, Show, Exception)
data MergedSubstitutionFromReteTokenIsInvalid = MergedSubstitutionFromReteTokenIsInvalid deriving (Eq, Show, Exception)
data ProductionTokenSubstitutionIsInvalid = ProductionTokenSubstitutionIsInvalid deriving (Eq, Show, Exception)                                              

{-
  General idea for naive binary-join rule-at-a-time rule addition
  ------------
  1. For each rule:
   a) choose an ordering of conditions such that every
   variable referenced in an inter-condition test occurs in a binding
   position before occurring in the test.

   b) For each condition in order:
      i) create a new α-memory
     ii) convert the WMEPattern to a set of JoinConditions, using prior nodes
    iii) share or create a new node
     iv) add new node to list of prior nodes for the rule

   c) create a production node for the rule
  ------------
  This could be optimised by finding good multi-join-tests; this requires
  making conditions equivalent when they are α-equivalent. This is tricky
  as it requires consideration of the bindings to earlier variables.
-}

{-# INLINEABLE deleteNode #-}
deleteNode ::
  ( HasProductionRuleActivity agent WMEMetadata,
    HasCallStack,
    Members
      '[ ReteLookup WMEMetadata,
         RuleDatabase,
         ReteTokenManipulation WMEMetadata,
         ReteBuilder (WMEMetadata),
         Transactional,
         Reader agent,
         IDGen,
         Log,
         Assert
       ]
      r
  ) =>
  ReteEnvironment WMEMetadata ->
  ReteNode WMEMetadata ->
  Sem r ()
deleteNode env node = do
  -- remove child nodes
  children <- node ^!! childNodes . folded . ignoringLevel
  forM_ children (deleteNode env)

  -- remove tokens from node
  toks <- node ^!! memory . tokens . elems
  removeTokens toks

  removeNodeFromReteEnvironment node env

  -- clear node
  unlinkNodeForDeletion node

{-# INLINEABLE captureActivity #-}

-- | Perform an action, collecting any production rule activity.
captureActivity ::
  ( HasProductionRuleActivity agent WMEMetadata,
    HasTracer agent,
    HasCallStack,
    Members
      '[ OpenTelemetrySpan,
         OpenTelemetryContext,
         Resource,
         RuleDatabase,
         ReteBuilder (WMEMetadata),
         Transactional,
         Reader agent,
         IDGen,
         Log,
         Assert, Embed IO
       ]
      r
  ) =>
  ReteEnvironment WMEMetadata ->
  ( Sem
      ( ReteLookup WMEMetadata
          : ReteTokenManipulation WMEMetadata
          : r
      )
      x
  ) ->
  Sem r (HashSet (RuleActivation), HashSet (RuleRetraction), x)
captureActivity env m = do
  activity <- ask <&> view productionRuleActivity
  (activated', retracted', results) <- runReteTokenManipulation $ runReteLookup env do
    results <- m
    activated <- activity ^!! ruleFirings . elems
    resetSet (activity ^. ruleFirings)
    retracted <- activity ^!! ruleRetractions . elems
    resetSet (activity ^. ruleRetractions)
    pure (activated, retracted, results)
  -- collect the activity, getting the rule match from the production token
  activated <- activated' ^!! traversed . act matchFromToken . to RuleActivation
  retracted <- retracted' ^!! traversed . act matchFromToken . to RuleRetraction
  let activity' = (HS.fromList activated, HS.fromList retracted, results)
  attr "TotalActivations" (fromString . show $ lengthOf folded activated) $ attr "TotalRetractions" (fromString . show $ lengthOf folded retracted) $ do
    debug' $ "Activations:" <+> viaShow activated
    debug' $ "Retractions:" <+> viaShow retracted

    info' $ "Activations:" <+> render activated
    info' $ "Retractions:" <+> render retracted
  pure activity'

{-# INLINEABLE capturingActivityOnFoldable #-}

-- | Perform an action on a collection of objects with WMEIdentifiers, collecting any production rule activity.
capturingActivityOnFoldable ::
  ( HasProductionRuleActivity agent WMEMetadata,
    OT.HasTracer agent,
    Foldable t,
    Renderable wme,
    Monoid x,
    HasCallStack,
    Members
      '[ OpenTelemetrySpan,
         OpenTelemetryContext,
         Resource,
         RuleDatabase,
         ReteBuilder (WMEMetadata),
         Reader agent,
         Transactional,
         IDGen,
         Log,
         Assert, Embed IO
       ]
      r
  ) =>
  ReteEnvironment WMEMetadata ->
  t wme ->
  ( wme ->
    Sem
      ( ReteLookup WMEMetadata
          : ReteTokenManipulation WMEMetadata
          : r
      )
      x
  ) ->
  Sem r (HashSet (RuleActivation), HashSet (RuleRetraction), x)
capturingActivityOnFoldable env wmes m = captureActivity env do
  let action wme = \existing ->
        {-fmap (withStrategy rpar) $-}
        do
          res <- m wme
          pure (res <> existing)
  -- TODO OPTIMISE : Build a multimap; run everything all at once

  foldrM action mempty wmes

{-# INLINEABLE capturingActivityOnWMEs #-}
capturingActivityOnWMEs ::
  ( Foldable t,
    HasCallStack,
    Monoid x,
    OT.HasTracer agent,
    HasProductionRuleActivity agent WMEMetadata,
    Members '[Embed IO, OpenTelemetrySpan, OpenTelemetryContext, Resource, RuleDatabase, ReteBuilder (WMEMetadata), Reader agent, Transactional, IDGen, Log, Assert] r
  ) =>
  ReteEnvironment WMEMetadata ->
  t WME ->
  (forall g. (Traversable g) => g WME -> (STMS.Set ((ReteNode WMEMetadata) ? IsAlphaNode WMEMetadata)) -> Sem (ReteLookup WMEMetadata : ReteTokenManipulation WMEMetadata : r) x) ->
  Sem r (HashSet (RuleActivation), HashSet (RuleRetraction), x)
capturingActivityOnWMEs env wmes m = capturingActivityOnFoldable
  env
  wmes
  \wme -> push "SingleWME" . attr "WME" (fromString . show . render $ wme ^. wMEIdentifier) $ do
    debug' $ "Considering WME " <+> render wme
    nodes <- findAlphaMemoriesByWME wme
    nodesAsSet <- nodes ^!! elems
    debug' $ viaShow (lengthOf folded nodesAsSet) <+> " alpha memories found for WME: " <+> viaShow nodesAsSet
    res <- m [wme] nodes
    debug' $ "wme:" <+> viaShow wme
    pure res
