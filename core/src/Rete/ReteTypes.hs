{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE UndecidableInstances #-}

module Rete.ReteTypes (module Rete.ReteTypes, module CRTN) where
import Rete.Types.Tokens
-- import qualified Control.Exception as CE (assert) 

import Data.Refined

import Core.CoreTypes

import Rete.Types.Nodes as CRTN hiding (CommonNodeData, MemoryBlock, RelativesCollection, ReteNode)
import qualified Rete.Types.Nodes as CRTN
import Cosmolude hiding ((...))






import StmContainers.Set as STMS

-- TODO: Export ReteNode a as an alias for CRTN.ReteNode (WMEToken' a) a

type ReteNode a = CRTN.ReteNode (WMEToken' a) a

type MemoryBlock a = CRTN.MemoryBlock (WMEToken' a) a

type CommonNodeData a = CRTN.CommonNodeData (WMEToken' a) a

pattern CommonNodeData :: NodeIdentifier -> NodeLevel -> TVar (CRTN.RelativesCollection tok a) -> TVar (Seq (CRTN.ReteNode tok a)) -> TVar (CRTN.RelativesCollection tok a) -> ParentArity -> CRTN.CommonNodeData tok a
pattern CommonNodeData ident level children emptyParents parents arity = CommonNodeData' ident level children emptyParents parents arity

pattern MemoryBlock :: CRTN.ReteNode tok metadata -> Set tok -> CRTN.MemoryBlock tok metadata
pattern MemoryBlock node tokens = CRTN.MemoryBlock' node tokens

type RelativesCollection a = CRTN.RelativesCollection (WMEToken' a) a

type HasCommonNodeData' c a = CRTN.HasCommonNodeData c (WMEToken' a) a

type HasMemoryBlock' c metadata = CRTN.HasMemoryBlock c (WMEToken' metadata) metadata

type IsMemoryNode metadata = IsMemoryNode' (WMEToken' metadata) metadata
type IsJoinNode metadata = IsJoinNode' (WMEToken' metadata) metadata
type IsProductionNode metadata = IsProductionNode' (WMEToken' metadata) metadata
type IsAlphaNode metadata = IsAlphaNode' (WMEToken' metadata) metadata
type IsNegativeNode metadata = IsNegativeNode' (WMEToken' metadata) metadata
type IsNCCNode metadata = IsNCCNode' (WMEToken' metadata) metadata
type IsNCCPartnerNode metadata = IsNCCPartnerNode' (WMEToken' metadata) metadata








newtype PreviouslyConsideredTokenCombo a = PreviouslyConsideredTokenCombo {_previousComboCommonData :: CommonTokenData a} deriving (Generic)

makeLenses ''PreviouslyConsideredTokenCombo

instance Renderable metadata => Renderable (PreviouslyConsideredTokenCombo metadata) where
  render jpt = "Previously considered combo:" <+> render (jpt ^. previousComboCommonData)

instance Renderable metadata => Show (PreviouslyConsideredTokenCombo metadata) where
  show = show . render

instance HasCommonTokenData (PreviouslyConsideredTokenCombo metadata) metadata where
  {-# INLINE commonTokenData #-}
  commonTokenData = previousComboCommonData

instance HasIdentifier (PreviouslyConsideredTokenCombo metadata) where
  {-# INLINE identifier #-}
  identifier = commonTokenData . identifier

deriving via ByIdentifier (PreviouslyConsideredTokenCombo metadata) instance Eq (PreviouslyConsideredTokenCombo metadata)

deriving via ByIdentifier (PreviouslyConsideredTokenCombo metadata) instance Hashable (PreviouslyConsideredTokenCombo metadata)


type PatternAffectsFields = (Bool, Bool, Bool)


newtype PropagatingToken metadata = PropagatingToken {_rawToken :: WMEToken' metadata} deriving newtype (Eq, Renderable, Show, Generic, Hashable)

makeLenses ''PropagatingToken

instance (HasCommonTokenData (PropagatingToken metadata) metadata) where
  {-# INLINE commonTokenData #-}
  commonTokenData = rawToken . commonTokenData

{-# INLINE propagatingMatch #-}
propagatingMatch :: Fold (PropagatingToken metadata) (WMEPatternMatch' metadata)
propagatingMatch = rawToken . match

newtype CandidateToken metadata = CandidateToken {_rawCandidateToken :: WMEToken' metadata} deriving newtype (Eq, Renderable, Show, Generic, Hashable)

makeLenses ''CandidateToken

instance (HasCommonTokenData (CandidateToken metadata) metadata) where
  {-# INLINE commonTokenData #-}
  commonTokenData = rawCandidateToken . commonTokenData

instance HasIdentifier (CandidateToken metadata) where
  {-# INLINE identifier #-}
  identifier = commonTokenData . identifier

data TokenAddition metadata = AlreadyPresent | FreshlyAdded (WMEToken' metadata) deriving (Generic)

makePrisms ''TokenAddition

instance Renderable metadata => Renderable (TokenAddition metadata) where
  render AlreadyPresent = "Token already present"
  render (FreshlyAdded tok) = "Freshly added token:" <+> render tok

instance Renderable metadata => Show (TokenAddition metadata) where
  show = show . render

deriving instance Eq (WMEToken' metadata) => Eq (TokenAddition metadata)

deriving instance Hashable (WMEToken' metadata) => Hashable (TokenAddition metadata)

-- data RuleActivation metadata = RuleActivation (WMEToken' metadata) deriving (Eq, Show, Generic, Hashable)

-- data RuleRetrmetadata = RuleRetr(WMEToken' metadata) deriving (Eq, Show, Generic, Hashable)

data ProductionRuleActivity metadata = ProductionRuleActivity {_ruleFirings :: STMS.Set (WMEToken' metadata ? IsProductionToken' metadata ) , _ruleRetractions :: STMS.Set (WMEToken' metadata ? IsProductionToken' metadata)}

makeClassy ''ProductionRuleActivity
{-# INLINE newProductionRuleActivity #-}
newProductionRuleActivity :: STM (ProductionRuleActivity metadata)
newProductionRuleActivity = ProductionRuleActivity <$> STMS.new <*> STMS.new

resetProductionRuleActivity :: ProductionRuleActivity metadata -> STM ()
resetProductionRuleActivity (ProductionRuleActivity a r) = do
  STMS.reset a
  STMS.reset r



data AccumulatedTokenMatches metadata = AccumulatedTokenMatches { _accumulatedWMEs :: Seq (WME' metadata), _accumulatedSubstitution :: SubstitutionLattice Value} deriving stock (Eq, Show)
makeClassy ''AccumulatedTokenMatches


instance Renderable metadata => Renderable (AccumulatedTokenMatches metadata) where
    render (AccumulatedTokenMatches wmes sub) = vsep ["Accumulated token matches",
                                                      "----------------------------",
                                                 "WMEs:" <+> render wmes,
                                                      "Substitution:" <+> render sub]

           
{-# INLINEABLE tokenMatchData #-}
-- TODO FIXME i think this is going to bug out when multiple rules are in the net with different variable
-- names...? HOW TO FIX: Compute variable remapping rule names to actual variable names in the nodes. Store
-- this mapping in the rule; or better yet store a "corrected" list of variable names in the rule and act on
-- them instead.
tokenMatchData :: forall metadata.  WMEToken' metadata -> AccumulatedTokenMatches metadata
tokenMatchData tok = AccumulatedTokenMatches wmes (-- CE.assert (sub /= incompatibleSubstitution)
                                                     sub) where
  (sub,wmes) = combinePatternMatchesOf (runFold $ (Fold (match )) -- <> (Fold (immediateParents.folded.match))
                                                    <> (Fold (tailMatches.folded.match))) tok


               
{-# INLINEABLE combineTokenMatches #-}
-- TODO OPTIMISE!!!
combineTokenMatches :: forall metadata f. Foldable f => f (WMEToken' metadata) -> AccumulatedTokenMatches metadata
combineTokenMatches toks = AccumulatedTokenMatches wmes sub  where
    (sub, wmes) = combinePatternMatchesOf (runFold $ (Fold (folded . match ))
                                                    <> (Fold (folded . tailMatches.folded.match))) toks
    
  


data OriginIsNewlyNonEmpty metadata = OriginIsNewlyNonEmpty -- (NonEmpty (WMEToken' metadata))
  | OriginAlreadyHadTokens -- (NonEmpty (WMEToken' metadata))
  | OriginIsAlphaNode
  | ReachedProductionNode
  | ActivatedNodeIsBeingConstructed
  | OriginIsNewlyEmpty

data LeftOrRightActivation = LeftActivation | RightActivation        deriving (Eq  , Show)

