
{-# LANGUAGE InstanceSigs #-}

module Rete.Types.CollectionTokens where
import qualified Prelude
----------import qualified Control.Exception as CE

import Cosmolude 
-- import Data.Foldable.WithIndex
-- import Data.Functor.WithIndex
import Data.Functor.WithIndex.Instances ()
--import qualified Data.HashMap.Strict as HM
--import Data.Semigroup
-- import Data.Traversable.WithIndex
--import GHC.IsList (IsList)
--------------------------import Rete.Types.Nodes
import qualified StmContainers.Set as STMS
--------------------import Algebra.PartialOrd
    
data ColumnIdentifier = ColumnIdentifier {_alphaNodeID :: NodeIdentifier, _introducingNodeID :: NodeIdentifier}
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable, Renderable)

makeLenses ''ColumnIdentifier

-- instance HasIdentifier ColumnIdentifier where
--   identifier = nodeID . identifier




-- data MatchEquivalencies' a = MatchEquivalencies {_equivalencies :: STMS.Set a} deriving (Generic)

data TokenColumn' a = TokenColumn {_columnID :: {-# UNPACK #-} !ColumnIdentifier, _contents :: STMS.Set (STMS.Set a)} deriving (Generic)
makeLenses ''TokenColumn'
type TokenColumn = TokenColumn' VariableBinding

makeTokenColumn :: ColumnIdentifier -> STM (TokenColumn' a)
makeTokenColumn ident = TokenColumn <$> pure ident <*> STMS.new

mergeNestedSetsWhenDifferentByOne :: (Hashable a, Eq a) => f (f a) -> f (f a) -> Maybe (f (f a))
mergeNestedSetsWhenDifferentByOne _left _right = Prelude.error "mergeNestedSetsWhenDifferentByOne" {-do
  res <- [(x,y) | x^.. wat just use a  join here. either inner or outer. 
  -}
  
  
  
  
                        
mergeTokenColumns :: Eq a => TokenColumn' a -> TokenColumn' a -> STM (Maybe (TokenColumn' a))
mergeTokenColumns (TokenColumn _leftID _leftContents) (TokenColumn _rightID _rightContents) = Prelude.error "mergeTokenColumns" --do
  



-- instance HasIdentifier (TokenColumn' a) where
--   identifier = columnID . identifier

-- deriving via ByIdentifier (TokenColumn' a) instance Eq (TokenColumn' a)
