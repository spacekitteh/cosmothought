module Rete.Types.Tokens where
import Rete.Types.Nodes
import Core.Patterns.PatternSyntax
import qualified Data.HashMap.Strict as HM
import qualified System.IO.Unsafe as UNSAFE
import Cosmolude hiding ((...))
import Core.CoreTypes



import Logic.Proof
import qualified StmContainers.Set as STMS

--------------------------------------------------------------------------------
-- Common token data
--------------------------------------------------------------------------------

-- | Invariants:
-- - forall t in wmeToken^.commonTokenData..childTokens  .     t^.immediateParents.head == wmeToken
-- - exists parent  . parent =  wmeToken^.commonTokenData.immediateParents.head  ==> wmeToken is in parent^.commonTokenData.childTokens
data CommonTokenData a = CommonTokenData
  { _tokenID :: TokenIdentifier,
    _containingReteNode :: ReteNode (WMEToken' a) a,
    _tailMatches :: HM.HashMap NodeIdentifier (WMEToken' a),
    _immediateParents :: Seq (WMEToken' a),
    _childTokens :: STMS.Set (WMEToken' a),
    _accumulatedTruth' :: WMETruth
  }
  deriving (Generic)

instance Renderable a => Show (CommonTokenData a) where
  show = show . render

instance Renderable a => Renderable (CommonTokenData a) where
  render CommonTokenData {..} = vsep ["Token ID" <+> render _tokenID,
                                      "in node" <+> render _containingReteNode,
                                      "Accumulated truth" <+> render _accumulatedTruth',
                                      "Parent tokens:" <+>  hang 2 ( (vsep (render <$> (toList _tailMatches))))]

-- instance Each/Cons CommonTokenData for WMEPatternMatch', making sure it's always correct
--------------------------------------------------------------------------------
-- Tokens
--------------------------------------------------------------------------------

data WMEToken' a
  = MemToken {_match :: WMEPatternMatch' a,_memBlock :: MemoryBlock (WMEToken' a) a, _commonData :: CommonTokenData a}
  | NegatedToken { -- _patternMatchedAgainst :: WMEPattern, _wmeSubstitution' :: Substitution Value,
                  _negativeJoinResults' :: STMS.Set (NegativeJoinResult' a), _memBlock :: MemoryBlock (WMEToken' a) a, _commonData :: CommonTokenData a}
  | NCCToken {  _nccResults' :: STMS.Set ((WMEToken' a) ? IsNCCPartnerToken' a),
               _memBlock :: MemoryBlock (WMEToken' a) a, _commonData :: CommonTokenData a}
  | NegatedConjunctionPartnerToken {_nccTokenOwner :: TVar ( Maybe ((WMEToken' a) ? IsNCCToken' a)),  _memBlock :: MemoryBlock (WMEToken' a) a, _commonData :: CommonTokenData a}
  | ProductionToken {_memBlock :: MemoryBlock (WMEToken' a) a, _commonData :: CommonTokenData a}
  deriving (Generic)


data NegativeJoinResult' metadata = NegativeJoinResult {_owner :: WMEToken' metadata, _wme :: WME' metadata} deriving (Generic, Show)
--instance Show (WME' metadata) => Show (NegativeJoinResult' metadata) where
--  show (NegativeJoinResult token wme) = show wme
instance Renderable metadata => Renderable (NegativeJoinResult' metadata) where
  render (NegativeJoinResult owner wme) = "Negative join result:" <+> render owner <+> render wme

data TokenTypes metadata = AMemToken metadata
  | ANegatedToken metadata
  | ANCCToken metadata
  | ANCCPartnerToken metadata 
  | AProductionToken metadata
newtype TokenType name = TokenType Defn
type role TokenType nominal

newtype IsProductionToken' metadata name = IsProductionToken Defn
type role IsProductionToken' representational nominal

newtype IsNegationToken' metadata name = IsNegationToken Defn
type role IsNegationToken' representational nominal
newtype IsNCCPartnerToken' metadata name = IsNCCPartnerToken Defn
type role IsNCCPartnerToken' representational nominal
newtype IsNCCToken' metadata name = IsNCCToken Defn
type role IsNCCToken' representational nominal

instance SumTypeFamily (WMEToken' metadata) where
  type Discriminator (WMEToken' metadata) = TokenType


instance forall metadata.  ProveCorrectSubType (WMEToken' metadata) (IsProductionToken' metadata) (AProductionToken metadata) where
  type IsCorrectSubType (AProductionToken metadata) = IsProductionToken' metadata
  {-# INLINE proveIsCorrectSubType #-}
  proveIsCorrectSubType (The (ProductionToken {})) = Just axiom
  proveIsCorrectSubType _ = Nothing
                            
instance forall metadata.  ProveCorrectSubType (WMEToken' metadata) (IsNegationToken' metadata) (ANegatedToken metadata) where
  type IsCorrectSubType (ANegatedToken metadata) = IsNegationToken' metadata
  {-# INLINE proveIsCorrectSubType #-}
  proveIsCorrectSubType (The (NegatedToken {})) = Just axiom
  proveIsCorrectSubType _ = Nothing

instance forall metadata.  ProveCorrectSubType (WMEToken' metadata) (IsNCCPartnerToken' metadata) (ANCCPartnerToken metadata) where
  type IsCorrectSubType (ANCCPartnerToken metadata) = IsNCCPartnerToken' metadata
  {-# INLINE proveIsCorrectSubType #-}
  proveIsCorrectSubType (The (NegatedConjunctionPartnerToken {})) = Just axiom
  proveIsCorrectSubType _ = Nothing
instance forall metadata.  ProveCorrectSubType (WMEToken' metadata) (IsNCCToken' metadata) (ANCCToken metadata) where
  type IsCorrectSubType (ANCCToken metadata) = IsNCCToken' metadata
  {-# INLINE proveIsCorrectSubType #-}
  proveIsCorrectSubType (The (NCCToken {})) = Just axiom
  proveIsCorrectSubType _ = Nothing


instance Renderable a => Renderable (WMEToken' a) where
  render MemToken {..} = "Memory token:" <+> angles (render _match <+> render _commonData)
  render NegatedToken {..} = "Negated token:" <+> angles (render _commonData)
  render NCCToken {..} = vsep ["NCC token" <+> angles (render _commonData), nest 2 ("NCC partner results:" <+>  vsep (render <$> (UNSAFE.unsafePerformIO $ doTransaction (_nccResults'^!!elems))))]
  render NegatedConjunctionPartnerToken {..} = "NCC partner token:" <+> angles (render _commonData)
  render ProductionToken {..} = "Production token:" <+> angles (render _commonData)

instance Renderable a => Show (WMEToken' a) where
  show t@(NegatedToken {..}) = show $ render (UNSAFE.unsafePerformIO $ doTransaction (_negativeJoinResults'^!!elems) )<+> render t
  show t = show . render $ t

class HasWMETokens cont metadata where
  ownedTokens :: Lens' cont (STMS.Set (WMEToken' metadata))
  negativeJoinResults :: Lens' cont (STMS.Set ((NegativeJoinResult' metadata)))

instance HasWMETokens metadata metadata => HasWMETokens (WME' metadata) metadata where
  {-# INLINE ownedTokens #-}
  --  ownedTokens :: HasWMETokens c metadata => Lens' (WME' metadata) (Set (WMEToken' metadata))
  ownedTokens = metadata . ownedTokens
  {-# INLINE negativeJoinResults #-}
  negativeJoinResults = metadata . negativeJoinResults

makeClassy ''CommonTokenData
instance HasAccumulatedTruth (CommonTokenData a) Variable where
    {-# INLINE accumulatedTruth #-}
    accumulatedTruth = accumulatedTruth'
{-instance (HasCommonTokenData (CommonTokenData metadata) metadata) => HasWMETokens (CommonTokenData metadata) metadata where
  {-# INLINE ownedTokens #-}
  --  ownedTokens :: (HasCommonTokenData (CommonTokenData a) a) => Lens' (CommonTokenData a) (Set (WMEToken' a))
  ownedTokens = childTokens
  {-# INLINE negativeJoinResults #-}
  negativeJoinResults = -}
makeLenses ''NegativeJoinResult'
makeClassy ''WMEToken'
makeClassyPrisms ''WMEToken'
                 
instance HasAccumulatedTruth (WMEToken' a) Variable where
    {-# INLINE accumulatedTruth #-}
    accumulatedTruth = commonTokenData . accumulatedTruth

instance (WrappingClassyInstance tok (IsNCCPartnerToken' metadata) (ANCCPartnerToken metadata) (HasWMEToken' tok metadata)) => HasWMEToken' (tok ? IsNCCPartnerToken' metadata)  metadata where
  {-# INLINE wMEToken' #-}
  wMEToken' = promoteAffineToLens wMEToken'
instance (WrappingClassyInstance tok (IsNCCToken' metadata) (ANCCToken metadata) (HasWMEToken' tok metadata)) => HasWMEToken' (tok ? IsNCCToken' metadata)  metadata where
  {-# INLINE wMEToken' #-}
  wMEToken' = promoteAffineToLens wMEToken'
instance (WrappingClassyInstance tok (IsNCCPartnerToken' metadata) (ANCCPartnerToken metadata) (HasCommonTokenData tok metadata)) => HasCommonTokenData (tok ? IsNCCPartnerToken' metadata)  metadata where
  {-# INLINE commonTokenData #-}
  commonTokenData = promoteAffineToLens commonTokenData
instance (WrappingClassyInstance tok (IsNCCToken' metadata) (ANCCToken metadata) (HasCommonTokenData tok metadata)) => HasCommonTokenData (tok ? IsNCCToken' metadata)  metadata where
  {-# INLINE commonTokenData #-}
  commonTokenData = promoteAffineToLens commonTokenData
{-# INLINE nccResults #-}
nccResults :: HasCallStack => Lens' ((WMEToken' metadata) ? IsNCCToken' metadata) (STMS.Set ((WMEToken' metadata) ? IsNCCPartnerToken' metadata))
nccResults = promoteAffineToLens nccResults'


             
instance HasMemoryBlock (WMEToken' metadata) (WMEToken' metadata) metadata where
  {-# INLINE memoryBlock #-}
  memoryBlock = memBlock

instance HasCommonTokenData (WMEToken' a) a where
  {-# INLINE commonTokenData #-}
  commonTokenData = commonData
-- instance (ProveCorrectSubType (WMEToken' a) IsProductionToken, HasCommonTokenData (WMEToken' a) a) => HasCommonTokenData ((WMEToken' a) ? pred) a where
--   {-# INLINE commonTokenData #-}
--   commonTokenData = promoteAffineToLens commonData
-- instance HasWMEIdentifier (WMEToken' a) where
--   {-# INLINE wMEIdentifier #-}
--   wMEIdentifier = commonTokenData . wMEIdentifier

instance HasIdentifier (CommonTokenData a) where
  {-# INLINE identifier #-}
  identifier = tokenID . identifier

deriving via ByIdentifier (CommonTokenData a) instance Eq (CommonTokenData a)

deriving via ByIdentifier (CommonTokenData a) instance Hashable (CommonTokenData a)

instance HasIdentifier (WMEToken' metadata) where
  {-# INLINE identifier #-}
  identifier = commonData . identifier

deriving via ByIdentifier (WMEToken' metadata) instance Eq (WMEToken' metadata)

deriving via ByIdentifier (WMEToken' metadata) instance Hashable (WMEToken' metadata)
instance Eq (NegativeJoinResult' metadata) where
  {-# INLINE (==) #-}
  a == b = a^.owner == b^.owner
instance Hashable (NegativeJoinResult' metadata) where
  hashWithSalt salt a = hashWithSalt salt (a^.owner)

-- instance HasWMEIdentifier (CommonTokenData a) where
--   {-# INLINE wMEIdentifier #-}
--   wMEIdentifier = match . wMEIdentifier
{-# INLINE tokenWMEFromNode #-}
{-# SCC tokenWMEFromNode #-}
tokenWMEFromNode :: HasCommonTokenData c metadata => NodeIdentifier -> Traversal' (WMEToken' metadata) (WMEToken' metadata)
tokenWMEFromNode ident = failing (filtered (\other -> ident == other ^. containingReteNode . nodeIdentifier)) (tailMatches . at ident . _Just)
