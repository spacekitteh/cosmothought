{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE UndecidableSuperClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Rete.Types.Nodes (CommonNodeData (CommonNodeData'), HasCommonNodeData (..), HasReteNode (..), ReteNode, pattern AlphaNode, _AlphaNode, pattern JoinNode, _JoinNode,  pattern MemoryNode, _MemoryNode, pattern NegativeNode, _NegativeNode, pattern NegatedConjugationNode, _NegatedConjugationNode, pattern NegatedConjugationPartnerNode, _NegatedConjugationPartnerNode, pattern ProductionNode, _ProductionNode, IsAlphaNode', IsMemoryNode', IsJoinNode', IsNegativeNode', IsNCCNode', IsNCCPartnerNode', IsProductionNode', MemoryBlock (MemoryBlock'), HasMemoryBlock (..), JoinPredicate (..), JoinCondition (..), HasJoinCondition (..), JoinPredicateSatisfaction (..), joinConditions, referenceCount, matchPattern, memoryNode, betaMemoryNode, allChildNodesCollection, hasEmptyMemory, constantTestHashKey,allAncestors, allChildNodes, rightAmem, getReferenceCount, childNodes, parentNodes, canHaveParentNodes, canHaveChildNodes, emptyParentNodes, ParentArity (..), RelativesCollection, ignoringLevel, ReteNodeOrderingInvariantException (..),  NodeLevel, WMEInAlphaNodes (..), HasJoinConditions, getsLinkedWhenEmpty, nccPartnerNode, actualNCCNode, pendingResultsForNCCNode, _UNSAFEchildNodes, nodeSymbol, nodeSymbolWithID, nodeSymbolWithIDPlain, matchVariableMapping, patternComparisonTestToJoinPredicate, priorTokenNode, priorTokenField, currentTokenNode, currentTokenField, tokenNode, tokenField, JoinNodeArgument (JoinNodeArgument), JoinArgument(..)) where

import Control.Exception (Exception)


import Core.CoreTypes
import Core.Patterns.HashKey
import Cosmolude hiding (TVar, readTVar, (...))
import qualified Data.HashSet as HS
import Data.Heap as Heap
import Data.Refined

import GHC.TypeLits
import Logic.Proof
--import Logic.Propositional
import qualified System.IO.Unsafe as UNSAFE
import  Control.Concurrent.STM
import StmContainers.Set as STMS
--import Data.HashPSQ
--import Theory.Equality
--import Theory.Named

data JoinPredicate = LT | LTE | EQ | GTE | GT | NE deriving (Eq, Ord, Generic, Hashable, Enum, Bounded)

patternComparisonTestToJoinPredicate :: ComparisonTest -> JoinPredicate
patternComparisonTestToJoinPredicate (LessThan _) = LT
patternComparisonTestToJoinPredicate (LessThanOrEqualTo _) = LTE
patternComparisonTestToJoinPredicate (EqualTo _) = EQ
patternComparisonTestToJoinPredicate (GreaterThan _) = GT
patternComparisonTestToJoinPredicate (GreaterThanOrEqualTo _) = GTE
patternComparisonTestToJoinPredicate (NotEqualTo _) = NE
                   
type STMType a = forall r. Member (Transactional) r => Sem r a --Control.Concurrent.STM.STM

data JoinNodeArgument = JoinNodeArgument {_tokenField :: WMEField, _tokenNode :: NodeIdentifier} deriving (Eq, Ord, Generic, Hashable)
instance Renderable JoinNodeArgument where
    render (JoinNodeArgument f n) = render n <> ":::" <> render f
data JoinArgument = NodeArgument JoinNodeArgument
                  | ValueArgument Value deriving (Eq, Ord, Generic, Hashable)
instance Renderable JoinArgument where
    render (NodeArgument arg) = render arg
    render (ValueArgument v) = render v
makePrisms ''JoinArgument
makeLenses ''JoinNodeArgument    
data JoinCondition = JoinCondition
  { _currentArgument :: JoinNodeArgument,
    _priorArgument :: JoinArgument,
    _joinTest :: JoinPredicate
  }
  deriving (Eq, Ord, Generic, Hashable)

makeClassy ''JoinCondition

{-# INLINE currentTokenField  #-}
currentTokenField :: Lens' JoinCondition WMEField
currentTokenField = currentArgument . tokenField
{-# INLINE currentTokenNode #-}                    
currentTokenNode :: Lens' JoinCondition NodeIdentifier
currentTokenNode = currentArgument . tokenNode
{-# INLINE priorTokenField #-}                   
priorTokenField ::  Traversal' JoinCondition  WMEField
priorTokenField =  (priorArgument . _NodeArgument . tokenField)
{-# INLINE priorTokenNode #-}                  
priorTokenNode :: Traversal' JoinCondition NodeIdentifier                  
priorTokenNode = priorArgument . _NodeArgument . tokenNode
           
data JoinPredicateSatisfaction = JoinPredicateFails | JoinPredicateMet Value JoinPredicate Value deriving (Eq, Ord, Show, Generic, Hashable)


instance Renderable JoinPredicate where
  render LT = "<"
  render LTE = "≤"
  render EQ = "≛"
  render GTE = "≥"
  render GT = ">"
  render NE = "≠"

instance Show JoinPredicate where
  show = show . render

instance Renderable JoinCondition where
  render JoinCondition {..} = render _currentArgument <+> render _joinTest <+> render _priorArgument

instance Show JoinCondition where
  show = show . render

type NodeLevel = Natural

--instance Pretty (ReteNode tok a) => Pretty (Entry NodeLevel (ReteNode tok a)) where
--  pretty (Entry lvl node) = ("Level:" <+> pretty lvl) <+> ("Node:" <+> pretty node)

type RelativesCollection tok a = Heap (Entry NodeLevel (ReteNode tok a))
--type RelativesCollection tok a = HashPSQ NodeIdentifier NodeLevel (ReteNode tok a)
--payload :: a -> a
--payload = id
{-# INLINE ignoringLevel #-}
--ignoringLevel :: a -> a
--ignoringLevel = id
ignoringLevel :: Lens' (Entry NodeLevel (ReteNode tok a)) (ReteNode tok a)
ignoringLevel = lens payload (\(Entry lvl _) node -> Entry lvl node)

--------------------------------------------------------------------------------
-- Common node data
--------------------------------------------------------------------------------

data ParentArity = Nullary | Unary | Binary | KAry deriving (Eq, Show, Generic)

instance Renderable ParentArity where
  render = viaShow

data CommonNodeData (tok :: Type) (a :: Type) = CommonNodeData'
  { _nodeIdentifier :: NodeIdentifier,
    _level :: NodeLevel, -- Invariant: a^.childNodes.all.level > a^.level > a.parentNodes.all.level,
    _childNodes' :: TVar (RelativesCollection tok a),
    _emptyParentNodes' :: TVar  (Seq (ReteNode tok a)),
    _parentNodes' :: TVar  (RelativesCollection tok a),
    _parentArity :: ParentArity
  }
  deriving stock (Generic)


--------------------------------------------------------------------------------
-- Rete nodes
--------------------------------------------------------------------------------

{--- | For each activating node, this caches the cartesian product of tokens in the other nodes
data JoinKAryCachedToks tok = JoinKAryCachedToks (HashMap NodeIdentifier (HashSet (HashSet tok)))
addTokensToCachedToks :: Hashable tok => JoinKAryCachedToks tok
-}

data NodeTypes tok metadata = Alpha tok metadata | Join tok metadata | Negative tok metadata | Production tok metadata | Memory tok metadata | NCC tok metadata | NCCPartner tok metadata

data NodeType name -- = NodeType Defn

type role
  NodeType -- representational representational
    nominal

newtype IsAlphaNode' tok metadata name = IsAlphaNode' Defn

type role IsAlphaNode' representational representational nominal

newtype IsJoinNode' tok metadata name = IsJoinNode' Defn

type role IsJoinNode' representational representational nominal

newtype IsMemoryNode' tok metadata name = IsMemoryNode' Defn

type role IsMemoryNode' representational representational nominal

newtype IsNegativeNode' tok metadata name = IsNegativeNode' Defn

type role IsNegativeNode' representational representational nominal

newtype IsProductionNode' tok metadata name = IsProductionNode' Defn

type role IsProductionNode' representational representational nominal

newtype IsNCCNode' tok metadata name = IsNCCNode' Defn

type role IsNCCNode' representational representational nominal

newtype IsNCCPartnerNode' tok metadata name = IsNCCPartnerNode' Defn

type role IsNCCPartnerNode' representational representational nominal

instance SumTypeFamily (ReteNode tok metadata) where
  type Discriminator (ReteNode tok metadata) = NodeType

type family HasJoinConditions tok metadata (pred :: Type -> Type) :: Constraint where
  HasJoinConditions tok metadata (IsJoinNode' tok metadata) = ()
  HasJoinConditions tok metadata (IsNegativeNode' tok metadata) = ()
  HasJoinConditions tok metadata ty = TypeError ((ShowType ty) :<>: Text " does not have join conditions.")




data ReteNode tok a
  = AlphaNode' {_matchPattern' :: WMEPattern, _memory :: MemoryBlock tok a, _referenceCount' :: TVar  Natural, _constantTestHashKey' :: MemoryConstantTestHashKey ,  _cNodeData :: CommonNodeData tok a }
  | JoinNode'
      { _joinConditionsByInitiatingNode :: HashMap NodeIdentifier (HashSet (JoinCondition)),
        -- | INVARIANT: rightAmem is the amem that corresponds to the first JoinCondition, and is included in Parents.
        _rightAmem' :: TVar  ((ReteNode tok a) ? IsAlphaNode' tok a),
        _memoryNode' :: TVar (ReteNode tok a ? IsMemoryNode' tok a),
        _cNodeData :: CommonNodeData tok a
      }
  | NegativeNode' {_joinConditionsByInitiatingNode :: HashMap NodeIdentifier (HashSet JoinCondition), _rightAmem' :: TVar ((ReteNode tok a) ? IsAlphaNode' tok a), _memory :: MemoryBlock tok a, _cNodeData :: CommonNodeData tok a}
  | ProductionNode' {_associatedRule :: RuleIdentifier, _memory :: MemoryBlock tok a, _matchVariableMapping' :: Substitution Variable, _cNodeData :: CommonNodeData tok a}
  | MemoryNode' {_memory :: MemoryBlock tok a, _allChildNodes' :: TVar (RelativesCollection tok a), _cNodeData :: CommonNodeData tok a}
  | NegatedConjugationNode' {_memory :: MemoryBlock tok a, _partnerNode' :: ((ReteNode tok a) ? IsNCCPartnerNode' tok a), _cNodeData :: CommonNodeData tok a}
  | NegatedConjugationPartnerNode' {_nccBranchPoint :: NodeIdentifier, _pendingResultsForNCCNode' :: MemoryBlock tok a, _actualNCCNode' :: TVar ((ReteNode tok a) ? IsNCCNode' tok a), _cNodeData :: CommonNodeData tok a}
  deriving (Generic)

{-# INLINEABLE canHaveChildNodes #-}
canHaveChildNodes :: ReteNode tok a -> Bool
canHaveChildNodes ProductionNode' {} = False
canHaveChildNodes NegatedConjugationPartnerNode' {} = False
canHaveChildNodes _ = True

{-# INLINEABLE getsLinkedWhenEmpty #-}
getsLinkedWhenEmpty :: ReteNode tok a -> Bool
getsLinkedWhenEmpty NegativeNode' {} = True
getsLinkedWhenEmpty _ = False

{-# INLINEABLE canHaveParentNodes #-}
canHaveParentNodes :: ReteNode tok a -> Bool
canHaveParentNodes AlphaNode' {} = False
canHaveParentNodes _ = True

pattern AlphaNode :: WMEPattern -> MemoryBlock token metadata -> TVar Natural -> MemoryConstantTestHashKey ->CommonNodeData token metadata -> (ReteNode token metadata) ? IsAlphaNode' token metadata
pattern AlphaNode matchPattern memoryBlock refCount testHashKey nodeData <- The (AlphaNode' matchPattern memoryBlock refCount testHashKey nodeData)
  where
    AlphaNode matchPattern memoryBlock refCount testHashKey nodeData = Data.Refined.assert (AlphaNode' matchPattern memoryBlock refCount testHashKey nodeData)

{-# INLINE _AlphaNode #-}
_AlphaNode :: Prism' (ReteNode tok metadata) ((ReteNode tok metadata) ? IsAlphaNode' tok metadata)
_AlphaNode = _CorrectSubType

pattern JoinNode ::
  HashMap NodeIdentifier (HashSet JoinCondition) ->
  TVar ((ReteNode tok metadata) ? IsAlphaNode' tok metadata) ->
  TVar ((ReteNode tok metadata) ? IsMemoryNode' tok metadata) ->
  CommonNodeData tok metadata ->
  (ReteNode tok metadata) ? IsJoinNode' tok metadata
pattern JoinNode conds amem mem cnd <- The (JoinNode' conds amem mem cnd)
  where
    JoinNode conds amem mem cnd = Data.Refined.assert (JoinNode' conds amem mem cnd)

{-# INLINE _JoinNode #-}
_JoinNode :: Prism' (ReteNode tok metadata) ((ReteNode tok metadata) ? IsJoinNode' tok metadata)
_JoinNode = _CorrectSubType

pattern NegativeNode :: HashMap NodeIdentifier (HashSet JoinCondition) -> TVar ((ReteNode tok metadata) ? IsAlphaNode' tok metadata) -> MemoryBlock tok metadata -> CommonNodeData tok metadata -> (ReteNode tok metadata) ? IsNegativeNode' tok metadata
pattern NegativeNode conds amem mem cnd <- The (NegativeNode' conds amem mem cnd)
  where
    NegativeNode conds amem mem cnd = Data.Refined.assert (NegativeNode' conds amem mem cnd)
    
{-# INLINE _NegativeNode #-}
_NegativeNode :: Prism' (ReteNode tok metadata) ((ReteNode tok metadata) ? IsNegativeNode' tok metadata)
_NegativeNode = _CorrectSubType

pattern ProductionNode :: RuleIdentifier -> MemoryBlock tok metadata -> Substitution Variable -> CommonNodeData tok metadata -> (ReteNode tok metadata) ? IsProductionNode' tok metadata
pattern ProductionNode ruleID mem varMap cnd <- The (ProductionNode' ruleID mem varMap cnd)
  where
    ProductionNode ruleID mem varMap cnd = Data.Refined.assert (ProductionNode' ruleID mem varMap cnd)

{-# INLINE _ProductionNode #-}
_ProductionNode :: Prism' (ReteNode tok metadata) ((ReteNode tok metadata) ? IsProductionNode' tok metadata)
_ProductionNode = _CorrectSubType

pattern MemoryNode :: MemoryBlock tok metadata -> TVar (RelativesCollection tok metadata) -> CommonNodeData tok metadata -> (ReteNode tok metadata) ? IsMemoryNode' tok metadata
pattern MemoryNode mem children cnd <- The (MemoryNode' mem children cnd)
  where
    MemoryNode mem children cnd = Data.Refined.assert (MemoryNode' mem children cnd)


{-# INLINE _MemoryNode #-}
_MemoryNode :: Prism' (ReteNode tok metadata) ((ReteNode tok metadata) ? IsMemoryNode' tok metadata)
_MemoryNode = _CorrectSubType


pattern NegatedConjugationNode :: MemoryBlock tok metadata -> ((ReteNode tok metadata) ? IsNCCPartnerNode' tok metadata) -> CommonNodeData tok metadata -> (ReteNode tok metadata) ? IsNCCNode' tok metadata
pattern NegatedConjugationNode mem partner cnd <- The (NegatedConjugationNode' mem partner cnd)
  where
    NegatedConjugationNode mem partner cnd = Data.Refined.assert (NegatedConjugationNode' mem partner cnd)

{-# INLINE _NegatedConjugationNode #-}
_NegatedConjugationNode :: Prism' (ReteNode tok metadata) ((ReteNode tok metadata) ? IsNCCNode' tok metadata)
_NegatedConjugationNode = _CorrectSubType



pattern NegatedConjugationPartnerNode ::  NodeIdentifier -> MemoryBlock tok metadata  ->  TVar ((ReteNode tok metadata) ? IsNCCNode' tok metadata) -> CommonNodeData tok metadata -> (ReteNode tok metadata) ? IsNCCPartnerNode' tok metadata
pattern NegatedConjugationPartnerNode branchPoint memblock actual cnd <- The (NegatedConjugationPartnerNode' branchPoint memblock actual cnd)
  where
    NegatedConjugationPartnerNode branchPoint memblock actual cnd = Data.Refined.assert (NegatedConjugationPartnerNode' branchPoint memblock actual cnd)


{-# INLINE _NegatedConjugationPartnerNode #-}
_NegatedConjugationPartnerNode :: Prism' (ReteNode tok metadata) ((ReteNode tok metadata) ? IsNCCPartnerNode' tok metadata)
_NegatedConjugationPartnerNode = _CorrectSubType
    

instance forall tok metadata. ProveCorrectSubType (ReteNode tok metadata) (IsAlphaNode' tok metadata) (Alpha tok metadata) where
  type IsCorrectSubType (Alpha tok metadata) = IsAlphaNode' tok metadata

  {-# INLINE proveIsCorrectSubType #-}
  proveIsCorrectSubType (The (AlphaNode' {})) = Just axiom
  proveIsCorrectSubType _ = Nothing

instance forall tok metadata. ProveCorrectSubType (ReteNode tok metadata) (IsMemoryNode' tok metadata) (Memory tok metadata) where
  type IsCorrectSubType (Memory tok metadata) = IsMemoryNode' tok metadata

  {-# INLINE proveIsCorrectSubType #-}
  proveIsCorrectSubType (The (MemoryNode' {})) = Just axiom
  proveIsCorrectSubType _ = Nothing

instance forall tok metadata. ProveCorrectSubType (ReteNode tok metadata) (IsJoinNode' tok metadata) (Join tok metadata) where
  type IsCorrectSubType (Join tok metadata) = IsJoinNode' tok metadata

  {-# INLINE proveIsCorrectSubType #-}
  proveIsCorrectSubType (The (JoinNode' {})) = Just axiom
  proveIsCorrectSubType _ = Nothing

instance forall tok metadata. ProveCorrectSubType (ReteNode tok metadata) (IsNegativeNode' tok metadata) (Negative tok metadata) where
  type IsCorrectSubType (Negative tok metadata) = IsNegativeNode' tok metadata

  {-# INLINE proveIsCorrectSubType #-}
  proveIsCorrectSubType (The (NegativeNode' {})) = Just axiom
  proveIsCorrectSubType _ = Nothing

instance forall tok metadata. ProveCorrectSubType (ReteNode tok metadata) (IsNCCNode' tok metadata) (NCC tok metadata) where
  type IsCorrectSubType (NCC tok metadata) = IsNCCNode' tok metadata

  {-# INLINE proveIsCorrectSubType #-}
  proveIsCorrectSubType (The (NegatedConjugationNode' {})) = Just axiom
  proveIsCorrectSubType _ = Nothing

instance forall tok metadata. ProveCorrectSubType (ReteNode tok metadata) (IsNCCPartnerNode' tok metadata) (NCCPartner tok metadata) where
  type IsCorrectSubType (NCCPartner tok metadata) = IsNCCPartnerNode' tok metadata

  {-# INLINE proveIsCorrectSubType #-}
  proveIsCorrectSubType (The (NegatedConjugationPartnerNode' {})) = Just axiom
  proveIsCorrectSubType _ = Nothing

instance forall tok metadata. ProveCorrectSubType (ReteNode tok metadata) (IsProductionNode' tok metadata) (Production tok metadata) where
  type IsCorrectSubType (Production tok metadata) = IsProductionNode' tok metadata

  {-# INLINE proveIsCorrectSubType #-}
  proveIsCorrectSubType (The (ProductionNode' {})) = Just axiom
  proveIsCorrectSubType _ = Nothing

--------------------------------------------------------------------------------
-- Memories
--------------------------------------------------------------------------------

data MemoryBlock tok metadata = MemoryBlock' {_inReteNode :: ReteNode tok metadata, _tokens :: STMS.Set (tok)} deriving (Generic)

instance Renderable (MemoryBlock tok a) where
  render _ = "memory blocK: PRETTY TODO"

instance Show (MemoryBlock tok a) where
  show = show . render

--------------------------------------------------------------------------------
-- Common node data instances
--------------------------------------------------------------------------------
makeClassy ''CommonNodeData

instance (WrappingClassyInstance node (IsAlphaNode' tok meta) (Alpha tok meta) (HasCommonNodeData node tok meta)) => HasCommonNodeData (node ? IsAlphaNode' tok meta) tok meta where
  {-# INLINE commonNodeData #-}
  commonNodeData = promoteAffineToLens commonNodeData

instance (WrappingClassyInstance node (IsMemoryNode' tok meta) (Memory tok meta) (HasCommonNodeData node tok meta)) => HasCommonNodeData (node ? IsMemoryNode' tok meta) tok meta where
  {-# INLINE commonNodeData #-}
  commonNodeData = promoteAffineToLens commonNodeData

instance (WrappingClassyInstance node (IsJoinNode' tok meta) (Join tok meta) (HasCommonNodeData node tok meta)) => HasCommonNodeData (node ? IsJoinNode' tok meta) tok meta where
  {-# INLINE commonNodeData #-}
  commonNodeData = promoteAffineToLens commonNodeData

instance (WrappingClassyInstance node (IsProductionNode' tok meta) (Production tok meta) (HasCommonNodeData node tok meta)) => HasCommonNodeData (node ? IsProductionNode' tok meta) tok meta where
  {-# INLINE commonNodeData #-}
  commonNodeData = promoteAffineToLens commonNodeData

instance (WrappingClassyInstance node (IsNegativeNode' tok meta) (Negative tok meta) (HasCommonNodeData node tok meta)) => HasCommonNodeData (node ? IsNegativeNode' tok meta) tok meta where
  {-# INLINE commonNodeData #-}
  commonNodeData = promoteAffineToLens commonNodeData

instance (WrappingClassyInstance node (IsNCCNode' tok meta) (NCC tok meta) (HasCommonNodeData node tok meta)) => HasCommonNodeData (node ? IsNCCNode' tok meta) tok meta where
  {-# INLINE commonNodeData #-}
  commonNodeData = promoteAffineToLens commonNodeData

instance (WrappingClassyInstance node (IsNCCPartnerNode' tok meta) (NCCPartner tok meta) (HasCommonNodeData node tok meta)) => HasCommonNodeData (node ? IsNCCPartnerNode' tok meta) tok meta where
  {-# INLINE commonNodeData #-}
  commonNodeData = promoteAffineToLens commonNodeData





instance HasIdentifier (CommonNodeData tok a) where
  {-# INLINE identifier #-}
  identifier = nodeIdentifier . identifier


               
deriving via ByIdentifier (CommonNodeData tok a) instance Eq (CommonNodeData tok a)

deriving via ByIdentifier (CommonNodeData tok a) instance Ord (CommonNodeData tok a)

deriving via ByIdentifier (CommonNodeData tok a) instance Hashable (CommonNodeData tok a)

{-# INLINE childNodes #-}

{-# SCC Rete.Types.Nodes.childNodes #-}

childNodes :: (HasCommonNodeData c tok a, Member Transactional r) => Action (Sem r) c (RelativesCollection tok a)
childNodes = act \c -> readT (c ^. childNodes')

{-# INLINE parentNodes #-}

{-# SCC Rete.Types.Nodes.parentNodes #-}

parentNodes :: (HasCommonNodeData c tok a, Member Transactional r) => Action (Sem r) c (RelativesCollection tok a)
parentNodes = act \c -> readT (c ^. parentNodes')

{-# INLINE emptyParentNodes #-}

{-# SCC Rete.Types.Nodes.emptyParentNodes #-}

emptyParentNodes :: (HasCommonNodeData c tok a, Member Transactional r) => Action (Sem r) c (Seq (ReteNode tok a))
emptyParentNodes = act \c -> readT (c ^. emptyParentNodes')

--------------------------------------------------------------------------------
-- Memory block instances
--------------------------------------------------------------------------------
makeClassy ''MemoryBlock

--------------------------------------------------------------------------------
-- Rete node instances
--------------------------------------------------------------------------------
-- makeClassyPrisms ''ReteNode
makeClassy ''ReteNode

instance (WrappingClassyInstance node (IsAlphaNode' tok meta) (Alpha tok meta) (HasReteNode node tok meta)) => HasReteNode (node ? IsAlphaNode' tok meta) tok meta where
  {-# INLINE reteNode #-}
  reteNode = promoteAffineToLens reteNode

instance (WrappingClassyInstance node (IsMemoryNode' tok meta) (Memory tok meta) (HasReteNode node tok meta)) => HasReteNode (node ? IsMemoryNode' tok meta) tok meta where
  {-# INLINE reteNode #-}
  reteNode = promoteAffineToLens reteNode

instance (WrappingClassyInstance node (IsJoinNode' tok meta) (Join tok meta) (HasReteNode node tok meta)) => HasReteNode (node ? IsJoinNode' tok meta) tok meta where
  {-# INLINE reteNode #-}
  reteNode = promoteAffineToLens reteNode

instance (WrappingClassyInstance node (IsProductionNode' tok meta) (Production tok meta) (HasReteNode node tok meta)) => HasReteNode (node ? IsProductionNode' tok meta) tok meta where
  {-# INLINE reteNode #-}
  reteNode = promoteAffineToLens reteNode

instance (WrappingClassyInstance node (IsNegativeNode' tok meta) (Negative tok meta) (HasReteNode node tok meta)) => HasReteNode (node ? IsNegativeNode' tok meta) tok meta where
  {-# INLINE reteNode #-}
  reteNode = promoteAffineToLens reteNode

instance (WrappingClassyInstance node (IsNCCNode' tok meta) (NCC tok meta) (HasReteNode node tok meta)) => HasReteNode (node ? IsNCCNode' tok meta) tok meta where
  {-# INLINE reteNode #-}
  reteNode = promoteAffineToLens reteNode

instance (WrappingClassyInstance node (IsNCCPartnerNode' tok meta) (NCCPartner tok meta) (HasReteNode node tok meta)) => HasReteNode (node ? IsNCCPartnerNode' tok meta) tok meta where
  {-# INLINE reteNode #-}
  reteNode = promoteAffineToLens reteNode

instance HasMemoryBlock ((ReteNode tok metadata) ? IsAlphaNode' tok metadata) tok metadata where
  {-# INLINE memoryBlock #-}
  memoryBlock = promoteAffineToLens memory

instance HasMemoryBlock ((ReteNode tok metadata) ? IsProductionNode' tok metadata) tok metadata where
  {-# INLINE memoryBlock #-}
  memoryBlock = promoteAffineToLens memory

instance HasMemoryBlock ((ReteNode tok metadata) ? IsMemoryNode' tok metadata) tok metadata where
  {-# INLINE memoryBlock #-}
  memoryBlock = promoteAffineToLens memory

instance HasMemoryBlock ((ReteNode tok metadata) ? IsNegativeNode' tok metadata) tok metadata where
  {-# INLINE memoryBlock #-}
  memoryBlock = promoteAffineToLens memory


instance HasMemoryBlock ((ReteNode tok metadata) ? IsNCCNode' tok metadata) tok metadata where
  {-# INLINE memoryBlock #-}
  memoryBlock = promoteAffineToLens memory
  
instance HasMemoryBlock ((ReteNode tok metadata) ? IsNCCPartnerNode' tok metadata) tok metadata where
  {-# INLINE memoryBlock #-}
  memoryBlock = pendingResultsForNCCNode
{-# INLINE memoryNode #-}
memoryNode :: Lens' ((ReteNode tok metadata) ? IsJoinNode' tok metadata) (TVar ((ReteNode tok metadata) ? IsMemoryNode' tok metadata))
memoryNode = promoteAffineToLens memoryNode'

{-# INLINE nccPartnerNode #-}
nccPartnerNode :: Lens' ((ReteNode tok metadata) ? IsNCCNode' tok metadata)  ((ReteNode tok metadata) ? IsNCCPartnerNode' tok metadata)
nccPartnerNode = promoteAffineToLens partnerNode'

{-# INLINE actualNCCNode #-}
actualNCCNode ::  Lens'  ((ReteNode tok metadata) ? IsNCCPartnerNode' tok metadata) (TVar ((ReteNode tok metadata) ? IsNCCNode' tok metadata) )
actualNCCNode = promoteAffineToLens actualNCCNode'

{-# INLINE pendingResultsForNCCNode #-}
pendingResultsForNCCNode ::  Lens'  ((ReteNode tok metadata) ? IsNCCPartnerNode' tok metadata) (MemoryBlock tok metadata )
pendingResultsForNCCNode = promoteAffineToLens pendingResultsForNCCNode'


{-# INLINE constantTestHashKey #-}
constantTestHashKey :: Lens' ((ReteNode tok metadata) ? IsAlphaNode' tok metadata) MemoryConstantTestHashKey
constantTestHashKey = promoteAffineToLens constantTestHashKey'

-- class (forall tok metadata pred subtype. ProveCorrectSubType @(NodeTypes tok metadata) (ReteNode tok metadata) pred subtype, HasJoinConditions tok metadata subtype) => HasJoinConditions' tok metadata subtype where

-- {-# INLINE individualJoinConditions #-}
-- individualJoinConditions :: IndexedFold NodeIdentifier ((ReteNode tok metadata) ? IsCorrectSubType subtype) JoinCondition
-- individualJoinConditions = joinConditions @tok @metadata @_ <. folded

{-# INLINE joinConditions #-}
joinConditions :: (HasReteNode (ReteNode tok metadata ? pred) tok metadata, ProveCorrectSubType (ReteNode tok metadata) pred subtype, HasJoinConditions tok metadata pred) => IndexedTraversal' NodeIdentifier ((ReteNode tok metadata) ? pred) (HashSet JoinCondition)
joinConditions = (joinConditionsByInitiatingNode) . itraversed


matchVariableMapping :: Lens' ((ReteNode tok metadata) ? IsProductionNode' tok metadata) (Substitution Variable)
matchVariableMapping = promoteAffineToLens matchVariableMapping'

-- instance HasJoinConditions' tok metadata (IsNegativeNode' tok metadata) where
--  {-# INLINE joinConditions #-}
--  joinConditions = (promoteAffineToLens joinConditionsByInitiatingNode) . itraversed

{-# INLINE matchPattern #-}
matchPattern :: Lens' ((ReteNode tok metadata) ? IsAlphaNode' tok metadata) (WMEPattern)
matchPattern = promoteAffineToLens matchPattern'

-- addChildLinkToParent :: ReteNode a -> ReteNode a -> STM ()
-- addChildLinkToParent parent child = runSLens @OpSTM @RealWorld (asLens (parent ^. commonNodeData . childNodes')) (stateModify (insertNode child))
{-# INLINE getReferenceCount #-}
getReferenceCount ::  (Member (Transactional) r) => Action (Sem r) ((ReteNode tok a) ? IsAlphaNode' tok a) Natural
getReferenceCount = act (\node -> readT (node ^. referenceCount))

{-# INLINE betaMemoryNode #-}
betaMemoryNode ::  ( Member (Transactional)  r) => Action (Sem r) ((ReteNode tok a) ? IsJoinNode' tok a) ((ReteNode tok a) ? IsMemoryNode' tok a)
betaMemoryNode = act (\node -> readT (node ^. memoryNode))

{-# INLINE referenceCount #-}
referenceCount :: Lens' ((ReteNode tok metadata) ? IsAlphaNode' tok metadata) (TVar Natural)
referenceCount = promoteAffineToLens referenceCount'


-- {-# INLINE addPatternMappingToAlphaNode #-}
-- addPatternMappingToAlphaNode :: (Members '[Log, OpenTelemetryContext, OpenTelemetrySpan, Transactional, Assert] r) => (ReteNode tok metadata) ? IsAlphaNode' tok metadata -> WMEPattern -> MemoryConstantTestHashKey -> Sem r (Substitution Variable)
-- addPatternMappingToAlphaNode node@(The node') pat key = do
--   let patKey = wmePatternToConstantTestHashKey pat
--   case patKey of
--     Nothing -> assertM False "WME pattern has no constant test hash key!" pat ()
--     Just patKey' -> do
--                 assertM (key == patKey') "Wrong constant test pattern key given!" (pat,patKey',key) ()
--                 assertM (node^.constantTestHashKey == key) "Different shaped pattern!" (node,pat) ()
--   let mapping = HM
  
  

{-# INLINE rightAmem #-}
rightAmem :: (HasReteNode (ReteNode tok metadata ? pred) tok metadata, ProveCorrectSubType (ReteNode tok metadata) pred subtype, HasJoinConditions tok metadata pred, Member (Transactional) r) => Action (Sem r) ((ReteNode tok metadata) ? pred) ((ReteNode tok metadata) ? IsAlphaNode' tok metadata)
rightAmem = (unsafeSingular rightAmem') . act readT


{-# INLINE allChildNodesCollection #-}
allChildNodesCollection :: forall tok a. Lens' ((ReteNode tok a) ? IsMemoryNode' tok a) (TVar (RelativesCollection tok a))
allChildNodesCollection = promoteAffineToLens allChildNodes'


nodeSymbol :: ReteNode tok a -> Doc ann
nodeSymbol AlphaNode' {} = "α"
nodeSymbol JoinNode' {} = "⋈"
nodeSymbol MemoryNode' {} = "⧦"
nodeSymbol NegativeNode' {} = "¬"
nodeSymbol ProductionNode'{} = "𝛠"
nodeSymbol NegatedConjugationNode' {} = "⊼"
nodeSymbol NegatedConjugationPartnerNode' {} = "⊼⋇"

nodeSymbolPlain :: ReteNode tok a -> Doc ann
nodeSymbolPlain AlphaNode' {} = "a"
nodeSymbolPlain JoinNode' {} = "j"
nodeSymbolPlain MemoryNode' {} = "m"
nodeSymbolPlain NegativeNode' {} = "n"
nodeSymbolPlain ProductionNode'{} = "p"
nodeSymbolPlain NegatedConjugationNode' {} = "nj"
nodeSymbolPlain NegatedConjugationPartnerNode' {} = "njp"                                               
nodeSymbolWithID :: ReteNode tok a -> CoreDoc
nodeSymbolWithID node = annotate (IDAnnotation $  node^.identifier) (nodeSymbol node)
nodeSymbolWithIDPlain :: ReteNode tok a -> CoreDoc
nodeSymbolWithIDPlain node = (nodeSymbolPlain node) <> "(" <> (render $ node^.identifier)<> ")"
instance Renderable (CommonNodeData tok a) where
  render cnd@(CommonNodeData' {..}) = vsep [ "ID:" <+> render _nodeIdentifier,
                                       "Level:" <+> render _level,
                                       "Arity:" <+> render _parentArity,
                                       "Parent nodes:" <+> hang 2 (UNSAFE.unsafePerformIO $ doTransaction ( vsep <$> (cnd^!!parentNodes.folded.ignoringLevel.to nodeSymbolWithIDPlain ))),
                                       "Child nodes:" <+> hang 2 (UNSAFE.unsafePerformIO $ doTransaction ( vsep <$> (cnd^!!childNodes.folded.ignoringLevel.to nodeSymbolWithIDPlain )))
                                                                                                          ]

instance Show (CommonNodeData tok a) where
  show = show . render

                   


                          
instance (Renderable tok, Renderable a) => Renderable (ReteNode tok a) where
  render node@AlphaNode' {..} = namedBracket (nodeSymbol node) ("Matching" <+> render _matchPattern' <+> render _memory <+> render _cNodeData)
  render node@JoinNode' {..} =
    namedBracket
      (nodeSymbol node)
      ( "Conditions:"
          <+> render
            ( _joinConditionsByInitiatingNode ^.. folded . folded
            )
          <+> render _cNodeData
      )
  render node@MemoryNode' {..} = namedBracket (nodeSymbol node) ("Memory contents:" <+> render _memory <+> hang 2 (render _cNodeData))
  render node@NegativeNode' {..} = namedBracket (nodeSymbol node) ("Memory contents:" <+> render _memory <+> hang 2 (render _cNodeData))
  render node@ProductionNode' {..} = namedBracket (nodeSymbol node) ("Associated rule:" <+> render _associatedRule <+> "Memory contents:" <+> render _memory <+> hang 2 (render _cNodeData) <+> "Variable mapping:" <+> render _matchVariableMapping')
  render node@NegatedConjugationNode' {..} = namedBracket (nodeSymbol node) ("Memory contents:" <+> render _memory <+> "Partner:" <+> render ((UNSAFE.unsafePerformIO $ doTransaction (pure _partnerNode')) ^. promoteAffineToLens identifier) <+> hang 2 (render _cNodeData))
  render node@NegatedConjugationPartnerNode' {..} = namedBracket (nodeSymbol node) ("NCC node:" <+> render ((UNSAFE.unsafePerformIO $ doTransaction (readT _actualNCCNode')) ^. promoteAffineToLens identifier) <+> hang 2 (render _cNodeData))

instance (Renderable tok, Renderable a) => Show (ReteNode tok a) where
  show = show . render

{-# INLINE allChildNodes #-}
allChildNodes :: (Member (Transactional ) r) => MonadicFold (Sem r) ((ReteNode tok a) ? IsMemoryNode' tok a) ((ReteNode tok a))
allChildNodes = (act \node -> readT (node ^. allChildNodesCollection)) . folded . to payload

instance HasCommonNodeData (ReteNode tok a) tok a where
  {-# INLINE commonNodeData #-}
  commonNodeData = cNodeData

instance HasIdentifier (ReteNode tok a) where
  {-# INLINE identifier #-}
  identifier = commonNodeData . identifier

deriving via ByIdentifier (ReteNode tok a) instance Eq (ReteNode tok a)

instance Ord (ReteNode tok a) where
  {-# INLINE (<=) #-}
  a <= b = a ^. commonNodeData <= b ^. commonNodeData

deriving via ByIdentifier (ReteNode tok a) instance Hashable (ReteNode tok a)

data ReteNodeOrderingInvariantException tok a = ReteNodeOrderingInvariantException NodeIdentifier (RelativesCollection tok a) (RelativesCollection tok a) deriving (Show)

instance (Typeable tok, Typeable a, Renderable tok, Renderable a) => Exception (ReteNodeOrderingInvariantException tok a)

{-# INLINEABLE allAncestors #-}
{-# SCC allAncestors #-}
allAncestors :: ReteNode tok a -> STMType (HashSet (ReteNode tok a))
allAncestors node = allAncestors' node HS.empty
  where
    allAncestors' x currentAncestors = do
      parents' <- x ^!! parentNodes . folded . to payload

      let newParents = HS.difference (HS.fromList parents') currentAncestors
          newAncestors = HS.union currentAncestors newParents
      res <- forM (HS.toList newParents) \y -> do
        allAncestors' y newAncestors
      pure (HS.unions res)

{-# INLINE hasEmptyMemory #-}
{-# SCC hasEmptyMemory #-}
hasEmptyMemory :: Member (Transactional) r => ReteNode tok a -> Sem r Bool
hasEmptyMemory node = do    
  let tokensSet = node ^? memory . tokens
  case tokensSet of
    Nothing -> pure False
    Just tokens' -> isNullSet tokens'


class WMEInAlphaNodes tok metadata wme where
  inhabitedAlphaNodes :: Lens' wme (STMS.Set ((ReteNode tok metadata) ? IsAlphaNode' tok metadata))

instance (WMEInAlphaNodes tok metadata metadata) => WMEInAlphaNodes tok metadata (WME' metadata) where
  {-# INLINE inhabitedAlphaNodes #-}
  inhabitedAlphaNodes = metadata . inhabitedAlphaNodes


{-# INLINE _UNSAFEchildNodes #-}
_UNSAFEchildNodes :: Lens' (ReteNode tok a) (TVar (RelativesCollection tok a))
_UNSAFEchildNodes = childNodes'
