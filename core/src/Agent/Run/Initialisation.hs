module Agent.Run.Initialisation where
import Cosmolude
import Data.Either
import Agent.AgentData as AAD
import DiPolysemy
import Polysemy.Error
import Agent.Run.LowLevelInfrastructure
import Core.CoreTypes
import qualified OpenTelemetry.Context as OTC
--import Core.Identifiers.IdentifierGenerator
getNewAgentData :: (CreateBlankWMEMetadata metadata, HasWMESupport metadata, HasCreatingState metadata, Show metadata, WMEIsState metadata, WMETruthValue (WME' metadata), HasMaybeWMPreference metadata ObjectIdentifier, HasExtraObjectIdentifiers metadata) => Maybe OTC.Context -> IO (Either AgentInitialisationError (AgentData metadata proof))
getNewAgentData ctx = do
  tracer <- cosmoThoughtCoreTracer
  runLowLevelEffects ctx tracer .  runDiNoop . interpretTransaction . runError $ newAgentData tracer 
  
resetAgentData :: (CreateBlankWMEMetadata metadata, HasWMESupport metadata, HasCreatingState metadata, Show metadata, WMEIsState metadata, WMETruthValue (WME' metadata), HasMaybeWMPreference metadata ObjectIdentifier, HasExtraObjectIdentifiers metadata) => Maybe OTC.Context -> AgentData metadata proof -> IO (Either AgentInitialisationError ())
resetAgentData ctx agentData = do
  tracer <- cosmoThoughtCoreTracer
  runLowLevelEffects ctx tracer . runDiNoop .   runError$ AAD.resetAgentData agentData  
