
module Agent.AgentData (AgentData, newAgentData, resetAgentData, AgentInitialisationError(..), LowLevelInfrastructureEffects, InitialisationEffects)where
import Cosmolude
import Polysemy.Embed
import Polysemy.Internal
import Core.CoreTypes
import Data.UUID.V4
import System.Mem.Weak
import Polysemy.Error
import DiPolysemy
import qualified OpenTelemetry.Trace.Core as OT
import PSCM.ProblemSubstates
import qualified Control.Exception as Exception
import Rete.Environment
import Rete.ReteTypes (ProductionRuleActivity, HasProductionRuleActivity(productionRuleActivity), newProductionRuleActivity, resetProductionRuleActivity)
import Core.Rule.Database
import Core.Identifiers.IdentifierGenerator

import Core.WorkingMemory
import PSCM.Errors
import PSCM.States
import Core.Truth.Model
import Polysemy.Reader
import Core.Configuration

import OpenTelemetry.Trace.Core    
import qualified Prelude (error)    

data AgentInitialisationTokens metadata = AgentInitialisationTokens {
      _topLevelStateInitialisedToken :: TVar (TopLevelStateInitialised metadata),
      _workingMemoryInitialisedToken ::  TVar WorkingMemoryInitialised
      }
                               
newAgentInitialisationTokens :: STM (AgentInitialisationTokens metadata)
newAgentInitialisationTokens = AgentInitialisationTokens
                               <$> newTVar (Prelude.error "Uninitialised top state! Call resetEnv.")
                               <*> newTVar (Prelude.error "Uninitialised working memory! Call resetEnv.")
makeClassy ''AgentInitialisationTokens


instance HasWorkingMemoryInitialisedToken (AgentInitialisationTokens metadata) where
    {-# INLINE proveWorkingMemoryIsInitialised #-}
    proveWorkingMemoryIsInitialised = act (\toks -> readTVar (toks^.workingMemoryInitialisedToken))

instance HasTopLevelStateInitialised (AgentInitialisationTokens metadata) metadata where
    {-# INLINE topLevelStateInitialised #-}
    topLevelStateInitialised = act (\toks -> readTVar (toks^.topLevelStateInitialisedToken))



                                      
data AgentData metadata proof = AgentData
               {
                  _agentIdentifier :: Identifier,
                  _tracer' :: Tracer,
                  _configurationData' :: ConfigurationData,
                  _idGenState :: IdentifierGenerationState,
                  _workingMemoryData' :: WorkingMemoryData metadata,
                  _ruleDatabase :: RuleDB,
                  _stateMap' :: StateMap metadata,
                  _pendingActivity' :: PendingActivity metadata proof,
                  _truthModel' :: TruthModel,
                  _reteEnvironment' :: ReteEnvironment metadata,
                  _productionRuleActivity' :: ProductionRuleActivity metadata,
                  _stateManagementState' :: StateManagementState metadata,
                  _agentInitialisationTokens' :: AgentInitialisationTokens metadata,
                  _testOutcome' :: TVar TestOutcome
               }
instance Show (AgentData metadata proof) where
    show _ = "Agent data"
makeClassy ''AgentData

instance HasIdentifier (AgentData metadata proof) where
    {-# INLINE identifier #-}
    identifier = agentIdentifier
           
instance HasTracer (AgentData metadata proof) where
    {-# INLINE tracerL #-}
    tracerL = tracer'

instance HasConfigurationData (AgentData metadata proof) where
    {-# INLINE configurationData #-}
    configurationData = configurationData'
instance HasLogConfigurationData (AgentData metadata proof) where
    {-# INLINE logConfigurationData #-}
    logConfigurationData = configurationData . logConfigurationData
instance HasIdentifierGenerationState (AgentData metadata proof) where
    {-# INLINE identifierGenerationState #-}
    identifierGenerationState = idGenState
           
instance HasWorkingMemoryData (AgentData metadata proof) metadata where
    {-# INLINE workingMemoryData #-}
    workingMemoryData = workingMemoryData'

instance HasRuleDB (AgentData metadata proof) where
    {-# INLINE ruleDB #-}
    ruleDB = ruleDatabase

instance HasStateMap (AgentData metadata proof) metadata where
    {-# INLINE stateMap #-}
    stateMap = stateMap'

instance HasPendingActivity (AgentData metadata proof) metadata proof where
    {-# INLINE pendingActivity #-}
    pendingActivity = pendingActivity'

instance HasTruthModel (AgentData metadata proof) where
    {-# INLINE truthModel #-}
    truthModel = truthModel'

instance HasReteEnvironment (AgentData metadata proof) metadata where
    {-# INLINE reteEnvironment #-}
    reteEnvironment = reteEnvironment'

instance HasProductionRuleActivity (AgentData metadata proof) metadata  where
    {-# INLINE productionRuleActivity #-}
    productionRuleActivity = productionRuleActivity'

instance HasStateManagementState (AgentData metadata proof) metadata where
    {-# INLINE stateManagementState #-}
    stateManagementState = stateManagementState'
                      
instance HasAgentInitialisationTokens (AgentData metadata proof) metadata  where
    {-# INLINE agentInitialisationTokens #-}
    agentInitialisationTokens = agentInitialisationTokens'

instance HasWorkingMemoryInitialisedToken (AgentData metadata proof) where
    {-# INLINE proveWorkingMemoryIsInitialised #-}
    proveWorkingMemoryIsInitialised = agentInitialisationTokens . proveWorkingMemoryIsInitialised
instance HasTopLevelStateInitialised (AgentData metadata proof) metadata where
    {-# INLINE topLevelStateInitialised #-}
    topLevelStateInitialised = agentInitialisationTokens . topLevelStateInitialised
instance WithTestOutcome (AgentData metadata proof) where
    testOutcome = testOutcome'

data AgentInitialisationError = AgentInitialisationError (Exception.SomeException) deriving (Show, Generic, Exception.Exception)


type LowLevelInfrastructureEffects = '[Transactional, OpenTelemetryContext, OpenTelemetrySpan,Input Tracer, Resource]
type InitialisationEffects = Append '[Error AgentInitialisationError, Log] LowLevelInfrastructureEffects
    


                              
                                      
{-# INLINE newAgentData #-}
newAgentData :: (CreateBlankWMEMetadata metadata, HasWMESupport metadata, HasCreatingState metadata,  Show metadata, WMEIsState metadata, WMETruthValue (WME' metadata), HasMaybeWMPreference metadata ObjectIdentifier, HasExtraObjectIdentifiers metadata,Members (ScopedTransaction : Embed IO : InitialisationEffects) r) => Tracer -> Sem r (AgentData metadata proof)
newAgentData tracer  = transaction  . raise $ do
  ident <- UniqueIdentifier <$> embed nextRandom                        
  configData <- embedSTM newConfigurationData               
  idgenstate <- embedSTM newIDGenState  
  wmd <- embedSTM blankWorkingMemory
  ruledb <- embedSTM newRuleDatabase
  sm <- newStateMap
  pending <- embedSTM newPendingActivity
  tm <- embedSTM newTruthModel
  env <- embedSTM newReteEnvironment
  activity <- embedSTM newProductionRuleActivity
  sms <- embedSTM newStateManagementState
  tokens <- embedSTM newAgentInitialisationTokens
  testoutcome <- newT NoTestCompleted
  let agent = AgentData ident tracer configData idgenstate wmd ruledb sm pending tm env activity sms tokens testoutcome
  initialiseAgentData agent
  embed $ addFinalizer agent $ do
      provider <- OT.getGlobalTracerProvider
      _ <- OT.forceFlushTracerProvider provider Nothing
      pure ()
  pure agent
{-# INLINE initialiseAgentData #-}  
initialiseAgentData :: forall metadata proof r.  ( CreateBlankWMEMetadata metadata, HasWMESupport metadata, HasCreatingState metadata, Show metadata, HasMaybeWMPreference metadata ObjectIdentifier, HasExtraObjectIdentifiers metadata, WMEIsState metadata, WMETruthValue (WME' metadata), Members (InitialisationEffects) r) => (AgentData metadata proof) -> Sem r ()
initialiseAgentData agent = do
  resetRuleDatabase (agent^.ruleDB)
  resetStateMap (agent^.stateMap)
  wmInitialised <- resetWorkingMemory (agent^.workingMemoryData)
  writeT wmInitialised (agent^.agentInitialisationTokens.workingMemoryInitialisedToken)
  topInitialised <- mapError (AgentInitialisationError . Exception.SomeException) . runReader agent .  runIDGenWith agent .  runRuleDatabase agent .  processWorkingMemory . runDiNoop   $ initialiseStateWMEs  wmInitialised
  writeT topInitialised (agent^.agentInitialisationTokens.topLevelStateInitialisedToken)

     
  
resetAgentData :: (CreateBlankWMEMetadata metadata, HasWMESupport metadata, HasCreatingState metadata, Show metadata, HasMaybeWMPreference metadata ObjectIdentifier, HasExtraObjectIdentifiers metadata,WMEIsState metadata, WMETruthValue (WME' metadata), Members ( InitialisationEffects) r) => (AgentData metadata proof) -> Sem r ()
resetAgentData agent@(AgentData _ident _tracer configData idgenstate _wmd _ruledb _sm pending tm env activity sms _tokens _testoutcome) =   do
  embedSTM $ do
           resetConfigurationData configData
           resetIDGenState idgenstate
           resetPendingActivity pending
           resetTruthModel tm
           resetReteEnvironment env
           resetProductionRuleActivity activity
           resetStateManagementState sms
  initialiseAgentData agent                    
