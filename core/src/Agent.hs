module Agent  where
import DiPolysemy
import qualified Di
import qualified Di.Core
import Data.Either
import System.Directory
import System.FilePath    
import Rete.Environment    
import Core.Rule.Database
import Polysemy.Internal
import Polysemy.Final
import Polysemy.Reader
import Rete
import Rete.ReteTypes
import qualified OpenTelemetry.Trace.Core as OT    
import qualified PSCM as PSCM
import qualified Control.Exception as Exception
import qualified Command as Command
import qualified Data.Text as T

import Agent.Run.LowLevelInfrastructure
import Agent.Run.Initialisation
--import PSCM.ProblemSubstates

import Agent.Run.Memory
import GHC.Stack

import Core.Identifiers.IdentifierGenerator
import Core.Utils.Logging 

import Cosmolude

import Core.WME
import Core.Configuration
import Core.WorkingMemory
import Polysemy.Error
import OpenTelemetry.Trace.Core  ( tracerL)
import Agent.AgentData (AgentData)
import qualified OpenTelemetry.Context as OTC


type Agent = AgentData WMEMetadata (WMEToken ? IsProductionToken)

    

type PostLogInfrastructure agent r = (HasCallStack, Members PostLogInfrastructureEffects r, HasTracer agent, HasConfigurationData agent)
type PostLogInfrastructureEffects = '[KeyedState LogConfiguration,
     KeyedState Configuration,Assert]
    
type PostLogInfrastructureEffectsIO = Append PostLogInfrastructureEffects LowLevelEffects

{-# INLINE runPostLogInfrastructureIO #-}    
runPostLogInfrastructureIO :: (HasCallStack, r ~ PostLogInfrastructureEffectsIO, PostLogInfrastructure agent r) => Maybe OTC.Context -> agent -> Sem r x -> IO x
runPostLogInfrastructureIO ctx agent = do
    runFinal
      . embedToFinal
      . runLowLevelInfrastructureEffects ctx agent
      . assertsToIOException (agent^.tracerL)
      . runKeyedStateVarsIO (configurationToVariables agent)
      . embedLogConfiguration 
    
type Infrastructure agent r = PostLogInfrastructure agent (IDGen : Log : r)

-- 
type RLR agent = '[Error RuleNotInRuleDatabase, Log, Reader agent]

type RLRd agent = Append (RLR agent) PostLogInfrastructureEffectsIO
    
type MinimalParserEffects' agent metadata  = Append (BasicMemoryEffects metadata) (RLRd agent)
type MinimalParserEffects agent = MinimalParserEffects' agent WMEMetadata

type MinimalParser agent r = (HasLogConfigurationData agent,
                                                      HasIdentifier agent,
                              WorkingMemoryAgentConstraints agent WMEMetadata,
                              HasTracer agent,
                              HasRuleDB agent,
                              HasConfigurationData agent,
                              Members (MinimalParserEffects agent) r)

handleRuleNotInDBError :: (HasCallStack, Members '[Final IO] r) => InterpreterFor (Error RuleNotInRuleDatabase) r
handleRuleNotInDBError sem = do
  result <- errorToIOFinal sem
  case result of
    Left exc -> Exception.throw exc
    Right a -> pure a
                
    
{-# INLINE runMinimalParser' #-}        
runMinimalParser' ::  (HasCallStack, r ~ (MinimalParserEffects agent), MinimalParser agent r)  => Maybe OTC.Context -> Bool -> agent -> Sem r a -> IO a
runMinimalParser' ctx doLog agent sem =  Di.new $ \di -> do
                 runPostLogInfrastructureIO ctx agent
                 . runReader agent
                 . (if doLog then runDi (Di.Core.filter (filterByLogSettings agent) di) else runDiNoop)
                 . handleRuleNotInDBError 
                 . runBasicMemory agent $ sem

                   
                     

type PrePSCMEffects agent = Append (Error (PSCM.SubstateIdentificationException WMEMetadata (WMEToken ? IsProductionToken)) : ReteEffects) (MinimalParserEffects agent)
    
type PrePSCM agent r = (HasCallStack, Members (PrePSCMEffects agent) r, ReteConstraints agent, MinimalParser agent r)
    
{-# INLINE runPrePSCM' #-}
runPrePSCM' :: (HasCallStack,r ~ (PrePSCMEffects agent), PrePSCM agent r)  => Maybe OTC.Context -> Bool -> agent -> Sem r a -> IO  a
runPrePSCM' ctx doLog agent = (runMinimalParser' ctx doLog agent)
                    . runReteEffects . handleSubstateIdentificationExceptionAsIOException

handleSubstateIdentificationExceptionAsIOException ::
  ( HasReteEnvironment agent metadata,
    HasTracer agent,
    HasWorkingMemoryData agent metadata,
    PSCM.MetadataAndProofConstraints metadata proof,
    Members
      '[ 
         GlobalContextID,
         WorkingMemory' metadata,
         RuleDatabase,
         OpenTelemetrySpan,
         OpenTelemetryContext,
         Resource,
         Log,
         IDGen,
         Transactional,
         Reader agent,
         Embed IO,
         Final IO
       ]
      r
  ) =>
  InterpreterFor (Error (PSCM.SubstateIdentificationException metadata proof)) r
handleSubstateIdentificationExceptionAsIOException sem = do
  res <- errorToIOFinal $ catch sem \e@(PSCM.SubstateIdentificationException ty match) -> do
--    agent <- ask
    setSpanStatus (OT.Error (T.pack $ show ty))
    ident <- globalContext
    cacheDir <- embed $ getXdgDirectory XdgCache ("cosmothought" </> "crash" </> (show ident))
    embed $ createDirectoryIfMissing True cacheDir

    let wmFilePath = cacheDir </> "working-memory.svg"
        reteGraphForRuleFilePath = cacheDir </> "rete-graph-for-rule.svg"
        reteGraphFilePath = cacheDir </> "whole-rete-graph.svg"
        size = 4096
    Command.emitWorkingMemoryGraph size wmFilePath
    Command.emitReteNodeDiagramForRule (match ^. rule) size reteGraphForRuleFilePath
    Command.emitReteNodeDiagramForAllRules  size reteGraphFilePath
    recordException
      e
      [ ("crash.id", fromString . show $ ident),
        ("crash.dump.diagrams.working_memory.file.path", fromString wmFilePath),
        ("crash.dump.diagrams.rete_nodes_for_rule.file.path", fromString reteGraphForRuleFilePath),
        ("crash.dump.diagrams.rete_nodes.path", fromString reteGraphFilePath)
      ]
    withCurrentSpan (\s -> endSpan s Nothing)

    throw e
  case res of
    Right ok -> pure ok
    Left err -> embed $ do
      provider <- OT.getGlobalTracerProvider
      !_ <- OT.forceFlushTracerProvider provider Nothing
      Exception.throwIO err

                        


type ProductionSystemEffects agent r = PSCM.ProductionSystemEffects' agent WMEMetadata (ReteNode WMEMetadata) (WMEToken ? IsProductionToken) r                      
type PSCMEffectStack agent = Append (PSCM.PSCMEffects WMEMetadata (WMEToken ? IsProductionToken)) (PrePSCMEffects agent)
                      
{-# INLINE runPSCM' #-}
runPSCM' :: (HasCallStack,r ~ PSCMEffectStack agent, PrePSCM agent r, PSCM.HasTopLevelStateInitialised agent WMEMetadata, PSCM.PSCMConstraints agent WMEMetadata (WMEToken ? IsProductionToken)) => Maybe OTC.Context -> Bool -> agent -> Sem r a -> IO  a
runPSCM' ctx doLog agent =
    runPrePSCM' ctx doLog agent
    . PSCM.runPSCM


withFreshAgent ::  HasCallStack => Maybe OTC.Context -> Bool -> (Agent -> Sem (PSCMEffectStack Agent) x) -> IO x
withFreshAgent ctx doLog f = do
  Right agent <- getNewAgentData  ctx
  x <- runPSCM' ctx doLog agent (f agent)
  provider <- OT.getGlobalTracerProvider
  !_ <- OT.forceFlushTracerProvider provider Nothing
  pure x
withFreshAgentIO :: HasCallStack => Maybe OTC.Context -> (Agent -> IO x) -> IO x
withFreshAgentIO ctx f = do
  Right agent <- getNewAgentData ctx
  x <- f agent
  provider <- OT.getGlobalTracerProvider
  !_ <- OT.forceFlushTracerProvider provider Nothing
  pure x

-- 1. initializeGlobalTracer
-- 2. tracer <- 
