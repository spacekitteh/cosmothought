module ForTesting where

import Core.WorkingMemory (HasWorkingMemoryData)
import Rete.Environment
import Core.WME    
import Control.Monad.IO.Class    
import OpenTelemetry.Context  as OT
import Command
import Control.Monad.Trans.Resource    
import Core.Error
import Agent
import Agent.Run.Initialisation
import Agent.Run.LowLevelInfrastructure
import Agent.Run.Memory
import Core.Configuration
--import UX.CommandProcessor
import UX.CosmoScript
import Cosmolude
import Data.Either
import Data.Text as T
import Data.Text.IO
import DiPolysemy
import Error.Diagnose

import UX.CosmoScript.Syntax.Parser
import Polysemy.Reader
import System.FilePath
import qualified System.IO.Unsafe as UNSAFE (unsafePerformIO)
import qualified Prelude (undefined)

import OpenTelemetry.Trace as OT hiding (inSpan)
type MinimalCommandEffects agent = PSCMEffectStack agent

createTestSpan :: (MonadIO m, MonadResource m) => Agent ->  OT.Context -> Text -> OT.SpanArguments -> m OT.Span
createTestSpan agent ctx name args =  do
  (_,span) <- allocate (liftIO $ OT.createSpan (agent^.tracerL) ctx name args) (\span -> liftIO $ OT.endSpan span Nothing)
  pure span



    
parseMinimal' :: HasCallStack => Agent -> Parser (MinimalParserEffects Agent) a -> FilePath -> Text -> IO (Either (ParseErrorBundle DiagnosticMessageType ParsingError) a)
parseMinimal' agent parser name body = do
  tracer <- cosmoThoughtCoreTracer
  parseWithMinimalSem
    tracer
    False
    (configurationToVariables agent)
    ( runReader agent
        . runDiNoop
        . handleRuleNotInDBError
        . runBasicMemory agent
    )
    parser
    name
    body

parseMinimal :: HasCallStack => Agent -> Parser (MinimalParserEffects Agent) a -> FilePath -> Text -> IO (Either (Diagnostic DiagnosticMessageType) a)
parseMinimal agent parser name body = do
  tracer <- cosmoThoughtCoreTracer
  parseWithMinimalSem'
    tracer
    False
    (configurationToVariables agent)
    ( runReader agent
        . runDiNoop
        . handleRuleNotInDBError
        . runBasicMemory agent
    )
    parser
    name
    body

makeParseFailGoldenTestOutput :: HasCallStack => Agent -> Parser (MinimalParserEffects Agent) a -> FilePath -> Text -> IO (Text)
makeParseFailGoldenTestOutput agent parser filename input = do
  Left a <- parseMinimal' agent parser filename input
  pure (diagnosticToPrettyMessage . (\d -> addFile d filename (T.unpack input)) . diagnose @WhenParsing $ a)

makeParseFailGoldenTestOutputFromFile :: HasCallStack => Agent -> Parser (MinimalParserEffects Agent) a -> FilePath -> IO (Text)
makeParseFailGoldenTestOutputFromFile agent parser filename = do
  let filename' = takeFileName filename
  input <- readFile filename
  makeParseFailGoldenTestOutput agent parser filename' input

writeParseFailGoldenTestOutput :: HasCallStack => Agent -> Parser (MinimalParserEffects Agent) a -> FilePath -> IO (CoreDoc)
writeParseFailGoldenTestOutput agent parser filename = do
  let filename' = takeFileName filename
      expected = addExtension filename "expected"

  input <- readFile filename
  t <- makeParseFailGoldenTestOutput agent parser filename' input
  writeFile expected t
  pure (render t)

fromRight' :: Either a b -> b
fromRight' (Left _) = Prelude.undefined
fromRight' (Right b) = b

{-# NOINLINE globalAgent #-}                       
globalAgent :: Agent
globalAgent = fromRight' $ UNSAFE.unsafePerformIO $ getNewAgentData mempty

testCommand :: (r ~ MinimalCommandEffects Agent, HasCallStack) => Text -> IO CommandResults--(ProcessCommandResult r)
testCommand cmd =
  (runPSCM' mempty True globalAgent) $ inSpan "ProcessCommand?" defaultSpanArguments (parseAndExecute "" cmd)--(processCommand' globalAgent cmd)

parseAndExecute :: ( HasTracer agent, HasWorkingMemoryData agent WMEMetadata, HasReteEnvironment agent WMEMetadata, Members (MinimalCommandEffects agent) r) => FilePath -> Text -> Sem r CommandResults
parseAndExecute file cmd = do
  
  parseResults <- Cosmolude.push "parsingCommand" $ runAndDiagnoseParser (parseCosmoScript{- ParsingUnspecified-}) file cmd
  case parseResults of
    Left err -> do
      let laidOut = diagnosticToPrettyCoreDoc err
      pure $ resultingIn # [CommandError (Right laidOut)]
    Right parsed -> processCosmoScriptCommand parsed
                    
runCommand :: (r ~ MinimalCommandEffects Agent, HasCallStack) => Agent -> Text -> IO CommandResults --(ProcessCommandResult r)
runCommand agent cmd = (runPSCM' mempty True agent) $ inSpan "runCommand" defaultSpanArguments (parseAndExecute "" cmd)--(processCosmoScriptCommand cmd) --(processCommand' agent cmd)
