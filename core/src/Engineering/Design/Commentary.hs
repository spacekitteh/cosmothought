{-# LANGUAGE UndecidableInstances #-}

module Engineering.Design.Commentary where

import Data.Data (Data, Typeable)

import Data.UUID.Types
import GHC.Generics (Generic)
import Polysemy
import Prettyprinter
import Text.URI
import Prelude

type Synopsis ann = Doc ann

type Reasoning ann = Doc ann

data Requirement ann where
  RequirementUUID :: UUID -> Requirement ann
  RequirementURI :: URI -> Requirement ann
  ARequirement :: Synopsis ann -> Reasoning ann -> Requirement ann
  deriving (Show, Generic)

deriving instance (Eq (Synopsis ann)) => Eq (Requirement ann)

deriving instance (Ord (Synopsis ann)) => Ord (Requirement ann)

deriving instance (Typeable ann, Data ann, Data (Synopsis ann)) => Data (Requirement ann)

data Commentary ann m a where
  Comment :: Doc ann -> Commentary ann m ()
  Explanation :: Doc ann -> Commentary ann m ()
  Assumption :: Doc ann -> Commentary ann m ()
  Citation :: Doc ann -> Commentary ann m ()
  RequiredBy :: (Traversable t) => Doc ann -> t (Requirement ann) -> Commentary ann m ()

makeSem ''Commentary

{-# INLINE CONLIKE ignoreCommentary #-}
ignoreCommentary :: InterpreterFor (Commentary ann) r
ignoreCommentary = interpret $ \case
  Comment _ -> pure ()
  Explanation _ -> pure ()
  Assumption _ -> pure ()
  Citation _ -> pure ()
  RequiredBy _ _ -> pure ()
