module Engineering.Instrumentation.StatisticsCollection where
-- --import Data.ByteString
import Polysemy
-- import Data.Text
import Prelude
-- --import Numeric.Natural
-- import Polysemy.State.Keyed
-- -- import System.Remote.Monitoring
-- -- import System.Metrics.Counter
-- -- import System.Metrics.Gauge

-- import System.Metrics.Prometheus.Metric.Counter  as Counter hiding (sample) 
-- import System.Metrics.Prometheus.Metric.Gauge as Gauge hiding (sample)
-- import System.Metrics.Prometheus.Concurrent.Registry as Registry
-- import System.Metrics.Prometheus.MetricId
-- import System.Metrics.Prometheus.Http.Scrape
-- import StmContainers.Map as STMM
-- import Control.Concurrent.STM
-- -- data MetricBuilder counter gauge m a where
-- --   NewCounter :: Text -> MetricBuilder counter gauge m counter
-- --   NewGauge :: Text -> MetricBuilder counter gauge m gauge

-- -- makeSem ''MetricBuilder

data StatisticsCollection' counter gauge m a where
  IncrementCounter :: counter -> StatisticsCollection' counter gauge m ()
  AddToCounter :: counter -> Int -> StatisticsCollection' counter gauge m ()
  IncrementGauge :: gauge -> StatisticsCollection' counter gauge m ()
  DecrementGauge :: gauge -> StatisticsCollection' counter gauge m ()
  AddToGauge :: gauge -> Double -> StatisticsCollection' counter gauge m ()
  SubtractFromGauge :: gauge -> Double -> StatisticsCollection' counter gauge m ()
  SetGaugeTo :: gauge -> Double -> StatisticsCollection' counter gauge m ()
makeSem ''StatisticsCollection'


ignoreMetrics :: -- counter -> gauge ->  
  InterpreterFor  (StatisticsCollection' counter gauge) r
ignoreMetrics -- defaultCounter defaultGauge
  = (interpret \case
        IncrementCounter _ -> pure ()
        AddToCounter _ _ -> pure ()
        IncrementGauge _ -> pure ()
        DecrementGauge _ -> pure ()
        AddToGauge _ _ -> pure ()
        SubtractFromGauge _ _ -> pure ()
        SetGaugeTo _ _ -> pure ()
    )
--  -- . (interpret \case
--  --  NewCounter _ -> pure defaultCounter
--  --  NewGauge _ -> pure defaultGauge) 


-- data MetricStorage counter gauge store a where
--   MetricStore :: MetricStorage counter gauge store store
--   Counter :: Text -> MetricStorage counter gauge store counter
--   Gauge :: Text -> MetricStorage counter gauge store gauge




-- {-# INLINE runPrometheusMetricStorage #-}
-- runPrometheusMetricStorage :: (Members '[Embed STM] r) => TVar Registry -> STMM.Map (Name, Labels) Counter -> STMM.Map (Name,Labels) Gauge ->  InterpreterFor (KeyedState (MetricStorage Counter Gauge Registry)) r
-- runPrometheusMetricStorage reg counters gauges = interpret \case
--   GetAt MetricStore -> embed $ readTVar reg
--   PutAt MetricStore r -> embed $ writeTVar reg r
--   GetAt (Counter


-- {- INLINE runPrometheus #-}
-- runPrometheus :: (Members '[KeyedState (MetricStorage Counter Gauge Registry), Embed IO] r) => Int -> [Text] -> InterpreterFor (StatisticsCollection' Counter Gauge) r
-- runPrometheus port path sem  = do
--   registry <- embed $ Registry.new
--   putAt MetricStore registry
--   embed $ (serveMetrics port path (Registry.sample registry) )
--   interpret ( \case
--     IncrementCounter counter -> embed $ Counter.inc counter
--     AddToCounter counter n -> embed $ Counter.add n counter
--     IncrementGauge gauge -> embed $ Gauge.inc gauge
--     DecrementGauge gauge -> embed $ Gauge.dec gauge
--     AddToGauge gauge n -> embed $ Gauge.add n gauge
--     SubtractFromGauge gauge n -> embed $ Gauge.sub n gauge
--     SetGaugeTo gauge n -> embed $ Gauge.set n gauge) sem
  
  
-- -- {-# INLINE runEkgMetricBuilder #-}
-- -- runEkgMetricBuilder :: (Members '[StatisticsCollection Counter Gauge, KeyedState (MetricStorage Counter Gauge Store)] r) => InterpreterFor (MetricBuilder Counter Gauge) r
-- -- runEkgMetricBuilder =

-- -- runEkgMetricStorage :: K

-- -- {-# INLINE runEkgMetrics #-}
-- -- runEkgMetrics ::(Members '[Embed IO, KeyedState (MetricStorage Counter Gauge Server)] r) =>  ByteString -> Int ->  InterpreterFor (StatisticsCollection' Counter Gauge) r
-- -- runEkgMetrics host port sem = do
-- --   server <- embed $ forkServer host port
-- --   putAt MetricStore server
-- --   (flip interpret) sem  \case 
-- --     IncrementCounter counter -> embed $ inc counter
-- --     AddToCounter counter n -> embed $ add counter n
-- --     IncrementGauge gauge -> embed $ inc gauge
-- --     DecrementGauge gauge -> embed $ dec gauge
-- --     AddToGauge gauge n -> embed $ add gauge n
-- --     SubtractFromGauge gauge n -> embed $ sub gauge n
-- --     SetGaugeTo gauge n -> embed $ set gauge n

  
