module Rete where
import Cosmolude
import Rete.ReteEffect

import Core.WME
import Rete.Environment
import Polysemy
import Control.Monad.Fix
import Polysemy.Fixpoint
import Polysemy.Reader
import Rete.ReteBuilder
import Rete.ReteTypes
import Rete.Dynamics.TokenFlow
import Core.Rule.Database

--(HasPendingActivity agent, HasProductionRuleActivity agent WMEMetadata, HasStateMap agent
type ReteConstraints agent =     (HasTracer agent, HasProductionRuleActivity agent WMEMetadata, HasReteEnvironment agent WMEMetadata)
type ReteEffects = '[Rete, ReteBuilder WMEMetadata,  ReteTokenManipulation WMEMetadata, 
                               Fixpoint]
{-# INLINE runReteEffects #-}
runReteEffects :: (
                   ReteConstraints agent,
                   HasCallStack,
                   Members '[Reader agent, OpenTelemetrySpan, OpenTelemetryContext , Resource,  Log, WorkingMemory, Embed IO, RuleDatabase,   Transactional,IDGen, Assert, Final m] r,
                   MonadFix m) =>
                  InterpretersFor ReteEffects r
runReteEffects = do
  fixpointToFinal
      . runReteTokenManipulation --env
      . runReteBuilder
      . runRete
    
