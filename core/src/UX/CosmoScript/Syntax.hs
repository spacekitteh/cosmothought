module UX.CosmoScript.Syntax where
import qualified Df1
import Core.CoreTypes
import Cosmolude
import System.FilePath
import Data.List.NonEmpty
import GHC.Float
import Core.Configuration
import Core.WME    
data Expression -- f a
  = Var Variable
  | AntiVar String
  | Let Variable Expression Expression
  | Lambda Variable Expression
  | Apply Expression Expression
  | Literal Value
  deriving (Eq, Show, Generic, Hashable)

instance Plated Expression where
  {-# INLINEABLE plate #-}
  plate _ e@(Var _) = pure e
  plate _ e@(AntiVar _) = pure e
  plate _ e@(Literal _) = pure e
  plate f (Let var e1 e2) = Let var <$> f e1 <*> f e2
  plate f (Lambda var e) = Lambda var <$> f e
  plate f (Apply e1 e2) = Apply <$> f e1 <*> f e2


data CosmoScriptCommand = CosmoScriptCommand CosmoScriptWithoutHelpCommand
                         | HelpCommand CosmoScriptWithoutHelpCommand
                           deriving (Eq, Show, Generic, Hashable)
data CosmoScriptWithoutHelpCommand
    = Add AddCommand 
    | Remove RemoveCommand
    | Set SetCommand
    | Query QueryCommand
    | Print PrintCommand
    | Show ShowCommand
      deriving (Eq, Show, Generic, Hashable)

data AddCommand 
    = AddRules (NonEmpty Rule)
    | AddWMEs (NonEmpty WME)
      deriving (Eq, Show, Generic, Hashable)

data RemoveCommand
    = RemoveRules (NonEmpty RuleIdentifier)
    | RemoveWMEs (NonEmpty WME)
      deriving (Eq, Show, Generic, Hashable)

data SetCommand
    = SetLogLevel Df1.Level
    | SetLogPath [Df1.Path]
    | SetLogExcludes [Df1.Path]
    | SetLogIncludes [Df1.Path]
    | SetProductionRuleActivityOnRemoval ActivityOnRuleRemoval
      deriving (Eq, Show, Generic, Hashable)

data QueryCommand 
    = Exists ExistsCommand
    | Stats StatsCommand
      deriving (Eq, Show, Generic, Hashable)
data IsAsserting = NonAsserting | Asserting deriving (Eq, Show, Generic, Hashable)               
data ExistsCommand
    = ExistsWMEPattern IsAsserting WMEPattern
      deriving (Eq, Show, Generic, Hashable)
data StatsCommand
    = StatsObjectReferences ObjectIdentifier
      deriving (Eq, Show, Generic, Hashable)
data PrintCommand 
    = PrintRules
    | PrintWMEs
    | PrintReteNodes
      deriving (Eq, Show, Generic, Hashable)

data ShowCommand
    = ShowDiagramWorkingMemory Float FilePath
    | ShowDiagramReteNodesForRule Rule Float FilePath
    | ShowDiagramReteNodes Float FilePath
      deriving (Eq, Show, Generic, Hashable)

   
data CosmoScript = CosmoScriptBasicBlock (NonEmpty CosmoScriptCommand) deriving (Eq, Show, Generic, Hashable)
