module UX.CosmoScript.Syntax.Parser where
import UX.CosmoScript.Syntax
import Control.Monad.Combinators.Expr
import qualified Control.Applicative.Combinators.NonEmpty as NE
import qualified Text.Megaparsec.Char.Lexer as L    
import Cosmolude
import Data.Tuple (snd)    
import Data.Text as T
import Polysemy.Reader
import Core.WME
import Core.CoreTypes    
import Core.Rule.Parser
import Core.Configuration
import Core.Patterns.Parser
import GHC.Float
import System.FilePath
    
binary :: (Members '[IDGen, Transactional] r) => Text -> (a -> a -> a) -> Operator (ParsecT ParsingError Text (Sem r)) a
binary  name f = InfixL  (f <$ keyword name)

prefix, postfix :: (Members '[IDGen, Transactional] r) => Text -> (a -> a) -> Operator (ParsecT ParsingError Text (Sem r))  a
prefix  name f = Prefix  (f <$ keyword name)
postfix name f = Postfix (f <$ keyword name)


operatorTable :: (Members '[IDGen, Transactional] r) => [[Operator (ParsecT ParsingError Text (Sem r)) Expression]]
operatorTable = operatorTable

-- pArithTerm :: Parser r (Expression)
-- pArithTerm = choice
--   [
--     ArithVar <$> lexeme (parseVariable ParsingExpression),
--     Num <$> lexeme parseConstantValue,
--     parseParens parseArithExpr

--   ]

-- parseArithExpr :: Parser r (ArithExpr Variable)
-- parseArithExpr = simplify <$> makeExprParser pArithTerm arithOperatorTable

parseAddCommand :: (HasTracer agent,
                    Members '[ObjectIdentityMap WME, Reader agent, Log, Resource, OpenTelemetryContext, OpenTelemetrySpan, WorkingMemory] r) =>
                    Parser r AddCommand 
parseAddCommand  = try (AddRules <$ keyword "rules" <*> NE.some parseRule)
                              <|> (AddWMEs <$ keyword "wmes" <*> NE.some (snd <$> parseWME))
                              
parseRemoveCommand :: (HasTracer agent,
                    Members '[ObjectIdentityMap WME, Reader agent, Log, Resource, OpenTelemetryContext, OpenTelemetrySpan, WorkingMemory] r) => Parser r RemoveCommand
parseRemoveCommand  = try (RemoveRules <$ keyword "rules" <*> NE.some (RuleIdentifier <$> parseIdentifier))
                                 <|> (RemoveWMEs <$ keyword "wmes" <*> NE.some (snd <$> parseWME))

parseSetCommand :: Parser r SetCommand
parseSetCommand = try (do
                        _ <- keyword "log"
                        do                             
                         try (SetLogLevel <$ keyword "level" <*> parseDf1Level)
                          <|> try (SetLogPath <$ keyword "path" <*> many parseDf1Path)
                          <|> try (SetLogExcludes <$ keyword "excludes" <*> many parseDf1Path)
                          <|> (SetLogIncludes <$ keyword "includes" <*> many parseDf1Path)
                         
                      )
                    <|> (do
                          _ <- keyword "production"
                          _ <- keyword "rule"
                          _ <- keyword "activity"
                          _ <- keyword "on"
                          _ <- keyword "removal"
                          SetProductionRuleActivityOnRemoval <$> parseRuleActivityOnRemoval                          
                        )
                  
                             
                              
parseRuleActivityOnRemoval :: Parser r ActivityOnRuleRemoval
parseRuleActivityOnRemoval = try (AlwaysDoActivity <$ verbatim "always")
                             <|> try (WhenNotOperatorSupported <$ (keyword "when" <* keyword "not" <* keyword "operator" <* verbatim "supported"))
                             <|> (NeverDoActivity <$ verbatim "never")





parseQueryCommand :: (Members '[ObjectIdentityMap (WME' metadata)] r) => Parser r QueryCommand
parseQueryCommand = try (Exists <$ keyword "exists!" <*> parseExistsCommand Asserting)
                    <|> try (Exists <$ keyword "exists?" <*> parseExistsCommand NonAsserting)
                    <|> (Stats <$ keyword "stats" <*> parseStatsCommand)


parseExistsCommand :: (Members '[ObjectIdentityMap (WME' metadata)] r) => IsAsserting -> Parser r ExistsCommand
parseExistsCommand doesItAssert = (ExistsWMEPattern doesItAssert) <$ keyword "wme" <*> parseWMEPattern ParsingExpression

parseStatsCommand :: (Members '[ObjectIdentityMap (WME' metadata)] r) => Parser r StatsCommand
parseStatsCommand = keyword "stats" $> StatsObjectReferences <* keyword "object" <* keyword "references" <*> (parseObjectIdentifierLookup ParsingUnspecified)
                        

parsePrintCommand :: Parser r PrintCommand
parsePrintCommand = try (PrintRules <$ keyword "rules")
                    <|> try (PrintWMEs <$ keyword "wmes")
                    <|> (do
                          _ <- keyword "rete"
                          PrintReteNodes <$ keyword "nodes")


parseImageSize :: Parser r Float
parseImageSize = (lexeme L.float) <?> "Image size as a non-negative floating point number"

parseFilePath :: Parser r FilePath
parseFilePath = T.unpack <$> lexeme stringLiteral
                 
parseShowCommand ::(HasTracer agent,
                    Members '[ObjectIdentityMap WME, Reader agent, Log, Resource, OpenTelemetryContext, OpenTelemetrySpan] r) =>
    Parser r ShowCommand
parseShowCommand = do
                    _ <- keyword "diagram"
                    try ( do
                         _ <- keyword "rete"
                         _ <- keyword "nodes"
                         try (do
                               _ <- keyword "for"
                               _ <- keyword "rule"
                               ShowDiagramReteNodesForRule <$> parseRule <*> parseImageSize <*> parseFilePath
                             )
                              <|> (ShowDiagramReteNodes <$> parseImageSize <*> parseFilePath)
                        )
                      <|> (do
                            _ <- keyword "working"
                            _ <- keyword "memory"
                            ShowDiagramWorkingMemory <$> parseImageSize <*> parseFilePath)
                      
                    
parseCosmoScriptWithoutHelp ::(HasTracer agent,
                    Members '[ObjectIdentityMap WME, Reader agent, Log, Resource, OpenTelemetryContext, OpenTelemetrySpan, WorkingMemory] r) =>  {-WhenParsing ->-} Parser r CosmoScriptWithoutHelpCommand
parseCosmoScriptWithoutHelp  = try (Add <$ keyword "add" <*> parseAddCommand)  --whenParsing)
                               <|> try   (Remove <$ keyword "remove" <*> parseRemoveCommand)-- whenParsing)
                               <|> try (Set <$ keyword "set" <*> parseSetCommand)
                               <|> try (Query <$> parseQueryCommand) 
                               <|> try (Print <$ keyword "print" <*>  parsePrintCommand)
                               <|> ( Show <$ keyword "show" <*>  parseShowCommand)

parseCosmoScript ::                                    (HasTracer agent,
                    Members '[ObjectIdentityMap WME, Reader agent, Log, Resource, OpenTelemetryContext, OpenTelemetrySpan, WorkingMemory] r) =>  {-WhenParsing ->-} Parser r CosmoScriptCommand
parseCosmoScript {-whenParsing-} = (try (CosmoScriptCommand <$> parseCosmoScriptWithoutHelp {-whenParsing-})
                               <|> (keyword "help" $> HelpCommand <*> parseCosmoScriptWithoutHelp {-whenParsing-})) -- <* lexeme ";"
-- parseCosmoScript ::                                    (HasTracer agent,
--                     Members '[ObjectIdentityMap WME, Reader agent, Log, Resource, OpenTelemetryContext, OpenTelemetrySpan, WorkingMemory] r) =>  {-WhenParsing ->-} Parser r CosmoScript
-- parseCosmoScript {-whenParsing-} = CosmoScriptBasicBlock <$> NE.some 
