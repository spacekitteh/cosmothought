module UX.Commands.Remove where

import Command
import Core.CoreTypes
import Core.Rule
import Cosmolude hiding (Any, Named, group)
import Data.List.NonEmpty (NonEmpty)
import PSCM
import Core.WorkingMemory.Reifiable
import UX.CosmoScript.Syntax as AST
import Core.WME

removeRulesCommand :: (HasRuleIdentifier ruleIDHolder, Member (ProductionSystem' metadata) r) => NonEmpty ruleIDHolder -> Sem r CommandResults
removeRulesCommand rules = do
  removeRules (fmap (view ruleIdentifier) rules)
  pure $ resultingIn # [SideEffect]

removeWMEsCommand :: (ReifiableToWMEs (WME' metadata) metadata, Member (ProductionSystem' metadata) r) => NonEmpty (WME' metadata) -> Sem r CommandResults
removeWMEsCommand wmes = do
  removeElements wmes
  pure $ resultingIn # [SideEffect]

processRemoveCommand :: (Members '[ProductionSystem' WMEMetadata] r) => AST.RemoveCommand -> Sem r CommandResults
processRemoveCommand (AST.RemoveRules rules) = removeRulesCommand rules
processRemoveCommand (AST.RemoveWMEs wmes) = removeWMEsCommand wmes
