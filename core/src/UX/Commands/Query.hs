module UX.Commands.Query where
import Command
import Core.CoreTypes

import Cosmolude hiding (Any, Named, group)
import qualified Data.List as DL
import GHC.Exts hiding (Any)
import GHC.Real
import PSCM.Effects.WorkingMemory hiding (numberOfReferences)
import UX.CosmoScript.Syntax as AST

queryExistsWMEPattern :: forall metadata r. (Members '[WorkingMemory' metadata] r) => IsAsserting -> WMEPattern -> Sem r CommandResults
queryExistsWMEPattern doesItAssert pat = do
  wmes <- findWMEsByPattern pat
  let listed = DL.intersperse hardline $ fmap render wmes
      idents = wmes ^.. folded . identifier . to IdentifierResult
      succeeds = has folded wmes
      message = if succeeds then "WME(s) exist matching pattern:" else "No WMEs match pattern"
      identsWithAssertion = case doesItAssert of
                              NonAsserting -> idents
                              Asserting | succeeds -> TestSucceeds : idents
                                        | otherwise -> (TestFails ("Failed to match pattern: " <+> render pat)) : idents
                                                       
  pure $ CommandResults (fromList $ (Message (message <+> (vsep listed))) : identsWithAssertion)

queryStatsObjectReferences :: forall  metadata r. (Members '[WorkingMemory' metadata, ObjectIdentityMap (WME' metadata)] r) => ObjectIdentifier -> Sem r CommandResults
queryStatsObjectReferences oid = do
  refs <- numberOfReferences oid
  let results = case refs of
        Nothing -> [(Message "No object found with that OID"), IdentifierResult (oid ^. identifier)]
        Just count -> [ValueResult ((fromIntegral count) ^. re _ConstantValue)]
  pure $ CommandResults (fromList results)

processExistsCommand ::forall  metadata r. (Members '[WorkingMemory' metadata] r) => AST.ExistsCommand -> Sem r CommandResults
processExistsCommand (ExistsWMEPattern doesItAssert pat) = queryExistsWMEPattern doesItAssert pat

processStatsCommand :: forall  metadata r. (Members '[WorkingMemory' metadata, ObjectIdentityMap (WME' metadata)] r) => AST.StatsCommand -> Sem r CommandResults
processStatsCommand (StatsObjectReferences oid) = queryStatsObjectReferences oid
                                                  
processQueryCommand :: forall  metadata r. (Members '[WorkingMemory' metadata, ObjectIdentityMap (WME' metadata)] r) => AST.QueryCommand -> Sem r CommandResults
processQueryCommand (AST.Exists cmd) = processExistsCommand cmd
processQueryCommand (AST.Stats cmd) = processStatsCommand cmd
