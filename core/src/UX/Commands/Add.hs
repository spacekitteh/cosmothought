module UX.Commands.Add (processAddCommand) where
import Command
import Core.CoreTypes
import Core.Rule
import Cosmolude hiding (Any, Named, group)
import Data.List.NonEmpty (NonEmpty)
import PSCM
import Core.WorkingMemory.Reifiable 
import UX.CosmoScript.Syntax as AST
import Core.WME
import Polysemy.Reader    
addRulesCommand :: (Members [ProductionSystem' metadata,OpenTelemetryContext, OpenTelemetrySpan, Reader agent, Log, Resource ] r, HasTracer agent) => NonEmpty Rule -> Sem r CommandResults
addRulesCommand rules = push "addRulesCommand" do
  () <- addRules rules
  pure $ resultingIn # [SideEffect] -- TODO: Include additions/removals somehow

addWMEsCommand :: forall metadata r. (ReifiableToWMEs (WME' metadata) metadata, Member (ProductionSystem' metadata) r) => NonEmpty (WME' metadata) -> Sem r CommandResults
addWMEsCommand wmes = do
  () <- addElements wmes
    -- firings <- addWMEs wmes
  pure $ resultingIn # [SideEffect] -- , Message (pretty wmes), Firings firings]

processAddCommand :: forall agent r. (Members [ProductionSystem' WMEMetadata ,OpenTelemetryContext, OpenTelemetrySpan, Reader agent, Log, Resource ] r, HasTracer agent) => AST.AddCommand -> Sem r CommandResults
processAddCommand (AST.AddRules rules) = addRulesCommand rules
processAddCommand (AST.AddWMEs wmes) = addWMEsCommand wmes
