module UX.Commands.Set where
import Command
import Core.Configuration
import Cosmolude hiding (Any, Named, group)
import qualified Data.HashSet as HS
import qualified Data.Sequence as Seq
import qualified Df1
import UX.CosmoScript.Syntax as AST
    
setLogLevel :: (Members '[KeyedState LogConfiguration] r) => Df1.Level -> Sem r CommandResults
setLogLevel level = do
  putAt MinimumLevel level
  pure $ resultingIn # [SideEffect]

setLogPath :: (Members '[KeyedState LogConfiguration] r) => [Df1.Path] -> Sem r CommandResults
setLogPath path = do
  putAt SuperPath (fmap coerce $ Seq.fromList path)
  pure $ resultingIn # [SideEffect]

setLogExcludes :: (Members '[KeyedState LogConfiguration] r) => [Df1.Path] -> Sem r CommandResults
setLogExcludes paths = do
  let excls = HS.fromList . coerce $ paths
  putAt ExcludePaths excls
  pure $ resultingIn # [SideEffect, Message ("Log exclusions set to:" <+> viaShow excls)]

setLogIncludes :: (Members '[KeyedState LogConfiguration] r) => [Df1.Path] -> Sem r CommandResults
setLogIncludes paths = do
  let r = case paths of
        [] -> Nothing
        xs -> Just (HS.fromList . coerce $ xs)
  putAt IncludePaths r
  pure $ resultingIn # [SideEffect, Message ("Log inclusions set to:" <+> viaShow r)]

           
setProductionRuleActivityOnRemoval :: (Member (KeyedState PSCMConfiguration) r) => ActivityOnRuleRemoval -> Sem r CommandResults
setProductionRuleActivityOnRemoval mode = do
  putAt PerformActivityOnRuleRemoval mode
  pure $ resultingIn # [SideEffect]

processSetCommand :: (Members '[KeyedState PSCMConfiguration, KeyedState LogConfiguration] r) => AST.SetCommand -> Sem r CommandResults
processSetCommand (SetLogLevel lvl) = setLogLevel lvl
processSetCommand (SetLogPath path) = setLogPath path
processSetCommand (SetLogExcludes excls) = setLogExcludes excls
processSetCommand (SetLogIncludes incls) = setLogIncludes incls
processSetCommand (SetProductionRuleActivityOnRemoval mode) = setProductionRuleActivityOnRemoval mode
