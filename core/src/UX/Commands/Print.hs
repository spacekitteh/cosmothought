module UX.Commands.Print where
import Command
import Core.Rule.Database
import Core.WME
import Cosmolude hiding (Any, Named, group)
import qualified Data.List as DL
--import DevelopmentInfo
import PSCM.Effects.WorkingMemory hiding (numberOfReferences)
import Polysemy.Reader (Reader, ask)
import Rete.Environment
import UX.CosmoScript.Syntax as AST

printRules :: Members '[RuleDatabase] r => Sem r CommandResults
printRules = do
  (rules :: [Rule]) <- getAllRulesInDatabase
  let (listed :: [CoreDoc]) = DL.intersperse hardline $ fmap render rules
  pure $ resultingIn # [Message (vsep listed)]


printWMEs :: Members '[WorkingMemory] r => Sem r CommandResults
printWMEs = do
    wmes <- getAllWMEs
    let listed = DL.intersperse hardline $ fmap render wmes
    pure $ resultingIn # [Message (vsep listed)]

printReteNodes :: (HasReteEnvironment agent WMEMetadata, Members '[Reader agent, Transactional] r) => Sem r CommandResults
printReteNodes =  do
  agent <- ask
  let env = agent^.reteEnvironment
  nodes <- env ^!! allNodes . elems
  let listed = DL.intersperse hardline $ fmap render nodes
  pure $ resultingIn # [Message (vsep listed)]


processPrintCommand :: (HasReteEnvironment agent WMEMetadata, Members '[Transactional,Reader agent,  RuleDatabase, WorkingMemory] r) => AST.PrintCommand -> Sem r CommandResults
processPrintCommand AST.PrintRules = printRules
processPrintCommand AST.PrintWMEs = printWMEs
processPrintCommand AST.PrintReteNodes = printReteNodes

       
    
