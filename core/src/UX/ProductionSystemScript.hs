module UX.ProductionSystemScript where

-- import CalamityCommands
-- import Core.Rule

import Command
import Cosmolude
-- import qualified Data.Sequence as Seq

import qualified Data.Sequence as Seq
import Data.Text

data ProductionSystemScriptSource = ProductionSystemScriptSource {_source :: Text} deriving (Eq, Show)

makeClassy ''ProductionSystemScriptSource

-- | A script that has been parsed into individual commands, but is missing metadata about the script itself.
data ParsedScript = ParsedScript {_rawCommands :: Seq RawCosmicCommand} deriving (Eq, Show)

makeLenses ''ParsedScript

parseScript' :: Parser r ParsedScript
parseScript' = do
  (ParsedScript . Seq.fromList) <$> some parseRawCosmicCommand

-- idea: Use calamitycommands. create a custom context, with the remaining text after as much parameters have been parsed as possible. this will create a series of parsers, each based on the command prefix.
-- ParsedScript <$> Seq.fromList <$> some parseCosmicCommand

data CosmicScript = CosmicScript {_commands :: Seq RawCosmicCommand}

makeLenses ''CosmicScript

elaborateScript :: (Members '[IDGen] r) => ParsedScript -> Sem r CosmicScript
elaborateScript (ParsedScript c) = CosmicScript <$> pure c
