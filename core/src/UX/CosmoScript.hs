module UX.CosmoScript (Expression, CosmoScriptCommand, processCosmoScriptCommand) where

--import Cosmolude
import UX.CosmoScript.Syntax
import Command
import Core.Configuration
import Core.Rule.Database
import Core.WME
import Core.WorkingMemory (HasWorkingMemoryData)
import Cosmolude hiding (Any, Named, group)
--import DevelopmentInfo
import PSCM
import Polysemy
import Polysemy.Reader (Reader)
import Rete.Environment
import UX.Commands

    


processHelpCommand :: CosmoScriptWithoutHelpCommand -> Sem r CommandResults
processHelpCommand _cmd = pure $ resultingIn # [Message "Help: not yet implemented"]



processCosmicCommand :: ( HasTracer agent, HasWorkingMemoryData agent WMEMetadata, HasReteEnvironment agent WMEMetadata, Members '[ProductionSystem' WMEMetadata, KeyedState PSCMConfiguration, KeyedState LogConfiguration, ObjectIdentityMap WME, Log, Transactional, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Embed IO, WorkingMemory, RuleDatabase, Resource] r) =>CosmoScriptWithoutHelpCommand -> Sem r CommandResults
processCosmicCommand (Add cmd) = processAddCommand cmd
processCosmicCommand (Remove cmd) = processRemoveCommand cmd
processCosmicCommand (Set cmd) = processSetCommand cmd
processCosmicCommand (Query cmd) = processQueryCommand cmd
processCosmicCommand (Print cmd) = processPrintCommand cmd
processCosmicCommand (Show cmd) = processShowDiagramCommand cmd
  
                         
processCosmoScriptCommand ::( HasTracer agent, HasWorkingMemoryData agent WMEMetadata, HasReteEnvironment agent WMEMetadata, Members '[ProductionSystem' WMEMetadata, KeyedState PSCMConfiguration, KeyedState LogConfiguration, ObjectIdentityMap WME, Log, Transactional, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Embed IO, WorkingMemory, RuleDatabase, Resource] r) => CosmoScriptCommand -> Sem r CommandResults
processCosmoScriptCommand (CosmoScriptCommand cmd) = push "processCosmoScriptCommand" $ processCosmicCommand cmd
processCosmoScriptCommand (HelpCommand cmd) = push "Get help for CosmoScript command" $ processHelpCommand cmd
  
