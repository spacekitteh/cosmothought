{-# OPTIONS_GHC -fplugin=Polysemy.Plugin #-}

module UX.CommandProcessor (AsCommandResult' (..), CommandResult' (..), CommandResult, CommandResults, commandResults, processCommand', ProcessCommandResult, runInCommandHandlerContext) where

import CalamityCommands
import CalamityCommands.Command
import qualified CalamityCommands.Group (Group)
import CalamityCommands.Parser (ParserEffs)
import Command
import Core.Configuration
import Core.CoreTypes
import Core.Identifiers.IdentifierGenerator
import Core.Patterns.Parser
import Core.Rule
import Core.Rule.Database
import Core.Rule.Parser
import Core.WME
import Core.WorkingMemory (HasWorkingMemoryData)
import Cosmolude hiding (Any, Named, group)
import Data.Coerce (coerce)
import Data.Either
import qualified Data.HashSet as HS
import qualified Data.List as DL
import Data.List.NonEmpty (NonEmpty)
import qualified Data.Sequence as Seq
import Data.Text (Text)
import qualified Data.Text as T
import Data.Type.Equality
import DevelopmentInfo
import qualified Df1
import GHC.Exts hiding (Any)
import GHC.Float
import GHC.Real
import PSCM
import PSCM.Effects.WorkingMemory hiding (numberOfReferences)
import Polysemy
import Polysemy.Final.More
import Polysemy.Fixpoint
import Polysemy.Reader (Reader, ask, runReader)
import Rete.Environment
import Rete.ReteEffect (Rete)
import UX.Commands

newtype ParsedRule = ParsedRule Rule

instance
  forall
    (agent :: Type)
    a
    c
    r.
  ( HasTracer agent,
    Members
      '[ ObjectIdentityMap (WME' a),
         IDGen,
         Transactional,
         Log,
         Resource,
         OpenTelemetryContext,
         OpenTelemetrySpan,
         Reader agent
       ]
      r
  ) =>
  ParameterParser ParsedRule c r
  where
  parameterDescription = "production rule"
  parse =
    adaptToCommand @agent
      (ParsedRule <$> parseRule @agent) -- @agent) -- @a @((Reader c) : r) )

newtype ParsedObjectIdentifier = ParsedObjectIdentifier ObjectIdentifier

instance
  forall c r.
  ( -- HasTracer agent,
    Members '[{-Reader agent, Reader c,-} ObjectIdentityMap (WME), IDGen, Transactional] r
  ) =>
  ParameterParser ParsedObjectIdentifier c r
  where
  parameterDescription = "object identifier"
  parse =
    adaptToCommand
      (ParsedObjectIdentifier <$> parseObjectIdentifierLookup ParsingUnspecified)

type ProductionSystem = ProductionSystem' WMEMetadata

type ProcessingConstraints agent r = (Members '[Input Tracer, Resource, OpenTelemetryContext, OpenTelemetrySpan, KeyedState LogConfiguration, KeyedState PSCMConfiguration, Log, ObjectIdentityMap (WME), WorkingMemory, Fixpoint, IDGen, Transactional, Rete, ProblemSubstates WMEMetadata, ProductionSystem, RuleDatabase, Embed IO] r)

definePrintCommands ::
  forall agent parseEffects baseEffects c.
  ( Members
      '[Final (Sem baseEffects)]
      parseEffects,
    CommandContext (Sem baseEffects) c CommandResults,
    ProcessingConstraints agent baseEffects,
    ProcessingConstraints agent parseEffects,
    DSLC (Sem baseEffects) c CommandResults parseEffects
  ) =>
  ReteEnvironment WMEMetadata ->
  Sem
    parseEffects
    -- (DSLState (Sem baseEffects) c CommandResults parseEffects)
    (Command (Sem baseEffects) c (CommandResults))
definePrintCommands env = group "print" $ do
  _ <- command @'[] "rules" $ \_ctx ->
    printRules
  command @'[] "wmes" $ \_ctx -> printWMEs
  group "rete" $ do
    command @'[] "nodes" $ \_ctx -> printReteNodes env

queryStatsObjectReferencesCommandHandler ::
  ( CommandContext (Sem baseEffects) c CommandResults,
    Members '[WorkingMemory, ObjectIdentityMap WME, Transactional, IDGen] parseEffects,
    DSLC (Sem baseEffects) c CommandResults parseEffects,
    Member (Final (Sem baseEffects)) parseEffects
  ) =>
  Sem
    parseEffects
    -- (DSLState (Sem baseEffects) c CommandResults parseEffects)
    (Command (Sem baseEffects) c CommandResults)
queryStatsObjectReferencesCommandHandler = command @'[Named "object identifier" ParsedObjectIdentifier] "references" $ \_ctx (ParsedObjectIdentifier oid) -> queryStatsObjectReferences oid

queryExistsWMEPatternCommandHandler ::
  ( CommandContext (Sem baseEffects) c CommandResults,
    Members '[WorkingMemory, ObjectIdentityMap WME, Transactional, IDGen] parseEffects,
    DSLC (Sem baseEffects) c CommandResults parseEffects,
    Member (Final (Sem baseEffects)) parseEffects
  ) =>
  Sem
    parseEffects
    -- (DSLState (Sem baseEffects) c CommandResults parseEffects)
    (Command (Sem baseEffects) c CommandResults)
queryExistsWMEPatternCommandHandler =
  command @'[Named "wme pattern" ParsedWMEPattern] "wme" $ \(_ctx :: c) (ParsedWMEPattern pat) -> queryExistsWMEPattern pat

defineQueryCommands ::
  ( CommandContext (Sem baseEffects) c CommandResults,
    Members [WorkingMemory, ObjectIdentityMap WME, Transactional, IDGen] parseEffects,
    Member (Final (Sem baseEffects)) parseEffects,
    DSLC (Sem baseEffects) c CommandResults parseEffects
  ) =>
  Sem
    parseEffects
    -- (DSLState (Sem baseEffects) c CommandResults parseEffects)
    (Command (Sem baseEffects) c (CommandResults))
defineQueryCommands = do
  _ <- group "stats" $ do
    group "object" $ do
      queryStatsObjectReferencesCommandHandler
  group "exists" $ do
    queryExistsWMEPatternCommandHandler

defineSetCommands ::
  forall agent parseEffects baseEffects c.
  ( Members
      '[Final (Sem baseEffects)]
      parseEffects,
    CommandContext (Sem baseEffects) c CommandResults,
    ProcessingConstraints agent baseEffects,
    ProcessingConstraints agent parseEffects,
    DSLC (Sem baseEffects) c CommandResults parseEffects
  ) =>
  Sem
    parseEffects
    -- (DSLState (Sem baseEffects) c CommandResults parseEffects)
    (Command (Sem baseEffects) c (CommandResults))
defineSetCommands = group "set" $ do
  _ <- group "production" $ do
    group "rule" $ do
      group "activity" $ do
        group "on" $ do
          command @'[Named "removal" ParsedActivityOnRuleRemoval] "removal" $ \_ctx (ParsedActivityOnRuleRemoval mode) -> setProductionRuleActivityOnRemoval mode

  group "log" $ do
    _ <- command @'[Named "level" (ParsedLevel)] "level" $ \_ctx (ParsedLevel level) -> setLogLevel level
    command @'[Named "path" [ParsedPath]] "path" $ \_ctx (path) -> setLogPath (coerce path)

    command @'[Named "excludes" [ParsedPath]] "excludes" $ \_ctx paths -> setLogExcludes (coerce paths)

    command @'[Named "includes" [ParsedPath]] "includes" $ \_ctx paths -> setLogIncludes (coerce paths)

newtype ParsedActivityOnRuleRemoval = ParsedActivityOnRuleRemoval ActivityOnRuleRemoval

instance (Members '[Transactional, IDGen] r) => ParameterParser ParsedActivityOnRuleRemoval c r where
  parameterDescription = "activity on rule removal"
  parse =
    adaptToCommand
      $ ParsedActivityOnRuleRemoval
      <$> ( try (AlwaysDoActivity <$ verbatim "always")
              <|> try (WhenNotOperatorSupported <$ (keyword "when" <* keyword "not" <* keyword "operator" <* verbatim "supported"))
              <|> (NeverDoActivity <$ verbatim "never")
          )

newtype ParsedLevel = ParsedLevel Df1.Level

instance (Members '[Transactional, IDGen] r) => ParameterParser ParsedLevel c r where
  parameterDescription = "log level"
  parse = adaptToCommand (ParsedLevel <$> parseDf1Level)

newtype ParsedPath = ParsedPath Df1.Path

instance (Members '[Transactional, IDGen] r) => ParameterParser ParsedPath c r where
  parameterDescription = "path"
  parse = adaptToCommand (ParsedPath <$> parseDf1Path)

newtype ParsedRuleID = ParsedRuleID RuleIdentifier

instance forall agent c r. (Members '[Transactional, IDGen] r) => ParameterParser ParsedRuleID c r where
  parameterDescription = "rule identifier"
  parse = adaptToCommand @agent ((ParsedRuleID . RuleIdentifier) <$> parseIdentifier)

addRulesCommandHandler ::
  forall agent metadata c parseEffects baseEffects.
  ( HasTracer agent,
    CommandContext (Sem baseEffects) c CommandResults,
    Members
      '[ ProductionSystem' metadata,
         Reader agent,
         ObjectIdentityMap (WME' metadata),
         IDGen,
         Transactional,
         Log,
         Resource,
         OpenTelemetryContext,
         OpenTelemetrySpan
       ]
      parseEffects,
    DSLC (Sem baseEffects) c CommandResults parseEffects,
    Member (Final (Sem baseEffects)) parseEffects
  ) =>
  Sem
    parseEffects
    -- (DSLState (Sem baseEffects) c CommandResults parseEffects)
    (Command (Sem baseEffects) c CommandResults)
addRulesCommandHandler =
  do
    CalamityCommands.help (const "Add new production rules") $ command @'[Named "rules" (NonEmpty ParsedRule)] "rules" $ \_ctx rules -> do
      addRulesCommand (coerce rules)

defineAddCommands ::
  forall agent parseEffects baseEffects c.
  ( CommandContext (Sem baseEffects) c CommandResults,
    HasTracer agent,
    Members
      [ ProductionSystem,
        Reader agent,
        ObjectIdentityMap (WME),
        IDGen,
        Transactional,
        Log,
        Resource,
        OpenTelemetryContext,
        OpenTelemetrySpan,
        WorkingMemory
      ]
      parseEffects,
    Member (Final (Sem baseEffects)) parseEffects,
    DSLC (Sem baseEffects) c CommandResults parseEffects
  ) =>
  Sem
    parseEffects
    -- (DSLState (Sem baseEffects) c CommandResults parseEffects)
    (Command (Sem baseEffects) c (CommandResults))
defineAddCommands = CalamityCommands.help (const "Manually add items to memory")
  $ group "add"
  $ do
    _ <- CalamityCommands.help (const ("Add new WMEs")) $ command @'[Named "wmes" (NonEmpty (WME))] "wmes" $ \_ctx wmes -> do
      addWMEsCommand wmes
    addRulesCommandHandler

defineRemoveCommands ::
  forall agent parseEffects baseEffects c.
  ( CommandContext (Sem baseEffects) c CommandResults,
    HasTracer agent,
    Members '[ProductionSystem, Transactional, IDGen, Reader agent, WorkingMemory, ObjectIdentityMap WME] parseEffects,
    Member (Final (Sem baseEffects)) parseEffects,
    DSLC (Sem baseEffects) c CommandResults parseEffects
  ) =>
  Sem
    parseEffects
    -- (DSLState (Sem baseEffects) c CommandResults parseEffects)
    (Command (Sem baseEffects) c (CommandResults))
defineRemoveCommands = group "remove" $ do
  _ <- command @'[Named "rule identifiers" (NonEmpty ParsedRuleID)] @c @CommandResults @(Sem baseEffects) @parseEffects "rules" $ \_ctx rules -> do
    removeRulesCommand ((coerce :: NonEmpty ParsedRuleID -> NonEmpty RuleIdentifier) rules)
  command @'[Named "wmes" (NonEmpty (WME))] "wmes" $ \_ctx wmes -> do
    removeWMEsCommand wmes

defineShowDiagramCommands ::
  forall agent parseEffects baseEffects c.
  ( HasTracer agent,
    HasWorkingMemoryData
      agent
      WMEMetadata,
    Members
      '[Final (Sem baseEffects), Reader agent, Log, ObjectIdentityMap WME, IDGen, WorkingMemory, Transactional, Resource, OpenTelemetryContext, OpenTelemetrySpan, RuleDatabase, Embed IO]
      parseEffects,
    Members '[Reader agent, Log, Resource, OpenTelemetryContext, OpenTelemetrySpan] baseEffects,
    CommandContext (Sem baseEffects) c CommandResults,
    HasWorkingMemoryData agent WMEMetadata,
    HasReteEnvironment agent WMEMetadata,
    --    ProcessingConstraints agent baseEffects,
    --    ProcessingConstraints agent parseEffects,
    DSLC (Sem baseEffects) c CommandResults parseEffects
  ) =>
  Sem
    parseEffects
    -- (DSLState (Sem baseEffects) c CommandResults parseEffects)
    (Command (Sem baseEffects) c (CommandResults))
defineShowDiagramCommands = group "show" $ group "diagram" $ do
  _ <- group "working" $ command @'[Named "size" Float, Named "filepath" Text] "memory" $ \_ctx size filePath -> do
    showDiagramWorkingMemory @agent size (T.unpack filePath)
  group "rete" $ do
    _ <- group "nodes" $ group "for" $ command @'[Named "rule" ParsedRule, Named "size" Float, Named "filepath" Text] "rule" $ \_ctx (ParsedRule rule) size filePath -> do
      showDiagramReteNodesForRule @agent rule size (T.unpack filePath)
    command @'[Named "size" Float, Named "filepath" Text] "nodes" $ \_ctx size filePath -> do
      showDiagramReteNodes @agent size (T.unpack filePath)

commandHandler ::
  -- forall agent (parseEffects :: EffectRow) (baseEffects :: EffectRow) c.
  ( HasCallStack,
    HasTracer agent,
    HasReteEnvironment agent WMEMetadata,
    HasWorkingMemoryData agent WMEMetadata,
    Members '[Input Tracer, Resource, OpenTelemetryContext, OpenTelemetrySpan, KeyedState LogConfiguration, KeyedState PSCMConfiguration, Log, ObjectIdentityMap (WME), WorkingMemory, Fixpoint, IDGen, Transactional, Rete, ProblemSubstates WMEMetadata, ProductionSystem, RuleDatabase, Embed IO, Reader agent] baseEffects,
    -- Members
    --   '[ ProductionSystem,
    --      Transactional,
    --      IDGen,
    --      Reader agent,
    --      WorkingMemory,
    --      ObjectIdentityMap WME,
    --      Log,
    --      Resource,
    --      OpenTelemetryContext,
    --      OpenTelemetrySpan
    --    ]
    --   parseEffects,
    --    ProcessingConstraints agent baseEffects,
    --    Member (Reader agent) parseEffects,
    CommandContext (Sem baseEffects) c CommandResults,
    --    DSLC (Sem baseEffects) c CommandResults parseEffects,
    DSLC (Sem baseEffects) c CommandResults baseEffects
  ) =>
  agent ->
  Sem baseEffects (CommandHandler (Sem baseEffects) c CommandResults)
commandHandler agent =
  runFinalSem
    $ do
      (commands, _) <-
        buildCommands do
          ( raiseDSL do
              definePrintCommands (agent ^. reteEnvironment)
              defineQueryCommands
              defineAddCommands -- @agent @parseEffects @baseEffects @c
              defineSetCommands
              defineShowDiagramCommands -- @agent @parseEffects @baseEffects @c
              defineRemoveCommands
              command @'[] "git-version" $ \_ctx -> do
                pure $ resultingIn # [Message . render $ repoHash]
            )
          helpCommand (\_ctx text -> pure $ resultingIn # [Message . render $ text])
      pure commands

type CoreCommandContext r = BasicContext (Sem r) CommandResults

type ProcessCommandResult baseEffects = Maybe (Either (CmdInvokeFailReason (CoreCommandContext baseEffects)) ((CoreCommandContext baseEffects), CommandResults))

processCommand' ::
  ( HasCallStack,
    HasTracer agent,
    CommandContext
      (Sem baseEffects)
      (CoreCommandContext (Sem baseEffects)) -- (DSLState (Sem baseEffects) c (ProcessCommandResult baseEffects) baseEffects))
      CommandResults,
    Member (Reader agent) baseEffects,
    ProcessingConstraints agent baseEffects,
    HasReteEnvironment agent WMEMetadata,
    HasWorkingMemoryData agent WMEMetadata,
    -- _ -- ,
    DSLC (Sem baseEffects) (CoreCommandContext baseEffects) CommandResults baseEffects
  ) =>
  agent ->
  Text ->
  Sem baseEffects (ProcessCommandResult baseEffects)
processCommand' env message = do
  addAttributes [("ux.command", toAttribute message)]
  handler <- commandHandler env -- @parseEffects
  runFinalSem . embedToFinal . useBasicContext . useConstantPrefix "!" $ processCommands handler message

runInCommandHandlerContext ::
  ( HasCallStack,
    HasTracer agent,
    CommandContext
      (Sem baseEffects)
      (CoreCommandContext baseEffects)
      CommandResults,
    Member (Reader agent) baseEffects,
    ProcessingConstraints agent baseEffects,
    HasReteEnvironment agent WMEMetadata,
    HasWorkingMemoryData agent WMEMetadata,
    DSLC (Sem baseEffects) (CoreCommandContext baseEffects) CommandResults baseEffects
  ) =>
  agent ->
  ( CommandHandler (Sem baseEffects) (CoreCommandContext baseEffects) CommandResults ->
    Sem
      ( ParsePrefix Text
          : ConstructContext msg (BasicContext m a') m a'
          : Embed (Sem baseEffects)
          : Final (Sem baseEffects)
          : baseEffects
      )
      (ProcessCommandResult baseEffects)
  ) ->
  Sem baseEffects (ProcessCommandResult baseEffects)
runInCommandHandlerContext env action = do
  handler <- commandHandler env -- @parseEffects
  runFinalSem . embedToFinal . useBasicContext . useConstantPrefix "!" $ action handler
