module Command (module Command, module Commands.Results, module Commands.ProductionSystem) where

import Commands.Results
import Commands.ProductionSystem

--import Core.Rule.Parser () -- For orphan instance
import qualified Df1


import Polysemy.Error hiding (try)
import Diagrams.Prelude hiding ((#))
import Rete.Environment
import Diagrams.Backend.SVG


import Polysemy.Reader
import Core.Rule.Database
import Polysemy.Embed
import Rete.Diagram
import Core.WorkingMemory.Diagram
import Core.CoreTypes
import Core.Rule
import System.FilePath
import Core.WME
import Core.WorkingMemory
import Cosmolude hiding (group, Named, Any(..))
import Data.Either

import GHC.Exts hiding (Any)
import qualified Data.Text as T
import Text.Megaparsec.Char
import Control.Exception (Exception(..))

type CommandResult = CommandResult' WMEMetadata (WMEToken ? IsProductionToken)
type CommandResults = CommandResults' WMEMetadata (WMEToken ? IsProductionToken)
newtype RawCosmicCommand = RawCosmicCommand {_rawCommand :: T.Text} deriving (Eq, Show)

parseRawCosmicCommand :: Parser r RawCosmicCommand
parseRawCosmicCommand =  RawCosmicCommand <$> (takeWhile1P Nothing (const True)) <* (try eof <|> (void eol <* ((try (char '!')) <|> char '?')))

                         
data ShowDiagramCommand = ShowReteNetworkDiagram Double FilePath | ShowReteNetworkDiagramForRule RuleIdentifier Double FilePath | ShowWorkingMemoryDiagram Double FilePath deriving stock (Eq, Show, Generic)
data PrintCommand = PrintRules | PrintWMEs | PrintNodes deriving stock (Eq, Show, Generic)

data StatsCommand = OIDStats OIDStatsCommand  deriving stock (Eq, Show, Generic)
data QueryCommand = Stats StatsCommand | Exists ExistsCommand deriving (Eq, Show, Generic)
data SetLogCommand = SetLevel Df1.Level | SetPath [Df1.Path] | SetExcludes [Df1.Path] | SetIncludes [Df1.Path] deriving stock (Eq, Show, Generic)
data SetCommand = SetProductionRuleActivity SetProductionRuleActivityCommand
                | SetLog SetLogCommand
                  deriving stock (Eq, Show, Generic)


data CoreCommand = AQueryCommand QueryCommand
              | APrintCommand PrintCommand
              | ASetCommand SetCommand
              | AnAddCommand AddCommand
              | ARemovalCommand RemoveCommand
              | AShowDiagramCommand ShowDiagramCommand
              | VersionCommand
                deriving stock (Eq, Show, Generic)


drawDiagram :: FilePath -> Double -> QDiagram B V2 Double Any -> IO ()
drawDiagram filePath size diag = renderPretty filePath (dims (V2 size size)) diag


                                 
returnDiagram :: forall  r b err. (Show err, Show b, Exception err, -- Diagrams.Prelude.Renderable (Diagrams.Prelude.Path V2 Double) b, Diagrams.Prelude.Renderable (Text Double) b,
                                             Members '[ Embed IO] r) => Double -> FilePath -> Either err (QDiagram B V2 Double Any, b) ->  Sem r CommandResults
returnDiagram size filePath result = case result of
        Left err -> pure $ resultingIn # [CommandError . Left .  toException $ err]
        Right (diag, graph) -> do
          embed $ drawDiagram filePath  size diag
          pure $ resultingIn # [SideEffect, Message . viaShow $ graph, FileWrittenTo filePath]
               
emitReteNodeDiagramForRule ::  forall agent metadata r. (HasTracer agent, Cosmolude.Renderable metadata, HasReteEnvironment agent metadata, Members '[Log, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, Embed IO, Transactional,   RuleDatabase] r) => Rule -> Double -> FilePath -> Sem r CommandResults
emitReteNodeDiagramForRule  rule size filePath =  do
  result <-  runError $  mapError RuleNotInReteEnv $ mapError RuleNotInDB $ renderReteNetworkGraphForRule rule
  returnDiagram  size filePath result
emitReteNodeDiagramForAllRules ::  forall agent metadata  r . (Cosmolude.Renderable metadata, HasReteEnvironment agent metadata, Members '[Log, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, Embed IO, Transactional,  RuleDatabase] r) =>Double -> FilePath -> Sem r CommandResults
emitReteNodeDiagramForAllRules size filePath =  do
  result <- runError $  mapError RuleNotInDB $ renderAllReteNodes 
  returnDiagram size filePath result
  
emitWorkingMemoryGraph ::  forall agent metadata r. (HasTracer agent, HasExtraObjectIdentifiers metadata, WMEIsState metadata, HasMaybeWMPreference metadata ObjectIdentifier, Show metadata, Cosmolude.Renderable metadata, HasWorkingMemoryData agent metadata,  HasReteEnvironment agent metadata, Members '[Reader agent, Log, WorkingMemory' metadata, OpenTelemetrySpan, OpenTelemetryContext, RuleDatabase, Embed IO, Transactional] r) =>  Double -> FilePath -> Sem r CommandResults
emitWorkingMemoryGraph size filePath = do
    (diag, graph) <-  renderWorkingMemoryGraph @_ @metadata @_ 
    embed $ drawDiagram filePath size diag
    pure $ resultingIn # [SideEffect, Message . viaShow $ graph, FileWrittenTo filePath]


data ShowDiagramCommandError = RuleNotInReteEnv RuleNodesNotInReteEnvironment
                             | RuleNotInDB RuleNotInRuleDatabase
                               deriving (Show, Exception)         
