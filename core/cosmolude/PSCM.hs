{-# LANGUAGE QuasiQuotes #-}
{-# OPTIONS_GHC -Wwarn=unused-top-binds #-}

module PSCM
  (
    module PSCM.ProductionSystem,
    module PSCM.Effects.ProblemSubstates,
    module PSCM.States,
--    module PSCM,
    PSCMEffects,
    PSCMConstraints,
    runPSCM,
--    runPSCMLoop,
    PSCMPS.HasTopLevelStateInitialised,
    SubstateIdentificationException (..), -- runPSCMPhase,
    enterPSCMPhase,
  )
where

import Data.Tuple (snd)
import Core.Configuration
import Core.CoreTypes
import PSCM.Errors (WithTestOutcome)
-- import PSCM.States
import qualified Data.Text as T
import Core.Rule.Database
import Core.Truth.Model
import Core.WorkingMemory
import Core.WorkingMemory.Reifiable
import Cosmolude
import Data.Either
import Data.Semigroup
import Development.DomainModelling (decision)
import Model.PSCM
import Model.PSCM.BasicTypes
import PSCM.Effects.RuleMatcher
import PSCM.Impasses
import PSCM.Operators
import PSCM.Effects.ProblemSubstates ( ProblemSubstates, currentState  )
import PSCM.ProblemSubstates (HasStateManagementState, HasTopLevelStateInitialised, runProblemSubstates, currentDepth )
import qualified PSCM.ProblemSubstates as PSCMPS (HasTopLevelStateInitialised)
import PSCM.ProductionSystem
import PSCM.States (HasPendingActivity, HasStateMap, StateData, resetStateMap, Impasse)
import Polysemy
import Polysemy.Error
import Polysemy.Internal
import Polysemy.Methodology
import Polysemy.Output
import Polysemy.Reader
import Polysemy.State
import Polysemy.Tagged
import qualified Prelude (error)

type PSCMEffects metadata proof = '[ProductionSystem' metadata, ProblemSubstates metadata, KeyedState PSCMConfiguration, VariableMapping ConcreteTruth, Tagged PSCMLoopPhase (State PSCMLoopPhase), Tagged CurrentOperator (State (ChosenOperator metadata))]

type PSCMConstraints agent metadata proof = (HasTracer agent, WithTestOutcome agent,MetadataAndProofConstraints metadata proof, HasTopLevelStateInitialised agent metadata, HasStateManagementState agent metadata, HasPendingActivity agent metadata proof, HasTruthModel agent, HasPendingActivity agent metadata proof, HasStateMap agent metadata, HasWorkingMemoryData agent metadata)

runPSCM :: (CreateBlankWMEMetadata metadata, HasWMESupport metadata, HasCreatingState metadata, WMEIsState metadata, PSCMConstraints agent metadata proof, HasCallStack, Members '[GlobalContextID, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, Transactional, KeyedState Configuration, Log, ObjectIdentityMap (WME' metadata), Error (SubstateIdentificationException metadata proof), Embed IO, Final IO, IDGen, RuleDatabase, WorkingMemory' metadata, RuleMatcher metadata matcherComponents proof, Reader agent] r) => Sem (Append (PSCMEffects metadata proof)  r) x -> Sem r x
runPSCM s = push "runPSCM" do
  agent <- ask
  --  interpPrePSCM env
  -- handlePSCMExcptionsAsIO
  --   .
  (evalState (Prelude.error "CurrentOperator not initialised!") . untag @CurrentOperator )
    . (evalState InputElaboration . untag @PSCMLoopPhase  )
    . runVariableMapping agent
    . embedPSCMConfiguration
    . runProblemSubstates
    . interpretTransaction
    . runProductionSystem
    . raiseUnder
    $ s

data HaltOrContinue = Halt | Continue deriving (Eq, Show)

gatherPSCMStatistics :: Sem r PSCMStatisticsForIteration
gatherPSCMStatistics = pure PSCMStatisticsForIteration

processPSCMLoopStatistics :: (HasStateManagementState agent metadata, Members '[Reader agent, Transactional, Input PSCMStatistics, Tagged PSCMLoopPhase (State PSCMLoopPhase), RuleMatcher metadata matcherComponent matchProof] r) => PSCMStatisticsForIteration -> Sem r PSCMStatistics
processPSCMLoopStatistics _iterationStats = do
  totalStats <- input
  phase <- tag @PSCMLoopPhase  get
  stateDepth <- currentDepth
  (ruleCount, _) <- getAllRules 
  pure (totalStats & pscmCycleCount +~ 1 &  currentPhase .~ phase & currentStateDepth .~ stateDepth & totalRules .~ ruleCount)

decideToHaltLoop :: (Members '[KeyedState PSCMConfiguration, Input PSCMStatistics] r) => EnvironmentalInput metadata -> Sem r Bool
decideToHaltLoop (EnvironmentalInput Halt _) = pure True
decideToHaltLoop _ = do
  cycleLimit <- getAt MaximumPSCMCycles
  case cycleLimit of
    Nothing -> pure False
    Just limit -> do
      stats <- input @PSCMStatistics
      let cycleCount = stats ^. pscmCycleCount
      pure (cycleCount >= limit)

-- runPSCMLoop :: (WMETruthValue (WME' metadata), HasCreatingState metadata, CreateBlankWMEMetadata metadata, HasWMESupport metadata, ReifiableToWMEs (WME' metadata) metadata, HasMaybeWMPreference (metadata) ObjectIdentifier, Hashable (WME' metadata), HasStateMap agent metadata, HasCallStack, Members '[KeyedState PSCMConfiguration, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, Log, Reader agent, Transactional, ProblemSubstates metadata, ProductionSystem' metadata, WorkingMemory' metadata, IDGen] r) => Sem r PSCMStatistics
-- runPSCMLoop = execState newPSCMStatistics enterPSCM

-- runPSCMPhase :: (ProductionSystemEffects' agent metadata component proof r, PSCMConstraints agent metadata proof, HasCreatingState metadata, CreateBlankWMEMetadata metadata,WMEIsState metadata,  HasWorkingMemoryData agent metadata,  HasWMESupport metadata, ReifiableToWMEs (WME' metadata) metadata, HasMaybeWMPreference (WME' metadata) ObjectIdentifier,Hashable (WME' metadata), HasStateMap agent metadata, HasCallStack, Members '[ KeyedState PSCMConfiguration, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, Log, Reader agent, Transactional, ProblemSubstates metadata, ProductionSystem' metadata, WorkingMemory' metadata] r) => Sem r PSCMStatistics
-- runPSCMPhase = execState newPSCMStatistics enterPSCMPhase

enterPSCMPhase :: (ProductionSystemEffects' agent metadata component proof r, PSCMConstraints agent metadata proof, HasCreatingState metadata, WMEIsState metadata, HasWorkingMemoryData agent metadata, CreateBlankWMEMetadata metadata, HasWMESupport metadata, ReifiableToWMEs (WME' metadata) metadata, HasMaybeWMPreference (WME' metadata) ObjectIdentifier, Hashable (WME' metadata), HasStateMap agent metadata, HasCallStack, Members '[Tagged PSCMLoopPhase (State PSCMLoopPhase), Tagged CurrentOperator (State (ChosenOperator metadata)), Tagged AllOperatorProposals (State (OperatorProposals metadata)), KeyedState PSCMConfiguration, State PSCMStatistics, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, Log, Reader agent, Transactional, ProblemSubstates metadata, ProductionSystem' metadata, WorkingMemory' metadata] r) => Sem r ()
enterPSCMPhase = push "enterPSCMPhase" $ do
  currentPhase <- tag @PSCMLoopPhase get
  addAttributes [("pscm.phase", toAttribute @T.Text $ fromString (show currentPhase))]
  pscmPhase runUntilQuiescence
    & untag @GetEnvironmentalInput
    & runInputSem getEnvironmentalInput
    & untag @ProcessEnvironmentalInput
    & runMethodologySem processEnvironmentalInput
    & untag @DecideToHaltLoop
    & decision
    & runMethodologySem ((runInputSem get) . decideToHaltLoop)
    & untag @ProposeOperators
    & runMethodologySem (\x ->  push "ProposeOperators" do
                                 res <- snd <$> gatherOperatorProposals x
                                 addAttributes [("pscm.operators.proposed", toAttribute res)]
                                 pure res)
    & runDecisionPhase
    & untag @ApplyChosenOperator
    & runMethodologySem applyChosenOperator -- runApplicationPhase)
    & untag @SetOperatorSelectionInWorkingMemoryAndElaborate
    & runMethodologyPure (const ()) -- This phase is moot as we handle everything in the ApplyChosenOperator phase.
    & runOutputPhase
    & untag @ResolveImpasses
    & runMethodologySem resolveAllImpasses
    & untag @GatherPSCMCycleStatisticsForCurrentIteration
    & runInputSem gatherPSCMStatistics
    & untag @ProcessPSCMCycleStatistics
    & runMethodologySem ((runInputSem get) . processPSCMLoopStatistics)
    & untag @EmitPSCMCycleStatistics
    & runOutputSem put
    --    & untag @AllOperatorProposals & evalState mempty -- start with empty state? TODO FIXME ?
    --    & untag @CurrentOperator & evalState (Prelude.error "CurrentOperator not initialised!") -- TODO FIXME? This is only the uninitialised error

    & untag @GetEnvironmentalOutputs
    & runInputSem (pure EnvironmentalOutput) --(Prelude.error "GetEnvironmentalOutputs")
    & untag @IsAtQuiescence
    & runInputSem (pure True) -- Prelude.error "IsAtQuiescence")
    --    & untag @PSCMLoopPhase & evalState InputElaboration

-- enterPSCM :: (WMETruthValue (WME' metadata),HasCreatingState metadata, CreateBlankWMEMetadata metadata, HasWMESupport metadata, ReifiableToWMEs (WME' metadata) metadata, HasMaybeWMPreference ( metadata) ObjectIdentifier, Hashable (WME' metadata), HasStateMap agent metadata, HasCallStack, Members '[KeyedState PSCMConfiguration, State PSCMStatistics, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, Log, Reader agent, Transactional, IDGen, ProblemSubstates metadata, ProductionSystem' metadata, WorkingMemory' metadata] r) => Sem r ()
-- enterPSCM =
--   pscmLoop
--     & untag @GetEnvironmentalInput
--     & runInputSem getEnvironmentalInput
--     & untag @ProcessEnvironmentalInput
--     & runMethodologySem processEnvironmentalInput
--     & untag @DecideToHaltLoop
--     & decision
--     & runMethodologySem ((runInputSem get) . decideToHaltLoop)
--     & runProposalPhase
--     & runDecisionPhase
--     & runApplicationPhase
--     & runOutputPhase
--     & untag @ResolveImpasses
--     & runMethodologySem resolveAllImpasses
--     & untag @GatherPSCMCycleStatisticsForCurrentIteration
--     & runInputSem gatherPSCMStatistics
--     & untag @ProcessPSCMCycleStatistics
--     & runMethodologySem ((runInputSem get) . processPSCMLoopStatistics)
--     & untag @EmitPSCMCycleStatistics
--     & runOutputSem put

resolveAllImpasses :: ChosenOperator metadata -> Sem r ()
resolveAllImpasses _ = pure ()

runDecisionPhase ::
  (Show metadata, Renderable metadata, HasTracer agent, WMETruthValue (WME' metadata), HasCreatingState metadata, CreateBlankWMEMetadata metadata, HasWMESupport metadata, ReifiableToWMEs (WME' metadata) metadata, Hashable (WME' metadata), HasMaybeWMPreference ( metadata) ObjectIdentifier, HasStateMap agent metadata, HasCallStack, Members '[ IDGen, OpenTelemetrySpan, OpenTelemetryContext, Resource, ProblemSubstates metadata, ProductionSystem' metadata, WorkingMemory' metadata, Log, Reader agent, Transactional] r) =>
  InterpretersFor
    '[ Tagged CurrentState (Input StateIdentifier),
       Tagged GetOperatorProposals (Methodology StateIdentifier (OperatorProposals metadata)),
       Tagged EvaluateOperatorProposals (Methodology (OperatorProposals metadata) (Either (Impasse metadata) (ChosenOperator metadata))),
       Tagged SelectNewOperator (Output (ChosenOperator metadata)),
       Tagged CreateNewSubstate (Output (Impasse metadata))
     ]
    r
runDecisionPhase s =
  s
    & untag @CurrentState
    & (runInputSem currentState)
    & untag @GetOperatorProposals
    & (runMethodologySem (\x -> snd <$> gatherOperatorProposals x))
    & untag @EvaluateOperatorProposals
    & ((runMethodologySem (\a -> runInputSem currentState $ evaluateOperators a)))
    & untag @SelectNewOperator
    & (runOutputSem applyChosenOperator)
    & untag @CreateNewSubstate
    & (runOutputSem createSubstateFromImpasse)

data EnvironmentalInput metadata where
  EnvironmentalInput :: (ReifiableToWMEs a metadata) => HaltOrContinue -> a -> EnvironmentalInput metadata

getEnvironmentalInput :: Sem r (EnvironmentalInput metadata)
getEnvironmentalInput = pure (EnvironmentalInput Continue ())

processEnvironmentalInput :: (HasCallStack, Members '[ProductionSystem' metadata] r) => EnvironmentalInput metadata -> Sem r ()
processEnvironmentalInput (EnvironmentalInput _ input) = addElements input

data EnvironmentalOutput = EnvironmentalOutput deriving (Eq, Show, Generic)

instance Semigroup EnvironmentalOutput where
  {-# INLINE (<>) #-}
  EnvironmentalOutput <> EnvironmentalOutput = EnvironmentalOutput

instance Monoid EnvironmentalOutput where
  mempty = EnvironmentalOutput

getEnvironmentalOutput :: (Members '[WorkingMemory' metadata] r) => Sem r EnvironmentalOutput
getEnvironmentalOutput = pure mempty

doApplicationPhase :: (HasMaybeWMPreference metadata ObjectIdentifier, HasCreatingState metadata, CreateBlankWMEMetadata metadata, HasWMESupport metadata, ReifiableToWMEs (WME' metadata) metadata, HasStateMap agent metadata, Members '[Log, Transactional, WorkingMemory' metadata, Reader agent, ProblemSubstates metadata, ProductionSystem' metadata] r) => ChosenOperator metadata -> Sem r EnvironmentalOutput
doApplicationPhase chosen = do
  applyChosenOperator chosen
  getEnvironmentalOutput

runApplicationPhase :: (HasMaybeWMPreference metadata ObjectIdentifier, HasCreatingState metadata, CreateBlankWMEMetadata metadata, HasWMESupport metadata, ReifiableToWMEs (WME' metadata) metadata, HasStateMap agent metadata, Members '[Log, Reader agent, Transactional, ProblemSubstates metadata, ProductionSystem' metadata, WorkingMemory' metadata] r) => InterpreterFor (Tagged ApplyChosenOperator (Methodology (ChosenOperator metadata) EnvironmentalOutput)) r
runApplicationPhase s = s & untag @ApplyChosenOperator & runMethodologySem doApplicationPhase

runOutputPhase :: InterpreterFor (Tagged OutputResultsToEnvironment (Output EnvironmentalOutput)) r
runOutputPhase s = s & untag @OutputResultsToEnvironment & ignoreOutput

runProposalPhase :: (HasTracer agent, Show metadata, Renderable metadata, HasMaybeWMPreference ( metadata) ObjectIdentifier, Members '[Log, OpenTelemetryContext, OpenTelemetrySpan, Resource, Reader agent, WorkingMemory' metadata] r) => InterpreterFor (Tagged ProposeOperators (Methodology StateIdentifier (OperatorProposals metadata))) r
runProposalPhase s = s & untag @ProposeOperators & runMethodologySem (\x -> snd <$> (gatherOperatorProposals x))

