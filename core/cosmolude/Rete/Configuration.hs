{-# LANGUAGE NamedFieldPuns #-}

module Rete.Configuration (ReteConfiguration (..), newReteConfigurationData, resetReteConfigurationData, ReteConfigurationData, reteConfigurationToVariables) where
import Control.Concurrent.STM
--import GHC.Generics
import Prelude



data ReteConfiguration a where
 UseNonBinaryNodes :: ReteConfiguration Bool
 
data ReteConfigurationData = ReteConfigurationData {
      useNonBinaryNodesVar :: TVar Bool
    }

{-# INLINE newReteConfigurationData #-}
newReteConfigurationData :: STM ReteConfigurationData
newReteConfigurationData = ReteConfigurationData <$> newTVar True

resetReteConfigurationData :: ReteConfigurationData -> STM ()
resetReteConfigurationData (ReteConfigurationData useNonBinary) = do
  writeTVar useNonBinary True
                           
{-# INLINE reteConfigurationToVariables #-}
reteConfigurationToVariables :: ReteConfigurationData -> ReteConfiguration a -> TVar a
reteConfigurationToVariables (ReteConfigurationData {useNonBinaryNodesVar}) UseNonBinaryNodes = useNonBinaryNodesVar
