{-# LANGUAGE NamedFieldPuns #-}
module PSCM.Configuration (ActivityOnRuleRemoval(..), PSCMConfiguration(..), pscmConfigurationToVariables, PSCMConfigurationData, newPSCMConfigurationData, resetPSCMConfigurationData) where
import Control.Concurrent.STM
import GHC.Generics
import Prelude
import Core.Truth
import Algebra.Lattice
import Model.PSCM.BasicTypes
import Data.Hashable    
data ActivityOnRuleRemoval = AlwaysDoActivity
                           | WhenNotOperatorSupported
                           | NeverDoActivity deriving (Eq, Show, Generic, Hashable)





data PSCMConfiguration a where
 PerformActivityOnRuleRemoval :: PSCMConfiguration ActivityOnRuleRemoval
 AllowParallelStates :: PSCMConfiguration Bool
 DropWMEsWithTruthBelow :: PSCMConfiguration ConcreteTruth
 Don'tActivateRulesWithTruthBelow :: PSCMConfiguration ConcreteTruth
 MaximumIterationsForQuiescence :: PSCMConfiguration (Maybe QuiescenceFuel)
 MaximumPSCMCycles :: PSCMConfiguration (Maybe PSCMCycleCount)
 

data PSCMConfigurationData  = PSCMConfigurationData {
      performActivityOnRuleRemovalVar :: TVar ActivityOnRuleRemoval,
      allowParallelStatesVar :: TVar Bool,
      dropWMEsWithTruthBelowVar :: TVar ConcreteTruth,
      don'tActivateRulesWithTruthBelowVar :: TVar ConcreteTruth,
      maximumIterationsForQuiescenceVar :: TVar (Maybe QuiescenceFuel),
      maximumPSCMCyclesVar :: TVar (Maybe PSCMCycleCount)
    }
{-# INLINE newPSCMConfigurationData #-}                            
newPSCMConfigurationData :: STM PSCMConfigurationData
newPSCMConfigurationData = PSCMConfigurationData
                             <$> newTVar AlwaysDoActivity
                             <*> newTVar True
                             <*> newTVar bottom
                             <*> newTVar bottom
                             <*> newTVar Nothing
                             <*> newTVar Nothing
                                 
                                 
resetPSCMConfigurationData :: PSCMConfigurationData -> STM ()
resetPSCMConfigurationData (PSCMConfigurationData performActivityOnRuleRemovalVar
                                                  allowParallelStatesVar
                                                  dropWMEsWithTruthBelowVar
                                                  don'tActivateRulesWithTruthBelowVar
                                                  maximumIterationsForQuiescenceVar
                                                  maximumPSCMCyclesVar) = do
  writeTVar performActivityOnRuleRemovalVar AlwaysDoActivity
  writeTVar allowParallelStatesVar True
  writeTVar dropWMEsWithTruthBelowVar bottom
  writeTVar don'tActivateRulesWithTruthBelowVar bottom
  writeTVar maximumIterationsForQuiescenceVar Nothing
  writeTVar maximumPSCMCyclesVar Nothing
                           
                            
{-# INLINE pscmConfigurationToVariables #-}
pscmConfigurationToVariables :: PSCMConfigurationData ->  PSCMConfiguration a -> TVar a
pscmConfigurationToVariables (PSCMConfigurationData{..}) PerformActivityOnRuleRemoval = performActivityOnRuleRemovalVar
pscmConfigurationToVariables (PSCMConfigurationData{..}) AllowParallelStates = allowParallelStatesVar
pscmConfigurationToVariables (PSCMConfigurationData{..}) DropWMEsWithTruthBelow = dropWMEsWithTruthBelowVar
pscmConfigurationToVariables (PSCMConfigurationData{..}) Don'tActivateRulesWithTruthBelow = don'tActivateRulesWithTruthBelowVar
pscmConfigurationToVariables (PSCMConfigurationData{..}) MaximumIterationsForQuiescence = maximumIterationsForQuiescenceVar
pscmConfigurationToVariables (PSCMConfigurationData{..}) MaximumPSCMCycles = maximumPSCMCyclesVar
