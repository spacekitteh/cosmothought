{-# LANGUAGE QuasiQuotes #-}
{-# OPTIONS_GHC -Wwarn=x-partial #-}
module PSCM.Operators (proposeOperator, gatherOperatorProposals, evaluateOperators, applyChosenOperator, ChosenOperator, OperatorProposals) where

import Core.CoreTypes
import Core.WorkingMemory
import Core.WorkingMemory.Reifiable
import Cosmolude
import Data.Either
import qualified Data.HashMap.Strict as HM
import qualified Data.HashSet as HS
import Data.HashSet.Lens
import Data.Sequence (Seq (..))
import qualified Data.Sequence as Seq
import Math.ParetoFront
import Model.PSCM.Impasses
import PSCM.Effects.ProductionSystem
import PSCM.Operators.OperatorProposals
import PSCM.Effects.ProblemSubstates
import PSCM.States (HasStateMap, Impasse, makeStateAugmentation)
import Polysemy.ConstraintAbsorber.MonadState
import Polysemy.Error
import Polysemy.Output
import Polysemy.Reader
import Polysemy.State
import Polysemy.Tagged
import Prelude (Int, head, (^))

-- anyAreRejected' :: (HasIdentifier b, Hashable b, Functor f, Foldable f, Member (State (a, HashSet b)) r) => f b -> Sem r Bool
-- anyAreRejected' objs = absorbState $ uses _2 (\rejects -> any ((flip HS.member) (HS.map (view identifier) rejects)) (view identifier <$> objs))

anyAreRejected :: (HasIdentifier b, Hashable b, Member (State (a, HashSet b)) r) => [b] -> Sem r (HashSet b)
anyAreRejected objs =
  let candidates = HM.fromList . (fmap (\a -> (a ^. identifier, a))) $ objs
   in absorbState
        $ uses
          _2
          ( \rejects' ->
              let rejects = HM.mapKeys (view identifier) $ HM.mapWithKey const $ HS.toMap rejects'
                  pairedUp = HM.intersectionWith (\a b -> (HS.singleton a) <> HS.singleton b) candidates rejects
               in mconcat (HM.elems pairedUp)
          )

-- foldMap (\candidate -> if HS.member (candidate^. rejects
-- any ((flip HS.member) (HS.map (view identifier) rejects)) (view identifier <$> objs))

differenceByID :: (HasIdentifier a, Hashable a, Foldable f, Foldable g) => f a -> g a -> HashSet a
differenceByID candidates rejections =
  let rejs = HS.fromList (rejections ^.. folded . identifier)
   in HS.fromList $ candidates ^.. folded . filtered (\candidate -> not ((candidate ^. identifier) `HS.member` rejs))

addCandidates :: (HasIdentifier a, Eq a, Hashable a, Foldable f, Member (State (HashSet a, HashSet a)) r) => f a -> Sem r ()
addCandidates objs = absorbState $ do
  rejects <- use _2
  _1 <>= (setOf folded objs) `differenceByID` rejects

addRejections :: (HasIdentifier a, Eq a, Hashable a, Foldable f, Member (State (HashSet a, HashSet a)) r) => f a -> Sem r ()
addRejections objs = absorbState $ do
  _1 %= (`differenceByID` setOf folded objs)
  _2 <>= setOf folded objs

data RequiredProhibitedPhase

data AcceptRejectPhase

data OverallResults

-- TODO IMPROVE
minimiseConflictSet :: -- (Hashable (OperatorProposal metadata)) => HashSet (OperatorProposal metadata) -> HashSet (OperatorProposal metadata)
  OperatorProposals metadata -> OperatorProposals metadata
minimiseConflictSet (OperatorProposals xs) = OperatorProposals $ foldMap stratum $ getFront . head . getStrata $ xs -- . foldMap stratum $  a <> b

doRequiredProhibitPhase :: (Hashable (OperatorProposal metadata), Members '[State (HashSet (OperatorProposal metadata), HashSet (OperatorProposal metadata)), Tagged RequiredProhibitedPhase (Output (OperatorProposal metadata)), Error (Impasse metadata)] r) => [OperatorProposal metadata] -> Sem r ()
doRequiredProhibitPhase front = absorbState
  $ do
    let prohibits = setOf (folded . filtered (has (wMPreference . _Prohibit))) front
    _1 .= setOf (folded . filtered (has (wMPreference . _Required))) front
    _2 .= prohibits
    s <- get
    when (has (_2 . folded) s) do
      rejected <- anyAreRejected (s ^.. _1 . folded)
      when (not $ HS.null rejected) do
        let conflictSet = rejected -- minimiseConflictSet (s ^. _1) (s ^. _2)
        throw (ConstraintFailure (ConstraintFailureImpasse (foldMap proposeOperator conflictSet)))
    forMOf_ (_1 . folded) s (tag @RequiredProhibitedPhase . output)

doAcceptableAndRejectPhase :: (Hashable (OperatorProposal metadata), Members '[State (HashSet (OperatorProposal metadata), HashSet (OperatorProposal metadata)), Tagged AcceptRejectPhase (Output (OperatorProposal metadata)), Error (Impasse metadata)] r) => [OperatorProposal metadata] -> [OperatorProposal metadata] -> Sem r ()
doAcceptableAndRejectPhase accepts rejects = absorbState
  $ do
    addRejections rejects
    addCandidates accepts
    s <- get
    forMOf_ (_1 . folded) s (tag @AcceptRejectPhase . output)

getBetterAndWorse :: OperatorProposal metadata -> Maybe (ObjectIdentifier, ObjectIdentifier)
getBetterAndWorse (OperatorProposal a (OtherIsPreferable b) _) = Just (b, a)
getBetterAndWorse (OperatorProposal a (PreferableOverOther b) _) = Just (a, b)
getBetterAndWorse _ = Nothing

data BetterWorsePhase

doBetterWorsePhase :: (Hashable (OperatorProposal metadata), Members '[State (HashSet (OperatorProposal metadata), HashSet (OperatorProposal metadata)), Tagged BetterWorsePhase (Output (OperatorProposal metadata)), Error (Impasse metadata)] r) => [OperatorProposal metadata] -> Sem r ()
doBetterWorsePhase bettersAndWorses = absorbState
  $ do
    let worses = bettersAndWorses ^.. folded . to getBetterAndWorse . _Just . _2
        betters = bettersAndWorses ^.. folded . to getBetterAndWorse . _Just . _1
    addRejections ((flip OperatorProposal) Reject <$> worses <*> pure Nothing)
    addCandidates ((flip OperatorProposal) Acceptable <$> betters <*> pure Nothing)
    s <- get
    forMOf_ (_1 . folded) s (tag @BetterWorsePhase . output)

data IndifferentPhase

getIndifferentPairs :: OperatorProposal metadata -> Maybe (ObjectIdentifier, ObjectIdentifier)
getIndifferentPairs (OperatorProposal a (IndifferentTo b) _) = Just (a, b)
getIndifferentPairs _ = Nothing

doIndifferentPhase :: (Hashable (OperatorProposal metadata), Members '[State (HashSet (OperatorProposal metadata), HashSet (OperatorProposal metadata)), Tagged IndifferentPhase (Output (OperatorProposal metadata)), Error (Impasse metadata)] r) => [OperatorProposal metadata] -> Sem r ()
doIndifferentPhase indifferents = absorbState do
  let plainIndifferents = setOf (folded . filtered (has (wMPreference . _Indifferent))) indifferents
  _1 .= plainIndifferents
  let numberOfRelations = lengthOf ((folded . to getIndifferentPairs . _Just) . to \(a, b) -> HS.fromList [(a, b), (b, a)]) indifferents
  when (numberOfRelations == (HS.size plainIndifferents) ^ (2 :: Int)) do
    forM_ plainIndifferents (tag @IndifferentPhase . output)
  throw (Tie $ TieImpasse (foldMap proposeOperator plainIndifferents))

data BestPhase

doBestPhase :: (Hashable (OperatorProposal metadata), Members '[State (HashSet (OperatorProposal metadata), HashSet (OperatorProposal metadata)), Tagged BestPhase (Output (OperatorProposal metadata)), Error (Impasse metadata)] r) => [OperatorProposal metadata] -> Sem r ()
doBestPhase bests = absorbState do
  _1 .= setOf folded bests
  forM_ bests (tag @BestPhase . output)

data WorstPhase

doWorstPhase :: (Hashable (OperatorProposal metadata), Members '[State (HashSet (OperatorProposal metadata), HashSet (OperatorProposal metadata)), Tagged WorstPhase (Output (OperatorProposal metadata)), Error (Impasse metadata)] r) => [OperatorProposal metadata] -> Sem r ()
doWorstPhase worsts = absorbState do
  addRejections worsts
  s <- get
  forMOf_ (_1 . folded) s (tag @WorstPhase . output)

getStrataPhaseDropping :: Int -> (WMPreference ObjectIdentifier -> Bool) -> Fold (OperatorProposals metadata) (OperatorProposal metadata)
getStrataPhaseDropping q f = dropping q (allProposals . to (quota (q + 1)) . to getStrata . folded . to getFront . folded) . filtered (\prop -> f (prop ^. wMPreference))

requiresAndProhibits :: Fold (OperatorProposals metadata) (OperatorProposal metadata)
requiresAndProhibits = allProposals . to (quota 1) . to getStrata . _head . to getFront . filtered (\op -> has (folded . wMPreference . _Required) op || has (folded . wMPreference . _Prohibit) op) . folded

data EvaluationDone = Done | NotDone deriving (Eq)

runEvaluationProcess ::
  ( Hashable (OperatorProposal metadata),
    HasTracer agent, 
    Members
      '[ Reader agent,
         OpenTelemetrySpan,
         OpenTelemetryContext,
         Resource,
         Log,
         State (HashSet (OperatorProposal metadata), HashSet (OperatorProposal metadata)),
         Tagged OverallResults (Output (OperatorProposal metadata)),
         Error (Impasse metadata),
         State EvaluationDone
       ]
      r
  ) =>
  OperatorProposals metadata ->
  Sem r ()
runEvaluationProcess props = absorbState @(_, _) . absorbState @EvaluationDone $ push "runEvaluationProcess" do
  let requiredAndProhibited = props ^.. requiresAndProhibits
  when (has folded requiredAndProhibited) do
    (requires, ()) <- runOutputList . untag @RequiredProhibitedPhase $ doRequiredProhibitPhase requiredAndProhibited
    case requires of
      [] -> pure ()
      [x] -> do
        put Done
        tag @OverallResults $ output x
      xs -> throw (ConstraintFailure (ConstraintFailureImpasse (foldMap proposeOperator xs)))

  let reqsCount = length requiredAndProhibited
      acceptables = props ^.. getStrataPhaseDropping reqsCount (has _Acceptable)
      accCount = length acceptables
      rejects = props ^.. getStrataPhaseDropping (reqsCount + accCount) (has _Reject)
  finishedAfterRequiredProhibited <- get
  when (finishedAfterRequiredProhibited == NotDone && has folded (acceptables <> rejects)) do
    (accepts', ()) <- runOutputList . untag @AcceptRejectPhase $ doAcceptableAndRejectPhase acceptables rejects
    case accepts' of
      [] -> throw (NoChange (OperatorNoChangeImpasse props))
      [x] -> do
        put Done
        tag @OverallResults $ output x
      _ -> do
        pure ()
  let rejsCount = length rejects
      bettersAndWorses = props ^.. getStrataPhaseDropping (reqsCount + accCount + rejsCount) (\prop -> has _PreferableOverOther prop || has _OtherIsPreferable prop)
  finishedAfterRejectPhase <- get
  when (finishedAfterRejectPhase == NotDone && has folded bettersAndWorses) do
    (betters', ()) <- runOutputList . untag @BetterWorsePhase $ doBetterWorsePhase bettersAndWorses
    case betters' of
      [] -> do
        let problematic = minimiseConflictSet (foldMap proposeOperator bettersAndWorses) -- (setOf (getStrataPhaseDropping (reqsCount + accCount + rejsCount) (\prop -> has _PreferableOverOther prop )) props) (setOf (getStrataPhaseDropping (reqsCount + accCount + rejsCount) (\prop ->  has _OtherIsPreferable prop)) props)
        throw (Conflict (ConflictImpasse problematic))
      [x] -> do
        put Done
        tag @OverallResults $ output x
      _ -> pure ()
  let bwCount = length bettersAndWorses
      bests = props ^.. getStrataPhaseDropping (reqsCount + accCount + rejsCount + bwCount) (has _Best)
  finishedAfterBetterWorsePhase <- get
  when (finishedAfterBetterWorsePhase == NotDone && has folded bests) do
    (bests', ()) <- runOutputList . untag @BestPhase $ doBestPhase bests
    case bests' of
      [] -> throw (NoChange (OperatorNoChangeImpasse props))
      [x] -> do
        put Done
        tag @OverallResults $ output x
      _ -> pure ()
  -- xs -> pass only candidates that are best --pure () --throw (Tie (TieImpasse (foldMap proposeOperator xs))) -- ?

  let bestCount = length bests
      worsts = props ^.. getStrataPhaseDropping (reqsCount + accCount + rejsCount + bwCount + bestCount) (has _Worst)
  finishedAfterBestPhase <- get
  when (finishedAfterBestPhase == NotDone && has folded worsts) do
    (worsts', ()) <- runOutputList . untag @WorstPhase $ doWorstPhase worsts
    case worsts' of
      [] -> throw (NoChange (OperatorNoChangeImpasse props))
      [x] -> do
        put Done
        tag @OverallResults $ output x
      _ -> pure ()
  --      xs -> pure ()--throw (Tie (TieImpasse (foldMap proposeOperator xs))) -- ?

  let worstCount = length worsts
      indifferents = props ^.. getStrataPhaseDropping (reqsCount + accCount + rejsCount + bwCount + bestCount + worstCount) (\op -> has _Indifferent op || has _IndifferentTo op)
  finishedAfterWorstPhase <- get
  when (finishedAfterWorstPhase == NotDone && has folded indifferents) do
    (indiffs', ()) <- runOutputList . untag @IndifferentPhase $ doIndifferentPhase indifferents
    case indiffs' of
      [] -> throw (NoChange (OperatorNoChangeImpasse props) )
      xs -> forM_ xs (tag @OverallResults . output)

{-# INLINEABLE evaluateOperators #-}
evaluateOperators :: (HasTracer agent,  Hashable (OperatorProposal metadata), Members '[Input StateIdentifier, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, Log] r) => OperatorProposals metadata -> Sem r (Either (Impasse metadata) (ChosenOperator metadata))
evaluateOperators proposals = do
  runError
  . evalState (HS.empty :: HashSet (OperatorProposal metadata), HS.empty :: HashSet (OperatorProposal metadata))
  . evalState NotDone
  $ do
    (res :: Seq (OperatorProposal metadata), ()) <- (runOutputMonoid (Seq.singleton)) . untag @OverallResults $ runEvaluationProcess proposals
    case res of
      Seq.Empty -> throw (NoChange (OperatorNoChangeImpasse proposals))
      chosen :<| Seq.Empty -> do
        ident <- input
        pure (ChosenOperator (chosen ^. proposedOperator) proposals ident)
      multiple -> throw (Tie (TieImpasse (foldMap proposeOperator multiple)))

{-# INLINEABLE applyChosenOperator #-}
applyChosenOperator :: forall agent metadata r. (HasMaybeWMPreference metadata ObjectIdentifier, ReifiableToWMEs (WME' metadata) metadata, HasCreatingState metadata, CreateBlankWMEMetadata metadata, HasWMESupport metadata, HasStateMap agent metadata, Members '[Log, Transactional, WorkingMemory' metadata, Reader agent, ProblemSubstates metadata, ProductionSystem' metadata] r) => ChosenOperator metadata -> Sem r ()
applyChosenOperator (ChosenOperator chosen proposals sid) = do
  currentOp <- currentOperator sid
  newWMEToAdd <- makeStateAugmentation sid (operatorSymbol ^. re _Symbol) (chosen ^. re (_SymbolicValue . _ObjectIdentity))

  let wmesToRemove = (proposals ^.. allProposals . to getStrata . folded . to getFront . folded . relevantWME . _Just) <> (currentOp ^.. folded)

  addAndRemoveElements [newWMEToAdd] wmesToRemove
