module PSCM.ProductionSystem.ActivityIdentification (type MetadataAndProofConstraints, stratifyActivity, identifySubstateMatchAppliesTo) where  

import Control.Lens.Action
import Core.CoreTypes
import Core.Patterns.PatternSyntax
import Core.Rule
import Core.Rule.Database
import Core.Rule.Match
import Core.WorkingMemory
import Cosmolude hiding (empty, id, note, (.))
import qualified Data.Maybe as UNSAFE (fromJust)
import qualified Data.Text as T
import Development.DomainModelling hiding (Output)
import qualified OpenTelemetry.Trace.Core as OT

import PSCM.States as States
import PSCM.Errors    
import Polysemy.Error
import Polysemy.Reader
import qualified Prelude (error)



type MetadataAndProofConstraints metadata proof = (HasWMESupport metadata, WMEIsState metadata, WMETruthValue (WME' metadata), HasMaybeWMPreference metadata ObjectIdentifier, Renderable metadata, Show metadata, Typeable metadata, Typeable proof, Show proof, Renderable metadata, Renderable proof, CreateBlankWMEMetadata metadata, HasAccumulatedTruth proof Variable, HasExtraObjectIdentifiers metadata)

{-# INLINEABLE identifySubstateMatchAppliesTo #-}

-- | Determine which state a rule matched against, as well as the depth of that state.
identifySubstateMatchAppliesTo ::
  ( Typeable metadata, HasStateMap agent metadata, Typeable proof,
    HasTracer agent,
    --    HasReteEnvironment agent metadata,
    Show metadata,
    Show proof,
    Renderable metadata,
    Renderable proof,
    HasWorkingMemoryData agent metadata,
    Members
      '[ Reader agent,
         GlobalContextID,
         OpenTelemetrySpan,
         OpenTelemetryContext,
         WorkingMemory' metadata,
         RuleDatabase,
         Resource,
         Log,
         IDGen,
         Transactional,
         Error (SubstateIdentificationException metadata proof)
       ]
      r
  ) =>
  RuleMatch' metadata proof ->
  Sem r (StateIdentifier, StateOrdering)
identifySubstateMatchAppliesTo match = onException (push "IdentifySubstateMatchAppliesTo" do
  stateMap <- ask <&> view States.stateMap
  --  let normalised = (fst (Matcher.productionConditionMap (match ^? rule))) -- for attributes
  addAttributes
    [ ("rule.id", toAttribute (match ^. rule . identifier))
    --    ,
    --      ("pscm.substate.identification.raw_match", toAttribute (T.pack $ show match)),
    --      ("pscm.substate.identification.rule.variables", toAttribute (T.pack $ show . render $ (normalised ^.. variables))),
    --      ("pscm.substate.identification.match", toAttribute (T.pack $ show $ render match)),
    --      ("pscm.substate.identification.raw_rule", toAttribute (T.pack $ show $ match ^. rule)),
    --      ("pscm.substate.identification.normalised_rule", toAttribute (T.pack $ show normalised)),
    --      ("pscm.substate.identification.nice_rule", toAttribute (T.pack $ show $ render normalised)),
    --      ("pscm.substate.identification.unification_substitution", toAttribute (T.pack $ show $ render $ match ^. substitution)),
    --      ("pscm.substate.identification.raw_unification_substitution", toAttribute (T.pack $ show $ match ^. substitution))
    ]
  stateVar <- case match ^? rule . stateTestingPattern . fusing (patternObject . patVariable . variable) of
    Nothing -> do
      setSpanStatus (OT.Error "Missing state variable")
      throw (SubstateIdentificationException MissingStateVariableInRule match)
      Prelude.error ("Missing state variable")
    Just sv -> pure sv
  addAttributes [("pscm.substate.identification.state_variable", toAttribute (T.pack $ show stateVar))]
  substituted <- case match ^? substitution . at stateVar . _Just of
    Nothing -> do
      setSpanStatus (OT.Error "Missing state variable in unification substitution")
      throw (SubstateIdentificationException MissingStateVariableInSubstitution match)
      Prelude.error ("Missing state variable in unification substitution")
    Just s -> pure s
  addAttributes [("pscm.substate.identification.unification_substitution.at_state_var", toAttribute (T.pack $ show $ substituted))]

  objectID <- case substituted ^? symbolicValue . Core.CoreTypes.oid of
    Nothing -> do
      setSpanStatus (OT.Error "Missing match")
      throw (SubstateIdentificationException MissingMatchedObject match)
      Prelude.error ("Missing match")
    Just o -> pure o
  statesByObjects <- stateMap ^!! statesByObject . elemsM
  addAttributes
    [ ("pscm.substate.oid", toAttribute (T.pack $ show $ objectID)),
      ("pscm.substate.identification.states_by_object", toAttribute (T.pack $ show statesByObjects))
    ]
  ident' <- stateMap ^! statesByObject . act (lookupMap objectID)
  ident <- case ident' of
    Nothing -> do
      setSpanStatus (OT.Error "State identity not found")
      let exc = (SubstateIdentificationException (StateIdentityNotFound objectID statesByObjects) match)
      recordException exc [("pscm.substate.oid", toAttribute (T.pack $ show $ objectID)),      ("pscm.substate.identification.states_by_object", toAttribute ((fmap (T.pack . show) statesByObjects)))]
      throw exc
      Prelude.error "State identity not found"
    Just i -> pure i
  ordering <- UNSAFE.fromJust <$> stateMap ^! orderingsByState . act (lookupMap ident)
  pure (ident, ordering))
  ( forceFlushGlobalTracerProvider Nothing 
  )


{-# INLINEABLE stratifyActivity #-}
-- | Sorts through the given rule activity and partitions it according to the state, such that superstates will be processed before substates.
stratifyActivity ::
  ( HasStateMap agent metadata,
    HasTracer agent,
    HasPendingActivity agent metadata proof,
    MetadataAndProofConstraints metadata proof,
    HasWorkingMemoryData agent metadata,
    Members
      '[ Reader agent,
         Error (SubstateIdentificationException metadata proof),
         GlobalContextID,
         WorkingMemory' metadata,
         RuleDatabase,
         OpenTelemetrySpan,
         OpenTelemetryContext,
         Resource,
         Log,
         IDGen,
         Transactional
       ]
      r
  ) =>
  HashSet (RuleActivation' metadata proof) ->
  HashSet (RuleRetraction' metadata proof) ->
  Sem r ()
stratifyActivity activations retractions = do
  pending <- ask <&> view pendingActivity

  push "activations" $ forM_ activations \activation@(RuleActivation match) -> do
    (ident, depth) <- identifySubstateMatchAppliesTo match
    pending ^! pendingActivationsBySubstate . insertingAt ident depth activation
  push "retractions" $ forM_ retractions \retraction@(RuleRetraction match) -> do
    when (hasn't (rule . operatorSupported ) match) do
      (ident, depth) <- identifySubstateMatchAppliesTo match
      pending ^! pendingRetractionsBySubstate . insertingAt ident depth retraction

    
