module PSCM.ProductionSystem.PerformActivity (processActivity, type ActivityPerformanceConstraints) where

import Algebra.PartialOrd
import Control.Exception (Exception)
import Control.Lens.Action
import Core.Action
import Core.CoreTypes
import Core.Patterns.PatternSyntax
import Core.Rule
import Core.Rule.Match
import Core.Truth.Model
import Core.WorkingMemory
import Cosmolude hiding (empty, id, note, (.))
import qualified Data.HashMap.Strict as HM
import Data.HashPSQ
import qualified Data.HashSet as HS
import qualified Data.Text as T
import Development.DomainModelling hiding (Output)
import PSCM.Configuration
import PSCM.Effects.ProblemSubstates
import PSCM.Errors (TestOutcome (..), WithTestOutcome (..))
import PSCM.ProductionSystem.ActivityIdentification
import PSCM.States as States
import Polysemy.Error
import Polysemy.Reader
import Prettyprinter.Render.String
import System.FilePath
import System.IO
import System.Process.Typed
import qualified Prelude (error)

{-# INLINEABLE addingWME #-}

-- | We will be adding a WME to working memory.
addingWME ::
  (HasTracer agent, HasWMESupport metadata, HasMaybeWMPreference metadata ObjectIdentifier, CreateBlankWMEMetadata metadata, HasPendingActivity agent metadata proof, Members '[Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, Log, Transactional, IDGen, WorkingMemory' metadata, KeyedState PSCMConfiguration] r) =>
  StateOrdering ->
  RuleIdentifier ->
  InstantiationIdentifier ->
  Maybe ObjectIdentifier ->
  StateIdentifier ->
  Substitution Value ->
  WMEPattern ->
  WMETruth ->
  RuleMatch' metadata proof ->
  Sem r ()
addingWME depth ruleID instantiationID operatorID stateIdent substitution pat truth _match = push "addingWME" do
  agent <- ask
  let pending = agent^.pendingActivity
  wmeSupport' <- case operatorID of
    Nothing -> pure $ RuleSupported (HS.singleton instantiationID) HS.empty
    Just op -> do
      -- TODO FIXME add dependencies to GDS
      pure $ RuleSupported (HS.singleton instantiationID) (HS.singleton op)
  -- Create (or retrieve an existing) WME from a pattern.
  addAttributes
    [ ("pscm.activity.wmes.addition.pattern", toAttribute (T.pack $ show . render $ pat)),
      ("pscm.activity.wmes.addition.substitution", toAttribute (T.pack $ show . render $ substitution))
    ]
  wme' <- runError $ createWMEFromPattern ruleID (substitution) pat
  case wme' of
    Left exc -> do
      recordException exc [("exception.description", "WME creation error")]
      error "WME creation error" ([] :: [(T.Text, Attribute)])
    Right !wme -> do
      wme ^! metadata . wmeSupport . act (modifyT ((<>) wmeSupport'))
      pending ^! pendingWMEInsertionsBySubstate . insertingAtWith truth (\/) stateIdent depth wme

--      case
{-# INLINEABLE removingWME #-}
removingWME ::
  (HasTracer agent, HasMaybeWMPreference metadata ObjectIdentifier, HasWMESupport metadata, CreateBlankWMEMetadata metadata, HasPendingActivity agent metadata proof, Members '[Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, Transactional, IDGen, WorkingMemory' metadata, Log] r) =>
  StateOrdering ->
  WMEPattern ->
  RuleIdentifier ->
  StateIdentifier ->
  Substitution Value ->
  WMETruth ->
  RuleMatch' metadata proof ->
  Sem r ()
removingWME depth pat ruleID stateIdent substitution truth _match = do
  agent <- ask
  let pending = agent^.pendingActivity                                                                                 
  addAttributes
    [ ("pscm.activity.wmes.removal.pattern", toAttribute (T.pack $ show . render $ pat)),
      ("pscm.activity.wmes.removal.substitution", toAttribute (T.pack $ show . render $ substitution))
    ]
  -- Create or retrieve an existing WME from a pattern.
  wme' <- runError $ createWMEFromPattern ruleID (substitution) pat
  case wme' of
    Left exc -> do
      recordException exc [("exception.description", "WME creation error")]
      error "WME creation error" ([] :: [(T.Text, Attribute)])
    Right !wme -> do
      pending ^! pendingWMEDeletionsBySubstate . insertingAtWith truth (/\) stateIdent depth wme


-- instantiateObjectReference rule field variable = do
--     case sub ^? at objectVar . _Just . _SymbolicValue . _ObjectIdentity of
--       Nothing -> do
--         fresh <- createFreshObjectIDBasedOnVariable ruleID objectVar
--         note (MalformedWMEPattern (Right O) pat sub) fresh
--       Just o' -> pure o'
{-# INLINEABLE createWMEFromPattern #-}
createWMEFromPattern :: (HasTracer agent,CreateBlankWMEMetadata metadata, HasMaybeWMPreference metadata ObjectIdentifier, HasCallStack, Members '[Reader agent, Resource, Log, OpenTelemetryContext, OpenTelemetrySpan, WorkingMemory' metadata, IDGen, Transactional, Error MalformedWMEPattern] r) => RuleIdentifier -> Substitution Value -> WMEPattern -> Sem r (WME' metadata)
createWMEFromPattern ruleID sub pat = push "createWMEFromPattern" do
  addAttributes
    [ ("rule.id", toAttribute ruleID),
      ("wme.creation.pattern", toAttribute pat)
    ]
  exists <- findWMEsByPattern pat
  case exists ^? _head of
    Just existing -> pure existing
    Nothing -> do
      metadata <-
        ( do
            metadata <- embedSTM createBlankWMEMetadata
            substituted <- case pat^.patternPreference of
                             WithAPreference p -> forMOf (_Just . traversed) (Just p) (\var -> do
                                                                      let (found) = sub ^? at var . _Just . _ObjectIdentity
                                                                      note (MalformedWMEPattern (Left MetadataField) pat sub) found)
                             _ -> pure Nothing

            -- (substituted) <-
            --   forMOf
            --   (_Just . traversed)
            --     (pat ^. patternPreference)
            --     ( \var -> do
            --         let (found) = sub ^? at var . _Just . _ObjectIdentity

            --         note (MalformedWMEPattern (Left MetadataField) pat sub) found
            --     )
            pure (metadata & preference .~ substituted)
          )

      o <- case pat ^? patternObject . _AnObjectIdentity of
        Nothing -> do
          let (patObjectVariable :: Maybe Variable) = pat ^? patternObject . _APatternVariable . _1 . variable

          objectVar <- note (MalformedWMEPattern (Right O) pat sub) patObjectVariable
          case sub ^? at objectVar . _Just . _SymbolicValue . _ObjectIdentity of
            Nothing -> do
              fresh <- createFreshObjectIDBasedOnVariable ruleID objectVar
              note (MalformedWMEPattern (Right O) pat sub) fresh
            Just o' -> pure o'
        Just o' -> pure o'
      a <- case pat ^. patternAttribute of
        AnObjectIdentity i -> pure $ ObjectIdentity i
        AConstantEqualityTest (PatternSymbol s) -> pure $ Symbol s
        APatternVariable p _ | Just a <- sub ^? at (p ^. variable) . _Just . _SymbolicValue -> pure a
        _ -> do
          error
            "Invalid Attribute in WME pattern!"
            ( (makeAttributesWithRaw "wme.creation.pattern" pat)
                <> (makeAttributesWithRaw "wme.creation.substitutions" sub)
            )
          throw $ MalformedWMEPattern (Right A) pat sub
      v <- case pat ^. patternValue of
        AnObjectIdentity i -> pure $ Symbolic (ObjectIdentity i)
        AConstantEqualityTest (PatternSymbol s) -> pure $ Symbolic (Symbol s)
        AConstantEqualityTest (PatternConstant v) -> pure $ Constant v
        APatternVariable p _ | Just v <- sub ^? at (p ^. variable) . _Just -> pure v
        _ -> do
          error
            "Invalid Value in WME pattern!"
            ( (makeAttributesWithRaw "wme.creation.pattern" pat)
                <> (makeAttributesWithRaw "wme.creation.substitutions" sub)
            )
          throw $ MalformedWMEPattern (Right V) pat sub
      let pref = metadata^.preference
      ident <- contentAddressableIdentifier (WMEContentKey o a v pref)
      pure (WME o a v ident metadata)

data MetadataField = MetadataField deriving (Eq, Show)

data MalformedWMEPattern = MalformedWMEPattern (Either MetadataField WMEField) WMEPattern (Substitution Value) deriving (Eq, Show, Exception)

{-# INLINE computeOperatorSupport #-}

-- | If the rule tests an operator, determine which one.
computeOperatorSupport :: RuleMatch' metadata proof -> (Maybe ObjectIdentifier)
computeOperatorSupport match = do
  opVar <- match ^? rule . operatorSupported . _Just . patternValue . patVariable . variable
  match ^? substitution . at opVar . _Just . _ObjectIdentity

renderToFile :: (HasCallStack) => (Members '[Embed IO] r) => MessageDestination -> CoreDocStream -> Sem r ()
renderToFile (File _path h) msg = do
  handle <- case h of
    Just h' -> pure h'
    Nothing -> Prelude.error "Ugh fix this using a KV store mapping paths to h andles "
  embed $ hPutStrLn handle (renderString msg)

executeProcess :: (Members '[Embed IO] r) => FilePath -> [ProcessArgument] -> Sem r ()
executeProcess path args = do
  let config = System.Process.Typed.proc path args
  void $ startProcess config

{-# INLINEABLE instantiateActivation #-}

-- | Determine the effects of a rule activating
instantiateActivation :: (HasTracer agent, WithTestOutcome agent, MetadataAndProofConstraints metadata proof, HasPendingActivity agent metadata proof, Members '[OpenTelemetrySpan, OpenTelemetryContext, Resource, ProblemSubstates metadata, Embed IO, Log, VariableMapping ConcreteTruth, KeyedState PSCMConfiguration, Transactional, WorkingMemory' metadata, IDGen, Reader agent] r) => StateIdentifier -> StateOrdering -> RuleActivation' metadata proof -> WMETruth -> Sem r ()
instantiateActivation ident depth (RuleActivation match) strength = pushConsumer "instantiateActivation" do
  -- compute operator if necessary, remembering to take into account DontCareCondition
  let operatorID = computeOperatorSupport match
  --
  trueStrength <- computeTruth strength
  cutoff <- getAt Don'tActivateRulesWithTruthBelow
  addAttributes [("rule.activation.strength", toAttribute trueStrength), ("rule.activation.cutoff", toAttribute cutoff)]
  when (cutoff `leq` trueStrength) $ do
    addEvent "Rule activation" [("rule.id", toAttribute (match ^. rule . identifier)), ("rule.activation.id", toAttribute (match ^. identifier)), ("pscm.state.id", toAttribute ident)]
--    pending <- ask <&> view pendingActivity
    forMOf_ (rule . ruleRHS . actions . folded) match \case
      NewWME !pat -> addingWME  depth (match ^. rule . ruleIdentifier) (match ^. instantiationID) operatorID ident (match ^. substitution) pat (match ^. matchingToken . accumulatedTruth) match
      Core.Action.RemoveWME !pat -> removingWME  depth pat (match ^. rule . ruleIdentifier) ident (match ^. substitution) (match ^. matchingToken . accumulatedTruth) match
      Core.Action.Log !when' !level !msg | executeOnActivation when' -> log level . fromString . renderString $ msg
      Core.Action.Log _ _ _ -> pure ()
      Output when' dest msg | executeOnActivation when' -> renderToFile dest msg
      Output _ _ _ -> pure ()
      RunProcess when' path args | executeOnActivation when' -> executeProcess path args
      RunProcess _ _ _ -> pure ()
      SetQuiescenceFuelForCurrentState fuel -> do
        setStateQuiescenceFuel fuel
      TestSuccess when'
        | executeOnActivation when' ->
            do
              agent <- ask
              writeT TestWasSuccessful (agent ^. testOutcome)
        | otherwise -> pure ()
      TestFailure when' msg
        | executeOnActivation when' ->
            do
              agent <- ask
              writeT (TestFailed msg) (agent ^. testOutcome)
        | otherwise -> pure ()

{-# INLINEABLE instantiateRetraction #-}

-- | Determine the effects of a rule retracting
instantiateRetraction :: (HasTracer agent,WithTestOutcome agent, HasWMESupport metadata, CreateBlankWMEMetadata metadata, HasMaybeWMPreference metadata ObjectIdentifier, HasAccumulatedTruth proof Variable, HasPendingActivity agent metadata proof, Members '[ OpenTelemetrySpan, OpenTelemetryContext, Resource, Log, Embed IO, VariableMapping ConcreteTruth, KeyedState PSCMConfiguration, Transactional, WorkingMemory' metadata, IDGen, Reader agent] r) => StateIdentifier -> StateOrdering -> RuleRetraction' metadata proof -> WMETruth -> Sem r ()
instantiateRetraction ident depth (RuleRetraction match) strength = pushConsumer "instantiateRetraction" $ do
  -- compute operator if necessary, remembering to take into account DontCareCondition
  let operatorID = computeOperatorSupport match
  --
  trueStrength <- computeTruth strength
  cutoff <- getAt Don'tActivateRulesWithTruthBelow
  addAttributes [("rule.retraction.strength", toAttribute trueStrength), ("rule.retraction.cutoff", toAttribute cutoff)]
  let operatorSupported = isJust operatorID
  when (cutoff `leq` trueStrength) do
    addEvent "Rule retraction" [("rule.id", toAttribute (match ^. rule . identifier)), ("rule.retraction.id", toAttribute (match ^. identifier)), ("pscm.state.id", toAttribute ident)]
--    pending <- ask <&> view pendingActivity
    forMOf_ (rule . ruleRHS . actions . folded) match \case
      NewWME !pat -> when (not operatorSupported) do
        removingWME  depth pat (match ^. rule . ruleIdentifier) ident (match ^. substitution) (match ^. matchingToken . accumulatedTruth) match
      Core.Action.RemoveWME !pat -> when (not operatorSupported) do
        -- Don't remove O-supported WMEs by retraction!
        addingWME depth (match ^. rule . ruleIdentifier) (match ^. instantiationID) operatorID ident (match ^. substitution) pat (match ^. matchingToken . accumulatedTruth) match
      Core.Action.Log !when' !level !msg | executeOnRetraction when' -> log level . fromString . renderString $ msg
      Core.Action.Log _ _ _ -> pure ()
      Output when' dest msg | executeOnRetraction when' -> renderToFile dest msg
      Output _ _ _ -> pure ()
      RunProcess when' path args | executeOnRetraction when' -> executeProcess path args
      RunProcess _ _ _ -> pure ()
      SetQuiescenceFuelForCurrentState _ -> pure ()
      TestSuccess when'
        | executeOnRetraction when' ->
            do
              agent <- ask
              writeT TestWasSuccessful (agent ^. testOutcome)
        | otherwise -> pure ()
      TestFailure when' msg
        | executeOnRetraction when' ->
            do
              agent <- ask
              writeT (TestFailed msg) (agent ^. testOutcome)
        | otherwise -> pure ()

data ProcessActivityType = Activations | Retractions deriving (Show)

instance ToPrimitiveAttribute ProcessActivityType where
  toPrimitiveAttribute Activations = "Activations"
  toPrimitiveAttribute Retractions = "Retractions"

instance ToAttribute ProcessActivityType

{-# INLINEABLE processActivitySet #-}
processActivitySet ::
  ( Hashable a, HasTracer agent,
    Members '[Reader agent, Log, OpenTelemetrySpan, OpenTelemetryContext, Resource, Transactional] r
  ) =>
  ProcessActivityType ->
  PendingActivity metadata proof ->
  OrderedActivity a ->
  StateOrdering ->
  (StateIdentifier -> StateOrdering -> a -> WMETruth -> Sem r ()) ->
  Lens' (PendingActivity metadata proof) (TMVar (HashPSQ StateIdentifier StateOrdering (HashMap a WMETruth))) ->
  Sem r ()
processActivitySet !activityType !pending !pendingSet !depth !action !optic = push "processActivitySet" do
  addAttributes [("pscm.activity.type", toAttribute [activityType])]
  let (listOfActivitySets, remainingPending) = atMostView depth pendingSet
  processOperation (/\) action listOfActivitySets
  putTM remainingPending (pending ^. optic)
  where
    foldActivity operator l = foldBy (HM.unionWith operator) HM.empty $! fmap (\(!i, !o, !s) -> HM.mapKeys (i,o,) s) $ l
    performInstantiations activity op = (flip HM.traverseWithKey) activity \(!ident, !ordering, !activityToDo) strength -> op ident ordering activityToDo strength
    processOperation operator action l = push "processOperation" do
      let !activity = foldActivity operator l
      !_ <- performInstantiations activity action
      pure ()

type ActivityPerformanceConstraints agent metadata proof r =
  ( WMEIsState metadata,
    WMETruthValue (WME' metadata),
    HasTracer agent,
    Typeable metadata,
    Typeable proof,
    Show metadata,
    Show proof,
    Renderable metadata,
    Renderable proof,
    HasExtraObjectIdentifiers metadata,
    WithTestOutcome agent,
    HasWMESupport metadata,
    CreateBlankWMEMetadata metadata,
    HasMaybeWMPreference metadata ObjectIdentifier,
    HasAccumulatedTruth proof Variable,
    HasPendingActivity agent metadata proof,
    Members
      '[ ProblemSubstates metadata,
         OpenTelemetrySpan,
         OpenTelemetryContext,
         Resource,
         Log,
         Embed IO,
         VariableMapping ConcreteTruth,
         KeyedState PSCMConfiguration,
         Transactional,
         WorkingMemory' metadata,
         IDGen,
         Reader agent
       ]
      r
  )

{-# INLINEABLE processActivity #-}
processActivity :: (ActivityPerformanceConstraints agent metadata proof r) => agent -> Sem r ()
processActivity agent | pending <- agent ^. pendingActivity = push "processActivity" $ do
  pa <- takeTM (pending ^. pendingActivationsBySubstate)
  pr <- takeTM (pending ^. pendingRetractionsBySubstate)
  let minActivations = findMin pa
      minRetractions = findMin pr
  case (minActivations, minRetractions) of
    ( Just (_aIdent, !aDepth, _adds),
      Just (_rIdent, !rDepth, _deletes)
      ) -> do
        -- Process from all sibling states at the highest priority at once!
        let depthToGrab = min aDepth rDepth
        addAttributes [("pscm.activity.depth", toAttribute depthToGrab), ("pscm.activity.type", toAttribute [("Activations" :: T.Text), "Retractions"])]
        processActivitySet Activations pending pa depthToGrab instantiateActivation pendingActivationsBySubstate
        processActivitySet Retractions pending pr depthToGrab instantiateRetraction pendingRetractionsBySubstate
    ( Just (_aIdent, !aDepth, _adds),
      Nothing
      ) -> do
        addAttributes [("pscm.activity.depth", toAttribute aDepth), ("pscm.activity.type", toAttribute ["Activations" :: T.Text])]
        processActivitySet Activations pending pa aDepth instantiateActivation pendingActivationsBySubstate
        putTM pr (pending ^. pendingRetractionsBySubstate)
    ( Nothing,
      Just (_rIdent, !rDepth, _deletes)
      ) -> push "Just retractions" do
        addAttributes [("pscm.activity.depth", toAttribute rDepth), ("pscm.activity.type", toAttribute ["Retractions" :: T.Text])]
        processActivitySet Retractions pending pr rDepth instantiateRetraction pendingRetractionsBySubstate
        putTM pa (pending ^. pendingActivationsBySubstate)
    (Nothing, Nothing) -> push "No activity" do
      putTM pa (pending ^. pendingActivationsBySubstate)
      putTM pr (pending ^. pendingRetractionsBySubstate)
