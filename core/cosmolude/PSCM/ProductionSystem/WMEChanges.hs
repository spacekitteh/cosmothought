module PSCM.ProductionSystem.WMEChanges (applyWMChanges, getNextWMChangesAndReset, deleteWMEs, type MetadataAndProofConstraints ) where

import Algebra.PartialOrd




import Core.CoreTypes


import Core.Rule.Database


import Core.Truth.Model
import Core.WorkingMemory

import Cosmolude hiding (empty, id, note, (.))
import qualified Data.HashMap.Strict as HM

import Data.HashPSQ
import qualified Data.HashSet as HS




import Development.DomainModelling hiding (Output)



import PSCM.Effects.RuleMatcher 

import PSCM.Configuration





import PSCM.Errors
import PSCM.ProductionSystem.ActivityIdentification
import PSCM.States as States
-- import Polysemy.Embed (Embed, embed)
import Polysemy.Error
import Polysemy.Reader








-- | Gets the highest priority rule activity to process
--
--
-- TODO FIXME: TODO FIXME: Figure out how to deal with fuel exhaustion of superstates when there are child states which haven't reached quiescence.
{-# INLINEABLE getNextWMChangesAndReset #-}
getNextWMChangesAndReset :: (HasTracer agent, HasPendingActivity agent metadata proof, Members '[Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Resource, Transactional, Log] r) =>  Sem r (Maybe (StateIdentifier, HashMap (WME' metadata) WMETruth, HashMap (WME' metadata) WMETruth))
getNextWMChangesAndReset  = push "getNextWMChangesAndReset" do
  pending <- ask <&> view pendingActivity                            
  pa <- takeTM (pending ^. pendingWMEInsertionsBySubstate)
  pr <- takeTM (pending ^. pendingWMEDeletionsBySubstate)

  -- TODO FIXME =======================================================================================================================
  -- TODO FIXME
  -- TODO FIXME
  -- ensure we don't retract something that is operator supported when we are just retracting due to an instantiation-caused retraction
  -- TODO FIXME
  -- TODO FIXME
  -- TODO FIXME =======================================================================================================================

  let minAdditions = findMin pa
      minRemovals = findMin pr
  case (minAdditions, minRemovals) of
    ( Just (aIdent, aDepth, _adds),
      Just (rIdent, rDepth, _deletes)
      ) -> do
        -- Process from all sibling states at the highest priority at once!
        let depthToGrab = min aDepth rDepth
            stateIdent = if aDepth <= rDepth then aIdent else rIdent
            (listOfAdditionSets, remainingPendingAdditions) = atMostView depthToGrab pa
            (listOfRemovalSets, remainingPendingRemovals) = atMostView depthToGrab pr
        --     states = (HS.fromList (fmap (view _1) listOfAdditionSets)) <> HS.fromList (fmap (view _1) listOfRemovalSets)

        -- statesToKeep' <- (flip HM.traverseWithKey) (HS.toMap states) \state () -> do
        --   res <- useFuel state
        --   pure (state, res)
        -- let statesToKeep = setOf (statesToKeep' . folded . filtered snd . _1) statesToKeep'
        --     filteredListOfAdditionSets = listOfAdditionSets ^..folded . filtered (\(ident, _, _) -> HS.member ident statesToKeep)
        --     filteredListOfRemovalSets = listOfRemovalSets ^..folded . filtered (\(ident, _, _) -> HS.member ident statesToKeep)

        let additions = foldBy (HM.unionWith (\/)) HM.empty $ fmap (\(_, _, wmes) -> wmes) $ listOfAdditionSets -- filteredListOfAdditionSets
            removals = foldBy (HM.unionWith (/\)) HM.empty $ fmap (\(_, _, wmes) -> wmes) $ listOfRemovalSets -- filteredListOfRemovalSets
        putTM remainingPendingAdditions (pending ^. pendingWMEInsertionsBySubstate)
        putTM remainingPendingRemovals (pending ^. pendingWMEDeletionsBySubstate)
        pure $! Just (stateIdent, additions, removals)
    ( Just (aIdent, aDepth, _adds),
      Nothing
      ) -> do
        let (listOfAdditionSets, remainingPendingAdditions) = atMostView aDepth pa
            --          states = HS.fromList ( fmap (view _1) listOfAdditionSets)
            additions = foldBy (HM.unionWith (\/)) HM.empty $ fmap (\(_, _, wmes) -> wmes) $ listOfAdditionSets -- filteredListOfAdditionSets
        putTM remainingPendingAdditions (pending ^. pendingWMEInsertionsBySubstate)
        putTM pr (pending ^. pendingWMEDeletionsBySubstate)
        pure $! Just (aIdent, additions, HM.empty)
    ( Nothing,
      Just (rIdent,rDepth, _deletes)
      ) -> do
        let (listOfRemovalSets, remainingPendingRemovals) = atMostView rDepth pr
            removals = foldBy (HM.unionWith (/\)) HM.empty $ fmap (\(_, _, wmes) -> wmes) $ listOfRemovalSets -- filteredListOfRemovalSets
            --            states = HS.fromList $ fmap (view _1) listOfRemovalSets
        putTM pa (pending ^. pendingWMEInsertionsBySubstate)
        putTM remainingPendingRemovals (pending ^. pendingWMEDeletionsBySubstate)
        pure $! Just (rIdent, HM.empty, removals)
    (Nothing, Nothing) -> do
      putTM pa (pending ^. pendingWMEInsertionsBySubstate)
      putTM pr (pending ^. pendingWMEDeletionsBySubstate)
      pure Nothing

{-# INLINEABLE applyWMChanges #-}
-- | Apply the changes to working memory, collecting the new rule activations and retractions, then stratify
-- them.
applyWMChanges :: (MetadataAndProofConstraints metadata proof, HasWorkingMemoryData agent metadata, HasPendingActivity agent metadata proof, HasStateMap agent metadata,HasTracer agent, Members '[RuleMatcher metadata matchComponents proof, GlobalContextID,  OpenTelemetrySpan, OpenTelemetryContext, Resource, VariableMapping ConcreteTruth, KeyedState PSCMConfiguration, Transactional, WorkingMemory' metadata, RuleDatabase, Log, IDGen, Reader agent,  Error (SubstateIdentificationException metadata proof)] r) => HashMap (WME' metadata) WMETruth -> HashMap (WME' metadata) WMETruth -> Sem r ()
applyWMChanges possibleAdditions possibleRemovals = push "applyWMChanges" do
  -- Combine map value WMETruths with any existing WME's truth value
  cutoff <- getAt DropWMEsWithTruthBelow

  -- Additions
  toAdd' <- (flip HM.traverseWithKey) possibleAdditions \wme strength -> do
    currentStrength <- readT (wme ^. truthValue)
    let abstractStrength = currentStrength \/ strength
    concreteStrength <- computeTruth $ abstractStrength
    pure (wme, concreteStrength, abstractStrength)

  let toAdd = toAdd' ^.. folded . filtered (\(_wme, strength, _abstract) -> cutoff `leq` strength) . to (\(wme, _concreteStrength, strength) -> (wme, strength))
  addWMEsToWM toAdd -- WorkingMemory
  (newActivationsFromAddition, newRetractionsFromAddition) <- addWMEs (toAdd ^.. folded . _1) -- Rete

  -- Removals
  toRemove' <- (flip HM.traverseWithKey) possibleRemovals \wme strength -> do
    currentStrength <- readT (wme ^. truthValue)
    let !abstractStrength = currentStrength /\ strength
    concreteStrength <- computeTruth $ abstractStrength
    when (concreteStrength >= cutoff) do
      writeT abstractStrength (wme ^. truthValue)
    pure (wme, concreteStrength)

  let !toRemove = toRemove' ^.. folded . filtered (\(_wme, strength) -> cutoff `leq` strength) . _1
  (newActivationsFromRemoval, newRetractionsFromRemoval) <- removeWMEs toRemove -- Rete
  removeWMEsFromWM toRemove -- WorkingMemory
  let activations = (HS.union newActivationsFromAddition newActivationsFromRemoval)
      retractions = (HS.union newRetractionsFromAddition newRetractionsFromRemoval)
  -- Done
  stratifyActivity activations retractions

{-# INLINEABLE deleteWMEs #-}
deleteWMEs :: (HasTracer agent, MetadataAndProofConstraints metadata proof, HasWorkingMemoryData agent metadata, HasPendingActivity agent metadata proof, HasStateMap agent metadata, Members '[GlobalContextID, OpenTelemetrySpan, RuleMatcher metadata matchComponents proof, OpenTelemetryContext, Resource, VariableMapping ConcreteTruth, KeyedState PSCMConfiguration, Transactional, WorkingMemory' metadata, Log, IDGen, Reader agent, RuleDatabase, Error (SubstateIdentificationException metadata proof)] r) => HashSet (WME' metadata) -> Sem r ()
deleteWMEs wmes = do
  let deletingMap = HS.toMap wmes <&> const top
  applyWMChanges HM.empty deletingMap    
