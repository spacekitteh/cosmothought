module PSCM.Effects.ProblemSubstates where
    

-- import Data.HashPSQ as PSQ
import Control.Exception
import Model.PSCM.BasicTypes
import Core.CoreTypes
import PSCM.States
import Cosmolude hiding (cons, lookupObjectIDByName)
import Data.List.NonEmpty
import qualified StmContainers.Set as STMS (Set)



data SubstateDoesNotExist = SubstateDoesNotExist {currentState :: StateIdentifier, desiredSubstate :: ObjectIdentifier}
                            deriving stock (Eq, Show, Generic)
                            deriving anyclass Exception

data ProblemSubstates metadata m a where
  -- | Create a new substate.
  InitiateSubstate :: Impasse metadata -> ObjectIdentifier -> StatePriority -> ProblemSubstates metadata m (NonEmpty (WME' metadata))
  -- | Declare the substate to be resolved, and terminate all subsubstates and sibling substates.
  ResolveSubstate :: ImpasseResolution metadata -> ProblemSubstates metadata m ()
  -- | Enter a particular substate.
  DescendIntoSubstate :: ObjectIdentifier -> ProblemSubstates metadata m () --(Maybe SubstateDoesNotExist)
  AscendToParentSubstate :: ProblemSubstates metadata m ()
  ChildStates :: ProblemSubstates metadata m (Maybe (STMS.Set (StateData metadata)))
  -- | Set a state priority. With clever use, this can allow nonuniformly-interleaved search.
  SetStatePriority ::
    StateIdentifier ->
    -- | The priority; smaller is higher priority. No state with a lower (== larger)
    -- priority will run until all activity at all higher depths and higher priorities have processed.
    StatePriority ->
    ProblemSubstates metadata m ()
  CurrentState :: ProblemSubstates metadata m StateIdentifier
  SetStateQuiescenceFuel :: Maybe QuiescenceFuel -> ProblemSubstates metadata m ()
  UseFuel :: StateIdentifier -> ProblemSubstates metadata m Bool
  CurrentOperator :: StateIdentifier -> ProblemSubstates metadata m (Maybe (WME' metadata))
  GetAllStates :: ProblemSubstates metadata m [StateIdentifier]
makeSem ''ProblemSubstates    
