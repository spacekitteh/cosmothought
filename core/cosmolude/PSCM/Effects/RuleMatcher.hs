module PSCM.Effects.RuleMatcher where
import Cosmolude
import qualified Data.HashSet as HS
import qualified Data.HashMap.Strict as HM
import Data.Tuple (swap)    
import Core.Rule.Match
import Core.CoreTypes
import Numeric.Natural (Natural)


-- TODO: Make this a higher-order effect by adding CapturingActivityOnFoldable    
data RuleMatcher metadata matcherComponents matchProof  m a where
  AddWMEs :: Foldable t => t (WME' metadata) -> RuleMatcher metadata matcherComponents matchProof m (HashSet (RuleActivation' metadata matchProof), HashSet (RuleRetraction' metadata matchProof))
  RemoveWMEs :: (Foldable t) => t (WME' metadata) -> RuleMatcher metadata matcherComponents matchProof  m (HashSet (RuleActivation' metadata matchProof), HashSet (RuleRetraction' metadata matchProof))
  AddRules :: (Each1 (t Rule) (t Rule) Rule Rule, Traversable t) => t Rule -> RuleMatcher metadata matcherComponents matchProof m (HashSet (matcherComponents), HashSet (RuleActivation' metadata matchProof), HashSet (RuleRetraction' metadata matchProof))
  RemoveRules :: Foldable t => t Rule -> RuleMatcher metadata matcherComponents matchProof m (HashSet (RuleActivation' metadata matchProof), HashSet (RuleRetraction' metadata matchProof))
  GetAllRules :: RuleMatcher metadata matcherComponent  matchProof m (Natural,[Rule])
makeSem ''RuleMatcher
                 
newtype ProductionWMEPatternMap rule = ProductionWMEPatternMap {_rulesForCondition :: HashMap WMEPattern (HashSet (rule, VariableRenaming))}
  deriving (Show)

instance (Renderable rule) => Renderable (ProductionWMEPatternMap rule) where
  render (ProductionWMEPatternMap r) =
    list . fmap (\(k, v) -> render k <+> "⥽" <+> render v) . HM.toList . fmap HS.toList $ r

makeLenses ''ProductionWMEPatternMap
-- | For a collection of rules, creating a mapping from WMEPatterns to the collection of rules that the
-- pattern appears in.
{-# INLINEABLE productionConditionMap #-}
productionConditionMap ::
  forall rule f.
  ( Hashable rule,
    HasConditions rule,
    Functor f,
    Foldable f,
    -- Each t t rule rule,
    HasVariables rule
  ) =>
  f rule ->
  (f rule, ProductionWMEPatternMap rule)
productionConditionMap rules = (normalised, result)
  where
    normalised = fmap makeVariablesTermLocal rules
    result =
      ProductionWMEPatternMap
        -- TODO TODO TODO : Merge the map depending on alpha equivalence; but doesn't that not matter anyway due
        -- to how the join nodes are looked up by join condition, which uses node indices, not variable names?

        -- TODO reorder conditions for maximal sharing; crude algorithm just looks at rearranging conditions based on their cardinality
        <$> HM.fromListWith HS.union
        $ swap
          <$> (normalised
                  ^@.. folded
                  . (reindexed (HS.singleton . (, emptySubstitution)) (selfIndex))
                  <. conditions
                  . conditionPatterns
              )
