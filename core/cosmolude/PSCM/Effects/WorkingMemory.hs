module PSCM.Effects.WorkingMemory where
import Numeric.Natural
import Core.Patterns.PatternSyntax
import Core.Patterns.HashKey
import Polysemy    
import Core.Variable
import Data.Maybe
import Data.Foldable
import Polysemy.Transaction
import GHC.Stack
import Control.Lens
import Control.Lens.Action
import Control.Applicative
import Data.Function
import Data.HashSet (HashSet)
import Core.Identifiers hiding (LookupObjectIDByName, lookupObjectIDByName, NumberOfReferences,numberOfReferences)
import Core.CoreTypes
--import StmContainers.Map as STMM
--import StmContainers.Multimap as STMMM
import StmContainers.Set as STMS
import qualified Core.Identifiers as ID
    
    
data WorkingMemory' metadata m a where
  AddWME :: WME' metadata -> WorkingMemory' metadata  m ()
  RemoveWME :: WME' metadata  -> WorkingMemory' metadata  m ()
  ObjectProperties :: ObjectIdentifier %1 -> WorkingMemory' metadata  m (Maybe (STMS.Set (WME' metadata )))
  LookupObjectIDByName :: SymbolName -> WorkingMemory'  metadata m ObjectIdentifier
  FindWMEsByPattern :: WMEPattern -> WorkingMemory' metadata m ([(WME' metadata)])
  AllWMEsByHash :: MemoryConstantTestHashKey -> WorkingMemory' metadata m (Maybe (STMS.Set (WME' metadata)))
  ContentAddressableIdentifier :: WMEContentKey ->  WorkingMemory' metadata m WMEIdentifier
  CreateFreshObjectIDBasedOnVariable :: RuleIdentifier -> Variable -> WorkingMemory' metadata m (Maybe ObjectIdentifier)
  GetAllObjectsAsKVs :: WorkingMemory' metadata m [(ObjectIdentifier, WME' metadata)]
  NumberOfReferences :: ObjectIdentifier %1 -> WorkingMemory' metadata m (Maybe Natural)
  GetWMEsToGC :: WorkingMemory' metadata m (HashSet (WME' metadata))
  GetReferencedObjects :: ObjectIdentifier -> WorkingMemory' metadata m [ObjectIdentifier]
  PreserveObjectFromGC :: ObjectIdentifier -> WorkingMemory' metadata m ()
  StopPreservingObjectFromGC :: ObjectIdentifier -> WorkingMemory' metadata m ()
                 

makeSem ''WorkingMemory'    

{-# INLINABLE addWMEsToWM #-}
addWMEsToWM :: (WMETruthValue (WME' metadata), Foldable f, Members '[Transactional, WorkingMemory' metadata] r)  => f (WME' metadata, WMETruth) -> Sem r ()
addWMEsToWM = mapM_ \(wme, truth) -> do
  writeT truth (wme^.truthValue)
  addWME wme

               

{-# INLINABLE removeWMEsFromWM #-}
removeWMEsFromWM :: (Foldable f, Member (WorkingMemory' metadata) r)  => f (WME' metadata) -> Sem r ()
removeWMEsFromWM = mapM_ removeWME        
{-# INLINE getAllWMEs #-}
getAllWMEs :: Member (WorkingMemory' metadata) r => Sem r [WME' metadata]
getAllWMEs = do
  objs <- getAllObjectsAsKVs
  pure (objs^.. folded . _2)


{-# INLINE runObjectIdentityMapWithWorkingMemoryData #-}
runObjectIdentityMapWithWorkingMemoryData :: (HasCallStack, Members '[WorkingMemory' metadata] r) => InterpreterFor (ObjectIdentityMap (WME' metadata)) r
runObjectIdentityMapWithWorkingMemoryData =
  interpret $ \case
    ID.LookupObjectIDByName n -> lookupObjectIDByName n
    ID.EntireObject o -> objectProperties o
    ID.NumberOfReferences o -> numberOfReferences o       


{-# INLINE wmesMatchingPattern #-}                               
wmesMatchingPattern :: (Members '[WorkingMemory' metadata] r) => MonadicFold (Sem r) WMEPattern (WME' metadata)
wmesMatchingPattern = (act findWMEsByPattern) . folded
