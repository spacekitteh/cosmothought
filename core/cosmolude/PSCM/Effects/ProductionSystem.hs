module PSCM.Effects.ProductionSystem where

import Cosmolude
import Core.WorkingMemory.Reifiable

data ProductionSystem' metadata m a where
  AddElements ::
    ReifiableToWMEs obj metadata =>
    obj ->
    ProductionSystem' metadata m ()
  RemoveElements ::
    ReifiableToWMEs obj metadata =>
    obj ->
    ProductionSystem' metadata m ()
  AddAndRemoveElements ::
    ReifiableToWMEs obj metadata=>
    obj ->
    obj ->
    ProductionSystem' metadata m ()
  AddRules :: (Each1 (t Rule) (t Rule) Rule Rule, Traversable t) => t Rule -> ProductionSystem' metadata m ()
  RemoveRules :: Traversable t => t RuleIdentifier -> ProductionSystem' metadata m ()

makeSem ''ProductionSystem'
        
