{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE TypeFamilyDependencies #-}
{-# LANGUAGE UndecidableSuperClasses #-}

module PSCM.Effects.ProductionSystem.DiscriminationNetwork where

import Cosmolude
import Data.Heap as Heap
import Data.Map
import GHC.IsList
import GHC.TypeNats

type NodeLevel = Natural

type RelativesCollection' a = Heap (Entry NodeLevel a)

{-# INLINE ignoringLevel #-}
ignoringLevel :: Lens' (Entry NodeLevel a) a
ignoringLevel = lens payload (\(Entry lvl _) node -> Entry lvl node)

data DiscriminationNetworkNodeKind (arity :: Nat) where
  AtomicPatternMatch :: DiscriminationNetworkNodeKind 0
  UnaryPredicate :: DiscriminationNetworkNodeKind 1
  ConjunctionOfConditions :: DiscriminationNetworkNodeKind arity
  NegationOfConditions :: DiscriminationNetworkNodeKind arity
  RuleMatch :: DiscriminationNetworkNodeKind 1

type BinaryJoin = ConjunctionOfConditions @2

type family TokenType networkType (nodeKind :: DiscriminationNetworkNodeKind n) = (tokenType :: Type) | tokenType -> nodeKind networkType

class
  (DiscriminationToken m networkType (NodeTokenType networkType nodeType), DiscriminationNetwork m networkType) =>
  DiscriminationNetworkNode (m :: Type -> Type) networkType nodeType
  where
  type NodeTokenType networkType nodeType = (tokenType :: Type) | tokenType -> nodeType networkType
  nodeIdentifier :: Lens' nodeType (NodeReference networkType)
  parentNodes ::
    nodeType -> m (RelativesCollection' nodeType)
  childNodes :: nodeType -> m (RelativesCollection' nodeType)

  createEmptyFreshToken :: nodeType -> m (NodeTokenType networkType nodeType)
  allTokens :: nodeType -> m [(NodeTokenType networkType nodeType)]

class (Eq (NodeReference networkType), Ord (NodeReference networkType), Monad m, Hashable (NodeReference networkType), DiscriminationToken m networkType (NodeTokenType networkType (GenericNodeType networkType)), DiscriminationNetworkNode m networkType (GenericNodeType networkType {-, Monad (MonadNodeData networkType), Monad (MonadTokenManipulation networkType)-})) => DiscriminationNetwork (m :: Type -> Type) networkType where
  type NodeReference (networkType :: Type) :: (Type)
  type DiscriminationNetworkNodeType networkType (nodeKind :: DiscriminationNetworkNodeKind n) = (nodeType :: Type) | nodeType -> nodeKind networkType
  type NetworkJoinConditionType networkType :: Type
  type AtomicPatternType (networkType :: Type) :: Type
  type RuleIdentifierType (networkType :: Type) :: Type
  type VariableRenamingType networkType :: Type
  type GenericNodeType networkType = (nodeType :: Type) | nodeType -> networkType

--    type family MonadNodeData (networkType :: Type) :: (Type -> Type)
--    type family MonadTokenManipulation (networkType :: Type) :: (Type -> Type)

-- parentNodes :: forall (n :: Nat) (nodeKind :: DiscriminationNetworkNodeKind n) m. (1 <= n, KnownNat n, Monad m) => DiscriminationNetworkNodeType networkType nodeKind -> m (RelativesCollection' networkType)
-- childNodes :: forall (n :: Nat) (nodeKind :: DiscriminationNetworkNodeKind n) m. (KnownNat n, Monad m) => DiscriminationNetworkNodeType networkType nodeKind -> m (RelativesCollection' networkType)

type GenericToken networkType = NodeTokenType networkType (GenericNodeType networkType)

class NetworkJoinCondition networkType joinCond

-- \| Takes a join condition, a lookup function taking an input node and returning a token,
--  performJoin :: joinCond -> (NodeReference networkType -> GenericToken networkType) -> [GenericToken networkType]

data JoinPlan networkType = JoinPlan [(NetworkJoinConditionType networkType)]

-- instance forall networkType. (NetworkJoinCondition networkType (NetworkJoinConditionType networkType)) => NetworkJoinCondition networkType (JoinPlan networkType) where
--   performJoin (JoinPlan conds) theMap = (fmap (\c -> performJoin c theMap) conds)

deriving instance (Eq (NetworkJoinConditionType networkType)) => Eq (JoinPlan networkType)

instance IsList (JoinPlan networkType) where
  type Item (JoinPlan networkType) = NetworkJoinConditionType networkType
  fromList = JoinPlan
  toList (JoinPlan p) = p

-- | NodeJoinPlans consists of a , with each tuple being a parent node, and list of join
-- conditions.
--
-- For each (node, [joinCondition]) tuple, the [joinCondition] specifies how to join an incoming token from `node` to the other
data NodeJoinPlans networkType = NodeJoinPlans (Map (NodeReference networkType) (JoinPlan networkType))
  deriving stock (Generic)

instance (Ord (NodeReference networkType)) => IsList (NodeJoinPlans networkType) where
  type Item (NodeJoinPlans networkType) = (NodeReference networkType, JoinPlan networkType)
  fromList = NodeJoinPlans . GHC.IsList.fromList
  toList (NodeJoinPlans m) = GHC.IsList.toList m

class (DiscriminationNetwork m networkType) => DiscriminationToken (m :: Type -> Type) networkType token where
  type MatchType (token :: Type) :: Type
  inNode :: token -> (NodeReference networkType)

  parentTokens :: token -> m [token]

class (DiscriminationNetwork m networkType) => MonadDiscriminationNetworkBuilder networkType m where
  embedAtomicPatternMatch :: DiscriminationNetworkNodeType networkType AtomicPatternMatch -> m (GenericNodeType networkType)
  embedUnaryPredicate :: DiscriminationNetworkNodeType networkType UnaryPredicate -> m (GenericNodeType networkType)
  embedConjunction :: DiscriminationNetworkNodeType networkType ConjunctionOfConditions -> m (GenericNodeType networkType)
  embedDisjunction :: DiscriminationNetworkNodeType networkType NegationOfConditions -> m (GenericNodeType networkType)
  embedRuleEndpoint :: DiscriminationNetworkNodeType networkType RuleMatch -> m (GenericNodeType networkType)
  createNewDiscriminationNetwork :: m ()
  createOrShareAtomicPatternMatches :: (Traversable t) => t (AtomicPatternType networkType) -> m (t (DiscriminationNetworkNodeType networkType AtomicPatternMatch, VariableRenamingType networkType))
  createOrShareUnaryPredicate :: (parentNodeType ~ DiscriminationNetworkNodeType networkType parentNodeKind) => parentNodeType -> (TokenType networkType parentNodeKind -> Bool) -> m (DiscriminationNetworkNodeType networkType UnaryPredicate)
  createOrShareConjunctionOfConditions :: (KnownNat n, Traversable t) => SNat n -> t (GenericNodeType networkType) -> NodeJoinPlans networkType -> m (DiscriminationNetworkNodeType networkType (ConjunctionOfConditions @n))
  createOrShareNegationOfConditions :: (KnownNat n, Traversable t) => SNat n -> t (GenericNodeType networkType) -> NodeJoinPlans networkType -> m (DiscriminationNetworkNodeType networkType (NegationOfConditions @n))
  createRuleMatchEndpoint :: RuleIdentifierType networkType -> VariableRenamingType networkType -> m (DiscriminationNetworkNodeType networkType RuleMatch)

createOrShareNegatedAtomicPatternMatch :: forall networkType m. (MonadDiscriminationNetworkBuilder networkType m) => AtomicPatternType networkType -> m (DiscriminationNetworkNodeType networkType (NegationOfConditions @1), DiscriminationNetworkNodeType networkType AtomicPatternMatch, VariableRenamingType networkType)
createOrShareNegatedAtomicPatternMatch pat = do
  (Identity (alphaNode' :: DiscriminationNetworkNodeType networkType AtomicPatternMatch, renaming)) <- createOrShareAtomicPatternMatches (Identity pat)
  alphaNode <- embedAtomicPatternMatch alphaNode'
  negationNode <- createOrShareNegationOfConditions (SNat @1) (Identity alphaNode) (NodeJoinPlans (GHC.IsList.fromList [(alphaNode ^. nodeIdentifier @m @networkType, JoinPlan [])]))
  pure (negationNode, alphaNode', renaming)

createOrShareBinaryJoin :: forall networkType m. (MonadDiscriminationNetworkBuilder networkType m) => GenericNodeType networkType -> GenericNodeType networkType -> NodeJoinPlans networkType -> m (DiscriminationNetworkNodeType networkType BinaryJoin)
createOrShareBinaryJoin left right conds = createOrShareConjunctionOfConditions (SNat @2) ([left, right]) conds

data DiscriminationNetworkBuilder networkType m a where
  NewDiscriminationNetwork :: DiscriminationNetworkBuilder networkType m ()
  BuildOrShareAtomicPatternMatches :: (Traversable t) => t (AtomicPatternType networkType) -> DiscriminationNetworkBuilder networkType m (t (DiscriminationNetworkNodeType networkType AtomicPatternMatch, VariableRenamingType networkType))
  BuildOrShareUnaryPredicate :: (parentNodeType ~ DiscriminationNetworkNodeType networkType parentNodeKind) => parentNodeType -> (TokenType networkType parentNodeKind -> Bool) -> DiscriminationNetworkBuilder networkType m (DiscriminationNetworkNodeType networkType UnaryPredicate)
  BuildOrShareConjunctionOfConditions :: (KnownNat n, Traversable t) => SNat n -> t (GenericNodeType networkType) -> NodeJoinPlans networkType -> DiscriminationNetworkBuilder networkType m (DiscriminationNetworkNodeType networkType (ConjunctionOfConditions @n))
  BuildOrShareNegationOfConditions :: (KnownNat n, Traversable t) => SNat n -> t (GenericNodeType networkType) -> NodeJoinPlans networkType -> DiscriminationNetworkBuilder networkType m (DiscriminationNetworkNodeType networkType (NegationOfConditions @n))
  BuildRuleMatchEndpoint :: (RuleIdentifierType networkType) -> (VariableRenamingType networkType) -> DiscriminationNetworkBuilder networkType m (DiscriminationNetworkNodeType networkType RuleMatch)
  InjectAtomicPatternMatch :: DiscriminationNetworkNodeType networkType AtomicPatternMatch -> DiscriminationNetworkBuilder networkType m (GenericNodeType networkType)
  InjectUnaryPredicate :: DiscriminationNetworkNodeType networkType UnaryPredicate -> DiscriminationNetworkBuilder networkType m (GenericNodeType networkType)
  InjectConjunctionOfConditions :: DiscriminationNetworkNodeType networkType ConjunctionOfConditions -> DiscriminationNetworkBuilder networkType m (GenericNodeType networkType)
  InjectDisjunctionOfConditions :: DiscriminationNetworkNodeType networkType NegationOfConditions -> DiscriminationNetworkBuilder networkType m (GenericNodeType networkType)
  InjectRuleEndpoint :: DiscriminationNetworkNodeType networkType RuleMatch -> DiscriminationNetworkBuilder networkType m (GenericNodeType networkType)

makeSem ''DiscriminationNetworkBuilder

instance (DiscriminationNetwork (Sem r) networkType, (NetworkJoinConditionType networkType) ~ joinCondType, Member (DiscriminationNetworkBuilder networkType) r) => MonadDiscriminationNetworkBuilder networkType (Sem r) where
  createNewDiscriminationNetwork = newDiscriminationNetwork
  createOrShareAtomicPatternMatches = buildOrShareAtomicPatternMatches
  createOrShareUnaryPredicate = buildOrShareUnaryPredicate
  createOrShareConjunctionOfConditions = buildOrShareConjunctionOfConditions
  createOrShareNegationOfConditions = buildOrShareNegationOfConditions
  createRuleMatchEndpoint ::
    ( DiscriminationNetwork (Sem r) networkType,
      NetworkJoinConditionType networkType ~ joinCondType,
      Member (DiscriminationNetworkBuilder networkType) r
    ) =>
    RuleIdentifierType networkType ->
    VariableRenamingType networkType ->
    Sem r (DiscriminationNetworkNodeType networkType RuleMatch)
  createRuleMatchEndpoint = buildRuleMatchEndpoint
  embedAtomicPatternMatch = injectAtomicPatternMatch

  embedUnaryPredicate = injectUnaryPredicate
  embedConjunction = injectConjunctionOfConditions
  embedDisjunction = injectDisjunctionOfConditions
  embedRuleEndpoint = injectRuleEndpoint
