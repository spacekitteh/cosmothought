module PSCM.ProductionSystem (ProductionSystem', addElements, addAndRemoveElements, removeElements, addRules, removeRules, runProductionSystem, MetadataAndProofConstraints, SubstateIdentificationExceptionType (..), SubstateIdentificationException (..), ProductionSystemEffects', removeGarbageSimple, runOneCycle, runUntilQuiescence, setsAnOperatorPreference, doChangesAlterAnImpasse, runGC) where

-- import Polysemy.Embed (Embed, embed)

-- import System.Mem

-- import qualified Control.Exception as CE (assert)
import Control.Lens.Action
import Core.CoreTypes
import Core.Rule.Database
import Core.Rule.Match
import Core.Rule.Optimisation
import Core.WorkingMemory
import Core.WorkingMemory.Reifiable
import Cosmolude hiding (empty, id, note, (.))
import qualified Data.HashMap.Lazy as HML
import qualified Data.HashMap.Strict as HM
import qualified Data.HashSet as HS
import Data.HashSet.Lens
import qualified Data.Set.Ordered as SO
import qualified Data.Text as T
import Development.DomainModelling hiding (Output)
import GC.Model
import GHC.Exts (SPEC (..))
import Model.PSCM
import Model.PSCM.BasicTypes
import Model.PSCM.Impasses
import PSCM.Configuration
import PSCM.Effects.ProblemSubstates
import PSCM.Effects.ProductionSystem
import qualified PSCM.Effects.RuleMatcher as Matcher
import PSCM.Errors
import PSCM.Operators
import PSCM.Operators.OperatorProposals (chosenIdentifier, proposals, proposeOperatorWME, validOperatorProposals)
import PSCM.ProblemSubstates (HasTopLevelStateInitialised)
import PSCM.ProductionSystem.ActivityIdentification
import PSCM.ProductionSystem.PerformActivity
import PSCM.ProductionSystem.WMEChanges
import PSCM.States as States
import Polysemy.Error
import Polysemy.Reader
import Polysemy.State
import qualified Prelude (error)

type ProductionSystemEffects' agent metadata components proof r =
  ( MetadataAndProofConstraints metadata proof,
    ActivityPerformanceConstraints agent metadata proof r,
    HasStateMap agent metadata,
    Members '[Matcher.RuleMatcher metadata components proof, Error (SubstateIdentificationException metadata proof), GlobalContextID, RuleDatabase, ObjectIdentityMap (WME' metadata), Tagged PSCMLoopPhase (State PSCMLoopPhase), Tagged CurrentOperator (State (ChosenOperator metadata))] r -- ent, IDGen, RuleDatabase, ProblemSubstates metadata, KeyedState PSCMConfiguration, Error (SubstateIdentificationException metadata proof)] r
  )

-- TODO OPTIMISE: Use information from the cycle's production activity to get bounds on what WMEs could possibly be affected due to their state depth
getObjectsToConsiderForGarbageCollection :: forall agent metadata r. (HasWorkingMemoryData agent metadata, Members '[WorkingMemory' metadata] r) => Sem r (HashSet ObjectIdentifier)
getObjectsToConsiderForGarbageCollection = do
  kvs <- getAllObjectsAsKVs
  pure $ setOf (folded . _1) kvs

removeGarbageSimple :: (Eq node, Ord node, Hashable node, Foldable g, Foldable h, Members '[Transactional, Tagged GetLinks (Methodology node (g node))] r) => HS.HashSet node -> h node -> Sem r [node]
removeGarbageSimple nodesToConsider roots = do
  worklist <- newT SO.empty
  garbage <- newT []
  pending <- newT HS.empty
  fullyProcessed <- newT HS.empty
  removeGarbage
    & untag @GetFoldable
    & runMethodologyPure id
    & untag @ResetMarkForNextGCCycle
    & runMethodologyPure (const ())
    & untag @ScheduleForDeletion
    & runOutputSem (\node -> modifyT (node :) garbage)
    & untag @GetNodesToConsider
    & runInputConst nodesToConsider
    & untag @GetRoots
    & runInputConst roots
    & untag @IsNodeUnderConsideration
    & decision
    & runMethodologyPure (\a -> HS.member a nodesToConsider)
    & untag @AddNodeToWorklist
    & runMethodologySem (\node -> modifyT (node SO.|<) worklist)
    & untag @RemoveNodeFromWorklist
    & runMethodologySem (\node -> modifyT (SO.delete node) worklist)
    & untag @GetNextNodeToProcess
    & runInputSem ((flip SO.elemAt 0) <$> readT worklist)
    & untag @MarkNodeAsPending
    & runMethodologySem (\node -> modifyT (HS.insert node) pending)
    & untag @MarkNodeAsFullyProcessed
    & runMethodologySem
      ( \node -> do
          modifyT (HS.delete node) pending
          modifyT (HS.insert node) fullyProcessed
      )
    & untag @IsNodeMarked
    & decision
    & runMethodologySem
      ( \node -> do
          pending' <- readT pending
          fullyProcessed' <- readT fullyProcessed
          pure (HS.member node pending' || HS.member node fullyProcessed')
      )
  garbage' <- readT garbage
  pure garbage'

performGarbageCollection :: forall agent metadata components proof r. (HasWorkingMemoryData agent metadata, ProductionSystemEffects' agent metadata components proof r) => Sem r (HashSet (WME' metadata))
performGarbageCollection = push "PSCM.ProductionSystem.performGarbageCollection" do
  nodesToConsider <- getObjectsToConsiderForGarbageCollection @agent @metadata
  roots :: [StateIdentifier] <- getAllStates
  addAttributeWithRaw "pscm.gc.roots" roots
  debug' $ "Performing tracing GC with roots" <+> render roots
  garbage' <-
    removeGarbageSimple nodesToConsider (roots ^.. folded . stateObject)
      & untag @GetLinks
      & runMethodologySem getReferencedObjects

  wmes <- forM garbage' \obj -> do
    attrs <- objectProperties obj
    case attrs of
      Nothing -> do
        error "Tried to delete non-existent object!" [("wme.oid", toAttribute obj)]
        pure (HS.empty)
      Just wmes -> wmes ^! extractSet
  pure (HS.unions wmes)

runGC :: (HasWorkingMemoryData agent metadata, ProductionSystemEffects' agent metadata components proof r) => Sem r ()
runGC = push "runGC" do
  wmesToDeleteByRefCounting <- getWMEsToGC
  deleteWMEs wmesToDeleteByRefCounting
  wmesToYeet <- performGarbageCollection
  deleteWMEs wmesToYeet

--  when True do
--    embed $ performGC

-- | The main loop of the production system. Instantiates the rule actions, applies them, and loops.
--
-- TODO: Add a "(<s> quiescence t)" WME after quiescence is reached.
runUntilQuiescence :: (HasWorkingMemoryData agent metadata, ProductionSystemEffects' agent metadata components proof r) => Sem r ()
runUntilQuiescence =
  {-# SCC runUntilQuiescence #-}
  push "runUntilQuiescence" $ do
    globalFuelLimit <- getAt MaximumIterationsForQuiescence

    runUntilQuiescence' globalFuelLimit (1 :: QuiescenceFuel) SPEC
  where
    runUntilQuiescence' globalFuelLimit (n :: QuiescenceFuel) !sPEC = do
      continue <- runOneCycle n
      case (globalFuelLimit, continue) of
        (Nothing, Just (!n')) -> do
          -- No global limit
          runUntilQuiescence' globalFuelLimit n' sPEC
        (Just globalFuelLimit', Just n') | n' <= globalFuelLimit' -> do
          runUntilQuiescence' globalFuelLimit n' sPEC
        (Just globalFuelLimit', Just _) -> do
          addEvent "Quiescence fuel exhaustion" [("pscm.activity.quiescence.limit", toAttribute (T.pack $ show globalFuelLimit'))]
        (_, Nothing) -> do
          pure ()

setsAnOperatorPreference :: (ProductionSystemEffects' agent metadata components proof r) => WME' metadata -> Sem r (Maybe (WME' metadata, StateIdentifier, ObjectIdentifier))
setsAnOperatorPreference wme@(WME s "operator" op _ meta)
  | Just oid <- op ^? _ObjectIdentifier,
    Just _ <- meta ^? preference @_ @ObjectIdentifier . _Just = do
      agent <- ask
      sid' <- lookupMap s (agent ^. stateMap . statesByObject)
      case sid' of
        Nothing -> pure Nothing
        Just sid -> pure $ Just (wme, sid, oid)
setsAnOperatorPreference _ = pure Nothing

-- createImpasseResolution ::
-- altersGoalDependencySet ::

impasse ::
  (HasWorkingMemoryData agent meadata, ProductionSystemEffects' agent metadata components proof r) =>
  MonadicFold (Sem r) StateIdentifier (Impasse' (OperatorProposals metadata))
impasse = state . impasseReason . _Just

handleImpasseOperatorPreferencesChanged ::
  ( HasWorkingMemoryData agent meadata, -- , ProductionSystemEffects' agent metadata components proof r
    HasTracer agent,
    HasMaybeWMPreference metadata ObjectIdentifier,
    Members
      '[ Reader agent,
         OpenTelemetryContext,
         OpenTelemetrySpan,
         ProblemSubstates metadata,
         Log,
         Error (ImpasseResolution' (OperatorProposals metadata) (ChosenOperator metadata) (ImpasseResolutionData metadata)),
         Resource
       ]
      r
  ) =>
  StateIdentifier ->
  Impasse' (OperatorProposals metadata) ->
  HashMap (WME' metadata) WMETruth ->
  HashMap (WME' metadata) WMETruth ->
  Sem r ()
handleImpasseOperatorPreferencesChanged affectedStateIdent impasseReason additions removals = push "handleImpasseOperatorPreferencesChanged" do
  -- 2.ii.
  let existingProposals = setOf (operators . proposals) impasseReason
      withNewProposals = existingProposals <> (setOf (folded . validOperatorProposals . to proposeOperatorWME) (HML.keys additions))
      newProposals' = HS.difference withNewProposals (setOf (folded . validOperatorProposals . to proposeOperatorWME) (HML.keys removals))
      newProposals = newProposals' ^. folded . to proposeOperator

  tentativeOutcome <- runInputConst affectedStateIdent $ evaluateOperators newProposals
  case tentativeOutcome of
    Right chosenOperator ->
      case impasseReason of
        NoChange (OperatorNoChangeImpasse _) -> do
          currentOpWME <- currentOperator affectedStateIdent
          if chosenOperator ^? chosenIdentifier /= currentOpWME ^? _Just . object
            then -- 2.a)
              push "operatorImpasseResolved" do
                throw $ resolveImpasse affectedStateIdent chosenOperator (makeImpasseResolutionData [] additions removals)
            else -- 2.b)
            -- TODO FIXME It should be modified in-place, not regenerated, incorporating the additional preferences added.
              push "operatorImpasseRegenerated" do
                error "OperatorImpasseRegenerated not implemented!" [("pscm.state.sid", toAttribute $ affectedStateIdent)]
                throw $ regenerateImpasse affectedStateIdent impasseReason (makeImpasseResolutionData [] additions removals)
        _ -> do
          throw $ resolveImpasse affectedStateIdent chosenOperator (makeImpasseResolutionData [] additions removals)
    Left newImpasse | newImpasse == impasseReason -> do
      -- Nothing to do.
      pure ()
    Left newImpasse -> do
      -- Regenerate with new impasse?
      throw $ regenerateImpasse affectedStateIdent newImpasse (makeImpasseResolutionData [] additions removals)

doChangesAlterAnImpasse :: (HasWorkingMemoryData agent meadata, ProductionSystemEffects' agent metadata components proof r) => StateIdentifier -> HashMap (WME' metadata) WMETruth -> HashMap (WME' metadata) WMETruth -> Sem r (Maybe (ImpasseResolution metadata))
doChangesAlterAnImpasse affectedStateIdent additions removals = do
  -- 1. Look up state to see if it has an impasse.
  --    a) If it doesn't, then it is the lowest state in the tree.
  --    b) If so, check that it isn't an OperatorNoChange impasse, because that's probably fine, and doesn't need an impasse resolution? Maybe?
  -- 2. i. Check if operator preferences have changed, *and* that ii. the preferences alter the outcome of the operator selection process
  --    a) If an operator is selected, resolve the substate.
  --    b) Otherwise, regenerate the substate.
  -- 3. Get GDS of the state.
  -- 4. Check if the GDS is affected by the additions or removals.
  --    a) If so, then either resolve, regenerate or eliminate the substate and its children, based on the type.
  --    b) Otherwise, return Nothing.

  -- What should the "affected state identifier" be? Should it be the state that created it, or the state that
  -- the WME is put in, or what? Since we process top-down, we will always be maximally consistent... Right? But
  -- what about timing? Because we check whether the changes resolve an impasse before the next production
  -- system cycle, how do we know if these changes fix it? Recall that the only way to actually /resolve/ an
  -- impasse is to alter operator preferences. So, we check /top down/. Since a production cannot test a
  -- substate value, it doesn't matter when the affectedState is deeper than the state the WME alterations go
  -- in. And since productions cannot create values in substates, it doesn't matter when the affectedState is
  -- shallower than the alterations appear in. Thus, we only need to check for alterations to the level of the
  -- affectedState!
  result <- runError $ do
    (impasseReason' :: Maybe (Impasse metadata)) <- affectedStateIdent ^!? impasse
    case impasseReason' of
      -- 1.a) No impasse in this state; we are in the lowest state, so nothing to do.
      Nothing -> pure ()
      -- 1.b) TODO FIXME ? What should this be
      Just impasseReason -> do
        -- 2.i.
        operatorPreferencesChange <- anyMOf (both . folded) (\wme -> isJust <$> (setsAnOperatorPreference wme)) (HML.keys additions, HML.keys removals)
        if operatorPreferencesChange
          then handleImpasseOperatorPreferencesChanged affectedStateIdent impasseReason additions removals
          else do
            -- 3.
            -- 4.

            -- 4.b)
            pure ()
  case result of
    Right () -> pure Nothing
    Left err -> pure (Just err)

-- _doChangesIntroduceInconsistency :: (HasWorkingMemoryData agent meadata, ProductionSystemEffects' agent metadata components proof r) => StateIdentifier -> HashMap (WME' metadata) WMETruth -> HashMap (WME' metadata) WMETruth -> Sem r (Maybe (ImpasseResolution metadata))
-- _doChangesIntroduceInconsistency _affectedStateIdent _additions _removals = do
--   -- 1. Look up state to see if it has an impasse.
--   --    a) If it doesn't, then it is the lowest state in the tree.
--   --    b) If so, check that it isn't an OperatorNoChange impasse, because that's probably fine, and doesn't need an impasse resolution? Maybe?
--   -- 2. i. Check if operator preferences have changed, *and* that ii. the preferences alter the outcome of the operator selection process
--   --    a) If an operator is selected, resolve the substate.
--   --    b) Otherwise, regenerate the substate.
--   -- 3. Get GDS of the state.
--   -- 4. Check if the GDS is affected by the additions or removals.
--   --    a) If so, then either resolve, regenerate or eliminate the substate and its children, based on the type.
--   --    b) Otherwise, return Nothing.

--   -- 1.
--   pure Nothing

-- impasseReason' <- affectedStateIdent ^!? state . impasseReason . _Just
-- case impasseReason' of
--   -- 1.a) No impasse in this state; we are in the lowest state, so nothing to do.
--   Nothing -> pure Nothing
--   -- 1.b) TODO FIXME ? What should this be
--   Just (NoChange (OperatorNoChangeImpasse _)) -> pure Nothing --- ?????
--   Just _impasseReason -> do
--     -- 2.i.
--     operatorPreferencesChange <- anyMOf (both . folded) (\wme -> isJust <$> (setsAnOperatorPreference wme)) (HML.keys additions, HML.keys removals)
--     if operatorPreferencesChange
--       then do
--         -- 2.ii.
--         currentProposals <- gatherOperatorProposals affectedStateIdent
--         let existingProposals = setOf folded (fst currentProposals)
--             withNewProposals = existingProposals <> (setOf (folded . to proposeOperatorWME) (HML.keys additions))
--             proposals' = HS.difference withNewProposals (setOf (folded . to proposeOperatorWME) (HML.keys removals))
--             proposals = proposals' ^. folded . to proposeOperator

--         tentativeOutcome <- runInputConst affectedStateIdent $ evaluateOperators proposals
--         if (has _Right tentativeOutcome)
--           then do
--             currentOpWME <- currentOperator affectedStateIdent
--             if tentativeOutcome ^? _Right . chosenIdentifier /= currentOpWME ^? _Just . object
--               then -- 2.a)
--               push "operatorImpasseResolved" do
--                 pure Nothing -- pure ImpasseResolved
--               else -- 2.b)
--               push "operatorImpasseRegenerated" do
--                 pure Nothing -- pure ImpasseRegenerated
--           else pure Nothing
--       else do
--         -- 3.
--         -- 4.

--         -- 4.b)
--         pure Nothing

runOneCycle :: (HasWorkingMemoryData agent metadata, ProductionSystemEffects' agent metadata components proof r) => QuiescenceFuel -> Sem r (Maybe QuiescenceFuel)
runOneCycle n = push "runOneCycle" $ attr "pscm.iteration" (showCoreDocAsText . render $ n) do
  agent <- ask

  processActivity agent
  nextChanges <- getNextWMChangesAndReset
  result <- case nextChanges of
    Nothing -> do
      info' $ "Activity complete after" <+> render n <+> "iterations"
      pure Nothing
    Just (affectedStateIdent, !additions', !removals') -> do
      willThereBeAnImpasseResolution <- doChangesAlterAnImpasse affectedStateIdent additions' removals'
      case willThereBeAnImpasseResolution of
        Nothing -> do
          applyWMChanges additions' removals'
          pure $ Just (n + 1)
        Just resolution -> push "resolvingSubstate" do
          addAttributeWithRaw "pscm.impasse.resolution" resolution
          --          CE.assert False (pure ())
          let additions = resolution ^. resolutionData . additionsToWorkingMemory
              removals = resolution ^. resolutionData . removalsFromWorkingMemory

          case resolution of
            ImpasseResolution _sid (ImpasseResolved operator) _ -> do
              tag @PSCMLoopPhase $ put OperatorElaboration
              tag @Model.PSCM.CurrentOperator $ put operator
            _ -> pure ()
          -- TODO FIXME ORDERING?
          applyWMChanges additions removals
          resolveSubstate resolution
          pure Nothing

  runGC
  pure result

kickStartWithWMEsAndRunUntilQuiescence :: (HasWorkingMemoryData agent metadata, ProductionSystemEffects' agent metadata components proof r) => HashMap (WME' metadata) WMETruth -> HashMap (WME' metadata) WMETruth -> Sem r ()
kickStartWithWMEsAndRunUntilQuiescence additions deletions = push "kickStartWithWMEsAndRunUntilQuiescence" do
  debug' $ "Adding these WMEs:" <+> render (additions ^.. ifolded . withIndex . _1)
  debug' $ "Removing these WMEs:" <+> render (deletions ^.. ifolded . withIndex . _1)
  applyWMChanges additions deletions
  runUntilQuiescence

kickStartWithActivityAndRunUntilQuiescence :: (HasWorkingMemoryData agent metadata, ProductionSystemEffects' agent metadata components proof r) => HashSet (RuleActivation' metadata proof) -> HashSet (RuleRetraction' metadata proof) -> Sem r ()
kickStartWithActivityAndRunUntilQuiescence activations retractions = push "kickStartWithActivityAndRunUntilQuiescence" do
  stratifyActivity activations retractions
  runUntilQuiescence

{-# INLINEABLE runProductionSystem #-}
runProductionSystem ::
  (HasWorkingMemoryData agent metadata, HasTopLevelStateInitialised agent metadata, Member ScopedTransaction r, ProductionSystemEffects' agent metadata components proof r) =>
  InterpreterFor (ProductionSystem' metadata) r
runProductionSystem = interpret $ \i -> do
  case i of
    AddElements initialWMEs ->
      transaction $! push "addElements" do
        wmes <- reifyToWMEs initialWMEs
        kickStartWithWMEsAndRunUntilQuiescence (HM.fromList wmes) HM.empty
    AddAndRemoveElements adds' removes' ->
      transaction $! push "addAndRemoveElements" do
        adds <- reifyToWMEs adds'
        removes <- reifyToWMEs removes'
        kickStartWithWMEsAndRunUntilQuiescence (HM.fromList adds) (HM.fromList removes)
    RemoveElements initialWMEs ->
      transaction $! push "removeElements" do
        wmes <- reifyToWMEs initialWMEs
        kickStartWithWMEsAndRunUntilQuiescence HM.empty (HM.fromList wmes)
    AddRules rules' ->
      transaction $! push "addRules" do
        addAttributeWithRaw "rule.addition.given_rules" (rules' ^.. folded)
        res <- runError @RuleSemanticError $ do
          rules <- traverse simplifyRule rules'
          addAttributeWithRaw "rule.addition.simplified_rules" (rules ^.. folded)
          (_, !activations, !retractions) <- Matcher.addRules rules
          kickStartWithActivityAndRunUntilQuiescence activations retractions
        case res of
          Right result -> pure result
          Left err -> do
            error (render err) ([] :: [(T.Text, Attribute)])
    RemoveRules ruleIDs ->
      transaction $! push "removeRules" do
        !rules <- traverse lookUpRule ruleIDs
        doActivity <- getAt PerformActivityOnRuleRemoval
        case doActivity of
          AlwaysDoActivity -> do
            (!activations, !retractions) <- Matcher.removeRules rules
            kickStartWithActivityAndRunUntilQuiescence activations retractions
          NeverDoActivity -> void $! Matcher.removeRules rules
          WhenNotOperatorSupported -> do
            -- TODO Check operator support
            -- Collect activity from operator-supported rules
            -- and perform it
            Prelude.error "WhenNotOperatorSupported rule removal option not implemented"

-- data RuleValidationException
-- validateRule :: (ProductionSystemEffects agent r, Members '[Error RuleValidationException] r) => Rule -> Sem r Rule
-- validateRule = pure
