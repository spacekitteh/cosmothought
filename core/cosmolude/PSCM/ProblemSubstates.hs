{-# LANGUAGE BangPatterns #-}

module PSCM.ProblemSubstates
  ( ProblemSubstates,
    runProblemSubstates,
    TopLevelStateInitialised,
    HasTopLevelStateInitialised (..),
    initialiseStateWMEs,
    getTopStateWMEs,
    StateManagementState,
    newStateManagementState,
    resetStateManagementState,
    HasStateManagementState (..),
    currentDepth,
  )
where

-- import Data.HashPSQ as PSQ

import Control.Exception
import Core.CoreTypes
import Core.WorkingMemory
import Cosmolude hiding (cons, lookupObjectIDByName)
import Data.List.NonEmpty as DLNE
import qualified Data.Text as T
import Model.PSCM.BasicTypes
import Model.PSCM.Impasses
import Numeric.Natural (Natural)
import PSCM.Configuration
import PSCM.Effects.ProblemSubstates
import PSCM.States
import Polysemy.Reader
import Prelude (fromIntegral)

data SubstateDoesNotExist = SubstateDoesNotExist {currentState :: StateIdentifier, desiredSubstate :: ObjectIdentifier}
  deriving stock (Eq, Show, Generic)
  deriving anyclass (Exception)

data StateManagementState metadata = StateManagementState {_topState :: StateData metadata, _currentPath :: TVar (NonEmpty StateIdentifier)}

makeClassy ''StateManagementState

resetStateManagementState :: StateManagementState metadata -> STM ()
resetStateManagementState (StateManagementState _ts cp) = do
  writeTVar cp (topLevelStateIdentifier :| [])

{-# INLINE newStateManagementState #-}
newStateManagementState :: STM (StateManagementState metadata)
newStateManagementState = StateManagementState <$> topLevelState <*> newTVar (topLevelStateIdentifier :| [])

{-# INLINE getCurrentState #-}
getCurrentState :: (HasStateManagementState agent metadata, Members [Transactional, Reader agent] r) => Sem r StateIdentifier
getCurrentState = do
  slate <- ask <&> view stateManagementState
  (current :| _) <- readT (slate ^. currentPath)
  pure current --  $ path ^. topState . stateIdentifier wait what

data TopLevelStateInitialised metadata = UNSAFETopLevelStateInitialised {-# UNPACK #-} !WorkingMemoryInitialised !(NonEmpty (WME' metadata))

class HasTopLevelStateInitialised ty metadata where
  topLevelStateInitialised :: Action STM ty (TopLevelStateInitialised metadata)

instance HasTopLevelStateInitialised (TopLevelStateInitialised metadata) metadata where
  {-# INLINE topLevelStateInitialised #-}
  topLevelStateInitialised = id

{-# INLINE getTopStateWMEs #-}
getTopStateWMEs :: TopLevelStateInitialised metadata -> NonEmpty (WME' metadata)
getTopStateWMEs (UNSAFETopLevelStateInitialised !_init wmes) = wmes

{-# INLINEABLE initialiseStateWMEs #-}
initialiseStateWMEs :: (HasCallStack, HasMaybeWMPreference metadata ObjectIdentifier, CreateBlankWMEMetadata metadata, HasCreatingState metadata, HasWMESupport metadata, WMEIsState metadata, WMETruthValue (WME' metadata), HasStateMap agent metadata, Members '[OpenTelemetrySpan, OpenTelemetryContext, Log, Reader agent, WorkingMemory' metadata, Transactional] r) => WorkingMemoryInitialised -> Sem r (TopLevelStateInitialised metadata)
initialiseStateWMEs (!workingMemInitialised) = do
  wmes' <- makeStateWMEs topLevelStateIdentifier Nothing
  topStateNameWME <- makeStateAugmentation topLevelStateIdentifier "name" "top"
  let wmes = topStateNameWME DLNE.<| wmes'
  addWMEsToWM (fmap (,top) wmes)
  topStatePrio <- newT 0
  fuel <- newT Nothing
  op <- newT Nothing
  gds <- embedSTM newGoalDependencySet
  let stateData = StateData topLevelStateIdentifier topStatePrio Nothing op fuel gds
  indexStateAtDepth topLevelStateIdentifier stateData 0
  pure (UNSAFETopLevelStateInitialised workingMemInitialised wmes)

{-# INLINEABLE makeStateWMEs #-}
makeStateWMEs :: (CreateBlankWMEMetadata metadata, HasMaybeWMPreference metadata ObjectIdentifier, HasCreatingState metadata, HasWMESupport metadata, WMEIsState metadata, HasStateMap agent metadata, Members '[OpenTelemetrySpan, OpenTelemetryContext, Log, Reader agent, WorkingMemory' metadata, Transactional] r) => StateIdentifier -> Maybe StateIdentifier -> Sem r (NonEmpty (WME' metadata))
makeStateWMEs stateIdent super = do
  addEvent "makeStateWMEs" [("state", toAttribute stateIdent)]
  -- Create the canonical state WME
  stateWME <- (makeStateAugmentation stateIdent "type" "state") <&> isState .~ True

  -- Create extra WMEs for substates
  aux <- case super of
    Nothing -> pure []
    Just superStateIdent -> do
      aug <- makeStateAugmentation stateIdent "superstate" (superStateIdent ^. stateObject . re _ObjectIdentity)
      pure [aug]

  pure (stateWME :| aux)

{-# INLINEABLE makeState #-}
makeState :: (HasCallStack, HasTracer agent, HasStateManagementState agent metadata, Members '[WorkingMemory' metadata, OpenTelemetrySpan, OpenTelemetryContext, Log, Transactional, Reader agent] r) => Impasse metadata -> ObjectIdentifier -> StatePriority -> Maybe QuiescenceFuel -> Sem r (StateData metadata)
makeState reason oid prio fuel = do
  preserveObjectFromGC oid
  slate <- ask <&> view stateManagementState
  path <- readT (slate ^. currentPath)
  let parent = fromMaybe topLevelStateIdentifier (firstOf folded path)
      ident = SubStateIdentifier parent oid
  prioVar <- newT prio
  opVar <- newT Nothing
  gds <- createGDSFromImpasse reason
  (fuelVar :: TVar (Maybe QuiescenceFuel)) <- newT fuel
  pure $ StateData ident prioVar (Just reason) opVar fuelVar gds

{-# INLINEABLE destroyState #-}
destroyState :: (HasTracer agent, HasPendingActivity agent metadata proof, HasStateMap agent metadata, Members '[WorkingMemory' metadata, Resource, Log, OpenTelemetryContext, OpenTelemetrySpan, Transactional, Reader agent] r) => StateData metadata -> Sem r ()
destroyState state = do
  info "destroying state" [("pscm.state.sid", state ^. stateIdentifier)]
  pending <- ask <&> view pendingActivity
  embedSTM $ removeStateFromPendingActivity pending (state ^. stateIdentifier)
  removeStateFromStateMap (state ^. stateIdentifier)
  stopPreservingObjectFromGC (state ^. stateIdentifier . stateObject)

destroyStateAndSubstates :: (HasTracer agent, HasPendingActivity agent metadata proof, HasStateMap agent metadata, Members '[WorkingMemory' metadata, Reader agent, Resource, Log, OpenTelemetryContext, OpenTelemetrySpan, Transactional] r) => ImpasseResolution metadata -> Sem r ()
destroyStateAndSubstates resolution = do
  chilluns <- ask <&> view (stateMap . stateChildren)
  let eliminateChildren s =
        ( do
            childrenStates <- lookupMultimapByKey (s ^. stateIdentifier) chilluns
            case childrenStates of
              Nothing -> pure ()
              Just children -> children ^! actAndDelete (act eliminateChildren)
            destroyState s
        )

  let sid = resolution ^. resolvedState
  currentStateData' <- getStateFromSID sid
  case currentStateData' of
    Nothing -> error "Current state doesn't exist?" [("pscm.state.sid", toAttribute sid)]
    Just currentStateData -> do
      eliminateChildren currentStateData
      destroyState currentStateData

currentDepth :: (HasStateManagementState agent metadata, Members [Transactional, Reader agent] r) => Sem r Natural
currentDepth = do
  slate <- ask <&> view stateManagementState
  path <- readT (slate ^. currentPath)
  pure $ fromIntegral (lengthOf folded path)

-- General idea: Treat states as a tree.
{-# INLINE runProblemSubstates #-}
runProblemSubstates ::
  forall agent metadata proof r.
  (Show metadata, HasCallStack, HasMaybeWMPreference metadata ObjectIdentifier, HasTracer agent, CreateBlankWMEMetadata metadata, HasWMESupport metadata, HasCreatingState metadata, WMEIsState metadata, HasStateManagementState agent metadata, HasStateMap agent metadata, HasPendingActivity agent metadata proof, HasTopLevelStateInitialised agent metadata, Members '[OpenTelemetrySpan, OpenTelemetryContext, Resource, WorkingMemory' metadata, KeyedState PSCMConfiguration, Log, Transactional, Reader agent] r) =>
  InterpreterFor (ProblemSubstates metadata) r
runProblemSubstates sem = do
  agent <- ask
  (UNSAFETopLevelStateInitialised !_wmi !_topstatewme) <- embedSTM $ agent ^! (topLevelStateInitialised @agent @metadata)
  interpret
    ( \i ->
        case i of
          CurrentOperator ident -> push "CurrentOperator" do
            states <- ask <&> view stateMap
            (currentState :: Maybe (StateData metadata)) <- lookupMap ident (states ^. allStates)
            currentState ^!? _Just . currentOperatorWME . act readT . _Just
          CurrentState -> getCurrentState
          InitiateSubstate reason oid prio -> push "InitiateSubstate" do
            slate <- ask <&> view stateManagementState
            currentState <- getCurrentState
            fuel <- getAt MaximumIterationsForQuiescence
            s <- makeState reason oid prio fuel
            path <- readT (slate ^. currentPath)
            let depth = (1 +) $ fromIntegral $ lengthOf folded path
            indexStateAtDepth currentState s depth
            pushDownSubstate slate s
            wmes <- makeStateWMEs (s ^. stateIdentifier) (Just currentState)

            pure wmes
          AscendToParentSubstate -> push "AscendToParentSubstate" do
            slate <- ask <&> view stateManagementState
            path <- readT (slate ^. currentPath)
            if lengthOf folded path <= 1
              then do
                error "Tried to ascend past root state" [("pscm.state.depth", toAttribute $ lengthOf folded path)]
              else do
                modifyT (\(_ :| xs) -> fromList xs) (slate ^. currentPath)
          ChildStates -> push "ChildStates" do
            chilluns <- ask <&> view (stateMap . stateChildren)
            current <- getCurrentState
            lookupMultimapByKey current chilluns
          ResolveSubstate resolution -> do
            case resolution ^. resolutionType of
              ImpasseEliminated -> push "EliminateSubstate" do
                destroyStateAndSubstates resolution
              ImpasseResolved chosen -> push "ResolveSubstate" do
                destroyStateAndSubstates resolution
                debug "Resolve substate" [("pscm.state.sid", toAttribute $ resolution ^. resolvedState), ("pscm.state.impasse_resolution", toAttribute (T.pack $ show $ chosen))]
              ImpasseRegenerated _impasse -> push "RegenerateSubstate" do
                destroyStateAndSubstates resolution
                -- TODO FIXME recreate the substate.
                error "Regenerate substate" [("pscm.state.sid", toAttribute $ resolution ^. resolvedState)]
          GetAllStates -> do
            states <- ask <&> view (stateMap . allStates)
            states ^!! elemsM . _1
          DescendIntoSubstate oid -> push "DescendIntoSubstate" do
            addAttributeWithRaw "pscm.substate.oid" oid
            slate <- ask <&> view stateManagementState
            statemap <- ask <&> view stateMap
            ident <- lookupMap oid (statemap ^. statesByObject)
            case ident of
              Nothing -> do
                critical "Substate does not exist!" [("pscm.state.oid", toAttribute oid)]
              Just i -> do
                addAttributeWithRaw "pscm.state.sid" i
                childState <- lookupMap i (statemap ^. allStates)
                case childState of
                  Nothing ->
                    error "Child substate does not exist!" [("pscm.state.sid", toAttribute i)]
                  Just childState' -> do
                    current <- getCurrentState
                    isChild <- lookupMultimap childState' current (statemap ^. stateChildren)
                    case isChild of
                      True -> do
                        pushDownSubstate slate childState'
                        pure ()
                      False -> do
                        error "Current state does not have child" ((makeAttributesWithRaw "pscm.state.current" current) <> (makeAttributesWithRaw "pscm.substate.sid" i))
          SetStatePriority ident prio -> push "SetStatePriority" do
            states <- ask <&> view stateMap
            pending <- ask <&> view pendingActivity
            pending ^! pendingActivationsBySubstate . settingPriority ident prio
            pending ^! pendingRetractionsBySubstate . settingPriority ident prio
            pending ^! pendingWMEInsertionsBySubstate . settingPriority ident prio
            pending ^! pendingWMEDeletionsBySubstate . settingPriority ident prio
            adjust ident (\(StateOrdering depth _) -> StateOrdering depth prio) (states ^. orderingsByState)
          SetStateQuiescenceFuel fuel -> push "SetStateQuiescenceFuel" do
            states <- ask <&> view stateMap
            currentState' <- getCurrentState
            currentState <- lookupMap currentState' (states ^. allStates)
            currentState ^! _Just . quiescenceFuelLimit . act (writeT fuel)
          UseFuel ident -> push "UseFuel" do
            addAttributes [("pscm.state.id", toAttribute ident)]
            states <- ask <&> view stateMap
            (currentState :: Maybe (StateData metadata)) <- lookupMap ident (states ^. allStates)
            (fuelLimit :: Maybe QuiescenceFuel) <-
              fmap
                join
                ( mapM
                    ( \(s :: StateData metadata) -> do
                        -- ewwwwwwwwwww
                        (res :: Maybe QuiescenceFuel) <- readT (s ^. quiescenceFuelLimit)
                        pure res
                    )
                    currentState
                )
            pending <- ask <&> view pendingActivity
            (fuelUsedByState :: QuiescenceFuel) <- pending ^! fuelUsed . getOrCreate (act (\_ -> pure (0 :: QuiescenceFuel))) ident
            case fuelLimit of
              Just limit | limit <= fuelUsedByState -> pure False
              _otherwise -> do
                pending ^! fuelUsed . insertingKVM ident (fuelUsedByState + 1)
                pure True
    )
    $ sem
  where
    pushDownSubstate :: (HasCallStack, HasStateManagementState s metadata, Members '[Transactional] r) => s -> StateData metadata -> Sem r ()
    pushDownSubstate slate s =
      modifyT
        ( \path -> do
            (s ^. stateIdentifier) DLNE.<| path
        )
        (slate ^. currentPath)
