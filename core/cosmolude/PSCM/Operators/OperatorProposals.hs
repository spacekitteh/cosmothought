﻿module PSCM.Operators.OperatorProposals (OperatorProposal (OperatorProposal), OperatorProposals (OperatorProposals), ChosenOperator (ChosenOperator), proposedOperator, chosenIdentifier, relevantWME, allProposals, proposeOperator, inState, fromProposals, operatorProposalWMEs, gatherOperatorProposals, proposeOperatorWME, proposals, validOperatorProposals) where

import Core.CoreTypes as CT
import Core.WorkingMemory
import Cosmolude
import qualified Data.HashMap.Strict as HMS
import qualified Data.HashSet as HS
import Data.Semigroup (Semigroup (..))
import Math.ParetoFront
import Polysemy.Reader

data OperatorProposal metadata = OperatorProposal {_proposedOperator :: ObjectIdentifier, _preference :: WMPreference ObjectIdentifier, _relevantWME :: Maybe (WME' metadata)} deriving (Eq, Show, Generic, Functor)

makeLenses ''OperatorProposal

instance HasIdentifier (OperatorProposal metadata) where
  {-# INLINE identifier #-}
  identifier = proposedOperator . identifier

deriving via ByIdentifier (OperatorProposal metadata) instance Hashable (OperatorProposal metadata)

instance HasWMPreference (OperatorProposal metadata) ObjectIdentifier where
  {-# INLINE wMPreference #-}
  wMPreference = PSCM.Operators.OperatorProposals.preference

instance (Renderable metadata) => Renderable (OperatorProposal metadata) where
  render (OperatorProposal a b wme) = render a <+> render b <+> render wme

deriving via ByRenderable (OperatorProposal metadata) instance (Renderable metadata) => ToPrimitiveAttribute (OperatorProposal metadata)

instance (Renderable metadata) => Renderable (Front (OperatorProposal metadata)) where
  render a = "Front" <+> renderList (getFront a)

instance (Renderable metadata) => Renderable (Strata (OperatorProposal metadata)) where
  render a = "Strata" <+> vsep (render <$> (getStrata a))

deriving via ByWMPreference (OperatorProposal metadata) instance Debatable (OperatorProposal metadata)

data OperatorProposals metadata = OperatorProposals {_allProposals :: Strata (OperatorProposal metadata)} deriving (Show, Generic)

instance Eq (OperatorProposals metadata) where
  -- We don't care what order the proposals are in in the fronts; thus, chuck everything into sets
  (OperatorProposals a) == (OperatorProposals b) = (nestedFold HS.singleton id a) == (nestedFold HS.singleton id b)

makeLenses ''OperatorProposals

{-# INLINE proposals #-}
proposals :: Fold (OperatorProposals metadata) (OperatorProposal metadata)
proposals = allProposals . folded

proposeOperator :: OperatorProposal metadata -> OperatorProposals metadata
proposeOperator = OperatorProposals . stratum

instance (Renderable metadata) => Renderable (OperatorProposals metadata) where
  render (OperatorProposals props) = render props

instance (Renderable metadata) => ToAttribute (OperatorProposals metadata) where
  toAttribute props = toAttribute (props ^.. proposals)

instance Semigroup (OperatorProposals metadata) where
  {-# INLINE (<>) #-}
  (OperatorProposals a) <> (OperatorProposals b) = OperatorProposals (a <> b)

instance Monoid (OperatorProposals metadata) where
  mempty = OperatorProposals mempty

data ChosenOperator metadata = ChosenOperator {_chosenIdentifier :: ObjectIdentifier, _fromProposals :: OperatorProposals metadata, _inState :: StateIdentifier} deriving (Eq, Show, Generic)

makeLenses ''ChosenOperator

-- instance Eq (ChosenOperator metadata) where
--   {-# INLINE (==) #-}
--   a == b = a ^. chosenIdentifier == b ^. chosenIdentifier && a ^. inState == b ^. inState

instance HasIdentifier (ChosenOperator metadata) where
  {-# INLINE identifier #-}
  identifier = chosenIdentifier . identifier

deriving via ByIdentifier (ChosenOperator metadata) instance (Hashable metadata) => Hashable (ChosenOperator metadata)

instance Renderable (ChosenOperator metadata) where
  render (ChosenOperator chosen _ _) = render chosen
{-# INLINE validOperatorProposals #-}
validOperatorProposals :: forall metadata. HasMaybeWMPreference (WME' metadata) ObjectIdentifier => Fold (WME' metadata) (WME' metadata)
validOperatorProposals = filtered (\op -> has (CT.preference @(WME' metadata) @ObjectIdentifier . _Just) op && has (value . _ObjectIdentity) op)
                                       
{-# INLINEABLE gatherOperatorProposalWMEs #-}
gatherOperatorProposalWMEs :: forall agent metadata r. (HasTracer agent, HasMaybeWMPreference (WME' metadata) ObjectIdentifier, Members '[Reader agent, WorkingMemory' metadata,  Log, OpenTelemetryContext, OpenTelemetrySpan, Resource] r) => StateIdentifier -> Sem r [WME' metadata]
gatherOperatorProposalWMEs ident = push "gatherOperatorProposalWMEs" do
  let stateOID = ident ^. stateObject
  results <- (operatorPreferenceTestPattern stateOID) ^!! wmesMatchingPattern . validOperatorProposals

  pure results

--  allOperatorsIncludingCurrentOperator <- findWMEsByPattern (operatorTestPattern stateOID)
--  pure $ allOperatorsIncludingCurrentOperator ^.. folded . filtered (\op -> has (CT.preference @(WME' metadata) @ObjectIdentifier . _Just) op && has (value . _ObjectIdentity) op)

{-# INLINE operatorProposalWMEs #-}
operatorProposalWMEs :: forall agent metadata r. (HasCallStack, HasTracer agent, HasMaybeWMPreference (WME' metadata) ObjectIdentifier, Members '[WorkingMemory' metadata, Reader agent, Log, OpenTelemetryContext, OpenTelemetrySpan, Resource] r) => MonadicFold (Sem r) StateIdentifier (WME' metadata)
operatorProposalWMEs = (act gatherOperatorProposalWMEs) . folded

{-# INLINE proposeOperatorWME #-}
proposeOperatorWME :: (HasCallStack, HasMaybeWMPreference metadata ObjectIdentifier) => WME' metadata -> OperatorProposal metadata
proposeOperatorWME op = OperatorProposal (op ^??! value . _ObjectIdentity) (op ^??! CT.preference . _Just) (Just op)

{-# INLINEABLE gatherOperatorProposals #-}
gatherOperatorProposals :: forall agent metadata r. (HasCallStack, Show metadata, Renderable metadata, HasTracer agent, HasMaybeWMPreference metadata ObjectIdentifier, Members '[Reader agent, WorkingMemory' metadata, Log, OpenTelemetryContext, OpenTelemetrySpan, Resource] r) => StateIdentifier -> Sem r ([OperatorProposal metadata], OperatorProposals metadata)
gatherOperatorProposals ident = push "gatherOperatorProposals" $ do
  proposals <- ident ^!! operatorProposalWMEs
  let operators =  proposals ^.. folded .  to proposeOperatorWME
  addAttributes (HMS.singleton "pscm.substate.sid" (toAttribute ident))
  addAttributeWithRaw "pscm.proposedOperators" operators
  --  operators <-
  pure (operators, operators ^. folded . to proposeOperator) -- look up all object properties of the state wme, then filter to just be operator proposals
