{-# LANGUAGE StrictData #-}

module PSCM.Errors where

import Control.Exception (Exception (..))
import Core.Rule.Match
import Cosmolude
import Prettyprinter.Render.String

data SubstateIdentificationExceptionType
  = MissingStateVariableInRule
  | MissingStateVariableInSubstitution
  | MissingMatchedObject
  | StateIdentityNotFound ObjectIdentifier [(ObjectIdentifier, StateIdentifier)]
  deriving (Eq, Ord, Generic, Hashable)

instance Show SubstateIdentificationExceptionType where
  show MissingStateVariableInRule = "Missing state variable in rule"
  show MissingStateVariableInSubstitution = "Missing state variable in unification substitution"
  show MissingMatchedObject = "Missing matched object"
  show (StateIdentityNotFound desired states) = "State identity not found.\nDesired: " ++ show desired ++ "\nStates:\n" ++ show states

data SubstateIdentificationException metadata proof
  = SubstateIdentificationException {_sidExcType :: SubstateIdentificationExceptionType, _relevantMatch :: RuleMatch' metadata proof}
  deriving (Eq, Show, Exception, Generic, Hashable)

makeLenses ''SubstateIdentificationException
makeClassyPrisms ''SubstateIdentificationException

instance HasRule (SubstateIdentificationException metadata proof) where
  {-# INLINE rule #-}
  rule = relevantMatch . rule

data ProductionSystemException metadata proof = SubstateID (SubstateIdentificationException metadata proof) deriving (Show, Exception)

data PSCMException metadata proof = InProductionSystem (ProductionSystemException metadata proof) deriving (Show, Exception)

data TestOutcome
  = TestWasSuccessful
  | TestFailed {_docStream :: CoreDocStream}
  | NoTestCompleted
  deriving (Eq, Generic, Hashable)
  deriving (Pretty) via ByRenderable TestOutcome

instance Renderable TestOutcome where
  render TestWasSuccessful = "succeeded"
  render (TestFailed err) = "failed with" <+> dquotes (render (renderString err))
  render NoTestCompleted = "no test completed"

class WithTestOutcome ty where
  testOutcome :: Lens' ty (TVar TestOutcome)
