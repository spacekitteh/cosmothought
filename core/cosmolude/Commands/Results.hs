module Commands.Results where
import Core.Rule.Match
import Core.CoreTypes
import System.FilePath
import Cosmolude hiding (group, Named, Any(..))
import Data.Semigroup



import GHC.Exts hiding (Any)
import Control.Exception (SomeException)
import Data.Either
    

data CommandResult' metadata proof
  = SideEffect
  | Message CoreDoc
  | Firings (HashSet (RuleActivation' metadata proof), HashSet (RuleRetraction' metadata proof))
  | ValueResult Value
  | IdentifierResult Identifier
  | FileWrittenTo FilePath
  | CommandError (Either SomeException CoreDoc)
  | TestSucceeds
  | TestFails CoreDoc
  deriving (Show, Generic)

makeClassyPrisms ''CommandResult'
           
data CommandResults' metadata proof = CommandResults {_commandResults :: Seq (CommandResult' metadata proof)} deriving (Show)

instance IsList (CommandResults' metadata proof) where
  type Item (CommandResults' metadata proof) = CommandResult' metadata proof
  {-# INLINE fromList #-}
  fromList = CommandResults . GHC.Exts.fromList
  {-# INLINE toList #-}
  toList (CommandResults r) = GHC.Exts.toList r

makeLenses ''CommandResults'

{-# INLINE resultingIn #-}
resultingIn :: Review (CommandResults' metadata proof) [CommandResult' metadata proof]
resultingIn = unto GHC.Exts.fromList

instance Semigroup (CommandResults' metadata proof) where
  {-# INLINE CONLIKE (<>) #-}
  (CommandResults a) <> (CommandResults b) = CommandResults (a <> b)

instance Monoid (CommandResults' metadata proof) where
  {-# INLINE CONLIKE mempty #-}
  mempty = CommandResults mempty                

includesTestSucceeds :: CommandResults' metadata proof -> Bool
includesTestSucceeds (CommandResults res) = has (folded . _TestSucceeds) res
includesTestFailures :: CommandResults' metadata proof -> [CoreDoc]
includesTestFailures (CommandResults res) = res ^.. folded . _TestFails
includesCommandErrors :: CommandResults' metadata proof -> ([Either SomeException CoreDoc])
includesCommandErrors (CommandResults res) = res^..folded . _CommandError

data TestResults = TestSuccessful
                 | TestErrors [SomeException] [CoreDoc]
                 deriving stock Show

class AsTestResults t where
    asTestResult :: t -> Maybe TestResults
instance AsTestResults (CommandResult' metadata proof) where
    asTestResult TestSucceeds = Just TestSuccessful
    asTestResult (TestFails doc) = Just (TestErrors [] [doc])
    asTestResult (CommandError (Left exc)) = Just (TestErrors [exc] [])
    asTestResult (CommandError (Right d)) = Just (TestErrors [] [d])
    asTestResult _ = Nothing
instance Semigroup TestResults where
    TestSuccessful <> rhs = rhs
    lhs <> TestSuccessful = lhs
    (TestErrors lex lds) <> (TestErrors rex rds) = TestErrors (lex <> rex) (lds <> rds)
instance Monoid TestResults where
    mempty = TestSuccessful
             
instance AsTestResults (CommandResults' metadata proof) where
    asTestResult (CommandResults res) = res ^. folded . to asTestResult 

                   
