module Commands.ProductionSystem where
import Core.Patterns.PatternSyntax
import Core.Rule
import Core.WME

import Cosmolude hiding (group, Named, Any(..))
import PSCM.Configuration
import Data.List.NonEmpty (NonEmpty)


data AddCommand = AddRules (NonEmpty Rule)
                | AddWMEs (NonEmpty WME)
                  deriving stock (Eq, Show, Generic)
makeClassyPrisms ''AddCommand                           
data RemoveCommand = RemoveRulesWithIdentifiers (NonEmpty RuleIdentifier)
                   | RemoveWMEs (NonEmpty WME)
                     deriving stock (Eq, Show, Generic)
makeClassyPrisms ''RemoveCommand
data ExistsCommand = ExistsWME WMEPattern deriving stock (Eq, Show, Generic)
makeClassyPrisms ''ExistsCommand                   
data OIDStatsCommand = NumberOfReferencesToOID ObjectIdentifier deriving stock (Eq, Show, Generic)
makeClassyPrisms ''OIDStatsCommand
data SetProductionRuleActivityCommand =  ActivtyOnRemoval ActivityOnRuleRemoval deriving stock (Eq, Show, Generic)
makeClassyPrisms ''SetProductionRuleActivityCommand                     
