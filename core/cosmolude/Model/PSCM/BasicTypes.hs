{-# LANGUAGE GeneralisedNewtypeDeriving #-}
module Model.PSCM.BasicTypes where
import Data.Word
import Data.Semigroup

import Data.Monoid    
import Prelude (Eq, Ord, Enum, Bounded, Num, Show, Read, Integral, Real)
import Data.Hashable
import GHC.Generics  
import Numeric.Natural    
import Control.Lens
import Algebra.Lattice
import Algebra.Lattice.Ordered    
import Algebra.PartialOrd

newtype QuiescenceFuel = QuiescenceFuel Word deriving newtype (Eq, Ord, Enum, Bounded, Num, Show, Read, Hashable)
newtype PSCMCycleCount = PSCMCycleCount Word64 deriving newtype (Eq, Ord, Enum, Bounded, Num, Show, Read, Hashable, Real, Integral)
data PSCMLoopPhase
  = InputElaboration
  | OperatorProposalAndEvaluation
  | OperatorDecision
  | OperatorElaboration
  | OperatorApplication
  | OutputToEnvironment
  deriving stock (Eq, Show, Generic, Read)
  deriving anyclass (Hashable)
           
newtype ObjectLevel = ObjectLevel {_level :: Natural}
    deriving newtype (Eq, Ord, Enum, Num, Show, Read, Hashable)
    deriving (PartialOrd, Lattice) via Ordered ObjectLevel
instance BoundedJoinSemiLattice ObjectLevel where
    bottom = 0

makeClassy ''ObjectLevel

           
data PSCMStatisticsForIteration  = PSCMStatisticsForIteration
                                   deriving stock (Eq, Show, Read, Generic)
                                   deriving anyclass (Hashable)

instance Semigroup PSCMStatisticsForIteration where
    {-# INLINE (<>) #-}
    _ <> _ = PSCMStatisticsForIteration
instance Monoid PSCMStatisticsForIteration where
    mempty = PSCMStatisticsForIteration

data PSCMStatistics = PSCMStatistics {_pscmCycleCount :: PSCMCycleCount,
                                      _currentPhase :: PSCMLoopPhase,
                                      _currentStateDepth :: Natural,
                                      _totalRules :: Natural
                                     } deriving stock (Eq, Show, Read, Generic)
                                                                     deriving anyclass (Hashable)
newPSCMStatistics :: PSCMStatistics
newPSCMStatistics = PSCMStatistics {_pscmCycleCount = 0, _currentPhase = InputElaboration, _currentStateDepth = 0, _totalRules = 0}
                                                                              
makeClassy ''PSCMStatistics

