module Model.PSCM.Productions where
import Model.DomainModelling
--import Model.PSCM.Operator

data GetPendingActivityFor state
type role GetPendingActivityFor  nominal


data ActivateRule
data RetractRule

data ComputeChangesToWorkingMemory
data ConsolidateChangesToWorkingMemory
data ApplyChangesToWorkingMemory

data ComputeChangesToMatches

data ComputeActivityFromChangedMatches
data NotifyOfNewPendingActivity



processRuleMatchChanges :: (Members '[
                              Tagged ComputeChangesToMatches (Input changedMatches),
                              Tagged ComputeActivityFromChangedMatches (Methodology changedMatches (pendingActivations, pendingRetractions)),
                              Tagged NotifyOfNewPendingActivity (Output pendingActivations),
                              Tagged NotifyOfNewPendingActivity (Output pendingRetractions)
                                    ]
                         r ) => Sem r ()
processRuleMatchChanges = do

  changedMatches <- tag @ComputeChangesToMatches input
  (newPendingActivations,newPendingRetractions) <- tag @ComputeActivityFromChangedMatches $ process changedMatches
  tag @NotifyOfNewPendingActivity $ output newPendingActivations
  tag @NotifyOfNewPendingActivity $ output newPendingRetractions


data ProcessRuleMatchChanges
data ProcessPendingActivityAndApplyToWorkingMemory
data SeeWhatWorkingMemoryChangesDid
data SplitOutPendingActivations
data SplitOutPendingRetractions

-- processActivity :: (Monoid workingMemoryChanges, Members '[Tagged SplitOutPendingActivations (Methodology pendingActivity pendingActivations),
--                                  Tagged SplitOutPendingRetractions (Methodology pendingActivity pendingRetractions),
--                                  Tagged ProcessPendingActivityAndApplyToWorkingMemory (Methodology (pendingActivations, pendingRetractions) workingMemoryChanges),
--                                  Tagged SeeWhatWorkingMemoryChangesDid (Methodology workingMemoryChanges changedMatches),
--                                  Tagged ProcessRuleMatchChanges (Methodology changedMatches (pendingActivations, pendingRetractions))
--                                  ] r) => InterpretersFor '[(Tagged ProcessPendingActivations (Methodology pendingActivity changesToMake)),                                                                                                                                                  Tagged ApplyChanges (Output changesToMake)] r
-- processActivity = interpret (\case
--   Output changesToMake -> do

--     )
--   . untag . interpret (\case
--                           Process pendingActivity -> do
--                             a
--                             -- pendingActivations <- tag @SplitOutPendingActivations $ process pendingActivity
--                             -- pendingRetractions <- tag @SplitOutpendingRetractions $ process pendingActivity
--                             -- pendingActivationsHandled <- runInputConst pendingActivations
--                             ) . untag

-- processRuleMatchChanges' :: Members '[Methodology changedMatches (pendingActivations, pendingRetractions)] r =>  InterpretersFor '[Tagged ComputeChangesToMatches (Input changedMatches),
--                               Tagged ComputeActivityFromChangedMatches (Methodology changedMatches (pendingActivations, pendingRetractions)),
--                               Tagged NotifyOfNewPendingActivity (Output pendingActivations),
--                               Tagged NotifyOfNewPendingActivity (Output pendingRetractions)] r
-- processRuleMatchChanges' sem =  
                         


processPendingActivityToRuleMatchChanges :: (Members '[                               Tagged (GetPendingActivityFor state) (Input pendingActivations),
                              Tagged (GetPendingActivityFor state) (Input pendingRetractions),
                              Tagged ComputeChangesToWorkingMemory (Methodology pendingActivations workingMemoryChangesFromActivations),
                              Tagged ComputeChangesToWorkingMemory (Methodology pendingRetractions workingMemoryChangesFromRetractions),
                              Tagged ConsolidateChangesToWorkingMemory (Methodology (workingMemoryChangesFromActivations,workingMemoryChangesFromRetractions) workingMemoryChanges),
                              Tagged ApplyChangesToWorkingMemory (Output workingMemoryChanges)
                                               ] r) => Sem r ()
processPendingActivityToRuleMatchChanges = do
  pendingActivations <- tag @(GetPendingActivityFor _) input
  workingMemoryChangesFromActivations <- tag @ComputeChangesToWorkingMemory $ process pendingActivations
  pendingRetractions <- tag @(GetPendingActivityFor _) input
  workingMemoryChangesFromRetractions <- tag @ComputeChangesToWorkingMemory $ process pendingRetractions
  workingMemoryChanges <- tag @ConsolidateChangesToWorkingMemory $ process (workingMemoryChangesFromActivations, workingMemoryChangesFromRetractions)
  tag @ApplyChangesToWorkingMemory $ output workingMemoryChanges
