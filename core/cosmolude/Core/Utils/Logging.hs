{-# OPTIONS_GHC -Wwarn=orphans #-}
module Core.Utils.Logging     (module Core.Utils.Logging, module OpenTelemetry.Attributes) where
import Prettyprinter.Render.Text
import qualified System.IO.Unsafe as UNSAFE (unsafePerformIO)    
import Control.Concurrent.STM
import qualified Data.HashSet as HS
import qualified Data.HashMap.Lazy as HML    
import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import Control.Lens hiding (Level)
import GHC.Stack
import Data.Bits (xor)
import Polysemy.Transaction
import Prettyprinter
import GHC.Exts (SPEC(..), fromString, IsList(..), Item)
import Polysemy.Output
import DiPolysemy (Di(..), runDiNoop)
import Prelude hiding (log)
import Control.Monad
import Data.Hashable
import Core.Utils.Rendering.CoreDoc
import qualified Di.Core as Di
import qualified Di.Df1 as Dif1
import qualified DiPolysemy as DiP
import qualified Df1
import Df1 (Path, Level)
import qualified Polysemy.Reader as P
import Polysemy
import Polysemy.OpenTelemetry
-- import Polysemy.Input
import qualified OpenTelemetry.Trace.Core as OT 
import Data.Text
import Data.Foldable (elem)
import Polysemy.Resource
import  OpenTelemetry.Attributes (Attribute)


showCoreDocAsText :: CoreDoc -> Text
showCoreDocAsText msg = renderStrict $ layoutPretty defaultLayoutOptions $ fuse Deep msg
    
-- log :: Member Log r => Level -> CoreDoc -> Sem r ()
-- log level msg = DiP.log level (showCoreDocAsText msg)

{-# INLINE push #-}                
push ::(OT.HasTracer agent, HasCallStack, Members '[ P.Reader agent, Resource, OpenTelemetryContext, OpenTelemetrySpan, Log] r) => Text -> Sem r a -> Sem r a
push n sem = withFrozenCallStack $ pushWith n OT.defaultSpanArguments sem
            
{-# INLINE pushWith #-}             
pushWith :: forall agent r a.  (OT.HasTracer agent, HasCallStack, Members '[ P.Reader agent, Resource, OpenTelemetryContext, OpenTelemetrySpan, Log] r) => Text -> OT.SpanArguments -> Sem r a -> Sem r a
pushWith n args sem = withFrozenCallStack  $ P.inputToReader  $ inSpan n args (DiP.push (Df1.segment n) (raise sem))
{-# INLINE pushProducer #-}
pushProducer :: (OT.HasTracer agent, HasCallStack, Members '[ P.Reader agent, Resource, OpenTelemetryContext, OpenTelemetrySpan, Log] r) => Text -> Sem r a -> Sem r a
pushProducer n sem = withFrozenCallStack $ pushWith n (OT.defaultSpanArguments {OT.kind = OT.Producer}) sem 
{-# INLINE pushConsumer #-}
pushConsumer :: (OT.HasTracer agent, HasCallStack, Members '[ P.Reader agent, Resource, OpenTelemetryContext, OpenTelemetrySpan, Log] r) => Text -> Sem r a -> Sem r a
pushConsumer n sem = withFrozenCallStack $ pushWith n (OT.defaultSpanArguments {OT.kind = OT.Consumer}) sem

                     
attr :: (Members '[OpenTelemetryContext, OpenTelemetrySpan, Log] r) => Text -> Text -> Sem r a -> Sem r a
attr k v sem = do
  withCurrentSpan \span ->
               whenSpanIsRecording span  do
                 addAttributes [(k, OT.toAttribute v)]
  -- DiP.attr (Df1.key k) v
  sem
    
debug'' :: (Members  '[Log] r) => CoreDoc -> Sem r ()
debug'' ~msg' | ~msg <-  show . fuse Deep $ msg'  = do
  DiP.debug_ . fromString $ msg

df1LevelToOTSeverity :: Df1.Level -> (Text, Int)
df1LevelToOTSeverity Df1.Debug = ("DEBUG", 5)
df1LevelToOTSeverity Df1.Info = ("INFO", 9)
df1LevelToOTSeverity Df1.Notice = ("INFO2", 10)
df1LevelToOTSeverity Df1.Warning = ("WARN",13)
df1LevelToOTSeverity Df1.Error = ("ERROR", 17)
df1LevelToOTSeverity Df1.Critical = ("ERROR2",18)
df1LevelToOTSeverity Df1.Alert = ("ERROR3", 19)
df1LevelToOTSeverity Df1.Emergency = ("FATAL",21)
     
log :: Members  '[Log, OpenTelemetryContext, OpenTelemetrySpan] r =>  Df1.Level -> CoreDoc -> Sem r ()
log level msg = emitLogMessage' (df1LevelToOTSeverity level) ([] :: [(Text, Bool)]) msg
{-# INLINE emitLogMessage #-}
emitLogMessage :: (IsList attrs, Item (attrs) ~ (Text,x), Members  '[OpenTelemetryContext, OpenTelemetrySpan] r, OT.ToAttribute x) => Text -> Int -> attrs ->  CoreDoc ->  Sem r ()
emitLogMessage ~severityName ~severityNumber ~extraAttributes ~msg'  | ~msg <- showCoreDocAsText  msg'  = do
  addEvent  msg $ HML.fromList [("log.severity_text", OT.toAttribute severityName), ("log.severity_number", OT.toAttribute severityNumber) ]  <> (HML.fromList $ (toList extraAttributes) & traversed .  _2 %~ OT.toAttribute)
  when (severityNumber >= 13) do
    setSpanStatus (OT.Error  msg)

     
{-# INLINE emitLogMessage' #-}
emitLogMessage' :: (IsList attrs, Item (attrs) ~ (Text,x), Members  '[Log,OpenTelemetryContext, OpenTelemetrySpan] r,  OT.ToAttribute x) => (Text, Int) -> attrs ->  CoreDoc ->  Sem r ()
emitLogMessage'  (~severityName, ~severityNumber) ~extraAttributes ~msg' = do
  emitLogMessage severityName severityNumber extraAttributes msg'
  --log level msg'

debug  :: (IsList attrs, Item (attrs) ~ (Text,x), Members  '[Log,OpenTelemetryContext, OpenTelemetrySpan] r, OT.ToAttribute x) => CoreDoc -> attrs -> Sem r ()
debug  ~msg ~attributes = withCurrentSpan \span -> whenSpanIsRecording span do
  emitLogMessage' (df1LevelToOTSeverity Df1.Debug)  attributes msg 

debug' :: (Members  '[Log,OpenTelemetryContext, OpenTelemetrySpan] r) => CoreDoc -> Sem r ()
debug' ~msg = debug msg ([] :: [(Text, Bool)])
info :: (IsList attrs, Item (attrs) ~ (Text,x), Members  '[Log,OpenTelemetryContext, OpenTelemetrySpan] r, OT.ToAttribute x) => CoreDoc  -> attrs -> Sem r ()
info ~msg ~attributes = emitLogMessage' (df1LevelToOTSeverity Df1.Info) attributes  msg
info' :: (Members  '[Log,OpenTelemetryContext, OpenTelemetrySpan] r) => CoreDoc -> Sem r ()
info' ~msg = info msg ([] :: [(Text, Bool)])
notice :: (IsList attrs, Item (attrs) ~ (Text,x), Members  '[Log,OpenTelemetryContext, OpenTelemetrySpan] r, OT.ToAttribute x) => CoreDoc  -> attrs -> Sem r ()
notice ~msg ~attributes = emitLogMessage' (df1LevelToOTSeverity Df1.Notice) attributes  msg            
notice' :: (Members  '[Log,OpenTelemetryContext, OpenTelemetrySpan] r) => CoreDoc -> Sem r ()
notice' ~msg = notice msg ([] :: [(Text, Bool)])
warning :: (IsList attrs, Item (attrs) ~ (Text,x), Members  '[Log,OpenTelemetryContext, OpenTelemetrySpan] r, OT.ToAttribute x) => CoreDoc -> attrs -> Sem r ()
warning ~msg ~attributes = emitLogMessage' (df1LevelToOTSeverity Df1.Warning) attributes msg
error :: (IsList attrs, Item (attrs) ~ (Text,OT.Attribute), Members  '[Log,OpenTelemetryContext, OpenTelemetrySpan] r) => CoreDoc -> attrs -> Sem r ()
error ~msg ~attributes= emitLogMessage' (df1LevelToOTSeverity Df1.Error)  attributes msg
critical :: (IsList attrs, Item (attrs) ~ (Text,OT.Attribute), Members  '[Log,OpenTelemetryContext, OpenTelemetrySpan] r) => CoreDoc -> attrs -> Sem r ()
critical ~msg ~attributes= emitLogMessage' (df1LevelToOTSeverity Df1.Critical) attributes msg
alert :: (IsList attrs, Item (attrs) ~ (Text,OT.Attribute), Members  '[Log,OpenTelemetryContext, OpenTelemetrySpan] r ) => CoreDoc -> attrs -> Sem r ()
alert ~msg ~attributes = emitLogMessage' (df1LevelToOTSeverity Df1.Alert) attributes msg
emergency :: (IsList attrs, Item (attrs) ~ (Text,OT.Attribute), Members  '[Log,OpenTelemetryContext, OpenTelemetrySpan] r) => CoreDoc -> attrs -> Sem r ()
emergency ~msg ~attributes = emitLogMessage' (df1LevelToOTSeverity Df1.Emergency) attributes msg


makeAttributesWithRaw :: (Renderable a, Show a) =>  Text -> a -> HML.HashMap Text OT.Attribute
makeAttributesWithRaw ~attrName ~obj = HML.fromList [(attrName, OT.toAttribute . pack . show . render $ obj),
                                      (attrName <> "_raw", OT.toAttribute . pack . show $ obj)]
                           
addAttributeWithRaw :: (HasWellKnownAttributes a, Members  '[OpenTelemetryContext, OpenTelemetrySpan] r, Show a, Renderable a) => Text -> a ->  Sem r ()
addAttributeWithRaw ~attrName ~obj  = addAttributes ( HML.union (makeAttributesWithRaw attrName obj) (toAttributes obj))
                           
             
instance Hashable (Dif1.Level) where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt lvl = hashWithSalt salt (fromEnum lvl)

instance Hashable Dif1.Path where
    {-# INLINE hashWithSalt #-}
    hashWithSalt salt (Df1.Push seg) = hashWithSalt salt (Df1.unSegment seg)
    hashWithSalt salt (Df1.Attr k v) = hashWithSalt salt (Df1.unKey k) `xor` hashWithSalt salt (Df1.unValue v)

type LogMessage = Df1.Log


type DiType = Di.Di Dif1.Level Dif1.Path Dif1.Message
type Log = DiP.Di Dif1.Level Dif1.Path Dif1.Message

{-# INLINE discardStandardLog #-}
discardStandardLog :: forall r. InterpreterFor Log r
discardStandardLog = runDiNoop
{-# INLINE runDiReader #-}
runDiReader :: forall r. Members '[ Transactional, P.Reader DiType] r => InterpreterFor Log r
runDiReader = runDiReader' SPEC where
  runDiReader' :: SPEC -> InterpreterFor Log r
  runDiReader' !sPEC = interpretH $ \case
      Log level msg -> do
        di <- P.ask
              -- TODO add OT  logging events here
        (Di.log' embedSTM di level msg) >>= pureT
      Flush         -> do
        di <- P.ask 
        (Di.flush' embedSTM di) >>= pureT
      Local f m     -> do
        m' <- runDiReader' sPEC <$> runT m
        raise $ P.local  f m'
      Fetch -> do
        di <- Just <$> P.ask 
        pureT di

{-# INLINE runDiOutput #-}
runDiOutput :: forall r. Members '[Output (Dif1.Level,Dif1.Message), Transactional] r => InterpreterFor Log r
runDiOutput = runDiOutput' SPEC where
  runDiOutput' :: SPEC -> InterpreterFor Log r
  runDiOutput' !sPEC = interpretH $ \case
      Log level msg -> do
        output (level,msg) >>= pureT
      Flush         -> do
         pureT ()
      Local _f m     -> do
        m' <- runDiOutput' sPEC <$> runT m
        raise m'
      Fetch -> do        
        pureT Nothing


        
{-# INLINE runDi #-}
runDi :: Members '[Transactional] r => DiType -> InterpreterFor Log r
runDi di = P.runReader di . runDiReader . raiseUnder

           

data LogConfigurationData = LogConfigurationData {
      _minimumLevel :: TVar Dif1.Level,
      _superPath :: TVar (Seq Path),
      _includePaths :: TVar (Maybe (HS.HashSet Path)),
      _excludePaths :: TVar (HS.HashSet Path)
    }
makeClassy ''LogConfigurationData

{-# INLINE newLogConfigurationData #-}           
newLogConfigurationData :: STM LogConfigurationData
newLogConfigurationData = LogConfigurationData <$> newTVar Df1.Debug <*> newTVar  Seq.empty <*> newTVar Nothing <*> newTVar HS.empty

resetLogConfigurationData :: LogConfigurationData -> STM ()
resetLogConfigurationData (LogConfigurationData min super incl excl) = do
  writeTVar min Df1.Debug
  writeTVar super Seq.empty
  writeTVar incl Nothing
  writeTVar excl HS.empty
                          
data LogConfiguration a where
  MinimumLevel :: LogConfiguration Dif1.Level
  SuperPath :: LogConfiguration (Seq Path)
  IncludePaths :: LogConfiguration (Maybe (HS.HashSet Path))
  ExcludePaths :: LogConfiguration (HS.HashSet Path) 

{-# INLINE logConfigurationToVariables #-}
logConfigurationToVariables :: LogConfigurationData -> LogConfiguration a -> TVar a
logConfigurationToVariables logFilterSettings MinimumLevel = logFilterSettings^.minimumLevel
logConfigurationToVariables logFilterSettings SuperPath = logFilterSettings^.superPath
logConfigurationToVariables logFilterSettings IncludePaths = logFilterSettings^.includePaths
logConfigurationToVariables logFilterSettings ExcludePaths = logFilterSettings^.excludePaths

           
{-# INLINEABLE filterByLogSettings #-}
filterByLogSettings :: (HasLogConfigurationData agent, HasCallStack) =>  agent -> Df1.Level -> Seq Df1.Path -> Df1.Message -> Bool
filterByLogSettings agent l p _m   = l >= minLevel  && path == Seq.take pthlength p  && include && not exclude where
  (LogConfigurationData lvl pth incl excl) = agent^.logConfigurationData                                 
  pthlength = Seq.length path
  path = UNSAFE.unsafePerformIO (readTVarIO pth)
  minLevel = UNSAFE.unsafePerformIO  (readTVarIO lvl)
  include = case UNSAFE.unsafePerformIO (readTVarIO incl) of
    Nothing -> True
    Just (includes) -> allOf folded (\a -> Data.Foldable.elem a p) includes
  excludes = UNSAFE.unsafePerformIO (readTVarIO excl)
  exclude =  anyOf folded (\a -> HS.member a excludes) p


                              


