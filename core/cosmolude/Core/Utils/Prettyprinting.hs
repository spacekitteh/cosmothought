{-# OPTIONS_GHC -Wwarn=orphans -Wno-incomplete-patterns#-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE EmptyCase #-}
module Core.Utils.Prettyprinting (module Core.Utils.Prettyprinting, module Core.Utils.Rendering.CoreDoc) where

import Prettyprinter
import Core.Utils.Rendering.CoreDoc
import Data.Hashable
namedBracket :: Doc a -> Doc a -> Doc a
namedBracket intype body = "⟦" <> intype <> "|" <> body <> "⟧"




 
                     
         
type CoreDocStream = SimpleDocStream CoreAnnotations

{- Orphan instance for CoreDocStream :( -}
instance Hashable CoreDocStream
