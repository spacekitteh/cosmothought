{-# LANGUAGE CPP #-}
module Core.Utils.Debugging (gcIfDebug) where
import Prelude
#ifdef DEBUG
import System.Mem

#endif
import Polysemy
import GHC.Stack

gcIfDebug :: (HasCallStack, Members '[Embed IO] r) => Sem r ()
gcIfDebug = do
#ifdef DEBUG
 embed performGC
{-# NOINLINE gcIfDebug #-}
#else
 pure ()
{-# INLINE gcIfDebug #-}
#endif
