{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE EmptyCase #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
{-# OPTIONS_GHC -Wwarn=orphans #-}
module Core.Utils.Rendering.CoreDoc where
import Data.Data
import Model.PSCM.BasicTypes
import Core.Identifiers.IdentifierType
--import {-# SOURCE #-} Core.Truth
import Data.Char
--import Data.Bits (xor)
import Data.Word    
import Data.Hashable
import Data.Interned
import Data.Foldable
import Data.Interned.Text
import Data.Maybe
import Data.Refined
import Data.Text (Text)
import Prettyprinter.Render.Text as Textual
import Data.The
import Prettyprinter.Internal.Type
import Di.Df1 (Level)
import qualified Data.HashSet as HS
import GHC.Generics
import Numeric.Natural
import Prettyprinter
import Prelude
import Data.List.NonEmpty (NonEmpty)
import Control.Lens ((^..), folded, Plated(..), rewrite)
import OpenTelemetry.Trace.Core
import Error.Diagnose.Style as EDS hiding (defaultStyle)
import Prettyprinter.Render.TerminalModified
--import Data.String (fromString)
--import Prettyprinter.Render.Util.StackMachine
    
import Core.Truth
import Core.Variable
import Core.Utils.MetaTypes 
    
data CoreAnnotations
  = IDAnnotation Identifier
  | AbstractTruthAnnotation WMETruth
  | ConcreteTruthAnnotation ConcreteTruth
  | DiagnosticAnnotation (EDS.Annotation CoreAnnotations)
  deriving (Eq, Generic, Hashable, Data)
deriving instance Data (EDS.Annotation CoreAnnotations)
deriving instance Hashable (EDS.Annotation CoreAnnotations)
{-instance Eq EDS.Annotation where
    {-# INLINE (==) #-}
    (ThisColor a) == (ThisColor b) = a == b
    MaybeColor == MaybeColor = True
    WhereColor == WhereColor = True
    HintColor == HintColor = True
    FileColor == FileColor = True
    RuleColor == RuleColor = True
    (KindColor a) == (KindColor b) = a == b
    NoLineColor == NoLineColor = True
    (MarkerStyle a) == (MarkerStyle b) = a == b
    CodeStyle == CodeStyle = True
    _ == _ = False
                  
instance Hashable EDS.Annotation where
    {-# INLINE hashWithSalt #-}
    hashWithSalt salt (ThisColor b) = hashWithSalt salt (0::Int) `xor` hashWithSalt salt b
    hashWithSalt salt MaybeColor = hashWithSalt salt (1::Int)
    hashWithSalt salt WhereColor = hashWithSalt salt (2::Int)
    hashWithSalt salt HintColor = hashWithSalt salt (3::Int)
    hashWithSalt salt FileColor = hashWithSalt salt (4::Int)
    hashWithSalt salt RuleColor = hashWithSalt salt (5::Int)
    hashWithSalt salt (KindColor b) = hashWithSalt salt (6::Int) `xor` hashWithSalt salt b
    hashWithSalt salt NoLineColor = hashWithSalt salt (7::Int)
    hashWithSalt salt (MarkerStyle anno) = hashWithSalt salt (8::Int) `xor` hashWithSalt salt anno
    hashWithSalt salt CodeStyle = hashWithSalt salt (9::Int)-}
           
instance Show CoreAnnotations where
  show (IDAnnotation i) = show i
  show (AbstractTruthAnnotation truth) = show truth
  show (ConcreteTruthAnnotation truth) = show truth
  show (DiagnosticAnnotation _) = "Diagnostic annotation"
instance Renderable CoreAnnotations where
    render = viaShow
type CoreDoc = Doc CoreAnnotations

    
defaultStyle :: CoreDoc -> Doc AnsiStyle
defaultStyle = Prettyprinter.reAnnotate style
  where
    style :: CoreAnnotations -> AnsiStyle
    style = \case
      DiagnosticAnnotation a -> case a of
        ThisColor isError -> color if isError then Red else Yellow
        MaybeColor -> color Magenta
        WhereColor -> colorDull Blue
        HintColor -> color Cyan
        FileColor -> bold <> colorDull Green
        RuleColor -> bold <> color Black
        KindColor isError -> bold <> style (DiagnosticAnnotation (ThisColor isError))
        NoLineColor -> bold <> colorDull Magenta
        MarkerStyle st ->
          let ann = style (DiagnosticAnnotation st)
           in if ann == style (DiagnosticAnnotation CodeStyle)
                then ann
                else underlinedWith (Just CurlyUnderline) (Just (Red)) <> bold <> ann
        CodeStyle -> color White
      IDAnnotation ident -> hyperlinked (show ident)
      AbstractTruthAnnotation _ -> mempty -- TODO
      ConcreteTruthAnnotation _ -> mempty -- TODO


-- mergeAnnotations :: CoreDoc ->  Maybe CoreDoc
-- mergeAnnotations (Cat (Annotated lAnn lDoc) (Annotated rAnn rDoc)) | lAnn == rAnn = Just (Annotated lAnn (Cat lDoc rDoc))
-- mergeAnnotations _ = Nothing

-- simplifyCoreDoc :: CoreDoc -> CoreDoc
-- simplifyCoreDoc = rewrite mergeAnnotations 

mergeAnnotations' :: Eq a => SimpleDocStream a -> Maybe (SimpleDocStream a)
mergeAnnotations' (SAnnPush outerAnn (SChar c (SAnnPop (SAnnPush innerAnn inner)))) | outerAnn == innerAnn = Just ( SAnnPush outerAnn (SChar c inner))
mergeAnnotations' (SAnnPush outerAnn (SText i t  (SAnnPop (SAnnPush innerAnn inner)))) | outerAnn == innerAnn = Just (SAnnPush outerAnn (SText i t inner))
mergeAnnotations' (SAnnPush outerAnn (SLine i (SAnnPop (SAnnPush innerAnn inner)))) | outerAnn == innerAnn = Just (SAnnPush outerAnn (SLine i inner))
mergeAnnotations' _ = Nothing

instance Data (SimpleDocStream a) => Plated (SimpleDocStream a)
deriving instance Data a => Data (SimpleDocStream a)

                      
simplifyCDS :: (Plated (SimpleDocStream a), Eq a) => SimpleDocStream a -> (SimpleDocStream a)
simplifyCDS = rewrite mergeAnnotations'
                                                                                                             
class Renderable a where
  render :: a -> CoreDoc
  default render :: (Generic a, Renderable' (Rep a)) => a -> CoreDoc
  render x = render' (from x)
  renderList :: [a] -> CoreDoc
  renderList = align . list . map render
instance Renderable Variable where
    render (AVariable ident name) = angles (annotate (asAnnotation ident) (render name))

prettyVariable :: Variable -> Doc Identifier
prettyVariable v@AVariable {..} = annotate _variableID ( unAnnotate (render v) -- <+> pretty _variableID
                                                       )

--instance Show Variable where
--  show v = show $! renderSimplyDecorated id (\_ -> "[") (\i -> fromString $ "]</" ++ (show . render $ i) ++ ">") (layoutPretty defaultLayoutOptions (prettyVariable v))
                                    
instance Renderable WMEField where
  render O = "object"
  render A = "attribute"
  render V = "value"                                    
newtype SelfAnnotates a = SelfAnnotates a

class ToAnnotation a where
  asAnnotation :: a -> CoreAnnotations

instance ToAnnotation Identifier where
  asAnnotation = IDAnnotation

instance Pretty a => Pretty (SelfAnnotates a) where
  pretty (SelfAnnotates a) = pretty a

instance (ToAnnotation a, Pretty a) => Renderable (SelfAnnotates a) where
  render (SelfAnnotates a) = annotate (asAnnotation a) (pretty a)

deriving via (SelfAnnotates Identifier) instance Renderable Identifier
class Renderable' f where
  render' :: f a -> CoreDoc

instance Renderable' V1 where
  render' x = case x of {}

instance Renderable' U1 where
  render' U1 = emptyDoc

instance (Renderable' f, Renderable' g) => Renderable' (f :+: g) where
  render' (L1 x) = render' x
  render' (R1 x) = render' x

instance (Renderable' f, Renderable' g) => Renderable' (f :*: g) where
  render' (x :*: y) = render' x <+> render' y

instance Renderable c => Renderable' (K1 i c) where
  render' (K1 x) = render x

instance Renderable' f => Renderable' (M1 i t f) where
  render' (M1 x) = render' x

instance Renderable InternedText where
  render = pretty . unintern

instance Renderable Rational where
  render = viaShow
instance Renderable QuiescenceFuel where
    render = viaShow
instance Renderable Double where
  render = pretty

instance Renderable Integer where
  render = pretty

instance Renderable CoreDoc where
  render = id

instance Renderable Word where
  render = pretty
instance Renderable Word8 where
  render = pretty
instance Renderable Int where
  render = pretty

instance Renderable Natural where
  render = pretty

instance Renderable Bool where
  render = pretty

newtype ByRenderable a = ByRenderable a

instance Renderable Text where
  render = pretty

instance Renderable a => Renderable (ByRenderable a) where
  render (ByRenderable a) = render a

instance Renderable a => ToPrimitiveAttribute (ByRenderable a) where
  {-# INLINE toPrimitiveAttribute #-}
  toPrimitiveAttribute = toPrimitiveAttribute . Textual.renderStrict . layoutCompact . render
                            
instance Renderable a => Pretty (ByRenderable a) where
  pretty = unAnnotate . render

instance {-# OVERLAPPING #-} Renderable String where
  render = pretty

instance Renderable a => Renderable (Maybe a) where
  render = maybe mempty render
  renderList = renderList . catMaybes

instance Renderable a => Renderable [a] where
  render = renderList

instance Renderable a => Renderable (NonEmpty a) where
  render a = renderList (a^..folded)

instance (Renderable a, Renderable b) => Renderable (a, b) where
  render (a, b) = tupled [render a, render b]

instance (Renderable a, Renderable b, Renderable c) => Renderable (a, b, c) where
  render (a, b, c) = tupled [render a, render b, render c]

instance Renderable a => Renderable (a ? p) where
  {-# INLINE render #-}
  render (The thing) = render thing

instance Renderable Level where
  render lvl = render (fmap toLower (show lvl))

instance Renderable a => Renderable (HS.HashSet a) where
  render = renderList . HS.toList

instance {-# OVERLAPPABLE #-} (Foldable f, Renderable a) => Renderable (f a) where
  render = renderList . toList


deriving via SelfAnnotates WMETruth instance Renderable WMETruth
deriving via SelfAnnotates ConcreteTruth instance Renderable ConcreteTruth

instance ToAnnotation ConcreteTruth where
    asAnnotation = ConcreteTruthAnnotation

deriving via ByRenderable ConcreteTruth instance ToPrimitiveAttribute ConcreteTruth

instance ToAttribute ConcreteTruth              
instance ToAnnotation WMETruth where
    asAnnotation = AbstractTruthAnnotation

                   
