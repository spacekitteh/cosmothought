module Core.Utils.Parsing (module Core.Utils.Parsing,module Core.Utils.Parsing.Errors, module Text.Megaparsec, module Core.Utils.Parsing.Core {-, module Core.Utils.Parsing.RegularExpression-}) where
import Core.Utils.Parsing.Errors
import Polysemy.Input
import Polysemy.Assert
--import CalamityCommands.ParameterInfo
--import CalamityCommands.Parser
import Control.Applicative
import Core.Utils.Logging
import Control.Category
import Control.Concurrent.STM

import Core.Identifiers.IdentifierGenerator
import Core.Error
import Core.Utils.Parsing.Core
--import Core.Utils.Parsing.RegularExpression

import Data.Char
import Data.Either
import Data.Foldable
--import DiPolysemy
import Data.Functor
import Polysemy.State.Keyed
import Core.Configuration

import Data.List.NonEmpty as NE
  ( NonEmpty (..)
  )
import Data.Maybe
import Data.String
import Data.Text (Text)
import qualified Data.Text as T

--import qualified Di
import Polysemy.Resource
import Polysemy.OpenTelemetry

import Error.Diagnose
import Polysemy

import Polysemy.Transaction
import Prettyprinter
  ( Pretty,
    pretty,
  )
import qualified Prettyprinter as PP
import qualified Prettyprinter.Render.String as PP
import Text.Megaparsec hiding (State, many, match, noneOf, parse, parseMaybe, some, tokens)
import qualified Text.Megaparsec as MP


import Prelude hiding ((.))
import OpenTelemetry.Trace.Core (Tracer)
-- adaptToCommand :: forall c r a. (Members '[IDGen, Transactional] r, ParameterParser a c r) => Parser (P.Reader c : r) a -> P.Sem (ParserEffs c r) a
-- adaptToCommand m = do
--   s <- P.get
--   res <- P.raise . P.raise $ runParserT (skipN (s ^. to off) *> trackOffsets (space *> m)) "" (s ^. to msg)
--   case res of
--     Right (a, offset) -> do
--       P.modify (\p@ParserState {..} -> p {off = off + offset})
--       pure a
--     Left s -> P.throw (name', T.pack $ errorBundlePretty s)
--   where
--     name' =
--       let ParameterInfo (fromMaybe "" -> name) type_ _ = parameterInfo @a @c @r
--        in name <> ":" <> T.pack (show type_)
--     skipN n = void $ takeP Nothing n
--     trackOffsets m = do
--       offs <- getOffset
--       a <- m
--       offe <- getOffset
--       pure (a, offe - offs)

parseWithMinimalSem ::
  (Members '[IDGen, Transactional] r) =>
  Tracer ->
  Bool -> 
  (forall x. Configuration x -> TVar x) -> 
  (Sem r (Either (ParseErrorBundle DiagnosticMessageType ParsingError) a) -> Sem '[ -- IDGen, Log,
                                                                                         KeyedState LogConfiguration, KeyedState Configuration, Assert, Transactional, OpenTelemetryContext, OpenTelemetrySpan, Input Tracer, Resource,  Embed IO, Final IO] (Either (ParseErrorBundle DiagnosticMessageType ParsingError) a)) ->
  Parser r a ->
  String ->
  Text ->
  IO (Either (ParseErrorBundle DiagnosticMessageType ParsingError) a)
parseWithMinimalSem tracer dolog keyLookup otherRunners p s input = minimalSem tracer dolog keyLookup otherRunners (runParserT p s input)

parseWithMinimalSem' ::
  (Members '[IDGen, Transactional] r) =>
  Tracer ->
  Bool -> 
  (forall x. Configuration x -> TVar x) -> 
  (Sem r (Either (Diagnostic DiagnosticMessageType ) a) -> Sem '[ -- IDGen, Log,
                                                                                         KeyedState LogConfiguration, KeyedState Configuration, Assert, Transactional, OpenTelemetryContext, OpenTelemetrySpan, Input Tracer, Resource,  Embed IO, Final IO] (Either (Diagnostic DiagnosticMessageType) a)) ->
  Parser r a ->
  String ->
  Text ->
  IO (Either (Diagnostic DiagnosticMessageType) a)
parseWithMinimalSem' tracer dolog keyLookup otherRunners p s input = minimalSem tracer dolog keyLookup otherRunners (runAndDiagnoseParser p s input)

runAndDiagnoseParser :: (Members '[IDGen, Transactional] r) =>Parser r a -> FilePath -> Text -> Sem r (Either (Diagnostic DiagnosticMessageType) a)
runAndDiagnoseParser parser sourceName sourceBody = do
  result <- maybeDiagnose @WhenParsing <$> runParserT parser sourceName sourceBody
  case result of
    Right _ -> pure result
    Left diagnostic -> do
      pure (Left (addFile diagnostic sourceName (T.unpack sourceBody)))
                

                                                                    
customStateParseWithMinimalSem ::
  (Members '[IDGen, Transactional] r) =>
  Tracer -> 
  Bool -> 
  (forall x. Configuration x -> TVar x) -> 
  (Sem r (Either (ParseErrorBundle DiagnosticMessageType ParsingError) a) -> Sem '[ -- IDGen, Log,
                                                                                         KeyedState LogConfiguration, KeyedState Configuration, Assert, Transactional, OpenTelemetryContext, OpenTelemetrySpan,Input Tracer, Resource,  Embed IO, Final IO] (Either (ParseErrorBundle DiagnosticMessageType ParsingError) a)) ->
  Parser r a ->
  MP.State DiagnosticMessageType ParsingError -> 
  IO (Either (ParseErrorBundle DiagnosticMessageType ParsingError) a)
customStateParseWithMinimalSem tracer dolog  keyLookup otherRunners thingToRun s = do
  minimalSem tracer dolog keyLookup otherRunners (snd <$> runParserT' thingToRun s)
  

                                                        
minimalSem ::
  (Members '[IDGen, Transactional] r) =>
  Tracer -> 
  Bool -> 
  (forall x. Configuration x -> TVar x) -> 
  (Sem r a -> Sem '[ -- IDGen, Log,
                                                                                         KeyedState LogConfiguration, KeyedState Configuration, Assert, Transactional, OpenTelemetryContext, OpenTelemetrySpan, Input Tracer, Resource,  Embed IO, Final IO] (a)) ->
  Sem r a ->
  IO ( a)
minimalSem tracer _doLog keyLookup otherRunners thingToRun = -- Di.new $ \di ->
  runFinal $ embedToFinal @(IO) $ resourceToIOFinal . runInputConst tracer .  runOpenTelemetrySpan . runOpenTelemetryContext $  interpretTransactional . assertsToIOException tracer.    runKeyedStateVarsIO keyLookup . embedLogConfiguration -- . (if doLog then runDi di else runDiNoop)  $ runIDGen
               $  otherRunners $ thingToRun

{-# INLINE pp #-}
pp :: Text -> PP.Doc ann
pp = pretty

{-# INLINE showsPrecDefault #-}
showsPrecDefault :: Pretty a => a -> ShowS
showsPrecDefault =
  PP.renderShowS . PP.layoutSmart PP.defaultLayoutOptions . pretty

{-# INLINE prettyPrintNonEmpty #-}
prettyPrintNonEmpty :: Pretty a => NonEmpty a -> [PP.Doc ann]
prettyPrintNonEmpty a = fmap pretty (toList a)

{-# INLINE prettyPrintSList #-}
prettyPrintSList :: [PP.Doc ann] -> PP.Doc ann
prettyPrintSList [p] = p
prettyPrintSList ps = PP.parens . PP.fillSep $ ps

{-# INLINE prettyPrintSList' #-}
prettyPrintSList' :: Text -> [PP.Doc ann] -> PP.Doc ann
prettyPrintSList' name list = prettyPrintSList $ (pretty name) : list

parseString :: Parser r Text
parseString = takeWhileP (Just "string token") isPrint
