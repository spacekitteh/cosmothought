{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE QuasiQuotes #-}
module Core.Utils.Parsing.Core where -- (Parser, stringLiteral, symbolChar, parseExactly, parseLineComment, sc, lexeme, verbatim, keyword, parseParens, parseMaybeParens, parseEmptyParens, parseNonEmpty, parseNonEmptyPairList, parseBool, bracketed, parseIdentifier, parseObjectIdentifier, parseSymbolName, parseObjectIdentifierGivenID, parseVariableName, parseBracketedVariableName, ParsingError (..), diagnose, ) where
import Data.Type.Equality
import Core.Utils.Parsing.Errors
import Control.Applicative
import Control.Category
import Control.Lens
import Control.Monad
import qualified Control.Monad.Trans.Class as T
import Core.Identifiers
import Core.Utils.MetaTypes
import Core.Identifiers.IDGen
import Core.Variable
import Data.Bool
import Data.Char
import qualified Df1
import Data.Foldable as DF
import Data.Function (($))
import Core.Identifiers.IdentifierType
import Data.Interned
import Data.Interned.Text
import Data.List.NonEmpty as NE
  ( NonEmpty (..),
    length,
    zip,
  )
import Data.Maybe
import Data.String as DS (IsString (..))
import Data.Text (Text)
import qualified Data.Text as T
import Data.UUID.Types as UUID
import Polysemy
import Text.Megaparsec hiding (State, many, match, noneOf, parse, parseMaybe, some, tokens)
import Text.Megaparsec.Char (char, eol, space1, string)
import qualified Text.Megaparsec.Char as TMC
import qualified Text.Megaparsec.Char.Lexer as L
import qualified Text.URI as U (RText, RTextLabel (..), mkScheme, parser )
import Text.URI.Lens
import Text.Read (Read, reads)
import Text.Show (Show, show)
import Prelude ((+), (-))

readableDatatype :: (Read a, MonadParsec e s m,Token s ~ Char,  IsString s, Show s ) => m a
readableDatatype = do
  input <- getInput
  let inputAsString = show input
  offset <- getOffset
  choice $
    (\(a, input') -> a <$ setInput ( DS.fromString  input')
                       <* setOffset (offset + DF.length inputAsString - DF.length input'))
    <$> reads inputAsString
  
quoted2 :: (MonadParsec e s m, Token s ~ Char, IsString (Tokens s)) => m Text
quoted2 = fmap DS.fromString $ between (char '\"') (char '\"') (many (try escaped <|> normalChar))
   where
     escaped = '\"' <$ string "\\\""
     normalChar = anySingleBut '\"'
    
quoted :: Parser r Text
quoted = fmap DS.fromString $ between (char '\"') (char '\"') (many (try escaped <|> normalChar))
   where
     escaped = '\"' <$ parseExactly "\\\""
     normalChar = anySingleBut '\"'
stringLiteral :: Parser r Text
stringLiteral = ({-lexeme-} quoted) <?> "String literal"
--stringLiteral = (fmap DS.fromString $ char '\"' *> manyTill L.charLiteral (char '\"')) <?> "String literal"

symbolChar :: Parser r Char
symbolChar = satisfy (\c -> elem c ("!-_" :: [Char]))

parseExactly :: Text -> Parser r Text
parseExactly = string

{-# INLINE parseLineComment #-}
parseLineComment :: Parser r ()
parseLineComment = L.skipLineComment ";" <* eol

{-# INLINE sc #-}
sc :: Parser r ()
sc = L.space space1 parseLineComment empty

{-# INLINE lexeme #-}
lexeme :: Parser r a -> Parser r a
lexeme p = L.lexeme sc p

{-# INLINE verbatim #-}
verbatim :: Text -> Parser r Text
verbatim p= L.symbol sc p

{-# INLINE keyword #-}
keyword :: Text -> Parser r Text
keyword k = lexeme (string k <* satisfy isSpace)

{-# INLINE bracketed #-}
bracketed :: Parser r a -> Parser r a
bracketed p = between (verbatim "[") (verbatim "]") p

{-# INLINE parseParens #-}
parseParens :: Parser r a -> Parser r a
parseParens = between (verbatim "(") (verbatim ")")

{-# INLINE parseMaybeParens #-}
parseMaybeParens :: Parser r a -> Parser r a
parseMaybeParens p = try (parseParens p) <|> p

{-# INLINE parseEmptyParens #-}
parseEmptyParens :: Parser r ()
parseEmptyParens = parseParens sc

{-# INLINE parseNonEmpty #-}
parseNonEmpty :: Parser r a -> Parser r (NonEmpty a)
parseNonEmpty p = (:|) <$> p <*> many p

{-# INLINE parseNonEmptyPairList #-}
parseNonEmptyPairList :: Parser r a -> Parser r b -> Parser r (NonEmpty (a, b))
parseNonEmptyPairList pa pb = do
  as <- parseParens $ parseNonEmpty pa
  (b : bs) <- parseParens $ count (NE.length as) pb
  pure (NE.zip as (b :| bs))


parseBool :: Parser r Bool
parseBool = True <$ verbatim "true" <|> False <$ verbatim "false"

parseInternedTextBy :: Parser r Text -> Parser r InternedText
parseInternedTextBy parser = label "Interned string" do
  str <- parser
  pure (intern str)

urnScheme :: Maybe (U.RText U.Scheme)
urnScheme = (U.mkScheme "urn")

uuidPrefix :: Text
uuidPrefix = "uuid:"

{-# INLINEABLE parseIdentifier #-}
parseIdentifier :: Parser r Identifier
parseIdentifier =
  label
    "Identifier"
    ( ( ( {-dbg "uuid parsing" $-} try do
            i <- UniqueIdentifier <$> parseUUID <?> "Base Identifier UUID"
            contexts <- ((many (parseExactly "/" *> L.decimal))) <?> "Identifier context chain"
            foldrM (\a b -> pure $ ContextualIdentifier a b) i contexts
        )
          <|> ( ( do
                    uri {-dbg "uriparser"-} <- U.parser
                    case uri ^. uriScheme of
                      Nothing -> empty
                      Just _urnScheme
                        | firstPiece : _ <- uri ^.. uriPath . folded . unRText,
                          uuidPrefix `T.isPrefixOf` firstPiece ->
                            empty
                      Just _ -> pure (AURI uri)
                    <?> "URI with non-urn:uuid: scheme"
                )
              )
      )
    )

parseSymbolText :: Parser r Text
parseSymbolText =
  ( ( do
        first <- TMC.letterChar <|> symbolChar
        rest <- many (TMC.alphaNumChar <|> symbolChar)
        pure (T.pack (first : rest))
    )
      <?> "symbol"
  )

{-# INLINE parseSymbolName #-}
parseSymbolName :: Parser r SymbolName
parseSymbolName = parseInternedTextBy parseSymbolText -- (takeWhile1P (Just "alphabetic") isAlpha)

parseVariableName :: Parser r SymbolName
parseVariableName = parseInternedTextBy (between ({-dbg "opening angle"-} (parseExactly "<" <?> "Variable opening symbol")) ({-dbg "closing angle"-} (((parseExactly ">" {-<|> verbatim ">"-})) <?> "Variable closing symbol")) ({-dbg "symboltxt"-} parseSymbolText))

parseBracketedVariableName :: Parser r SymbolName
parseBracketedVariableName = parseInternedTextBy (between (verbatim "[<" <?> "Bracketed variable opening symbol") ((verbatim ">]" <?> "Bracketed variable closing symbol")) (parseSymbolText))

{-# INLINEABLE parseVariable #-}
parseVariable :: WhenParsing -> Parser r Variable
parseVariable _w = do
  ( ( try do
        n <- parseBracketedVariableName
        i <- between (verbatim "</") ((parseExactly ">") <|> verbatim ">") (try (lexeme parseIdentifier) <|> parseIdentifier)
        pure (AVariable i n)
    )
      <|> ( do
              ident <- T.lift newGlobalIdentifier
              AVariable ident <$> (parseVariableName)
          )
    )


parseObjectIdentifier :: WhenParsing -> Parser r ObjectIdentifier
parseObjectIdentifier w =
  ( try (ObjectIdentifier <$> (between (TMC.string "|") (TMC.string "|") parseSymbolName) <*> (parseParens parseIdentifier))
      <|> ( errorWhenParses
              w
              (parseVariable w)
              ( \w -> case w of
                  ParsingWME ->
                    ( Just (ParsingError w Nothing (VariableInWME ^. re _WMEParseError))
                    )
                  _ -> Nothing
              )
          )
  )
    <?> "Object identifier"

parseObjectIdentifierName :: WhenParsing -> Parser r SymbolName
parseObjectIdentifierName w =
  ( try (between (TMC.string "|") (TMC.string "|") (parseSymbolName)) <?> "Bracketed object ID name"
  )
    <|> try
      ( parseSymbolName <?> "Plain object ID name"
      )
    <|> ( errorWhenParses
            w
            (parseVariable w)
            ( \w -> case w of
                ParsingWME ->
                  ( Just (ParsingError w Nothing (VariableInWME ^. re _WMEParseError))
                  )
                _ -> Nothing
            )
        )

parseObjectIdentifierLookup :: forall obj r. (Members '[ObjectIdentityMap obj] r) => WhenParsing -> Parser r ObjectIdentifier
parseObjectIdentifierLookup wp = do
  oidName <- parseObjectIdentifierName wp
  T.lift (lookupObjectIDByName oidName)


   
parseObjectIdentifierGivenID :: WhenParsing -> Identifier -> Parser r ObjectIdentifier
parseObjectIdentifierGivenID w i =
  try
    ( ObjectIdentifier
        <$> parseObjectIdentifierName w
        <*> pure i
        <?> "Object identifier name"
    )

{-# INLINEABLE parseUUID #-}
parseUUID :: Parser r UUID
parseUUID = label
  "UUID"
  do
    optional (string "urn:uuid:") <?> "URN UUID scheme"
    str <- takeP (Just "UUID") 36 <?> "UUID string"
    case fromText str of
      Nothing -> empty -- fail "Unable to parse UUID"
      Just u -> pure u


parseDf1Level :: Parser r Df1.Level
parseDf1Level = try (Df1.Emergency <$ verbatim "emergency") <|>
    try (Df1.Alert <$ verbatim "alert") <|>
    try (Df1.Critical <$ verbatim "critical") <|>
    try (Df1.Error <$ verbatim "error") <|>
    try (Df1.Warning <$ verbatim "warning") <|>
    try (Df1.Notice <$ verbatim "notice") <|>
    try (Df1.Info <$ verbatim"info") <|>
    (Df1.Debug <$ verbatim "debug")
    
parseDf1Path :: Parser r Df1.Path
parseDf1Path = do
  try ((Df1.Attr <$> (DS.fromString  <$> manyTill L.charLiteral (char '=')) <*> (DS.fromString  <$> manyTill L.charLiteral (( space1)  ))) <?> "key=value")
  <|> (fmap (Df1.Push . DS.fromString . T.unpack )  $ stringLiteral) <?> "path segment"
