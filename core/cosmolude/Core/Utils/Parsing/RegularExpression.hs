module Core.Utils.Parsing.RegularExpression where

{-
import Control.Monad (guard, liftM, return)
import qualified Control.Monad.Trans.Class as T
import Core.Utils.Parsing.Core
import Data.Foldable (asum)
import qualified Data.List as DL
import qualified Data.Set as Set (fromList)
import Data.String (fromString)
import Data.Text
import Polysemy
import Polysemy.State
import Text.Megaparsec hiding (State)
import Text.Megaparsec.Char
import Text.Regex.TDFA.Pattern
import Prelude hiding (concat, null, unwords)

-- | An element inside @[...]@, denoting a character class.
data BracketElement
  = -- | A single character.
    BEChar Char
  | -- | A character range (e.g. @a-z@).
    BERange Char Char
  | -- | @foo@ in @[.foo.]@.
    BEColl [Char]
  | -- | @bar@ in @[=bar=]@.
    BEEquiv [Char]
  | -- | A POSIX character class (candidate), e.g. @alpha@ parsed from @[:alpha:]@.
    BEClass [Char]

-- | Return either an error message or a tuple of the Pattern and the
-- largest group index and the largest DoPa index (both have smallest
-- index of 1).  Since the regular expression is supplied as [Char] it
-- automatically supports unicode and @\\NUL@ characters.

{-parseRegex :: Text -> Either ParseError (Pattern, (GroupIndex, DoPa))
parseRegex x =
  runParser
    ( do
        pat <- p_regex
        eof
        (lastGroupIndex, lastDopa) <- getState
        return (pat, (lastGroupIndex, DoPa lastDopa))
    )
    (0, 0)
    x
    x
-}
data RegexParseState = RegexParseState GroupIndex Int
parseRegex :: Parser r Pattern
parseRegex =
    lift (runState (0,0)

p_regex ::  Parser ((State (GroupIndex, Int)) : r) Pattern
p_regex = do
  (POr <$> sepBy1 p_branch (char '|'))

-- man re_format helps a lot, it says one-or-more pieces so this is
-- many1 not many.  Use "()" to indicate an empty piece.
p_branch :: (Members '[State (GroupIndex, Int)] r) => Parser r Pattern
p_branch = PConcat <$> some p_piece

p_piece :: (Members '[State (GroupIndex, Int)] r) => Parser r Pattern
p_piece = (p_anchor <|> p_atom) >>= p_post_atom -- correct specification

p_atom :: (Members '[State (GroupIndex, Int)] r) => Parser r Pattern
p_atom = p_group <|> p_bracket <|> p_char <?> "an atom"

group_index :: (Members '[State (GroupIndex, Int)] r) => Parser r (Maybe GroupIndex)
group_index = do
  (gi, ci) <- T.lift get
  let index = succ gi
  T.lift (put (index, ci))
  return (Just index)

p_group :: (Members '[State (GroupIndex, Int)] r) => Parser r Pattern
p_group =
  lookAhead (char '(') >> do
    index <- group_index
    (PGroup index) <$> between (char '(') (char ')') p_regex

-- p_post_atom takes the previous atom as a parameter
p_post_atom :: Pattern -> Parser r Pattern
p_post_atom atom =
  (char '?' >> return (PQuest atom))
    <|> (char '+' >> return (PPlus atom))
    <|> (char '*' >> return (PStar True atom))
    <|> p_bound atom
    <|> return atom

p_bound :: Pattern -> Parser r Pattern
p_bound atom = try $ between (char '{') (char '}') (p_bound_spec atom)

p_bound_spec :: Pattern -> Parser r Pattern
p_bound_spec atom = do
  lowS <- some digitChar
  let lowI = read lowS
  highMI <- option (Just lowI) $ try $ do
    _ <- char ','
    -- parsec note: if 'many digits' fails below then the 'try' ensures
    -- that the ',' will not match the closing '}' in p_bound, same goes
    -- for any non '}' garbage after the 'many digits'.
    highS <- many digitChar
    if DL.null highS
      then return Nothing -- no upper bound
      else do
        let highI = read highS
        guard (lowI <= highI)
        return (Just (read highS))
  return (PBound lowI highMI atom)

-- An anchor cannot be modified by a repetition specifier
p_anchor :: (Members '[State (GroupIndex, Int)] r) => Parser r Pattern
p_anchor =
  (char '^' >> liftM PCarat char_index)
    <|> (char '$' >> liftM PDollar char_index)
    <|> try
      ( do
          _ <- string "()"
          index <- group_index
          return $ PGroup index PEmpty
      )
    <?> "empty () or anchor ^ or $"

char_index :: (Members '[State (GroupIndex, Int)] r) => Parser r DoPa
char_index = do
  (gi, ci) <- T.lift get
  let ci' = succ ci
  T.lift (put (gi, ci'))
  return (DoPa ci')

p_char :: (Members '[State (GroupIndex, Int)] r) => Parser r Pattern
p_char = p_dot <|> p_left_brace <|> p_escaped <|> p_other_char
  where
    p_dot = char '.' >> char_index >>= return . PDot
    p_left_brace = try $ (char '{' >> notFollowedBy digitChar >> char_index >>= return . (`PChar` '{'))
    p_escaped = char '\\' >> anySingle >>= \c -> char_index >>= return . (`PEscape` c)
    p_other_char = do
      c <- noneOf specials
      i <- char_index
      return (PChar i c)
      where
        specials = "^.[$()|*+?{\\" :: [Char]

-- parse [bar] and [^bar] sets of characters
p_bracket :: (Members '[State (GroupIndex, Int)] r) => Parser r Pattern
p_bracket = (char '[') >> ((char '^' >> p_set True) <|> (p_set False))

p_set :: (Members '[State (GroupIndex, Int)] r) => Bool -> Parser r Pattern
p_set invert = do
  initial <- option "" (char ']' >> return "]")
  values <- if null initial then some p_set_elem else many p_set_elem
  _ <- char ']'
  ci <- char_index
  let chars =
        maybe'set $
          DL.concat $
            (unpack initial)
              : [c | BEChar c <- values]
              : [[start .. end] | BERange start end <- values]
      colls = maybe'set [PatternSetCollatingElement (coll) | BEColl coll <- values]
      equivs = maybe'set [PatternSetEquivalenceClass (equiv) | BEEquiv equiv <- values]
      class's = maybe'set [PatternSetCharacterClass (a'class) | BEClass a'class <- values]
      maybe'set x = if DL.null x then Nothing else Just (Set.fromList x)
      sets = PatternSet chars class's colls equivs
  sets `seq` return $ if invert then PAnyNot ci sets else PAny ci sets

-- From here down the code is the parser and functions for pattern [ ] set things

p_set_elem :: Parser r BracketElement
p_set_elem =
  checkBracketElement
    =<< asum
      [ p_set_elem_class,
        p_set_elem_equiv,
        p_set_elem_coll,
        p_set_elem_range,
        p_set_elem_char,
        fail "Failed to parse bracketed string"
      ]

p_set_elem_class :: Parser r BracketElement
p_set_elem_class =
  liftM BEClass $
    try (between (string "[:") (string ":]") (some $ noneOf (":]" :: [Char])))

p_set_elem_equiv :: Parser r BracketElement
p_set_elem_equiv =
  liftM BEEquiv $
    try (between (string "[=") (string "=]") (some $ noneOf ("=]" :: [Char])))

p_set_elem_coll :: Parser r BracketElement
p_set_elem_coll =
  liftM BEColl $
    try (between (string "[.") (string ".]") (some $ noneOf (".]" :: [Char])))

p_set_elem_range :: Parser r BracketElement
p_set_elem_range = try $ do
  start <- noneOf ("]" :: [Char])
  _ <- char '-'
  end <- noneOf ("]" :: [Char])
  return $ BERange start end

p_set_elem_char :: Parser r BracketElement
p_set_elem_char = do
  c <- noneOf ("]" :: [Char])
  return (BEChar c)

-- | Fail when 'BracketElement' is invalid, e.g. empty range @1-0@.
-- This failure should not be caught.
checkBracketElement :: BracketElement -> Parser r BracketElement
checkBracketElement e =
  case e of
    BERange start end
      | start > end ->
          fail $
            unpack $
              unwords
                [ "End point",
                  singleton end,
                  "of dashed character range is less than starting point",
                  singleton start
                ]
      | otherwise -> ok
    BEChar _ -> ok
    BEClass _ -> ok
    BEColl _ -> ok
    BEEquiv _ -> ok
  where
    ok = return e
-}
