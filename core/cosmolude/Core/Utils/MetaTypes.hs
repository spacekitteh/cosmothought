{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# OPTIONS_GHC -Wno-orphans -Wno-incomplete-patterns #-}

-- | Types which aren't actually present as runtime values, but instead are only used for plumbing and type indexing.
module Core.Utils.MetaTypes where

-- import Language.Haskell.TH

-- import Core.Utils.Prettyprinting

-- import Data.Either.Combinators (rightToMaybe)
-- import Polysemy.Conc.Effect.Race (Race)
-- import Polysemy.Conc.Effect.Scoped (Scoped_)
-- import qualified Polysemy.Conc.Effect.Sync as Sync
-- import Polysemy.Conc.Effect.Sync (Sync)
-- import Polysemy.Conc.Interpreter.Scoped (runScopedAs)
-- import qualified Polysemy.Conc.Race as Race

import Control.Applicative
import Control.Lens hiding ((...), (^?!))
import qualified Control.Lens ((^?!))
import Control.Monad
import Data.Data
import Data.Foldable
import Data.Functor.Apply
import Data.Hashable
import Data.Interned.Internal
import Data.Interned.Internal.Text
import Data.Kind
import Data.List.NonEmpty
import Data.Refined
import Data.Semigroup
import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import Data.Text (Text)
import Data.The
import GHC.Generics (Generic)
import GHC.Stack
import Language.Haskell.TH.Quote
import Language.Haskell.TH.Syntax (Lift (..))
import Logic.Proof
import Logic.Propositional
import Math.ParetoFront
import Prettyprinter
import Text.URI.QQ as QQ
import Theory.Equality
import Theory.Named
import Prelude

{-# WARNING (^??!) "This is unsafe; use a proof instead" #-}
{-# INLINE (^??!) #-}

infixl 8 ^??!

(^??!) :: HasCallStack => s -> Getting (Endo a) s a -> a
(^??!) = (Control.Lens.^?!)

instance Eq a => Eq (a ? p) where
  {-# INLINE (==) #-}
  (The a) == (The b) = a == b

instance Hashable a => Hashable (a ? p) where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt (The thing) = hashWithSalt salt thing

instance Show a => Show (a ? p) where
  {-# INLINE show #-}
  show (The thing) = show thing

instance Pretty a => Pretty (a ? p) where
  {-# INLINE pretty #-}
  pretty (The thing) = pretty thing

instance Ord a => Ord (a ? p) where
  {-# INLINE (<=) #-}
  (The a) <= (The b) = a <= b

class Each1 s t a b | s -> a, t -> b, s b -> t, t a -> s where
  each1 :: Traversal1 s t a b

instance {-# OVERLAPPABLE #-} Each1 s t a b => Each s t a b where
  {-# INLINE each #-}
  each = cloneTraversal each1

instance Each1 (NonEmpty a) (NonEmpty b) a b where
  {-# INLINE each1 #-}
  each1 f (first :| []) = (\first' -> first' :| []) <$> f first
  each1 f (first :| (x : xs)) = liftF2 (\a (y :| ys) -> a :| (y : ys)) (f first) (each1 f (x :| xs))

data WMEField = O | A | V deriving (Eq, Ord, Show, Read, Generic, Hashable, Enum, Bounded)

type SymbolName = InternedText

instance Lift SymbolName where
  liftTyped t = do
    let t' = symbolNameToText t
    [||(textToSymbolName t')||]

--      textToSymbolName <$> liftTyped <$> symbolNameToText
deriving instance Data SymbolName

{-# INLINE CONLIKE textToSymbolName #-}
textToSymbolName :: Text -> SymbolName
textToSymbolName = intern

{-# INLINE CONLIKE symbolNameToText #-}
symbolNameToText :: SymbolName -> Text
symbolNameToText = unintern

class SumTypeFamily generalType where
  type Discriminator generalType :: (Type -> Type)

class (pred ~ (IsCorrectSubType specificType), SumTypeFamily generalType) => ProveCorrectSubType generalType pred (specificType :: subTypes) | specificType -> generalType pred, pred -> specificType where
  type IsCorrectSubType specificType :: (Type -> Type)
  proveIsCorrectSubType :: (generalType ~~ n) -> Maybe (Proof ((Discriminator generalType n) == specificType))
  {-# INLINE CONLIKE isCorrectSubTypeGivesPredicate #-}
  isCorrectSubTypeGivesPredicate :: forall n. Proof ((((Discriminator generalType) n) == specificType) --> IsCorrectSubType specificType n)
  isCorrectSubTypeGivesPredicate = axiom
  {-# INLINE CONLIKE maybeCorrectSubType #-}
  maybeCorrectSubType :: forall (n :: Type). (generalType ~~ n) -> Maybe (generalType ? (IsCorrectSubType specificType))
  maybeCorrectSubType thing = case proveIsCorrectSubType thing of
    Just proof -> Just (unname (thing ... (elimImpl proof ((isCorrectSubTypeGivesPredicate)))))
    Nothing -> Nothing
  {-# INLINE CONLIKE maybeCorrectSubType' #-}
  maybeCorrectSubType' :: generalType -> Maybe (generalType ? (IsCorrectSubType specificType))
  maybeCorrectSubType' thing' = name thing' \thing -> case proveIsCorrectSubType thing of
    Just proof -> Just (unname (thing ... (elimImpl proof ((isCorrectSubTypeGivesPredicate)))))
    Nothing -> Nothing

{-# INLINE promoteAffineToLens #-}
promoteAffineToLens ::
  ( HasCallStack,
    Defining (pred ()),
    ProveCorrectSubType gen pred specific
  ) =>
  Traversal' gen field ->
  Lens' (gen ? (IsCorrectSubType specific)) (field)
promoteAffineToLens field = (iso the (Data.Refined.assert)) . (unsafeSingular field)

{-# INLINE knownCorrectSubType #-}
knownCorrectSubType :: forall generalType specificType pred. ProveCorrectSubType generalType pred specificType => Fold generalType (generalType ? (IsCorrectSubType specificType))
knownCorrectSubType = to (\n -> name n (maybeCorrectSubType)) . _Just where

-- TODO: convert this to template haskell(/generics?) so you don't need the Defining
-- (pred ()) constraint at the site where it's used when pred is polymorphic

type WrappingClassyInstance general pred specific cl =
  ( cl, -- Defining (pred ()),
    ProveCorrectSubType general pred specific --  => cl (general ? pred)
  )

{-# INLINE _CorrectSubType #-}
_CorrectSubType :: ProveCorrectSubType general pred specific => Prism' general (general ? (IsCorrectSubType specific))
_CorrectSubType = prism' the maybeCorrectSubType'

errorCode :: QuasiQuoter
errorCode =
  QuasiQuoter
    { quoteExp = \s -> ((quoteExp QQ.uri) ("error-code/" ++ s)),
      quotePat = \s -> ((quotePat QQ.uri) ("error-code/" ++ s)),
      quoteType = undefined,
      quoteDec = error "not implemented"
    }

{-# INLINEABLE collectionDominates #-}
collectionDominates :: (Debatable a) => Seq a -> Seq a -> Comparison
collectionDominates Seq.Empty Seq.Empty = WeakTie
collectionDominates Seq.Empty _ = StrongTie
collectionDominates _ Seq.Empty = StrongTie
collectionDominates (a :< as) (b :< bs) = (weigh a b) <> collectionDominates as bs

{-# INLINEABLE subsumeDominatedWith #-}
subsumeDominatedWith :: (Foldable f, Debatable a) => (a -> a -> Bool) -> f a -> Seq a
subsumeDominatedWith f col = runIdentity $ subsumeDominatedWithM (\a b -> pure (f a b)) col

{-# INLINEABLE discardDominated #-}
discardDominated :: (Foldable f, Debatable a, Monad m) => (a -> a -> m Bool) -> f a -> m (Seq a)
discardDominated extraComparisons f = do
  foldM
    ( \existing cond -> do
        well <-
          ( anyMOf
              folded
              ( \other -> do
                  extraResults <- extraComparisons cond other
                  pure $ (weigh cond other == Dominated || weigh other cond == Dominates) && not (extraResults)
              )
              f
            )
        if well
          then pure existing
          else pure (cond :< existing)
    )
    Seq.empty
    f

{-# INLINEABLE subsumeDominatedWithM #-}
subsumeDominatedWithM :: (Foldable f, Debatable a, Monad m) => (a -> a -> m Bool) -> f a -> m (Seq a)
subsumeDominatedWithM extraComparisons f =
  do
    kept <- discardDominated extraComparisons f
    foldrM
      ( \ new prior-> do
          well <-
            anyMOf
              folded
              ( \other -> do
                  extraResults <- extraComparisons new other
                  pure $ weigh new other == WeakTie || weigh other new == WeakTie || extraResults
              )
              prior
          if well
            then pure prior
            else pure (prior :> new)
      )
      Seq.empty
      kept

{-# INLINE anyMOf #-}
anyMOf :: forall m s a. Monad m => Getting (Dual (Endo (Any -> m Any))) s a -> (a -> m Bool) -> s -> m Bool
anyMOf l f s = do
  res <-
    foldrMOf
      l
      ( \a existing  -> do
          res <- f a
          pure (existing <> (Any res))
      )
      (Any False)
      s
  pure (getAny res)

{-# INLINE subsumeDominated #-}
subsumeDominated :: (Foldable f, Debatable a) => f a -> Seq a
subsumeDominated = subsumeDominatedWith (const (const False))
