{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# OPTIONS_GHC -Wwarn=x-partial #-}
module Core.WorkingMemory.Diagram where
-- import Diagrams.Backend.PGF
-- import           Data.GraphViz.Commands
import Data.Either
import Core.WorkingMemory
import Cosmolude hiding (note, vsep, (#))
import Data.Graph.Inductive.PatriciaTree
import Data.GraphViz
import Core.CoreTypes
import qualified Data.Text as T
--import Diagrams.Backend.SVG
import Diagrams.Prelude hiding (render, value)
import Diagrams.TwoD.GraphViz
import Diagrams.TwoD.Text

import Polysemy.Embed

import Polysemy.Reader
import Prettyprinter.Render.Text
import Data.List
import Prelude (RealFloat, Double)
    
type WorkingMemoryGraph metadata = Gr (Either ObjectIdentifier Value) (WME' metadata)



generateWorkingMemoryGraph  :: (  HasExtraObjectIdentifiers metadata, HasWorkingMemoryData agent metadata, WMEIsState metadata, HasMaybeWMPreference metadata ObjectIdentifier, HasCallStack, Members '[WorkingMemory' metadata, Reader agent, Log, OpenTelemetrySpan, OpenTelemetryContext] r) => Sem r (WorkingMemoryGraph metadata)
generateWorkingMemoryGraph = do

  objects <- getAllObjectsAsKVs 
  let oids =  objects^..folded . _1 . re _Left
      vals = objects^..folded . _2 . value . filtered (hasn't _ObjectIdentifier) .  re _Right
      oidEdges = concat $ fmap (\(obj,wme) -> wme^.. objectIdentifiersInWME @ObjectIdentifier . to \referencedOID -> (Left obj, Left referencedOID, wme)) objects
      valueEdges = concat $ fmap (\(obj, wme) -> wme^..value . filtered (hasn't _ObjectIdentifier) . to \v -> (Left obj, Right v, wme)) objects
      edges = oidEdges <> valueEdges
      --   referencedOID <- 
      --   pure  (obj,referencedOID, wme)
  pure $! mkGraph (oids <> vals) ( edges)   
        

      
                                   

strictCD :: (RealFloat n, Typeable n, Diagrams.Prelude.Renderable (Text n) b) => CoreDoc -> QDiagram b V2 n Any
strictCD = text . T.unpack . renderStrict . layoutPretty defaultLayoutOptions

renderObject :: (Typeable n, RealFloat n, Ord n, Diagrams.Prelude.Renderable (Diagrams.Prelude.Path V2 n) b, Diagrams.Prelude.Renderable (Text n) b) =>  Either ObjectIdentifier Value -> P2 n -> QDiagram b V2 n Any
renderObject (Left obj) =    
  place $
    ( ( fontSize 20 $
          vsep
            2.0
            ( fmap
                strictCD
                [ 
                  render obj
                ]
            )
      )
        <> roundedRect 30 10 5
        # fc purple
    )
renderObject (Right val) =
    place $
          ((strictCD (render val) # fontSize 10)  <> roundedRect 15 7 2 # fc green)

renderWME :: (Typeable n, RealFloat n, Ord n,Diagrams.Prelude.Renderable (Diagrams.Prelude.Path V2 n) b, Diagrams.Prelude.Renderable (Text n) b) => Either ObjectIdentifier Value -> P2 n -> Either ObjectIdentifier Value -> P2 n -> WME' metadata -> Diagrams.Prelude.Path V2 n -> QDiagram b V2 n Any
renderWME _ p1 dest p2 wme p = (place (strictCD reason # size # offset
                                                                            
                                   ) (pathCentroid p)) `atop` arrowBetween' (opts) p1 p2 
  where
    reason = case wme^?attribute . _ObjectIdentifier of
               Just oid | Left oid == dest -> "<attr>"
               _ -> "^" <> (render  $ wme^.attribute)
    size = case dest of
              Left _ -> fontSize 20 
              Right _ -> fontSize 10
    offset = (case dest of
                Left _ -> translate (r2 (8,3))
                Right _ ->translate (r2 (3,3)))
    opts = with & gaps .~ 16 & arrowShaft .~ (unLoc . head $ pathTrails p)

renderWorkingMemoryGraph :: forall agent metadata r b. (HasExtraObjectIdentifiers metadata, HasWorkingMemoryData agent metadata, WMEIsState metadata, HasMaybeWMPreference metadata ObjectIdentifier, HasCallStack,Diagrams.Prelude.Renderable (Diagrams.Prelude.Path V2 Double) b, Diagrams.Prelude.Renderable (Text Double) b, Members '[WorkingMemory' metadata, Embed IO, Transactional, Reader agent, Log, OpenTelemetrySpan, OpenTelemetryContext] r) => Sem r (QDiagram b V2 Double Any, WorkingMemoryGraph metadata)
renderWorkingMemoryGraph = do
  g <- generateWorkingMemoryGraph
  laidOut <- embed $ layoutGraph Dot g
  let drawing = drawGraph renderObject renderWME  laidOut
  pure (drawing,g)
