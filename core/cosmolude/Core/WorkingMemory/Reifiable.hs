{-# LANGUAGE DefaultSignatures #-}
module Core.WorkingMemory.Reifiable where

import Cosmolude

import PSCM.Effects.WorkingMemory
import Core.CoreTypes
--import Model.PSCM.BasicTypes

--import PSCM.ProblemSubstates    
    
import qualified Data.HashSet as HS
class Applicative m => ReifiableToWMEPatterns m obj where
  reifyToWMEPatterns :: obj -> m (HashSet WMEPattern, Substitution Value)

instance Applicative m =>  ReifiableToWMEPatterns m WMEPattern where
  {-# INLINE reifyToWMEPatterns #-}  
  reifyToWMEPatterns obj = pure (HS.singleton obj, emptySubstitution)





                                     
                                
class  ReifiableToWMEs obj metadata where
  reifyToWMEs :: Members '[Log, WorkingMemory' metadata,Transactional, IDGen, ObjectIdentityMap (WME' metadata)] r => obj -> Sem r [(WME' metadata, WMETruth)]
  default reifyToWMEs :: (ReifiableToObjectAugmentations obj metadata, Members '[Log, WorkingMemory' metadata,Transactional, IDGen, ObjectIdentityMap (WME' metadata)] r) => obj -> Sem r [(WME' metadata, WMETruth)]
  reifyToWMEs obj = do
      newIdent <- newGlobalIdentifier
      let oid = ObjectIdentifier (textToSymbolName $ fromString $ show (render newIdent)) newIdent
      reifyToObjectAugmentations oid obj


instance ReifiableToWMEs () metadata where
    reifyToWMEs () = pure []
instance {-#OVERLAPPABLE #-} (Foldable f, ReifiableToWMEs obj metadata) => ReifiableToWMEs  (f obj) metadata where
  {-# INLINE reifyToWMEs #-}
  reifyToWMEs wmes = do
    let wmes' = wmes^..folded
    results <- mapM reifyToWMEs wmes'
    pure (concat results)
         
instance {-# OVERLAPS #-} WMETruthValue (WME' metadata) => ReifiableToWMEs (WME' metadata) metadata where
    {-# INLINE reifyToWMEs #-}
    reifyToWMEs x = do
      truth <- readT (x^.truthValue)
      pure [(x, truth)]

--instance ReifiableToWMEs NoChangeImpasse metadata
    
    
                                 
class ReifiableToWMEs obj metadata => ReifiableToObjectAugmentations obj metadata where                 
  reifyToObjectAugmentations :: Members '[Log, WorkingMemory' metadata,Transactional, IDGen, ObjectIdentityMap (WME' metadata)] r => ObjectIdentifier -> obj -> Sem r [(WME' metadata, WMETruth)]
                                 
instance ReifiableToObjectAugmentations () metadata where
    reifyToObjectAugmentations _ _ = pure []
                                     
-- instance (CreateBlankWMEMetadata metadata,
--           HasCreatingState metadata,
--           HasWMESupport metadata) =>
--     ReifiableToObjectAugmentations NoChangeImpasse metadata where
        
