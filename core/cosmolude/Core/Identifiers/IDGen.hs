module Core.Identifiers.IDGen where
import Core.Identifiers.IdentifierType
import Core.Identifiers
import Data.Functor
import Polysemy
import Core.Variable



data IDGen m a where
  NewGlobalIdentifier :: IDGen m Identifier
  NewLocalIdentifierInContext :: Identifier -> IDGen m Identifier
  NewObjectIdentifierBasedOnVariable :: Variable -> IDGen m ObjectIdentifier
  FreshVariableBasedOn :: HasVariable base => base -> IDGen m Variable

makeSem ''IDGen

{-# INLINE newNodeIdentifier #-}
newNodeIdentifier :: Member IDGen r => Sem r NodeIdentifier
newNodeIdentifier = NodeIdentifier <$> newGlobalIdentifier

{-# INLINE newWMEIdentifier #-}
newWMEIdentifier :: Member IDGen r => Sem r WMEIdentifier
newWMEIdentifier = WMEIdentifier <$> newGlobalIdentifier

