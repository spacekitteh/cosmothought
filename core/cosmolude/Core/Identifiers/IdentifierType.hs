{-# OPTIONS_GHC -funbox-strict-fields #-}

module Core.Identifiers.IdentifierType where
import Polysemy    
import Data.Hashable
import Data.Maybe (fromJust)
import GHC.Generics
import Prelude
import Control.Lens
import Prettyprinter
import Data.UUID.Types
import qualified Text.URI as U
import Text.URI (URI)
import Language.Haskell.TH.Syntax
import Data.Data
--------------------------------------------------------------------------------
-- Identifiers
--------------------------------------------------------------------------------
data Identifier
  = UniqueIdentifier {_uuid :: !UUID}
  | ContextualIdentifier {_uniqueNumber :: !Integer, _context :: !Identifier}
  | AURI {_uri :: !URI}
  deriving stock (Eq, Ord, Generic, Lift, Data)
  deriving anyclass (Hashable)






           
instance Pretty Identifier where
  pretty (UniqueIdentifier i)
    | i == arbitraryTestIdentifierUUID = "testId"
    | i == placeholderIdentifierUUID = "placeholder ID"
    | i == termLocalVariableBaseIdentifierUUID = "term-local variable"
  pretty (AURI u) = pretty (U.render u)
  pretty UniqueIdentifier {..} = "urn:uuid:" <> viaShow _uuid
  pretty ContextualIdentifier {..} = pretty _context <> "/" <> pretty _uniqueNumber

instance Show Identifier where
    show = show . pretty
                                                        
--deriving via (OrdKey Identifier) instance TrieKey Identifier
--deriving via (OrdKey Identifier) instance ShowTrieKey Identifier
arbitraryTestIdentifierUUID :: UUID
arbitraryTestIdentifierUUID = fromJust . fromString $ "0674785f-e665-436c-b167-b8dcb48f4252"

arbitraryTestIdentifier :: Identifier
arbitraryTestIdentifier = UniqueIdentifier arbitraryTestIdentifierUUID

placeholderIdentifierUUID :: UUID
placeholderIdentifierUUID = fromJust . fromString $ "f0776a2b-0682-4597-92c6-51bbececdc7e"

placeholderIdentifier :: Identifier
placeholderIdentifier = UniqueIdentifier placeholderIdentifierUUID

termLocalVariableBaseIdentifierUUID :: UUID
termLocalVariableBaseIdentifierUUID = fromJust . fromString $ "b0e2564b-291c-40c2-9d0d-aa478894d482"

termLocalVariableBaseIdentifier :: Identifier
termLocalVariableBaseIdentifier = UniqueIdentifier termLocalVariableBaseIdentifierUUID

    

makeClassy ''Identifier
makeClassyPrisms ''Identifier
           

newtype ByIdentifier a = ByIdentifier a

instance (HasIdentifier a) => HasIdentifier (ByIdentifier a) where
  {-# INLINE identifier #-}
  identifier = coerced . (identifier @a)

instance (HasIdentifier a) => Eq (ByIdentifier a) where
  {-# INLINE (==) #-}
  a == b = a ^. identifier == b ^. identifier

instance (HasIdentifier a) => Ord (ByIdentifier a) where
  {-# INLINE (<=) #-}
  a <= b = a ^. identifier <= b ^. identifier

instance (HasIdentifier a) => Hashable (ByIdentifier a) where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt m = hashWithSalt salt (m ^. identifier)

instance (HasIdentifier a) => Pretty (ByIdentifier a) where
  {-# INLINEABLE pretty #-}
  pretty a = pretty (a ^. identifier)

instance (HasIdentifier a) => Show (ByIdentifier a) where
  {-# INLINEABLE show #-}
  show = show . pretty


                 
data GlobalContextID m a where
    GlobalContext :: GlobalContextID m Identifier

makeSem ''GlobalContextID

        
{-# INLINE runGlobalContextWith #-}        
runGlobalContextWith :: HasIdentifier agent => agent -> InterpreterFor GlobalContextID r
runGlobalContextWith agent = interpret \case
  GlobalContext -> pure (agent^.identifier)
