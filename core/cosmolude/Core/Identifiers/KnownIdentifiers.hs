{-# LANGUAGE QuasiQuotes #-}
module Core.Identifiers.KnownIdentifiers where
import Core.Identifiers.IdentifierType
import Core.Identifiers 
import Core.Truth () -- For staging

topLevelStateID :: Identifier
topLevelStateID = [ident|pscm/state|] --baseCosmoThoughtURI & UL.uriPath <>~ [ [QQ.pathPiece|pscm|], [QQ.pathPiece|state|]]
                          
topLevelStateObjectID :: ObjectIdentifier
topLevelStateObjectID = ObjectIdentifier "top" topLevelStateID
topLevelStateIdentifier :: StateIdentifier
topLevelStateIdentifier = TopState topLevelStateObjectID


noOpOperatorIdentifier :: ObjectIdentifier
noOpOperatorIdentifier = ObjectIdentifier "no-op" [ident|pscm/operators/no-op|]
                          
longTermMemoryBaseIdentifier :: Identifier
longTermMemoryBaseIdentifier = [ident|memory/longterm|] 

semanticMemoryIdentifier :: Identifier
semanticMemoryIdentifier = [ident|memory/longterm/semantic|]
emotionalMemoryIdentifier :: Identifier
emotionalMemoryIdentifier = [ident|memory/longterm/emotional|]
proceduralMemoryIdentifier :: Identifier
proceduralMemoryIdentifier = [ident|memory/longterm/procedural|] 
                    

