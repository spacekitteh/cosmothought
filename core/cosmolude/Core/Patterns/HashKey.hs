module Core.Patterns.HashKey (MemoryConstantTestHashKey, wmePatternToConstantTestHashKey, generaliseMemoryConstantTestHashKey) where

import Core.Patterns.PatternSyntax


import Core.CoreTypes


--import Core.Variable


import Core.Identifiers
import Prelude
import GHC.Generics (Generic)
import Control.Lens
import Data.Hashable
import Core.Utils.Rendering.CoreDoc
import Prettyprinter

import Math.ParetoFront hiding (singleton)


data ObjectPatternHashKey
  = OID ObjectIdentifier
  | StateVarTest
  | NormalVarTest
  | SomethingElse
  deriving (Eq, Ord, Generic, Hashable, Show)

instance Renderable ObjectPatternHashKey where
  render (OID oid) = render oid
  render StateVarTest = "state <var>"
  render NormalVarTest = "<var>"
  render SomethingElse = "[[non constant]]"
instance Debatable ObjectPatternHashKey where
    -- Domination = more general
    weigh (OID a) (OID b) = if a == b then WeakTie else StrongTie
    weigh StateVarTest (OID _) = Dominates
    weigh NormalVarTest (OID _) = Dominates
    weigh (OID _) StateVarTest = Dominated
    weigh (OID _) NormalVarTest = Dominated
    weigh StateVarTest NormalVarTest = Dominated
    weigh NormalVarTest StateVarTest = Dominates
    weigh _ _ = StrongTie
                                       
{-# INLINE objectPatternHashKey #-}
objectPatternHashKey :: Getter PatternCondition ObjectPatternHashKey
objectPatternHashKey = to go
  where
    go (APatternVariable (StatePatternVariable _) _) = StateVarTest
    go (APatternVariable (PlainPatternVariable _) _) = NormalVarTest
    go (AnObjectIdentity oid) = OID oid
    go _ = SomethingElse

data WMEPreferenceHashKey
  = NoPatternPreference
  | Can'tHavePreference
  | MustHaveAWMPreference
  | APref (WMPreference ())
  deriving (Eq, Ord, Generic, Hashable, Show)

instance Debatable WMEPreferenceHashKey where
    weigh MustHaveAWMPreference Can'tHavePreference = StrongTie
    weigh Can'tHavePreference MustHaveAWMPreference = StrongTie
    weigh MustHaveAWMPreference _ = Dominates
    weigh _ MustHaveAWMPreference = Dominated
    weigh Can'tHavePreference _ = Dominates
    weigh _ Can'tHavePreference = Dominated
    weigh NoPatternPreference NoPatternPreference = WeakTie
    weigh NoPatternPreference _ = Dominates
    weigh _ NoPatternPreference = Dominated
    weigh (APref _a) (APref _b) = StrongTie -- TODO FIXME

           
patternPrefToPrefHashKey :: WMPreferencePattern -> WMEPreferenceHashKey
patternPrefToPrefHashKey Don'tCareAboutPreference = NoPatternPreference
patternPrefToPrefHashKey MustNotHavePreference = Can'tHavePreference
patternPrefToPrefHashKey MustHaveAPreference = MustHaveAWMPreference
patternPrefToPrefHashKey (WithAPreference pref) = APref (fmap (const ()) pref)
           
instance Renderable WMEPreferenceHashKey where
  render = viaShow
           
-- data ComparisonTestHashKey = LT | LTE | EQ | GT | GTE | NE   deriving (Eq, Ord, Generic, Hashable, Show)
                           
-- instance Renderable ComparisonTestHashKey where
--   render LT = "<"
--   render LTE = "≤"
--   render EQ = "≛"
--   render GTE = "≥"
--   render GT = ">"
--   render NE = "≠"                    
              
data TestHashNormalField
  = SomeConstantPatternValue ConstantPatternValue
  | IDontCare
--  | RelationshipWithConstants (Seq (ComparisonTestHashKey, Value))
  | SomeOtherThing
  deriving (Eq, Ord, Generic, Hashable, Show)



           
instance Renderable TestHashNormalField where
  render (SomeConstantPatternValue cpv) = render cpv
  render IDontCare = "*"
--  render (RelationshipWithConstant comparisons) = "<var>" <+> render comparisons
  render SomeOtherThing = "[[something]]"

{-# INLINE patternConditionAsTestHashNormalField #-}
patternConditionAsTestHashNormalField :: PatternCondition -> TestHashNormalField
patternConditionAsTestHashNormalField (AConstantEqualityTest cpv) = SomeConstantPatternValue cpv
patternConditionAsTestHashNormalField DontCareCondition = IDontCare
--patternConditionAsTestHashNormalField (APatternVariable _ (Just
patternConditionAsTestHashNormalField _ = SomeOtherThing

data MemoryConstantTestHashKey = MemoryConstantTestHashKey ObjectPatternHashKey TestHashNormalField TestHashNormalField WMEPreferenceHashKey (Maybe IntraPatternConsistency) deriving (Eq, Ord, Generic, Hashable, Show) -- TODO: Optimise this for size.

{-# INLINE generaliseMemoryConstantTestHashKey #-}
generaliseMemoryConstantTestHashKey :: MemoryConstantTestHashKey -> [MemoryConstantTestHashKey]
generaliseMemoryConstantTestHashKey (MemoryConstantTestHashKey a b c d e) =
  [ MemoryConstantTestHashKey a' b' c' d' e' | a' <- [a, SomethingElse, StateVarTest, NormalVarTest], b' <- [b, IDontCare, SomeOtherThing], c' <- [c, IDontCare, SomeOtherThing], d' <- [NoPatternPreference, d] ++(case d of
                            APref _-> [MustHaveAWMPreference]
                            _ -> []) ,  e' <- [e]
  ]



instance Renderable MemoryConstantTestHashKey where
  render (MemoryConstantTestHashKey a b c NoPatternPreference Nothing) = parens (render a <+> render b <+> render c)
  render (MemoryConstantTestHashKey a b c d Nothing) = parens (render a <+> render b <+> render c <+> render d)
  render (MemoryConstantTestHashKey a b c NoPatternPreference (Just e)) = parens (render a <+> render b <+> render c <+> render e)
  render (MemoryConstantTestHashKey a b c d (Just e)) = parens (render a <+> render b <+> render c <+> render d <+> render e)



{-# INLINEABLE wmePatternToConstantTestHashKey #-}
wmePatternToConstantTestHashKey :: WMEPattern -> Maybe MemoryConstantTestHashKey
wmePatternToConstantTestHashKey pat = Just $ MemoryConstantTestHashKey (pat ^. patternObject . objectPatternHashKey) (patternConditionAsTestHashNormalField (pat ^. patternAttribute)) (patternConditionAsTestHashNormalField (pat ^. patternValue)) (patternPrefToPrefHashKey (pat^.patternPreference))intraPatterns
  where
    -- TODO FIXME: Hangon, intra-pattern consistency checks are non-constant tests. I haven't gotten around to implementing them yet!
    -- objConsistency = Nothing --checkForIntraPatternConsistencyBinding (pat^.patternObject) (pat^?patternAttribute.
    intraPatterns = Nothing

-- intraPatterns = case IntraPatternConsistency oev aev veo of
--                   IntraPatternConsistency Nothing Nothing Nothing -> Nothing
--                   e = Just e

                   
