
module Core.Patterns.QQ where
import Core.Identifiers.IdentifierGenerator
import Core.Patterns.Parser
import Data.Either
import Data.Typeable
import Cosmolude
import Text.Megaparsec

import Language.Haskell.TH
import Language.Haskell.TH.Quote
import Prelude (undefined)
import Data.Text
import OpenTelemetry.Trace.Core

-- | architectural dot path quasiquoter
pathPatterns ::  QuasiQuoter
pathPatterns  =  QuasiQuoter {
                               quoteExp = pathPatternsExp,
                               quotePat = pathPatternsPat,
                               quoteType = undefined,
                               quoteDec = undefined
                               }

        
pathPatternsExp  ::  String -> Q Exp
pathPatternsExp source' = do
  let source = pack source'
      offset = 0
  loc <- location
  let (line, column) = loc_start loc
      actualPosition = SourcePos (loc_filename loc) (mkPos line) (mkPos (column + offset))
      s = State
          { stateInput = source,
            stateOffset = 0,
            statePosState =
                PosState
                  { pstateInput = source,
                    pstateOffset = 0,
                    pstateSourcePos = actualPosition,
                    pstateTabWidth = defaultTabWidth,
                    pstateLinePrefix = ""
                  },
            stateParseErrors = []
                               }
  tp <- runIO getGlobalTracerProvider
  let tracer = makeTracer tp (InstrumentationLibrary "CosmoThought TH" "TH lol") tracerOptions
  parsed' <-  runIO $ customStateParseWithMinimalSem tracer True undefined (runIDGen . interpret (const undefined))  (do
                                                               baseVar <- parsePatternVariable (CompileTime ParsingUnspecified)
                                                               let basePat = APatternVariable baseVar Nothing
                                                               path <-  parseDotPathNotation (CompileTime {-w-} ParsingUnspecified) basePat
                                                               pure (makeVariablesTermLocal path)
                                                        ) s
  case parsed' of
    Right parsed -> --dataToExpQ (const Nothing `extQ`) --
                   [| parsed |]
    Left err -> fail (diagnosticToPrettyString . diagnose @WhenParsing  $ err)



-- | Extend a generic query by a type-specific case
-- From SYB
extQ :: ( Typeable a
        , Typeable b
        )
     => (a -> q)
     -> (b -> q)
     -> a
     -> q
extQ f g a = maybe (f a) g (cast a)
             
pathPatternsPat :: {-WhenParsing -> Variable ->-} String -> Q Pat
pathPatternsPat = undefined
