{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
module Core.Patterns.PatternSyntax where -- (module Core.Patterns.PatternSyntax, module Core.Patterns.PatternSyntax.Variable) where
--import qualified Control.Exception as CE (assert)
-- import Polysemy.Error
-- import Polysemy.NonDet
-- import Control.Exception (Exception)

-- import Text.Regex.PCRE.Light (Regex)
--import Control.Monad
--import Algebra.Lattice
import Core.CoreTypes
import Core.Identifiers.IdentifierGenerator
import Core.Utils.MetaTypes
import Core.Variable
import Core.Variable.AlphaEquivalence
import Core.Variable.Substitution
import Core.Utils.Rendering.CoreDoc
import GHC.Generics (Generic)
import Polysemy    
import Data.Hashable
import Control.Lens
import Core.Identifiers.IdentifierType
import Core.Identifiers
import OpenTelemetry.Trace.Core
import Control.Applicative
import Data.Text

import Data.Sequence
import Core.Utils.Prettyprinting    
import Prelude hiding ((.))
import Control.Category ((.))
import Prettyprinter
import Data.Interned.Text
import Data.List (nub)


import Data.Sequence.Lens
import Language.Haskell.TH.Syntax
import Math.ParetoFront hiding (singleton)
import Text.RE.TDFA.Text (RE, reSource)

--------------------------------------------------------------------------------
-- Raw pattern syntax
--------------------------------------------------------------------------------

data PatternVariable
  = PlainPatternVariable {_patternVariableVariable :: !Variable}
  | StatePatternVariable {_patternVariableVariable :: !Variable}
  deriving (Eq, Ord, Show, Generic, Hashable, Lift)
  deriving (Pretty) via ByRenderable PatternVariable

instance Debatable PatternVariable where
  {-# INLINEABLE weigh #-}
  weigh (PlainPatternVariable a) (PlainPatternVariable b) = if a == b then WeakTie else StrongTie
  weigh (PlainPatternVariable a) (StatePatternVariable b) = if a == b then Dominated else StrongTie
  weigh (StatePatternVariable a) (PlainPatternVariable b) = if a == b then Dominates else StrongTie
  weigh (StatePatternVariable a) (StatePatternVariable b) = if a == b then WeakTie else StrongTie

instance Renderable PatternVariable where
  render (PlainPatternVariable v) = render v
  render (StatePatternVariable v) = "state" <+> render v

makeLenses ''PatternVariable
makeClassyPrisms ''PatternVariable

instance AlphaEquivalence PatternVariable where
  {-# INLINE isAlphaEquivalent #-}
  isAlphaEquivalent (PlainPatternVariable _) (StatePatternVariable _) = (latticeToVariableRenaming incompatibleSubstitution, latticeToVariableRenaming incompatibleSubstitution)
  isAlphaEquivalent (StatePatternVariable _) (PlainPatternVariable _) = (latticeToVariableRenaming incompatibleSubstitution,latticeToVariableRenaming incompatibleSubstitution)
  isAlphaEquivalent a b = (Core.Variable.Substitution.singleton (a ^. variable) (b ^. variable), Core.Variable.Substitution.singleton (b ^. variable) (a ^. variable))

instance HasVariables PatternVariable where
  {-# INLINE variables #-}
  variables = variable

instance HasVariable PatternVariable where
  {-# INLINE variable #-}
  variable = patternVariableVariable

data ConstantPatternValue where
  PatternSymbol :: {_symbol :: !SymbolName} -> ConstantPatternValue
  PatternConstant :: {_value :: !ConstantValue} -> ConstantPatternValue
  PatternText :: {_text :: !Text} -> ConstantPatternValue
  deriving (Eq, Ord, Generic, Hashable, Renderable, Lift)
  deriving (Pretty) via ByRenderable ConstantPatternValue

instance Debatable ConstantPatternValue where
  {-# INLINEABLE weigh #-}
  weigh a b = if a == b then WeakTie else StrongTie

instance Show ConstantPatternValue where
  show = show . render

makeClassyPrisms ''ConstantPatternValue

data ComparisonAgainst
  = CompareAgainstVariable {_comparisonVariable :: !Variable}
  | CompareAgainstValue {_comparisonValue :: !Value}
  deriving (Eq, Ord, Show, Generic, Hashable, Renderable, Lift)
  deriving (Pretty) via ByRenderable ComparisonAgainst

instance Debatable ComparisonAgainst where
  {-# INLINEABLE weigh #-}
  weigh a b = if a == b then WeakTie else StrongTie

makeClassyPrisms ''ComparisonAgainst
makeClassy ''ComparisonAgainst

instance HasVariables ComparisonAgainst where
  {-# INLINE variables #-}
  variables = comparisonVariable

data ComparisonTest
  = LessThan {_comparingTo :: !ComparisonAgainst}
  | LessThanOrEqualTo {_comparingTo :: !ComparisonAgainst}
  | EqualTo {_comparingTo :: !ComparisonAgainst}
  | GreaterThan {_comparingTo :: !ComparisonAgainst}
  | GreaterThanOrEqualTo {_comparingTo :: !ComparisonAgainst}
  | NotEqualTo {_comparingTo :: !ComparisonAgainst}
  deriving (Eq, Ord, Show, Generic, Hashable, Lift)
  deriving (Pretty) via ByRenderable ComparisonTest

instance Debatable ComparisonTest where
  {-# INLINEABLE weigh #-}
  weigh (LessThan (CompareAgainstValue a)) (LessThanOrEqualTo (CompareAgainstValue b)) = if a <= b then Dominates else Dominated
  weigh (LessThan a) (LessThanOrEqualTo b) = if a == b then Dominates else StrongTie
  weigh (LessThan (CompareAgainstValue a)) (NotEqualTo (CompareAgainstValue b)) = if a <= b then Dominates else StrongTie
  weigh (LessThan a) (NotEqualTo b) = if a == b then Dominates else StrongTie
  weigh (EqualTo (CompareAgainstValue a)) (LessThanOrEqualTo (CompareAgainstValue b)) = if a <= b then Dominates else
                                                                                            StrongTie -- Error
  weigh (EqualTo a) (LessThanOrEqualTo b) = if a == b then Dominates else StrongTie
  weigh (LessThanOrEqualTo (CompareAgainstValue a)) (LessThan (CompareAgainstValue b)) = if a >= b then Dominated else Dominates
  weigh (LessThanOrEqualTo a) (LessThan b) = if a == b then Dominated else StrongTie
  weigh (NotEqualTo (CompareAgainstValue a)) (LessThan (CompareAgainstValue b)) = if a >= b then Dominated else StrongTie
  weigh (NotEqualTo a) (LessThan b) = if a == b then Dominated else StrongTie
  weigh (LessThanOrEqualTo (CompareAgainstValue a)) (EqualTo (CompareAgainstValue b)) = if a >= b then Dominated else
                                                                                            StrongTie -- Error                                      
  weigh (LessThanOrEqualTo a) (EqualTo b) = if a == b then Dominated else StrongTie

  weigh (GreaterThan (CompareAgainstValue a)) (GreaterThanOrEqualTo (CompareAgainstValue b)) = if a >= b then Dominates else Dominated                                            
  weigh (GreaterThan a) (GreaterThanOrEqualTo b) = if a == b then Dominates else StrongTie
  weigh (GreaterThan (CompareAgainstValue a)) (NotEqualTo (CompareAgainstValue b)) = if a >= b then Dominates else StrongTie                                                   
  weigh (GreaterThan a) (NotEqualTo b) = if a == b then Dominates else StrongTie
  weigh (EqualTo (CompareAgainstValue a)) (GreaterThanOrEqualTo (CompareAgainstValue b)) = if a >= b then Dominates else
                                                                                            StrongTie -- Error                                         
  weigh (EqualTo a) (GreaterThanOrEqualTo b) = if a == b then Dominates else StrongTie
  weigh (GreaterThanOrEqualTo (CompareAgainstValue a)) (GreaterThan (CompareAgainstValue b)) = if a <= b then Dominated else Dominates                                               
  weigh (GreaterThanOrEqualTo a) (GreaterThan b) = if a == b then Dominated else StrongTie
  weigh (NotEqualTo (CompareAgainstValue a)) (GreaterThan (CompareAgainstValue b)) = if a <= b then Dominated else StrongTie                                                   
  weigh (NotEqualTo a) (GreaterThan b) = if a == b then Dominated else StrongTie
  weigh (GreaterThanOrEqualTo (CompareAgainstValue a)) (EqualTo (CompareAgainstValue b)) = if a <= b then Dominated else
                                                                                            StrongTie -- Error                                             
  weigh (GreaterThanOrEqualTo a) (EqualTo b) = if a == b then Dominated else StrongTie
  weigh a b = if a == b then WeakTie else StrongTie

instance Renderable ComparisonTest where
  render (LessThan v) = "<" <+> render v
  render (LessThanOrEqualTo v) = "<=" <+> render v
  render (EqualTo v) = "==" <+> render v
  render (GreaterThan v) = ">" <+> render v
  render (GreaterThanOrEqualTo v) = ">=" <+> render v
  render (NotEqualTo v) = "/=" <+> render v

makeLenses ''ComparisonTest
makeClassyPrisms ''ComparisonTest
instance HasVariables ComparisonTest where
  {-# INLINE variables #-}
  variables = comparingTo . variables

           

data RegexWithText = RegexWithText {_theRegex :: RE, _description :: InternedText, _regexIdentifier :: Identifier}
  deriving (Generic)
  deriving (Pretty) via ByRenderable RegexWithText

instance Renderable RegexWithText where
  render RegexWithText {..} = (render _description) <+> namedBracket "regex" (render (reSource _theRegex)) <+> "ID:" <+> render _regexIdentifier

makeLenses ''RegexWithText

instance Debatable RegexWithText where
  {-# INLINEABLE weigh #-}
  weigh a b = if a == b then WeakTie else StrongTie

instance HasIdentifier RegexWithText where
  {-# INLINE identifier #-}
  identifier = regexIdentifier

-- | TODO FIXME: Make this compare the (normalised?) text of the regex
deriving via ByIdentifier RegexWithText instance Eq RegexWithText

deriving via ByIdentifier RegexWithText instance Hashable RegexWithText

deriving via ByIdentifier RegexWithText instance Ord RegexWithText

instance Show RegexWithText where
  show = show . render

instance Lift RegexWithText where
  liftTyped = liftTyped

type VariableConstraintTests = Seq ComparisonTest
{-# INLINE CONLIKE conditionConstraints #-}    
conditionConstraints :: Traversal' VariableConstraintTests ComparisonTest
conditionConstraints = traversed

    
data PatternPredicate
  = AComparisonTest {_orderingTest :: VariableConstraintTests}
  | ARegexTest {_regexTest :: !RegexWithText}
  deriving (Eq, Ord, Generic, Hashable, Renderable, Lift)
  deriving (Pretty) via ByRenderable PatternPredicate

instance Debatable PatternPredicate where
  {-# INLINEABLE weigh #-}
  weigh a b = if a == b then WeakTie else StrongTie

makeClassy ''PatternPredicate

instance HasVariables PatternPredicate where
  {-# INLINE variables #-}
  variables = confusing (orderingTest . traversed . variables)

instance Show PatternPredicate where
  show = show . render

data Satisfaction = Fails | SatisfiesWith PatternPredicate (Seq Value) deriving (Eq, Ord, Show, Generic, Hashable)

makePrisms ''Satisfaction

instance Semigroup Satisfaction where
  {-# INLINEABLE (<>) #-}
  (SatisfiesWith la lb) <> (SatisfiesWith ra rb) | la == ra = SatisfiesWith la (lb <> rb)
  _ <> _ = Fails

data PatternCondition
  = APatternVariable {_patVariable :: !PatternVariable, _predicate :: Maybe PatternPredicate}
  | AConstantEqualityTest {_constantPattern :: !ConstantPatternValue}
  | AnObjectIdentity {_oid :: !ObjectIdentifier}
  | DontCareCondition
  deriving (Eq, Ord, Generic, Hashable, Show, Lift)
  deriving (Pretty) via ByRenderable PatternCondition

instance Debatable PatternCondition where
  {-# INLINEABLE weigh #-}
  weigh a b | a == b = WeakTie
  weigh DontCareCondition _ = Dominated
  weigh _ DontCareCondition = Dominates
  weigh (APatternVariable a Nothing) (APatternVariable b Nothing) = weigh a b
  weigh (APatternVariable _ Nothing) (APatternVariable _ _) = StrongTie
  weigh (APatternVariable _ _) (APatternVariable _ Nothing) = StrongTie
  weigh (APatternVariable _ (Just a)) (APatternVariable _ (Just b)) = weigh a b
  weigh _ _ = StrongTie

instance Renderable PatternCondition where
  render DontCareCondition = "*"
  render APatternVariable {..} = render _patVariable <+> render _predicate
  render AConstantEqualityTest {..} = render _constantPattern
  render AnObjectIdentity {..} = render _oid

makeClassyPrisms ''PatternCondition
makeLenses ''PatternCondition

instance HasVariables PatternCondition where
  {-# INLINE variables #-}
  variables f (APatternVariable pv pred) =
    APatternVariable
      <$> (variables f pv)
      <*> ((_Just . variables) f pred)
  variables _ s = pure s

instance AsConstantPatternValue PatternCondition where
  {-# INLINE _ConstantPatternValue #-}
  _ConstantPatternValue = _AConstantEqualityTest

--------------------------------------------------------------------------------
-- Patterns against WMEs
--------------------------------------------------------------------------------

data WMPreferencePattern = Don'tCareAboutPreference
                         | MustNotHavePreference
                         | MustHaveAPreference
                         | WithAPreference (WMPreference Variable)
  deriving (Eq, Ord, Show, Generic, Hashable, Lift)
  deriving (Pretty) via ByRenderable WMPreferencePattern
  deriving (ToPrimitiveAttribute) via ByRenderable WMPreferencePattern
instance Renderable WMPreferencePattern where
    render Don'tCareAboutPreference = "*"
    render MustNotHavePreference = "x"
    render MustHaveAPreference = "M"
    render (WithAPreference a) = render a           
makeLenses ''WMPreferencePattern
makePrisms ''WMPreferencePattern

instance HasVariables WMPreferencePattern where
    {-# INLINE variables #-}
    variables = _WithAPreference . variables
--instance CanHaveAWMPreference WMPreferencePattern where
    

data WMEPattern = WMEPattern {_patternObject :: !PatternCondition, _patternAttribute :: !PatternCondition, _patternValue :: !PatternCondition, _patternPreference :: !WMPreferencePattern}
  deriving (Eq, Ord, Show, Generic, Hashable, Lift)
  deriving (Pretty) via ByRenderable WMEPattern
  deriving (ToPrimitiveAttribute) via ByRenderable WMEPattern

instance Debatable WMEPattern where
  {-# INLINEABLE weigh #-}
  weigh (WMEPattern obj1 attr1 val1 pref1) (WMEPattern obj2 attr2 val2 pref2) = (weigh obj1 obj2) <> (weigh attr1 attr2) <> (weigh val1 val2) <> (if pref1 == pref2 then WeakTie else StrongTie)

instance ToAttribute WMEPattern

instance HasVariables WMEPattern where
  {-# INLINE variables #-}
  variables f (WMEPattern o a v pref) = WMEPattern <$> (variables f o) <*> (variables f a) <*> (variables f v) <*> ((variables) f pref)

{-# INLINE isOperatorPattern #-}
isOperatorPattern :: WMEPattern -> Bool
isOperatorPattern WMEPattern {..} = _patternAttribute ^? _PatternSymbol == Just operatorSymbol

{-# INLINE CONLIKE variableToPatternCondition #-}
variableToPatternCondition :: Variable -> PatternCondition
variableToPatternCondition v = (APatternVariable (v ^. re _PlainPatternVariable) Nothing)

-- parseMultiValuedAttributePatterns :: (Members '[ObjectIdentityMap obj] r) =>  WhenParsing -> Parser r (NonEmpty WMEPattern)
-- parseMultiValuedAttributePatterns w = do

-- parseCompoundWMEPatterns :: (Members '[ObjectIdentityMap obj] r) =>  WhenParsing -> Parser r (NonEmpty WMEPattern)
-- parseCompoundWMEPatterns w = do
--   o <- parseObjectPattern w
--   attrWMEs <- parseDotPathNotation w o

instance Renderable WMEPattern where
  render {-pat@-}(WMEPattern {..}) =
    parens $
      render _patternObject
        <+> "^" <> render _patternAttribute
        <+> render _patternValue
          <+> render _patternPreference -- ( case _patternPreference of
             --     Just Acceptable | not (isOperatorPattern pat) -> mempty
             --     Just pref -> space <> renderWMPreference (not (isOperatorPattern pat)) pref
             --     Nothing -> mempty
             -- )

-- instance Show WMEPattern where --
--   show = show . pretty

makeLenses ''WMEPattern

-- instance HasMaybeWMPreference WMEPattern Variable where
--   {-# INLINE preference #-}
--   preference = patternPreference . _WithAPreference 

{-# INLINE patternSymbols #-}
patternSymbols :: Traversal' WMEPattern SymbolName
patternSymbols = each . _ConstantPatternValue . _PatternSymbol

{-# INLINE patternBindingVariables #-}
patternBindingVariables :: IndexedTraversal' WMEField WMEPattern Variable
patternBindingVariables = conjoined (confusing (each . patVariable . variable)) indexedVersion
  where
    indexedVersion f ~(WMEPattern o a v p) = WMEPattern <$> ((variables) ((indexed f) O) o) <*> ((variables) ((indexed f) A) a) <*> ((variables) ((indexed f) V) v) <*> pure p

instance Each WMEPattern WMEPattern PatternCondition PatternCondition where
  {-# INLINE each #-}
  each f ~(WMEPattern o a v p) = WMEPattern <$> f o <*> f a <*> f v <*> pure p

{-# INLINE patternConditions #-}                                 
patternConditions :: IndexedTraversal' WMEField WMEPattern PatternCondition
patternConditions = conjoined each (\f ~(WMEPattern o a v p) -> WMEPattern <$> (indexed f) O o <*> (indexed f) A a <*> (indexed f) V v <*> pure p)
                                 
data WMEPatternMatch' a = WMEPatternMatch {_patternMatched :: WMEPattern, _wmeSubstitution :: Substitution Value, _matchedWME :: WME' a}
  deriving (Eq, Ord, Generic, Hashable)

makeLenses ''WMEPatternMatch'

instance Renderable a => Renderable (WMEPatternMatch' a) where
  render WMEPatternMatch {..} = "WME Pattern match:" <+> angles ("Pattern:" <+> render _patternMatched <+> "Substitution" <+> render _wmeSubstitution <+> "WME:" <+> render _matchedWME)

instance Renderable a => Show (WMEPatternMatch' a) where
  show = show . render

instance HasWMEIdentifier (WMEPatternMatch' a) where
  {-# INLINE wMEIdentifier #-}
  wMEIdentifier = fusing (matchedWME . wMEIdentifier)

data VariableBinding = VariableBinding {_boundVariable :: Variable, _bindingInformation :: !BindingInformation}
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable)

                  
{-# INLINEABLE isLinearPattern #-}
isLinearPattern :: WMEPattern -> Bool
isLinearPattern pat@(WMEPattern obj attr val _)
  | (Nothing, Nothing) <- checkForIntraPatternConsistencyBinding obj (A, attr ^? patVariable) (V, val ^? patVariable),
    (Nothing, Nothing) <- checkForIntraPatternConsistencyBinding attr (V, val ^? patVariable) (O, obj ^? patVariable),
    (Nothing, Nothing) <- checkForIntraPatternConsistencyBinding val (O, obj ^? patVariable) (A, attr ^? patVariable) =
      let vars = pat ^.. patternBindingVariables in vars == nub vars
  | otherwise = False


{-# INLINE valueToPatternCondition #-}
valueToPatternCondition :: Value -> PatternCondition
valueToPatternCondition (Symbolic s) = symbolicValueToPatternCondition s
valueToPatternCondition (Constant c) = c ^. re (_ConstantPatternValue . _PatternConstant)
valueToPatternCondition (Textual t) = t ^. re (_ConstantPatternValue . _PatternText)
{-# INLINE symbolicValueToPatternCondition #-}
symbolicValueToPatternCondition :: SymbolicValue -> PatternCondition
symbolicValueToPatternCondition (Symbol attrName) = attrName ^. re (_ConstantPatternValue . _PatternSymbol)
symbolicValueToPatternCondition (ObjectIdentity oid) = AnObjectIdentity oid
symbolicValueToPatternCondition (SymbolicIdentity _) = DontCareCondition -- TODO figure out what the correct pattern is

                                    
{-# INLINE operatorPreferenceTestPattern #-}
operatorPreferenceTestPattern :: ObjectIdentifier -> WMEPattern
operatorPreferenceTestPattern oid =
  let obj = AnObjectIdentity oid
      attr = AConstantEqualityTest (PatternSymbol operatorSymbol)
      val = DontCareCondition
      pref = MustHaveAPreference
   in (WMEPattern obj attr val pref)

{-# INLINE wmeAsConstantPattern #-}
wmeAsConstantPattern :: (HasMaybeWMPreference metadata ObjectIdentifier, WMEIsState metadata, Members '[IDGen] r) => WME' metadata -> Sem r WMEPattern
wmeAsConstantPattern wme = do
  let objPat = AnObjectIdentity (wme ^. object)
      attrPat = symbolicValueToPatternCondition (wme ^. attribute)
      valPat = valueToPatternCondition (wme ^. value)
      prefPat = case wme^.preference of
               Nothing -> Don'tCareAboutPreference
               Just (pref :: WMPreference ObjectIdentifier) -> WithAPreference $ fmap (const ((error "This should not have been hit!") :: Variable)) pref --Don'tCareAboutPreference -- TODO FIGURE OUT wme ^. preference
  pure $ WMEPattern objPat attrPat valPat prefPat

{-# INLINE combinePatternMatchesOf #-}
combinePatternMatchesOf ::  Fold r (WMEPatternMatch' a) -> r -> (SubstitutionLattice Value, Seq (WME' a))
combinePatternMatchesOf f r = (combined, seqs)
  where
    (combined, seqs) = foldMapOf f mapping r
    mapping patternMatch = (patternMatch ^. wmeSubstitution . re __Substitution , seqOf matchedWME patternMatch)

class HasWMEPatterns c where
  wmePatterns :: Traversal' c WMEPattern



data IntraPatternConsistencyTest = EqualToIntra WMEField | NotEqualToIntra WMEField deriving (Eq, Ord, Generic, Hashable, Show)

instance Renderable IntraPatternConsistencyTest where
  render = viaShow



data IntraPatternConsistency = IntraPatternConsistency {_objEqAttr :: !(Maybe IntraPatternConsistencyTest, Maybe IntraPatternConsistencyTest), _attrEqVal :: !(Maybe IntraPatternConsistencyTest, Maybe IntraPatternConsistencyTest), _valEqObj :: !(Maybe IntraPatternConsistencyTest, Maybe IntraPatternConsistencyTest)} deriving (Eq, Ord, Generic, Hashable, Show)


{-# INLINEABLE checkForIntraPatternConsistencyBinding #-}
checkForIntraPatternConsistencyBinding :: PatternCondition -> (WMEField, Maybe PatternVariable) -> (WMEField, Maybe PatternVariable) -> (Maybe IntraPatternConsistencyTest, Maybe IntraPatternConsistencyTest)
checkForIntraPatternConsistencyBinding _ (_, Nothing) (_, Nothing) = (Nothing, Nothing)
checkForIntraPatternConsistencyBinding field (otherField1, other1) (otherField2, other2) = case field ^? predicate . _Just . orderingTest of
  Nothing -> (Nothing, Nothing)
  Just tests -> ((findOf (folded . _EqualTo . comparisonVariable) (\a -> Just a == other1 ^? _Just . variable) tests) <&> const (EqualToIntra otherField1), (findOf (folded . _EqualTo . comparisonVariable) (\a -> Just a == other2 ^? _Just . variable) tests) <&> const (EqualToIntra otherField2))
           


                             
makeLenses ''IntraPatternConsistency
makePrisms ''IntraPatternConsistencyTest
instance Renderable IntraPatternConsistency where
  render = viaShow



-- {-# INLINE isCoherent #-}
-- isCoherent :: Fold IntraPatternCoherency IntraPatternCoherency
-- isCoherent f TriviallyCoherent = f TriviallyCoherent
-- isCoherent f c@(Coherent _) = f c
-- isCoherent _ nope = pure nope

-- data IntraPatternCoherency = TriviallyCoherent
--                            | Coherent IntraPatternConsistencyTest
--                            | Incoherent

-- intraPatternCoherency :: Maybe Variable -> [ComparisonTest] -> [ComparisonTest] -> Maybe Variable -> [ComparisonTest -> [ComparisonTest] -> IntraPatternCoherency
-- intraPatternCoherency ov oces ocnes av aces acnes
--     | Just o <- ov, Just  a <- av = let
--                     oeHasA = has (folded . filtered (== a)) oces
--                     aeHasO = has (folded . filtered (== o)) aces
--                     oneHasA = has (folded . filtered (== a)) ocnes
--                     aneHasO = has (folded . filtered (== o)) acnes
--                in if
--                    | o == a && not (oneHasA || aneHasO) -> TriviallyCoherent
--                    | o =/ a && (oneHasA || aneHasO) ->
--                           if not (oeHasA || aeHasO)
--                           then Coherent NotEqualToIntra
--                           else Incoherent
--                    | o =/ a && (oeHasA || aeHasO) ->
--                           if not (oneHasA || aneHasO)
--                           then Coherent EqualToIntra
--                           else Incoherent
--                    | otherwise -> TriviallyCoherent -- TODO FIXME take into account greaterthan/lessthan
--                                                     -- constraints
--    | Just o <- ov =  if
--                                           where

-- intraPatternCoherency _ _ _ _ _ _ = TriviallyCoherent -- TODO FIXME take into account

-- data PatternIsIncoherent = PatternIsIncoherent WMEPattern deriving (Show, Exception)

-- simplifyPattern :: (Members '[ Error PatternIsIncoherent, NonDet] r) => WMEPattern -> Sem r (Substitution Variable, Substitution Value, WMEPattern)
-- simplifyPattern pat = do

--   -- step 1: Find all equality substitutions for variables

--   let ov = pat^?patternObject. patVariable . variable
--       av = pat^?patternAttribute . patVariable . variable
--       vv = pat^?patternValue . patVariable . variable

--       ocs = pat^..patternObject . predicate .  _Just . orderingTest . folded
--       oces = ocs^..folded . _EqualTo . comparisonVariable
--       acs = pat^..patternAttributet . predicate .  _Just . orderingTest . folded
--       aces = acs^..folded . _EqualTo . comparisonVariable
--       vcs = pat^..patternValue . predicate .  _Just . orderingTest . folded
--       vces = vcs^..folded . _EqualTo . comparisonVariable

--       let allVarEqs = oces ++ aces ++ vces
--       -- idea: go through equality tests, rename everything with its first variable in its equality
--       -- conditions, and replace the cond var with the original var. then test for alpha equality? i just
--       -- realised i'm doing sat solving here... perhaps use a logic monad? :V

--   let ocnes = ocs^..folded._NotEqualTo . comparisonVariable
--       acnes = acs^..folded._NotEqualTo . comparisonVariable
--       vcnes = vcs^..folded._NotEqualTo . comparisonVariable

-- intraPatternConsistency :: WMEPattern -> Either IntraPatternCoherency  IntraPatternConsistency
-- intraPatternConsistency pat    = result where

--     oea

--     oea = Nothing
--     aev | Just _ <- av, Just _ <- vv = Right EqualToIntra
--     aev = Nothing
--     veo | Just _ <- vv, Just _ <- ov = Right EqualToIntra
--     veo  = Nothing

