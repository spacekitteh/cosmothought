module Core.Patterns.Parser where -- (parseWMEPattern,  ParsedWMEPattern (..), WMEPathContext,    baseVariable, attributePath, lastObjPattern, lastAttrPattern, completePathContext) where

import qualified Control.Monad.Trans.Class as T
import Core.CoreTypes
-- import Text.Regex.PCRE.Light (Regex)

import Core.Identifiers.IdentifierGenerator
import Core.Utils.MetaTypes
import Core.Utils.Parsing
import Core.Variable


import Cosmolude

import qualified Data.List as DL
import Data.List.NonEmpty

import qualified Data.Sequence as Seq
import Data.Sequence.Lens
import Data.Text
import Text.RE.TDFA.Text (compileRegex)
import Language.Haskell.TH.Syntax


parseStateVariable :: WhenParsing -> Bool
parseStateVariable (ParsingRule (ParsingRuleLHS (Just ParsingStateTestPattern) (Just O))) = True
parseStateVariable ParsingUnspecified = True
parseStateVariable _ = False
                                        
           
{-# INLINEABLE parsePatternVariable #-}
parsePatternVariable :: WhenParsing -> Parser r PatternVariable
parsePatternVariable w =
  ( if alsoAtCompileTime parseStateVariable w
      then
        ( try
            ( StatePatternVariable
                <$> ( (keyword "state" <?> "State keyword") *> ((parseVariable w <?> "state variable"))
                    )
                <?> "State pattern variable"
            )
        )
      else mzero
  )
    <|> (PlainPatternVariable <$> parseVariable w <?> "Plain pattern variable")




{-# INLINEABLE parseConstantPatternValue #-}
parseConstantPatternValue :: Parser r ConstantPatternValue
parseConstantPatternValue =
  (try (PatternConstant <$> parseConstantValue))
    <|> (PatternSymbol <$> parseSymbolName)



{-# INLINEABLE parseComparisonAgainst #-}
parseComparisonAgainst :: forall metadata r. Members '[ObjectIdentityMap (WME' metadata)] r => WhenParsing -> Parser r ComparisonAgainst
parseComparisonAgainst w =
  try (CompareAgainstVariable <$> parseVariable w)
    <|> CompareAgainstValue <$> parseValue w


           
{-# INLINEABLE parseComparisonTest #-}
parseComparisonTest :: Members '[ObjectIdentityMap (WME' metadata)] r => WhenParsing -> Parser r ComparisonTest
parseComparisonTest w =
  try (LessThan <$> (keyword "<" *> parseComparisonAgainst w))
    <|> try (LessThanOrEqualTo <$> (keyword "<=" *> parseComparisonAgainst w))
    <|> try (EqualTo <$> (keyword "==" *> parseComparisonAgainst w))
    <|> try (GreaterThan <$> (keyword ">" *> parseComparisonAgainst w))
    <|> try (GreaterThanOrEqualTo <$> (keyword ">=" *> parseComparisonAgainst w))
    <|> NotEqualTo <$> (keyword "/=" *> (parseComparisonAgainst w))




{-# INLINEABLE parseRegexWithText #-}
-- TODO FIXME TODO FIXME use actual regex parsing. just run a ParsecT Void Text State internally, and grab the state before and after to synchronise
parseRegexWithText :: Parser r RegexWithText
parseRegexWithText = do
  verbatim "["
  verbatim "regex"
  verbatim "|"
  r <- stringLiteral
  regex <- compileRegex (unpack r)
  ident <- T.lift newGlobalIdentifier
  pure (RegexWithText regex "" ident)


parsePatternPredicate :: Members '[ObjectIdentityMap (WME' metadata)] r => WhenParsing -> Parser r PatternPredicate
parsePatternPredicate w =
    (try (((AComparisonTest <$> (Seq.fromList <$> some (lexeme (parseComparisonTest w)<?> "Individual comparison test"))) <?> "Comparison tests")))
    <|> (ARegexTest <$> (parseRegexWithText))



parsingRuleField :: WMEField -> WhenParsing -> Bool
parsingRuleField w (ParsingRule (ParsingRuleLHS _ (Just f))) = w == f
parsingRuleField w (ParsingRule (ParsingRuleRHS (Just f))) = w == f
parsingRuleField _ _ = False



                       
parsePatternVariableCondition ::(Members '[ObjectIdentityMap (WME' metadata)] r) => WhenParsing -> Parser r PatternCondition
parsePatternVariableCondition w = APatternVariable <$> (parsePatternVariable w) <*> optional ((bracketed (parsePatternPredicate w))  <?> "pattern predicates") 
                       
parsePatternCondition :: forall metadata r. (Members '[ObjectIdentityMap (WME' metadata)] r) => WhenParsing -> Parser r PatternCondition
parsePatternCondition w
  | parsingRuleField O w =
      try ((DontCareCondition <$ verbatim "*"))
        <|> try ((APatternVariable <$> parsePatternVariable w <*> optional (bracketed ((parsePatternPredicate w) <?> "pattern predicates"))))
        <|> ((AnObjectIdentity <$> parseObjectIdentifierLookup w))
  | otherwise = parsePatternCondition' w

                
parsePatternCondition' :: Members '[ObjectIdentityMap (WME' metadata)] r => WhenParsing -> Parser r PatternCondition
parsePatternCondition' w | parsingRuleField O w = mzero
parsePatternCondition' w =
  try ((DontCareCondition <$ verbatim "*"))
    <|> try ((AnObjectIdentity <$> parseObjectIdentifier w))
    <|> try (parsePatternVariableCondition w)
    <|> ((AConstantEqualityTest <$> (parseConstantPatternValue)))


parsePatternWMPreference :: WhenParsing -> Parser r WMPreferencePattern
parsePatternWMPreference w =  (try (MustNotHavePreference <$ verbatim "x")) <|>
                              (try (MustHaveAPreference <$ verbatim "M"))
                                  <|> 
                                 (do
                                   p  <- optional (lexeme ((parseWMPreferenceWith (parseVariable w)) <?> "WM preference"))
                                   case p^?_Just._Just of
                                     Nothing -> pure Don'tCareAboutPreference
                                     Just p' -> pure $! WithAPreference p')

parseWMEPattern {-[Identifier] ->-} :: (Members '[ObjectIdentityMap (WME' metadata)] r) => WhenParsing -> Parser r WMEPattern
parseWMEPattern w = label "WME pattern" $ between (verbatim "(") (parseExactly ")") $  label "WMEPattern internals" do
  o <- parseObjectPattern w
  a <- parseSimpleAttributePattern Simple w
  v <-  parseValuePattern w
  
  pref <- parsePatternWMPreference w

  pure
    ( WMEPattern
        o
        a
        v
        pref
    )

data AttributeType = Simple | DotHead | DotTail | Multi

--  | Structured

parseObjectPattern :: (Members '[ObjectIdentityMap (WME' metadata)] r) => WhenParsing -> Parser r PatternCondition
parseObjectPattern w = lexeme (parsePatternCondition (properField O w {-objVarIdent objPredIdent-})) <?> "object pattern"

parseAttrLeader :: Parser r (Maybe Text)
parseAttrLeader = optional (parseExactly "^" <?> "Attribute leader")
                       
parseSimpleAttributePattern ::  Members '[ObjectIdentityMap (WME' metadata)] r => AttributeType -> WhenParsing -> Parser r PatternCondition
parseSimpleAttributePattern Simple w = (lexeme ( parseAttrLeader *> ( parsePatternCondition' (properField A w)) <?> "simple attribute pattern"))
parseSimpleAttributePattern DotHead w = lexeme ((parseExactly "^") *> parsePatternCondition' (properField A w)) <?> "head of dotted attribute pattern"
parseSimpleAttributePattern DotTail w = lexeme ((parseExactly ".") *> parsePatternCondition' (properField A w)) <?> "tail path segment of dotted attribute pattern"
parseSimpleAttributePattern Multi w = lexeme ((parseExactly "^") *> parsePatternCondition' (properField A w)) <?> "multi-attribute pattern"

parseValuePattern :: (Members '[ObjectIdentityMap (WME' metadata)] r) => WhenParsing -> Parser r PatternCondition
parseValuePattern w = lexeme (parsePatternCondition (properField V w {-valVarIdent valPredIdent-})) <?> "value pattern"

{-# INLINEABLE properField #-}
properField :: WMEField -> WhenParsing -> WhenParsing
properField v = alsoAtCompileTime (properField' v)
{-# INLINEABLE properField' #-}
properField' :: WMEField -> WhenParsing -> WhenParsing
properField' v (ParsingRule (ParsingRuleLHS w Nothing)) = ParsingRule (ParsingRuleLHS w (Just v))
properField' v (ParsingRule (ParsingRuleRHS Nothing)) = ParsingRule (ParsingRuleRHS (Just v))
properField' _ w = w

----------------------- TODO
--- Refactor condition and pattern parsing to be more like the SOAR grammar (c.f. SOAR manual, sec 3.3.7.1)

-- | Given @(<x> ^first.second.third [Y])@, this will yield the following patterns:
--
-- > (<x> ^first <x>) (<x1> ^second <x2>)
-- as well as @<x2>@ and @^third@.
parseDotPathNotation ::
  (Members '[ObjectIdentityMap (WME' metadata)] r) =>
  WhenParsing ->
  -- | The variable for the base object.
  PatternCondition ->
  Parser r WMEPathContext
parseDotPathNotation w baseObject = do
  header <- parseSimpleAttributePattern DotHead w
  rest <- some (parseSimpleAttributePattern DotTail w)
  baseVar <- msumOf (_APatternVariable . _1 . variable . to pure) baseObject -- if baseObject doesn't have a var, this will make the parser fail
  T.lift $  stampOutWMEPath (baseVar, baseObject) (header:|rest)




stampOutWMEPath :: (Members '[IDGen{-, ObjectIdentityMap obj-}] r) =>  
  -- | The variable for the base object.
  (Variable, PatternCondition) ->  
  -- | The path
  NonEmpty PatternCondition ->
  Sem r WMEPathContext --(NonEmpty WMEPattern, (PatternCondition, PatternCondition))          
stampOutWMEPath (baseVar,baseObject) (header :| rest) = do 
  headVar <- freshVariableBasedOn baseVar

  let headVarPat = variableToPatternCondition headVar
      basePattern = WMEPattern baseObject header headVarPat Don'tCareAboutPreference
  ((finalVar, _, finalAttr), theTail) <-
    mapAccumM
      ( \(_old :: Variable, objVar :: Variable, _priorAttr :: PatternCondition) attr -> do
          valVar <- freshVariableBasedOn baseVar
          let valPat = variableToPatternCondition valVar
              pat = WMEPattern (variableToPatternCondition objVar) attr valPat Don'tCareAboutPreference
          pure ((objVar, valVar, attr), pat)
      )
      (headVar, headVar, header)
      rest
  let finalPat = variableToPatternCondition finalVar
  pure $ WMEPathContext baseVar (basePattern :| (DL.init theTail)) finalPat  finalAttr
        




data WMEPathContext = WMEPathContext {_baseVariable :: Variable,  _attributePath :: NonEmpty WMEPattern,  _lastObjPattern :: PatternCondition, _lastAttrPattern :: PatternCondition} deriving (Eq, Show, Generic, Lift)
makeLenses ''WMEPathContext
completePathContext :: Monad m => m PatternCondition -> WMEPathContext -> m (Seq WMEPattern)
completePathContext valuePat (WMEPathContext {..}) = do
  val <- valuePat
  pure $ (seqOf folded _attributePath) :> (WMEPattern _lastObjPattern _lastAttrPattern val Don'tCareAboutPreference)
                    
instance HasVariables WMEPathContext where
    {-# INLINE variables #-}
    variables f (WMEPathContext v pats lp la) = WMEPathContext <$> f v <*> variables f pats <*> variables f lp <*> variables f la
instance Renderable WMEPathContext where
    render (WMEPathContext base path lastObj lastAttr) = vsep ["Base variable:" <+> render base,
                                                               "Path:" <+> render path,
                                                               "Last object:" <+> render lastObj,
                                                               "Last attribute:" <+> render lastAttr,
                                                               "Metasyntax:" <+>  (hsep (render <$> path^..folded)) <+> "(" <> render lastObj <+>  "^" <> render lastAttr <+> "[[X]])"
                                                              ]


-- newtype ParsedWMEPattern = ParsedWMEPattern WMEPattern      
-- instance ( Members '[ ObjectIdentityMap (WME' metadata), Transactional, IDGen] r) => ParameterParser ParsedWMEPattern c r where
--   parameterDescription = "WME pattern"
--   parse = adaptToCommand (ParsedWMEPattern <$> parseWMEPattern ParsingExpression)                                                         
