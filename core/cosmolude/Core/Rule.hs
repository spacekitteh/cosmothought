{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE StrictData #-}
module Core.Rule (Rule (Rule), RuleLHS,  RuleRHS , RuleLHSInfo , RuleRHSInfo , RuleInfo,  ruleIdentifier,   ruleLHSInfo, ruleRHSInfo,  stateTestingPattern, Core.Rule.operatorSupported, ruleProvenance, RuleProvenance(..),  HasRule(rule,ruleDescription, ruleLHS, ruleRHS, ruleInfo,extraBindings), WithoutMeta(WithoutMeta), actions, updateStateTestPattern,  conditionPatterns, RuleCondition(PositiveCondition,  NegatedConjunction), AppearanceSense, proveIsOfSense, varsInPositiveCondition,  HasConditions(..), ConditionSense(..), rulePatternConditions , objectsToCreate) where

import qualified Control.Lens.Unsound as UNSAFE (adjoin)




import Data.Functor.Bind.Class (Apply)
import Core.Action.Expression
import Core.CoreTypes


import Core.Patterns.PatternSyntax
import Core.Rule.Parts

import Prelude
import Control.Lens
import Core.Identifiers
import Core.Identifiers.IdentifierType
import Data.Hashable
import Core.Utils.MetaTypes
import Core.Variable.AlphaEquivalence
import Prettyprinter
import Core.Utils.Rendering.CoreDoc
import GHC.Generics (Generic)
import Core.Variable
import Core.Variable.Substitution


import Data.Sequence (Seq)


import Core.Rule.Condition









data Rule = Rule {_ruleIdentifier' :: RuleIdentifier, _ruleDescription :: CoreDoc, _ruleLHS :: RuleLHS, _ruleRHS :: RuleRHS,  _extraBindings :: Substitution (RuleExpr Variable), _objectsToCreate :: Seq Variable, _ruleInfo :: RuleInfo}
  deriving stock (Show, Generic)

makeClassy ''Rule





           

updateStateTestPattern :: Rule -> Rule
updateStateTestPattern rule = rule & ruleInfo.ruleLHSInfo.stateTestPattern .~ (rule^. stateTestingPattern)

{-# INLINE operatorSupported #-}
operatorSupported :: Getter Rule (Maybe WMEPattern)
operatorSupported = ruleInfo . Core.Rule.Parts.operatorSupported 

{-# INLINE stateTestingPattern #-}
stateTestingPattern :: Getter Rule WMEPattern
stateTestingPattern =  singular wmePatterns

instance HasConditions Rule where
  {-# INLINE conditions #-}
  conditions = fusing (ruleLHS . conditions)

instance HasWMEPatterns Rule where
  {-# INLINE wmePatterns #-}
  wmePatterns = {-UNSAFE.adjoin (ruleInfo.ruleLHSInfo . stateTestPattern)-}  confusing (UNSAFE.adjoin (cloneTraversal (ruleLHS . each1 . ( conditionPatterns) )) (cloneTraversal (ruleRHS . rhsPatterns)))

-- instance {-#OVERLAPS #-} Each Rule Rule Variable Variable where
--   -- \| =Safety= The patterns in each side are disjoint; thus the traverrsal laws will hold. If a pattern occurs
--   --       twice, then either it occurs twice in the LHS, twice in the RHS, or once each side; but patterns on the LHS
--   --       are different from patterns on the RHS by virtue of pattern direction.
--   --
--   {-# INLINE each #-}
--   each = UNSAFE.adjoin (wmePatterns . variables) (extraBindings.traversed . traversed )

-- TODO possibly optimise
instance HasVariables Rule where
  {-# INLINE variables #-}
  variables =  confusing (UNSAFE.adjoin (wmePatterns . variables) (UNSAFE.adjoin (objectsToCreate . traversed) (UNSAFE.adjoin (extraBindings . __Substitution . traverseSubstitutionKeysWith (const )) (extraBindings. __Substitution . traversed . traversed ) )))

instance HasRuleIdentifier (Rule) where
  {-# INLINE ruleIdentifier #-}
  ruleIdentifier = ruleIdentifier'

instance HasIdentifier (Rule) where
  {-# INLINE identifier #-}
  identifier = ruleIdentifier . identifier

{-# INLINE rulePatternConditions #-}               
rulePatternConditions :: (HasRule r, Apply f,
      Applicative f) =>
      IndexedLensLike' WMEField f r PatternCondition
rulePatternConditions = conjoined (confusing (ruleLHS . conditions . conditionPatterns . patternConditions)) (fusing (ruleLHS.conditions) . conditionPatterns . patternConditions)
               
deriving via (ByIdentifier (Rule)) instance Eq (Rule)

deriving via (ByIdentifier (Rule)) instance Hashable (Rule)

deriving via (ByIdentifier (Rule)) instance Ord (Rule)

newtype WithoutMeta = WithoutMeta {_withMeta :: Rule}

makeLenses ''WithoutMeta

instance HasVariables WithoutMeta  where
  {-# INLINE variables #-}
  variables = withMeta . variables

instance Eq WithoutMeta where
  {-# INLINE (==) #-}
  (WithoutMeta a) == (WithoutMeta b) = (a ^. ruleLHS) == (b ^. ruleLHS) && (a ^. ruleRHS) == (b ^. ruleRHS) && (a^.extraBindings == b ^.extraBindings)

instance Hashable WithoutMeta where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt (WithoutMeta a) = hashWithSalt salt (a ^. ruleLHS, a ^. ruleRHS, a ^. extraBindings)

instance AlphaEquivalence WithoutMeta where
  {-# INLINE isAlphaEquivalent #-}
  isAlphaEquivalent (WithoutMeta a) (WithoutMeta b) = (a & identifier .~ termLocalVariableBaseIdentifier & ruleDescription .~ (b ^. ruleDescription)) =@=? (b & identifier .~ termLocalVariableBaseIdentifier) 

deriving via (EqualityModuloAlpha WithoutMeta) instance AlphaEquivalence Rule

instance Renderable (Rule) where
  render Rule {..} = braces (vsep [dquotes _ruleDescription, render _ruleLHS, "-->", render _ruleRHS])


-- applyRuleResult :: RuleResult -> Sem r ()


