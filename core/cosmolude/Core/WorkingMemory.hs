{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE LinearTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE BangPatterns #-}
module Core.WorkingMemory (HasWorkingMemoryInitialisedToken(..),HasWorkingMemoryData(workingMemoryData), WorkingMemoryInitialised, processWorkingMemory, resetWorkingMemory, WorkingMemoryData(WorkingMemoryData), blankWorkingMemory, {-collectDirectChildIdentifiers,-} module PSCM.Effects.WorkingMemory, WorkingMemoryConstraints, WorkingMemoryAgentConstraints)  where
import GHC.Exts
import Prelude (Integral(..))
import qualified Prelude (error)
import Polysemy.Transaction
import Core.CoreTypes
import Core.Patterns.PatternSyntax
import Core.Identifiers.IdentifierGenerator
--import Core.WME
import Cosmolude hiding (LookupObjectIDByName, lookupObjectIDByName, NumberOfReferences,numberOfReferences)
import qualified Data.HashSet as HS
import Core.Rule.Database

import Polysemy.Reader
import StmContainers.Map as STMM
import StmContainers.Multimap as STMMM
import StmContainers.Set as STMS

import PSCM.Effects.WorkingMemory

data ObjectReferences = ObjectReferences {_objectsItIsReferencedBy :: STMMM.Multimap ObjectIdentifier WMEIdentifier,
                                          _otherObjectsItReferences :: STMMM.Multimap ObjectIdentifier WMEIdentifier }
{-# INLINE newObjectReferences #-}                      
newObjectReferences :: STM ObjectReferences
newObjectReferences = ObjectReferences <$> STMMM.new <*> STMMM.new

makeLenses ''ObjectReferences                      
                                                                       
    
data WorkingMemoryData metadata = WorkingMemoryData
  { _asSet :: STMMM.Multimap ObjectIdentifier (WME' metadata),
    _objectIdentities :: STMM.Map SymbolName ObjectIdentifier,
    _wmesByHashKey :: STMMM.Multimap MemoryConstantTestHashKey (WME' metadata),
    _contentIdentifiers :: STMM.Map WMEContentKey WMEIdentifier,
    _objectReferences :: STMM.Map ObjectIdentifier ObjectReferences,
    _oidsToGC :: STMS.Set ObjectIdentifier,
    _objectsToPreserve :: STMS.Set ObjectIdentifier
  }

instance Renderable (WorkingMemoryData metadata) where
  render _ = "Working memory data: render todo"

instance Show (WorkingMemoryData metedata) where
  show = show . render

{-# INLINE CONLIKE blankWorkingMemory #-}
blankWorkingMemory :: STM (WorkingMemoryData metadata)
blankWorkingMemory = WorkingMemoryData <$> STMMM.new <*> STMM.new <*> STMMM.new <*> STMM.new <*> STMM.new  <*> STMS.new <*> STMS.new

                                   

data WorkingMemoryInitialised = UNSAFEWorkingMemoryInitialisedToken

class HasWorkingMemoryInitialisedToken ty where
    proveWorkingMemoryIsInitialised :: Action STM ty WorkingMemoryInitialised
instance HasWorkingMemoryInitialisedToken WorkingMemoryInitialised where
    {-# INLINE proveWorkingMemoryIsInitialised #-}
    proveWorkingMemoryIsInitialised = id

makeClassy ''WorkingMemoryData

{-# INLINEABLE initialiseWorkingMemory #-}
initialiseWorkingMemory :: (Member Transactional r, HasWorkingMemoryData wmd metadata) => wmd -> Sem r ()
initialiseWorkingMemory wmd | workingMemory <- wmd^.workingMemoryData = do
  -- insert top-level state identifier
  workingMemory^!objectIdentities . insertingKVM (topLevelStateObjectID^.symbol') topLevelStateObjectID
  workingMemory^!objectsToPreserve . inserting topLevelStateObjectID


{-# INLINEABLE addToObjectReferences #-}                     
addToObjectReferences :: forall agent wmd metadata prefType r.
                         (HasTracer agent, HasExtraObjectIdentifiers metadata,
                          HasWorkingMemoryData wmd metadata,
                          HasMaybeWMPreference (WME' metadata) prefType,
                          AsObjectIdentifier prefType,
                          Members '[
                           Log,
                           Reader agent,
                           OpenTelemetrySpan,
                           OpenTelemetryContext,
                           Resource,
                           Transactional
                           ] r) =>
                         wmd -> WME' metadata -> Sem r ()
addToObjectReferences wmd' wme | wmd <- wmd'^.workingMemoryData = push "Core.WorkingMemory.addToObjectReferences"
  do
    let wmeIdent = wme^.wMEIdentifier
    wmeObjRefs <- wmd^!objectReferences.getOrCreate (act \_ -> newObjectReferences) (wme^.object)
--    addAttributes [("working_memory.referenced_oids_in_wme", toAttribute count)]
    forMOf_ (objectIdentifiersInWME @prefType . filtered (/= (wme^.object))) wme (\oid -> do
                                                      
                                      wmeObjRefs^!otherObjectsItReferences . insertingKVMM oid wmeIdent
                                      refsFor <- wmd^!objectReferences.getOrCreate (act \_ -> newObjectReferences) oid
                                      refsFor^!objectsItIsReferencedBy . insertingKVMM (wme^.object) wmeIdent 
                                               
                                      -- Remove from OIDs to GC
                                      wmd^!oidsToGC.deleting oid
                                   )
             
{-# INLINEABLE removeFromObjectReferences #-}
removeFromObjectReferences :: forall agent wmd metadata prefType r. (HasTracer agent, HasExtraObjectIdentifiers metadata, HasWorkingMemoryData wmd metadata, HasMaybeWMPreference (WME' metadata) prefType, AsObjectIdentifier prefType, Members '[Log, Reader agent, OpenTelemetrySpan, OpenTelemetryContext , Resource,Transactional] r) => wmd -> WME' metadata -> Sem r ()
removeFromObjectReferences wmd' wme | wmd <- wmd'^.workingMemoryData = push "Core.WorkingMemory.removeFromObjectReferences" do
  let wmeIdent = wme^.wMEIdentifier
  wmeObjRefs <- wmd^!objectReferences.getOrCreate (act \_ -> newObjectReferences) (wme^.object)
--  addAttributes [("working_memory.referenced_oids_in_wme", toAttribute count)]
  forMOf_ (objectIdentifiersInWME @prefType . filtered (\ obj -> obj /= (wme^.object))) wme  (\(oid::ObjectIdentifier) -> do
                                      wmeObjRefs^!otherObjectsItReferences.deletingKVMM oid wmeIdent 
                                      referenced <- lookupMap oid (wmd^.objectReferences) 
                                      case referenced of
                                        Nothing -> do
                                            -- Something funky has occured; just noop
                                            error "Something weird happened while removing from object references" [("wme", toAttribute (wmeIdent^.identifier))] 
                                        Just refs -> do
                                         refs^!objectsItIsReferencedBy.deletingKVMM (wme^.object) wmeIdent
                                         isntReferenced <- isNullMultimap (refs^.objectsItIsReferencedBy)
                                         when isntReferenced do
                                           preserved <- lookupSet oid (wmd^.objectsToPreserve)
                                           when (not preserved) do
                                             wmd^!oidsToGC.inserting oid                                   
                                   )       


-- {-# INLINEABLE increaseReferences #-}
-- increaseReferences :: forall wmd metadata prefType r. (HasExtraObjectIdentifiers metadata, HasWorkingMemoryData wmd metadata, HasMaybeWMPreference (WME' metadata) prefType, AsObjectIdentifier prefType, Members '[Log, Reader agent, OpenTelemetrySpan, OpenTelemetryContext , Resource,Transactional] r) => wmd -> WME' metadata -> Sem r ()
-- increaseReferences wmd' wme  | wmd <- wmd'^.workingMemoryData =  push "increaseReferences" do
--   let count = lengthOf (objectIdentifiersInWME @prefType)  wme
--   addAttributes [("working_memory.referenced_oids_in_wme", toAttribute count)]
--   forMOf_ (objectIdentifiersInWME @prefType) wme (\oid -> do                                      
--                                       count <- wmd^!objectReferenceCount.getOrCreate (act \_ -> pure 0) oid
                                               
--                                        -- Remove from OIDs to GC
--                                       when (count == 0) do
--                                         wmd^!oidsToGC.deleting oid
                                           
--                                       -- Increase reference count
--                                       wmd^!objectReferenceCount.insertingKVM oid (count +1)
--                                    )
  
-- {-# INLINEABLE decreaseReferences #-}
-- decreaseReferences :: forall wmd metadata prefType r. (HasExtraObjectIdentifiers metadata, HasWorkingMemoryData wmd metadata, HasMaybeWMPreference (WME' metadata) prefType, AsObjectIdentifier prefType, Members '[Log, Reader agent, OpenTelemetrySpan, OpenTelemetryContext , Resource,Transactional] r) => wmd -> WME' metadata -> Sem r ()
-- decreaseReferences wmd' wme | wmd <- wmd'^.workingMemoryData = do
--   let count = lengthOf (objectIdentifiersInWME @prefType)  wme
--   addAttributes [("working_memory.referenced_oids_in_wme", toAttribute count)]
--   forMOf_ (objectIdentifiersInWME @prefType) wme  (\oid -> do                                      
--                                       count' <- lookupMap oid (wmd^.objectReferenceCount)
--                                       case count' of
--                                         Nothing ->
--                                             -- Something funky has occured; just noop
--                                             pure ()
--                                         Just k | k <= 1 -> do
--                                           -- When the count is 1 or 0, we need to GC it          
--                                           wmd^!oidsToGC.inserting oid
--                                           wmd^!objectReferenceCount.deletingKVM oid
--                                         Just k -> do
--                                           -- Otherwise, just decrease the ref count
--                                           wmd^!objectReferenceCount.insertingKVM oid (k - 1)                                      
--                                    )
  

                                            
               
{-# INLINEABLE addWMEToWMD #-}
addWMEToWMD :: forall agent wmd metadata prefType r. (HasTracer agent, HasExtraObjectIdentifiers metadata, WMEIsState metadata, HasWorkingMemoryData wmd metadata, HasMaybeWMPreference (WME' metadata) prefType, AsObjectIdentifier prefType, Members '[Log, Reader agent, OpenTelemetrySpan, OpenTelemetryContext , Resource, Transactional] r) => wmd -> WME' metadata -> MemoryConstantTestHashKey -> WMEContentKey -> Sem r ()
addWMEToWMD workingMemory' w masterPattern contentKey | workingMemory <- workingMemory'^.workingMemoryData = do
  workingMemory^!objectIdentities . insertingKVM (w^.object.symbol') (w^.object)
  workingMemory ^! contentIdentifiers . insertingKVM contentKey (w ^. wmeIdentity)
  insertInMultimap w (w ^. object) (workingMemory ^. asSet)
  addToObjectReferences @_ @_ @_ @prefType workingMemory w
                   
  mapM_ insertPattern (generaliseMemoryConstantTestHashKey masterPattern)  

    where
      insertPattern p =   workingMemory'^!workingMemoryData.wmesByHashKey.insertingKVMM p w

  

{-# INLINE resetWorkingMemory #-}
resetWorkingMemory :: (Member Transactional r, HasWorkingMemoryData wmd metadata) => wmd -> Sem r (WorkingMemoryInitialised)
resetWorkingMemory wmd' | wmd@(WorkingMemoryData asSet objectIdentities wmesByHashKey contentIdentifiers objectReferences wmesToGC objectsToPreserve) <- wmd'^.workingMemoryData = do
  resetMultimap asSet
  resetMap objectIdentities
  resetMultimap wmesByHashKey
  resetMap contentIdentifiers
  resetMap objectReferences
  resetSet wmesToGC
  resetSet objectsToPreserve
  initialiseWorkingMemory wmd
  pure  UNSAFEWorkingMemoryInitialisedToken



getObjectProperties :: Members '[Transactional] r =>
                       WorkingMemoryData metadata ->
                       ObjectIdentifier ->
                       Sem r (Maybe (STMS.Set (WME' metadata)))                               
getObjectProperties workingMemory i = lookupMultimapByKey i (workingMemory ^. asSet)


type WorkingMemoryAgentConstraints agent metadata = (HasTracer agent, HasExtraObjectIdentifiers metadata, HasWorkingMemoryInitialisedToken agent, HasIdentifierGenerationState agent, HasWorkingMemoryData agent metadata, WMEIsState metadata, HasMaybeWMPreference metadata ObjectIdentifier)
                                      
type WorkingMemoryConstraints agent metadata r = ( WorkingMemoryAgentConstraints agent metadata, HasCallStack, Members '[OpenTelemetrySpan, OpenTelemetryContext , Resource, Log, Reader agent, Transactional] r)
                                      
{-# INLINE processWorkingMemory #-}
processWorkingMemory :: (Show metadata, Members '[IDGen, RuleDatabase] r, WorkingMemoryConstraints agent metadata r) =>   InterpreterFor (WorkingMemory' metadata) r
processWorkingMemory sem = do
  agent <- ask
  _initialised@(!UNSAFEWorkingMemoryInitialisedToken) <- embedSTM (agent^!proveWorkingMemoryIsInitialised)
  idGenState <- ask <&> view identifierGenerationState
  workingMemory <- ask <&> view workingMemoryData
  -- Initialise top-level state OID
  interpret  (\case
   NumberOfReferences o -> push "NumberOfReferences" do
    addAttributes [("pscm.working_memory.oid", toAttribute o)]
    refs <- lookupMap o (workingMemory^.objectReferences)
    case refs of
      Nothing -> pure Nothing
      Just r ->  Just . fromInteger . toInteger <$> numberOfKeysInMultimap (r^.objectsItIsReferencedBy)
   GetWMEsToGC -> push "GetWMEsToGC" do
    trash <- workingMemory^!oidsToGC.elems.(act \oid -> getObjectProperties workingMemory oid) . _Just . extractSet
    addAttributes [("pscm.working_memory.gc.number_to_delete", toAttribute (HS.size trash))]
    pure trash

   FindWMEsByPattern pat ->
    {-# SCC findWMEsByPattern #-}
    push "FindWMEByPattern" do
      addAttributeWithRaw "pscm.working_memory.pattern" pat
      let masterPattern' = wmePatternToConstantTestHashKey pat
      case masterPattern' of
        Nothing -> Prelude.error "non-constant patterns not implemented" -- pure []
        Just masterPattern -> do
          addAttributeWithRaw "pscm.working_memory.wme_lookup.master_pattern" masterPattern
          wmes <- lookupMultimapByKey masterPattern (workingMemory ^. wmesByHashKey)
          wmes^!! _Just .  elems
   AllWMEsByHash key ->
    push "AllWMEsByHash" do
     lookupMultimapByKey key (workingMemory^.wmesByHashKey)
   AddWME w -> push "AddWME" do
    pat <- wmeAsConstantPattern w
    let masterPattern' = wmePatternToConstantTestHashKey pat
    case masterPattern' of
      Nothing -> do
        error  "Invalid pattern!" ((makeAttributesWithRaw "pscm.working_memory.wme_addition.pattern" pat) <> (makeAttributesWithRaw "pscm.working_memory.wme_addition.wme" w))
      Just masterPattern -> do
        debug' $ "Master pattern: " <+> viaShow masterPattern                               
        let  contentKey = WMEContentKey (w ^. object) (w ^. attribute) (w ^. value) (w^.preference)
        addWMEToWMD @_ @_ @_ @ObjectIdentifier workingMemory w masterPattern contentKey

   RemoveWME w -> do
    removeFromObjectReferences @_ @_ @_ @ObjectIdentifier workingMemory w            
    pat <- wmeAsConstantPattern w
    let masterPattern' = wmePatternToConstantTestHashKey pat
    case masterPattern' of
      Nothing -> pure ()
      Just masterPattern -> do           
        debug' $ "Master pattern: " <+> viaShow masterPattern
        deleteFromMultimap w (w ^. object) (workingMemory ^. asSet)
        mapM_ deletePattern (generaliseMemoryConstantTestHashKey masterPattern)

    where
      deletePattern p =   workingMemory^!wmesByHashKey.deletingKVMM p w
   GetAllObjectsAsKVs -> workingMemory^!!asSet.elemsMM
   GetReferencedObjects obj -> workingMemory^!! objectReferences . getOrCreate (act \ _ -> newObjectReferences) obj . otherObjectsItReferences . multimapKeys
   ContentAddressableIdentifier contentKey -> do
    workingMemory ^! contentIdentifiers . getOrCreate (act \_ -> WMEIdentifier <$> newRandomIdentifier) contentKey
   ObjectProperties i -> do
     getObjectProperties workingMemory i
   PreserveObjectFromGC obj -> do
                workingMemory^!objectsToPreserve . inserting obj
   StopPreservingObjectFromGC obj -> do
                workingMemory^!objectsToPreserve . deleting obj                             
   LookupObjectIDByName n -> do
    workingMemory ^! objectIdentities . getOrCreate (act $ \n -> ObjectIdentifier n <$> (runIDGenSTMWith idGenState newGlobalIdentifier)) n

   -- TODO: Use ruleID to look up further helpful context for nicer naming
   CreateFreshObjectIDBasedOnVariable _ruleID var -> push "CreateFreshObjectIDBasedOnVariable" $ do
     let baseName = var^.variableName
         baseID' = symbolNameToURIIdentifier baseName
     attr "pscm.variable.name" ( fromString . show $ baseName) do
       case baseID' of
         Nothing -> do
           error  "Attempted to create Identifier URI" (makeAttributesWithRaw "pscm.variable" var)
           pure Nothing
         Just baseID -> fmap Just do
           newID <- newLocalIdentifierInContext baseID
     
           let symName = textToSymbolName . fromString . show . render $ newID
               oid = ObjectIdentifier symName newID
           workingMemory^!objectIdentities . insertingKVM symName oid
           pure oid
             ) sem


-- {-# INLINEABLE collectDirectChildIdentifiers #-}

-- -- | Collect the identifiers in all of an object's augmentations.
-- collectDirectChildIdentifiers :: (HasCallStack, Members '[Transactional, (WorkingMemory' metadata)] r) => ObjectIdentifier -> Sem r (HS.HashSet Identifier)
-- collectDirectChildIdentifiers i = do
--   props <- objectProperties i
--   case props of
--     Nothing -> pure HS.empty
--     Just wmes ->
      
--         Polysemy.Transaction.foldlM'
--           ( \current input ->
--               do
--                 pure (current <> (foldMapOf identifiersInWME HS.singleton input))
--           )
--           HS.empty
--           (STMS.unfoldlM wmes)
