{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# OPTIONS_GHC -funbox-strict-fields #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
module Core.CoreTypes
  ( WMEContentKey (..),
                  CreateBlankWMEMetadata (..),
--                  CanHaveAWMPreference (..),
    field,
    parseWME',
    parseIsState,
    parseValue,
    parseConstantValue,
    symbolicValue,
    oid,
    BindingInformation(BindingInformation),
                      definingWME, definingField,
    AsIdentifier (..),
    Value (..),
    AsValue(..),
    ConstantValue (..),
    asRational,
    AsSymbolicValue (..),
    SymbolicValue (..),
    AsConstantValue (..),
    WME' (..),
    WMEIsState (..),
    WMEIdentifier (..),
    SymbolName,
    wmeToSExpressionString,
    Core.CoreTypes.Value.symbol,
    identity,
    constantValue,
    objectIdentifiersInWME,
    ByIdentifier (..),
    WMESupport (..),
    HasWMESupport (..),
    HasWMPreference (..),
    AsWMPreference(..),
    WMPreference (..),
    WMEParseInfo (..),
    parseWMPreferenceWith,
    prettyWMPreference,
    renderWMPreference,
    operatorSymbol,
    operatorValue,
    WMETruthValue (..),
    ByWMPreference (..),
    HasMaybeWMPreference(..),
    module Core.CoreTypes.WME,
    identifiersInWME, allIdentifiersInWME
  )
where
import Data.HashSet (HashSet)  
import qualified Data.HashSet as HS  
import Algebra.PartialOrd

import Core.CoreTypes.WME    
import qualified Control.Monad.Trans.Class as T
import Core.Identifiers
import Core.Variable
import Core.Utils.MetaTypes
import Core.Identifiers.IdentifierType
import Core.Utils.Rendering.CoreDoc
import GHC.Generics
import Control.Lens
import Polysemy
import Core.Utils.Parsing
import Prelude hiding ((.))
import Control.Concurrent.STM
import Data.Maybe    

import Prettyprinter

import Control.Category ((.))

import Data.Hashable
import Math.ParetoFront

import Data.Text (Text)
import Text.Megaparsec.Char

import Text.Megaparsec.Char.Lexer hiding (lexeme)
-- import Text.Megaparsec.Debug



import Language.Haskell.TH.Syntax
import Core.CoreTypes.Value
--------------------------------------------------------------------------------
-- WMEs
--------------------------------------------------------------------------------


class WMEIsState wme where
  isState :: Lens' wme Bool

class WMETruthValue wme where
  truthValue :: Lens' wme (TVar WMETruth)


class CreateBlankWMEMetadata a where
    createBlankWMEMetadata :: STM a
data WMPreference ty = Required | Prohibit | Reject | Acceptable | Best | Worst | OtherIsPreferable ty | PreferableOverOther ty | IndifferentTo ty | Indifferent (Maybe  Double)  deriving (Eq, Show, Generic, Hashable, Functor, Foldable, Traversable, Lift, Ord)
makeClassy ''WMPreference
makeClassyPrisms ''WMPreference
class HasMaybeWMPreference c ty where
    preference :: Lens' c (Maybe (WMPreference ty))

-----------------------------------------------------------------------------------------------------------------------
-------------------------------------TODO FIGURE THIS OUT--------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
{-# INLINE identifiersInWME #-}
identifiersInWME :: HasMaybeWMPreference (WME' a) ObjectIdentifier => Fold (WME' a) Identifier
identifiersInWME =
  runFold $ (Fold $ attribute . _Identifier) <> (Fold $ value . _Symbolic . _Identifier) <> (Fold $ preference . _Just . folded . identifier @ObjectIdentifier)  -- TODO don't forget WM preference?

{-# INLINE allIdentifiersInWME #-}
allIdentifiersInWME :: HasMaybeWMPreference (WME' a) ObjectIdentifier => Fold (WME' a) Identifier
allIdentifiersInWME =
  runFold $ Fold (object . identity') <> Fold identifiersInWME
           
-- data WithAPreference = NoPreference |                   
-- class CanHaveAWMPreference c ty where
--     possiblePreference :: Traversal' c ((Maybe (WMPreference ty)))
--     default possiblePreference :: HasMaybeWMPreference c ty => Traversal' c (Maybe (Maybe (WMPreference ty)))
--     possiblePreference =  preference 
                              
parseIsState :: Parser r (Bool)
parseIsState = do
  isState <- optional (verbatim "state") <?> "state keyword"
  if (isJust isState) then pure True else pure False

data WMEParseInfo = WMEParseInfo {wmeIsState :: ~Bool, attributeIsSymbol :: ~Bool, value :: ~(Maybe (Either Text ConstantValue)), isOperatorProposal :: ~Bool} deriving (Eq, Read, Show)


parseObjectField :: forall r a. (Members '[ObjectIdentityMap (WME' a)] r) => Parser r ObjectIdentifier
parseObjectField = (try (lexeme (parseObjectIdentifierLookup @(WME' a) ParsingWME) <?> "WME object"))
                   <|> (errorWhenParses ParsingWME (parseVariable ParsingWME )
                   (\case
                     ParsingWME -> Just (ParsingError ParsingWME Nothing (VariableInWME ^. re _WMEParseError))
                     _ -> Nothing
                   )
                   )   
parseAttributeField :: (Members '[ObjectIdentityMap (WME' a)] r) => Parser r SymbolicValue
parseAttributeField = (try (lexeme ((optional (string "^")) *> parseSymbolicValue ParsingWME) <?> "WME attribute"))
                      <|> (errorWhenParses ParsingWME (parseVariable ParsingWME )
                   (\case
                     ParsingWME -> Just (ParsingError ParsingWME Nothing (VariableInWME ^. re _WMEParseError))
                     _ -> Nothing
                   )
                   )   
                      
parseValueField :: (Members '[ObjectIdentityMap (WME' a)] r) => Parser r Value
parseValueField = lexeme (parseValue ParsingWME) <?> "WME value"
           
parseWME' :: forall r a. (Members '[ObjectIdentityMap (WME' a)] r, Show a, WMEIsState a, (HasMaybeWMPreference a ObjectIdentifier) ) => (WMEParseInfo -> Parser r a) -> (WMEContentKey -> Sem r WMEIdentifier) -> Parser r ((WMEParseInfo, WME' a))
parseWME' parseMetadata wmeIdentGen = parseParens $ label "WME internals" do
  isState <- (parseIsState <?> "WME state")
  o <- parseObjectField
  a <- parseAttributeField
  v <- parseValueField
--  possibleIdent <- T.lift (wmeIdentGen (WMEContentKey o a v pref))
--  ident <- (option possibleIdent (WMEIdentifier <$> (try parseIdentifier) <?> "WME identifier"))
  let info =
        WMEParseInfo
          { wmeIsState = isState,
            attributeIsSymbol = (has _Symbol a),
            value = case v of
              _vn | Just vn <- v ^? _SymbolicValue . _Symbol -> Just (Left (symbolNameToText vn))
              _tn | Just vn <- v ^? _Textual -> Just (Left vn)
              _cv | Just cv <- v ^? _ConstantValue -> Just (Right cv)
              _ -> Nothing,
            isOperatorProposal = a == operatorSymbol ^. re _Symbol
          }
  meta <- (parseMetadata info <?> "WME metadata")
  ident <- T.lift (wmeIdentGen (WMEContentKey o a v (meta^.preference)))
  pure
    ( info,
      WME
        o
        a
        v
        ident
        meta
    )


{-# INLINEABLE field #-}
field :: WMEField -> Getter (WME' a) Value
field O = object . re _ObjectIdentity . re (_Symbolic)
field A = attribute . re (_Symbolic)
field V = value


instance (WMEIsState a) => WMEIsState (WME' a) where
  {-# INLINE isState #-}
  isState = fusing (metadata . isState)

instance WMEIsState Bool where
  {-# INLINE isState #-}
  isState = id





wmeToSExpressionString :: WME' a -> String
wmeToSExpressionString = show . render

data WMESupport = Architectural | ManuallySpecified | RuleSupported (HashSet InstantiationIdentifier) (HashSet ObjectIdentifier) deriving (Eq, Ord, Show, Generic, Hashable)

instance Renderable WMESupport where
  render = viaShow

instance PartialOrd WMESupport where
    {-# INLINE leq #-}
    Architectural `leq` Architectural = True
    Architectural `leq` _ = False
    ManuallySpecified `leq` Architectural = True
    ManuallySpecified `leq` ManuallySpecified = True
    ManuallySpecified `leq` _ = False
    (RuleSupported iids1 oids1) `leq` (RuleSupported iids2 oids2) = HS.isSubsetOf iids1 iids2 && HS.isSubsetOf oids1 oids2
    (RuleSupported _ _) `leq` _ =  True
    -- (OperatorSupported oids1) `leq` (OperatorSupported oids2) = HS.isSubsetOf oids1 oids2
    -- (OperatorSupported _) `leq` Architectural = True
    -- (OperatorSupported _) `leq` ManuallySpecified = True
    -- (OperatorSupported _) `leq` (InstantiationSupported _) = False
    -- (InstantiationSupported lhs) `leq` (InstantiationSupported rhs) = HS.isSubsetOf lhs rhs
    -- (InstantiationSupported _) `leq` _ = True

instance Semigroup WMESupport where
    {-# INLINE (<>) #-}
    Architectural <> _ = Architectural
    _ <> Architectural = Architectural
    ManuallySpecified <> _ = ManuallySpecified
    _ <> ManuallySpecified = ManuallySpecified
    (RuleSupported iids1 oids1) <> (RuleSupported iids2 oids2) = RuleSupported (iids1<>iids2) (oids1 <> oids2)
    -- (OperatorSupported lhs) <> (OperatorSupported rhs) = OperatorSupported (lhs <> rhs)
    -- lhs@(OperatorSupported _) <> _ = lhs
    -- _ <> rhs@(OperatorSupported _) = rhs
    -- (InstantiationSupported lhs) <> (InstantiationSupported rhs) = InstantiationSupported (lhs <> rhs)
           
class HasWMESupport obj where
    wmeSupport :: Lens' obj (TVar WMESupport)

                     
data WMEContentKey = WMEContentKey ObjectIdentifier SymbolicValue Value (Maybe (WMPreference ObjectIdentifier))
  deriving stock (Eq, Show, Generic)
  deriving anyclass (Hashable)


{-# INLINE objectIdentifiersInWME #-}
objectIdentifiersInWME :: forall prefType a. ( HasExtraObjectIdentifiers  a, AsObjectIdentifier prefType, HasMaybeWMPreference (WME' a) prefType) => Fold (WME' a) ObjectIdentifier
objectIdentifiersInWME =
  runFold $ (Fold $ attribute . _ObjectIdentity) <> (Fold $ value . _Symbolic . _ObjectIdentity) <> Fold (preference @(WME' a) @prefType . _Just . folded . _ObjectIdentifier) <> Fold (metadata . extraObjectIdentifiers)
                 
instance HasExtraObjectIdentifiers ty => HasExtraObjectIdentifiers (WMPreference ty) where
    {-# INLINE extraObjectIdentifiers #-}
    extraObjectIdentifiers = confusing (traversed . extraObjectIdentifiers)

                 
newtype ByWMPreference a = ByWMPreference {_unByWMPreference :: a} deriving newtype (Eq, Show, Generic, Hashable)
makeLenses ''ByWMPreference

instance (HasIdentifier a) => HasIdentifier (ByWMPreference a) where
    {-# INLINE identifier #-}
    identifier = fusing (unByWMPreference . identifier)
instance (HasWMPreference a ty) => HasWMPreference (ByWMPreference a) ty where
    {-# INLINE wMPreference #-}
    wMPreference = fusing (unByWMPreference . wMPreference)

instance HasMaybeWMPreference metadata ty => HasMaybeWMPreference (WME' metadata) ty where
    {-# INLINE preference #-}
    preference = fusing (metadata . preference)


data BindingInformation = BindingInformation {_definingWME :: !WMEIdentifier, _definingField :: {-# UNPACK #-} !WMEField}
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable)

makeLenses ''BindingInformation

instance HasIdentifier BindingInformation where
  identifier :: Lens' BindingInformation Identifier
  identifier = definingWME . identifier





-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------                   

-- rankEqually :: (HasIdentifier obj, HasIdentifier ty, HasWMPreference obj ty ) => WMPreference ty -> WMPreference obj -> (ByWMPreference obj) -> ByWMPreference obj -> Maybe Comparison
-- rankEqually good bad a@(view wMPreference -> x) b@(view wMPreference -> y) |x == good && x == y && a^.identifier == b^.identifier = Just StrongTie
-- rankEqually good bad a@(view wMPreference -> x) b@(view wMPreference -> y) |x == bad && x == y && a^.identifier == b^.identifier = Just StrongTie
-- rankEqually good bad a@(view wMPreference -> x) b@(view wMPreference -> y) |x == good && y == bad && a^.identifier == b^.identifier = Just StrongTie
-- rankEqually good bad a@(view wMPreference -> x) b@(view wMPreference -> y) |x == bad && y == good && a^.identifier == b^.identifier = Just StrongTie
-- rankEqually good bad a@(view wMPreference -> x) b@(view wMPreference -> y) |x == good && x == y  = Just WeakTie
-- rankEqually good bad a@(view wMPreference -> x) b@(view wMPreference -> y) |x == bad && x == y = Just WeakTie
-- rankEqually good bad a@(view wMPreference -> x) b@(view wMPreference -> y) |x == good && y == bad = Just WeakTie
-- rankEqually good bad a@(view wMPreference -> x) b@(view wMPreference -> y) |x == bad && y == good  = Just WeakTie
-- rankEqually good bad a@(view wMPreference -> x) _ | x == good || x == bad = Just Dominated
-- rankEqually good bad _ a@(view wMPreference -> y) | y == good || y == bad = Just Dominated                                                                             
-- rankEqually _ _ _ _ = Nothing


instance (HasIdentifier obj, HasIdentifier ty, HasWMPreference obj ty ) =>  Debatable (ByWMPreference obj) where
    {-# INLINABLE weigh #-}
    -- | Here we order preferences by the *order of processing*.

--render $ stratum (OperatorProposal topLevelStateObjectID Required) <> stratum (OperatorProposal topLevelStateObjectID Prohibit) <> stratum (OperatorProposal noOpOperatorIdentifier Acceptable) <> stratum (OperatorProposal noOpOperatorIdentifier Reject) <> stratum (OperatorProposal noOpOperatorIdentifier (PreferableOverOther topLevelStateObjectID)) <> stratum (OperatorProposal topLevelStateObjectID Acceptable)<> stratum (OperatorProposal topLevelStateObjectID (PreferableOverOther noOpOperatorIdentifier)) <> stratum (OperatorProposal noOpOperatorIdentifier Required)<> stratum (OperatorProposal noOpOperatorIdentifier Prohibit) <> stratum (OperatorProposal topLevelStateObjectID Reject) <> stratum (OperatorProposal (ObjectIdentifier "foo" arbitraryTestIdentifier ) Acceptable) <> stratum (OperatorProposal (ObjectIdentifier "lonesome" [ident|lolol|]) Best) <> stratum (OperatorProposal noOpOperatorIdentifier Best) <> stratum (OperatorProposal topLevelStateObjectID Best) <> stratum (OperatorProposal topLevelStateObjectID Worst) <> stratum (OperatorProposal (ObjectIdentifier "lonesome" [ident|lolol|]) Acceptable)


    -- Stage 5: Indifference
    -- NOTE! We explicitly _don't_ want to take numeric preferences into account here; that is for later mechanisms to use.
    -- BUGBUG? What about Indifferent/Indifferent? Shouldn't that be a StrongTie?
    weigh (view wMPreference -> IndifferentTo a') (view wMPreference -> IndifferentTo b') | a'^.identifier == b'^.identifier = StrongTie
    weigh (view wMPreference -> IndifferentTo _) (view wMPreference -> Indifferent _) = StrongTie
    weigh (view wMPreference -> Indifferent _) (view wMPreference -> IndifferentTo _) = StrongTie                                                                                      
    weigh (view wMPreference -> IndifferentTo _) _  = Dominated
    weigh _ (view wMPreference -> IndifferentTo _)  = Dominates
    weigh (view wMPreference -> Indifferent _) _  = Dominated
    weigh _ (view wMPreference -> Indifferent _)  = Dominates



    -- Stage 4: Best/worst
    weigh a@(view wMPreference -> Best) b@(view wMPreference -> Worst) | a^.identifier == b^.identifier = StrongTie
    weigh a@(view wMPreference -> Worst) b@(view wMPreference -> Best) | a^.identifier == b^.identifier = StrongTie
    weigh (view wMPreference -> Best) (view wMPreference -> Worst) = Dominates
    weigh (view wMPreference -> Worst) (view wMPreference -> Best) = Dominated
    weigh (view wMPreference -> Worst) (view wMPreference -> Worst) = StrongTie
    weigh (view wMPreference -> Best) (view wMPreference -> Best) = StrongTie
                                                                        
    weigh (view wMPreference -> Worst) _ = Dominated
    weigh _ (view wMPreference -> Worst) = Dominates
    weigh (view wMPreference -> Best) _ = Dominated
    weigh _ (view wMPreference -> Best) = Dominates

    -- Stage 3: Better/worse
    -- Two preferences 
    weigh a@(view wMPreference -> PreferableOverOther b') b@(view wMPreference -> PreferableOverOther a') | (a^.identifier) == (a'^.identifier) &&  (b^.identifier == b'^.identifier) = StrongTie
    weigh a@(view wMPreference -> OtherIsPreferable b') b@(view wMPreference -> OtherIsPreferable a') | a^.identifier == a'^.identifier &&  b^.identifier == b'^.identifier = StrongTie
                                                                                                                                                                              
    weigh (view wMPreference -> OtherIsPreferable b') ( b) | (b'^.identifier) == (b^.identifier) = Dominates
    weigh (view wMPreference -> PreferableOverOther a) ( b) | a^.identifier == b^.identifier = Dominated
    weigh (view wMPreference -> OtherIsPreferable a') (view wMPreference -> PreferableOverOther b) | a'^.identifier == b^.identifier = Dominated
    weigh a (view wMPreference -> OtherIsPreferable b) | a^.identifier == b^.identifier = Dominates

                                                                                          
    weigh a (view wMPreference -> PreferableOverOther b') | a^.identifier /= b'^.identifier = WeakTie
    weigh (view wMPreference -> PreferableOverOther a') b | a'^.identifier /= b^.identifier = WeakTie
    weigh (view wMPreference -> PreferableOverOther _) _ = Dominated
    weigh _ (view wMPreference -> PreferableOverOther _) = Dominates
    weigh (view wMPreference -> OtherIsPreferable _) _ = Dominated
    weigh _ (view wMPreference -> OtherIsPreferable _) = Dominates

    -- Stage 2: Accept/reject
    weigh a@(view wMPreference -> Reject) b@(view wMPreference -> Acceptable) | a^.identifier == b^.identifier = StrongTie
    weigh a@(view wMPreference -> Acceptable) b@(view wMPreference -> Reject) | a^.identifier == b^.identifier = StrongTie
    weigh (view wMPreference -> Reject) (view wMPreference -> Acceptable) = Dominates
    weigh (view wMPreference -> Acceptable) (view wMPreference -> Reject) = Dominated
    weigh (view wMPreference -> Acceptable) (view wMPreference -> Acceptable) = StrongTie
    weigh (view wMPreference -> Reject) (view wMPreference -> Reject) = StrongTie
                                                                        
    weigh (view wMPreference -> Acceptable) _ = Dominated
    weigh _ (view wMPreference -> Acceptable) = Dominates
    weigh (view wMPreference -> Reject) _ = Dominated
    weigh _ (view wMPreference -> Reject) = Dominates

                                                    
    -- Stage 1: Require/prohibit
    -- If an operator is both required and prohibited, this is the first thing that must be considered
    weigh a@(view wMPreference -> Required) b@(view wMPreference -> Prohibit) | a^.identifier == b^.identifier = StrongTie
    weigh a@(view wMPreference -> Prohibit) b@(view wMPreference -> Required) | a^.identifier == b^.identifier= StrongTie
    -- A require and prohibit on different operators need to be considered at the same time.
    weigh (view wMPreference -> Required) (view wMPreference -> Prohibit) = WeakTie
    weigh (view wMPreference -> Required) (view wMPreference -> Required) = WeakTie
    weigh (view wMPreference -> Prohibit) (view wMPreference -> Prohibit) = WeakTie
    weigh (view wMPreference -> Prohibit) (view wMPreference -> Required) = WeakTie
    -- Require/prohibit preferences are considered first.
    weigh (view wMPreference -> Required) _ = Dominates
    weigh (view wMPreference -> Prohibit) _ = Dominates
-- redundant?    weigh _ (view wMPreference -> Required) = Dominated
-- redundant?    weigh _ (view wMPreference -> Prohibit) = Dominated



                                                         

                                           

-- redundant?    weigh _ _ = WeakTie                                                                                            
    --weigh (view wMPreference -> Best)


-- instance PartialOrd (WMPreference ty) where
--     {-# INLINE leq #-}
--     Required `leq` Prohibited = False
--     Prohibited `leq` Required = False
--     Required `leq` _ = True
--     Acceptable `leq` Prohibited = False
--     _ `leq` Prohibited = True

--     _ `leq` Acceptable = True
    


           
instance HasVariables (WMPreference Variable) where
  {-# INLINEABLE variables #-}
  variables f (OtherIsPreferable var) = OtherIsPreferable <$> f var
  variables f (PreferableOverOther var) = PreferableOverOther <$> f var
  variables _ pref = pure pref

instance (HasWMPreference metadata ty) => HasWMPreference (WME' metadata) ty where
  {-# INLINE wMPreference #-}
  wMPreference = fusing (metadata . wMPreference)

prettyWMPreference :: forall ty ann.
  Renderable ty =>
  -- | If acceptable is implicit
  Bool ->
  WMPreference ty ->
  Doc ann
prettyWMPreference b t =  unAnnotate $ renderWMPreference  b t
renderWMPreference ::
  Renderable ty =>
  -- | If acceptable is implicit
  Bool ->
  WMPreference ty ->
  CoreDoc      
renderWMPreference _ Required = "!"
renderWMPreference _ Reject = "-"
renderWMPreference _ Prohibit = "~"
renderWMPreference _ (PreferableOverOther o) = ">" <+> render o
renderWMPreference _ (OtherIsPreferable o) = "<" <+> render o
renderWMPreference True Acceptable = ""
renderWMPreference False Acceptable = "+"
renderWMPreference _ (IndifferentTo o) = "=" <+> render o
renderWMPreference _ (Indifferent Nothing) = "="
renderWMPreference _ (Indifferent (Just d)) = "="<+> render d
renderWMPreference _ Best = ">"
renderWMPreference _ Worst = "<"
instance Renderable ty => Renderable (WMPreference ty) where
    render = renderWMPreference False
parseWMPreferenceWith :: Parser r ty -> Parser r (Maybe (WMPreference ty))
parseWMPreferenceWith tyParser = Just <$> (try (OtherIsPreferable <$> (keyword "<" *> tyParser))
                                 <|> try (PreferableOverOther <$> (keyword ">" *> tyParser))
                                 <|> try (Best <$ verbatim ">")
                                 <|> try (Worst <$ verbatim "<")
                                 <|> try (Acceptable <$ verbatim "+")
                                 <|> try (Prohibit <$ verbatim "~")
                                 <|> try (Reject <$ verbatim "-")
                                 <|> try (Required <$ verbatim "!")
                                 <|> try (IndifferentTo <$> (keyword "=" *> tyParser))
                                 <|> try ((Indifferent <$ (verbatim "=")) <*> optional (signed sc float)))
