module Core.CoreTypes.GoalDependencySet (
    HasGoalDependencySet(..),
    GoalDependencySet(..),
 ) where
import GHC.Generics
--import Prelude
import Control.Lens
import qualified StmContainers.Set as STMS
import Core.Identifiers


newtype GoalDependencySet = GDS {_underlyingGDS :: STMS.Set WMEIdentifier} deriving stock (Generic)
makeClassy ''GoalDependencySet

