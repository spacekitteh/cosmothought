{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# OPTIONS_GHC -funbox-strict-fields #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}


module Core.CoreTypes.WME (WME'(WME), object, attribute, value, wmeIdentity, metadata{-, identifiersInWME, allIdentifiersInWME-})
    where
import Control.Lens
import Core.Identifiers
import Core.Identifiers.IdentifierType
import GHC.Generics
import Data.Functor    
import Core.Utils.Rendering.CoreDoc
import Data.Hashable
import OpenTelemetry.Attributes
import Core.CoreTypes.Value
import Prelude
import Prettyprinter
    
    
data WME' a = WME {_object :: ObjectIdentifier, _attribute :: SymbolicValue, _value :: Value, _wmeIdentity :: WMEIdentifier, _metadata :: a}
  deriving stock (Show, Generic, Functor)
    
makeLenses ''WME'
instance Eq (WME' a) where
  {-# INLINE (==) #-}
  a == b = a._wmeIdentity == b._wmeIdentity

instance HasIdentifier (WME' a) where
  {-# INLINE identifier #-}
  identifier = fusing (wmeIdentity . identifier)
           
instance HasObjectIdentifier (WME' a) where
  {-# INLINE objectIdentifier #-}
  objectIdentifier = object

instance Ord (WME' a) where
  {-# INLINE (<=) #-}
  a <= b = a ^. wmeIdentity <= b ^. wmeIdentity
           
instance Renderable (WME' a) where
  render w = parens $  render (w ^. object) <+> "^" <> render (w ^. attribute) <+> render (w ^. value)             
deriving via ByRenderable (WME' a) instance Renderable a => ToPrimitiveAttribute (WME' a)

instance Hashable (WME' a) where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt WME {..} = hashWithSalt salt _wmeIdentity
         
         
instance HasWMEIdentifier (WME' a) where
  {-# INLINE wMEIdentifier #-}
  wMEIdentifier = wmeIdentity

instance Field1 (WME' a) (WME' a) ObjectIdentifier ObjectIdentifier where
  {-# INLINE _1 #-}
  _1 = object

instance Field2 (WME' a) (WME' a) SymbolicValue SymbolicValue where
  {-# INLINE _2 #-}
  _2 = attribute

instance Field3 (WME' a) (WME' a) Value Value where
  {-# INLINE _3 #-}
  _3 = value

instance HasCreatingState a => HasCreatingState (WME' a) where
    {-# INLINE creatingState #-}
    creatingState = fusing (metadata . creatingState)

