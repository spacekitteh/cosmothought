{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ViewPatterns #-}

module Core.Action where
import Control.Lens
import Core.Variable
import Model.PSCM.BasicTypes
import Core.Patterns.PatternSyntax
import Data.Hashable
import Core.Utils.Rendering.CoreDoc
import GHC.Generics (Generic)
import Prelude
import Prettyprinter
import Core.Utils.Prettyprinting
import qualified Df1
import Prettyprinter.Render.String
import System.IO

type ProcessArgument = String

data MessageDestination = File FilePath (Maybe Handle) -- OsPath
  deriving (Eq, Generic)

instance Show MessageDestination where
  show (File path _) = show path

stdOutDestination :: MessageDestination
stdOutDestination = File "/dev/stdout" (Just stdout)



instance Hashable MessageDestination where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt (File path _) = hashWithSalt salt path

data WhenToExecute = OnActivation | OnRetraction | Both deriving (Eq, Show, Generic, Hashable)

instance Renderable WhenToExecute where
  render OnActivation = "on activation"
  render OnRetraction = "on retraction"
  render Both = "when activated and retracted"
--  render OnCommand = "on command"
                     
executeOnActivation :: WhenToExecute -> Bool
executeOnActivation OnRetraction = False
--executeOnActivation OnCommand = False
executeOnActivation _ = True

executeOnRetraction :: WhenToExecute -> Bool
executeOnRetraction OnActivation = False
--executeOnRetraction OnCommand = False
executeOnRetraction _ = True


data RuleAction
  = NewWME {_actionPattern :: WMEPattern}
  | RemoveWME {_actionPattern :: WMEPattern}
  | Log {_whenToExecute :: WhenToExecute, _messageLevel :: Df1.Level, _docStream :: CoreDocStream}
  | Output {_whenToExecute :: WhenToExecute, _destination :: MessageDestination, _docStream :: CoreDocStream}
  | SetQuiescenceFuelForCurrentState {_fuelLimit :: Maybe QuiescenceFuel}
  | RunProcess {_whenToExecute :: WhenToExecute, _path :: FilePath, _args :: [ProcessArgument]}
  | TestSuccess {_whenToExecute :: WhenToExecute}
  | TestFailure {_whenToExecute :: WhenToExecute, _docStream :: CoreDocStream}
  deriving (Eq, Generic,  Hashable)
  deriving (Pretty) via ByRenderable RuleAction



instance Show RuleAction where
    show (NewWME w) = "NewWME " ++ show w
    show (RemoveWME w) = "RemoveWME " ++ show w
    show x = show . render $ x

--renderMaybeWhenToExecute :: Maybe WhenToExecute -> CoreDoc
--renderMaybeWhenToExecute Nothing = 
--renderMaybeWhenToExecute (Just when) = " " <> "on" <+> render when
             
instance Renderable RuleAction where
  render (NewWME w) = render w
  render (Core.Action.RemoveWME w) = "-" <> render w
  render (Core.Action.Log when lvl w) = "log at level" <+> render lvl <+> render when <+> dquotes (render (renderString w))
  render (Output when fd w) = "print to" <+> (dquotes $ viaShow fd) <+> render when <+> dquotes (render (renderString w))
  render (SetQuiescenceFuelForCurrentState Nothing) = "set fuel to unlimited"
  render (SetQuiescenceFuelForCurrentState (Just fuel)) = "set fuel to" <+> render fuel
  render (RunProcess when path args) = "exec" <+> render when <+> render path <+> render (concat args) <> ","
  render (TestSuccess when) = "success" <+> render when
  render (TestFailure when w) = "failure" <+> render when <+> "with" <+> dquotes (render (renderString w))


data ActionWMEPatternTag = AddingWME | RemovingWME

makeClassy ''RuleAction
makeClassyPrisms ''RuleAction

instance HasVariables RuleAction where
    {-# INLINE variables #-}
    variables = actionPattern.variables
