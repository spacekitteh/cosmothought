{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}

module Core.Variable.Substitution (Substitution, constructSubstitution, AsSubstitution (__Substitution), ifoldedSubstitution, HasSubstitution (substitution), (|=>), mergeSubstitutions, singleton, emptySubstitution, traverseSubstitutionKeysWith, SubstitutionLattice, VariableRenaming, renameVariables, renameVariables', composeSubstitution, latticeToVariableRenaming, extractSubstitutionFromVariableMapping, extractSubstitutionFromVariableMappingBy, substitutionsOverlap, variableRenamingToSubstitutionLattice, incompatibleSubstitution, trivialSubstitution ) where

import Algebra.Lattice
import Algebra.PartialOrd
import Control.Applicative
import Control.Lens
import Control.Lens.Extras
import Control.Monad
import Core.Utils.Prettyprinting
import Core.Variable
import Data.Coerce
import Data.Function
import qualified Data.HashMap.Lazy as HML
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HMS
import Data.Hashable
import Data.Maybe
import GHC.Generics (Generic)
import GHC.Stack
import Polysemy
import Polysemy.AtomicState
import Prettyprinter
import Prelude
import Data.List (sort)
--------------------------------------------------------------------------------
-- Substitutions
--------------------------------------------------------------------------------
newtype Substitution v = Substitution {_varMap :: HashMap Variable v}
  deriving newtype (Functor, FoldableWithIndex Variable)
  deriving stock (Generic, Show, Foldable, Traversable)
  deriving (Pretty) via ByRenderable (Substitution v)



makeClassy ''Substitution
makeClassyPrisms ''Substitution
-- | The HashMap equality instance is broken when there are hash collisions.
instance Eq v => Eq (Substitution v) where
    {-# INLINEABLE (==) #-}
    (Substitution a) == (Substitution b) = HMS.isSubmapOf a b && HMS.isSubmapOf b a
deriving anyclass instance Hashable v => Hashable (Substitution v)
instance Ord v => Ord (Substitution v) where
    {-# INLINEABLE compare #-}
    compare a b = ((a^@..ifoldedSubstitution) & partsOf each %~ sort) `compare` ((b^@..ifoldedSubstitution) & partsOf each %~ sort)
instance (Renderable v) => Renderable (Substitution v) where
  render (Substitution m) = namedBracket "σ" (align $ vsep $ punctuate comma $ HMS.toList m <&> \(var, val) -> render var <+> "⤇" <+> render val)

blankSubstitution :: Substitution v
blankSubstitution = Substitution HMS.empty
trivialSubstitution :: AsSubstitution k v => k
trivialSubstitution = blankSubstitution ^. re __Substitution
-- instance Eq v  => Semigroup (Substitution v) where
--     {-# INLINE (<>) #-}
--     a <> b | Just res <- mutualIfOverlapping a b = res
--            | otherwise = mempty
-- instance Semigroup (Substitution v) =>  Monoid (Substitution v) where
--     mempty = blankSubstitution

{-# INLINE ifoldedSubstitution #-}
ifoldedSubstitution :: (AsSubstitution s v) => IndexedFold Variable s v
ifoldedSubstitution = __Substitution . varMap . ifolded

constructSubstitution :: (AsSubstitution s v, Foldable f) => f (Variable, v) -> s
constructSubstitution container = (Substitution . HMS.fromList $ container ^.. folded) ^. re __Substitution

{-# INLINE singleton #-}
singleton :: (AsSubstitution s v) => Variable -> v -> s
singleton k v = (Substitution (HMS.singleton k v)) ^. re __Substitution

type instance Index (Substitution v) = Variable

type instance IxValue (Substitution v) = v

instance Ixed (Substitution v) where
  {-# INLINE ix #-}
  ix k = coerced . ix @(HashMap Variable v) k

instance At (Substitution v) where
  {-# INLINE at #-}
  at ::
    Index (Substitution v) ->
    Lens' (Substitution v) (Maybe (IxValue (Substitution v)))
  at k = coerced . at @(HashMap Variable v) k

infixr 5 |=>

{-# INLINE (|=>) #-}
(|=>) :: AsSubstitution s v => Variable -> v -> s -> s
!var |=> !val = __Substitution . varMap %~ (HMS.insert var val)

-- {-# INLINE substitutionsOverlap #-}

-- | Determine if two substitutions are compatible (that is, the substitutions produce identical results)
substitutionsOverlap :: (Eq v) => Substitution v -> Substitution v -> Bool
substitutionsOverlap !left !right = andOf folded (HMS.intersectionWith (==) (left ^. varMap) (right ^. varMap))

instance (Eq v) => PartialOrd (Substitution v) where
  {-# INLINE leq #-}
  a `leq` b = (a ^. varMap) `HMS.isSubmapOf` (b ^. varMap)

invalidSubstitution :: (AsSubstitution s v, Alternative f) => f s
invalidSubstitution = empty

{-# INLINEABLE combineIfOverlapping #-}
combineIfOverlapping :: (Alternative f, Eq v, AsSubstitution s v) => s -> s -> f s
combineIfOverlapping left' right'
  | Just left <- left' ^? __Substitution,
    Just right <- right' ^? __Substitution,
    substitutionsOverlap left right,
    unified <-
      HMS.unionWith
        (\l r -> if l == r then l else Nothing)
        (fmap Just $ left ^. varMap)
        (fmap Just $ right ^. varMap) =
      if allOf folded (is _Just) unified then pure ((Substitution (fmap fromJust unified)) ^. re __Substitution) else invalidSubstitution
  | Just _ <- left' ^? __Substitution, Nothing <- right' ^? __Substitution = pure left'
  | Just _ <- right' ^? __Substitution, Nothing <- left' ^? __Substitution = pure right'
  | otherwise = invalidSubstitution

{-# INLINEABLE mutualIfOverlapping #-}
mutualIfOverlapping :: (Alternative f, Eq v, AsSubstitution s v) => s -> s -> f s
mutualIfOverlapping left' right'
  | Just left <- left' ^? __Substitution,
    Just right <- right' ^? __Substitution,
    unified <-
      HMS.intersectionWith
        (\l r -> if l == r then l else Nothing)
        (fmap Just $ left ^. varMap)
        (fmap Just $ right ^. varMap),
    allOf folded (is _Just) unified =
      pure $ (Substitution (fmap fromJust unified)) ^. re __Substitution
  | otherwise = invalidSubstitution

-- instance (Eq (m (Substitution v)), Eq v, MonadPlus m) => Lattice (m (Substitution v)) where
-- {-# INLINE (\/) #-}
-- a \/ b  | a == pure blankSubstitution = b
--        -- | b == pure blankSubstitution = a
--        -- | otherwise =
--       =
--                       do

--   -- b' <- if b == empty then pure blankSubstitution else b
--   a' <- a
--   b' <- b
--   combineIfOverlapping a' b'

-- {-# INLINE (/\) #-}
-- a /\ b = do
--   a' <- a
--   b' <- b
--   mutualIfOverlapping a' b'

-- instance (Eq v) => Lattice (Substitution v) where
--     (\/) = (<>)
--     a /\ b | Just res <-  mutualIfOverlapping a b = res
--            | otherwise = blankSubstitution

-- instance (Eq v) => BoundedJoinSemiLattice (Substitution v) where
--     bottom = blankSubstitution
-- instance (Eq (m (Substitution v)), Eq v, MonadPlus m) => BoundedJoinSemiLattice (m (Substitution v)) where
--     bottom :: HasCallStack => m (Substitution v)
--     bottom = empty

{-# INLINE emptySubstitution #-}
emptySubstitution :: (AsSubstitution s v) => s
emptySubstitution = blankSubstitution ^. re __Substitution

{-# INLINE traverseSubstitutionKeysWith #-}
traverseSubstitutionKeysWith :: (a -> a -> a) -> Traversal' (Substitution a) Variable
traverseSubstitutionKeysWith merge f (Substitution vars) = Substitution <$> (HML.fromListWith merge) <$> traverseOf (each . _1) f (HML.toList vars)

instance (Semigroup v) => HasVariables (Substitution v) where
  {-# INLINE variables #-}
  variables = traverseSubstitutionKeysWith (<>)

newtype SubstitutionLattice v = SubstitutionLattice {_underlyingSubstitution' :: (Maybe (Substitution v))}
  deriving newtype (Eq, Ord, Hashable, Pretty, Renderable)
  deriving stock (Generic, Show, Functor, Foldable, Traversable)
makeLenses ''SubstitutionLattice

--    (SubstitutionLattice Nothing) \/ (SubstitutionLattice (Just _)) = top
--    a \/ b = combineIfOverlapping a b

-- deriving via  (Maybe (Substitution v))      instance (Eq v) => Lattice (SubstitutionLattice v)

makeClassyPrisms ''SubstitutionLattice
-- deriving via  (Maybe (Substitution v))      instance (Eq v) => Monoid (SubstitutionLattice v)

-- newtype MutualSubstitutions a = MutualSubstitutions (SubstitutionLattice a)
--     deriving newtype (Eq, Ord, Hashable, Pretty, Renderable)
--     deriving stock (Generic, Show, Functor, Foldable, Traversable)
-- instance (Eq v) => Semigroup (MutualSubstitutions v) where
--     (MutualSubstitutions a) <> (MutualSubstitutions b) = MutualSubstitutions (mutualIfOverlapping a b)


                                                         
instance (Eq v) => Lattice (SubstitutionLattice v) where
  a \/ b | a == trivialSubstitution = trivialSubstitution
         | b == trivialSubstitution = trivialSubstitution
  (SubstitutionLattice Nothing) \/ (SubstitutionLattice Nothing) = incompatibleSubstitution
  a@(SubstitutionLattice (Just _)) \/ (SubstitutionLattice Nothing) = a
  (SubstitutionLattice Nothing) \/ b@(SubstitutionLattice (Just _)) = b
  (SubstitutionLattice (Just a)) \/ (SubstitutionLattice (Just b)) = SubstitutionLattice ((mutualIfOverlapping a b))

  a /\ b | a == incompatibleSubstitution = incompatibleSubstitution
         | b == incompatibleSubstitution = incompatibleSubstitution
  (SubstitutionLattice (Just a)) /\ (SubstitutionLattice (Just b))
    | substitutionsOverlap a b = SubstitutionLattice (combineIfOverlapping a b)
    | otherwise = incompatibleSubstitution
  a /\ (SubstitutionLattice Nothing) = a
  (SubstitutionLattice Nothing) /\ b = b
                 
--instance (Eq v) => BoundedJoinSemiLattice (SubstitutionLattice v) where
--  bottom = incompatibleSubstitution

instance (Eq v) => BoundedMeetSemiLattice (SubstitutionLattice v) where
  top =  trivialSubstitution


incompatibleSubstitution :: AsSubstitutionLattice k v => k
incompatibleSubstitution =  (SubstitutionLattice invalidSubstitution)^. re __SubstitutionLattice
deriving via (Meet (SubstitutionLattice v)) instance (Eq v) => Semigroup (SubstitutionLattice v)

deriving via (Meet (SubstitutionLattice v)) instance (Eq v) => Monoid (SubstitutionLattice v)


{-# INLINE mergeSubstitutions #-}
{-# SCC mergeSubstitutions #-}
-- | Combine a bunch of substitutions into one big one. If any of them are invalid, the resulting substitution is invalid.
mergeSubstitutions :: forall v t. (HasCallStack {-MonadPlus m,-}, Eq v, Foldable t) => t (SubstitutionLattice v) -> SubstitutionLattice v
mergeSubstitutions = meets --do -- meets --joins t -- joins (t)

--   foldlMOf
--     (traversed {-. to (\a -> if a == empty then pure blankSubstitution else a)-}) -- (\a b -> (pure a) \/ (pure b)) emptySubstitution t --
--     combineIfOverlapping
--     emptySubstitution
--     t -- MAYBE WILL SHORT CIRCUIT ON NOTHING?
-- guard (has __Substitution res)
-- pure (res)
type instance Index (SubstitutionLattice v) = Variable

type instance IxValue (SubstitutionLattice v) = v

instance Ixed (SubstitutionLattice v) where
  {-# INLINE ix #-}
  ix k = coerced . _Just . ix @(HashMap Variable v) k

instance At (SubstitutionLattice v) where
  {-# INLINE at #-}
  -- at ::
  --   Index VariableRenaming ->
  --   Lens' VariableRenaming (Maybe (IxValue VariableRenaming ))
  at k =
    lens
      ( \s -> case s ^? __Substitution . at k . _Just of
          Just a -> Just a
          Nothing -> Nothing
      )
      ( \s v -> case s ^? __Substitution of
          Nothing -> SubstitutionLattice Nothing
          Just s' -> (s' & at k .~ v) ^. re __Substitution
      )

{-# INLINE actualSubstitution #-}
actualSubstitution :: Prism (SubstitutionLattice s) (SubstitutionLattice t) (Substitution s) (Substitution t)
actualSubstitution = underlyingSubstitution' . _Just

instance AsSubstitution (SubstitutionLattice v) v where
  {-# INLINE __Substitution #-}
  __Substitution = actualSubstitution

instance (Semigroup v) => HasVariables (SubstitutionLattice v) where
  {-# INLINE variables #-}
  variables = actualSubstitution . variables

newtype VariableRenaming = VariableRenaming {_underlyingSubstitution'' :: SubstitutionLattice Variable}
  deriving newtype (Eq, Ord, Hashable, Pretty, Renderable, Lattice, {- BoundedJoinSemiLattice,-} BoundedMeetSemiLattice)
  deriving stock (Generic, Show)

makeLenses ''VariableRenaming

instance AsSubstitution (VariableRenaming) Variable where
  {-# INLINE __Substitution #-}
  __Substitution = underlyingSubstitution'' . __Substitution

-- instance HasVariables  (VariableRenaming) where
--   {-# INLINE variables #-}
--   variables = __Substitution . variables

type instance Index VariableRenaming = Variable

type instance IxValue VariableRenaming = Variable

instance Ixed (VariableRenaming) where
  {-# INLINE ix #-}
  ix k = coerced . _Just . ix @(HashMap Variable Variable) k

instance At VariableRenaming where
  {-# INLINE at #-}
  -- at ::
  --   Index VariableRenaming ->
  --   Lens' VariableRenaming (Maybe (IxValue VariableRenaming ))
  at k =
    lens
      ( \s -> case s ^? __Substitution . at k . _Just of
          Just a -> Just a
          Nothing -> Just k
      )
      ( \s v -> case s ^? __Substitution of
          Nothing -> VariableRenaming (SubstitutionLattice Nothing)
          Just s' -> (s' & at k .~ v) ^. re __Substitution
      )
instance AsSubstitutionLattice VariableRenaming Variable where
    {-# INLINE __SubstitutionLattice #-}
    __SubstitutionLattice = underlyingSubstitution''
{-# INLINE latticeToVariableRenaming #-}
latticeToVariableRenaming :: SubstitutionLattice Variable -> VariableRenaming
latticeToVariableRenaming = coerce

variableRenamingToSubstitutionLattice :: VariableRenaming -> SubstitutionLattice Variable
variableRenamingToSubstitutionLattice = coerce

{-# INLINE renameVariables' #-}
renameVariables' :: (HasVariables term) => term -> Substitution Variable -> term
renameVariables' term sub = term & variables %~ \v -> HMS.findWithDefault v v (sub ^. varMap)

{-# INLINE renameVariables #-}
renameVariables :: (Alternative f, AsSubstitution s Variable, HasVariables term) => term -> s -> f term
renameVariables term sub'
  | Just sub <- sub' ^? __Substitution = pure (term & variables %~ \v -> HMS.findWithDefault v v (sub ^. varMap))
  | otherwise = empty

{-# INLINE composeSubstitution #-}
composeSubstitution ::  Substitution v -> Substitution Variable -> Substitution v
composeSubstitution (Substitution a) (Substitution v) = Substitution (a `compose'` v)
  where
    compose' bc ab = HMS.union (HMS.compose bc ab) bc

{-# INLINEABLE extractSubstitutionFromVariableMapping #-}
extractSubstitutionFromVariableMapping :: v -> Sem ((VariableMapping v) : r) a -> Sem r (Substitution v, a)
extractSubstitutionFromVariableMapping defaultValue = extractSubstitutionFromVariableMappingBy (const . pure $ defaultValue)

{-# INLINEABLE extractSubstitutionFromVariableMappingBy #-}
extractSubstitutionFromVariableMappingBy :: forall r v a. (Variable -> Sem r v) -> Sem ((VariableMapping v) : r) a -> Sem r (Substitution v, a)
extractSubstitutionFromVariableMappingBy compute sem = do
  (kvs, res) <- runAtomicStateViaState HMS.empty $ runVariableMappingBy' (raise . compute) (raiseUnder sem)
  pure (Substitution kvs, res)
