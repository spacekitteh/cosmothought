{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}
module Core.Variable.AlphaEquivalence where

import Math.ParetoFront hiding (singleton)
import Data.Tuple
import Control.Applicative
import Core.Utils.MetaTypes
import Data.Sequence (Seq)
import Core.Variable
import Core.Variable.Substitution

import Control.Lens

import Core.Identifiers.IdentifierType
import Core.Identifiers.IDGen
import Data.Function

import Data.HashSet (HashSet)
import qualified Data.HashSet as HS

import Data.Hashable


import Data.Maybe


import Data.Traversable


import Polysemy


import Prelude
import qualified Data.Text as T

-- TODO OPTIMISE
{-# INLINEABLE freshenVariableIDs #-}
freshenVariableIDs :: (HasVariables object, Members '[IDGen] r) => object -> Sem r object
freshenVariableIDs !object = do
  let vars = object ^. partsOf variables
  varContextID <- newGlobalIdentifier
  modifiedVars <- forM vars \var -> do
    ident <- newLocalIdentifierInContext varContextID
    pure (var & variableID .~ ident  )
  let varmap = makeVariablesWithSameNameReferToSameID (HS.fromList modifiedVars)
  pure $! (renameVariables' object (constructSubstitution (varmap^@..ifolded)))
       
{-# INLINEABLE generaliseVariables #-}
generaliseVariables :: (HasVariables object, Members '[IDGen] r) => object -> Sem r object
generaliseVariables !object = do
  let vars = object ^. partsOf variables
  varContextID <- newGlobalIdentifier
  modifiedVars <- iforM vars \i var -> do
    ident <- newLocalIdentifierInContext varContextID
    pure (var & variableID .~ ident & variableName %~ \n -> textToSymbolName (symbolNameToText n <> "/" <> (T.pack $ show i) ))
  pure $! object & partsOf variables .~ modifiedVars
--  let varmap = makeVariablesWithSameNameReferToSameID (HS.fromList modifiedVars)
--  pure $! (renameVariables object (Substitution varmap))
-- TODO OPTIMISE
{-# INLINEABLE makeVariablesTermLocal #-}
makeVariablesTermLocal :: (HasVariables object) => object -> object
makeVariablesTermLocal !object = (renameVariables' modifiedObj (constructSubstitution (varmap^@..ifolded)))
  where
    vars = object ^. partsOf variables
    varContextID = termLocalVariableBaseIdentifier
    modifiedVars =
      (flip imap)
        vars
        ( \pos var ->
            let ident = ContextualIdentifier (fromIntegral pos) varContextID
             in (var & variableID .~ ident)
        )
    modifiedObj = object & partsOf variables .~ modifiedVars
    varmap = makeVariablesWithSameNameReferToSameID (HS.fromList modifiedVars)

newtype ByEach c o = ByEach' {_unEach :: c}

instance {-# OVERLAPS #-} (Each c c o o, HasVariables o) => HasVariables (ByEach c o) where
  {-# INLINE variables #-}
  variables = iso (\(ByEach' a) -> a) (\a -> ByEach' a) . each . variables

{-# INLINEABLE makeVariablesTermLocalInEach #-}
makeVariablesTermLocalInEach :: forall container object. (Each container container object object, HasVariables object) => container -> container
makeVariablesTermLocalInEach container = res
  where
    (ByEach' res) = makeVariablesTermLocal @(ByEach container object) (ByEach' container)




--------------------------------------------------------------------------------
-- α-equivalence
--------------------------------------------------------------------------------
class AlphaEquivalence term where
  isAlphaEquivalent ::
    -- | The term we want to  change
    term ->
    -- | The term we want to change to
    term ->
    (VariableRenaming, VariableRenaming)

instance AlphaEquivalence Variable where
  {-# INLINE isAlphaEquivalent #-}
  isAlphaEquivalent !a !b = (singleton a b, singleton b a)

instance AlphaEquivalence (HashSet Variable) where
  {-# INLINE isAlphaEquivalent #-}
  isAlphaEquivalent a b
    | HS.size a == HS.size b =
         (constructSubstitution (zip (HS.toList a) (HS.toList b)), constructSubstitution (zip (HS.toList b) (HS.toList a)))
  isAlphaEquivalent _ _ = (incompatibleSubstitution,incompatibleSubstitution)

-- instance Alpha
{-# INLINE findSomeVarMapping' #-}
findSomeVarMapping' ::Alternative f =>  HasVariables term => term -> term -> f (VariableRenaming, [(Variable, Variable)])
findSomeVarMapping' a b =  let vars = zip (a ^. partsOf variables) (b ^. partsOf variables) in  pure $! (constructSubstitution (vars ^.. folded . filtered (\(x,y) -> x /= y)), vars)

{-# INLINE findSomeVarMapping #-}
findSomeVarMapping ::Alternative f =>  HasVariables term => term -> term -> f (VariableRenaming)
findSomeVarMapping a b = fmap fst (findSomeVarMapping' a b)

-- where
--   theMap = makeVariablesWithSameNameReferToSameID (HS.union (setOf variables a) (setOf variables b))

newtype EqualityModuloAlpha term = EqualityModuloAlpha term deriving newtype (Show)

{-# INLINEABLE subsumesVariables #-}
-- | Is term `a` more general than term `b`, in the sense of variables? If so, provide the mapping from a to b and b to a.
subsumesVariables :: (Alternative f, Eq term, HasVariables term) => term -> term -> f (VariableRenaming, VariableRenaming)
subsumesVariables a b | Just ((sub, keys)) <- findSomeVarMapping' a b, sub /= incompatibleSubstitution, (renameVariables a sub) == Just b = pure (sub, constructSubstitution (fmap swap keys))
subsumesVariables _ _ = empty

                          
instance (Eq term, HasVariables term) => AlphaEquivalence (EqualityModuloAlpha term) where
  {-# INLINE isAlphaEquivalent #-}
  isAlphaEquivalent (EqualityModuloAlpha a) (EqualityModuloAlpha b)
    -- \| renamed <- a & partsOf variables .~ (b ^. partsOf variables) = renamed == b
    | Just (subAToB, _) <- subsumesVariables a b, Just (subBToA, _) <- subsumesVariables b a, subAToB /= latticeToVariableRenaming incompatibleSubstitution, subBToA /= latticeToVariableRenaming incompatibleSubstitution
                           --res <- mergeSubstitutions [ variableRenamingToSubstitutionLattice subAToB, variableRenamingToSubstitutionLattice subBToA]-- , renamedA <- renameVariables a subAToB, renamedB <-
                        = (subAToB,  subBToA) --  res but actual variables will be from a disjoint set, right?
  isAlphaEquivalent _ _ = (incompatibleSubstitution,incompatibleSubstitution)

infix 4 =@=

{-# INLINE (=@=) #-}
(=@=) :: (AlphaEquivalence term) => term -> term -> Bool
a =@= b = let (suba, subb) = isAlphaEquivalent (a) (b) in has __Substitution suba && has __Substitution subb

instance (Eq term, AlphaEquivalence term) => Eq (EqualityModuloAlpha term) where
  {-# INLINE (==) #-}
  (EqualityModuloAlpha a) == (EqualityModuloAlpha b) = a =@= b

instance (Eq term, HasVariables term, AlphaEquivalence term, Hashable term) => Hashable (EqualityModuloAlpha term) where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt (EqualityModuloAlpha term) = hashWithSalt salt (makeVariablesTermLocal term)

infix 4 =@=?

{-# INLINE (=@=?) #-}
(=@=?) :: (Eq term, HasVariables term) => term -> term -> (VariableRenaming, VariableRenaming)
a =@=? b = isAlphaEquivalent (EqualityModuloAlpha a) (EqualityModuloAlpha b)



{-# INLINE subsumeDominatedModuloAlpha #-}
subsumeDominatedModuloAlpha :: (AlphaEquivalence a, Foldable f, Debatable a) => f a -> Seq a
subsumeDominatedModuloAlpha = subsumeDominatedWith (=@=)


-- data VariableGeneralisation = VariableGeneralisation {_
