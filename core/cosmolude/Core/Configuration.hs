{-# LANGUAGE NamedFieldPuns #-}
 module Core.Configuration (HasConfigurationData(..), ConfigurationData, Configuration, newConfigurationData, resetConfigurationData, configurationToVariables, embedPSCMConfiguration, embedReteConfiguration, embedLogConfiguration,
                                            module CPC,
                                            module CRC,
                                            module CUL
                           ) where
import Control.Concurrent.STM
import Control.Lens
import PSCM.Configuration
import Rete.Configuration
import PSCM.Configuration as CPC (PSCMConfiguration(..), ActivityOnRuleRemoval(..)) 
import Rete.Configuration as CRC (ReteConfiguration(..)) 
import Polysemy
import Polysemy.State.Keyed
import Prelude
import Core.Utils.Logging
import Core.Utils.Logging as CUL (LogConfiguration(..))



data Configuration a where
  PSCMConfig :: PSCMConfiguration a -> Configuration a
  LogConfig :: LogConfiguration a -> Configuration a
  -- TODO Refactor this to an "ExtraConfig" 
  ReteConfig :: ReteConfiguration a -> Configuration a

data ConfigurationData = ConfigurationData
                       {
                         pscmConfig :: PSCMConfigurationData,
                         logConfig :: LogConfigurationData,
                         reteConfig :: ReteConfigurationData
                       }
instance HasLogConfigurationData ConfigurationData where
    {-# INLINE logConfigurationData #-}
    logConfigurationData = lens (\(ConfigurationData{logConfig}) -> logConfig) (\config logconfig -> config {logConfig = logconfig})  
class HasConfigurationData ty where
    configurationData :: Lens' ty ConfigurationData
    
{-# INLINE newConfigurationData #-}                       
newConfigurationData :: STM ConfigurationData
newConfigurationData = ConfigurationData <$> newPSCMConfigurationData <*> newLogConfigurationData <*> newReteConfigurationData

resetConfigurationData :: ConfigurationData -> STM ()
resetConfigurationData (ConfigurationData {..}) = do
  resetPSCMConfigurationData pscmConfig
  resetLogConfigurationData logConfig
  resetReteConfigurationData reteConfig
                       
                
{-# INLINE embedReteConfiguration #-}
embedReteConfiguration :: Member (KeyedState Configuration) r => InterpreterFor (KeyedState ReteConfiguration) r
embedReteConfiguration = rename ReteConfig

{-# INLINE embedPSCMConfiguration #-}
embedPSCMConfiguration :: Member (KeyedState Configuration) r => InterpreterFor (KeyedState PSCMConfiguration) r
embedPSCMConfiguration = rename PSCMConfig

{-# INLINE embedLogConfiguration #-}
embedLogConfiguration :: Member (KeyedState Configuration) r => InterpreterFor (KeyedState LogConfiguration) r
embedLogConfiguration = rename LogConfig

{-# INLINE configurationToVariables #-}
configurationToVariables :: HasConfigurationData agent => agent -> Configuration a -> TVar a
configurationToVariables agent (PSCMConfig pc) | (ConfigurationData {pscmConfig}) <- agent^.configurationData  = pscmConfigurationToVariables pscmConfig pc
configurationToVariables agent (LogConfig lc)  | (ConfigurationData {logConfig}) <- agent^.configurationData = logConfigurationToVariables logConfig lc
configurationToVariables agent (ReteConfig rc) | (ConfigurationData {reteConfig})  <- agent^.configurationData = reteConfigurationToVariables reteConfig rc
