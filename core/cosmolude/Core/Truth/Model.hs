module Core.Truth.Model where
import Control.Applicative
import Algebra.Heyting
import Core.Variable
import Algebra.Lattice    
import Control.Lens
import Core.Truth
import Polysemy
import Polysemy.Transaction
import Algebra.Heyting.Free
import Control.Concurrent.STM
import qualified StmContainers.Map as STMM    

data TruthModel = TruthModel {_truthMapping :: STMM.Map Variable ConcreteTruth}
makeClassy ''TruthModel

{-# INLINE newTruthModel #-}           
newTruthModel :: STM TruthModel
newTruthModel = TruthModel <$> STMM.new

resetTruthModel :: TruthModel -> STM ()
resetTruthModel (TruthModel tm) = STMM.reset tm

           
{-# INLINE computeTruth #-}
-- TODO FIXME convert to ConcreteTruth and use Numeric.Log.Sum?
computeTruth :: (Member (VariableMapping concreteTruth) r, Heyting concreteTruth) => WMETruth -> Sem r concreteTruth
computeTruth (WMETruth f) = do
  asConcrete <- traverse getVariableValue f
  pure (retractFree asConcrete)

{-# INLINE runVariableMapping #-}
runVariableMapping :: (Member Transactional r, HasTruthModel container) => container -> InterpreterFor (VariableMapping ConcreteTruth) r
runVariableMapping container = runVariableMappingWith top (container^.truthMapping)
    
