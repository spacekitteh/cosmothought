module Core.Error where
import Control.Category

-- import qualified OpenTelemetry.Trace.Core as OT
-- import Polysemy.Transaction
-- import Data.Foldable
-- import Control.Exception (SomeException)
-- import Polysemy.OpenTelemetry
-- import Polysemy
-- import System.Directory
-- import Data.UUID.V4
-- import System.IO    
import Control.Lens
import Control.Monad
-- import Data.UUID
-- import System.FilePath

import Data.Bifunctor
import Data.Function (($))
import Data.Text (Text)
import Error.Diagnose hiding (defaultStyle)
import qualified Prettyprinter as PP
import Prettyprinter.Render.TerminalModified
import Text.Megaparsec hiding (State, many, match, noneOf, parse, parseMaybe, some, tokens)
import qualified Text.Megaparsec as MP
import qualified Text.URI as U ( URI, render, )
import Prelude ( Num (..),  fromIntegral)--, Show(..), Bool(..), pure)
import qualified Data.String as DS
import Core.Utils.Rendering.CoreDoc
import qualified Data.Set as Set
type DiagnosticMessageType = Text
type ErrorCode = U.URI
    
class ToErrorCode err where
    getErrorCode :: err -> ErrorCode

class ErrorCodeRendering msg where
    renderErrorCode :: ErrorCode -> msg

instance ErrorCodeRendering DiagnosticMessageType where
    renderErrorCode e = renderStrict (PP.layoutPretty PP.defaultLayoutOptions ( PP.annotate (color Red) $ PP.unAnnotate $ render  $ U.render e))

class HasErrorPosition e where
    errorPosition :: e -> Position

instance HasErrorPosition MP.SourcePos where
    errorPosition MP.SourcePos {..} =
      let start = bimap (fromIntegral . MP.unPos) (fromIntegral . MP.unPos) (sourceLine, sourceColumn)
          end = second (+ 1) start
       in Position start end sourceName

class HasMarker e msg where
    errorMarker :: e -> Marker msg

class GetErrorContext e ctx | e -> ctx where
    {-# MINIMAL defaultContext #-}
    hintContext :: e -> ctx
    hintContext _ = defaultContext @e
    defaultContext :: ctx
class HasHintsWhen ctx e msg where
    {-# MINIMAL hintsWhenWithDefault | hintsWhen #-}
    hintsWhenWithDefault :: [Note msg] -> ctx -> e -> [Note msg]
    hintsWhenWithDefault _ = hintsWhen
    hintsWhen :: ctx -> e -> [Note msg]
    hintsWhen = hintsWhenWithDefault []



                
hintsInContext :: (GetErrorContext e ctx, HasHintsWhen ctx e msg) => e -> [Note msg]
hintsInContext e = hintsWhen (hintContext e) e

hintsInContextWithDefault :: (GetErrorContext e ctx, HasHintsWhen ctx e msg) => [Note msg] -> e -> [Note msg]
hintsInContextWithDefault def e = hintsWhenWithDefault def (hintContext e) e

instance (DS.IsString msg, GetErrorContext e ctx, HasHintsWhen ctx e msg) => HasHintsWhen ctx (MP.ParseError s e) msg where
    hintsWhenWithDefault trivialHints _ (MP.TrivialError {}) = trivialHints
    hintsWhenWithDefault def _ (MP.FancyError _ errs) =
      Set.toList errs >>= \case
        MP.ErrorCustom e -> hintsInContextWithDefault def e
        MP.ErrorFail s -> (Note (DS.fromString s)) : def
        MP.ErrorIndentation {} -> (Note "ErrorIndentation TODO implement") : def                   


-- data CrashAnnotation = FileAnnotation Text FilePath
--                      | TelemetryAnnotations [(Text, OT.Attribute)]
--                      | CrashCode (forall e. (Renderable e, ToErrorCode e) => e)

-- data CrashContext = CrashContext UUID FilePath [CrashAnnotation] (Maybe SomeException)

-- createCrashContext :: (Members '[OpenTelemetryContext, OpenTelemetrySpan, Embed IO] r) => [CrashContext -> Sem r CrashAnnotation] -> Sem r CrashContext
-- createCrashContext annotators = do
--   ident <- embed nextRandom
--   cacheDir <- embed $ getXdgDirectory XdgCache ("cosmothought" </> "crash" </> (show ident))
--   embed $ createDirectoryIfMissing True cacheDir
--   foldM (\ctx@(CrashContext uuid path anns) action -> do
--            ann <- action ctx
--            pure (CrashContext uuid path (ann:anns) Nothing)) (CrashContext ident cacheDir []) annotators
                                                          

-- data CrashHandling m a where
--     GetOrCreateCrashContext :: CrashHandling m CrashContext
--     AnnotateCrashContext :: CrashAnnotation -> CrashHandling m ()
--     CrashInfoDirectory :: CrashHandling m AbsoluteFilePath
--     SetException :: SomeException -> CrashHandling m ()

                    
-- {-# INLINE runCrashHandling #-}
-- runCrashHandling :: (Members '[Transactional, OpenTelemetryContext, OpenTelemetrySpan, Resource, Final IO] r) => InterpreterFor CrashHandling r
-- runCrashHandling sem = do
--   ctxV <- newT Nothing
--   res <- interpret (\case
--           GetOrCreateCrashContext -> ensureContextIsInitialised ctxV
--           SetException e -> do
--             (CrashContext uuid path anns _) <- ensureContextIsInitialised ctxV
--             writeT (Just (CrashContext uuid path anns (Just e))) ctxV
--           AnnotateCrashContext ann -> do
--             (CrashContext uuid path anns exc) <- ensureContextIsInitialised ctxV
--             writeT (Just (CrashContext uuid path (ann:anns) exc)) ctxV
--              CrashInfoDirectory -> do
--                (CrashContext _ path _) <- readT ensureContextIsInitiialised ctxV
--                pure path
--          ) sem
--   let excHandler = do
--         ctx' <- readT ctxV
--         case ctx' of
--           Nothing -> pure res
--           Just (CrashContext ident path anns exc) -> do
--             let initialAttrs = [("crash.id", fromString . show $ ident)]
--             attrs <- foldrM collectAndActOnAnnotations initialAttrs anns
--             case exc of
--               Nothing -> addAttributes
              




-- collectAndActOnAnnotations :: Members '[ OpenTelemetryContext, OpenTelemetrySpan] r => CrashAnnotation -> [(Text, OT.Attribute)] -> Sem r [(Text, OT.Attribute)]
-- collectAndActOnAnnotations (FileAnnotation attrName path) anns = pure ((attrName, T.pack . show $ path) : anns)
-- collectAndActOnAnnotations (TelemetryAnnotations anns) existing = pure (anns <> existing)
-- collectAndActOnAnnotations (CrashCode e) anns =  do
--     let asErrorCode = U.render (getErrorCode e)
--         msg = T.pack . show . render $ e
--     setSpanStatus (OT.Error msg)
--     pure (("crash.code", T.pack . show $ asErrorCode) : anns)
    
    
  
-- ensureContextIsInitialised :: Members '[Transactional, OpenTelemetryContext, OpenTelemetrySpan, Embed IO] r
--                               => TVar (Maybe CrashContext) -> Sem r CrashContext
-- ensureContextIsInitialised ctxV = do
--   ctx' <- readT ctxV
--   case ctx' of
--     Nothing -> do
--               ctx <- createCrashContext []
--               writeT (Just ctx) ctxV
--               pure ctx
--     Just ctx -> pure ctx
