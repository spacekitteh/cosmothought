{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}
module Core.Variable where
import Polysemy
import Polysemy.Transaction
import qualified StmContainers.Map as STMM
import Data.Data
import Control.Lens
import Polysemy.AtomicState

import Core.Identifiers.IdentifierType

import Data.Function

import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HMS
import Data.HashSet (HashSet)
import qualified Data.HashSet as HS

import Data.Hashable


import Language.Haskell.TH.Syntax



import Data.Tuple (swap)
import GHC.Generics (Generic)

import Prelude
import Core.Utils.MetaTypes
import Core.Truth
data Variable = AVariable {_variableID :: !Identifier, _variableName :: !SymbolName}
  deriving (Generic, Lift, Data)

instance Show Variable where
    show (AVariable i n) = "["++show n++"]</" ++ show i ++ ">"

makeClassy ''Variable
makeClassyPrisms ''Variable

type WMETruth = WMETruth' Variable

    
instance Each1 Variable Variable Variable Variable where
  {-# INLINE each1 #-}
  each1 f = f

{-# INLINEABLE makeVariablesWithSameNameReferToSameID #-}
-- TODO OPTIMISE: Use a union-find structure (like hEgg!)
makeVariablesWithSameNameReferToSameID :: HashSet Variable -> HashMap Variable Variable
makeVariablesWithSameNameReferToSameID input = HMS.compose mappingVarNameToVar mappingVarToVarName 
  where
    asVarVarNameList = fmap (\var -> (var, var ^. variableName)) . HS.toList $ input
    mappingVarToVarName = HMS.fromList asVarVarNameList
    mappingVarNameToVar = HMS.fromList $ fmap swap asVarVarNameList


instance HasIdentifier Variable where
  {-# INLINE identifier #-}
  identifier = variableID

deriving via ByIdentifier Variable instance Eq Variable

deriving instance Ord Variable

deriving via ByIdentifier Variable instance Hashable Variable

class HasVariables c where
  variables :: Traversal' c Variable
instance HasVariables Variable where
    variables = id

instance HasVariables Int where
    variables = ignored
                
instance {-# OVERLAPPABLE #-} (HasVariables a, Each c c a a) => HasVariables c where
  {-# INLINE variables #-}
  variables = each . variables
instance HasVariables [Variable] where
    {-# INLINE variables #-}
    variables = traversed

data VariableMapping concrete m a where
  MapVariableTo :: Variable -> concrete -> VariableMapping concrete m ()
  GetVariableValue :: Variable -> VariableMapping concrete m concrete
  MaybeGetVariableValue :: Variable -> VariableMapping concrete m (Maybe concrete)
makeSem ''VariableMapping

{-# INLINE runVariableMappingWith #-}
runVariableMappingWith :: Member Transactional r => concrete -> STMM.Map Variable concrete -> InterpreterFor (VariableMapping concrete) r
runVariableMappingWith defaultValue = runVariableMappingBy (const . pure  $  defaultValue)
        
{-# INLINE runVariableMappingBy #-}
runVariableMappingBy :: Member Transactional r => (Variable -> Sem r concrete) -> STMM.Map Variable concrete -> InterpreterFor (VariableMapping concrete) r
runVariableMappingBy computeValue existingMap = interpret \case
  MapVariableTo !var !val -> insertInMap val var existingMap
  MaybeGetVariableValue var -> lookupMap var existingMap    
  GetVariableValue !var -> do
    res <- lookupMap var existingMap
    case res of
      Nothing -> computeValue var
      Just !val -> pure val


{-# INLINE runVariableMappingBy' #-}
runVariableMappingBy' :: Member (AtomicState (HashMap Variable concrete)) r => (Variable -> Sem r concrete) -> Sem ((VariableMapping concrete):r) a -> Sem r a
runVariableMappingBy' computeValue  = interpret \case
  MapVariableTo !var !val -> atomicModify (HMS.insert var val)
  MaybeGetVariableValue var -> atomicGets (HMS.lookup var)
  GetVariableValue !var -> do
    res <- atomicGets (HMS.lookup var)
    case res of
      Nothing -> computeValue var
      Just !val -> pure val
                   
