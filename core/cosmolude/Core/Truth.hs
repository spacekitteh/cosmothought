{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# OPTIONS_GHC -Wwarn=inline-rule-shadowing#-}
module Core.Truth where
import Data.Coerce

import Data.Foldable
import Algebra.PartialOrd
import Algebra.Heyting
import Algebra.Heyting.Free

--import Core.Variable
import Algebra.Lattice
import Prelude
import GHC.Generics (Generic)
import Numeric.Log
import Data.Hashable
import Algebra.Lattice.Ordered
import Prettyprinter
import Data.Data
import Control.Lens
import Data.Refined
import Data.The
import qualified Data.Set as Set

    

newtype WMETruth' a = WMETruth {_unWMETruth :: Free a}
  deriving newtype (Eq,  Show, Lattice, BoundedMeetSemiLattice, BoundedJoinSemiLattice, Heyting, PartialOrd)
  deriving stock (Generic, Data)

instance (Show a) => Pretty (WMETruth' a) where
    pretty = viaShow
instance (Ord a, Hashable a) => Hashable (WMETruth' a) where
    hashWithSalt salt (WMETruth x) = hashWithSalt (78987654 + salt) (lowerFree (Set.singleton . hashWithSalt salt) x) 



class HasAccumulatedTruth obj a where
    accumulatedTruth :: Getter obj (WMETruth' a)
instance HasAccumulatedTruth (WMETruth' a) a where
    {-# INLINE accumulatedTruth #-}
    accumulatedTruth = to id
instance (HasAccumulatedTruth obj a) => HasAccumulatedTruth (obj ? anything) a where
    {-# INLINE accumulatedTruth #-}
    accumulatedTruth = to the . accumulatedTruth
newtype ConcreteTruth = ConcreteTruth {_unConcreteTruth :: Log Double}
  deriving newtype (Eq, Show, Ord, Real, Num, Floating, Fractional, Hashable)
  deriving stock (Generic, Data)
      
instance Pretty ConcreteTruth where
    pretty = viaShow
           
instance PartialOrd ConcreteTruth where
  {-# INLINE leq #-}
  leq = (<=)
instance Lattice ConcreteTruth where
  {-# INLINE (/\) #-}
  (/\) = (*)
  {-# INLINE (\/) #-}
  (\/) = (+)

instance Bounded ConcreteTruth where
  minBound = bottom
  maxBound = top
instance BoundedJoinSemiLattice ConcreteTruth where
  bottom = ConcreteTruth (Exp (-1/0))
instance BoundedMeetSemiLattice ConcreteTruth where
  top = ConcreteTruth (Exp 0)
deriving via Ordered (ConcreteTruth) instance Heyting  (ConcreteTruth)
  

{-# RULES "meetsLogDomain" meets =  ConcreteTruth . Numeric.Log.sum . fmap coerce . toList  #-}



