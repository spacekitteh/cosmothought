module Core.Rule.Optimisation (simplifyRule, optimiseConditions, simplifyConditionClassicalLogic', RuleSemanticError(..), constraints) where
import Algebra.Lattice
import Polysemy.Error
import Polysemy.Reader (Reader)    
import Core.Rule.Construction.Analysis    
import Control.Applicative
import Core.CoreTypes
import Core.Patterns.PatternSyntax
import qualified Data.Sequence as Seq    
import Core.Rule
import Core.Rule.Condition
import Core.Rule.Parts
import Cosmolude hiding (As)
import Data.Functor.Apply
import qualified Data.HashSet as HS
import Data.HashSet.Lens
import qualified Data.Histogram as Histo
import qualified Data.List as DL
import Data.List.NonEmpty (NonEmpty (..), nonEmpty)
import qualified Data.Text as T
import GHC.Exts (SPEC (..))
import qualified OpenTelemetry.Trace as OT
import Polysemy
-- import Polysemy (run)
import Polysemy.Fail (failToNonDet)
import Polysemy.NonDet


                            
conditionSimplification' :: Alternative f => PatternCondition -> f PatternCondition
conditionSimplification' (APatternVariable var (Just (AComparisonTest tests))) | tests == Seq.empty = pure (APatternVariable var Nothing)
conditionSimplification' (APatternVariable var (Just (AComparisonTest tests))) = do
  coherent <- coherentTests tests
  pure (APatternVariable var (Just (AComparisonTest coherent)))
conditionSimplification' _ = empty

constantFolding' :: (Alternative f, Members '[VariableMapping Value] r) => PatternCondition -> Sem r (f PatternCondition) -- TODO Add checking for inconsistent constraints (e.g. LT 4, GT 4)
constantFolding' (APatternVariable var (Just (AComparisonTest tests))) = runNonDet . failToNonDet $ do
  coherent <- coherentTests tests
  Just constant <- pure $ firstOf (folded . _EqualTo . comparisonValue) coherent
  mapVariableTo (var ^. variable) constant
  pure $! (valueToPatternCondition constant)
constantFolding' (APatternVariable var _) = do
  val <- maybeGetVariableValue (var ^. variable)
  case val of
    Nothing -> pure empty
    Just v -> pure $ pure (valueToPatternCondition v)
constantFolding' _ = pure empty

liftSimplificationToPattern :: (Monad m) => (PatternCondition -> m (Maybe PatternCondition)) -> WMEPattern -> m (Maybe WMEPattern)
liftSimplificationToPattern op (WMEPattern o a v pref) = do
  o' <- (fromMaybe o) <$> (op o)
  a' <- (fromMaybe a) <$> (op a)
  v' <- (fromMaybe v) <$> (op v)
  if o /= o' || a /= a' || v /= v' then pure (pure (WMEPattern o' a' v' pref)) else pure empty

conditionConstantFolding :: Members '[VariableMapping Value] r => RuleCondition -> Sem r (Maybe RuleCondition)
conditionConstantFolding (PositiveCondition pat) = (fmap PositiveCondition) <$> (liftSimplificationToPattern constantFolding' pat)
conditionConstantFolding _ = pure Nothing

conditionSimplification :: Monad m => RuleCondition -> m (Maybe RuleCondition)
conditionSimplification (PositiveCondition pat) = (fmap PositiveCondition) <$> (liftSimplificationToPattern (pure . conditionSimplification') pat)
conditionSimplification _ = pure empty

doubleNegationElimination :: Alternative f => RuleCondition -> f RuleCondition
doubleNegationElimination (NegatedConjunction ((NegatedConjunction (!cond :| [])) :| [])) = pure cond
doubleNegationElimination _ = empty

classicalSimplifications :: (Monad m) => [RuleCondition -> m (Maybe RuleCondition)] -> RuleCondition -> m (Maybe RuleCondition)
classicalSimplifications extras a = do
  let dng = doubleNegationElimination a
      subsumed = subsumption a
  smallerConditions <- conditionSimplification a
  rest <- traverse ($ a) extras
  pure (asum (dng : subsumed : smallerConditions : rest))

subsumption :: Alternative f => RuleCondition -> f RuleCondition
subsumption (NegatedConjunction (a :| [])) = do
  subsumed <- subsumption a
  pure (NegatedConjunction (subsumed :| []))
subsumption (NegatedConjunction (a :| as)) | (b :< bs) <- subsumeDominated (a :| as), DL.length as /= DL.length bs = pure (NegatedConjunction (b :| (bs ^.. folded)))
subsumption _ = empty

simplifyConditionClassicalLogic' :: RuleCondition -> RuleCondition
simplifyConditionClassicalLogic' = run . rewriteM (classicalSimplifications [])

simplifyConditionClassicalLogic :: Members '[VariableMapping Value] r => RuleCondition -> Sem r RuleCondition
simplifyConditionClassicalLogic = rewriteM (classicalSimplifications [conditionConstantFolding])

subsumeDominatedModuloAlphaPost :: (Foldable f, Members '[VariableMapping Variable] r) => f RuleCondition -> Sem r (Seq RuleCondition)
subsumeDominatedModuloAlphaPost = do
  subsumeDominatedWithM
    ( \a b -> do
        let result =
              (a & confusing (immediateConditionPatterns . each . patVariable) %~ (\v -> PlainPatternVariable (v ^. variable)))
                == (b & confusing (immediateConditionPatterns . each . patVariable) %~ (\v -> PlainPatternVariable (v ^. variable)))
        if result
          then do
            let vars = DL.zip (a ^.. confusing (immediateConditionPatterns . each . patVariable . variable)) (b ^.. confusing (immediateConditionPatterns . each . patVariable . variable))
            forM_ vars \(k, v) -> do
              mapVariableTo k v
            pure True
          else do
            pure False
    )

{-# INLINEABLE optimiseConditions #-}
optimiseConditions :: (HasTracer agent, Members '[VariableMapping Variable, NonDet, Reader agent, Log, Resource, OpenTelemetrySpan, OpenTelemetryContext] r) => OriginalRuleConditions -> Sem r OptimisedRuleConditions
optimiseConditions orig = push "optimiseConditions" do
  addAttributeWithRaw "rule.conditions.original" orig
  if has (patternObject . _APatternVariable . _1 . _StatePatternVariable) (orig ^. originalStateTestPattern)
    then do
      subsumed <- subsumeDominatedModuloAlphaPost ((PositiveCondition (orig ^. originalStateTestPattern)) :< (orig ^. originalRemainingConditions))
      let result = (OptimisedRuleConditions (subsumed ^??! _head . immediateConditionPatterns) (subsumed ^.. _tail . folded) emptySubstitution) -- todo figure out the substitution?
      addAttributeWithRaw "rule.conditions.optimised" result
      pure result
    else mzero

  
         
simplifyRuleConditions :: (HasRule rule, Members '[VariableMapping Value] r) => rule -> Sem r rule
simplifyRuleConditions r = do

  --TODO nub conditions (by subsumption?)       
  
  traverseOf (confusing (rule . ruleLHS . each)) simplifyConditionClassicalLogic r

-- simplifyRuleConditionsRewrite :: Members '[VariableMapping Value] r => Rule -> Sem r (Maybe Rule)
-- simplifyRuleConditionsRewrite r = do
--   simplified <- simplifyRuleConditions r
--   if (WithoutMeta simplified  == WithoutMeta r) then pure Nothing else pure (Just simplified)

setOfSingleOccurances :: (Hashable a, Ord a) => [a] -> HashSet a
setOfSingleOccurances as = as & Histo.fromList & Histo.toList & setOf (folded . filtered (\c -> c ^. _2 == 1) . _1)

{-# INLINE unusedVariableFilter #-}
unusedVariableFilter :: (HasRule rule, Applicative f, Apply f) => (Bool -> Bool) -> LensLike f rule rule Variable Variable
unusedVariableFilter f = confusing (rule . ruleLHS . each) . conditionPatterns . confusing
                         (each
                          . filtered (\pv -> f (hasn't
                                                 (confusing (_APatternVariable
                                                  . _2
                                                  . _Just
                                                  . orderingTest
                                                  . traversed
                                                  . comparingTo
                                                  . comparisonValue
                                                 )) pv
                                                &&
                                                has
                                                 (_APatternVariable
                                                  . _1
                                                  . _PlainPatternVariable
                                                 ) pv
                                               )
                                     )
                          . variables)



  

{-

TODO FIXME: What about unbound variables with constraints from multiple sources?

Consider something like:

<var1>[< <existential>]
<var2>[> <existential>]

This implies that <var1> < <var2>, but how to actually go about extracting that?

Idea: Only filter out unused variables if there are not at least two constraints which reference it
-}
eliminateUnusedBindings :: (HasTracer agent, Renderable rule, Eq rule, HasRule rule, Members '[Reader agent, Log, Resource, OpenTelemetrySpan, OpenTelemetryContext, NonDet] r) => rule -> Sem r rule
eliminateUnusedBindings r = push "eliminateUnusedBindings" do
  let lhsVarsToSave = setOf (unusedVariableFilter not) r
      lhsVars = setOfSingleOccurances (r ^.. unusedVariableFilter id)
      rhsVars = setOf (ruleRHS . variables) r
      unused = lhsVars `HS.difference` (rhsVars `HS.union` lhsVarsToSave)
  addAttributes
    [ ("pscm.rules.simplification.rule", OT.toAttribute . T.pack . show . render $ r),
      ("pscm.rules.simplification.lhs_variables_under_consideration", OT.toAttribute . T.pack . show . render $ lhsVars),
      ("pscm.rules.simplification.lhs_variables_to_save", OT.toAttribute . T.pack . show . render $ lhsVarsToSave),
      ("pscm.rules.simplification.rhs_variables", OT.toAttribute . T.pack . show . render $ rhsVars),
      ("pscm.rules.simplification.unused_variables", OT.toAttribute . T.pack . show . render $ unused)
    ]
  if HS.null unused
    then mzero
    else do
      let modified =
            r
              & ruleLHS
                . each
                . conditionPatterns
                . confusing (each
                . filtered
                  (\pv -> has (confusing $ _APatternVariable . _1 . variable . filtered (flip HS.member unused)) pv))
                .~ DontCareCondition
              & ruleLHS
                . each
                . conditionPatterns
                . confusing (each
                . _APatternVariable
                . _2
                . _Just
                . orderingTest)
                %~ Seq.filter (hasn't (confusing $ comparingTo . comparisonVariable . filtered (flip HS.member unused)))
                
      if (WithoutMeta (modified^.rule)) == (WithoutMeta (r^.rule)) then mzero else pure modified

eliminateAllDontCare'' :: Alternative f => SPEC -> [RuleCondition] -> f [RuleCondition]

eliminateAllDontCare'' !_sPEC ((PositiveCondition (WMEPattern DontCareCondition DontCareCondition DontCareCondition Don'tCareAboutPreference)) : xs) = pure xs
eliminateAllDontCare'' !sPEC ((p@(PositiveCondition _)) : xs) = (p:) <$> eliminateAllDontCare'' sPEC xs
eliminateAllDontCare'' !sPEC (n@(NegatedConjunction (PositiveCondition (WMEPattern DontCareCondition DontCareCondition DontCareCondition Don'tCareAboutPreference) :| [])) : ys) | Just ys' <- eliminateAllDontCare'' sPEC ys = pure (n : ys')
eliminateAllDontCare'' !_sPEC (NegatedConjunction (PositiveCondition (WMEPattern DontCareCondition DontCareCondition DontCareCondition Don'tCareAboutPreference) :| (x:xs)) : ys) = pure $ (NegatedConjunction (x :| xs)) : ys
eliminateAllDontCare'' !sPEC ((n@(NegatedConjunction (_ :| []))) : ys) = fmap (n :) $ eliminateAllDontCare'' sPEC ys
eliminateAllDontCare'' !sPEC ((NegatedConjunction (x :| (x' : xs))) : ys) | Just [] <- eliminateAllDontCare'' sPEC [x] = pure ((NegatedConjunction (x' :| xs)) : ys) -- not redundant because nested negations
eliminateAllDontCare'' !sPEC ((NegatedConjunction (x :| xs)) : ys) | Just xs' <- eliminateAllDontCare'' sPEC xs = pure $ NegatedConjunction (x :| xs') : ys                                                                                      
eliminateAllDontCare'' !sPEC (x : xs) | Just xs' <- eliminateAllDontCare'' sPEC xs = pure (x : xs')
eliminateAllDontCare'' !_sPEC _ = empty

editRuleConditions :: forall agent rule r. (HasTracer agent, Eq rule, HasRule rule, Members '[Reader agent , Log, Resource, OpenTelemetrySpan, OpenTelemetryContext, NonDet] r) => SPEC -> (SPEC -> [RuleCondition] -> Sem r  ([RuleCondition])) -> rule -> Sem r rule
editRuleConditions !sPEC f r = push "editRuleConditions" do
  addAttributeWithRaw "rule.original" (r^.rule)
  r' <- (editRuleConditions' sPEC r)
  addAttributeWithRaw "rule.simplified" (r'^.rule)        
  if (WithoutMeta (r^.rule)) == (WithoutMeta (r'^.rule)) then do
      let attrs = (makeAttributesWithRaw "rule.original" (r^.rule)) <> makeAttributesWithRaw "rule.simplified" (r'^.rule)
      debug "No change to rule" attrs
      mzero
  else
      pure r'
     where
       editRuleConditions' :: SPEC -> rule -> Sem r rule
       editRuleConditions' !sPEC ruleHolder = do
         rs <- f sPEC (ruleHolder ^.. ruleLHS . patterns . folded)
         case nonEmpty rs of
           Nothing -> mzero
           Just rs' -> pure (ruleHolder & ruleLHS . patterns .~ rs')
         
eliminateAllDontCare :: (HasTracer agent, Eq rule, HasRule rule, Members '[Reader agent, Log, Resource, OpenTelemetrySpan, OpenTelemetryContext, NonDet] r) => SPEC -> rule -> Sem r rule
eliminateAllDontCare !sPEC r = push "eliminateAllDontCare" $ editRuleConditions sPEC eliminateAllDontCare'' r -- failToNonDet do
  -- Just r' <- pure (eliminateAllDontCare' sPEC r)
  -- if (WithoutMeta (r^.rule)) == (WithoutMeta (r'^.rule)) then mzero else pure r'

removeRedundantClauses :: (HasTracer agent, Eq rule, HasRule rule, Members '[Reader agent, Log, Resource, OpenTelemetrySpan, OpenTelemetryContext, NonDet] r) => SPEC -> rule -> Sem r rule
removeRedundantClauses !sPEC r = push "removeRedundantClauses" $ editRuleConditions sPEC f r where
    f !_sPEC (x:xs) =  do
      addAttributeWithRaw "rule.state_test_pattern" x
      addAttributeWithRaw "rule.original.remaining_conditions" xs
      let hs = HS.fromList xs
          xs' = HS.toList (HS.delete x hs)
      addAttributeWithRaw "rule.simplified.remaining_conditions" xs'                
      when (length xs == length xs') mzero
      pure (x:xs')
    f _ [] = mzero           
                               
-- eliminateAllDontCare' :: (HasRule rule, Alternative f) => SPEC -> rule -> f rule
-- eliminateAllDontCare' !sPEC rule = do
--   rs <- eliminateAllDontCare'' sPEC (rule ^.. ruleLHS . each1)
--   pure (rule & ruleLHS . partsOf each1 .~ rs)

simplifyRuleConds :: (HasTracer agent, Eq rule, HasRule rule, Members '[Reader agent, Log, Resource, OpenTelemetrySpan, OpenTelemetryContext, NonDet] r) => rule -> Sem r rule
simplifyRuleConds r = push "simplifyRuleConds" do
  (_dict, simplified) <- extractSubstitutionFromVariableMapping "____ERROR!" (simplifyRuleConditions r)
  if (WithoutMeta (simplified^.rule)) == (WithoutMeta (r^.rule)) then mzero else pure simplified -- TODO FIXME:  & extraBindings .~ dict

analyseVariables :: (HasTracer agent, Members '[Reader agent, Log, Resource, OpenTelemetrySpan, OpenTelemetryContext, NonDet, Error RuleSemanticError] r) => Rule' -> Sem r Rule'
analyseVariables r = push "analyseVariableTypes" do
  addAttributeWithRaw "rule.analysis.variables.types.original" (r^.types)
  let objs = r ^. fusing (ruleLHS . conditions) . conditionPatterns . confusing (patternObject . _APatternVariable . _1 . variable) . to \v -> (constructSubstitution [(v, (Meet (strongly Obj)))])
      rWithObjs = r & types %~ \t -> getMeet <$> (objs <> (Meet <$> t))
      (tys :: SubstitutionLattice (Meet VariableType), constrs :: SubstitutionLattice (Meet VariableConstraints) ) = rWithObjs ^. rulePatternConditions . to (\pat -> (Meet <$> patternConditionTypes (rWithObjs^.types) pat, Meet <$> patternConditionConstraints (rWithObjs^.constraints) pat ))
      r' = rWithObjs & types %~ (\t -> getMeet <$> (tys <> (Meet <$> t))) & constraints %~ \t -> getMeet <$> (constrs <> (Meet <$> t))
  addAttributeWithRaw "rule.analysis.variables.types.pattern_object_variables" (rWithObjs^.types)
  addAttributeWithRaw "rule.analysis.variables.types.further_analysed" (r'^.types)           
  when ( has (types . folded . _VariableType . _Left . _Conflict) r') do
       warning "Rule has incompatible types!" [("rule.plain", fromString @T.Text . show . render $ r), ("rule.types", fromString @T.Text . show . render $ r^.types)]
       throw RuleHasIncompatibleTypes
  if (r'^.types) == (r^.types) || (r'^.constraints) == (r^.constraints) then empty else pure r'
     

analyseCondition :: (HasTracer agent, Members '[Reader agent, Log, Resource, OpenTelemetrySpan, OpenTelemetryContext, NonDet, Error RuleSemanticError] r) => WMEPattern -> Sem r ()
analyseCondition (WMEPattern (AConstantEqualityTest _) _ _ _) = throw (PatternObjectIsConstantTest ^. re _InvalidPattern)
analyseCondition (WMEPattern (AnObjectIdentity _) _ _ _) = throw (PatternObjectIsIdentityTest ^. re _InvalidPattern)
analyseCondition (WMEPattern _ (APatternVariable (StatePatternVariable _) _) _ _) = throw (PatternAttributeVariableIsStateVariable ^. re _InvalidPattern)
analyseCondition (WMEPattern _ (AConstantEqualityTest (PatternConstant _)) _ _) = throw (PatternAttributeConstantIsNonSymbolic ^. re _InvalidPattern)
analyseCondition _ = pure ()
                     
analyseConditions :: (HasTracer agent, Members '[Reader agent, Log, Resource, OpenTelemetrySpan, OpenTelemetryContext, NonDet, Error RuleSemanticError] r) => Rule' -> Sem r Rule'
analyseConditions r = push "analyseConditions" do
  forMOf_ (fusing (ruleLHS.conditions) . conditionPatterns) r analyseCondition
  empty                                                                     
       


simplifyRuleRewrites :: (HasTracer agent, Members '[Reader agent, Log, Resource, OpenTelemetrySpan, OpenTelemetryContext,  Error RuleSemanticError] r) => SPEC -> Rule' -> Sem r (Maybe Rule')
simplifyRuleRewrites !sPEC rule = runNonDetMaybe do
  msum [
        analyseConditions rule, -- Ensure that patterns are well-formed
        analyseVariables rule, -- We want this before simplifications involving variables, because we want a type error to occur even when the relevant part of the rule is optimised away.
        eliminateUnusedBindings rule,
        eliminateAllDontCare sPEC rule,                                
        simplifyRuleConds rule,
        removeRedundantClauses sPEC rule
        ]


simplifyRule :: (HasTracer agent, Members '[Reader agent, Log, Resource, OpenTelemetrySpan, OpenTelemetryContext, Error RuleSemanticError] r) => Rule -> Sem r Rule
simplifyRule r = push "simplifyRule" $ do
  addAttributeWithRaw "rule.id" (r^.identifier)                   
  addAttributeWithRaw "pscm.rules.simplification.original_rule" r

  -- TODO FIXME: Collect the variable constraints and return them too                      
  (Rule' result tys constrs) <- simplifyRule' SPEC (Rule' r trivialSubstitution trivialSubstitution)
  when (tys == incompatibleSubstitution ) do
    warning "Rule has incompatible types!" [("rule.plain", fromString @T.Text . show . render $ r), ("rule.types", fromString @T.Text . show . render $ tys)]
    throw RuleHasIncompatibleTypes
  when (constrs == incompatibleSubstitution) do
    warning "Rule has incompatible variable constraints!" [("rule.plain", fromString @T.Text . show . render $ r )]
    throw IncompatibleVariableBindings
  addAttributeWithRaw "pscm.rules.simplification.simplified_rule" result
  pure (updateStateTestPattern result)
  where
    simplifyRule' !sPEC rule = do
      res' <- simplifyRuleRewrites sPEC rule
      case res' of
        Nothing -> pure rule
        Just res -> simplifyRule' sPEC res
