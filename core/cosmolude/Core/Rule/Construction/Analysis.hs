{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# OPTIONS_GHC -Wwarn=orphans #-}
module Core.Rule.Construction.Analysis (
                                        -- * Annotated rules
                                        --
                                        -- $annotatedRules
                                        Rule'(Rule'), theRule, types, constraints,
                                        -- * Rule semantic errors
                                        --
                                        -- $ruleSemanticErrors
                                        RuleSemanticError(..),
                                        -- * Analysis combinators
                                        --
                                        -- $variableConstraintAnalysis
                                        VariableConstraints,
                                        patternConstraintAnalysis,
                                        -- * Type checking
                                        --
                                        -- $typeChecking
                                        patternConditionTypes,
                                        VariableType, AsVariableType(..), VariableTypePlain(..), AsVariableTypePlain(..), strongly, weakly,
                                        -- * Constraint accumulation
                                        --
                                        -- $constraintAccumulation
                                        patternConditionConstraints
                                       ) where
import Algebra.Lattice
import Language.Haskell.TH.Syntax    
import Data.Set (Set)
--import qualified Data.Set as Set    

import Math.ParetoFront hiding (singleton)

import Algebra.PartialOrd    
import Control.Applicative
import Core.CoreTypes
import Core.Patterns.PatternSyntax

import Core.Rule


import Cosmolude hiding (As)



--import qualified Data.Histogram as Histo
--import qualified Data.List as DL
--import Data.List.NonEmpty (NonEmpty (..))
--import qualified Data.Text as T

--import qualified OpenTelemetry.Trace as OT

-- import Polysemy (run)
--import Polysemy.Fail (failToNonDet)
--import Polysemy.NonDet


import Iso.Deriving
import Data.Either    
import Algebra.Lattice.M3



    
----------------------------------------
--- Generic variable constraint analysis
----------------------------------------

-- $variableConstraintAnalysis
--
-- Combinator to perform a constraint analysis on a variable in a pattern condition.

{-# INLINE patternConstraintAnalysis #-} 
patternConstraintAnalysis :: Eq v => (SubstitutionLattice v -> Variable -> SubstitutionLattice v) -> (SubstitutionLattice v -> Variable -> VariableConstraintTests -> SubstitutionLattice v) -> SubstitutionLattice v -> PatternCondition -> SubstitutionLattice v
patternConstraintAnalysis _ _ sub _ | sub == incompatibleSubstitution = incompatibleSubstitution
patternConstraintAnalysis whenNothing _ sub (APatternVariable v Nothing) = whenNothing sub (v^.variable)
patternConstraintAnalysis _ whenSomething sub (APatternVariable v (Just (AComparisonTest constrs))) = whenSomething sub (v^.variable) constrs
patternConstraintAnalysis _ _ _ _ = trivialSubstitution

----------------------------------------
--- Type checking
----------------------------------------

-- $typeChecking
--
-- Best-effort analysis of variable types. Incompatible types will raise an error.

data VariableTypePlain = Conflict | 
                         Sym | Obj | Number   | Unknown
                               deriving (Eq, Show, Generic, Hashable)
makeClassyPrisms ''VariableTypePlain                                        

instance (Project a b, PartialOrd a) => PartialOrd (As a b) where
    {-# INLINE CONLIKE leq #-}
    leq (As a) (As b) = prj @a @b a `leq` prj b
instance (Project a b, Inject a b, Lattice a) => Lattice (As a b) where
    {-# INLINE CONLIKE (/\) #-}
    (As a) /\ (As b) = As . inj $ (prj @a @b a)  /\ (prj b)
    {-# INLINE CONLIKE (\/) #-}                        
    (As a) \/ (As b) = As . inj $ prj @a @b a \/ prj b                        
instance ( Project a b, Inject a b, BoundedJoinSemiLattice a) => BoundedJoinSemiLattice (As a b) where
    bottom = As (inj (bottom @a))
instance (Project a b, Inject a b, BoundedMeetSemiLattice a) => BoundedMeetSemiLattice (As a b) where
    top = As (inj (top @a))
          
deriving via ( M3 `As` VariableTypePlain) instance PartialOrd VariableTypePlain
deriving via ( M3 `As` VariableTypePlain) instance Lattice VariableTypePlain
deriving via ( M3 `As` VariableTypePlain) instance BoundedJoinSemiLattice VariableTypePlain
deriving via ( M3 `As` VariableTypePlain) instance BoundedMeetSemiLattice VariableTypePlain
         
instance Inject M3 VariableTypePlain  where
    {-# INLINE CONLIKE inj #-}
    inj M3o = Conflict
    inj M3a = Sym
    inj M3b = Obj
    inj M3c = Number
    inj M3i = Unknown    
instance Project  M3 VariableTypePlain where
    {-# INLINE CONLIKE prj #-}
    prj Conflict = M3o
    prj Sym = M3a
    prj Obj = M3b
    prj Number = M3c
    prj Unknown = M3i

instance Isomorphic M3 VariableTypePlain     


newtype VariableType = VariableType (Either (VariableTypePlain) (VariableTypePlain)) deriving stock (Eq, Show, Generic)
    deriving anyclass (Hashable)
    deriving newtype (PartialOrd, Lattice, BoundedJoinSemiLattice, BoundedMeetSemiLattice)
instance Renderable VariableType where
    render = viaShow
makeClassyPrisms ''VariableType
           
strongly, weakly :: VariableTypePlain -> VariableType
strongly = VariableType . Left
weakly = VariableType . Right
    
    
variableType :: SubstitutionLattice VariableType -> Variable -> ComparisonTest -> SubstitutionLattice VariableType
variableType sub v1 (EqualTo (CompareAgainstVariable v2)) | Just existing2 <- sub^? at v2 ._Just,
                                                            Just existing1 <- sub ^? at v1 . _Just,
                                                            existing1 /= (weakly Unknown),
                                                            existing2 /= (weakly Unknown) = constructSubstitution [(v1, existing2), (v2, existing1)]
variableType sub v1 (EqualTo (CompareAgainstVariable v2)) | Just existing <- sub^? at v2 ._Just, existing /= (weakly Unknown) = singleton v1 existing
variableType sub v1 (EqualTo (CompareAgainstVariable v2)) | Just existing <- sub^? at v1 ._Just, existing /= (weakly Unknown) = singleton v2 existing
variableType _ v1 (EqualTo (CompareAgainstVariable v2)) = constructSubstitution [(v1, weakly Unknown), (v2, weakly Unknown)]
variableType _ v1 (NotEqualTo (CompareAgainstVariable v2)) = constructSubstitution [(v1, weakly Unknown), (v2, weakly Unknown)]
variableType _ v1 comp | v:vs <- comp^..variables = constructSubstitution $ (v1, weakly Number) : (v, weakly Number) : fmap (, weakly Number) vs
variableType _ v (EqualTo (CompareAgainstValue (Symbolic (Symbol _)))) = singleton v (strongly Sym)
variableType _ v comp | Just _ <- comp^? comparingTo . comparisonValue . _Symbol = singleton v (weakly Sym)
variableType _ v (EqualTo (CompareAgainstValue (Symbolic (ObjectIdentity _)))) = singleton v (strongly Obj)
variableType _ v comp | Just _ <- comp^? comparingTo . comparisonValue . _ObjectIdentity = singleton v (weakly Obj)
variableType _ v (EqualTo (CompareAgainstValue (Constant _))) = singleton v (strongly Number)
variableType _ v comp | Just _ <- comp^? comparingTo . comparisonValue .  _Constant = singleton v (weakly Number)
variableType _ _ _ = trivialSubstitution

{-# INLINE patternConditionTypes #-}                     
patternConditionTypes :: SubstitutionLattice VariableType -> PatternCondition -> SubstitutionLattice VariableType
patternConditionTypes = patternConstraintAnalysis
                        (\_ v -> singleton (v^.variable) (weakly Unknown))
                        (\sub v tests' -> let types = tests'^.conditionConstraints . to (variableType sub (v^.variable)) . to (fmap Meet)  in  getMeet <$> types)




----------------------------------------
--- Constraint accumulation
----------------------------------------

-- $constraintAccumulation
--
-- When we simplify patterns via, for example, constant folding of equality constraints, we may discard other
-- constraints involved. Thus, it is important to collect all constraints between variables before we
-- simplify, in order to preserve correctness.


collectConstraints :: Eq v => SubstitutionLattice v -> Variable -> VariableConstraintTests -> SubstitutionLattice v
collectConstraints _ _v constrs' | constrs <- constrs'^.conditionConstraints . to stratum, (_simplified:_) <- fmap getFront $ getStrata constrs  = top
collectConstraints _ _ _ = top

-- TODO
-- NOTE NOTE NOTE: Must take into account negated conditions - meaning that constraints have to be inverted.
{-# INLINE patternConditionConstraints #-}
patternConditionConstraints :: SubstitutionLattice VariableConstraints -> PatternCondition -> SubstitutionLattice VariableConstraints
patternConditionConstraints = patternConstraintAnalysis
                              (const . const emptySubstitution)
                              (\sub v tests' -> let comparisons = tests' ^. to (collectConstraints sub (v^.variable)) . to (fmap Meet) in getMeet <$> comparisons)

    
newtype VariableConstraints = VariableConstraints {_accumulatedConstraints :: Set ComparisonTest}
    deriving newtype (Eq, Ord, Show, Lattice, BoundedJoinSemiLattice)
    deriving stock (Generic, Lift)
    deriving anyclass Hashable

instance Renderable VariableConstraints where
    render (VariableConstraints c) = render (c^..folded)
                 
makeClassy ''VariableConstraints


----------------------------------------
--- Annotated rule
----------------------------------------

-- $annotatedRules
--
-- Rules equipped with extra data learned during analysis.

-- | A rule  equipped with extra data learned during analysis.
data Rule' = Rule' {_theRule :: Rule, _types :: SubstitutionLattice VariableType, _constraints :: SubstitutionLattice (VariableConstraints)} deriving (Eq, Show)
makeLenses ''Rule'

instance HasRule Rule' where
    {-# INLINE rule #-}
    rule = theRule
instance Renderable Rule' where
    render (Rule' r tys constrs) = render r <+> render tys <+> render constrs


                                          
