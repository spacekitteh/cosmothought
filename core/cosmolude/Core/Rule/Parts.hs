{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE StrictData #-}

module Core.Rule.Parts ( -- * Rule left-hand sides
                        RuleLHS,  patterns, RuleLHSInfo(..), OriginalRuleConditions, originalStateTestPattern, originalRemainingConditions,
                               OptimisedRuleConditions (OptimisedRuleConditions), RuleLHSWithPlaceholders (RuleLHS'),
                         makeRuleLHS,computeRuleLHSInfo, ruleLHSWithPlaceholdersToOriginalRuleConditions, 
                         -- ** LHS accessors
                                    stateTestPattern, testsOperator, 
                         -- * Rule right-hand sides
                        RuleRHS(RuleRHS), RuleRHSInfo(..),  RuleRHSWithPlaceholders (RuleRHS'), computeRuleRHSInfo, numberOfOperatorPreferencesSet, operatorPreferencePattern,
                               -- ** RHS accessors
                       rhsPatterns, actions,
                                  -- * Rule info
                                  RuleInfo (RuleInfo), RuleProvenance (..), ruleLHSInfo, ruleRHSInfo, ruleProvenance, operatorSupported
                       ) where
import Data.Functor.Apply
--import qualified Control.Lens.Unsound as UNSAFE (adjoin)

import Data.List.NonEmpty (NonEmpty(..), nub)
import Core.Variable.Substitution
import GHC.Stack
import Core.Utils.MetaTypes
import Core.Variable.AlphaEquivalence
import Core.Identifiers    
import Core.Action
import Core.Action.Expression

import Control.Applicative
import Data.Sequence.Lens
import Core.Patterns.PatternSyntax
import Prelude
import Prettyprinter
import Data.Sequence (Seq)
import Data.Foldable
import Core.Variable

import GHC.Generics (Generic)
import Control.Lens
import Data.Hashable
import Core.Utils.Rendering.CoreDoc

import qualified Data.HashSet as HS
import Data.HashSet.Lens

import Core.Rule.Condition


import Numeric.Natural








--------------------------------------------------------------------------------
-- Rule left-hand sides
--------------------------------------------------------------------------------    

newtype RuleLHS = RuleLHS {_patterns :: NonEmpty RuleCondition}
  deriving stock (Generic)
  deriving newtype (Eq, Ord, Show, Hashable)
  deriving Pretty via ByRenderable RuleLHS
instance Renderable RuleLHS where
  render (RuleLHS p) = vsep (fmap render (toList p))

makeLenses ''RuleLHS
data OriginalRuleConditions = OriginalRuleConditions {_originalStateTestPattern :: WMEPattern, _originalRemainingConditions :: Seq RuleCondition} deriving (Eq, Show)
instance Renderable  OriginalRuleConditions where
    render (OriginalRuleConditions ostp orcs) = vsep ["Original state test pattern:" <+> render ostp, "Original remaining conditions:" <+> render orcs]
instance HasVariables OriginalRuleConditions where
    {-# INLINE variables #-}
    variables f (OriginalRuleConditions x xs) = OriginalRuleConditions <$> traverseOf (patternBindingVariables) f x <*> traverseOf (each. each . patternBindingVariables) f xs
makeLenses ''OriginalRuleConditions
           
data OptimisedRuleConditions = OptimisedRuleConditions {optimisedStateTestPattern :: WMEPattern, optimisedRemainingConditions :: [RuleCondition], optimisedExtraBindings :: SubstitutionLattice (RuleExpr Variable)} deriving (Eq, Show)
instance Renderable  OptimisedRuleConditions  where
    render (OptimisedRuleConditions ostp orcs oeb) = vsep ["Optimised state test pattern:" <+> render ostp, "Optimised remaining conditions:" <+> render orcs, "Optimised extra bindings:" <+> render oeb]


                                                   
{-#INLINE makeRuleLHS #-}
makeRuleLHS :: Alternative f => OptimisedRuleConditions -> f (RuleLHS, SubstitutionLattice (RuleExpr Variable))
-- TODO HACK: Figure out how to integrate the nubbing into the rewriting; and also, optimise it!
makeRuleLHS (OptimisedRuleConditions (stp@(WMEPattern (APatternVariable (StatePatternVariable _) _) _ _ _)) rest  bindings) = pure (RuleLHS  . nub $ (PositiveCondition stp) :| rest, bindings)
makeRuleLHS _ = empty

                                                 
data RuleLHSInfo = RuleLHSInfo
  { numberOfPatterns :: Natural,
    numberOfUniqueSymbols :: Natural,
    numberOfSymbols :: Natural,
    -- | Excludes variables appearing in conditions.
    numberOfUniqueBindingVariables :: Natural,
    numberOfBindingVariables :: Natural,
    testsOperator :: Maybe WMEPattern,
    stateTestPattern :: WMEPattern
  }
  deriving (Eq,  Show)
{-# INLINE testsOperator #-}
testsOperator :: Lens' RuleLHSInfo (Maybe WMEPattern)
testsOperator = lens (\RuleLHSInfo{..} -> testsOperator) (\lhsInfo new -> lhsInfo {testsOperator = new})

{-# INLINE stateTestPattern #-}
stateTestPattern :: Lens' RuleLHSInfo (WMEPattern)
stateTestPattern = lens (\RuleLHSInfo{..} -> stateTestPattern) (\lhsInfo new -> lhsInfo {stateTestPattern = new})

computeRuleLHSInfo :: HasCallStack =>  OriginalRuleConditions -> OptimisedRuleConditions -> RuleLHSInfo
computeRuleLHSInfo (OriginalRuleConditions originalHead originalRemainingConds) (OptimisedRuleConditions optimisedHead optimisedRemainingConds _) = info
  where
    pats = (PositiveCondition originalHead :| (originalRemainingConds^..folded))
    syms = pats ^.. confusing (each . each . patternSymbols)
    symCount = fromIntegral $ lengthOf (each . each . patternSymbols) pats
    uniqueSyms = fromIntegral $ HS.size $ HS.fromList syms
    vars = setOf (each . each . patternBindingVariables) pats
    varsCount = fromIntegral $ HS.size vars
    uniqueVars = fromIntegral $ HS.size $ HS.map (view variableName) vars
    stateVariable = optimisedHead^?patternObject . patVariable . variable
    testsCurrentOperator = findOf (each.each) (\pat -> isOperatorPattern pat && stateVariable == (pat^? (patternObject . patVariable . variable) ) ) (PositiveCondition optimisedHead :| optimisedRemainingConds) 
    info =
      RuleLHSInfo
        { numberOfPatterns = (fromIntegral . lengthOf (folded.conditionPatterns) $ pats),
          numberOfUniqueSymbols = uniqueSyms,
          numberOfSymbols = symCount,
          numberOfUniqueBindingVariables = uniqueVars,
          numberOfBindingVariables = varsCount,
          testsOperator = testsCurrentOperator,
          stateTestPattern =  optimisedHead
        }



instance HasConditions RuleLHS where
  {-# INLINE conditions #-}
  conditions = each1

instance Each1 RuleLHS RuleLHS RuleCondition RuleCondition where
  {-# INLINE each1 #-}
  each1 = patterns . (each1 )


-- instance Cons RuleLHS RuleLHS RuleCondition RuleCondition where
--   {-# INLINE _Cons #-}
--   _Cons = coerced . (_Cons @(Seq RuleCondition) ) . mapping coerced

-- instance Snoc RuleLHS RuleLHS RuleCondition RuleCondition where
--   {-# INLINE _Snoc #-}
--   _Snoc = coerced . (_Snoc @(Seq RuleCondition)) . swapped . mapping coerced . swapped



data RuleRHS = RuleRHS {_actions :: Seq RuleAction} deriving (Eq, Generic, Hashable, Show)
             deriving Pretty via ByRenderable RuleRHS

instance Renderable RuleRHS where
  render (RuleRHS p) = --vsep ["Actions:" <+>
                                            (vsep (fmap render (toList p)))
--                                  "Objects to create" <+> render vars]

makeLenses ''RuleRHS

data RuleRHSInfo = RuleRHSInfo
  { numberOfActions :: Natural,
    numberOfCreatedWMEs :: Natural,
    numberOfRemovedWMEs :: Natural,
    numberOfUniqueActionSymbols :: Natural,
    numberOfActionSymbols :: Natural,
-- TODO IS THIS CORRECT?    -- | Excludes variables appearing in conditions.
    numberOfUniqueActionBindingVariables :: Natural,
    numberOfActionBindingVariables :: Natural,
    numberOfOperatorPreferencesSet :: Natural,
    operatorPreferencePattern :: Maybe WMEPattern
  }
  deriving (Eq, Generic, Show)

computeRuleRHSInfo :: Seq RuleAction -> RuleRHSInfo
computeRuleRHSInfo actions = info
  where
    syms = actions ^.. confusing (each . actionPattern . patternSymbols)
    symCount = fromIntegral $ lengthOf (each . actionPattern . patternSymbols) actions
    uniqueSyms = fromIntegral $ HS.size $ HS.fromList syms
    vars = setOf (confusing $ each . actionPattern . patternBindingVariables) actions
    varsCount = fromIntegral $ HS.size vars
    uniqueVars = fromIntegral $ HS.size $ HS.map (view variableName) vars
    numberOfCreatedWMEs = fromIntegral $ lengthOf (each . _NewWME) actions
    numberOfRemovedWMEs = fromIntegral $ lengthOf (each . _RemoveWME) actions
    numberOfOperatorPreferencesSet = fromIntegral $ lengthOf (confusing $ each . filtered (has _NewWME) . actionPattern . patternPreference . _WithAPreference) actions
    operatorPreferencePattern' = firstOf (confusing $ each . filtered (has _NewWME) . actionPattern . filtered (has (patternPreference . _WithAPreference))) actions
    info = RuleRHSInfo {numberOfActions = (fromIntegral . length $ actions), numberOfUniqueActionSymbols = uniqueSyms, numberOfActionSymbols = symCount, numberOfUniqueActionBindingVariables = uniqueVars, numberOfActionBindingVariables = varsCount, numberOfCreatedWMEs = numberOfCreatedWMEs, numberOfRemovedWMEs = numberOfRemovedWMEs, numberOfOperatorPreferencesSet = numberOfOperatorPreferencesSet, operatorPreferencePattern = operatorPreferencePattern'}


numberOfOperatorPreferencesSet :: Getter RuleRHSInfo Natural
numberOfOperatorPreferencesSet = to (\(RuleRHSInfo{..}) -> numberOfOperatorPreferencesSet)
operatorPreferencePattern :: Getter RuleRHSInfo (Maybe WMEPattern)
operatorPreferencePattern = to (\(RuleRHSInfo{..}) -> operatorPreferencePattern)
instance Each RuleRHS RuleRHS RuleAction RuleAction where
  {-# INLINE each #-}
  each = actions . (each @(Seq RuleAction))


{-# INLINE rhsPatterns #-}
-- TODO: Make this an IndexedTraversal' ActionWMEPatternTag RuleRHS WMEPattern
rhsPatterns :: Traversal' RuleRHS WMEPattern
rhsPatterns = confusing (each . actionPattern)
                                        
newtype RuleLHSWithPlaceholders = RuleLHS' {_patternsWithoutVariableIdentifiers :: NonEmpty RuleCondition}
  deriving stock (Generic)
  deriving newtype (Eq, Ord, Show, Hashable)
makeLenses ''RuleLHSWithPlaceholders
ruleLHSWithPlaceholdersToOriginalRuleConditions :: Alternative f => RuleLHSWithPlaceholders -> f OriginalRuleConditions
ruleLHSWithPlaceholdersToOriginalRuleConditions (RuleLHS' ((PositiveCondition x@(WMEPattern (APatternVariable (StatePatternVariable _) _) _ _ _)) :| xs)) = pure (OriginalRuleConditions x (seqOf folded xs))
ruleLHSWithPlaceholdersToOriginalRuleConditions _ = empty
instance Each RuleLHSWithPlaceholders RuleLHSWithPlaceholders RuleCondition RuleCondition where
  {-# INLINE each #-}
  each = confusing (patternsWithoutVariableIdentifiers . each )

deriving via (EqualityModuloAlpha RuleLHSWithPlaceholders) instance AlphaEquivalence RuleLHSWithPlaceholders


data RuleRHSWithPlaceholders = RuleRHS' {_actionsWithoutVariableIdentifiers :: Seq RuleAction} deriving (Eq, Generic, Hashable, Show)
makeLenses ''RuleRHSWithPlaceholders

instance Each RuleRHSWithPlaceholders RuleRHSWithPlaceholders WMEPattern WMEPattern where
  {-# INLINE each #-}
  each = confusing (actionsWithoutVariableIdentifiers . (each @(Seq RuleAction)) . actionPattern)

deriving via (EqualityModuloAlpha RuleRHSWithPlaceholders) instance AlphaEquivalence RuleRHSWithPlaceholders


data RuleInfo = RuleInfo {_ruleLHSInfo :: RuleLHSInfo,  _ruleRHSInfo :: RuleRHSInfo, _ruleProvenance :: RuleProvenance, _operatorSupported :: Maybe WMEPattern} deriving (Eq, Show, Generic)
data RuleProvenance = Manual | Chunked [RuleIdentifier] deriving (Eq, Show, Generic)
              
makeLenses ''RuleInfo
