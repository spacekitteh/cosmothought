{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE PatternSynonyms #-}
{-# OPTIONS_GHC -Wwarn=deprecations #-}
module Core.Rule.Condition (RuleCondition(PositiveCondition), pattern NegatedConjunction, ConditionSense(..), conditionPatterns, varsInConditionSense, AppearanceSense, varsInPositiveCondition, HasConditions(..), proveIsOfSense, allSubConditions,  immediateConditionPatterns, coherentTests) where
import Math.ParetoFront
import Control.Applicative
import Core.Variable
import Theory.Named
import Data.The
import GHC.Stack
import Core.Utils.MetaTypes
import Core.Patterns.PatternSyntax
import Control.Lens

import qualified Data.List as DL
import Prelude
import Core.Variable.AlphaEquivalence
import Core.Utils.Rendering.CoreDoc
import Data.Sequence (Seq)
import GHC.Generics (Generic)
import Data.Hashable
import Control.Monad

import Theory.Equality

import Logic.Proof


import Data.List.NonEmpty
import Data.Sequence.Lens

import Prettyprinter as PP
data RuleCondition = PositiveCondition WMEPattern
                   | Negated RuleCondition (Seq RuleCondition)
                   deriving (Eq, Ord, Show, Generic, Hashable)
                   deriving Pretty via ByRenderable RuleCondition
                   deriving AlphaEquivalence via EqualityModuloAlpha RuleCondition


instance Debatable RuleCondition where
    {-# INLINEABLE weigh #-}
    weigh a b | a == b = WeakTie
    weigh (PositiveCondition a) (PositiveCondition b) = weigh a b
    weigh (Negated a as) (Negated b bs) = collectionDominates (a :< as) (b :< bs)
    weigh _ _ = StrongTie
instance Plated RuleCondition where
  {-# INLINE plate #-}
  plate _ p@(PositiveCondition _) =  pure p
  plate f (NegatedConjunction conds) = NegatedConjunction <$> traversed f conds
data AsNegatedConjunction = NegatedConj (NonEmpty RuleCondition)

{-# INLINE asNegatedConds #-}
asNegatedConds :: Alternative f => RuleCondition -> f AsNegatedConjunction
asNegatedConds (Negated first rest) = pure (NegatedConj (first :| (rest^..folded)))
asNegatedConds _ = empty

{-# COMPLETE PositiveCondition, NegatedConjunction #-}
pattern NegatedConjunction :: NonEmpty RuleCondition -> RuleCondition
pattern NegatedConjunction conds <- (asNegatedConds  -> Just (NegatedConj conds))  where  
    NegatedConjunction (first :| rest) = Negated first (seqOf folded rest)


{-# INLINEABLE coherentTests #-}
coherentTests :: Alternative f => Seq ComparisonTest -> f (Seq ComparisonTest)
coherentTests tests = do
  let front = tests^.folded . to Math.ParetoFront.singleton
  guard ((front^..folded.comparingTo) == DL.nub (front^..folded.comparingTo))
  pure (seqOf folded front)

                                         
data ConditionSense = PositiveAppearance | NegativeAppearance deriving (Eq, Show)

flipPolarity :: ConditionSense -> ConditionSense
flipPolarity PositiveAppearance = NegativeAppearance
flipPolarity NegativeAppearance = PositiveAppearance

instance Renderable RuleCondition where
  render (PositiveCondition pat) = render pat
  render (Negated x xs) = "-" <> PP.braces (PP.fillSep (fmap render (x : (xs^..folded))))

{-# INLINE immediateConditionPatterns #-}
immediateConditionPatterns :: IndexedTraversal' ConditionSense RuleCondition WMEPattern
immediateConditionPatterns f (PositiveCondition ~pat) = PositiveCondition <$> (indexed f PositiveAppearance pat)
immediateConditionPatterns f (NegatedConjunction ~conds) = NegatedConjunction <$>  ((reindexed flipPolarity (traversed . immediateConditionPatterns')) f  conds)
    where
      immediateConditionPatterns' :: IndexedTraversal' ConditionSense RuleCondition WMEPattern
      immediateConditionPatterns' f (PositiveCondition ~pat) = PositiveCondition <$> (indexed f PositiveAppearance pat)
      immediateConditionPatterns' _ (NegatedConjunction ~conds) = NegatedConjunction <$> pure conds
                          
{-# INLINE conditionPatterns #-}
conditionPatterns :: IndexedTraversal1' ConditionSense RuleCondition WMEPattern
conditionPatterns f (PositiveCondition ~pat) = PositiveCondition <$> (indexed f PositiveAppearance pat)
conditionPatterns f (NegatedConjunction ~conds) = NegatedConjunction <$>  ((reindexed flipPolarity (traversed1 . conditionPatterns)) f  conds)

{-# INLINE allSubConditions #-}
allSubConditions :: IndexedTraversal' ConditionSense RuleCondition RuleCondition
allSubConditions _ p@(PositiveCondition _) = (indexed pure PositiveAppearance p)
allSubConditions f (NegatedConjunction ~conds) = NegatedConjunction <$> ((reindexed flipPolarity (traversed . allSubConditions)) f  conds)



instance Each1 RuleCondition RuleCondition WMEPattern WMEPattern where
  {-# INLINE each1 #-}
  each1 = conditionPatterns

{-# INLINE varsInConditionSense #-}
-- | Traverse all pattern binding variables of a given sense.
varsInConditionSense ::   ( HasCallStack, Each rule rule' RuleCondition RuleCondition,
    Ixed amems,
    Index amems ~ WMEPattern
  ) =>
  ConditionSense -> 
  amems ->
  Variable ->
  Traversal rule rule' ((IxValue amems, WMEField), Variable) ((IxValue amems, WMEField), Variable)
varsInConditionSense sense amems var = confusing (each .
                                        (
                                          (reindexed
                                          (\i -> amems ^??! ix (i ^??! conditionPatterns))
                                           selfIndex)
                                          <.>
                                          (cloneTraversal (conditionPatterns . Control.Lens.index sense)
                                           .> patternBindingVariables)
                                          . ifiltered (\_ v -> v == var))
                                         . withIndex)
{-# INLINE varsInPositiveCondition #-}
varsInPositiveCondition ::
  ( Each rule rule' RuleCondition RuleCondition,
    Ixed amems,
    Index amems ~ WMEPattern
  ) =>
  amems ->
  Variable ->
  Traversal rule rule' ((IxValue amems, WMEField), Variable) ((IxValue amems, WMEField), Variable)
varsInPositiveCondition  = varsInConditionSense PositiveAppearance

data AppearanceSense name -- = AppearanceSense Defn
type role AppearanceSense nominal


class HasConditions c where
  conditions :: Traversal1' c RuleCondition


{-# INLINE proveIsOfSense #-}
proveIsOfSense :: RuleCondition ~~ cond -> Either (Proof ((AppearanceSense cond) == NegativeAppearance)) (Proof ((AppearanceSense cond) == PositiveAppearance))
proveIsOfSense (The (PositiveCondition _)) = Right axiom
proveIsOfSense _ = Left axiom




