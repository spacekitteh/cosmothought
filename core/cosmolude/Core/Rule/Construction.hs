module Core.Rule.Construction (elaborateRule, simplifyRule, simplifyConditionClassicalLogic') where
import Cosmolude hiding (Last (..))

--import Polysemy (raise)
--import Core.CoreTypes (Value)
--import Data.Tuple (uncurry)    
import qualified Control.Lens.Unsound as UNSAFE (adjoin)

import Polysemy.Reader (Reader)
import Polysemy.NonDet

import Polysemy.Error    
import Core.Action.Expression
import Core.Action
import qualified Data.HashSet as HS

import Data.HashSet.Lens    
import Data.Sequence.Lens


import Core.Rule.Optimisation

import Core.Rule.Parts

newtype PreRule = PreRule (OriginalRuleConditions,Seq RuleAction) deriving (Generic)
instance Wrapped PreRule

instance HasVariables PreRule where
  {-# INLINE variables #-}
  variables = _Wrapped'. UNSAFE.adjoin ( _1 . variables) (  _2. each.actionPattern.patternBindingVariables)


-- unifyRHSActionPatternWithOperator :: RuleAction -> Maybe (Variable, Value)
-- unifyRHSActionPatternWithOperator (NewWME (WMEPattern _ (APatternVariable pv _)  _ (Just _))) | operatorVariable <- pv^.patternVariableVariable = Just ( operatorVariable, "operator")
-- unifyRHSActionPatternWithOperator (RemoveWME (WMEPattern _ (APatternVariable pv _)  _ (Just _))) | operatorVariable <- pv^.patternVariableVariable = Just ( operatorVariable, "operator")
-- unifyRHSActionPatternWithOperator _ = Nothing
              
-- computeRHSOperatorBindings :: forall r. (Members '[VariableMapping Value] r) => Seq RuleAction -> Sem r ()
-- computeRHSOperatorBindings actions = do
--   let operatorBindings = actions^..folded . to unifyRHSActionPatternWithOperator . _Just
--   forM_ operatorBindings (uncurry mapVariableTo)
      
{-# INLINEABLE elaborateRule #-}
-- | Assigns consistent identifiers, produces rule metadata, and outputs the fully-formed, optimised rule.
elaborateRule :: forall agent r.  (HasTracer agent,  Members '[ Error RuleSemanticError, NonDet, Reader agent, Log, Resource, OpenTelemetrySpan, OpenTelemetryContext] r) =>  RuleIdentifier -> CoreDoc -> RuleLHSWithPlaceholders ->  RuleRHSWithPlaceholders -> SubstitutionLattice (RuleExpr Variable) -> Sem r Rule
elaborateRule ident doc lhs (RuleRHS' rhs) bindings = push "elaborateRule" $ do
  addAttributeWithRaw "rule.id" ident                                                        
  orig <-  ruleLHSWithPlaceholdersToOriginalRuleConditions lhs
  let Identity (PreRule (mappedLHSOriginal, mappedRHS)) =  makeVariablesTermLocal (  Identity $ PreRule ( orig,rhs))
  (sub, optimised) <- extractSubstitutionFromVariableMappingBy pure (optimiseConditions mappedLHSOriginal)
--  (sub'', ()) <- extractSubstitutionFromVariableMappingBy pure (computeRHSOperatorBindings mappedRHS)
  let lhsInfo = computeRuleLHSInfo mappedLHSOriginal optimised
      rhsInfo = computeRuleRHSInfo (mappedRHS)
  (lhs', extraBindings) <- makeRuleLHS optimised
  let bindings' = mergeSubstitutions [bindings,extraBindings]
  case bindings'^?__Substitution of
    Nothing -> throw IncompatibleVariableBindings
    Just bindings'' -> do
        let isLHSOperatorSupported = lhsInfo^.testsOperator
            operatorPreferencesSet = rhsInfo ^. numberOfOperatorPreferencesSet
            operatorPrefPat = rhsInfo ^. operatorPreferencePattern
            isOperatorSupported = (if has _Just isLHSOperatorSupported && operatorPreferencesSet == 0 then isLHSOperatorSupported else Nothing) <|> (if has _Just operatorPrefPat  && operatorPreferencesSet > 0 then operatorPrefPat else Nothing)
            lhsVars = setOf variables lhs'
            rhsVars = setOf variables mappedRHS
            objectsToCreate = seqOf folded $ HS.difference rhsVars lhsVars
                              
        renamed <- renameVariables (Rule ident doc lhs' (RuleRHS mappedRHS) bindings'' objectsToCreate (RuleInfo lhsInfo rhsInfo Manual isOperatorSupported)) sub
        result <- simplifyRule renamed
        addAttributeWithRaw "rule.object" result
        pure result
  
