{-# LANGUAGE GeneralisedNewtypeDeriving #-}

module Core.Rule.Parser (parseRule, parseRuleLHSWithPlaceholders, parseRuleRHSWithPlaceholders) where
import Polysemy.Reader (Reader)

import qualified Control.Monad.Combinators.NonEmpty as NE
import Control.Monad.Trans.Class
import Core.Action.Expression
import Core.Action.Expression.Parser
import Core.Action.Parser
import Core.CoreTypes
import Core.Patterns.Parser
import Core.Patterns.PatternSyntax
import Core.Rule
import Core.Rule.Construction
import Core.Rule.Parts
import Cosmolude hiding ((:<))
import Data.Either (Either (..))
import Data.List.NonEmpty (NonEmpty (..))
import Data.Sequence hiding (length)
import Data.Text (Text)
import qualified Df1
import Polysemy.Error (runError)
import Polysemy.NonDet
import Text.Megaparsec.Char
--import Text.Megaparsec.Debug
parseRuleCondition :: (Members '[ObjectIdentityMap (WME' a)] r) => Parser r Text -> Parser r RuleCondition
parseRuleCondition lhsToRHSParser =
  try (lexeme $ parsePositiveCondition (Just ParsingOrdinaryPattern))
    <|> ((parseNegativeCondition lhsToRHSParser))

parsePositiveCondition :: (Members '[ObjectIdentityMap (WME' a)] r) => Maybe WhenParsingRuleLHS -> Parser r RuleCondition
parsePositiveCondition w = (PositiveCondition <$> (parseWMEPattern (ParsingRule (ParsingRuleLHS w Nothing)))) <?> "Positive condition"

parseNegativeConditionInterior :: (Members '[ObjectIdentityMap (WME' a)] r) => Parser r Text -> Parser r RuleCondition
parseNegativeConditionInterior lhsToRHSParser =
  try
    ( between
        (verbatim "{")
        (verbatim "}")
        ( do
            res <- NE.some (parseRuleCondition lhsToRHSParser)
            pure (NegatedConjunction res)
        )
        <?> "Negated conjunction"
    )
    <|> ((NegatedConjunction <$> fmap ((:| [])) (lexeme $ parsePositiveCondition (Just ParsingOrdinaryPattern)) <?> "Single negated condition"))

parseNegativeCondition :: (Members '[ObjectIdentityMap (WME' a)] r) => Parser r Text -> Parser r RuleCondition
parseNegativeCondition lhsToRHSParser =
  ( ( (notFollowedBy lhsToRHSParser) *> parseExactly "-" -- <* notFollowedBy (try $ parseExactly "-")
    )
      *> (parseNegativeConditionInterior lhsToRHSParser)
  )
    <?> "Negative condition"

parseStateTestingPattern :: (Members '[ObjectIdentityMap (WME' a)] r) => Parser r Text -> Parser r RuleCondition
parseStateTestingPattern lhsToRHSParser =
  ( try
      ( ( do
            stc@(PositiveCondition stp) <- parsePositiveCondition (Just ParsingStateTestPattern)

            guard $ has (patternObject . _APatternVariable . _1 . _StatePatternVariable) stp
            guard $ hasn't (patternObject . predicate . _Just) stp
            guard $ hasn't (patternAttribute . predicate . _Just) stp
            guard $ hasn't (patternValue . predicate . _Just) stp
            pure stc
        )
          <?> "State testing pattern"
      )
  )
    <|> ( errorWhenParses
            (ParsingRule (ParsingRuleLHS (Just ParsingStateTestPattern) Nothing))
            (parseNegativeCondition lhsToRHSParser)
            ( \w -> Just (ParsingError w Nothing (RuleLHSStateTestPatternIsntPositive ^. re _RuleParseError))
            )
        )
    <|> ( errorWhenParses
            (ParsingRule (ParsingRuleLHS (Just ParsingStateTestPattern) Nothing))
            ( do
                stc@(PositiveCondition stp) <- parsePositiveCondition (Just ParsingStateTestPattern)
                guard $ (has (patternObject . predicate . _Just) stp) || (has (patternAttribute . predicate . _Just) stp) || (has (patternValue . predicate . _Just) stp)
                pure stc
            )
            ( \w -> Just (ParsingError w Nothing (RuleLHSStateTestPatternHasConstraints ^. re _RuleParseError))
            )
        )
    <|> ( errorWhenParses
            (ParsingRule (ParsingRuleLHS (Just ParsingStateTestPattern) Nothing))
            ( do
                PositiveCondition stp <- parsePositiveCondition (Just ParsingStateTestPattern)
                guard $ hasn't (patternObject . _APatternVariable . _1 . _StatePatternVariable) stp
            )
            (\w -> Just (ParsingError w Nothing (RuleLHSNoStateTestPattern ^. re _RuleParseError)))
        )

{-# INLINEABLE parseRuleLHS #-}
parseRuleLHS :: (Members '[ObjectIdentityMap (WME' a)] r) => Parser r Text -> Parser r RuleLHSWithPlaceholders
parseRuleLHS lhsToRHSParser =
  ( ( do
        stp <- lexeme (parseStateTestingPattern lhsToRHSParser)
        remaining <- manyTill (parseRuleCondition lhsToRHSParser) lhsToRHSParser
        --       let subsumed = subsumeDominated (stp' : remaining')

        pure $ RuleLHS' (stp :| remaining) -- <?> "Rule LHS"
    )
  )
    <|> ( errorWhenParses
            (ParsingRule (ParsingRuleLHS Nothing Nothing))
            (lhsToRHSParser)
            ( \w -> Just (ParsingError w Nothing (RuleLHSEmpty ^. re _RuleParseError))
            )
        )

{-# INLINEABLE parseRuleLHSWithPlaceholders #-}
parseRuleLHSWithPlaceholders :: forall agent metadata r. 
  (HasTracer agent,
             Members '[ObjectIdentityMap (WME' metadata), Reader agent,
                                                Log, Resource, OpenTelemetryContext, OpenTelemetrySpan] r) =>
  -- | A parser to detect when we have finished parsing the rule LHS.
  Parser r Text ->
  Parser r (RuleLHSInfo, RuleLHSWithPlaceholders)
parseRuleLHSWithPlaceholders lhsToRHSParser = do
  lhs <- makeVariablesTermLocal <$>  (parseRuleLHS lhsToRHSParser)
  -- Use monadic binding for automatic parser failure
  start <- getSourcePos
  rule' <- lift (runError . runNonDetMaybe $ elaborateRule @agent (RuleIdentifier arbitraryTestIdentifier) "" lhs (RuleRHS' mempty) emptySubstitution)
  end <- getSourcePos
  case rule' of
    Right (Just rule) ->   pure (rule ^. ruleInfo . ruleLHSInfo, lhs)
    Right Nothing ->                customFailure $  (ParsingError (ParsingRule RuleSemanticAnalysis) (Just (start, end)) (UnknownRuleSemanticsError ^. re (_RuleParsingError . _InvalidRuleSemantics)))
    Left e -> do
               customFailure $  (ParsingError (ParsingRule RuleSemanticAnalysis) (Just (start, end)) (e ^. re (_RuleParsingError . _InvalidRuleSemantics)))

{-# INLINEABLE parseRuleRHS #-}
parseRuleRHS :: (Members '[ObjectIdentityMap (WME' a)] r) => Parser r RuleRHSWithPlaceholders
parseRuleRHS =
  try (RuleRHS' <$> fromList <$> some (lexeme $ parseRuleAction Df1.Notice))
    <|> ( errorWhenParses
            (ParsingRule (ParsingRuleRHS Nothing))
            ( do
                _ <- optional sc
                verbatim "}"
            )
            ( \w -> Just (ParsingError w Nothing (RuleRHSEmpty ^. re _RuleParseError))
            )
        )

parseRuleRHSWithPlaceholders :: (Members '[ObjectIdentityMap (WME' a)] r) => Parser r (RuleRHSInfo, RuleRHSWithPlaceholders)
parseRuleRHSWithPlaceholders = do
  rhs@(RuleRHS' actions) <- parseRuleRHS
  let info = computeRuleRHSInfo actions
  pure (info, rhs)


      


             
parseRule :: forall agent  metadata r. (HasTracer agent, Members '[ObjectIdentityMap (WME' metadata), Reader agent, Log, Resource, OpenTelemetryContext, OpenTelemetrySpan] r) => Parser r Rule
parseRule = (between (verbatim "{") (verbatim "}") $ do
  let lhsToRHSParser = (verbatim "-->" <?> "-->")
  desc' <- optional (lexeme (stringLiteral <?> "Quoted rule description"))
  let desc = maybe "Unnamed rule" render desc'
  (lhs) <- parseRuleLHS lhsToRHSParser
  (rhs) <- parseRuleRHS
  bindings <- parseRuleBindings
  ident <- lift $ RuleIdentifier <$> newGlobalIdentifier
  elaboration <- lift . runError . runNonDetMaybe $ elaborateRule ident desc lhs rhs bindings
  case elaboration of
    Left err -> customFailure (ParsingError (ParsingRule RuleSemanticAnalysis) Nothing (err ^. re (_RuleParsingError . _InvalidRuleSemantics)))
    Right Nothing -> customFailure (ParsingError (ParsingRule RuleSemanticAnalysis) Nothing (UnknownRuleSemanticsError ^. re (_RuleParsingError . _InvalidRuleSemantics)))
    Right (Just rule) -> do
      lift $ addAttributeWithRaw "rule.id" ident
      pure rule) -- <?> "production rule  sandwiched between opening and closing curly braces"

parseRuleBindings :: Parser r (SubstitutionLattice (RuleExpr Variable))
parseRuleBindings = do
  bindingsPresent <- optional (lexeme "where")
  case bindingsPresent of
    Nothing -> pure emptySubstitution
    Just _ -> do
      list <- sepBy1 parseRuleBinding ((char ',') <* optional sc)
      pure $ constructSubstitution list

parseRuleBinding :: Parser r (Variable, RuleExpr Variable)
parseRuleBinding = do
  var <- lexeme (parseVariable ParsingExpression)
  _ <- (try $ (keyword "|=>" <?> "|=>")) <|> keyword "⤇"
  expr <- parseRuleExpression
  pure (var, expr)
