{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LinearTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NoFieldSelectors #-}

module Core.Patterns (patternMatchesWME, unifyObject, unifyAttribute, unifyValue, unifyPreference, sanitisePatternWithNewVariables) where


import Control.Monad.Zip
    
import Control.Lens.Extras
import Core.CoreTypes
import Core.Patterns.PatternSyntax
import Control.Lens
import Prelude
import Core.Variable.Substitution
import Core.Variable.AlphaEquivalence
import Data.Foldable
import Control.Monad    
import Polysemy
import Core.Identifiers
import Control.Applicative
import Core.Variable
import Data.Sequence (Seq)
import Core.Identifiers.IDGen    
import Data.Interned

import qualified Data.Sequence as Seq
import Data.Sequence.Lens
import Text.RE.TDFA hiding (re)
import Text.Regex.Lens

data ComparisonSatisfaction = ComparisonFailed | ComparisonSatisfiesWith (Seq ComparisonTest) (Seq Value)

instance Semigroup ComparisonSatisfaction where
  {-# INLINEABLE (<>) #-}
  (ComparisonSatisfiesWith la lb) <> (ComparisonSatisfiesWith ra rb) = ComparisonSatisfiesWith (la <> ra) (lb <> rb)
  _ <> _ = ComparisonFailed

instance Monoid ComparisonSatisfaction where
  mempty = ComparisonSatisfiesWith mempty mempty

{-# INLINEABLE performComparisonTest #-}
performComparisonTest :: Substitution Value -> ComparisonTest -> Value -> ComparisonSatisfaction
performComparisonTest _ p@(EqualTo (CompareAgainstValue l)) v
  | l == v =
      ComparisonSatisfiesWith (Seq.singleton p) (Seq.singleton v)
performComparisonTest _ p@(NotEqualTo (CompareAgainstValue l)) v
  | l /= v =
      ComparisonSatisfiesWith (Seq.singleton p) (Seq.singleton v)
performComparisonTest _ p@(LessThan (CompareAgainstValue (Constant r))) c@(Constant l)
  | asRational l < asRational r =
      ComparisonSatisfiesWith (Seq.singleton p) (Seq.singleton c)
performComparisonTest _ p@(LessThanOrEqualTo (CompareAgainstValue (Constant r))) c@(Constant l)
  | asRational l <= asRational r =
      ComparisonSatisfiesWith (Seq.singleton p) (Seq.singleton c)
performComparisonTest _ p@(GreaterThan (CompareAgainstValue (Constant r))) c@(Constant l)
  | asRational l > asRational r =
      ComparisonSatisfiesWith (Seq.singleton p) (Seq.singleton c)
performComparisonTest _ p@(GreaterThanOrEqualTo (CompareAgainstValue (Constant r))) c@(Constant l)
  | asRational l >= asRational r =
      ComparisonSatisfiesWith (Seq.singleton p) (Seq.singleton c)
performComparisonTest sub p val
  | Just var <- p ^? comparingTo . _CompareAgainstVariable,
    Just varVal <- sub ^. at var =
      performComparisonTest sub (p & comparingTo .~ CompareAgainstValue varVal) val
--  Just var <- p ^? comparingTo . _CompareAgainstVariable,
--   Nothing <- sub ^. at var -- When the substitution doesn't contain a binding, ignore it.
--   =
--     ComparisonSatisfiesWith (Seq.singleton p) Seq.empty
performComparisonTest _ _ _ = ComparisonFailed

{-# INLINEABLE valueSatisfiesPredicate #-}
valueSatisfiesPredicate :: Substitution Value -> PatternPredicate -> Value -> Satisfaction
valueSatisfiesPredicate sub (AComparisonTest tests) v
  | tested <- foldMap (\t -> performComparisonTest sub t v) tests =
      case tested of
        ComparisonFailed -> Fails
        ComparisonSatisfiesWith a b -> SatisfiesWith (AComparisonTest a) b
valueSatisfiesPredicate _ p@(ARegexTest (RegexWithText r _ _)) v
  | Just t <- v ^? _Symbol,
    has (regex (reRegex r)) (unintern t) -- TODO see if this needs to be optimised
    =
      let matches = seqOf (regex (reRegex r) . matchedString . to intern . re _Symbol) (unintern t)
       in SatisfiesWith p matches -- TODO could probably also match against numeric stuff
valueSatisfiesPredicate _ _ _ = Fails

{-# INLINEABLE patternConditionIsSatisfied #-}
patternConditionIsSatisfied :: SubstitutionLattice Value -> Maybe PatternPredicate -> Value -> Bool
patternConditionIsSatisfied _ Nothing _ = True
patternConditionIsSatisfied sub' (Just p) v
  | Just sub <- sub'^? __Substitution, _SatisfiesWith `is` (valueSatisfiesPredicate sub p v) = True
  | otherwise = False

-- instance AsPatternVariable PatternCondition

-- {-# INLINE _PatternVariable #-}

--  _PatternVariable = _APatternVariable <. _1

{-# INLINEABLE unifyObject #-}
unifyObject :: ( WMEIsState (WME' a)) => SubstitutionLattice Value -> PatternCondition -> WME' a ->  SubstitutionLattice Value
unifyObject _ DontCareCondition _ = emptySubstitution
unifyObject sub (APatternVariable (PlainPatternVariable var) t) wme
  | patternConditionIsSatisfied sub t (wme ^. object . re (_SymbolicValue . _ObjectIdentity)) =
      singleton var (wme ^. object . re (_SymbolicValue . _ObjectIdentity))
unifyObject sub (APatternVariable (StatePatternVariable var) t) wme
  | patternConditionIsSatisfied sub t (wme ^. object . re (_SymbolicValue . _ObjectIdentity)),
    wme ^. isState = 
      singleton var (wme ^. object . re (_SymbolicValue . _ObjectIdentity))
unifyObject _ (AnObjectIdentity oid) wme | oid == wme ^. object = emptySubstitution
unifyObject _ _ _ = incompatibleSubstitution

{-# INLINEABLE unifyAttribute #-}
unifyAttribute :: SubstitutionLattice Value -> PatternCondition -> WME' a -> SubstitutionLattice Value
unifyAttribute _ DontCareCondition _ = emptySubstitution
unifyAttribute _ (AConstantEqualityTest (PatternSymbol s)) wme
  | Just sym <- wme ^? attribute . _Symbol,
    sym == s = emptySubstitution
-- Only plain variables can match on attributes.
unifyAttribute sub (APatternVariable v t) wme
  | Just var <- v ^? _PlainPatternVariable,
    Just i <- wme ^? attribute . _SymbolicIdentity,
    patternConditionIsSatisfied sub t (wme ^. attribute . re _SymbolicValue) =
      singleton var (i ^. re _Identifier) 
unifyAttribute sub (APatternVariable v t) wme
  | Just var <- v ^? _PlainPatternVariable,
    Just s <- wme ^? attribute . _Symbol,
    patternConditionIsSatisfied sub t (wme ^. attribute . re _SymbolicValue) =
       singleton var (s ^. re _Symbol)
-- Attributes can't be non-symbolic constants.
unifyAttribute _ (AConstantEqualityTest (PatternConstant _)) _ = incompatibleSubstitution
unifyAttribute _ _ _ = incompatibleSubstitution

{-# INLINEABLE unifyValue #-}
unifyValue :: SubstitutionLattice Value -> PatternCondition -> WME' a -> SubstitutionLattice Value
unifyValue _ DontCareCondition _ = emptySubstitution
unifyValue _ (AConstantEqualityTest (PatternSymbol s)) wme
  | Just sym <- wme ^? value . _Symbol,
    sym == s =
      emptySubstitution
unifyValue _ (AConstantEqualityTest (PatternConstant c)) wme
  | Just constant <- wme ^? value . _ConstantValue,
    c == constant =
       emptySubstitution
-- Only plain variables can match on values.
unifyValue sub (APatternVariable v t) wme
  | Just var <- v ^? _PlainPatternVariable,
    val <- wme ^. value,
    patternConditionIsSatisfied sub t val =
      singleton var val
unifyValue _ _ _ = incompatibleSubstitution

{-# INLINEABLE unifyPreference #-}
unifyPreference ::
  forall a.
  (HasMaybeWMPreference (WME' a) ObjectIdentifier) =>
  SubstitutionLattice Value ->
  WMPreferencePattern ->
  WME' a ->
  SubstitutionLattice Value
unifyPreference _ Don'tCareAboutPreference _ = trivialSubstitution
unifyPreference _ MustNotHavePreference wme | hasn't (preference @(WME' a) @ObjectIdentifier . _Just) wme =  trivialSubstitution
unifyPreference _ MustHaveAPreference wme | has (preference @(WME' a) @ObjectIdentifier . _Just) wme = trivialSubstitution
unifyPreference sub (WithAPreference pattPref) wme -- preference @(WME' a) @prefType . _Just . folded . _ObjectIdentifier
  | Just wmePref <- wme ^. preference @(WME' a),
    Just (subbed :: WMPreference ObjectIdentifier) <- traverse (\(var :: Variable) -> sub ^? (at var) . _Just . _ObjectIdentifier) pattPref,
    patVar' <- pattPref ^? variables,
    wmePrefOID <- wmePref ^? folded,
    subbed == wmePref =
      case (patVar', wmePrefOID) of
        (Nothing, Nothing) -> emptySubstitution
        (Nothing, _) -> incompatibleSubstitution
        (_, Nothing) -> incompatibleSubstitution
        (Just patVar, Just oid) -> singleton patVar  (oid ^. re _ObjectIdentifier)
unifyPreference _ _ _ = incompatibleSubstitution

{-# INLINEABLE patternMatchesWME #-}
patternMatchesWME ::
  forall f a.
  (Alternative f, HasMaybeWMPreference (WME' a) ObjectIdentifier) =>
  WMEIsState (WME' a) =>
  SubstitutionLattice Value ->
  WMEPattern ->
  WME' a ->
 f (WMEPatternMatch' a)
patternMatchesWME sub pat wme
  | o <- unifyObject sub (pat ^. patternObject) wme, o /= incompatibleSubstitution,
    a <- unifyAttribute sub (pat ^. patternAttribute) wme, a /= incompatibleSubstitution,
    v <- unifyValue sub (pat ^. patternValue) wme, v /= incompatibleSubstitution,
    pref <- unifyPreference sub (pat ^. patternPreference) wme, pref /= incompatibleSubstitution,
    Just sub' <- (mergeSubstitutions [sub, o, a, v, pref])^?__Substitution =
      pure $ WMEPatternMatch pat sub' wme
  | otherwise = empty




--this needs to not be called in ReteBuilder, but as part of addRules; followed by makeVariableSWithSameNameReferToSameID.                

{-# INLINEABLE sanitisePatternWithNewVariables #-}
sanitisePatternWithNewVariables :: (Members '[IDGen, VariableMapping Variable] r) => WMEPattern -> Sem r (WMEPattern)
sanitisePatternWithNewVariables pat = do
  let patVars = pat^.. variables--patternBindingVariables
  newVars <- generaliseVariables patVars
  let subTo = constructSubstitution $ filter (\(x,y) -> x /= y)
              $ mzip patVars newVars 
  forM_ (mzip  patVars newVars ) \(x,y) -> when (x /= y) do
                                          mapVariableTo x y
  pure (renameVariables' pat subTo)
                  
