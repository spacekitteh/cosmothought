{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE QuasiQuotes #-}
{-# OPTIONS_GHC -Wwarn=orphans #-}
module Core.Identifiers where
import Numeric.Natural

import Core.Utils.Rendering.CoreDoc
import Control.Lens
import Prettyprinter.Render.Util.StackMachine
import Data.Hashable
import Data.Interned

import Data.Text as T

import GHC.Generics (Generic)
import Polysemy (makeSem)
import Prettyprinter
import qualified Data.String as DS
import qualified StmContainers.Set as STM
import OpenTelemetry.Trace.Core (ToPrimitiveAttribute(..), ToAttribute(..))
import Text.URI (URI, mkURI, relativeTo)


import qualified Text.URI.QQ as QQ
import Prelude
import Language.Haskell.TH.Quote
import Core.Identifiers.IdentifierType
import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import Core.Utils.MetaTypes

baseCosmoThoughtURI :: URI
baseCosmoThoughtURI = [QQ.uri|urn:cosmothought/|]




--------------------------------------------------------------------------------
-- ByIdentifier for deriving via
--------------------------------------------------------------------------------

instance (HasIdentifier a) => ToAnnotation (ByIdentifier a) where
    asAnnotation (ByIdentifier a) = asAnnotation (a^.identifier)
instance (HasIdentifier a) => Renderable (ByIdentifier a) where
    render (ByIdentifier a) = render (a^.identifier)



deriving via (ByRenderable Identifier) instance ToPrimitiveAttribute Identifier
instance ToAttribute Identifier
         

--------------------------------------------------------------------------------
-- WMEs
--------------------------------------------------------------------------------

newtype WMEIdentifier = WMEIdentifier {_rawWMEIdentifier :: Identifier}
  deriving stock (Eq, Ord, Generic, Lift)
  deriving anyclass (Hashable)

makeClassy ''WMEIdentifier

instance HasIdentifier WMEIdentifier where
  {-# INLINE identifier #-}
  identifier = rawWMEIdentifier

deriving via ByIdentifier WMEIdentifier instance Pretty WMEIdentifier

deriving via ByIdentifier WMEIdentifier instance Show WMEIdentifier

--------------------------------------------------------------------------------
-- Values
--------------------------------------------------------------------------------

{-# INLINEABLE symbolNameToURIIdentifier #-}
symbolNameToURIIdentifier :: SymbolName -> Maybe Identifier
symbolNameToURIIdentifier sym = do
  sub <- mkURI (symbolNameToText sym)
  res <- relativeTo sub baseCosmoThoughtURI
  pure $ AURI res
  
ident :: QuasiQuoter
ident = QuasiQuoter {quoteExp = \str ->
                                case (symbolNameToURIIdentifier . textToSymbolName . pack) str of
                                  Nothing -> fail "Couldn't parse identifier"
                                  Just x -> lift x,
                     quotePat =  \str ->
                                case (symbolNameToURIIdentifier . textToSymbolName . pack) str of
                                  Nothing -> fail "Couldn't parse identifier"
                                  Just x -> appE [|(==)|] (lift x) `viewP` [p|True|],
                     quoteType = undefined,
                     quoteDec = undefined}


data ObjectIdentifier = ObjectIdentifier {_symbol' :: SymbolName, _identity' :: Identifier}
  deriving stock (Generic, Lift)

makeClassy ''ObjectIdentifier
instance HasIdentifier ObjectIdentifier where
  {-# INLINE identifier #-}
  identifier = identity'


           
deriving via ByIdentifier ObjectIdentifier instance Eq ObjectIdentifier
deriving via ByIdentifier ObjectIdentifier instance Ord ObjectIdentifier
deriving via ByIdentifier ObjectIdentifier instance Hashable ObjectIdentifier
         
class AsObjectIdentifier c where
    _ObjectIdentifier :: Prism' c ObjectIdentifier

instance AsObjectIdentifier ObjectIdentifier where
    {-# INLINE _ObjectIdentifier #-}
    _ObjectIdentifier = id
           

--annotateOID :: ObjectIdentifier -> Doc Identifier
--annotateOID o@ObjectIdentifier{..} = annotate _identity' (pretty o)





data ObjectIdentityMap object m a where
  LookupObjectIDByName :: SymbolName -> ObjectIdentityMap object m ObjectIdentifier
  EntireObject :: ObjectIdentifier -> ObjectIdentityMap object m (Maybe (STM.Set object))
  NumberOfReferences :: ObjectIdentifier -> ObjectIdentityMap object m (Maybe Natural)

makeSem ''ObjectIdentityMap
instance Renderable ObjectIdentifier where
  render ObjectIdentifier {..} = annotate (IDAnnotation _identity') $ (enclose pipe pipe (render . unintern $ _symbol'))-- <+> parens (pretty _identity')
instance Show ObjectIdentifier where
  show o = show $ renderSimplyDecorated id (\_ -> mempty) (\case
                                                                (IDAnnotation i) -> DS.fromString $ "<" ++ (show . render $ i) ++ "/>"
                                                                _ -> ""
                                                          )
           (layoutPretty defaultLayoutOptions (render o))
class HasExtraObjectIdentifiers a where
    extraObjectIdentifiers :: Traversal' a ObjectIdentifier
--    extraObjectIdentifiers _ a = pure a
instance HasExtraObjectIdentifiers ObjectIdentifier where
    {-# INLINE extraObjectIdentifiers #-}
    extraObjectIdentifiers = id
instance ToPrimitiveAttribute ObjectIdentifier where
    {-# INLINABLE toPrimitiveAttribute #-}

    toPrimitiveAttribute  = toPrimitiveAttribute . T.pack . show
instance ToAttribute ObjectIdentifier
--------------------------------------------------------------------------------
-- Tokens
--------------------------------------------------------------------------------

newtype TokenIdentifier = TokenIdentifier {_rawTokenIdentifier :: Identifier}
  deriving stock (Eq, Ord, Generic)
  deriving anyclass (Hashable)
  deriving newtype (Show, Pretty)

makeLenses ''TokenIdentifier

instance HasIdentifier TokenIdentifier where
  {-# INLINE identifier #-}
  identifier :: Lens' TokenIdentifier Identifier
  identifier = rawTokenIdentifier

--------------------------------------------------------------------------------
-- Rete nodes
--------------------------------------------------------------------------------

newtype NodeIdentifier = NodeIdentifier {_rawNodeIdentifier :: Identifier}
  deriving stock (Eq, Ord, Generic)
  deriving newtype (Show, Hashable, Pretty)

makeLenses ''NodeIdentifier

instance HasIdentifier NodeIdentifier where
  identifier :: Lens' NodeIdentifier Identifier
  {-# INLINE identifier #-}
  identifier = rawNodeIdentifier

--------------------------------------------------------------------------------
-- Rules
--------------------------------------------------------------------------------
newtype RuleIdentifier = RuleIdentifier {_rawRuleIdentifier :: Identifier}
  deriving stock (Generic)
  deriving newtype (Eq, Ord, Show, Hashable, Pretty)

makeClassy ''RuleIdentifier

instance HasIdentifier RuleIdentifier where
  identifier :: Lens' RuleIdentifier Identifier
  {-# INLINE identifier #-}
  identifier = rawRuleIdentifier

newtype InstantiationIdentifier = InstantiationIdentifier {_rawInstantiationIdentifier :: Identifier}
  deriving stock (Generic)
  deriving newtype (Eq, Ord, Show, Hashable, Pretty)

makeClassy ''InstantiationIdentifier

instance HasIdentifier InstantiationIdentifier where
  identifier :: Lens' InstantiationIdentifier Identifier
  {-# INLINE identifier #-}
  identifier = rawInstantiationIdentifier




--instance Show ObjectIdentifier where
--  show o = show $ renderSimplyDecorated id (\_ -> mempty) (\i -> DS.fromString $ "</" ++ (show . render $ i) ++ ">") (layoutPretty defaultLayoutOptions (annotateOID o))
         

deriving via ByIdentifier WMEIdentifier instance Renderable WMEIdentifier
deriving via ByIdentifier TokenIdentifier instance Renderable TokenIdentifier
deriving via ByIdentifier RuleIdentifier instance Renderable RuleIdentifier
deriving via ByIdentifier NodeIdentifier instance Renderable NodeIdentifier
deriving via ByIdentifier InstantiationIdentifier instance Renderable InstantiationIdentifier
deriving via (ByRenderable RuleIdentifier) instance ToPrimitiveAttribute RuleIdentifier
instance ToAttribute RuleIdentifier     
deriving via ByIdentifier NodeIdentifier instance ToAnnotation NodeIdentifier
deriving via ByIdentifier ObjectIdentifier instance ToAnnotation ObjectIdentifier
deriving via ByIdentifier WMEIdentifier instance ToAnnotation WMEIdentifier
deriving via ByIdentifier TokenIdentifier instance ToAnnotation TokenIdentifier
deriving via ByIdentifier RuleIdentifier instance ToAnnotation RuleIdentifier

deriving via ByIdentifier InstantiationIdentifier instance ToAnnotation InstantiationIdentifier

                          
--------------------------------------------------------------------------------
-- States
--------------------------------------------------------------------------------
data StateIdentifier =
  TopState {_stateObject :: ObjectIdentifier}
  | SubStateIdentifier {_parentState :: StateIdentifier, _stateObject :: ObjectIdentifier}
  deriving stock (Eq, Generic, Show, Ord)
  deriving anyclass (Hashable)

class HasCreatingState obj where
    creatingState :: Lens' obj StateIdentifier

instance  Renderable StateIdentifier where
  render (TopState _ ) = "Top-level state"
  render (SubStateIdentifier parent oid) = "OID:" <+> render oid <+> "Parent:" <+> render parent    

deriving via (ByRenderable StateIdentifier) instance ToPrimitiveAttribute StateIdentifier
instance ToAttribute StateIdentifier
                                                    

makeLenses ''StateIdentifier
deriving via ByIdentifier StateIdentifier instance ToAnnotation StateIdentifier           
instance HasIdentifier StateIdentifier where
    {-# INLINE identifier #-}
    identifier = fusing (stateObject.identifier)
            
