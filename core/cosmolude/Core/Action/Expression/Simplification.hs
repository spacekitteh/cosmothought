module Core.Action.Expression.Simplification (simplify) where

import Core.Action.Expression
import Cosmolude hiding (Log)
import Prelude

{-# INLINEABLE constantPropagation #-}
constantPropagation :: (Eq a) => ArithExpr a -> Maybe (ArithExpr a)
constantPropagation (Neg (Num a)) = Just $ Num (-a)
constantPropagation ((Num a) :+: (Num b)) = Just $  Num (a + b)
constantPropagation ((Num a) :-: (Num b)) = Just $ Num (a - b)
constantPropagation (a :-: b) | a == b =  Just $ Num 0
constantPropagation ((Num a) :*: (Num b)) = Just $ Num (a * b)
constantPropagation ((Num a) :/: (Num b)) = Just $ Num (a / b)
--constantPropagation (a :/: b) | a == b && b /= (Num 0) = Just $ Num 1
constantPropagation (a :+: (Num 0)) = Just $ a
constantPropagation ((Num 0) :+: b) = Just $ b
constantPropagation (_ :*: (Num 0)) = Just $ Num 0
constantPropagation ((Num 0) :*: _) = Just $ Num 0
constantPropagation (a :*: (Num 1)) = Just $ a
constantPropagation ((Num 1) :*: b)= Just $ b
constantPropagation (Sin (Num a)) = Just $ Num (sin a)
constantPropagation (Cos (Num a)) = Just $ Num (cos a)
constantPropagation (Tan (Num a)) = Just $ Num (tan a)
constantPropagation (Sinh (Num a)) = Just $ Num (sinh a)
constantPropagation (Cosh (Num a)) = Just $ Num (cosh a)
constantPropagation (Tanh (Num a)) = Just $ Num (tanh a)
constantPropagation (Asin (Num a)) = Just $ Num (asin a)
constantPropagation (Acos (Num a)) = Just $ Num (acos a)
constantPropagation (Atan (Num a)) = Just $ Num (atan a)
constantPropagation (Asinh (Num a)) = Just $ Num (asinh a)
constantPropagation (Acosh (Num a)) = Just $ Num (acosh a)
constantPropagation (Atanh (Num a)) = Just $ Num (atanh a)
constantPropagation (Exp (Num a)) = Just $ Num (exp a)
constantPropagation (Log (Num a)) = Just $ Num (Prelude.log a)
constantPropagation _ = Nothing

findNoops :: (Eq a) => ArithExpr a -> Maybe (ArithExpr a)
findNoops (Exp (Log a)) = Just a
findNoops (Log (Exp a)) = Just a
findNoops (a :*: (Sign b)) | a == b = Just a
findNoops _ = Nothing

associate :: Eq a => ArithExpr a -> Maybe (ArithExpr a)
associate (a :+: (b :+: c)) = Just $ (a :+: b) :+: c
associate (a :-: (b :+: c)) = Just $ (a :-: b) :-: c
associate (a :-: (b :-: c)) = Just $ (a :-: b) :+: c
associate (a :*: (b :*: c)) = Just $ (a :*: b) :*: c
associate (a :/: (b :*: c)) = Just $ (a :/: b) :/: c
associate (a :/: (b :/: c)) = Just $ (a :/: b) :*: c
associate _ = Nothing

unDistribute :: Eq a => ArithExpr a -> Maybe (ArithExpr a)
unDistribute ((a :*: b) :+: (a' :*: c)) | a == a' =  Just $ a :*: (b :+: c)
unDistribute ((a :*: b) :-: (a' :*: c)) | a == a' =  Just $ a :*: (b :-: c)
unDistribute _ = Nothing


rewriteRules :: Eq a => ArithExpr a -> Maybe (ArithExpr a)
rewriteRules a = choice  $ fmap ($ a) [
  findNoops,
  constantPropagation,
  unDistribute,
  associate
  ]

simplify :: Eq a => ArithExpr a -> ArithExpr a
simplify = rewrite rewriteRules
