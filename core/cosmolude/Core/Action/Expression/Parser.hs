module Core.Action.Expression.Parser where

import Cosmolude hiding (Log)
import Core.CoreTypes
import Prelude hiding ((.))
import Control.Monad.Combinators.Expr
import Data.Text
import Core.Action.Expression
import Core.Action.Expression.Simplification


binary :: (Members '[IDGen, Transactional] r) => Text -> (a -> a -> a) -> Operator (ParsecT ParsingError Text (Sem r)) a
binary  name f = InfixL  (f <$ keyword name)

prefix, postfix :: (Members '[IDGen, Transactional] r) => Text -> (a -> a) -> Operator (ParsecT ParsingError Text (Sem r))  a
prefix  name f = Prefix  (f <$ keyword name)
postfix name f = Postfix (f <$ keyword name)


arithOperatorTable :: (Members '[IDGen, Transactional] r) => [[Operator (ParsecT ParsingError Text (Sem r)) (ArithExpr a)]]
arithOperatorTable = [
  [
--   prefix' "-" Neg,
--   prefix' "+" Prelude.id,
   prefix "abs" Abs,
   prefix "sign" Sign
  ],
  [
    prefix "sinh" Sinh,
    prefix "cosh" Cosh,
    prefix "tanh" Tanh,
    prefix "sin" Sin,
    prefix "cos" Cos,
    prefix "tan" Tan,
    prefix "asinh" Asinh,
    prefix "acosh" Acosh,
    prefix "atanh" Atanh,
    prefix "asin" Asin,
    prefix "acos" Acos,
    prefix "atan" Atan,

    prefix "log" Log,
    prefix "exp" Exp
  ],
  [
    binary "*" (:*:),
    binary "/" (:/:)
  ],
  [
    binary "+" (:+:),
    binary "-" (:-:)
  ]
  
    
                ]

pArithTerm :: Parser r (ArithExpr Variable)
pArithTerm = choice
  [
    ArithVar <$> lexeme (parseVariable ParsingExpression),
    Num <$> lexeme parseConstantValue,
    parseParens parseArithExpr

  ]

parseArithExpr :: Parser r (ArithExpr Variable)
parseArithExpr = simplify <$> makeExprParser pArithTerm arithOperatorTable


pSymTerm :: Parser r (SymExpr Variable)
pSymTerm = choice [
  SymVar <$> lexeme (parseVariable ParsingExpression),
  SymText <$> lexeme parseSymbolName,
  parseParens parseSymExpr
  ]

symOperatorTable  :: (Members '[IDGen, Transactional] r) => [[Operator (ParsecT ParsingError Text (Sem r)) (SymExpr a)]]
symOperatorTable =
  [
    [
      binary "<>" (:<>:)
    ]
  ]
  
parseSymExpr :: Parser r (SymExpr Variable)
parseSymExpr = makeExprParser pSymTerm symOperatorTable
  
parseRuleExpression :: Parser r (RuleExpr Variable)
parseRuleExpression = try (ArithmeticExpression <$> parseArithExpr ) <|> (SymbolicExpression <$> parseSymExpr)

