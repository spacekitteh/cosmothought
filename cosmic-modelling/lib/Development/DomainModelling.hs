module Development.DomainModelling (module Polysemy,
                              module Polysemy.Methodology,
                              module Polysemy.Tagged,
                              module Polysemy.Input,
                              module Polysemy.Output,
                              module Data.Functor,
                              module Control.Monad,
                              module Data.Function,
                              module Data.Bool,
                              module Data.Maybe,
                              module Control.Applicative,
                              module Data.Either,
                              module Data.Monoid,
                              module Prelude,
                              module Development.DomainModelling -- DecisionProcedureOn, decide, decision, ChoiceProcedureOn, choose, while, adaptToInOutMonoid,adaptToInOut', splitInput, combineOutput, combineInput, splitOutput, adaptToMonoidalOutput, adaptToInOut
                              )where




import Polysemy
--import Polysemy.Uncontrolled
import Polysemy.Methodology
import Polysemy.Tagged
import Polysemy.Input
import Polysemy.Output
import Data.Bool
import Data.Maybe
import Data.Function (($), (.))
import Control.Monad
import Data.Functor
import Control.Applicative
import Data.Monoid
import Data.Either
import Prelude (fst, snd, id)

data DecisionProcedureOn t m a where
  Decide :: t -> DecisionProcedureOn t m Bool

makeSem ''DecisionProcedureOn

{-# INLINE decision #-}
decision :: Members '[Methodology t Bool] r =>  InterpreterFor (DecisionProcedureOn t) r
decision = interpret \case
  Decide t -> process t

data ChoiceProcedureOn collection individual m a where
  Choose :: collection -> ChoiceProcedureOn collection individual m (Maybe individual)

makeSem ''ChoiceProcedureOn

{-# INLINE while #-}
while :: forall condition inputSource inputQueue workInput workOutput output r.  Members '[
                             Tagged inputSource (Input inputQueue),
                             Tagged condition (Methodology inputQueue (Maybe workInput)),
                             Tagged output (Output workOutput)
                           ] r      =>(workInput -> Sem r workOutput) -> Sem r ()
while doWork = do
  workQueue <- tag @inputSource input
  possibleWork <- tag @condition $ process workQueue
  case possibleWork of
    Nothing -> pure ()
    Just work -> do
      result <- doWork work
      tag @output $ output result
      while doWork



{-# INLINE adaptToInOut' #-}
adaptToInOut' :: Members '[Input input', Output output', Methodology (input,input') (output,output')] r => InterpreterFor (Methodology input output) r
adaptToInOut' = interpret \case
  Process i -> do
    i' <- input
    (o,o') <- process (i,i')
    output o'
    pure o

{-# INLINE adaptToInOutMonoid #-}      
adaptToInOutMonoid :: (Monoid output', Members '[Input input, Output output] r) => (output -> output') -> Sem (Input input : Output output : r) () -> InterpreterFor (Methodology input output') r
adaptToInOutMonoid toMonoid action = interpret \case
  Process i -> do
    (o,()) <- runOutputMonoid toMonoid $ runInputConst i action
    pure o
{-# INLINE adaptToMonoidalOutput #-}
adaptToMonoidalOutput ::  (Monoid output, Member (Methodology input output) r) => Sem (Methodology input () : Output output : r) () -> Sem r output
adaptToMonoidalOutput sem = fmap fst $ runOutputMonoid id $ interpret (\(Process i) -> do
    out <- process i
    output out) sem

adaptToInOutTagged :: (Monoid output, Members '[(Input input), ( Output output)] r) =>  Sem (Tagged inputTag (Input input) : Tagged outputTag (Output output) : r) () -> InterpreterFor (Methodology input output) r
adaptToInOutTagged action = adaptToTaggedInOutMonoid id action 

adaptToTaggedInOutMonoid :: (Monoid output', Members '[Input input, Output output] r) => (output -> output') -> Sem (Tagged inputTag (Input input) : Tagged outputTag (Output output) : r) () -> InterpreterFor (Methodology input output') r
adaptToTaggedInOutMonoid toMonoid action = interpret \case
  Process i -> do
    (o,()) <- runOutputMonoid toMonoid $ untag  $ runInputConst i (untag action)
    pure o

adaptToInOut :: (Monoid output, Members '[Input input, Output output] r) => Sem (Input input : Output output : r) () -> InterpreterFor (Methodology input output) r
adaptToInOut = adaptToInOutMonoid id 

adaptTaggedInOut :: forall methodologyTag inputTag outputTag input output r. (Monoid output, Members '[(Input input),  (Output output)] r) => Sem (Tagged inputTag (Input input) : Tagged outputTag (Output output) : r) () -> InterpreterFor (Tagged methodologyTag (Methodology input output)) r
adaptTaggedInOut action = adaptToInOutTagged action . untag @methodologyTag
  --tag @inputTag . tag @outputTag .    adaptToInOut . untag @methodologyTag $ sem

-- contraAdaptToInOut :: forall a b r x . (Members '[Input b, Output a] r)=> Sem (Methodology a b : r) x -> Sem r x
-- contraAdaptToInOut =  runUncontrolledAsInputOutput . runMethodologyAsUncontrolled . raiseUnder 
  

{-# INLINE splitInput #-}
splitInput :: (Members '[Input a, Input b] r) => (a -> b -> Sem r ab) -> InterpreterFor (Input ab) r
splitInput combine = interpret \case
  Input -> do
    a <- input
    b <- input
    ab <- combine a b
    pure ab
{-# INLINE combineOutput #-}
combineOutput :: (Members '[Output a, Output b] r) => (ab -> Sem r (a,b)) -> InterpreterFor (Output ab) r
combineOutput split = interpret \case
  Output ab -> do
    (a,b) <- split ab
    output a
    output b

{-# INLINE splitOutput #-}
splitOutput :: (Monoid a, Monoid b, Members '[Output ab] r) => (a -> b -> Sem r ab) -> InterpretersFor '[Output a, Output b] r
splitOutput combine sem = do 
  (b, (a,x)) <- runOutputMonoid id (runOutputMonoid id sem)
  ab <- combine a b
  output ab
  pure x

{-# INLINE combineInput #-}
combineInput :: (Members '[Input ab] r) => (ab -> Sem r (a,b)) -> InterpretersFor '[Input a, Input b] r
combineInput split sem = do
  ab <- input
  (a,b) <- split ab
  runInputConst b (runInputConst a sem)

  -- concat
  --   [ "[",
  --     giBranch gi,
  --     "@",
  --     giHash gi,
  --     " (",
  --     giCommitDate gi,
  --     ")",
  --     " (",
  --     show (giCommitCount gi),
  --     " commits in HEAD)",
  --     dirty,
  --     "] "
  --   ]
  -- where
  --   dirty
  --     | giDirty gi = " (uncommitted files present)"
  --     | otherwise = ""
  --   gi = $$tGitInfoCwd
