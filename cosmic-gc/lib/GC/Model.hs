module GC.Model where
import Development.DomainModelling
import Data.Foldable
import Prelude


data GarbageCollectionTracerMarking = Untouched | Pending | FullyProcessed
                              deriving stock (Eq, Ord)

    
-- | Given a set of nodes, this determine which of them are to be considered as GC roots.
data GetRoots
-- | Given a node, get the directly reachable nodes from it.
data GetLinks
-- | We have finished processing this node and have removed it from the worklist; we now need to mark it as
-- such.
data MarkNodeAsFullyProcessed
-- | Mark a newly-stumbled-upon node that has yet to be processed.
data MarkNodeAsPending
-- | Sometimes we only want to garbage collect a portion of memory.
data GetNodesToConsider
-- | Mark the node as being unreachable and needs to be deleted.
data ScheduleForDeletion
-- | Check if a node should actually be considered during garbage collection.
data IsNodeUnderConsideration
-- | Fetch a node from the worklist, while not removing it.
data GetNextNodeToProcess
-- | We've stumbled upon this node, so we need to add it to our pending work.
data AddNodeToWorklist
-- | We've processed the node.
data RemoveNodeFromWorklist
-- | Is the node marked in any way, other than as being "untouched"?
data IsNodeMarked
-- | Reset the mark to its untouched state
data ResetMarkForNextGCCycle
-- | Ensure that the nodes to process are in a foldable container.
data GetFoldable

-- | Add a node to the worklist and mark it as pending.
{-# INLINEABLE addPendingNode #-}    
addPendingNode :: (Members '[
                    Tagged MarkNodeAsPending (Methodology node ()),
                    Tagged AddNodeToWorklist (Methodology node ())
                            ] r)=>
                   node -> Sem r ()
addPendingNode node = do
  tag @AddNodeToWorklist $ process node
  tag @MarkNodeAsPending $ process node
   

-- | To initialise our mark-and-collect algorithm, we need to start off with the roots.
{-# INLINEABLE markRoots #-}
markRoots :: (Foldable f,
              Members '[
               Tagged GetRoots (Input (f node)),
               Tagged MarkNodeAsPending (Methodology node ()),
               Tagged AddNodeToWorklist (Methodology node ())
              ] r)
             => Sem r ()
markRoots = do
  roots <- tag @GetRoots input
  forM_ roots addPendingNode

-- | The workhorse of the "mark" or "trace" phase.
{-# INLINEABLE processWorkList #-}
processWorkList :: (Foldable g,
                    Members '[
                     Tagged GetNextNodeToProcess (Input (Maybe node)),
                     Tagged RemoveNodeFromWorklist (Methodology node ()),
                     Tagged GetLinks (Methodology node (g node)),
                     Tagged IsNodeUnderConsideration (DecisionProcedureOn node),
                     Tagged IsNodeMarked (DecisionProcedureOn node),
                     Tagged MarkNodeAsFullyProcessed (Methodology node ()),
                     Tagged MarkNodeAsPending (Methodology node ()),
                     Tagged AddNodeToWorklist (Methodology node ())
                    ] r)
                   => Sem r ()
processWorkList = do
  nextToConsider <- tag @GetNextNodeToProcess $ input
  case nextToConsider of
    Nothing -> pure () -- We've finished marking; return!
    Just currentNode -> do
      tag @RemoveNodeFromWorklist $ process currentNode                  
      tag @MarkNodeAsFullyProcessed $ process currentNode
      links <- tag @GetLinks $ process currentNode
      forM_ links \link -> do
        alreadyMarked <- tag @IsNodeMarked $ decide link
        isActuallyUnderConsideration <- tag @IsNodeUnderConsideration $ decide link
        when ((not alreadyMarked) && isActuallyUnderConsideration) do
          addPendingNode link
      processWorkList
                        

-- | Mark roots and then trace them.
{-# INLINEABLE markPhase #-}           
markPhase :: (Foldable f, Foldable g,
              Members '[
               Tagged GetNodesToConsider (Input nodesToConsider),
               Tagged GetRoots (Input (f node)),
               Tagged GetNextNodeToProcess (Input (Maybe node)),
               Tagged RemoveNodeFromWorklist (Methodology node ()),
               Tagged IsNodeUnderConsideration (DecisionProcedureOn node),                      
               Tagged GetLinks (Methodology node (g node)),
               Tagged IsNodeMarked (DecisionProcedureOn node),
               Tagged MarkNodeAsFullyProcessed (Methodology node ()),
               Tagged MarkNodeAsPending (Methodology node ()),
               Tagged AddNodeToWorklist (Methodology node ())] r)
              => Sem r ()
markPhase = do
  markRoots
  processWorkList


-- | For every node originally under consideration, if it hasn't been marked, then it is unreachable, and therefore garbage.
{-# INLINEABLE sweepPhase #-}
sweepPhase :: (Foldable h,
              Members '[
               Tagged GetFoldable (Methodology nodesToConsider (h node)),
               Tagged ResetMarkForNextGCCycle (Methodology node ()),
               Tagged ScheduleForDeletion (Output node),
               Tagged GetNodesToConsider (Input nodesToConsider),
               Tagged IsNodeMarked (DecisionProcedureOn node)
               ] r)
              => Sem r ()
sweepPhase = do
  nodesToConsider <- tag @GetNodesToConsider input
  nodes <- tag @GetFoldable $ process nodesToConsider
  forM_ nodes \node -> do
    marked <- tag @IsNodeMarked $ decide node
    if marked
      then do
        tag @ResetMarkForNextGCCycle $ process node
      else do
        tag @ScheduleForDeletion $ output node

        
                

-- | The top-level function.
{-# INLINEABLE removeGarbage #-}
removeGarbage :: forall nodesToConsider node f g h r.(Foldable f, Foldable g, Foldable h,
              Members '[
               Tagged GetFoldable (Methodology nodesToConsider (h node)),
               Tagged ResetMarkForNextGCCycle (Methodology node ()),
               Tagged ScheduleForDeletion (Output node),
               Tagged GetNodesToConsider (Input nodesToConsider),
               Tagged GetRoots (Input (f node)),
               Tagged IsNodeUnderConsideration (DecisionProcedureOn node),
               Tagged GetNextNodeToProcess (Input (Maybe node)),
               Tagged RemoveNodeFromWorklist (Methodology node ()),
               Tagged GetLinks (Methodology node (g node)),
               Tagged IsNodeMarked (DecisionProcedureOn node),
               Tagged MarkNodeAsFullyProcessed (Methodology node ()),
               Tagged MarkNodeAsPending (Methodology node ()),
               Tagged AddNodeToWorklist (Methodology node ())] r)
              => Sem r ()
removeGarbage = do
  markPhase
  sweepPhase


        
    
    
