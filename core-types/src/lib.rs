extern crate rug;
extern crate string_cache;
extern crate urn;
extern crate uuid;
use rug::{Assign, Integer};
use urn::*;
use uuid::*;
mod cosmothought {
    include!(concat!(env!("OUT_DIR"), "/cosmothought_atom.rs"));
}

pub enum Identifier {
    AUrn {
        urn: Urn,
    },
    AUUID {
        uuid: Uuid,
    },
    ContextualIdentifier {
        uniqueNumber: Integer,
        context: Identifier,
    },
}

fn use_the_atom(t: &str) {
    match (cosmothought::SymbolAtom::from(t)) {
        symbol_atom!("type") => println!("Found foo!"),
        symbol_atom!("state") => println!("Found bar!"),
        // foo_atom!("baz") => println!("Found baz!"), - would be a compile time error
        _ => {
            println!("String not interned");
            // We can intern strings at runtime as well
            cosmothought::SymbolAtom::from(t);
        }
    }
}
